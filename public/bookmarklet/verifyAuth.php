<?php
/*------------------------------------------------------------------ 
 * Copyright (c) 2013-2014 by Giftvise, Inc.
 * All rights reserved. 
 *------------------------------------------------------------------
 */
?>

<?php
session_start();  

if (!isset($_GET["guid"]) && empty($_GET["guid"]))
{    
    echo "GUID is not set. Aborting..";
    exit();
}

$connection = mysqli_connect('localhost', 'root', 'giftvise', 'giftvise');
if (!$connection) {
    echo "Error: DB connect ." . PHP_EOL;
    echo "Errno: " . mysqli_connect_errno() . PHP_EOL;
    echo "Error: " . mysqli_connect_error() . PHP_EOL;
    exit;
}

$guid = $_GET["guid"];
$user_id = -1;

if ($_SESSION['user_id'] == '' && !isset($_SESSION['user_id']) && empty($_SESSION['user_id']))
{
    //echo "Not logged in " . $_SESSION['user_id'] . " ";
}
else
{
    //echo "logged in " . $_SESSION['user_id'] . " ";
    $user_id = $_SESSION['user_id'];

    // First remove all existing entries for the current user
    $result = mysqli_query($connection, "DELETE FROM `bookmarklet_auth` WHERE user_id = $user_id");
    if (!$result) {
        // echo "Failed to remove existing rows with user_id " . $user_id; 
    }
}

   // echo $user_id . " " . $guid . " ";

$result =mysqli_query($connection, "INSERT INTO `bookmarklet_auth` VALUES ($user_id, '$guid')");
if (!$result)
{
    echo "SQL query failed ." . PHP_EOL;
    echo "Errno: " . mysqli_connect_errno() . PHP_EOL;
    echo "Error: " . mysqli_connect_error() . PHP_EOL;
    exit;
}
mysqli_close($connection);
?>
