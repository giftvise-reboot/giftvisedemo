<?php
/*------------------------------------------------------------------ 
 * Copyright (c) 2013-2014 by Giftvise, Inc.
 * All rights reserved. 
 *------------------------------------------------------------------
 */
?>

<?php
if(!headers_sent())
    header("Content-Type: text/javascript");

$user_id = -1;

if (!isset($_GET["guid"]) && empty($_GET["guid"]))
{
    ouput(array('errorNumber' => 1, 'errorMessage' => 'guid missing', 'user_id' => $user_id));   
    die();
}

$guid = $_GET["guid"];

$connection = mysqli_connect('localhost', 'root', 'giftvise', 'giftvise');
if (!$connection) {
    ouput(array('errorNumber' => 2, 'errorMessage' => 'SQL connection failed', 'user_id' => $user_id, 'errno' => mysqli_connect_errno(), 'error' => mysqli_connect_error()));
    die();
}

$result =  mysqli_query($connection, "SELECT * from bookmarklet_auth WHERE guid = '$guid'");
if (!$result)
{
    ouput( array('errorNumber' => 4, 'errorMessage' => 'GUID lookup failed', 'user_id' => $user_id, 'errno' => mysqli_connect_errno(), 'error' => mysqli_connect_error()));
    die();
}

while($obj = $result->fetch_object()) {
    $user_id = $obj->user_id;
    if ($user_id == -1) {
	continue;
    }

    break;
}

ouput( array('errorNumber' => 0, 'errorMessage' => 'lookup successful', 'user_id' => $user_id), $connection);

function ouput($message, $connection)
{
    echo "giftvise_dataCallback(" . json_encode($message) . ");";
    mysqli_close($connection);
}
?>
