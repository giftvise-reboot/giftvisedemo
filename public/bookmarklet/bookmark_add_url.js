/*------------------------------------------------------------------ 
 * Copyright (c) 2013-2014 by Giftvise, Inc.
 * All rights reserved. 
 *------------------------------------------------------------------
 

/* start of bookmark.js */
var Bookmarklet = new Array;
Bookmarklet['look-up-table'] = [
                       /* Get the title using  getelementbyId's */
                       ['btAsinTitle', 'title', 'SAC_prodName'], 
                       /* Get the title using getelementsByClassName */    
  	               ['title','wrapper','sacItemInfo', 'productTitle','productInfo_suppliesAccessories'],   
                        /* Get the price using  getelementbyId's */
		       ['priceblock_ourprice'/* Amazon.com */,
                        'price','salePrcVal','productPriceFinanceSection','activeImage', 'actualPriceValue','priceDetails','tabWindow','pgPrice',
                        'offer_price', 'prcIsum', 'mm-saleDscPrc', 'priceblock_saleprice'],  
                        /* Get the price using getelementsByClassName */ 
		       ['full-price', 'priceLarge', /* Amazon.com */ 'tagline' /* apple */, 'price_amount',/*office depot*/, 'selling-price',
                        'pricing','price','item-price', 'a-color-price','price-current', 'onlinePriceWM', 'camelPrice', 'onlinePriceWM','offerPrice', 
		        'finalPrice','actual-price', 'pricing_sale_price','price dot_top','salePrice','priceSale','standardProdPricingGroup',
		        'pr-pricing-sec','pricing','cat_item_price','lineItemInfo', 'sale right', 'current_price', 'prod_price_options',
                        'mpsTotalPrice', 'productPrice', 'product-price','regPrice','sale-price','price-value', 'before_price', 'topPriceRange'], 
                         /* Get the image using  getelementbyId's */ 
	               ['productInfo', 'productMainImage','main-image','heroImage','mainImage','largeProductImage','productDetailActiveImage','product_image', 'pipLargePreview',
                        'landingImage','selection-gallery-img-big', 'productZoomImage',
                        'image'], 
                         /* Get the image using getelementsByClassName */
		       ['mainSlide', 'rightImgContainer','item image',
                        'product-model','hero-image ir'], 
                         /* Get the thumb images using getelementbyId's */ 
                       ['thumbs-image', 'thumbs'], 
                         /* Get the thumb images using getelementsByClassName */
                       ['thumbs-image', 'img-wrap'], 
                         /* Get the images using URL substring */
       		       ['hero','is/image','product/Large', 'PIC', 'product_assets', 'Product_Images/Dell_Client_Products',
                        'img_/publishing/styleSwatches','images/lbox', 'Storefront/Company',
                         'pics/products','product','i/p','.jpg','.JPG', '.png','image/Sears','image'],    
                          /* Get the Overview of the item using mata data descriprion */ 
		       ['description', 'Description','DESCRIPTION', 'og:description'],
                          /*  Get the Overview of the item using using getelementbyId's */
         	       ['tab1','productDescription','description', 'ProductDetail_ProductDetails_div','product-info'],
                          /*  Get the Overview of the item using using getelementsByClassName */ 
		       ['content','cutlineOverviewTitle','ql-details-short-desc'], 
		];

var NUM_OF_CATEGORIES = 11;
var category = new Array;
category['category-look-up'] = [
                                  /* main category */  /* sub categories */
                                     ['women',        '0',  'accessories', 'clothing', 'shoes'],
                                     [' men',        '0',  'accessories', 'clothing', 'shoes'],
                                     [' kids',        '0',  'accessories', 'clothing', 'shoes'],
                                     ['baby',        '1',  'accessories', 'clothing', 'shoes', 'strollers', 'Cradle', 'Crib'],
                                     ['fashion',      '1',  'watch','jewelry', ' ring', 'style', 'dress'],
                                     ['health',       '1',  'medicine', 'gym', 'excercise', 'fitness'],
				     [' home ',	      '0',  'bath', 'bedding', 'appliances', 'decor', 'kitchen', 'patio'],
                                     ['electronics',  '1',  'computers', 'laptops', 'tablets', 'cameras', ' TV ', 'audio', 'phones', 'games'],
				     ['sports',	      '1',  'bikes', 'outdoors', 'exercise', 'accessories', 'water', 'skates'],
				     [' pets',	      '1',  'cats', 'dogs'],
                                     [' art ',        '1',  'collectibles', 'coins', 'antiques', 'paintings'], 
                               ];


/* list out website specific url tags */
var url_tag = new Array;
url_tag['url_tag'] = {'amazon': "giftvise01-20"};

 
var DESC_INDEX_BY_ID=0;
var DESC_INDEX_BY_CLASS_NAME=1;	 
var PRICE_INDEX_BY_ID=2;
var PRICE_INDEX_BY_CLASS_NAME=3;
var IMAGE_INDEX_BY_ID=4;
var IMAGE_INDEX_BY_CLASS_NAME=5;
var THUMB_IMAGE_INDEX_BY_ID=6;
var THUMB_IMAGE_INDEX_BY_CLASS_NAME=7;
var THUMB_IMAGE_INDEX_BY_SUBSTRING=8;
var OVERVIEW_INDEX_BY_META_DATA=9;
var OVERVIEW_INDEX_BY_ID=10;
var OVERVIEW_INDEX_BY_CLASS_NAME=11;
var OVERVIEW_MAX_LEN= 2000;

var g_overview ='';
var userID = 0;

var cat_m;
var cat_s;

var gvbm_doc;
var prd_url;

/* Start of giftvise_bookmarklet class */
function giftvise_bookmarklet() 
{


/* Function to foind out the main category of the product */
this.get_item_main_category = function(source) {
     var url = source;
     var patter = '';
     var str;

     for (var i = 0; i < NUM_OF_CATEGORIES; i++) {
          var cube = category['category-look-up'][i];
          var len = cube[0].length;

          if (len > 7) {
               str = cube[0].substring(0, len - 1);
          } else {
               str = cube[0];
          }

          pattern = new RegExp(str, "i");
          if(pattern.test(url)) {
               cat_m = cube[0];// alert(cat_m);
               return;
          }
     }
}

/* Function to foind out the sub category of the product */
this.get_item_sub_category = function(source) {
     var url = source;
     var patter = '';
     var str;

     for (var i = 0; i < NUM_OF_CATEGORIES; i++) { 
          var cube = category['category-look-up'][i];
          for(var j = 2; j < cube.length; j++) {
              str = cube[j];
              /*
              var len = cube[j].length;
              if (len > 8) {
                  str = cube[j].substring(0, len - 3);
              } else if (len > 4){
                  str = cube[j].substring(0, len - 1); 
              } else {
                  str = cube[j];
              }
              */
              pattern = new RegExp(str, "i");
              if(pattern.test(url)) {
                  if(cat_s == '') {
                     cat_s= cube[j];
                     //alert(cat_s);
                  } 
                  if(cat_m == '' && cube[1] == '1') {
                       cat_m = cube[0]; //alert(cat_m);
                  }
                  return;
              }
          }
     }
}

/* Function to extract name/title/description of the item */
this.get_item_description = function() {
    var desc = null;
    var cube = Bookmarklet['look-up-table'][DESC_INDEX_BY_ID];  
    for(var j = 0; j < cube.length; j++) {
	try {
            desc = gvbm_doc.getElementById(cube[j]).innerText.trim();
	    if (desc != null) {
  		 break;
            }
	} catch(e) {
	    desc = null;
        }
    }
   
   if (desc == null) {
       cube = Bookmarklet['look-up-table'][DESC_INDEX_BY_CLASS_NAME];
       for(var j = 0; j < cube.length; j++) {
           try {
	       desc = gvbm_doc.getElementsByClassName(cube[j])[0].innerText.trim();
           } catch(e) {
	       desc = null;
	   }
       }
    }
    if (desc == null) {
	 desc = gvbm_doc.title;
    }
    if (desc) {  
    desc = desc.replace(/\s+/g,' ');
    desc = desc.replace(/(<\/?\w+?>)\s*?(<\/?\w+?>)|(<\/?\w+?>)/g,' ');
    desc = desc.replace(/(<([^>]+)>)/ig, "");
    }
    return desc;
}	 			

this.formatdetails = function(details) {
     /* Multiple spaces are removed, if any */
    details = details.replace(/\s+/g,' ');

    /* remove html tags */
    details = details.replace(/(<\/?\w+?>)\s*?(<\/?\w+?>)|(<\/?\w+?>)/g,' ');
    details = details.replace(/(<([^>]+)>)/ig, "");
    var idx;
    idx = details.indexOf('html');
    if (idx >= 0) {
        details = '';
    }
    idx = details.indexOf('javascript');
     if (idx >= 0) {
         details = '';
    }
    return details;
}

/* Function extract item overview */	
this.get_item_overview = function(item) {
    var details = null;


    cube = Bookmarklet['look-up-table'][OVERVIEW_INDEX_BY_META_DATA];
/*
    for(var j = 0; j < cube.length; j++) {
         try {
           details = $('meta[name='+cube[j]+']').attr("content");
             if (details != null) {
                details = this.formatdetails(details);
                return details;
             }
         } catch(e) {
             details = null;
         }
    }
*/
    var metaTags=gvbm_doc.getElementsByTagName("meta");

    for(var j = 0; j < cube.length; j++) {
      for (var i = 0; i < metaTags.length; i++) {
         if (metaTags[i].getAttribute("name") ==  cube[j]) {
             details = metaTags[i].getAttribute("content");
             details = this.formatdetails(details);
             return details;
         }
      }
   }
	
    cube = Bookmarklet['look-up-table'][OVERVIEW_INDEX_BY_ID];  
    for(var i = 0; i < cube.length; i++) {
	try {
              details = gvbm_doc.getElementById(cube[i]).innerText.trim();			 
	    } catch(e) {
		try {
		     details = gvbm_doc.getElementById(cube[i])[0].innerHTML.trim();
                } catch (e) {
	             details = null;
		}
	    }
	    if (details != null) {
                details = this.formatdetails(details);
	        return details;
	    }
     }
    cube = Bookmarklet['look-up-table'][OVERVIEW_INDEX_BY_CLASS_NAME];  
    if (details == null) {
        for(var i = 0; i < cube.length; i++) {
             try {
                 details = gvbm_doc.getElementsByClassName(cube[i])[0].innerText.trim();
   	     } catch(e) {
                 try {
  	              details = gvbm_doc.getElementsByClassName(cube[i])[0].innerHTML.trim();
  	         } catch (e) {
		      details = null;
	         }
             }
	     if (details != null) {
                 details = this.formatdetails(details);
	         return details;
             }
        }
    }
    /*
    cube = Bookmarklet['look-up-table'][OVERVIEW_INDEX_BY_META_DATA];
    for(var j = 0; j < cube.length; j++) {
         try {
           details = $('meta[name='+cube[j]+']').attr("content");
             if (details != null) {
                details = this.formatdetails(details);
                return details;
             }
         } catch(e) {
             details = null;
         }
    }
    var metaTags=gvbm_doc.getElementsByTagName("meta");

    for(var j = 0; j < cube.length; j++) {
    for (var i = 0; i < metaTags.length; i++) {
        if (metaTags[i].getAttribute("name") ==  cube[j]) {
            details = metaTags[i].getAttribute("content");
            details = this.formatdetails(details);
           return details;
        }
    }
  }
*/
    if (details == null || details == 'undefined') {
        if (item.title != null && item.title != 'undefined') {
            details = item.title; 
        } else {
           details="No description available";
        }
    }
    details = this.formatdetails(details);
    return details;
}			 

this.validate_price = function(price, item) {
    var currencies = new Array("$", "Rs","INR", "`", "USD", "EURO");
    item.item_currency = '$';

    if (price == '' || price == null)
        return;
 
    for (var i=0; i < currencies.length; i++) {
         var index = price.indexOf(currencies[i]);
         if (index >= 0) {
              switch(currencies[i]) {
              case "Rs":
              case "RS":
              case "INR":
                 item.item_currency = "INR";
                 break;
              case "USD":
                 item.item_currency = "$";
                 break;
              default:
             break;
             }
        }
    }

    var d_index = 0;
    if (price.length) {
        price = price.replace(/^\d+(?:\.\d{0,2})/g, '');
        price = price.replace(/[^0-9\.]+/g,"");
        d_index = price.indexOf(".");
    }
    if (price.length && (d_index == 0 || d_index == (price.length - 1))) {
         price = price.replace(/[\.]+/g,"");
    }
    if (price.length) {
        price = price.trim();
    }
    if (price.length < 2) {
        price = null;
    }

   if (price.length > 9) {
       price = price.substring(0, 9);
   }
  
    return price;
}

/* Function to extract item price */
this.get_item_price = function(item) {
  var price = null;
  var i;

  item.item_currency = '$';
 
   cube = Bookmarklet['look-up-table'][PRICE_INDEX_BY_ID];  
    for(i = 0; i < cube.length; i++) {
		try {
              price = gvbm_doc.getElementById(cube[i]).innerText.trim();			 
	    } catch(e) {
		    try {
		  	    price = gvbm_doc.getElementById(cube[i])[0].innerHTML.trim();
		    } catch (e) {
			    price = null;
		    }
		}
           if (price != null) {
                price = this.validate_price(price, item);
                if (price != null)
	  	  break;
           }
    }
    cube = Bookmarklet['look-up-table'][PRICE_INDEX_BY_CLASS_NAME];  
    if (price == null) {
        for(i = 0; i < cube.length; i++) {
	    try {
                price = gvbm_doc.getElementsByClassName(cube[i])[0].innerText.trim();
   	    } catch(e) {
  	        try {
  	            price = gvbm_doc.getElementsByClassName(cube[i])[0].innerHTML.trim();
                } catch (e) {
		    price = null;
	        }
	    }
	    if (price != null) {
                price = this.validate_price(price, item);
                if (price != null)
	        break;
            }
        }
    }
    /* process the string for charactors, special charators etc.. */
    /*
     if (price == null) {
         return price;
     }

    price = price.replace(/[^0-9.$]+/g, '');
    price = price.trim(); 

    var n=price.indexOf("$");
    var n1=price.indexOf("\.");
    if (n1 < 0) {
        price=price.concat("\.00");
        if (n >= 0) {
           price = price.substring(n+1, price.length);
        }
    } else if (n >= 0 && n1) { 
        price = price.substring(n+1, n1+3);
    } else if (n) {
       price = price.substring(n+2, price.length);
    }
    if (price.length < 4) {
        price ='';
    } */
    return price;
}				

/* function to extract images */	
this.get_item_image = function() {
    var item =  {};
    item.images = [];
    item.image_from_url = 0;

    var metaTags=gvbm_doc.getElementsByTagName("meta");

    for (var i = 0; i < metaTags.length; i++) {
        if (metaTags[i].getAttribute("property") == "og:image") {
            item.image = metaTags[i].getAttribute("content");
            break;
        }
    }
    if (item.image != null) {
        str = item.image;
        var n = str.search(/logo/i);
        if (n >= 0) {
           item.image = null;
        }
        n = str.search(/https/i);
        if (n < 0) {
           item.image = null;
        }
    }


    if (item.image == null) {
	cube = Bookmarklet['look-up-table'][IMAGE_INDEX_BY_ID]; 
	for(var i = 0; i < cube.length; i++) {
	    try {
                item.image = gvbm_doc.getElementById(cube[i]).attributes.src.value;
                if (item.image != null) {
	             break;
		}
	    } catch(e) {
                 try {
                    var someimage = gvbm_doc.getElementById(cube[i]);
                    var myimg = someimage.getElementsByTagName('img')[0];
                     if (myimg != null) {
                        item.image = myimg.src;
                        break;
                     }
                } catch(e) { item.image = null; }
	    }
	}
    }
    if (item.image == null) {
	cube = Bookmarklet['look-up-table'][IMAGE_INDEX_BY_CLASS_NAME]; 
	for(var i = 0; i < cube.length; i++) {
	    try { 
                item.image = gvbm_doc.getElementsByClassName(cube[i]).attributes.src.value;
                if (item.image != null) {
	            break;
	        }
	    } catch(e) {
                 try { 
                    var someimage = gvbm_doc.getElementsByClassName(cube[i]);
                    var myimg = someimage.getElementsByTagName('img')[0];
                     if (myimg != null) { 
                        item.image = myimg.src;
                        break;
                     }
                } catch(e) { item.image = null; }
	    }
	}   
    }

    item.images.length = 0;
    if (item.image == null || item.images.length == 0) {
  	 var n_tumb_images = 0;
         cube = Bookmarklet['look-up-table'][THUMB_IMAGE_INDEX_BY_SUBSTRING]; 
         for(var i = 0; i < cube.length; i++) {
              for(var j = 0; j < gvbm_doc.images.length; j++) {
  		  str = gvbm_doc.images[j].src;
                  var n = str.search(cube[i]);
  		  if (n >= 0)
  		  {
  	              if (item.image == null) {	  
  			  item.image =  gvbm_doc.images[j].attributes.src.value;;
  		      } else {
  		          item.images[n_tumb_images] = gvbm_doc.images[j].attributes.src.value;
  		      }  
  		      n_tumb_images++;
  		  }  
              } /*
              if (item.image != null && n_tumb_images != 0) {
 	  	  break;
 	      } */
          }
  	  item.image_from_url = 1;
  	  item.images.length = n_tumb_images;
     }
   return item;	
}

this.get_item_info = function() {
   
          var item = {};
           item.cat_main = '';
           item.cat_sub = '';
           
           var hostparts = prd_url.split(".");
           var host = hostparts[1]; //]hostparts.length - 2];
           item.title = this.get_item_description();
           item.overview = this.get_item_overview(item);
           item.price = this.get_item_price(item);
           var img = this.get_item_image();
           item.main_image = img.image;
           item.other_images = [];

           for(var i=0;i<img.images.length;i++)
           {
                     if (img.image_from_url == 1) {
                           item.other_images[i] = img.images[i];
                     } else {
                           item.other_images[i] = img.images[i].attributes.src.value;
                     }
           }
           if (url_tag['url_tag'][host] == "undefined" || url_tag['url_tag'][host] == null) {
               item.url_tag = ''; //prd_url;  
           } else {
               item.url_tag =  url_tag['url_tag'][host]; 
           }
           //item.url_hostname = window.location.hostname;

           /* get item main and sub categories */
           cat_m = '';
           cat_s = '';
           this.get_item_main_category(item.title);

           if (cat_m == '') {
                this.get_item_main_category(item.overview);
           }
           if (cat_m == '') {
                this.get_item_main_category(prd_url); //gvbm_doc.documentURI);
           }

           this.get_item_sub_category(item.title);

           if (cat_s == '') {
                this.get_item_sub_category(item.overview);
           }
           if (cat_s == '') {
                this.get_item_sub_category(prd_url);
           }
           item.cat_main = cat_m;
           item.cat_sub = cat_s;
           // alert(cat_m);alert(cat_s);
           return item;
      }
} /* End of giftvise_bookmarklet */	

/* invoke call back handler of autocomplete */
function giftvise_dataCallback_autocomplete(message) {
      g_callback(message);
}

function close_window() {
     document.getElementById("giftvise-window").remove();
     //gv.hide();
}
function add_complete() {
      var checkDiv = gvbm_doc.getElementById("bmcheck");
      checkDiv.className = "bm-check-image bm-show";      
      setTimeout(function() { close_window(); }, 1000);
}

/* function add description to the product */
function gifvise_dataCallback_addproduct(message) {

     if (message.ErrorCode == '1') {
        alert("Product has been added to your Wishlist, but we were unable to upload its image");
     }
     setTimeout(function() { add_complete(); }, 10);
     var other_p_idx1 = 0;
     var idx = g_vlink.indexOf('~');
     if(idx) {
         g_vlink = g_vlink.replace('~', '');
         other_p_idx1 = idx;
     }
     g_vlink = encodeURI(Base64.encode(g_vlink));
     var url = "https://www.giftvise.com/webservice/updatebookmarkletitem/user_id/" + message.user_id;
         url += "/item_id/" + message.item_id;
         url += "/description/" + g_overview;
         url += "/vendor_link/" + g_vlink;
         url += "/other_sp1_idx/" + other_p_idx1;

     var js = gvbm_doc.createElement("script");
         js.type = "text/javascript";
         js.async = true;
         js.src = url;
     gvbm_doc.getElementsByTagName("head")[0].appendChild(js);
}

function remove(a)
{
        elem = gvbm_doc.getElementById(a);
        parent = elem.parentNode;

        parent.removeChild(elem);

        return;
}

{
     if (typeof String.prototype.trim !== "function")
      {
           String.prototype.trim = function ()
           {
                return this.replace(/^\s\s*/, "").replace(/\s\s*$/, "")
            };
      }

      if (typeof String.prototype.toHex !== "function")
      {
           String.prototype.toHex = function ()
           {
                var hex = '';
                for(var i = 0; i < this.length; i++)
                {
                     hex += ''+this.charCodeAt(i).toString(16);
                 }
                 return hex;
           };
     }
}

/*
        This function will act as a class in JS it will be used mostly
        as a namespace in the bookmarklet, to ensure that our other
        classes and methods don't cause problems with anything already
        on the page.
*/
function giftvise() //class
{
/*
        this.authenticate = function()
        {
                var guid = this.generateGUID();
                var iframe = gvbm_doc.createElement("iframe");
                iframe.className = "giftvise_authFrame";
                iframe.src = "https://www.giftvise.com/bookmarklet/verifyAuth.php?guid=" + guid;

                gvbm_doc.getElementsByTagName("body")[0].appendChild(iframe);

                setTimeout(function() { window.gv.authenticateStep2(guid); }, 1000);

        }

        this.authenticateStep2 = function(guid)
        {
                this.addScript("https://www.giftvise.com/bookmarklet/auth.php?guid=" + guid);
        }

(/
        /*
                This method injects the CSS and JS files into the current site.
                Return: void
        */
        this.injectCSS = function()
        {
                css = document.createElement("link");
                css.href="https://www.giftvise.com/newdesign/css/bookmarklet.css";
                css.rel="stylesheet";
                css.type="text/css";
       }

        /*
                This method injects a script tag into the DOM
                Parameters:
                        url - the source file for the script tag
                Return: void
        */
        this.addScript = function(url)
        {
                var js = document.createElement("script");
                js.type = "text/javascript";
                js.async = true;
                js.src = url;

                document.getElementsByTagName("head")[0].appendChild(js);

        }

        /*
                Send item to the giftvise api
                Parameters:
                        item - an item object to be stored.
                Return:
                        boolean - represents the sucessful save of the object to the api
        */
        var saved = false
        this.save = function(item)
        {
                alert("Chikke1111");
/*
                if (saved) {
                    return;
                }
                saved = true;
                //Consturcting the url
                var url = "https://www.giftvise.com/webservice/addbookmarklet/user_id/" + this.userID + "/name/" + encodeURIComponent(Base64.encode(item.title));

               if (item.overview.length > OVERVIEW_MAX_LEN) {
                   item.overview = item.overview.substring(0, OVERVIEW_MAX_LEN);
                }
                url += "/items_required_no/1/price/" + encodeURI(item.price);
                url += "/currency/" + encodeURIComponent(item.item_currency);
                url += "/added_from/" + encodeURI(Base64.encode(item.url_hostname));
                url += "/privacy/" + item.privacy;
                url += "/cat_main/" + encodeURIComponent(item.cat_main);
                url += "/cat_sub/" + encodeURIComponent(item.cat_sub);
                url += "/image_url/" + encodeURI(Base64.encode(item.main_image));

                // ~ is not encoded in encodeURI and Base64.encode 
                var other_p_idx1 = 0;
                var idx = item.url.indexOf('~');
                if(idx > 0) {
                   item.url =item.url.replace('~', '');
                    other_p_idx1 = idx;
                }
                url += "/vendor_link/" + encodeURI(Base64.encode(item.url));
                url += "/other_sp1_idx/" + other_p_idx1;

                //load the file in a script tag, so we can get the return data
                this.addScript(url);
                userID = this.userID;
                overview = encodeURIComponent(Base64.encode(item.overview));
                g_overview = overview;
                g_vlink = item.url;
*/
                //this.updatedescription();
                close_window();
        }

        this.generateGUID = function()
        {
                var guid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c)
                {
                        var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
                        return v.toString(16);
                });

                return guid;
        }

   function getnames(term) {
        var js = gvbm_doc.createElement("script");
                js.type = "text/javascript";
                js.async = true;
                js.src = "https://www.giftvise.com/bookmarklet/autocomplete.php?term=" + term;

                gvbm_doc.getElementsByTagName("head")[0].appendChild(js);

  }

   function recomendFormAutocomplete(){
           $('.recommend-form').css('display','block');
          $('.recommend-item-to-friend').val('');
           var itemId = $('.recommend-item-to-friend').attr('item-id');
           $('.recommend-item-to-friend').autocomplete({
               source : function(request, callback) {
                            getnames(request.term);
                            g_callback = callback;
             },
                  minLength: 1,
                  select: function( event, ui ) {
                  //recommend product to friend
                 /*
                    $.ajax({url:"/newproducts/senditem?send_userID=" + ui.item.label + "&item_id="+itemId,
                       success:function(result){
                           $('.recommend-form').css('display','none');
                            alert(result.msg);
                    }});
                  */
                 //    $('.recommend-form').css('display','none');
                          //  alert(result.msg);
            }
            }).data( "ui-autocomplete" )._renderItem = function( ul, item ) {
                return $( "<li>" )
               .append( "<a>" + item.value + "</a>" )
               .appendTo( ul );
              // alert(item.value);
           };
      }

        this.displayItem = function(item)
        {
              document.getElementsByTagName("head")[0].appendChild(css);

              var d = document;

              var content_div = d.getElementById("bookmarklet-body");
              var loadImg = d.getElementById("load-img");
              loadImg.className = "hidden";

              var form = d.createElement("form");
              form.className = "gv-bookmarklet-clearfix";

              var fieldset = d.createElement("fieldset");
              fieldset.className = "gv-bookmarklet-clearfix";


              var bm_left_div = d.createElement("div");
              bm_left_div.className = "bookmarklet-left";

              var bm_left_image_div = d.createElement("div");
              bm_left_image_div.className = "bookmarklet-img";

              /*
              var bm_left_div_rec = d.createElement("div");
              var bm_left_div_rec_a = d.createElement("a");
                  bm_left_div_rec_a.className = "btn btn-sm btn-inline recommend-btn";

                  bm_left_div_rec_a.onmouseenter = function(e) {
                               e.preventDefault();
                             recomendFormAutocomplete();
                           }

              var bm_left_div_rec_a_span = d.createElement("span");
                  bm_left_div_rec_a_span.className = "glyphicon glyphicon-thumbs-up";
              var bm_left_div_rec_a_label = d.createElement("label");
                  bm_left_div_rec_a.innerHTML = "Recommend";
              var bm_left_div_rec_a_span1 = d.createElement("span");
                  bm_left_div_rec_a_span1.className = "recommend-form";
              var bm_left_div_rec_a_span1_input = d.createElement("input");
                  bm_left_div_rec_a_span1_input.type = "text";
                  bm_left_div_rec_a_span1_input.className = "form-control recommend-item-to-friend";
                  bm_left_div_rec_a_span1_input.placeholder = "Friend's name";

              bm_left_div_rec_a.appendChild(bm_left_div_rec_a_span);
              bm_left_div_rec_a_span1.appendChild(bm_left_div_rec_a_span1_input);
              bm_left_div_rec_a.appendChild(bm_left_div_rec_a_span1);

              bm_left_div_rec.appendChild(bm_left_div_rec_a);
              */

              var main_image_wrapper = d.createElement("div");
              main_image_wrapper.className = "bookmarklet-main-image-wrapper";
              var main_image = d.createElement("img");
              main_image.className = "bookmarklet-main-img";
              main_image.src = item.main_image;
              main_image.alt = "Product Name";
              //main_image.className = "bookmarklet-img-main-image";
              bm_left_image_div.appendChild(main_image_wrapper);
              var hr_left = d.createElement("div")
              hr_left.className = "bookmarklet-hr width200";
              bm_left_image_div.appendChild(hr_left);
              main_image_wrapper.appendChild(main_image);

              var img_switch_div = d.createElement("div");
              img_switch_div.className = "bookmarklet-imgswitch";
              var images_switch_div = d.createElement("div");
              images_switch_div.className = "bookmarklet-images-switch";
              var images_container_div = d.createElement("div");
              images_container_div.className = "bookmarklet-images-container";
              var div_left = d.createElement("div");
              div_left.style.float="left";
              var div_right = d.createElement("div");
              div_right.style.float="right";

              var button_left = d.createElement("button");
              button_left.className = "bookmarklet-btn bookmarklet-float-left";
              var imgswitch = document.getElementsByClassName("bookmarklet-images-container");
              var currentImage = document.getElementsByClassName("next-bookmarklet-image");
              var mainImage = document.getElementsByClassName("bookmarklet-main-img");
              var current = 0;
              button_left.onclick = function(e) {
                  e.preventDefault();
                  //check for first image
                  if ((" " + imgswitch[0].firstChild.className + " ").replace(/[\n\t]/g, " ").indexOf(" next-bookmarklet-image ") <0 ){
                      var imagePreviousSibling = currentImage[0].previousSibling;
                      imagePreviousSibling.setAttribute("class","item-image-wrapper next-bookmarklet-image");
                      imagePreviousSibling.nextSibling.setAttribute("class","item-image-wrapper");
                      mainImage[0].setAttribute("src",imagePreviousSibling.firstChild.getAttribute("src"));
                      mainImage[0].style.height=(imagePreviousSibling.firstChild>200)?"auto":"200px";
                      current +=(current<((imgswitch[0].children.length -1)*50))? -50 : -100;
                      images_container_div.style.left="-"+current+"px";
                  }

              }
                  button_left.innerHTML = "&lt;"
              var button_right = d.createElement("button");
              button_right.className = "bookmarklet-btn bookmarklet-float-right";
              button_right.innerHTML = "&gt;"
              button_right.onclick = function(e) {
                  e.preventDefault();
                  //check for last image
                  if ((" " + imgswitch[0].lastChild.className + " ").replace(/[\n\t]/g, " ").indexOf(" next-bookmarklet-image ") <0 ){
                      mainImage[0].setAttribute("src",currentImage[0].nextSibling.firstChild.getAttribute("src"));
                      mainImage[0].style.height=(currentImage[0].nextSibling.firstChild>200)?"auto":"200px";
                      currentImage[0].nextSibling.className = "item-image-wrapper next-bookmarklet-image";
                      currentImage[0].className = "item-image-wrapper";
                      current += 50;
                      if(current<((imgswitch[0].children.length -1)*50)){
                         images_container_div.style.left="-"+current+"px";
                      }
                  }
              }

              var scroll_help_div = d.createElement("div");
               scroll_help_div.className = "scroll-help";
              var scroll_help = d.createElement("p");
              scroll_help.innerHTML = "&nbsp;&nbsp;&nbsp;&nbsp; Use < > arrows to scroll to the desired image";
              scroll_help_div.appendChild(scroll_help);

              var bm_right_content_div = d.createElement("div");
              bm_right_content_div.className = "bookmarklet-right";

              var bm_product_name_div = d.createElement("div");
              bm_product_name_div.className = "bookmarklet-form-group";
              var label = d.createElement("label");
              label.for = "input-name";
              label.innerHTML = "Name";
              var input_t = d.createElement("input");
              input_t.type = "text";
              input_t.className = "bookmarklet-form-control";
              input_t.id="input-name"
              input_t.placeholder="Product Name"
              input_t.value = item.title;

              var bm_product_price_div = d.createElement("div");
              bm_product_price_div.className = "bookmarklet-form-group";
              var label_p = d.createElement("label");
              label_p.for = "input-price";
              label_p.innerHTML = "Price in $";

               var price_check = d.createElement("label");
              price_check.innerHTML = "";
              price_check.className="gv_bm_error";
              price_check.style.color='red';
              price_check.id = "price-check";


              var str1 = "Price in";
              if (item.item_currency != null) {
                   label_p.innerHTML  = str1.concat(" ", item.item_currency);
              } else {
                 //label_p.innerHTML = "Price";
              }

              var input_p = d.createElement("input");
              input_p.type = "text";
              input_p.className = "bookmarklet-form-control";
              input_p.id="bm-input-price"
              input_p.value = item.price;

              var bm_product_desc_div = d.createElement("div");
              bm_product_desc_div.className = "bookmarklet-form-group";
              var label_desc = d.createElement("label");
              label_desc.for = "input-desc";
              label_desc.innerHTML = "Description";
              var input_desc = d.createElement("textarea");
              input_desc.className = "bookmarklet-form-control";
              input_desc.id="input-desc"

              item.overview = item.overview.replace(/Description/i, '');
              input_desc.value = item.overview;
              var bm_product_buttons_div = d.createElement("div");
              bm_product_buttons_div.className = "bookmarklet-buttons-group";

              var bm_product_privacy_div = d.createElement("div");
               bm_product_privacy_div.className = "privacy-switch"; //bookmarklet-privacy";

              var user_privacy = 0; // off
              var label_privacy = d.createElement("a");
                   label_privacy.className = "privacy-input switch";
                   label_privacy.innerHTML = "Keep Private";
                   label_privacy.onclick = function(e) {
                   if (!user_privacy) {
                      label_privacy.className = "privacy-input switch active";
                      user_privacy = 1;
                   }  else {
                      label_privacy.className = "privacy-input switch";
                      user_privacy = 0;
                   }
              }

              bm_product_privacy_div.appendChild(label_privacy);

              var price_edit = false;

              var save_button = d.createElement("button");
              save_button.className = "bookmarklet-btn add-btn-min-width";
              save_button.innerHTML = "Add";
              save_button.type = "submit";
              save_button.onclick = function(e) {
                      e.preventDefault();
                      if (input_p.value == "" || !(input_p.value) || input_p.value == null){
                          price_check.innerHTML = "Please enter price";
                          return false;
                         /*
                         var r = confirm("Price field is empty. Press OK to enter manually otherwise CANCEL.");
                         if(r == true) {
                            return false;
                         }
                         */
                         input_p.value = ' ';
                      }

                      if (input_desc.value == "" || !(input_desc.value) || input_desc.value == null){
                         /*
                              var r = confirm("Description field is empty. Press OK to enter manually otherwise CANCEL.");
                              if(r == true) {
                                 return false;
                              }
                          */
                         input_desc.value = "Description not available yet"
                      }
                      if (user_privacy) {
                          item.privacy = '0';
                      } else {
                          item.privacy = '1';
                      }
                      item.price = input_p.value;
                      item.title = input_t.value;
                      item.overview = input_desc.value;
                      item.main_image = main_image.src;
                      window.gv.save(item);
              };
                var itemImagesWrapper = d.createElement('div');
                itemImagesWrapper.className="item-image-wrapper next-bookmarklet-image";
                images_container_div.appendChild(itemImagesWrapper);
                images_switch_div.appendChild(images_container_div);

                var itemImages = d.createElement('img');
                itemImages.src=item.main_image;
                itemImages.className="item-image";
                itemImagesWrapper.appendChild(itemImages);

              for(var i= 0; i < 10 && i<item.other_images.length;i++)
              {
                   if(item.other_images[i] == '' || item.other_images[i] == 'undefined'
                      || item.other_images[i] == null) {
                        continue;
                   }
                   var itemImagesWrapper = d.createElement('div');
                   itemImagesWrapper.className="item-image-wrapper";
                   images_container_div.appendChild(itemImagesWrapper);
                   images_switch_div.appendChild(images_container_div);

                   var itemImages = d.createElement('img');
                   itemImages.src=item.other_images[i];
                   itemImages.className="item-image";
                   itemImagesWrapper.appendChild(itemImages);

              }
              div_left.appendChild(button_left);
              img_switch_div.appendChild(div_left);
              img_switch_div.appendChild(images_switch_div);
               div_right.appendChild(button_right);
              img_switch_div.appendChild(div_right);
              bm_left_div.appendChild(bm_left_image_div);

              if (item.other_images.length > 0) {
                  bm_left_div.appendChild(img_switch_div);
                  bm_left_div.appendChild(scroll_help_div);
              }
              //bm_left_div.appendChild(bm_left_div_rec);

              var check_image_div = d.createElement('div');
              check_image_div.className ="bm-check-image bm-hide";
              check_image_div.id = "bmcheck";

              /*
              var check_image = d.createElement('img');
              check_image.src = "httpss://www.giftvise.com/bookmarklet/add_complete_check.png";
              check_image.style.height="52px";
              check_image.style.width="68px";
              check_image.style.position="absolute";
              */

              var labela = d.createElement("label");
              labela.className="add-label";
              labela.innerHTML = "Added";

              //check_image_div.appendChild(check_image);
              check_image_div.appendChild(labela);
              bm_left_div.appendChild(check_image_div);

              fieldset.appendChild(bm_left_div);

              bm_product_name_div.appendChild(label);
              bm_product_name_div.appendChild(input_t);

              bm_product_price_div.appendChild(label_p);
              bm_product_price_div.appendChild(input_p);
              bm_product_price_div.appendChild(price_check);

              bm_product_desc_div.appendChild(label_desc);
              bm_product_desc_div.appendChild(input_desc);
              bm_product_buttons_div.appendChild(bm_product_privacy_div);
              bm_product_buttons_div.appendChild(save_button);

              bm_right_content_div.appendChild(bm_product_name_div);
              bm_right_content_div.appendChild(bm_product_price_div);
              bm_right_content_div.appendChild(bm_product_desc_div);
              var hr_right = d.createElement("div")
              hr_right.className = "bookmarklet-hr width296";
              bm_right_content_div.appendChild(hr_right);
              bm_right_content_div.appendChild(bm_product_buttons_div);

              fieldset.appendChild(bm_right_content_div);
              form.appendChild(fieldset);
              content_div.appendChild(form);
              alert(content_div.innerHTML);
        }
    this.displayLoading = function()
    {
        document.getElementsByTagName("head")[0].appendChild(css);

        var d = document;
        var mainDiv = d.createElement("div");

        mainDiv.className = "gv-bookmarklet";
        mainDiv.id = "giftvise-window";

        var gv_header = d.createElement("div");
        gv_header.className = "bookmarklet-header";

        var h_button = d.createElement("button");
        h_button.className = "close";
        h_button.innerHTML = "&times;";
        h_button.onclick = function() { close_window(); };

        var h4 = d.createElement("h4");
        h4.className = "bookmarklet-title";
        h4.innerHTML = "Add product to wishlist";

        var img = d.createElement("img");
        img.className = "gv-logo"
        img.src="https://www.giftvise.com/newdesign/img/tpl/new-giftvise-logo.png";
        img.alt="giftvise";

        h4.appendChild(img);
        gv_header.appendChild(h_button);
        gv_header.appendChild(h4);

        var content_div = d.createElement("div");
        content_div.className = "bookmarklet-body";
        content_div.id = "bookmarklet-body";

        var loadImg = d.createElement("img");
        loadImg.id = "load-img"
        loadImg.src="https://www.giftvise.com/images/ajax-loader.gif";
        loadImg.alt="Load image";

        content_div.appendChild(loadImg);

        document.getElementsByTagName("body")[0].appendChild(mainDiv);
        var targetDiv = document.getElementById("giftvise-window");

        targetDiv.appendChild(gv_header);
        targetDiv.appendChild(content_div);
    }

    this.hide = function(){
        var targetDiv = document.getElementById("giftvise-window");
        targetDiv.className = "hidden";
    }

var logged_in = 1;
var refreshIntervalId = 0;
var retry = 30;
var display = true;

this.displayItemNew = function(item)
{
     var imgswch = document.getElementById("bookmarklet-images-switch");

/*
     if (imgswch) {
         if ($('#bookmarklet-images-switch').parent.children().eq(0).length) {
             $('#bookmarklet-images-switch').parent.children().eq(0).remove();
         }
     }
*/
     $('.bookmarklet-main-img').attr('src', item.main_image);
     $('.add-item-url .product-name').attr('value', item.title);
     $('.add-item-url .price-url').attr('value', item.price);
     item.overview = item.overview.replace(/Description/i, '');

     $('#product-details-overview').val(item.overview);
     var category;
     if(item.cat_main != '' && item.cat_sub != '') {
         category = item.cat_main + ', ' + item.cat_sub;
     } else {
         category = item.cat_main +  item.cat_sub;
     }
     $('.add-item-url .product-tags').attr('value', category);
     $('.add-item-url .price-currency').html('Price('+item.item_currency+')*');
     $('.add-item-url .price-currency').attr('curr_type', item.item_currency);
     $('.add-item-url .price-currency').attr('url_tag', item.url_tag); 

 /*
      var d = document; 
      var images_switch_div = d.createElement("div");
              images_switch_div.className = "bookmarklet-images-switch";
              images_switch_div.id = "bookmarklet-images-switch";
              var images_container_div = d.createElement("div");
              images_container_div.className = "bookmarklet-images-container";
              var div_left = d.createElement("div");
              div_left.style.float="left";
              var div_right = d.createElement("div");
              div_right.style.float="right";

              var button_left = d.createElement("button");
              button_left.className = "bookmarklet-btn bookmarklet-float-left";
              var imgswitch = document.getElementsByClassName("bookmarklet-images-container");
              var currentImage = document.getElementsByClassName("next-bookmarklet-image");
              var mainImage = document.getElementsByClassName("bookmarklet-main-img");
              var current = 0;
              button_left.onclick = function(e) {
                  e.preventDefault();
                  //check for first image
                  if ((" " + imgswitch[0].firstChild.className + " ").replace(/[\n\t]/g, " ").indexOf(" next-bookmarklet-image ") <0 ){
                      var imagePreviousSibling = currentImage[0].previousSibling;
                      imagePreviousSibling.setAttribute("class","item-image-wrapper next-bookmarklet-image");
                      imagePreviousSibling.nextSibling.setAttribute("class","item-image-wrapper");
                      mainImage[0].setAttribute("src",imagePreviousSibling.firstChild.getAttribute("src"));
                      mainImage[0].style.height=(imagePreviousSibling.firstChild>200)?"auto":"200px";
                      current +=(current<((imgswitch[0].children.length -1)*50))? -50 : -100;
                      images_container_div.style.left="-"+current+"px";
                  }

              }
              button_left.innerHTML = "&lt;"
              var button_right = d.createElement("button");
              button_right.className = "bookmarklet-btn bookmarklet-float-right";
              button_right.innerHTML = "&gt;"
              button_right.onclick = function(e) {
                  e.preventDefault();
                  //check for last image
                  if ((" " + imgswitch[0].lastChild.className + " ").replace(/[\n\t]/g, " ").indexOf(" next-bookmarklet-image ") <0 ){
                      mainImage[0].setAttribute("src",currentImage[0].nextSibling.firstChild.getAttribute("src"));
                      mainImage[0].style.height=(currentImage[0].nextSibling.firstChild>200)?"auto":"200px";
                      currentImage[0].nextSibling.className = "item-image-wrapper next-bookmarklet-image";
                      currentImage[0].className = "item-image-wrapper";
                      current += 50;
                      if(current<((imgswitch[0].children.length -1)*50)){
                         images_container_div.style.left="-"+current+"px";
                      }
                   }
              } 
              var itemImagesWrapper = d.createElement('div');
                itemImagesWrapper.className="item-image-wrapper next-bookmarklet-image";
                images_container_div.appendChild(itemImagesWrapper);
                images_switch_div.appendChild(images_container_div);

                var itemImages = d.createElement('img');
                itemImages.src=item.main_image;
                itemImages.className="item-image";
                itemImagesWrapper.appendChild(itemImages);

              for(var i= 0; i < 10 && i<item.other_images.length;i++)
              {
                   if(item.other_images[i] == '' || item.other_images[i] == 'undefined'
                      || item.other_images[i] == null) {
                        continue;
                   }
                   var itemImagesWrapper = d.createElement('div');
                   itemImagesWrapper.className="item-image-wrapper";
                   images_container_div.appendChild(itemImagesWrapper);
                   images_switch_div.appendChild(images_container_div);

                   var itemImages = d.createElement('img');
                   itemImages.src=item.other_images[i];
                   itemImages.className="item-image";
                   itemImagesWrapper.appendChild(itemImages);

              }
              div_left.appendChild(button_left);
              
              var img_switch_div = document.getElementById('bookmarklet-imgswitch');

              img_switch_div.appendChild(div_left);
              img_switch_div.appendChild(images_switch_div);
              div_right.appendChild(button_right);
              img_switch_div.appendChild(div_right);
   */
}
this.parseItem = function(html_obj, url)
{
    gvbm_doc = html_obj;
    prd_url = url;
/*
     if (gv.userID == -1) {
         if (retry) {
             retry--;
         } else {
             if (logged_in == 1) {
                 gv.hide();
                 alert("Please try again");
             }
             window.location.reload(); 
         }
         return;
     } else { 
        // stop timer function 
        clearInterval(refreshIntervalId);
        if (display) {
            gft_bm = new giftvise_bookmarklet();
            item = gft_bm.get_item_info();
            gv.displayItem(item);
            display = false;
        }
    }
*/
            gft_bm = new giftvise_bookmarklet();
            var item = gft_bm.get_item_info();
            this.displayItemNew(item);
           // this.displayItem(item);

}

this.userID = -1;

 /* Constructor */
 {
    this.injectCSS();
    this.addScript("https://www.giftvise.com/bookmarklet//base64.js");
    /*
        this.addScript("https://www.giftvise.com/newdesign/js/vendor/jquery-ui.js");
        this.addScript("https://www.giftvise.com/newdesign/js/vendor/jquery-ui-1.10.3.custom.js");
    */
  }

  this.begin = function()
  {
/*     logged_in = 1;
     gv.userID = -1;
     this.authenticate();
     this.displayLoading();
     refreshIntervalId = setInterval(this.parseItem, 200);
*/
//    this.displayLoading();
  //  this.parseItem();
  }

} /* end of giftvise class */

/*
  This is the method used buy JSONP to provide data back to 
  the bookmarklet from a domain outside the current scope.
  Note: This method must be placed in the global scope.
     
  Parameters:
  message - json object
  Return:
  void
*/
function giftvise_dataCallback(message)
{
    //parse response
    if(message.errorNumber > 0)
    {
       //console.log(message.errorNumber);
    }
    //alert(message.user_id);
    //alert(message.errorNumber);

    if(message.user_id == -1)
    {
        gv.hide();
        gv.userID = -1; 
        logged_in = 0;
        alert("Please login to Giftvise, revisit this page and then click on the bookmarklet again"); 
        window.open("https://www.giftvise.com");
        window.location.reload();
        return;
    }

    if(typeof message.user_id !== "undefined" && message.user_id != -1)
    {
         //window.gv.userID = message.user_id;
         gv.userID = message.user_id;
         this.userID = message.user_id;
    }

    return;
}

var item, gv = new giftvise();
gv.begin();


