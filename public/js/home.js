jQuery(window).load(function(){
    $('body').on('change', ".product-curate-desc", function(e){
        var product_id = $(this).attr('product-id');
        var description = $('.editable-input textarea').val();
        $.ajax({
            url: '/products/itemcuratedinfo?product_id='+product_id+'&description='+description,
            success: function(response) {
                if (response.success) {
                } else {
                    alert("Could not update information", null);
                }
            }
        });
    });

    $('body').on('change', ".feature-curater-desc", function(e){
        var user_id = $(this).attr('user-id');
        var description = $('.editable-input textarea').val(); 
        $.ajax({
            url: '/products/curatorcuratedinfo?user_id='+user_id+'&description='+description,
            success: function(response) {
                if (response.success) {
                } else {
                    alert("Could not update information", null);
                }
            }
        });
    });

    $('body').on('change', ".curate-event-title", function(e){
          var event_id = $(this).attr('event-id');
          var title = $('.editable-input input').val();
          $.ajax({
            url: '/event/eventcuratetitle?event_id='+event_id+'&title='+title,
            success: function(response) {
                if (response.success) {
                } else {
                    alert("Could not update information", null);
                }
            }
        });
    });

    var tag_edit = 0;
    $('body').on('change', ".curate-event-desc", function(e){

        if(tag_edit) { tag_edit = 0; return; }

        var event_id = $(this).attr('event-id');
        var description = $('.editable-input input').val();
        $.ajax({
            url: '/event/eventcurate?event_id='+event_id+'&description='+description,
            success: function(response) {
                if (response.success) {
                } else {
                    alert("Could not update information", null);
                }
            }
        });
    });

    $('body').on('change', "#curated_event_tags", function(e){
        tag_edit = 1; 
        var event_id = $(this).attr('event-id');
        var description = $('.editable-input input').val();
        description = description.replace(/ +(?= )/g,'');
        $.ajax({
            url: '/event/eventcuratetags?event_id='+event_id+'&description='+description,
            success: function(response) {
                if (response.success) {
                } else {
                    alert("Could not update information", null);
                }
            }
        });
        if(description != 'Add tags') { 
           var $buy_btn = $(this).parent();
           $buy_btn = $buy_btn.find('#find_gifts_btn');
           var current = $buy_btn.attr('href').split("=");
           var newstr = description.replace(/[, ]/g, "|");
           newstr = current[0]+'='+newstr; 
           $buy_btn.attr('href', newstr);
        }
    });

    $('.bt-editable').editable();
});
