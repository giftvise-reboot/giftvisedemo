function addDynamicValidaror(){
    $('.event_name').each(function() {
        $(this).rules("add", {
            required: true
        })
    });
    $('.event_date').each(function() {
        $(this).rules("add", {
            required: true
        })
    });
}
function initValidateSignUpStepThreeForm(){
    $('#sign-up-step-three-form').validate();
}
$(document).ready(function() {
    initValidateSignUpStepThreeForm();
    $(document).on('submit', '#sign-up-step-three-form', function(e) {
        addDynamicValidaror();
        e.preventDefault(); 
        if ($('#sign-up-step-three-form').validate().form()) {
            $.ajax({
                url:"/login/signupstepthree",
                type:"POST",
                data: $('#sign-up-step-three-form').serialize(),
                success:function(result){
                    if (result.success){
                        window.location.replace(document.location.origin + "/login/home");
                    }else if (result.errors) {
                        result.errors.forEach(function(error) {
                            $('#' + error.field).after('<label class="error">' + error.errMsg + '</label>');
                        });
                    } else {                        
                        $('#skip-link').after('<label class="error">There are some problems! Please try later on account page. Thank you!</label>');
                    }
                }
            })
        }
    });
    $(document).on('click', '.minus', function(e) {
        var eventsCount = parseInt($('#events_count').val());
        var idText = $(this).attr('id').split('-');
        var currentIndex = parseInt(idText[1]);

        $(this).parent().parent().remove();
        while (currentIndex < eventsCount) {
            var nextIndex = parseInt(currentIndex) + 1;
            $('#event-' + nextIndex).attr('id','event-' + currentIndex).attr('name', 'event-' + currentIndex);
            $('#event-date-' + nextIndex).attr('id','event-date-' + currentIndex).attr('name', 'event-date-' + currentIndex);
            $('#index-' + nextIndex).attr('id','index-' + currentIndex).attr('name', currentIndex++);
        }
        $('#events_count').val(--eventsCount);
        if (eventsCount == 9) {
            $('#index-9').attr('class','plus').val('+');
        }
        initValidateSignUpStepThreeForm();
    });
    $(document).on('click', '.plus', function(e) {
        var eventsCount = parseInt($('#events_count').val());
        if (eventsCount < 10) {
            var inputClone = $(this).parent().parent().clone();
            var newIndex = eventsCount + 1;
            $('#event-' + eventsCount).attr('id','event-' + newIndex).attr('name', 'event-' + newIndex);
            $('#event-' + newIndex).val('');
            $('#event-date-' + eventsCount).attr('id','event-date-' + newIndex).attr('name', 'event-date-' + newIndex);
            $('#event-date-' + newIndex).val('');
            $('#index-' + eventsCount).attr('id','index-' + newIndex).attr('name', newIndex);
            $(this).parent().parent().before(inputClone);
            $('#index-' + eventsCount).attr('class','minus').val('-');
            $('#events_count').val(++eventsCount);
            if (eventsCount == 10) {
                $('#index-10').attr('class','minus').val('-');
            }
        }
        initDateTimePicker();
        initValidateSignUpStepThreeForm();

    })
});