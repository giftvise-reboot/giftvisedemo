jQuery(window).load(function(){
    var $container = $('#portfolio');
    var $loadComplete = true;

    function initFilter() {
        $('#portfolio-filter a').click(function () {
            $('#portfolio-filter li').removeClass('activeFilter');
            $(this).parent('li').addClass('activeFilter');
            var selector = $(this).attr('data-filter');
            $container.isotope({filter: selector});
            return false;
        });
    }

    function updateFilterByTagsList() {
        // get item ids
        var itemIds = "('";
        var noElements = true;
        $('.portfolio-image').each(function () {
            itemIds += $(this).attr('id') + "','";
            noElements = false;
        });

        //alert(itemIds);

        var $page = 'home';
        if (noElements == false) {
            var filters = new Array();
            $('#portfolio-filter li a').each(function () {
                filters.push($(this).attr('data-filter'));
            });
            $.ajax({
                type: "POST",
                url: "/products/getproductstagsasarray?page=" + $page + "&item_ids=" + itemIds.slice(0, -2) + ")",
                success: function (result) {
                    if (!(result.fail)) {
                        var filters = new Array();
                        $('#portfolio-filter li a').each(function () {
                            filters.push($(this).attr('data-filter'));
                        });
                        result.tags.forEach(function(tag) {
                            if ((tag.name !== null) && (tag.name !== '')) {
                                var tagName = '.' + tag.name.trim();
                                if (jQuery.inArray(tagName, filters) === -1) {
                                    var newFilter = $('#portfolio-filter').children().eq(1).clone();
                                    $('#portfolio-filter').append(newFilter);
                                    newFilter = $('#portfolio-filter').children().eq($('#portfolio-filter').children().length - 1);
                                    newFilter.children().eq(0).attr('data-filter', tagName).html(tag.name.trim());
                                }
                            }
                        });
                        $('#portfolio-filter').append(result);
                    }
                }
            });
        }
    }


    function LoadMoreProducts(beg) {

        if ($loadComplete != true) {
            return;
        }
        $loadComplete = false;

        var $next_start = 0;
        $('#portfolio').each(function(){
            $next_start = $(this).attr('next_start');
        });
        $next_start= $(".portfolio-image > div").size();
        $page = 'home&pgid=1';
        $subpage = '';
        $curatetype = '';
        $.ajax({
            cache:false,
            url: '/products/productsgrid?ajax=1&next_start='+ $next_start +'&page='+ $page +'&append=true&subpage='+$subpage+'&curatetype='+$curatetype,
            success: function(data){
                $container.isotope('insert', $(data), function(){
                    $container.imagesLoaded(function() {
                        $container.isotope('reLayout');
                    });
                });
                updateFilterByTagsList();
                initFilter();
                $loadComplete = true;
                /*
                 //$('#load-progress').hide();
                 //$('#load-progress img').hide();
                 updateFilterByTagsList();
                 initToggleTileHoverState();
                 initToolTips();
                 timeConvert();
                 loadComplete = true;*/
            }
        });
    }

    $container.isotope({ transitionDuration: '0.65s' });

    initFilter();

    $('#portfolio-shuffle').click(function(){
        $container.isotope('updateSortData').isotope({
            sortBy: 'random'
        });
    });

    $(window).resize(function() {
        $container.isotope('layout');
    });
    $(window).scroll(function(){
        if ((window.innerHeight + window.scrollY) >= ((0.75) * document.body.offsetHeight)) {
            // you're at the bottom of the page
            LoadMoreProducts(false);
        }
    });
    
    // remove from wishlist
    $(document).on('click', '.wishlist-delete', function(e){
        e.preventDefault();
        var $btn = $(this);
        $.ajax({
            url:"/products/deletewishlist?item_id="+$btn.attr('item-id')+"&user_id="+$btn.attr('user-id'),
            success:function(){
                var id = $btn.parent().parent().parent().attr('id');
                //remove tags if this is the last item with 
                var itemClasses = $('#'+id).attr('class').split(' ');
                itemClasses.forEach(function(itemClass) {
                    if ((itemClass != '') && ($('.'+itemClass).length == 1) && ($('[data-filter=".'+itemClass+'"]').length == 1)) {
                        $('[data-filter=".'+itemClass+'"]').parent().remove();
                    }
                });                
                //remove item
                $container.isotope('remove', $('#'+id));
                $container.isotope('layout');
                 initFilter();
            }
        });
    });

    $(document).on('click', '#write-about-me', function(e){
        var $cur_text = $('#about-me-statement').innerHTML;
        $('.write-about-me-save-btn').show();
        $('#about-me-info-edit').show();
        $('#about-me-info-edit').focus();
        $('#about-me-info-edit').innerHTML = $cur_text;
        $('.about-me-statement').hide();
    });



    $('body').on('change', "#edit-blog-link", function(e){
        var description = $('.editable-input input').val();
        $.ajax({
            type: "POST",
            url:'/login/updatebloglink?bloglink='+description+'',
            success:function(result){
            }
        });
        $("#blog-link-go").attr("href", description);
        $("#blog-link-go").html(description);
        $('.editable-input input').val('(edit link)');
    });

    $('.bt-editable').editable();

    $(document).on('click', '#button-about-me-save', function(e){
        e.preventDefault();
        var $about_me_txt = document.getElementById("about-me-info-edit");;
        $.ajax({
            type: "POST",
            url:'/login/updateaboutme?about-me-info='+$about_me_txt.value+'',
            success:function(result){
            }
        });
        $('#about-me-statement').show();
        document.getElementById("about-me-statement").innerHTML = $about_me_txt.value
        $('#about-me-info-edit').hide();
        $('.write-about-me-save-btn').hide();
    });

    $(document).on('click', '#user-upload-image', function(e) {
        $('#user-image').trigger('click');
    });

    // call global upload functionality from file-upload.js
    $.getScript(document.location.origin + '/js/abstract-file-upload.js', function() {
        var params = {
            image: {
                class: 'user-image',
                width: 620,
                height: 462,
                thumbwidth: 270
            },
            redirect: {
                url: '/settings/updateaccount'
            }
            
        };
        upload('.portfolio-image', params);
    });
/*
    var gvbm_doc; 
    var prd_url; 


(function(DOMParser) {  
    "use strict";  
    var DOMParser_proto = DOMParser.prototype  
      , real_parseFromString = DOMParser_proto.parseFromString;

    // Firefox/Opera/IE throw errors on unsupported types  
    try {  
        // WebKit returns null on unsupported types  
        if ((new DOMParser).parseFromString("", "text/html")) {  
            // text/html parsing is natively supported  
            return;  
        }  
    } catch (ex) {}  

    DOMParser_proto.parseFromString = function(markup, type) {  
        if (/^\s*text\/html\s*(?:;|$)/i.test(type)) {  
            var doc = document.implementation.createHTMLDocument("")
              , doc_elt = doc.documentElement
              , first_elt;

            doc_elt.innerHTML = markup;
            first_elt = doc_elt.firstElementChild;

            if (doc_elt.childElementCount === 1
                && first_elt.localName.toLowerCase() === "html") {  
                doc.replaceChild(first_elt, doc_elt);  
            }  

            return doc;  
        } else {  
            return real_parseFromString.apply(this, arguments);  
        }  
    };  
}(DOMParser));

    var add_in_progress = 0;
    $(document).on('click', '#create-new-product-url', function(e){
        e.preventDefault();
        if (add_in_progress) {
            return;
        }
        add_in_progress = 1;
        user_privacy = 1;
        var urlp = document.getElementById("url-product");
        prd_url = urlp.value;
        $('#load-url-img').removeClass('hidden');
        var gv = new giftvise();
        gv.begin();
        // if($("#add-url-of-item-form").valid()){
            $.ajax({
                type: "POST",
                url: '/products/addproducturl',
                data: $("#add-url-of-item-form").serialize(), // serializes the form's elements.
                success: function(data)
                {
                    if(data.fail == true) {
                         alert("Unable to grab the product from the given URL, please add the product manually");
                         //$modal.modal('hide');
                         return;
                    }
                    $('#load-url-img').addClass('hidden');
		    //$('.create-new-product-url-cancel').trigger('click');
                    $('.url-product-details').trigger('click');
                    var parser=new DOMParser();
                    gvbm_doc = parser.parseFromString(data, "text/html");
                    gv.parseItem(gvbm_doc, prd_url);
                }
            });
       // }
        add_in_progress = 0;
    });


$(document).on('click', '#create-new-product-url-add', function(e){
     e.preventDefault();
     var imageurl = $('.bookmarklet-main-img').attr('src');
     var namep = document.getElementById("url-product-name");;
     var name = namep.value;
     var pricep = document.getElementById("url-product-price");
     var price = pricep.value;
     var descp = document.getElementById("product-details-overview");
     var description = descp.value;
     var tagsp = document.getElementById("url-product-tags");
     var txt_tags = tagsp.value;
     var producturl = $('#url-product').val();
     var privacy = user_privacy
     var currency = $('.add-item-url .price-currency').attr('curr_type');
     var hostparts = producturl.split(".");
     var hostname = '';
     if(hostparts.length >= 2) {
          var tmp = hostparts[2].split('/');
          hostname = hostparts[1]+'.'+tmp[0];
     }
     var url_tag = $('.add-item-url .price-currency').attr('url_tag');
     
     var fdata = 'imageurl='+imageurl+'&name='+name+'&price='+price+'&description='+description+'&txt_tags='+txt_tags+'&producturl='+producturl+'&privacy='+privacy;
     fdata+= '&currency='+currency+'&added_from='+hostname+'&url_tag='+url_tag;

    // if($("#add-item-url-form").valid()){
         $.ajax({
             type: "POST",
             url: '/products/addproduct',
             data: fdata,
             success: function(data)
             {
                 if (data.success){
		     $('.create-new-product-url-add-close').trigger('click');
                     $modal.modal('hide');
                     var $page='home';
                     var $next_start= 0;
                     var $msg = data.msg;
                     $.ajax({
                         cache:false,
                         url: '/Newproducts/productsgrid?ajax=1&next_start='+$next_start+'&items_number=1&prepend=true&page='+$page,
                         success: function(data){
                             updateFilterByTagsList();
                          $container.prepend( $(data))
                                 .isotope( 'reloadItems' ).isotope({ sortBy: 'original-order' });

                             $container.imagesLoaded(function() {
                                 $container.isotope('reLayout');
                             });
	                     initFilter();
                         }
                     });
                 } else {
                    alert(data.msg);
                 }
             }
         });
//     }
    });
   */
})
