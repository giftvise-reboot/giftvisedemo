function validateSignUpForm(){
    $('#sign-up-form').validate({
        ignore:[],
        rules:{
            email_id:{
                required: true,
                email:true
            },
            password:{
                required: true
            }
        }
    });
}
$(document).ready(function() {

    $(document).on('submit', '#sign-up-form', function(e) {
        e.preventDefault();
        validateSignUpForm();
        if($("#sign-up-form").valid()){
            $.ajax({
                url:"/login/signup",
                type:"POST",
                data: $('#sign-up-form').serialize(),
                success:function(result){
                    if (result.success){
                        window.location.replace(document.location.origin + "/login/registersteptwo?id=" + result.id)
                    }else {
                        if (result.pwdErr) {
                            $('[name="password"]').after('<label for="email" class="error">' + 'PASSWORD MUST CONTAIN AT LEAST 6 CHARACTERS AND AT LEAST ONE DIGIT' + '</label>');
                        } else {
                            if (result.emailExist) {
                                $('[name="email_id"]').after('<label for="email" class="error">' + result.msg + '</label>');
                            } else {
                                $('[name="password"]').after('<label for="email" class="error">' + result.msg + '</label>');
                            }
                        }
                    }
                }
            })
        } 
    });

});