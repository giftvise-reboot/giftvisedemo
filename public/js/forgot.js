function validateForgotProcessForm(){
    $('#forgot-process-form').validate({
        ignore:[],
        rules:{
            email:{
                required: true,
                email:true
            }
        }
    });
}
$(document).ready(function() {

    $(document).on('submit', '#forgot-process-form', function(e) {
        e.preventDefault();
        validateForgotProcessForm();
        if($("#forgot-process-form").valid()){
            $.ajax({
                url:"/login/forgotprocess",
                type:"POST",
                data: $('#forgot-process-form').serialize(),
                success:function(result){
                    if (result.success){
                        $('.result-text').html(result.msg);
                    } else {
                        $('[name="email"]').after('<label for="email" class="error">' + result.msg + '</label>');
                    }
                }
            })
        }
    });

});
