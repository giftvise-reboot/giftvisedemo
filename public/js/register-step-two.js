function validateSignUpStepTwoForm(){
    $('#sign-up-step-two-form').validate({
        ignore:[],
        rules:{
            name:{
                required: true,
            },
            dob:{
                required: true
            },
            gender:{
                required: true
            }
        },
        messages: {},
        errorPlacement: function(error, element) {
            if (element.attr("name") == "gender") {
                error.insertAfter(".gender");
            } else {
                error.insertAfter(element);
            }
        }
    });
}
$(document).ready(function() {

    $(document).on('submit', '#sign-up-step-two-form', function(e) {
        e.preventDefault();
        validateSignUpStepTwoForm();
        if($("#sign-up-step-two-form").valid()){
            $.ajax({
                url:"/login/signupsteptwo",
                type:"POST",
                data: $('#sign-up-step-two-form').serialize(),
                success:function(result){
                    if (result.success){
                        window.location.replace(document.location.origin + "/login/registerstepthree?id=" + result.id)
                    }else {
                        $('[name="name"]').after('<label class="error">' + result.msg + '</label>');
                    }
                }
            })
        } else {
            if ($('#gender-error').length > 0) {
                $('#gender-error').css('float', 'right').css('margin-right', '-35px');
            }             
            if ($('#dob-error').length > 0) {
                $('.gender').css('margin-top', "-55px");
            }
            if (($('#gender-error').length > 0) && ($('#dob-error').length > 0)) {
                $('#gender-error').css('margin', '-29px -35px 0 0');
            }
            if (($('#dob-error').length > 0) || ($('#name-error').length > 0)) {
                $('#sign-up-step-two-submit').css('margin-top', '-10px');
            }
        }

    });
});