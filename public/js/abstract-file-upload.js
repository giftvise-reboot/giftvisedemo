/**
 * File Upload Functionality
 *
 * @param {string} selector - It can be the form id or the input of type file.
 * @param {object} options - Options for image class, dimensions, redirect to saving an item if necessary
 *        Example:
 *          options = {
 *              image: {
 *                  class: 'template-contactform-file',
 *                  width: 620,
 *                  height: 462,
 *                  thumbwidth: 270,
 *                  default: '#manual-image-default',
 *                  main: 'manual-image-main'
 *              },
 *              redirect: {
 *                  url: '/settings/updateaccount'
 *              }
 *          };
 */
function upload (selector, options) {
    var uri = '?class=' + options.image.class + '&width=' + options.image.width + '&height=' + options.image.height +'&thumbwidth=' + options.image.thumbwidth;
    var progressId = selector + ' ' + '.progress';
    var progressImage = $(progressId + ' ' + 'img');
    var progressBar = $(progressId + ' ' + '.bar');
    var prevImgId = $(selector + ' ' + '.image-url');

    var mainImage, defaultImage;
    if (options.image.default) {
        defaultImage = $(selector + ' ' + options.image.default);
    }
    if (options.image.main) {
        mainImage = $(selector + ' ' + options.image.main);
    } else {
        mainImage = $(selector + ' ' + '.file-upload-main-image');
    }
    $(selector).fileupload({
        dataType: 'json',
        url: '/fileupload/fileupload' + uri,
        add: function (e, data) {
            mainImage.hide();
            if (defaultImage) {
                defaultImage.hide();
            }
            $(progressId).show();
            progressImage.show();
            data.submit();
        },
        progressall: function (e, data) {
            mainImage.hide();
            if (defaultImage) {
                defaultImage.hide();
            }
            var progress = parseInt(data.loaded / data.total * 100, 10);
            progressBar.html( parseInt(data.loaded/1024, 10) + ' of ' +  parseInt(data.total/1024, 10) + 'KB' +
                ' Loaded' +' <br/>' + progress + '% Complete');
        },
        done: function (e, data) {
            $(progressId).hide();
            progressImage.hide();
            mainImage.show();
            if (data.result.success){
                mainImage.attr('src','/tempimg/' + data.result.file).css('max-width', '100%');
                //delete previous image
                var prevImg = prevImgId.attr('value');
                if (prevImg){
                    $.ajax({url:"/fileupload/deletetempimages?image="+prevImgId+"",success:function(){}});
                }
                //set image on input hidden
                prevImgId.attr('value', data.result.file);
                prevImgId.attr('src', '/tempimg/' + data.result.file);

                if (options.redirect) {
                    var item = 'profile_pic='+document.location.origin + mainImage.attr('src');
                    saveItem(options.redirect.url, item);
                }
            }
            else {
                var isFileSizeLarge = false;
                
                if (defaultImage) {
                    defaultImage.show();
                }

                for (var i in data.originalFiles) {
                    // if filesize is > 4MB
                    if (data.originalFiles[i].size > 4194304) {
                        isFileSizeLarge = true;
                    }
                }
                if (isFileSizeLarge) {
                    alert('The image you are trying to upload is too big. Please try a smaller image (limit is 4 MB)');
                } else {
                    alert('Could not complete the upload. Please try again later.');
                }
            }
        }
    });
}

/**
 *
 * @param {string} url - The url where to send the post
 * @param {string} item - Serialized data to be send to the url
 */
function saveItem(url, item) {
    $.ajax({
        type: "POST",
        url: url,
        data: item,
        success: function(data)
        {
            if (data.success) {
                alert('Updated successfully!');
            } else {
                alert('The profile image was not uploaded. Please contact the site administrator');
            }
        }
    });
}


