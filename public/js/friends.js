$(document).ready(function() {
function validateSaveGroupForm(){
    $("#save-group-form").validate({
        ignore: [],
        rules:{
            group_name:{
                required:true
            },
            description: {
                required:true
            }
        }
    });
}
function validateSearchFriendsForm(){
    $("#search-friends").validate({
        ignore: [],
        rules:{
            search:{
                required:true
            }
        }
    });
}
function validateInviteFriendForm() {
    $('#invite-friend-form').validate({
        ignore: [],
        rules: {
            email_id: {
                required: true,
                email: true
            }
        }
    });
}
function init() {
    $('#search-friends input').autocomplete().autocomplete('disable');
    if (window.location.pathname === '/friends') {
        $('#portfolio').isotope({filter: $('.activeFilter a').attr('data-filter')});
    }
    initFilter();
    initMagnificPopup();
}
function initFilter() {
    var selector = $('#portfolio-filter .activeFilter a').attr('data-filter');
    
    toggleSearchAddToGroupFriends(selector, $('#portfolio-filter .activeFilter a').attr('group-id'));
    toggleDeleteFriend(selector);
    
    $(document).on('click', '#portfolio-filter a', function(e) {
        removePreviousSearch();
        showFriends();
        $('#portfolio-filter li').removeClass('activeFilter');
        $(this).parent('li').addClass('activeFilter');
        var selector = $(this).attr('data-filter');
        $('#portfolio').isotope({filter: selector});
        var $groupInfo = $('#group-info');
        if (selector == '*' ) {
            if (!$groupInfo.hasClass('hidden')){
                $groupInfo.addClass('hidden')
            }
            $('[name=group]').val('');
            $('#search-friends input').val('').attr('placeholder', 'Search for a friend (type & hit enter..)').autocomplete('disable');
            $.ajax({
                url:"/friends/setgroupdata?id=0"
            });
        } else {
            if ($groupInfo.hasClass('hidden')){
                $groupInfo.removeClass('hidden')
            }
            $('#group-info .group-description').html($(this).attr('group-description'));
            $('#group-info .group-code').html($(this).attr('group-code'));
            $('[name=group]').val($(this).attr('group-code'));
        }
        toggleSearchAddToGroupFriends(selector, $(this).attr('group-id'));
        toggleDeleteFriend(selector);
        return false;
    });
}
function addFriend(friend, type) {
    var friendTemplate = $('.friendTemplate').clone();
    $('#portfolio').append(friendTemplate);
    var template = '#portfolio .friendTemplate';
    $(template).attr('id', 'friend-' + friend.id);
    $(template + ' .portfolio-image a').attr('src', friend.link);
    $(template + ' .portfolio-image a img').attr('src', friend.profile_pic);
    $(template + ' .portfolio-overlay a').css('margin-left','-25px');
    $(template + ' .portfolio-overlay .left-icon').attr('href', friend.profile_pic).attr('friend-id', friend.id);
    $(template + ' .portfolio-overlay .right-icon').attr('href', friend.link);
    $(template + ' .portfolio-desc h3 a').attr('href', friend.link).html(friend.name);
    $(template + ' .portfolio-desc .status').addClass('status-' + friend.id).removeClass('status');
    $(template).removeClass('friendTemplate hidden').addClass(type);
}
function hideFriends() {
    $('#portfolio').isotope({ filter: ':not(.friend)' });
}
function showFriends() {
    $('#portfolio').isotope({ filter: '.friend' });
}
function removePreviousSearch() {
    $('#portfolio .search').remove();
}
//return -1 if not found
function checkAlreadyInFriendList(friendId) {
    var friendIds = new Array();
    $('#portfolio .friend').each(function() {
        var idAttr = $(this).attr('id').split('-');
        friendIds.push(idAttr[1]);
    });
    return $.inArray(friendId, friendIds);
}
//$(document).ready(function() {
    init();
    $(document).on('submit', '#save-group-form', function(e){
        e.preventDefault();
        validateSaveGroupForm();
        if($('#save-group-form').valid()){
            $.ajax({
                type: "POST",
                url: '/friends/addgroup',
                data: $("#save-group-form").serialize(), // serializes the form's elements.
                success: function(data)
                {
                    // show response from the php script.
                    $.magnificPopup.close();
                    if (data.success){
                        var newFilter = $('#portfolio-filter').children().eq(0).clone();
                        $('#portfolio-filter').append(newFilter);
                        newFilter = $('#portfolio-filter').children().eq($('#portfolio-filter').children().length - 1);
                        var groupName = data.name.trim();
                        newFilter.children().eq(0).attr('data-filter', '.' + groupName).attr('group-code', data.code).attr('group-description', data.description).attr('group-id', data.id).html(groupName);
                        $('#portfolio-filter').children().eq(0).removeClass('activeFilter');
                        var $groupInfo = $('#group-info');
                        if ($groupInfo.hasClass('hidden')){
                            $groupInfo.removeClass('hidden')
                        }
                        $('#group-info .group-description').html(data.description);
                        $('#group-info .group-code').html(data.code);
                        newFilter.children().eq(0).trigger('click');
                        $('#portfolio').isotope({filter:  '.' + groupName});
                    } else {
                        alert(data.msg);
                    }
                }
            });
        }
    });
    $(document).on('submit', '#search-friends', function (e) {
        e.preventDefault();
        if ($('#search-friends input').attr('placeholder') != 'Add a friend') {
            validateSearchFriendsForm();
            if($('#search-friends').valid()){
                $.ajax({
                    type: "POST",
                    url: '/search/friends',
                    data: $("#search-friends").serialize(), // serializes the form's elements.
                    success: function(data) {
                        hideFriends();
                        removePreviousSearch();
                        data.friends.forEach(function(friend){
                            addFriend(friend, 'search');
                        });
                    }
                });
            }
        }
    });
    $(document).on('click', '.icon-line-plus', function (e) {
        e.preventDefault();
        var button = $(this);
        var socialId = button.parent().attr('social-id');
        if (socialId == undefined) {
            var friendId = button.parent().attr('friend-id');
            if ($('#friend-' + friendId).hasClass('search')) {
                $('#friend-' + friendId).addClass('hidden').removeClass('search pf-illustrations').addClass('pf-media pf-icons friend');
                $('#portfolio').isotope()
                    .append($('#friend-' + friendId))
                    .isotope('appended', $('#friend-' + friendId))
                    .isotope('layout')
                    .isotope({filter: 'friend'});
                $('#friend-' + friendId).removeClass('hidden')
            }
            $.ajax({
                url:"/friends/followfriend?friend_id="+friendId,
                success:function(result){
                    removePreviousSearch();
                    $('#portfolio').isotope({filter: "*"});
                    button.removeClass('icon-line-plus').addClass('icon-line-minus open-unfollow-popup').attr('friend-id', friendId);
                    button.parent().attr('data-mfp-src', '#unfollow-friend').attr('data-lightbox','inline');
                }
            });
        } else {
            $.ajax({
                url: document.location.origin + "/friends/sendtwitterinvitation?id=" + socialId,
                success: function (result) {
                    if (result.success) {
                        button.addClass('icon-ok').removeClass('icon-line-plus');
                    }
                }
            });
        }
    });
    $(document).on('click', '.icon-line-minus', function (e) {
        if ($("#portfolio-filter .activeFilter a").attr('data-filter') == '*') { // unfollow
            $('#unfollow-yes').attr('friend-id', $(this).attr('friend-id'));
        } else { //remove from group
            $('#delete-friend-group-yes').attr('friend-id', $(this).attr('friend-id'));
        }
    });
    $(document).on('click', '#unfollow-yes', function (e) {
        var friendId = $('#unfollow-yes').attr('friend-id');
        $.ajax({url:"/friends/deletefriend?friend_id="+friendId,
            success:function(){
                $.magnificPopup.close();
                $('#portfolio').isotope('remove', $('#friend-' + friendId));
                $('#portfolio').isotope('layout');
            }
        });
    });    
    $(document).on('click', '#delete-friend-group-yes', function (e) {
        var friendId = $('#delete-friend-group-yes').attr('friend-id');
        var groupBtn = $("#portfolio-filter .activeFilter a");
        var groupId = groupBtn.attr('group-id');
        $.ajax({url:"friends/deletefriendfromgroup?friend_id=" + friendId + "&group_id=" + groupId + "&return_null=false",
            success:function(){
                $.magnificPopup.close();
                $('#friend-'+friendId).removeClass(groupBtn.attr('data-filter').substring(1));
                var selector = groupBtn.attr('data-filter');
                $('#portfolio').isotope({filter: selector});
                removeGroupFromItem($('.status-'+friendId+' .group-'+groupId));
            }
        });
    });
    //invite friend by mail
    $(document).on('click', '#invite-friend', function(e) {
        e.preventDefault();
        validateInviteFriendForm();
        if ($('#invite-friend-form').valid()){
            $.ajax({
                url:"friends/sendinvitation",
                data: $("#invite-friend-form").serialize(),
                success:function(data){
                    $.magnificPopup.close();
                    if (data.friendExist) {
                        removePreviousSearch();
                        showFriends();
                        var message = "You already have him/her on your friend list!";
                        if (checkAlreadyInFriendList(data.friend.id) < 0) {
                            addFriend(data.friend, 'friend');
                            message = "Invitation sent and friend added to your list!";
                        }
                        $('#invitation-result-text').html(message);
                    }
                    $('#open-invite-friends-results').magnificPopup('open');
                }
            });
        }
    });
    //invite gmail contact friend
    $(document).on('submit', '.invite-friend-form', function(e) {
        e.preventDefault();
        $.ajax({
            url:"friends/sendinvitation",
            data: $(this).serialize(),
            success:function(data){
                if (data.friendExist) {
                    var message = "You already have him/her on your friend list!";
                    if (checkAlreadyInFriendList(data.friend.id) < 0) {
                        addFriend(data.friend, 'friend');
                        message = "Invitation sent and friend added to your list!";
                    }
                    $('#invitation-result-text').html(message);
                }
                $('#open-invite-friends-results').magnificPopup('open');
            }
        });
    });
    $(document).on('click', '#invite-friends-result button.button', function() {
        $('#invite-friends-result').magnificPopup('close');
    });
    $(document).on('click', '.cancel-action', function (e) {
        $.magnificPopup.close();
    });
    $(document).on('click', '[href=#group-delete]', function (e) {
        if ($("#portfolio-filter .activeFilter a").attr('data-filter') == '*') {
            $.magnificPopup.close();
        }
    });
    $(document).on('click', '#delete-group-btn', function (e) {
        if ($("#portfolio-filter .activeFilter a").attr('data-filter') != '*') {
            var groupId = $("#portfolio-filter .activeFilter a").attr('group-id');
            $.ajax({
                url:"/friends/deletegroup?grpid="+groupId,
                success:function(data){
                    $.magnificPopup.close();
                    if (data.fail){
                        alert('Could not delete the group. Please try again later.');
                    } else {
                        $("#portfolio-filter .activeFilter").remove();
                        //delete group from friend
                        removeGroupFromItem($('.portfolio-item .group-' + groupId));
                        //show all friends
                        $("#portfolio-filter").children().eq(0).children().trigger('click');
                    }
                }
            });
        }
    });
    $('#search-friends input').autocomplete({
        source: document.location.origin +"/friends/autocompletegetallfriends",
        minLength: 0,
        focus: function(event, ui) {
            $('#search-friends input').val(ui.item.value);
        },
        select: function( event, ui ) {
            //add user to group too
            var friendId = ui.item.label;
            var groupBtn = $("#portfolio-filter .activeFilter a");
            $.ajax({
                url: '/friends/addfriendtogroup?friend_id='+friendId+'&group_id='+groupBtn.attr('group-id') +'&return_null=false',
                success: function(data){
                    appendFriendToGroup(data, friendId, groupBtn.attr('group-id'), groupBtn.attr('data-filter').substring(1));
                }
            });
        }
    }).data( "ui-autocomplete" )._renderItem = function( ul, item ) {
        return $( "<li>" )
            .append( "<a>" + item.value + "</a>" )
            .appendTo( ul );
    };

    //invite friends from facebook
    $(document).on('click', '#invite-fb', function(e){
        e.preventDefault();
        googleAuth();
    });

    $(document).on('keyup', '#search-friends input', function (e) {
        var activeFiler = $('#portfolio-filter .activeFilter a');
        if ((e.keyCode == 13) && (activeFiler.attr('data-filter') != '*')) {
            addFriendToGroup(activeFiler.attr('group-id'), $(this).val());
        }
    });
    $(document).on('submit', '#search-friends', function (e) {
        e.preventDefault();
        var activeFiler = $('#portfolio-filter .activeFilter a');
        if (activeFiler.attr('data-filter') != '*') {
            addFriendToGroup(activeFiler.attr('group-id'), $('#search-friends input').val());
        }
    });
});

function toggleSearchAddToGroupFriends(selector, groupId) {
    if (selector == '*' ) {
        $('#search-friends input').val('').attr('placeholder', 'Search for a friend (type & hit enter..)').autocomplete('disable');
        //set group id 0 in session
        $.ajax({
            url:"/friends/setgroupdata?id=0"
        });
    } else {
        $('#search-friends input').val('').attr('placeholder', 'Add a friend').autocomplete('enable');
        //set group id in session
        $.ajax({
            url:"/friends/setgroupdata?id="+groupId
        });
    }
}

function toggleDeleteFriend(selector) {
    if (selector == '*' ) {
        $('.icon-line-minus').parent().attr('data-mfp-src','#unfollow-friend');
    } else {
        $('.icon-line-minus').parent().attr('data-mfp-src','#delete-friend-group-modal');
    }
}
function addFriendToGroup(groupId, friendName) {
    $.ajax({
        url: document.location.origin + "/friends/addnamefriendtogroup?friend_name=" + friendName + "&group_id=" + groupId,
        success: function (result) {
            appendFriendToGroup(result, result.friendId, groupId, $('[group-id="' + groupId + '"]').attr('data-filter').substring(1));
        }
    });
}
function appendFriendToGroup(data, friendId, groupId, groupName) {
    if(data.fail){
        alert('Unable to add this friend to the group. Please try again later.')
    } else {
        if (!$('#friend-' + friendId).hasClass(groupName)){
            $('#friend-' + friendId).addClass(groupName);
            $('#portfolio').isotope({filter: '.' + groupName});
            $('.icon-line-minus').parent().attr('data-mfp-src','#delete-friend-group-modal');
            var comma = ($('.status-' + friendId).children().length == 0) ? '': ', ';
            $('.status-' + friendId).append(comma + '<span class="group-' + groupId + '">'+groupName+'</span>');
        }
    }
}
function removeGroupFromItem(group) {
    var status = group.parent();
    group.remove();
    if (status.children().length > 0) {
        status.html(status.html().trim().split(',').clean('').join(', '));
    }
}
function initMagnificPopup() {
    $('#open-invite-friends-results').magnificPopup({
        items: {
            src: "#invite-friends-result",
            type: 'inline'
        }
    });
}
Array.prototype.clean = function(deleteValue) {
    for (var i = 0; i < this.length; i++) {
        if (this[i].trim() == deleteValue) {
            this.splice(i, 1);
            i--;
        }
    }
    return this;
};