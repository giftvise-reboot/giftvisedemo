jQuery(document).ready(function($) {

if ($('body').hasClass('device-sm') || $('body').hasClass('device-sm') || $('body').hasClass('device-xs') || $('body').hasClass('device-xxs')) {
    $('#add-from-bookmarklet').hide();
}

function validateSignInForm(){
    $('#login-form').validate({
        ignore:[],
        rules:{
            email:{
                required: true,
                email:true
            },
            password:{
                required: true
            }
        }
    });
}
//accept/decline friend follow
function changsStatus(userid, friend_id, placeid, status) {
    var notifId = placeid.slice(2);
    if (status == '2'){
        //decline
        res = confirm('Are you sure you want to ignore this friend?');
        if(res){
            $.ajax({url:"/friends/changestatus?user_id="+userid+"&friend_id="+friend_id+"&status="+status+"&notif_id="+notifId,
                success:function(result){
                    if(result.success){
                        $("#"+placeid+" .change-status").remove();
                        $("#"+placeid+" .drp-notif-content").append('<button type="button" class="delete-notif pull-right top-cart-item-quantity delete-notification-btn" notif-id="' + notifId +'" place="' + placeid + '" user-id="' + userid + '">x</button>');
                        $("#"+placeid+" small").html('ignored');
                    }
                }
            });
        }
    } else {
        //accept
        $.ajax({url:"/friends/changestatus?user_id="+userid+"&friend_id="+friend_id+"&status="+status+"&notif_id="+notifId,
            success:function(result){
                if(result.success){
                    $("#"+placeid+" .change-status").remove();
                    $("#"+placeid+" .drp-notif-content").append('<button type="button" class="delete-notif pull-right top-cart-item-quantity delete-notification-btn" notif-id="' + notifId +'" place="' + placeid + '" user-id="' + userid + '">x</button>');
                    $("#"+placeid+" small").html('allowed');
                    //var place = "'ub_" + userid + "'";
                    //var str = "'Are you sure you want to Block this friend?'";
                    //var statusVal = "'2'";
                    //$(".ub_"+userid).html('<a href="javascript:void(0);" onclick="blockfriend(' + userid + ', ' + friend_id + ',' + str + ',' + place +',' + statusVal + ')">Block</a>');
                }
            }
        });
    }
}
//$(document).ready(function() {

    $(document).on('submit', '#login-form', function(e) {
        e.preventDefault();
        validateSignInForm();
        if($("#login-form").valid()){
            $.ajax({
                url:"/login/login",
                type:"POST",
                data: $('#login-form').serialize(),
                success:function(result){
                    if (result.success){
                        window.location.replace(document.location.origin + "/login/home")
                    }else {
                        $('[name="password"]').after('<label for="email" class="error">' + result.msg + '</label>');
                        changeFormDimension();
                    }
                }
            })
        } else {
            changeFormDimension();
        }
    });
    
    //add from wishlist
    $(document).on('click', '.wishlist-add', function(e){
          e.preventDefault();
          var $btn = $(this);
          $.ajax({
                url:"/products/addwishlist?item_id="+$btn.attr('item-id')+"&user_id="+$btn.attr('user-id'),
                success:function(result){
                    $btn.children().addClass('icon-ok').removeClass('icon-line-plus');
                    $btn.removeClass('wishlist-add');
                     //alert("Item added to your wishlist successfully");
                }
            });
      });

    $(document).on('click', '#clear-notifications', function() {
        var activityIds = [];
        var notifications = $('#top-cart .top-cart-items .top-cart-item');
        notifications.each(function() {
            var idAttr =  $(this).attr('id').split('_');
            activityIds.push(idAttr[1]);
        });
        $.ajax({
            url: document.location.origin + "/notifications/deleteactivities?activity_ids="+activityIds.join(','),
            success:function(result){
                if (result.success) {
                    notifications.each(function() {
                        $(this).remove();
                    });
                    $('#clear-notifications').attr('disabled', 'disabled');
                    $('#top-cart .top-cart-items').html('<div class="drp-notif-content"><div class="notification-msg">No notifications at this time</div></div>');
                }
            }
        });
    });

    /*
     * Change forms dimension if there is an error
     */
    function changeFormDimension() {
        $('.panel').addClass('error-panel');
    }

    /*
     * NOTIFICATION Section
     */

    $(document).on('click', '.delete-notif', function(e){
        e.preventDefault();
        e.stopPropagation();
        deleteAct($(this).attr('notif-id'),$(this).attr('place'),$(this).attr('user-id'));
    });

    function deleteAct(activity_id, place_id, user_id){
        $.ajax({
            url: document.location.origin + "/notifications/deleteactivity?activity_id="+activity_id+"&user_id="+user_id,
            success:function(result){
                removeNotification(place_id);
            }
        });
    };

    function removeNotification(place_id){
        if ($('#'+place_id).siblings().length == 0){
            $('#'+place_id).parent().remove();
        } else {
            $('#'+place_id).hide();
        }
    }

    /*
     * Reset new notification counter
     */
    $("#top-cart-trigger").click(function(){
        var $newNotificatons = $('.new-notifications');

        if($newNotificatons.length != 0 && $newNotificatons.html().trim().length != 0){
            $.ajax({
                url:"/notifications/resetcounter",
                success:function(result){
                    $newNotificatons.html('');
                    $('.new-notifications').css('display', 'none');
                }
            });
        }
    });
    
    /*
     * Accept/decline user follow
     */
    $(document).on('click', '.change-status-notif', function(e){
        e.preventDefault();
        e.stopPropagation();
        changsStatus($(this).attr('user-id'),$(this).attr('friend-id'), $(this).attr('place'), $(this).attr('status'));
    });
    
    //initDateTimePicker();



    var gvbm_doc; 
    var prd_url; 


(function(DOMParser) {  
    "use strict";  
    var DOMParser_proto = DOMParser.prototype  
      , real_parseFromString = DOMParser_proto.parseFromString;

    // Firefox/Opera/IE throw errors on unsupported types  
    try {  
        // WebKit returns null on unsupported types  
        if ((new DOMParser).parseFromString("", "text/html")) {  
            // text/html parsing is natively supported  
            return;  
        }  
    } catch (ex) {}  

    DOMParser_proto.parseFromString = function(markup, type) {  
        if (/^\s*text\/html\s*(?:;|$)/i.test(type)) {  
            var doc = document.implementation.createHTMLDocument("")
              , doc_elt = doc.documentElement
              , first_elt;

            doc_elt.innerHTML = markup;
            first_elt = doc_elt.firstElementChild;

            if (doc_elt.childElementCount === 1
                && first_elt.localName.toLowerCase() === "html") {  
                doc.replaceChild(first_elt, doc_elt);  
            }  

            return doc;  
        } else {  
            return real_parseFromString.apply(this, arguments);  
        }  
    };  
}(DOMParser));

    var add_in_progress = 0;
    $(document).on('click', '#create-new-product-url', function(e){
        e.preventDefault();
        if (add_in_progress) {
            return;
        }
        add_in_progress = 1;
        user_privacy = 1;
        var urlp = document.getElementById("url-product");
        prd_url = urlp.value;
        $('#load-url-img').removeClass('hidden');
        var gv = new giftvise();
        gv.begin();
        // if($("#add-url-of-item-form").valid()){
            $.ajax({
                type: "POST",
                url: '/products/addproducturl',
                data: $("#add-url-of-item-form").serialize(), // serializes the form's elements.
                success: function(data)
                {
                    if(data.fail == true) {
                         alert("Unable to grab the product from the given URL, please add the product manually");
                         //$modal.modal('hide');
                         return;
                    }
                    $('#load-url-img').addClass('hidden');
		    //$('.create-new-product-url-cancel').trigger('click');
                    $('.url-product-details').trigger('click');
		    /*
                    if ($('.url-product-details').hasClass('hidden')) {
                        $('.url-product-details').removeClass('hidden');
                    }
		    */
                    var parser=new DOMParser();
                    gvbm_doc = parser.parseFromString(data, "text/html");
                    gv.parseItem(gvbm_doc, prd_url);
                }
            });
       // }
        add_in_progress = 0;
    });


    $(document).on('click', '#create-new-product-url-add', function(e){
         e.preventDefault();

         var imageurl = $('.bookmarklet-main-img').attr('src'); 
         var namep = document.getElementById("url-product-name");
         var name = namep.value;
         var pricep = document.getElementById("url-product-price");
         var price = pricep.value;
         var descp = document.getElementById("product-details-overview");
         var description = descp.value;
         var tagsp = document.getElementById("url-product-tags");
         var txt_tags = tagsp.value;
         var producturl = $('#url-product').val();
         var privacy;

         var sel = document.getElementById('add-url-item-privacy-option');
         if (sel.options[sel.selectedIndex].value == 'YES') {
               privacy  = 0;
         } else {
               privacy = 1;
         }

         var currency = $('.add-item-url .price-currency').attr('curr_type');
         var hostparts = producturl.split(".");
         var hostname = '';
         if(hostparts.length >= 2) {
              var tmp = hostparts[2].split('/');
              hostname = hostparts[1]+'.'+tmp[0];
         }
         var url_tag = $('.add-item-url .price-currency').attr('url_tag');

         var fdata = 'imageurl='+imageurl+'&name='+name+'&price='+price+'&description='+description+'&txt_tags='+txt_tags+'&producturl='+producturl+'&privacy='+privacy;
         fdata+= '&currency='+currency+'&added_from='+hostname+'&url_tag='+url_tag;

         saveProduct(fdata);

    });

    $(document).on('click', '#create-new-product-manual-add', function(e){
        e.preventDefault();

        var imageurl = document.location.origin + $('#manual-image-url').attr('src');
        var name = document.getElementById("manual-product-name").value;
        var price = document.getElementById("manual-product-price").value;
        var description = document.getElementById("manual-details-overview").value;
        var txt_tags = document.getElementById("manual-product-tags").value;
        var privacy;

        var sel = document.getElementById('manual-privacy-option');
        if (sel.options[sel.selectedIndex].value == 'YES') {
            privacy  = 0;
        } else {
            privacy = 1;
        }
        var data = 'imageurl='+imageurl+'&name='+name+'&price='+price+'&description='+description+'&txt_tags='+txt_tags+'&privacy='+privacy;
        
        saveProduct(data);
    });
    
function initDateTimePicker(){
    //$('.datetimepicker').datetimepicker({
    //    format: "MM-DD-YYYY",
    //    widgetPositioning: {
    //        vertical: 'bottom',
    //        horizontal: 'left'
    //    }
    //});
}


//jQuery(document).ready(function($) {

    var relatedPortfolio = $("#related-portfolio");

    relatedPortfolio.owlCarousel({
        margin: 30,
        nav: false,
        dots:true,
        autoplay: true,
        autoplayHoverPause: true,
        responsive:{
            0:{ items:1 },
            480:{ items:2 },
            768:{ items:3 },
            992:{ items:4 }
        }
    });

    // call global upload functionality from file-upload.js
    $.getScript(document.location.origin + '/js/abstract-file-upload.js', function() {
        var params = {
            image: {
                class: 'template-contactform-file',
                width: 620,
                height: 462,
                thumbwidth: 270,
                default: '#manual-image-default',
                main: '#manual-image-main'
            }
        };
        upload('#addproductmanualModal', params);
    });

    $(document).on('click', '.statusUnfollow', function (e) {
        $('#unfollow-yes').attr('friend-id', $(this).attr('friend-id'));
    });

    $(document).on('click', '.statusFollow', function (e) {
        var button = $(this);
        var friendId = button.attr('friend-id');
        $.magnificPopup.close();
        $.ajax({
            url:"/friends/followfriend?friend_id="+friendId,
            success:function(result){
                button.removeClass('statusFollow')
                    .addClass('statusPending')
                    .text('Pending')
            }
        });
    });

    $(document).on('click', '#unfollow-yes', function (e) {
        var friendId = parseInt($('#unfollow-yes').attr('friend-id'));
        $.ajax({url:"/friends/deletefriend?friend_id=" + friendId,
            success:function(){
                $('.statusUnfollow').removeClass('statusUnfollow')
                    .addClass('statusFollow')
                    .text('Follow')
                    .removeAttr('data-mfp-src')
                    .removeAttr('data-lightbox');
                $.magnificPopup.close();
            }
        });
    });
});

<!-- Portfolio Script  ============================================= -->
/*
jQuery(window).load(function(){

    var $container = $('#portfolio');

    $container.isotope({ transitionDuration: '0.65s' });

    $('#portfolio-filter a').click(function(){
        $('#portfolio-filter li').removeClass('activeFilter');
        $(this).parent('li').addClass('activeFilter');
        var selector = $(this).attr('data-filter');
        $container.isotope({ filter: selector });
        return false;
    });

    $('#portfolio-shuffle').click(function(){
        $container.isotope('updateSortData').isotope({
            sortBy: 'random'
        });
    });

    $(window).resize(function() {
        $container.isotope('layout');
    });

});
*/
function tagsInit() {
    $('.bt-tags').editable({
        inputclass: 'input-large',
        display:function() {
            $(this).html('<a href="#" class="bt-tags-edit"><i class="icon-edit"></i></a>');
        },
        select2: {
            tags: ['html', 'javascript', 'css', 'ajax'],
            tokenSeparators: [",", " "]
        }
    });
}

function saveProduct(formData) {
    $.ajax({
        type: "POST",
        url: '/products/addproduct',
        data: formData,
        success: function(data)
        {
            if (data.success){
                $('.create-new-product-url-add-close').trigger('click');
                window.location.href = "/wishlist/mywishlist";
            } else {
                alert(data.msg);
            }
        }
    });
}
