jQuery(window).load(function(){
    var $container = $('#portfolio');
    var $loadComplete = true;
    function initFilter() {
        $('#portfolio-filter a').click(function () {
            $('#portfolio-filter li').removeClass('activeFilter');
            $(this).parent('li').addClass('activeFilter');
            var selector = $(this).attr('data-filter');
            $container.isotope({filter: selector});
            return false;
        });
    }

    function updateFilterByTagsList() {
        // get item ids
        var itemIds = "('";
        var noElements = true;
        $('.portfolio-image').each(function () {
            itemIds += $(this).attr('id') + "','";
            noElements = false;
        });

        //alert(itemIds);



        var $page = 'feed';
        if (noElements == false) {
            var filters = new Array();
            $('#portfolio-filter li a').each(function () {
                filters.push($(this).attr('data-filter'));
            });
            $.ajax({
                type: "POST",
                url: "/products/getproductstagsasarray?page=" + $page + "&item_ids=" + itemIds.slice(0, -2) + ")",
                success: function (result) {
                    if (!(result.fail)) {
                        var filters = new Array();
                        $('#portfolio-filter li a').each(function () {
                            filters.push($(this).attr('data-filter'));
                        });
                        result.tags.forEach(function(tag) {
                            if ((tag.name !== null) && (tag.name !== '')) {
                                var tagName = '.' + tag.name.trim();
                                if (jQuery.inArray(tagName, filters) === -1) {
                                    var newFilter = $('#portfolio-filter').children().eq(1).clone();
                                    $('#portfolio-filter').append(newFilter);
                                    newFilter = $('#portfolio-filter').children().eq($('#portfolio-filter').children().length - 1);
                                    newFilter.children().eq(0).attr('data-filter', tagName).html(tag.name.trim());
                                }
                            }
                        });
                        $('#portfolio-filter').append(result);
                    }
                }
            });
            //FeedFiltersCheckControls($('.products-grid'));
        }
    }




    function LoadMoreProducts(beg) {

        if ($loadComplete != true) {
            return;
        }
        $loadComplete = false;

        var $next_start = 0;
        $('.portfolio-item').each(function(){
            $next_start = $(this).attr('next_start');
        });
        $next_start= $(".portfolio-image > div").size();

        $page = 'feed&pgid=0';
        $subpage = 'feed';
        $curatetype = '';
        $category = $container.attr('portfolio-type');
        $.ajax({
            cache:false,
            url: '/products/productsgrid?ajax=1&next_start='+ $next_start +'&page='+ $page +'&append=true&subpage='+$subpage+'&curatetype='+$curatetype+'&item_type='+$category,
            success: function(data){
                $container.isotope('insert', $(data), function(){
                    $container.imagesLoaded(function() {
                        $container.isotope('reLayout');
                    });  //alert("Chikke");
                });
                if (window.location.search.substring(1) == "") {
                    updateFilterByTagsList();
                    initFilter();
                }

                $loadComplete = true;
                /*
                 //$('#load-progress').hide();
                 //$('#load-progress img').hide();
                 updateFilterByTagsList();
                 initToggleTileHoverState();
                 initToolTips();
                 timeConvert();
                 loadComplete = true;*/
            }
        });
    }

    $container.isotope({ transitionDuration: '0.65s' });

    initFilter();

    $('#portfolio-shuffle').click(function(){
        $container.isotope('updateSortData').isotope({
            sortBy: 'random'
        });
    });

    $(window).resize(function() {
        $container.isotope('layout');
    });
    $(window).scroll(function(){
        if ((window.innerHeight + window.scrollY) >= ((0.75) * document.body.offsetHeight)) {
            // you're at the bottom of the page
            LoadMoreProducts(false);
        }
    });
});