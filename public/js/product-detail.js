function diff(A, B) {
    return A.filter(function (a) {
        return B.indexOf(a) == -1;
    });
}
function common(A, B) {
    return A.filter(function (a) {
        return B.indexOf(a) != -1;
    });
}
jQuery(document).ready(function ($) {

    var relatedPortfolio = $("#related-portfolio");

    relatedPortfolio.owlCarousel({
        margin: 20,
        nav: false,
        dots: true,
        autoplay: true,
        autoplayHoverPause: true,
        responsive: {
            0: {items: 1},
            480: {items: 2},
            768: {items: 3},
            992: {items: 4}
        }
    });

    

    function timeConvert() {
        $('[time-stamp]').each(function () {
            $(this).html($.format.toBrowserTimeZone($(this).attr('time-stamp'), "MM/dd/yy 'at' h:mm a"));
        });
        $('[time-stamp-ago]').each(function () {
            $(this).html($.format.prettyDate($(this).attr('time-stamp-ago')));
        });
        $('[created-time-stamp]').each(function () {
            $(this).html($.format.toBrowserTimeZone($(this).attr('created-time-stamp'), "MM/dd/yy"));
        });
    }

    timeConvert();
    
    tagsInit();

    var $buy_btn_link = null;

    function gv_Open_product_page() {
        window.open($buy_btn_link);
        $buy_btn_link = null;
    }

    $(document).on('click', '#reserve-gift-yes', function (e) {
        e.preventDefault();
        var $btn = $(this);
        var item_id = $btn.attr('item-id');
        $buy_btn_link = $btn.attr('href-lk');
        var receiver_id = $btn.attr('receiver-id');
        var amount = 0;
        var event_id = 0;
        
        $.ajax({
            url: "/event/pickgiftforevent?item_id=" + item_id + "&event_id=" + event_id + "&amount=" + amount + "&reciver=" + receiver_id,
            success: function (result) {
                $('.add-reserve-msg').html('<span><i class="icon-ok"></i>Reserved on:</span> <span>Today</span></li>');
		$('.add-reserve-msg').show();
                $.magnificPopup.close();
            }
        });

        if ($buy_btn_link) {
            gv_Open_product_page();
        }

    });

    $(document).on('click', '#reserve-gift-no', function (e) {
        e.preventDefault();
        var $btn = $(this);
        $buy_btn_link = $btn.attr('href-lk');
        $.magnificPopup.close();
        if ($buy_btn_link) {
            gv_Open_product_page();
        }

    });

    $(document).on('click', '.buy-btn', function (e) {
        e.preventDefault();
        var $btn = $(this);
        $buy_btn_user_id = $btn.attr('user-id');
        $buy_btn_link = $btn.attr('href');
        //alert($buy_btn_link); alert($buy_btn_user_id);
        var item_id = $btn.attr('item-id');
        var event_id = $btn.attr('event-id');
        var amount = 0; // $btn.attr('item-price');
        receiver = $btn.attr('receiver-id');

        if ($buy_btn_user_id != '') {
            if ($buy_btn_link) {
                setTimeout(function () {
                    gv_Open_product_page()
                }, 1000);
            }
            alert("Don't forget to sign in and reserve this gift, someone else would reserve this gift for your friend otherwise!");
        } else {
            var msg = 'Do you want to reserve this gift for your friend?';
            alert(msg);
            //alert(msg, CONFIRM_MODAL_RESERVE_BUY, item_id, event_id, 0, receiver);
        }
    });

    $(document).on('click', '#keep-private', function (e) {
        e.preventDefault();
        var $id = $(this).attr('item_id');
        var $pelem = $(this).find('#private-icon');
        var $privacy;
        if ($pelem.hasClass('icon-ok')) {
            $privacy = 1;
            $pelem.removeClass('icon-ok');
        } else {
            $privacy = 0;
            $pelem.addClass('icon-ok');
        }
        var $selector = '&public=' + $privacy;
        $.ajax({
            cache: false,
            url: '/products/changeshowto?&item_id=' + $id + $selector + '',
            success: function (data) {
            }
        });

    });

    $(document).on('click', '.wishlist-add-d', function (e) {
        e.preventDefault();
        var $btn = $(this);
        $.ajax({
            url: "/products/addwishlist?item_id=" + $btn.attr('item-id') + "&user_id=" + $btn.attr('user-id'),
            success: function (result) {
                $chk_icon_elem = $btn.find('#check-icon');
                $chk_icon_elem.removeClass('icon-plus-sign');
                $chk_icon_elem.addClass('icon-ok');
                $btn.removeClass('wishlist-add-d');
                $('.tags-edit').removeClass('hidden');
                //alert("Item added to your wishlist successfully");
            }
        });
    });

    $(document).on('click', '.go-back', function (e) {
        //window.history.back();
        $('#button-go-back').trigger();
    });

    $(document).on('click', '.bt-tags-edit', function (e) {
        e.preventDefault();
        $('.bt-tags').editable('show');
    });

    $('.bt-tags').on('save', function(e, params){
        var unchangedTags = common(params.newValue, tagNames);
        var tagsToDelete = diff(tagNames, unchangedTags);
        var tagsToAdd = diff(params.newValue, unchangedTags);
        var product_id = $(this).attr('item-id');
        // delete removed tag(s) if there are any
        if (tagsToDelete.length > 0 ) {
            var tagToDeleteIds = new Array();
            $.each(tagsToDelete, function(index, tag){
                tagToDeleteIds.push($('[tagname=".' + tag + '"]').attr('tag-id'));
            });
            $.ajax({
                url: '/products/deletetags?item_id='+product_id+'&tag_ids='+tagToDeleteIds.join(','),
                success: function(response) {
                    if (response.success) {
                        $.each(tagsToDelete, function(index, tag){
                            $('[tagname=".' + tag + '"]').remove();
                        });
                    } else {
                        alert("Could not update information", null);
                    }
                }
            });
        } 
        //add new tag(s) if there are any
        if (tagsToAdd.length > 0) {
            $.ajax({
                url: '/products/addtags?item_id='+product_id+'&tag_names='+tagsToAdd.join(','),
                success: function(response) {
                    if (!response.fail) {
                        $.each(response.tags, function(index, tag){
                            var newTag = '<a href="' + document.location.origin + '/products/explore?item_type=' + tag.name + '" tagname=".' + tag.name + '" tag-id="' + tag.id + '">' + tag.name + '</a>';
                            $('.tag-list').append(newTag);
                        })
                    } else {
                        alert("Could not update information", null);
                    }
                }
            });
        }
    });
    $('#widget-subscribe-form-email').autocomplete({
        source: document.location.origin + "/friends/autocompletegetallfriends",
        minLength: 0,
        focus: function (event, ui) {
            $('#widget-subscribe-form-email').val(ui.item.value);
            removeRecommendErrors();
        },
        select: function (event, ui) {
            //recommend product to friend
            $.ajax({
                url: document.location.origin + "/products/recommenditem?send_userID=" + ui.item.label + "&item_id=" + $('.wishlist-add-d').attr('item-id'),
                success: function (result) {
                    removeRecommendErrors();
                    $('#widget-subscribe-form-email').val('');
                    alert(result.msg);
                }
            });
        }
    }).data("ui-autocomplete")._renderItem = function (ul, item) {
        return $("<li>")
            .append("<a>" + item.value + "</a>")
            .appendTo(ul);
    };
    $(document).on('keyup', '#widget-subscribe-form-email', function (e) {
        if (e.keyCode == 13) {
            sendRecommedMail(e);
        }
    });

    $(document).on('submit', '#recommend-item-form', function (e) {
        sendRecommedMail(e)
    });
});
function sendRecommedMail(e) {
    e.preventDefault();
    var email = $('#widget-subscribe-form-email').val();
    var itemId = $('.wishlist-add-d').attr('item-id');
    $.ajax({
        url: document.location.origin + "/products/recommenditembymail?email=" + email + "&item_id=" + itemId,
        success: function (result) {
            if (result.success) {
                $('.recommend-form').css('display', 'none');
                alert(result.msg);
            } else {
                $('#recommend-item-form').after('<div class="email-error">' + result.emailError + '</div>');
            }
        }
    });
}
function removeRecommendErrors() {
    if ($('#widget-subscribe-form-email-error').length > 0) {
        $('#widget-subscribe-form-email-error').remove();
    }
}
