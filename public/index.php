<?php
/**
 * Bootstrap file
 */
//ini_set('session.cookie_domain', ".imagineads.com");
session_start();
//Add library and models directories to include path

set_include_path(
	"../library".PATH_SEPARATOR.
	"../application/models".PATH_SEPARATOR.
        "../application/plugins".PATH_SEPARATOR.
	get_include_path()
);

require_once('facebook-sdk-v5/autoload.php');

ini_set('display_errors', 1);

date_default_timezone_set('UTC');
//Setup autoloading through Zend Loader
require_once("Zend/Loader/Autoloader.php");
//echo 'test:'.$_SERVER['DOCUMENT_ROOT'];
$autoloader = Zend_Loader_Autoloader::getInstance();
$autoloader->setFallbackAutoloader(true);

//Load configuration
$server = new Zend_Config_Ini("../ini/server.ini");
$config = new Zend_Config_Ini("../ini/config.ini", $server->type);
Zend_Registry::set("server", $server);
Zend_Registry::set("config", $config);

$stream = @fopen('../newwebapp.log', 'a+');
$writer = new Zend_Log_Writer_Stream($stream);
//$formatter = new Zend_Log_Formatter_Simple(__CLASS__ . ':' . __FUNCTION__ . ':' . __LINE__ . ":%message%" . PHP_EOL);
//$writer->setFormatter($formatter);
$logger = new Zend_Log($writer);
Zend_Registry::set("logger", $logger);

//Publicly accessible pages
$public = array(
	"/^$/",
	"/^\/index$/i",
	"/^\/index\/(.*)$/i",
	"/\/auth$/i",
	"/\/auth\/(.*)$/i",
	"/\/password$/i",
	"/\/forgot$/i",
	"/\/token$/i",
	"/\/email\/$/i",
	"/\/verify$/i"
);

//Error reporting
error_reporting(E_ERROR | E_PARSE);
ini_set("display_errors", $server->displayErrors);
 error_reporting(E_ALL);
// ini_set("display_errors", 1);

//App engine
$app = new PS_AppEngine();

$app->addDomainToModule("default", $config->site->app->rule)
->addLayoutToModule("default",
	array(
		"public" 	=> PS_Util_Array::push($public, "^/"),
		"default"	=> "^/"
	)
)
->execute();

//Cache
$cache = Zend_Cache::factory("Core", "File",
	array("automatic_serialization" => true),
	array("cache_dir" => "../cache")
);

//pagination
Zend_View_Helper_PaginationControl::setDefaultViewPartial('controls.phtml');
//
//DB

$db = new PS_DB($config->database);
$db->setFetchMode(Zend_Db::FETCH_OBJ);
Zend_Db_Table_Abstract::setDefaultAdapter($db->getMasterConnection());
Zend_Db_Table_Abstract::setDefaultMetadataCache($cache);
Zend_Registry::set("db", $db);

if (isset($config->session->{$app->getModule()})){
	//DB based session handler
	Zend_Session::setSaveHandler(
		new session(
			$config
				->session
				->{$app->getModule()}
				->lifetime
		)
	);
}


$_SESSION = array();
$session = new session();
$_SESSION = $session->read(session_id());
//Start layout
$layout = Zend_Layout::startMvc("../application/layouts");
$layout->setLayout($app->getLayout());


//Directories
$dir 			= new stdClass();
$dir->webroot 	= dirname(__FILE__);
$dir->siteroot 	= $dir->webroot."/..";
$dir->email		= $dir->siteroot."/emails";
Zend_Registry::set("dir", $dir);
//Date
date_default_timezone_set("US/Eastern");

//Locale
Zend_Locale::setDefault("en_US");
Zend_Locale::setCache($cache);

//Translation

//Locale set to en_US (no auto-detect)
$translate = new Zend_Translate("tmx", "../application/locale/global.tmx", new Zend_Locale("en_US"));
PS_Form_Fields_Abstract::lookForTranslation();
PS_Listing_Abstract::lookForTranslation();
Zend_Registry::set("translate", $translate);
//Init module
Zend_Controller_Front::getInstance()
->addModuleDirectory("../application/modules")
->registerPlugin(new GiftviseAuth()) /*register giftvise authentication plugin*/
->setDefaultModule($app->getModule());



//HTTP Referrer
PS_Referrer::autoTrack();
$view= new Zend_View();

$view->addHelperPath("ZendX/JQuery/View/Helper", "ZendX_JQuery_View_Helper");
$view->jQuery()->addStylesheet('/public/jquery/ui/1.8.2/custom-min.css')
->setUiLocalPath('/public/jquery/ui/1.8.2/custom-min.js')
->enable()
->uiEnable();

$view->publicurls = $public;

$viewRenderer = new Zend_Controller_Action_Helper_ViewRenderer();

$viewRenderer->setView($view);


Zend_Controller_Action_HelperBroker::addHelper($viewRenderer);


//Mail default transport
Zend_Mail::setDefaultTransport(new Zend_Mail_Transport_Smtp($server->smtphost));
//Dispatch
Zend_Controller_Front::getInstance()
->dispatch();
