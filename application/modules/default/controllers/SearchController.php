<?php
class SearchController extends PS_Controller_Action
{
    public function preDispatch()
    {
    }

    /*
    *  index action
    */
    public function indexAction() {
        $db = Zend_Registry::get("db");
        $config = Zend_Registry::get("config");
        $server = Zend_Registry::get("server");
        $logger = Zend_Registry::get("logger");

        //user
        $user_details = users::getUserDetails($_SESSION['user_id']);
        $this->view->userDetail = $user_details;

        ///categories
        $categories = category::categoryList();
        $this->view->categories = $categories;

        $this->view->vpage = 5;
        $param = $this->getRequest()->getParams();

        $searchKey = trim($param['search']);
        $catVal = $param['item_type'];

        $this->view->search_key = $searchKey;
        $logger->info("Search key = ". $searchKey);

        $orderBy = '';
        if ($param['pricekey'] == 'price') {
            if ($orderBy != '')
                $orderBy .= ', item_price ASC';
            else
                $orderBy = ' item_price ASC';
        }

        if ($param['datekey'] == 'date') {
            if ($orderBy != '')
                $orderBy .= ', uw.added_date ASC';
            else
                $orderBy = 'uw.added_date ASC';
        }

        try {
            if ($searchKey != '') {

                $keych = '%' . $searchKey . '%';
                //$db_searchkey = $db->quote($keych);
                $db_searchkey = $keych;

                $logger->info("DB search Key: ". $keych);
                $products = wishlistitmes::searchResult($_SESSION['user_id'], $db_searchkey, $orderBy, $catVal);
                $logger->info("Products based on search key: ". count($products));

                if ($catVal != '') {
                    $catName = category::categoryName($catVal);
                } else {
                    $catName = 'Category';
                }

                /// search people/users
                $nameArr = explode(" ", $searchKey);

                if (count($nameArr) > 1) {
                    $fname = '%' . $nameArr[0] . '%';
                    $lname = '%' . $nameArr[1] . '%';

                    $fname1 = $db->quote($fname);
                    $lname1 = $db->quote($lname);
                } else {
                    $fname = '%' . $searchKey . '%';
                    $fname1 = $db->quote($fname);
                }

                $logger->info("User ID = ". $_SESSION['user_id']);
                
                $pcnt = 0;
                $skip_ct = 0;
                for ($i = 0; $i < count($products); $i++) {
                    $item = array();
                    if ($products[$i]->image == 'no-image-available') {
                        $skip_ct++;
                        continue;
                    }
                    $totalComments = itemcomments::countComments($products[$i]->id);
                    $userwished = wishlistitmes::countWished($products[$i]->id);

                    /// latest item
                    $timeAdded = wishlistitmes::latestItemHome($products[$i]->id);
                    $addeduser = users::getUserDetails($timeAdded['user_id']);

                    //
                    $itemwhished = wishlistitmes::chkWishedItem($products[$i]->id, $_SESSION['user_id']);
                    if ($itemwhished) {
                        $item['youwhished'] = 'Y';
                    } else {
                        $item['youwhished'] = 'N';
                    }
                    //
                    $item['comment'] = $totalComments;
                    $item['wished'] = $userwished;
                    $item['added_time'] = $timeAdded['time'];
                    $item['first_name'] = $addeduser->first_name;
                    $item['last_name'] = $addeduser->last_name;
                    $item['addedby'] = $addeduser->first_name . ' ' . substr($addeduser->last_name, 0, 1);
                    $item['first_name'] = $addeduser->first_name;
                    $item['user_id'] = $addeduser->user_id;
                    $items[] = $item;

                    if ($addeduser->first_name != '') {
                        $pcnt++;
                    }
                }
                //print_r($items);
                //exit;
                $this->view->items = $products;
                $this->view->items_count = count($products) - $skip_ct;
                //$this->view->totalProduct = $totalProduct;
                $this->view->totalProduct = $pcnt;
                $this->view->extval = wishlistitmes::prepareExtVal($products, $_SESSION['user_id']);
                $this->view->categoryName = $catName;
            } else {
                $this->view->error = 'Please enter value to search';
            }
        } catch (Exception $e) {
            $this->view->errors[] = $e->getMessage();
        }
    }

    public function friendsAction() {
        $logger = Zend_Registry::get("logger");

        $param = $this->getRequest()->getParams();

        $searchKey = trim($param['search']);
        $this->view->search_key = $searchKey;
        $logger->info("Search key = ". $searchKey);
        $err = '';
        $userResult = array(); 
        $success = false;
        try {
            if ($searchKey != '') {

                $keych = '%' . $searchKey . '%';
                //$db_searchkey = $db->quote($keych);
                $db_searchkey = $keych;

                $logger->info("DB search Key: ". $keych);

                $logger->info("User ID = ". $_SESSION['user_id']);

                if ($_SESSION['user_id'] != '') {

                    /* Search for all users in the database
                     * and not just user's friends
                     */

                    $usernames = explode(" ", $db_searchkey);
                    $matching_users = array();
                    if ($usernames[0] != '' &&$usernames[1] != '') {
                        $logger->info("db search for first name" . $usernames[0]. "last name ".$usernames[1]);
                        $matching_users = users::searchUsersByFullName($usernames[0], $usernames[1]);
                    }
                    if (count($matching_users) <= 0) {
                        /* treating searchKey as a username */
                        $logger->info("db search for " . $db_searchkey);
                        $matching_users = users::searchUsersByName($db_searchkey);
                        $logger->info("Found ". count($matching_users) . " matching users among all users of Giftvise");
                    }
                    $matching_users_friends_map = usersfriend::searchFriendsByNameMap($_SESSION['user_id'], $db_searchkey);
                    $logger->info("Found ". count($matching_users_friends_map) . " matching friends among all friends of the user");
                    //$logger->info(print_r($matching_users_friends_map, true));

                    foreach ($matching_users as $user) {
                        if ($user->user_id != $_SESSION['user_id']) {
                            $userResult[] = users::prepareFriendDetails($user, $matching_users_friends_map);
                        }
                        $success = true;
                    }
                }
            } else {
                $err = 'Please enter value to search';
            }
        } catch (Exception $e) {
            $err = $e->getMessage();
        }
        return $this->_helper->json(array('success' => $success, 'user_id' => $_SESSION['user_id'], 'friends'=> $userResult, 'errors' => $err));
    }
}
