<?php


class SettingsController extends PS_Controller_Action
{
    public function preDispatch()
    {
        $this->view->headTitle(PS_Translate::_(""));
    }
    /*
     * FAQS Page
     */
    public function faqsAction()
    {
        $this->view->headTitle()->prepend(PS_Translate::_('Faqs'));
//        $server = Zend_Registry::get("server");
//        $this->_helper->layout()->disableLayout();
    }

    /*
     * Policy Page
     */
    public function policyAction()
    {
        $this->view->headTitle()->prepend(PS_Translate::_("Privacy Policy"));
        $this->view->isGuest = (isset($_SESSION['user_id']) && !empty($_SESSION['user_id']) && $_SESSION['user_id'] != '') ? false : true;
    }

    /*
     * About Page
     */
    public function aboutAction()
    {
        $this->view->headTitle()->prepend(PS_Translate::_("About Us"));
//        $this->_helper->layout()->disableLayout();
    }

    /*
     * Account Page
     */
    public function accountAction()
    {
        try{
            $this->view->headTitle()->prepend(PS_Translate::_("My Account"));
            $this->_helper->layout()->disableLayout();
            $this->view->userDetails = users::getUserDetails($_SESSION['user_id']);
            $this->view->month = date('n', time());
            $this->view->year = date('Y', time());
            $this->view->events = event::getmonthevents($_SESSION["user_id"], $this->view->month, $this->view->year, '', 'life-event');
        } catch (Exception $e) {
            var_dump($e->getMessage());exit;
        }
    }

    /// signup validation
    protected function _initUpdateAccount() {

        $this->fields = new PS_Form_Fields(
            array(
                "email" => array(
                    new PS_Validate_EmailAddress(),
                    "filter" => new PS_Filter_RemoveSpaces()
                ),
                "password" => array(
                    //$password,
                    new PS_Validate_Password(array("min" => 6, "digitRequired" => true))
                )
            )
        );
        $params = $this->getRequest()->getParams();
        $this->fields->setData($params);
        $this->view->fields = $this->fields;
    }
    
    public function updateaccountAction()
    {
        $this->_helper->layout()->disableLayout();
        $params = $this->getRequest()->getParams();

        $this->_initUpdateAccount();

        try {
            $this->fields->validate();

            $upload = new fileupload();
            $fileOptions = array('url' => $params['profile_pic'], 'generatedName' => $_SESSION['user_id']);
            $fileData = $upload->handleFiles($params['image'], 'userpics', $fileOptions);
            $params['profile_pic'] = $fileData['image'];
            
            users::updateAccount($params);

            $eventsCount = $params['events_count'];
            if ($eventsCount > 0) {
                $index = 0;
                while($index++ < $eventsCount){
                    if (!empty($params['event-date-'.$index])) {
                        event::updateEventDate($params['event-id-'.$index],$params['event-date-'.$index]);
                    }
                }
            }
        } catch (Exception $e) {
            $passwordError = substr($this->view->printError("password"), 32, -16);
            $emailError = substr($this->view->printError("email"), 32, -16);
            return $this->_helper->json(array('success' => false, 'error' => $e->getMessage(), 'passwordError' => $passwordError, 'emailError' => $emailError));
        }
        return $this->_helper->json(array('success' => true));
    }

    /*
     * Help Page
     */
    public function helpAction()
    {
        $this->view->headTitle()->prepend(PS_Translate::_("Help"));
//        $this->_helper->layout()->disableLayout();
    }

    /*
     * Contact Page
     */
    public function contactAction()
    {
        $this->view->headTitle()->prepend(PS_Translate::_("Contact Us"));
//        $this->_helper->layout()->disableLayout();
    }

}