<?php
class FileuploadController extends PS_Controller_Action
{
    public function preDispatch()
    {
    }

    /*
    *  file upload action returns false if error
    */
    public function fileuploadAction()
    {
        // disable layout
        $this->_helper->layout()->disableLayout();

        $request = $this->getRequest();

        $params = $request->getParams();
        $file_class = $params['class'];
        try{
            if ($request->isPost())
            {
                //set validators
                $adapter = new Zend_File_Transfer_Adapter_Http();
                $adapter
                    ->addValidator('IsImage', false)
                    ->addValidator('Size', false, array('min' => 20, 'max' => 4200000));
                $files = $adapter->getFileInfo();

                foreach($files as $file)
                {
                    if (($adapter->isUploaded($file['name']))&& ($adapter->isValid($file['name'])))
                    {
                        $upload = new fileupload();
                        $result = $upload->imageUploadToTemp($file_class, $file['name'], $params["height"], $params["width"], $params["thumbwidth"]);
                    } else {
                        $errors = $adapter->getErrors();
                        foreach ($errors as $error){
                            if ($error == 'fileSizeTooBig'){
                                $result['fileToBig'] = true;
                            }
                        }
                        $result['success'] = false;
                    }
                }
            }
        } catch(Exception $e){
            $result['error'] = $e->getMessage();
            $result['success'] = false;
        }


        // header settings
        header('Vary: Accept');
        if (isset($_SERVER['HTTP_ACCEPT']) &&
            (strpos($_SERVER['HTTP_ACCEPT'], 'application/json') !== false)) {
            header('Content-type: application/json');
        }
        //for IE < 10
        else {
            header('Content-type: text/plain');
        }
        return $this->_helper->json($result);
    }

    function deletetempimagesAction()
    {
        // disable layout
        $this->_helper->layout()->disableLayout();

        $request = $this->getRequest();

        $image = $request->getParam("image");
        try{
            unlink ($_SERVER["DOCUMENT_ROOT"]."/tempimg/" . $image);
            unlink ($_SERVER["DOCUMENT_ROOT"]."/tempimg/thumb/" . $image);
            return $this->_helper->json('success');
        } catch (Exception $e){
            print_r($e->getMessage());
            exit;
        }
    }
}
