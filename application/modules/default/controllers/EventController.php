<?php
/*------------------------------------------------------------------ 
 * Copyright (c) 2013-2014 by Giftvise, Inc.
 * All rights reserved. 
 *------------------------------------------------------------------
 */
class EventController extends PS_Controller_Action
{
	public function preDispatch()
	{
		
		$this->view->headTitle(PS_Translate::_(""));
	}

	public function deleteeventAction(){
	
		$db = Zend_Registry::get("db");
		$config = Zend_Registry::get("config");
		$server = Zend_Registry::get("server");	
			
		$params = $this->getRequest()->getParams();
        try
        {
             // delete events
             $db->delete('events', 'id = '.$params["event_id"]);
             $db->delete('eventrsvp', 'event_id = '.$params["event_id"]);
             $deleted_date=date("Y-m-d H:i:s");
             $data=array('event_id'=>$params["event_id"],'user_id'=>$_SESSION["user_id"],'deleted_date'=>$deleted_date);
             $db->insert('delete_event_log', $data);
             return $this->_helper->json(array('success' => true, 'msg' => 'Event deleted successfuly!'));

        }catch(Exception $e)
			{
                return $this->_helper->json(array('success' => false, 'msg' => $e->getMessage()));
            }
	}
	
	public function taggiftsAction(){
	
		$db = Zend_Registry::get("db");
		$config = Zend_Registry::get("config");
		$server = Zend_Registry::get("server");				
		$params = $this->getRequest()->getParams();
		
		$categories = category::categoryList();
		$this->view->categories = $categories;	
		$user_details = users::getUserDetails($_SESSION['user_id']);
		
		$this->view->userDetail = $user_details;
		
		$event_id = $params['event_id'];
		$ord = $params['order'];
		$catVal = $params['item_type'];
		$this->view->item_type1 = $catVal;
		$this->view->user_id = $params['user_id'];		
		//
		
		/// follows
		$followCount = usersfriend::countFollowList($_SESSION['user_id']);
		$this->view->follow = $followCount;
		
		/// follows
		$followerCount = usersfriend::countFollowerList($_SESSION['user_id']);
		$this->view->followers = $followerCount;
		
		// total gift send by user
		$giftSend = event::giftSend($_SESSION['user_id']);
		$this->view->giftsend = $giftSend;
		
		// total gift recived by user
		$giftRecieved = event::giftGet($_SESSION['user_id']);
		$this->view->giftget = $giftRecieved;
		
		/// budget		
		$totalExpense = budget::totalExpense($_SESSION['user_id']);
		$yearlyExpense = budget::totalExpenseYearly($_SESSION['user_id'], date('Y'));
		
		if($totalExpense->total != '')
			$this->view->totalExpense = $totalExpense->total;
		else
			$this->view->totalExpense = 0;
			
		if($yearlyExpense->total != '')
			$this->view->yearlyExpense = $yearlyExpense->total;
		else
			$this->view->yearlyExpense = 0;
		//	
		
		$orderBy = '';		
		if($params['pricekey'] == 'price')
		{
			if($orderBy != '')
				$orderBy .= ', item_price ASC';
			else
				$orderBy = ' item_price ASC';
		}
		
		if($params['datekey'] == 'date')
		{
			if($orderBy != '')
				$orderBy .= ', uw.added_date DESC';
			else
				$orderBy = 'uw.added_date DESC';
		}
	
		if($catVal !=""){
			$tag_item_ids = category::getUserTags($catVal,$_SESSION['user_id']);			
			if($tag_item_ids){
				$lastValue = count($tag_item_ids)-1;
				$i=0;
				foreach($tag_item_ids as $item_ids){
					$tag_item_id_for_event .= $item_ids->item_id;
					if($i != $lastValue){
						$tag_item_id_for_event .= ",";
					}
					$i++;
				}				
				$products = wishlistitmes::getProductDetailForTaggedItems($tag_item_id_for_event);
			}	
			else{
				$products=array();
			}					
		}
		else{ 
			$products = wishlistitmes::getAllProducts($_SESSION['user_id'],$orderBy,$catVal);			
		}
		
		if($catVal != '')
		{
		   $catName = category::categoryName($catVal);
		}
		else{
			$catName = 'Category';
		}
		
		$tag_items = event::tagGiftForEvent($event_id);
		$tag_items_item_id=explode(",",$tag_items);
			
		for($i=0; $i < count($products);$i++)
		{
			$item = array();
			
			$showChk = wishlistitmes::showItem($products[$i]->item_id, $_SESSION['user_id'],$friend_details->user_id);
			
			$totalComments = itemcomments::countComments($products[$i]->item_id);
			$userwished = wishlistitmes::countWished($products[$i]->item_id);
			
			/// latest item
			$timeAdded = wishlistitmes::latestItem($products[$i]->item_id);
		 	$addeduser = users::getUserDetails($timeAdded['user_id']);	
		
			$item['comment'] = $totalComments;
			$item['wished'] = $userwished;
			
			if(in_array($products[$i]->item_id,$tag_items_item_id)){
				$is_tagged[] = "Yes";
			}
			else{
				$is_tagged[] = "No";
			}
			
			$item['added_time'] = $timeAdded['time'];
			$item['addedby'] = $addeduser->first_name.' '.$addeduser->last_name;
			$item['show_item'] = $showChk;
			$items[] = $item;
		}
		
		if($param['allevent'] != ""){
			$event_limit='';			
		}
		else{
			$event_limit="";
			//$event_limit="LIMIT 3";
		}
		// set event show limit
		if($param["data"] !=""){			
			$eventdate=date('Y-m-d',strtotime($param["data"]));
			$eventList=event::gettodayevents($_SESSION["user_id"],$eventdate,$event_limit);
			if(count($eventList)>0){
				echo "<ul>";
				foreach($eventList as $event){ 
					if(strlen($event->title)>24){
						$title=substr(ucfirst($event->title),0,24).'...';
					}
					else{ 
						$title=$event->title;
					}
					if($_SESSION['user_id'] == $event->created_user_id){
					echo "<li class='noarrow'>";
					}
					else{
						echo "<li>";
					}
					if($_SESSION['user_id'] == $event->created_user_id){
						echo "<a href='".$this->server->apphost."/event/myeventdescription/event_id/".$event->event_id."' class='iframe noarrow'>";
					}
					else{
						echo "<a href='".$this->server->apphost."/event/inviteeventdescription/event_id/".$event->event_id."' class='iframe'>";
					}
					echo "<span class='date'>".date('d' ,strtotime($event->event_date)).'<br>'. date('M' ,strtotime($event->event_date))."</span>
					<span class='lefttext'>".$title."
					<br>
					<span class='lightgray'>".$event->location."</span></span>
					</a>";
					if($_SESSION['user_id'] != $event->created_user_id){
						echo "<ul class='sub-level'>
						 <div class='eventsubbox'>
						 	<span class='imgbox'><img src='".$this->server->apphost."/images/event-img.jpg' alt=''></span>
						 	<div class='eventsubboxtext'><span class='font14'>".ucfirst($event->title)."</span><br>
						 ".date('l d M Y' ,strtotime($event->event_date)).' at '.date('h:i a' ,strtotime($event->event_time)).' in '.$event->location."
						<br />	";
						
						//<span class='font12'>".$event->first_name.' '.$event->last_name."</span>";
						//echo " invited you ";
						
					  if($event->RSVP_status=='0')
							$decline = '<b>Decline</b>';
						else
							$decline = 'Decline';
							
						if($event->RSVP_status=='1')
							$Attend = '<b>Attend</b>';
						else
							$Attend = 'Attend';
						
						if($event->RSVP_status=='2')
							$Maybe = '<b>Maybe</b>';
						else
							$Maybe = 'Maybe';
							
														
						echo "<span id='eventrsvp_".$event->event_id."'>
						<a href='' onClick='return eventrsvp(1,".$event->event_id.")'>".$Attend."</a> | 
						<a href='' onClick='return eventrsvp(2,".$event->event_id.")'>".$Maybe."</a> | 
						<a href='' onClick='return eventrsvp(0,".$event->event_id.")'>".$decline."</a></span>";	
											
					echo "</div>";
					echo 
					"<input name='comment' type='text' value='write a comment' id='comment_".$event->event_id."' class='textfield' onKeyPress='return submitenter(this,event,".$event->event_id.")' onBlur=\"javaScript: if (this.value=='') {this.value='write a comment';}\" onClick=\"javaScript: if (this.value=='write a comment') {this.value='';}\">
					<input type='hidden' name='event_id' value='".$event->event_id."'/>
					</form>
					</div>
					</ul>";
					}
				echo "</li>";				
					}
				echo "</ul>";
					exit;
				}
				else{
					echo "<ul><li>No events for this date </li></ul>";
					exit;
				}				
			}
		else{
			$eventdate='';
			$eventList=event::gettodayevents($_SESSION["user_id"],$eventdate,$event_limit);
			$this->view->events = $eventList;
		}
		
		$category = category::usersTags($_SESSION["user_id"]);
		$listTag = category::showTag($_SESSION['user_id']);
		
		//$this->view->tags=$category;
		$this->view->tags=$listTag;
		
		$event_dates=event::getEventDate($_SESSION["user_id"]);		
		$this->view->upcoming_event = $event_dates;
		
		$is_taggift=event::tagGiftForEvent($event_id);
		
		if($is_taggift){
			$tag_gift=explode(",",$is_taggift);
			$total_tag_gift=count($tag_gift);
			$this->view->total_tag_gift = $total_tag_gift;
		}		
		$event_notification=event::getEventNotification($_SESSION["user_id"]);
		if(count($event_notification)>0){
			$this->view->e_notification= count($event_notification);
		}
		
		//
		// user's activity
		$activity = users::getActivity($_SESSION['user_id']);
		
		$activityLog = array();
		for($i=0; $i < count($activity); $i++)
		{
			$listA = array();
			
			$sender = users::getUserDetails($activity[$i]->sender);
			$event_title = event::getEventTitle($activity[$i]->event_id);
			$receiver = users::getUserDetails($activity[$i]->receiver);
			
			$time = users::getTime(strtotime($activity[$i]->activity_date));
			
			$item_name=wishlistitmes::getProductName($activity[$i]->item_id);
			
			$listA['id'] = $activity[$i]->id;
			$listA['item_id'] = $activity[$i]->item_id;
			$listA['item_name'] = $item_name;
			$listA['event_title'] = $event_title;
			$listA['sender_id'] = $activity[$i]->sender;
			$listA['sender_name'] = $sender->first_name.' '.$sender->last_name;
			$listA['sender_image'] = $sender->profile_pic;
			
			$listA['receiver_id'] = $activity[$i]->receiver;
			$listA['receiver_name'] = $receiver->first_name.' '.$receiver->last_name;
			$listA['receiver_image'] = $receiver->profile_pic;
			
			$listA['activity_type'] = $activity[$i]->activity_type;
			$listA['activity_time'] = $time;
			
			$activityLog[] = $listA;
		}
		$this->view->activityLog = $activityLog;
		
		/// All activity
		$allActivity = users::getAllActivity($_SESSION['user_id']);
		
		
		for($i=0; $i < count($allActivity); $i++)
		{
			$listA = array();
			
			$sender = users::getUserDetails($allActivity[$i]->sender);
			$event_title = event::getEventTitle($allActivity[$i]->event_id);
			$receiver = users::getUserDetails($allActivity[$i]->receiver);
			
			$time = users::getTime(strtotime($allActivity[$i]->activity_date));
			
			$item_name=wishlistitmes::getProductName($allActivity[$i]->item_id);
			
			$listA['id'] = $allActivity[$i]->id;
			$listA['item_id'] = $allActivity[$i]->item_id;
			$listA['item_name'] = $item_name;
			$listA['event_title'] = $event_title;
			$listA['sender_id'] = $allActivity[$i]->sender;
			$listA['sender_name'] = $sender->first_name.' '.$sender->last_name;
			$listA['sender_image'] = $sender->profile_pic;
			
			$listA['receiver_id'] = $allActivity[$i]->receiver;
			$listA['receiver_name'] = $receiver->first_name.' '.$receiver->last_name;
			$listA['receiver_image'] = $receiver->profile_pic;
			
			$listA['activity_type'] = $allActivity[$i]->activity_type;
			$listA['activity_time'] = $time;
			
			$allActivityList[] = $listA;
		}
		$this->view->allActivityLog = $allActivityList;
		//
		
		$this->view->items = $products;
		$this->view->event_id = $event_id;
		$this->view->is_tagged = $is_tagged;
		$this->view->extval = $items;
		$this->view->categoryName = $catName;
		
	}
	
	public function pickgiftAction(){
	
		$db = Zend_Registry::get("db");
		$config = Zend_Registry::get("config");
		$server = Zend_Registry::get("server");				
		$params = $this->getRequest()->getParams();
		
		$categories = category::categoryList();
		$this->view->categories = $categories;	
		$user_details = users::getUserDetails($_SESSION['user_id']);
		$this->view->userDetail = $user_details;
		
		$ord = $params['order'];
		$catVal = $params['item_type'];
		$event_id = $params['event_id'];
		
		$event_creater_user_id=event::getEventCreatedUserIDByEventID($event_id);
		
		$createduser_details = users::getUserDetails($event_creater_user_id);
		$this->view->createduser_details = $createduser_details;
		
		$event_name = event::getEventTitle($params["event_id"]);
				
		$this->view->event_name = $event_name;		
		
		
		/// followos
		$followCount = usersfriend::countFollowList($event_creater_user_id);
		$this->view->follow = $followCount;
		
		/// followos
		$followerCount = usersfriend::countFollowerList($event_creater_user_id);
		$this->view->followers = $followerCount;
		
		// total gift send by user
		$giftSend = event::giftSend($event_creater_user_id);
		$this->view->giftsend = $giftSend;
		
		// total gift recived by user
		$giftRecieved = event::giftGet($event_creater_user_id);
		$this->view->giftget = $giftRecieved;
		
		/// budget		
		$totalExpense = budget::totalExpense($event_creater_user_id);
		$yearlyExpense = budget::totalExpenseYearly($event_creater_user_id, date('Y'));
		
		if($totalExpense->total != '')
			$this->view->totalExpense = $totalExpense->total;
		else
			$this->view->totalExpense = 0;
			
		if($yearlyExpense->total != '')
			$this->view->yearlyExpense = $yearlyExpense->total;
		else
			$this->view->yearlyExpense = 0;
		//
		
		
		
		$orderBy = '';		
		if($params['pricekey'] == 'price')
		{
			if($orderBy != '')
				$orderBy .= ', item_price ASC';
			else
				$orderBy = ' item_price ASC';
		}
		
		if($params['datekey'] == 'date')
		{
			if($orderBy != '')
				$orderBy .= ', uw.added_date DESC';
			else
				$orderBy = 'uw.added_date DESC';
		}
		
		if($catVal !=""){
			$pick_item_ids =category::getUserTags($catVal,$event_creater_user_id);			
			if($pick_item_ids){
				$lastValue=count($pick_item_ids)-1;
				$i=0;
				foreach($pick_item_ids as $item_ids){
					$pick_item_id_for_event .=$item_ids->item_id;
					if($i != $lastValue){
						$pick_item_id_for_event .=",";
					}
					$i++;
				}				
				$products = wishlistitmes::getProductDetailForTaggedItems($pick_item_id_for_event);
			}	
			else{
				$products=array();
			}					
		}
		else{ 
			$products = wishlistitmes::getAllProducts($event_creater_user_id,$orderBy,$catVal);			
		}
		
		if($catVal != '')
		{
		   $catName = category::categoryName($catVal);
		}
		else{
			$catName = 'Category';
		}
		
		
		for($i=0; $i < count($products);$i++)
		{
			$item = array();			
			$showChk = wishlistitmes::showItem($products[$i]->item_id, $event_creater_user_id,$friend_details->user_id);
			
			$totalComments = itemcomments::countComments($products[$i]->item_id);
			$userwished = wishlistitmes::countWished($products[$i]->item_id);	
			/// latest item
			$timeAdded = wishlistitmes::latestItem($products[$i]->item_id);
		 	$addeduser = users::getUserDetails($timeAdded['user_id']);
			
			//$is_buyed = event::isbuyGiftForEvent($_SESSION["user_id"],$products[$i]->item_id,$event_creater_user_id,$event_id);
			 $is_buyed = event::isbuyGiftForUser($event_creater_user_id, $products[$i]->item_id);	
			 $quantity = wishlistitmes::getQty($products[$i]->item_id,$event_creater_user_id);		 
			
			/*if($is_buyed){
				$item['is_buyed_status'] = 'Yes';
			}else{				
				$item['is_buyed_status'] = 'No';
			}*/
			if($is_buyed)
			{
				if($quantity == 0)
				{
					$item['is_buyed_status']='Yes';
				}else{
					$item['is_buyed_status']='No';
				}
			}else{ 
				if($quantity == 0)
					$item['is_buyed_status']='Yes';
				else
					$item['is_buyed_status']='No';
			}
			
			$itemwhished = wishlistitmes::chkWishedItem($products[$i]->item_id,$_SESSION['user_id']);
			
			if($itemwhished)
			{
				$item['youwhished'] = 'Y';
			}else{
				$item['youwhished'] = 'N';
			}
			
			$item['comment'] = $totalComments;
			$item['wished'] = $userwished;
			$item['added_time'] = $timeAdded['time'];
			$item['addedby'] = $addeduser->first_name.' '.$addeduser->last_name;
			$item['show_item'] = $showChk;
			$items[] = $item;
			
		}
		
		
		if($param['allevent'] !=""){
			$event_limit='';			
		}
		else{
			$event_limit="";
			//$event_limit="LIMIT 3";
		}
		// set event show limit
		if($param["data"] !=""){			
			$eventdate=date('Y-m-d',strtotime($param["data"]));
			$eventList=event::gettodayevents($_SESSION["user_id"],$eventdate,$event_limit);
			if(count($eventList)>0){
				echo "<ul>";
				foreach($eventList as $event){ 
					if(strlen($event->title)>24){
						$title=substr(ucfirst($event->title),0,24).'...';
					}
					else{ 
						$title=$event->title;
					}
					if($_SESSION['user_id'] == $event->created_user_id){
					echo "<li class='noarrow'>";
					}
					else{
						echo "<li>";
					}
					if($_SESSION['user_id'] == $event->created_user_id){
						echo "<a href='".$this->server->apphost."/event/myeventdescription/event_id/".$event->event_id."' class='iframe noarrow'>";
					}
					else{
						echo "<a href='".$this->server->apphost."/event/inviteeventdescription/event_id/".$event->event_id."' class='iframe'>";
					}
					
					echo "<span class='date'>".date('d' ,strtotime($event->event_date)).'<br>'. date('M' ,strtotime($event->event_date))."</span>
					<span class='lefttext'>".$title."
					<br>
					<span class='lightgray'>".$event->location."</span></span>
					</a>";
					if($_SESSION['user_id'] != $event->created_user_id){
						echo "<ul class='sub-level'>
						 <div class='eventsubbox'>
						 <span class='imgbox'><img src='".$this->server->apphost."/images/event-img.jpg' alt=''></span>
						 <div class='eventsubboxtext'><span class='font14'>".ucfirst($event->title)."</span><br>
						 ".date('l d M Y' ,strtotime($event->event_date)).' at '.date('h:i a' ,strtotime($event->event_time)).' in '.$event->location."
						<br />	";
						
						//<span class='font12'>".$event->first_name.' '.$event->last_name."</span>";
						//echo " invited you ";
					  if($event->RSVP_status=='0')
							$decline = '<b>Decline</b>';
						else
							$decline = 'Decline';
							
						if($event->RSVP_status=='1')
							$Attend = '<b>Attend</b>';
						else
							$Attend = 'Attend';
						
						if($event->RSVP_status=='2')
							$Maybe = '<b>Maybe</b>';
						else
							$Maybe = 'Maybe';
							
														
						echo "<span id='eventrsvp_".$event->event_id."'>
						<a href='' onClick='return eventrsvp(1,".$event->event_id.")'>".$Attend."</a> | 
						<a href='' onClick='return eventrsvp(2,".$event->event_id.")'>".$Maybe."</a> | 
						<a href='' onClick='return eventrsvp(0,".$event->event_id.")'>".$decline."</a></span>";	
											
					echo "</div>";
					echo 
					"<input name='comment' type='text' value='write a comment' id='comment_".$event->event_id."' class='textfield' onKeyPress='return submitenter(this,event,".$event->event_id.")' onBlur=\"javaScript: if (this.value=='') {this.value='write a comment';}\" onClick=\"javaScript: if (this.value=='write a comment') {this.value='';}\">
					<input type='hidden' name='event_id' value='".$event->event_id."'/>
					</form>
					</div>
					</ul>";
					}
				echo "</li>";				
					}
				echo "</ul>";
					exit;
				}
				else{
					echo "<ul><li>No events for this date</li></ul>";
					exit;
				}				
			}
		else{
			$eventdate='';
			$eventList=event::gettodayevents($_SESSION["user_id"],$eventdate,$event_limit);
			$this->view->events = $eventList;
		}
		
		$event_dates=event::getEventDate($_SESSION["user_id"]);		
		$this->view->upcoming_event = $event_dates;
		$category=category::usersTags($event_creater_user_id);
		
		$this->view->tags=$category;
		//$this->view->is_buyed=$category;
		
		$total_buyed_gift=event::totalBuyGiftsForEvent($_SESSION['user_id'],$event_creater_user_id,$event_id);
		
		if($total_buyed_gift){			
			$this->view->total_buyed_gift = $total_buyed_gift;
		}	
		
		$event_notification=event::getEventNotification($_SESSION["user_id"]);
		if(count($event_notification)>0){
			$this->view->e_notification= count($event_notification);
		}
		
		
		// user's activity
		$activity = users::getActivity($event_creater_user_id);
		
		
		$activityLog = array();
		for($i=0; $i < count($activity); $i++)
		{
			$listA = array();
			
			$sender = users::getUserDetails($activity[$i]->sender);
			$event_title = event::getEventTitle($activity[$i]->event_id);
			$receiver = users::getUserDetails($activity[$i]->receiver);
			
			$time = users::getTime(strtotime($activity[$i]->activity_date));
			
			$item_name=wishlistitmes::getProductName($activity[$i]->item_id);
			
			$listA['id'] = $activity[$i]->id;
			$listA['item_id'] = $activity[$i]->item_id;
			$listA['item_name'] = $item_name;
			$listA['event_title'] = $event_title;
			$listA['sender_id'] = $activity[$i]->sender;
			$listA['sender_name'] = $sender->first_name.' '.$sender->last_name;
			$listA['sender_image'] = $sender->profile_pic;
			
			$listA['receiver_id'] = $activity[$i]->receiver;
			$listA['receiver_name'] = $receiver->first_name.' '.$receiver->last_name;
			$listA['receiver_image'] = $receiver->profile_pic;
			
			$listA['activity_type'] = $activity[$i]->activity_type;
			$listA['activity_time'] = $time;
			
			$activityLog[] = $listA;
		}
		$this->view->activityLog = $activityLog;
		
		/// All activity
		$allActivity = users::getAllActivity($event_creater_user_id);
		
		
		for($i=0; $i < count($allActivity); $i++)
		{
			$listA = array();
			
			$sender = users::getUserDetails($allActivity[$i]->sender);
			$event_title = event::getEventTitle($allActivity[$i]->event_id);
			$receiver = users::getUserDetails($allActivity[$i]->receiver);
			
			$time = users::getTime(strtotime($allActivity[$i]->activity_date));
			
			$item_name=wishlistitmes::getProductName($allActivity[$i]->item_id);
			
			$listA['id'] = $allActivity[$i]->id;
			$listA['item_id'] = $allActivity[$i]->item_id;
			$listA['item_name'] = $item_name;
			$listA['event_title'] = $event_title;
			$listA['sender_id'] = $allActivity[$i]->sender;
			$listA['sender_name'] = $sender->first_name.' '.$sender->last_name;
			$listA['sender_image'] = $sender->profile_pic;
			
			$listA['receiver_id'] = $allActivity[$i]->receiver;
			$listA['receiver_name'] = $receiver->first_name.' '.$receiver->last_name;
			$listA['receiver_image'] = $receiver->profile_pic;
			
			$listA['activity_type'] = $allActivity[$i]->activity_type;
			$listA['activity_time'] = $time;
			
			$allActivityList[] = $listA;
		}
		$this->view->allActivityLog = $allActivityList;
		//
		
		$this->view->touser_id = $event_creater_user_id;
		$this->view->event_id = $event_id;
		$this->view->is_buyed_item = $is_buyed_status;
		$this->view->items = $products;
		$this->view->extval = $items;
		$this->view->categoryName = $catName;
		
	}
	
	// my event description
	
	//import facebook event
	public function inviteeventdescriptionAction()
	{	
		$db 	=   Zend_Registry::get("db");
		$config =   Zend_Registry::get("config");
		$server =   Zend_Registry::get("server");

		$this->view->headTitle(PS_Translate::_("Import Facebook Event"));
		$event_id = $this->_getParam("event_id");
		$commentall = $this->_getParam("commentall");
		$event_id = $this->_getParam("event_id");
		$rsvp = $this->_getParam("rsvp");
		$comment = $this->_getParam("comment");
		//
		$via = $this->_getParam("via");
		$this->view->via = $via;
		//		
		try{			
			$db->beginTransaction();
			if($rsvp){
				if($rsvp =='Attend') $rsvp_status='1';
				if($rsvp =='Decline') $rsvp_status='0';
				if($rsvp =='Maybe') $rsvp_status='2';
				
				$rsvp_action=array('RSVP_status'=>$rsvp_status);
				$where = array("invited_user = ?" => $_SESSION['user_id'],"event_id = ?" => $event_id);					
				$db->update('eventrsvp',$rsvp_action,$where);
			}
			if($comment){
				$comment_date=date("Y-m-d H:i:s");
				$comment=array("user_id"=>$_SESSION["user_id"],"event_id"=>$event_id,"comments"=>$comment,"comment_date"=>$comment_date);
				$db->insert("eventcomments",$comment);
			}
			
			$get_rsvp_status=event::getRsvpStatus($_SESSION["user_id"],$event_id);
			$user_pic=users::getUserDetails($_SESSION['user_id']);
			$event_description = event::eventDescriptionById($event_id);
			
			
			$this->view->event_description=$event_description;
			$this->view->user_profile_pic=$user_pic->profile_pic;
			
			$total_event_comment = event::totalCommentOfEvent($event_id);
			
			
			$this->view->total_event_comment=$total_event_comment;
			$this->view->event_id=$event_id;
			$this->view->rsvp_status=$get_rsvp_status;
			
			$event_creater_user_id=event::getEventCreatedUserIDByEventID($event_id);
			$total_buyed_gift=event::totalBuyGiftsForEvent($_SESSION['user_id'],$event_creater_user_id,$event_id);
		
			if($total_buyed_gift){			
				$this->view->total_buyed_gift = $total_buyed_gift;
			}	
			if($commentall == 'All'){
				$event_comments = event::getAllEventComments($event_id);
				$result='';
				
				foreach($event_comments as $commentresult){
				
					$user_pic_base_url = "https://".$server->apphost."/userpics/thumb/";
					
					$profile_pic=($commentresult->profile_pic ==""?"https://".$server->apphost."/images/icon.jpg":$user_pic_base_url.$commentresult->profile_pic);
					
					echo 	"<div class='commentsbox'>							
							<span class='imgbox'><img src='".$profile_pic."' alt=''></span>
							<div class='commentstext'><span class='font12'>".$commentresult->name."</span><br>
							".$commentresult->comments."</div>
								<div class='clear'></div>
							</div>";		
				}
				exit;
			}
			else{
				$event_comments=event::eventComments($event_id);				
				$this->view->event_comments=$event_comments;
			}
			$db->commit();
			
			
			// Get Tag id for this event
			$eventTitle = '%'.$event_description->title.'%';
			$eventTitle = $db->quote($eventTitle);
			
			$tag_id = event::getTagID($eventTitle);
			$this->view->tag_id = $tag_id;
			//exit;
			//		
			
		}
		catch(Exception $e)
		{
			//Rollback transaction			
			//$db->rollBack();
			echo $e->getMessage();
			$this->view->errors[] = $e->getMessage();	
			exit;
		}	
	}
	
	/*public function eventnotificationAction()
	{	
		$db 	=   Zend_Registry::get("db");
		$config =   Zend_Registry::get("config");
		$server =   Zend_Registry::get("server");
		
		try{
			$month='01';
			$year='2013';
			$db->beginTransaction();			
							
			$sql=$db->query("SELECT CONCAT(first_name,' ',last_name) AS name,profile_pic,created_user AS 			created_user_id,event_date,event_time,title,description,location,
							image AS event_image,ifnull(eventrsvp.invited_user,'') AS invited_user,RSVP_status,events.updated_date as updated_date,events.id as event_id FROM events LEFT JOIN eventrsvp ON eventrsvp.event_id=events.id JOIN users ON users.user_id=events.created_user
							WHERE invited_user ='".$_SESSION["user_id"]."' AND MONTH(event_date) ='".$month."' AND YEAR(event_date) ='".$year."' AND event_date>=curdate() ORDER BY updated_date DESC");
			
			$rows_event_notification=$sql->fetchAll();
			$this->view->event_notification=$rows_event_notification;
			$db->commit();
			
		}
		catch(Exception $e)
		{
			//Rollback transaction			
			$db->rollBack();
			$this->view->errors[] = $e->getMessage();	
			exit;
		}	
	}*/
	
	public function eventnotificationAction()
 { 
  $db  =   Zend_Registry::get("db");
  $config =   Zend_Registry::get("config");
  $server =   Zend_Registry::get("server");      
  
  try{
   $month=date("m");
   $year=date("Y");
   $db->beginTransaction();   
       
   /* $sql=$db->query("SELECT CONCAT(first_name,' ',last_name) AS name,profile_pic,created_user AS created_user_id,event_date,event_time,title,description,location,
       image AS event_image,ifnull(eventrsvp.invited_user,'') AS invited_user,RSVP_status,events.updated_date as updated_date,events.id as event_id FROM events LEFT JOIN eventrsvp ON eventrsvp.event_id=events.id JOIN users ON users.user_id=events.created_user
       WHERE invited_user ='".$_SESSION["user_id"]."' AND MONTH(event_date) ='".$month."' AND YEAR(event_date) ='".$year."' AND event_date>=curdate() ORDER BY updated_date DESC");
       */
   $sql=$db->query("SELECT CONCAT(first_name,' ',last_name) AS name,profile_pic,created_user AS created_user_id,event_date,event_time,title,description,location,
       image AS event_image,ifnull(eventrsvp.invited_user,'') AS invited_user,RSVP_status,events.updated_date as updated_date,events.id as event_id FROM events LEFT JOIN eventrsvp ON eventrsvp.event_id=events.id JOIN users ON users.user_id=events.created_user
       WHERE invited_user ='".$_SESSION["user_id"]."' AND event_date>=curdate() AND notification_status='0' ORDER BY updated_date DESC");
   $rows_event_notification=$sql->fetchAll();
   $this->view->event_notification=$rows_event_notification;
   
   // update event_notification status
   $notification_status=array("notification_status"=>'1');
   $where = array("invited_user = ?" => $_SESSION["user_id"]);     
   $db->update('eventrsvp',$notification_status,$where);      
   $db->commit();
   
  }
  catch(Exception $e)
  {
   //Rollback transaction   
   $db->rollBack();
   $this->view->errors[] = $e->getMessage(); 
   exit;
  } 
 }
 
	// myevent description page
	public function myeventdescriptionAction()
	{	
		$db 	=   Zend_Registry::get("db");
		$config =   Zend_Registry::get("config");
		$server =   Zend_Registry::get("server");						
		$event_id = $this->_getParam("event_id");
		$commentall = $this->_getParam("commentall");
		$event_id = $this->_getParam("event_id");
		$rsvp = $this->_getParam("rsvp");
		$comment = $this->_getParam("comment");
		
		//
		$via = $this->_getParam("via");
		$this->view->via = $via;
		//
		try{
			
			$db->beginTransaction();
			
			if($comment){
				$comment_date=date("Y-m-d H:i:s");
				$comment=array("user_id"=>$_SESSION["user_id"],"event_id"=>$event_id,"comments"=>$comment,"comment_date"=>$comment_date);
				$db->insert("eventcomments",$comment);
			}
			
			$user_pic=users::getUserDetails($_SESSION['user_id']);
			
			$event_description=event::eventDescriptionById($event_id);
			
			
			
			$this->view->event_description=$event_description;
			$this->view->user_profile_pic=$user_pic->profile_pic;
			$total_event_comment=event::totalCommentOfEvent($event_id);
			$this->view->total_event_comment=$total_event_comment;
			$this->view->event_id=$event_id;
			$this->view->rsvp_status=$get_rsvp_status;
			
			if($commentall=='All'){
			$event_comments=event::getAllEventComments($event_id);	
				$result='';
				foreach($event_comments as $commentresult){	
					$user_pic_base_url="https://".$server->apphost."/userpics/thumb/";
					$profile_pic=($commentresult->profile_pic ==""?"https://".$server->apphost."/images/icon.jpg":$user_pic_base_url.$commentresult->profile_pic);
					echo 	"<div class='commentsbox'>							
							<span class='imgbox'><img src='".$profile_pic."' alt=''></span>
							<div class='commentstext'><span class='font12'>".$commentresult->name."</span><br>
							".$commentresult->comments."</div>
								<div class='clear'></div>
							</div>";		
				}
				exit;
			}
			else{
				$event_comments=event::eventComments($event_id);				
				$this->view->event_comments=$event_comments;
			}
			$is_taggift=event::tagGiftForEvent($event_id);
			
			if($is_taggift){
				$tag_gift=explode(",",$is_taggift);
				$total_tag_gift=count($tag_gift);
				$this->view->total_tag_gift = $total_tag_gift;
			}
			
			
			$db->commit();
			
			// Get Tag id for this event
			$eventTitle = '%'.$event_description->title.'%';
			$eventTitle = $db->quote($eventTitle);
			
			$tag_id = event::getTagID($eventTitle);
			$this->view->tag_id = $tag_id;
		}
		catch(Exception $e)
		{
			//Rollback transaction			
			//$db->rollBack();
			echo $e->getMessage();
			$this->view->errors[] = $e->getMessage();	
			exit;
		}	
	}
	
	//insert comment
	public function addcommentAction()
	{	
		$db 	=   Zend_Registry::get("db");
		$config =   Zend_Registry::get("config");
		$server =   Zend_Registry::get("server");						
		$event_id = $this->_getParam("event_id");		
		$comment = $this->_getParam("comment");
		
		try{			
			$db->beginTransaction();			
				
				$comment_date=date("Y-m-d H:i:s");
				$comment=array("user_id"=>$_SESSION["user_id"],"event_id"=>$event_id,"comments"=>$comment,"comment_date"=>$comment_date);
				$db->insert("eventcomments",$comment);
				$db->commit();
				echo "<p>kjshdfkjhsf</p>";				
				exit;
			
		}
		catch(Exception $e)
		{
			//Rollback transaction			
			$db->rollBack();			
			$this->view->errors[] = $e->getMessage();	
			exit;
		}	
	}
	
	// event RSVp
	public function eventrsvpAction()
	{	
		$db 	=   Zend_Registry::get("db");
		$config =   Zend_Registry::get("config");
		$server =   Zend_Registry::get("server");		
		
		$event_id = $this->_getParam("event_id");
		$rsvp_opt = $this->_getParam("rsvp_opt");
		$rsvp_optval = $this->_getParam("rsvp_opt");
		$user_id = $_SESSION["user_id"];
		
		try{			
			$db->beginTransaction();			
			$rsvp_action=array('RSVP_status'=>$rsvp_opt);
			$where = array("invited_user = ?" => $user_id,"event_id = ?" => $event_id);					
			$db->update('eventrsvp',$rsvp_action,$where);						
			$db->commit();
            $event = event::eventDescriptionById($event_id);
            return $this->_helper->json(array('success' => true, 'accepted' => $event->going, 'declined' => $event->notgoing, 'maybe' => $event->maybe));

		}
		catch(Exception $e)
		{
			//Rollback transaction			
			$db->rollBack();
            return $this->_helper->json(array('success' =>false, 'msg' => $e->getMessage()));
		}	
	}
	
	public function getfriendforeventAction()
	{
		$db = Zend_Registry::get("db");
		$config = Zend_Registry::get("config");
		$server = Zend_Registry::get("server");
		
		$params = $this->getRequest()->getParams();
		$key = $params['key'];
		$event_id = $params['event_id'];
		
		$searchResult = usersfriend::searchFollowList($_SESSION['user_id'], $key);								
			$listStr = '';
			if(!empty($searchResult)){
				for($i=0; $i <count($searchResult); $i++)
				{
					$friend_event_status=event::isCheckFriendEventInviteOpt($searchResult[$i]->friend_id,$event_id);
					$list = array();
					$user_detail = users::getUserDetails($searchResult[$i]->friend_id);
					if($friend_event_status){

						$listStr .= '<li style="display: block; min-height: 40px;" id="c_'.$searchResult[$i]->friend_id.'">';
						$listStr .='<span id="invite_'.$searchResult[$i]->friend_id.'_'.$event_id.'" class="arrowtag"><img src="https://'.$server->apphost.'/images/tick.png" style="float:left"></span>';
					}
					else{
						$listStr .= '<li style="display: block; min-height: 40px;" id="c_'.$searchResult[$i]->friend_id.'" onClick="eventinvitefriend('.$searchResult[$i]->friend_id.', '.$event_id.')">';
						$listStr .='<span id="invite_'.$searchResult[$i]->friend_id.'_'.$event_id.'" class="arrowtag"></span>';
					}
					if($user_detail->profile_pic != '')
					{
					$listStr .='<img src="https://'.$server->apphost.'/userpics/thumb/'.$user_detail->profile_pic.'" style="float:left">';
					}
					else{
						$listStr .='<img src="https://'.$server->apphost.'/userpics/top-img.png" style="float:left">';
					}
					$listStr .= $user_detail->first_name.' '.$user_detail->last_name;
					$listStr .= '</li>';
				}
			}
			else{
				$listStr .= '<li style="display: block; min-height: 40px;">You do not have any friend.</li>';
			}
			echo $listStr;
			exit;
	}
	
	public function eventinvitefriendAction(){
		
		$db = Zend_Registry::get("db");
		$config = Zend_Registry::get("config");
		$server = Zend_Registry::get("server");
			
		$params = $this->getRequest()->getParams();
		
		try
		{
            $friendInvited=event::isCheckFriendEventInviteOpt($params['invite_user_id'],$params['event_id']);
            if($friendInvited){
                return $this->_helper->json(array('success' => false, 'invited'=>true, 'msg' => 'This friend is already invited'));
            } else {
                $db->beginTransaction();
                $db->insert('activity', array('activity_type' => 'Invitation', 'sender' => $_SESSION['user_id'], 'receiver' => $params['invite_user_id'], 'item_id' => '0', 'event_id' => $params['event_id'], 'activity_date' => date('Y-m-d H:i:s'), 'deleted_user' => '0', 'new' => true));
                $activityId = $db->lastInsertId();
                $invite_friend= array("user_id"=>$_SESSION['user_id'],"invited_user"=>$params['invite_user_id'],"event_id"=>$params['event_id'],"RSVP_status"=>'3', "notification_status" => $activityId);
                $db->insert('eventrsvp', $invite_friend);
                $db->commit();
            }
//			$totalinvited=event::getInvitedUsers($params["event_id"]);
            return $this->_helper->json(array('success' => true, 'msg' => 'Friend successfuly invited!'));
		}			
		catch(Exception $e)
		{
		//Rollback transaction
		
		$db->rollBack();
        return $this->_helper->json(array('success' => false, 'msg' => $e->getMessage()));
		}	
	}

    public function eventinvitegroupAction(){

        $db = Zend_Registry::get("db");

        $params = $this->getRequest()->getParams();

        $groupId = $params['invite_group_id'];
        $eventId = $params['event_id'];

        try
        {
            $db->beginTransaction();
            $friends = groups::getFriendsFromGroup($groupId, $_SESSION['user_id']);
            $invitedFriendsCount = 0;
            foreach($friends as $friend){
                if(!event::isCheckFriendEventInviteOpt($friend->user_id, $eventId)){
                    $db->insert('activity', array('activity_type' => 'Invitation', 'sender' => $_SESSION['user_id'], 'receiver' => $friend->user_id, 'item_id' => '0', 'event_id' => $eventId, 'activity_date' => date('Y-m-d H:i:s'), 'deleted_user' => '0', 'new' => true));
                    $activityId = $db->lastInsertId();
                    $invite_friend= array("user_id"=>$_SESSION['user_id'],"invited_user"=>$friend->user_id,"event_id"=>$eventId,"RSVP_status"=>'3', "notification_status" => $activityId);
                    $db->insert('eventrsvp', $invite_friend);
                    $invitedFriendsCount++;
                }
            }
            $db->commit();
            return $this->_helper->json(array('success' => true, 'count'=>$invitedFriendsCount, 'friendCount' => count($friends),'msg' => 'Friends successfuly invited!'));
        }
        catch(Exception $e)
        {
            //Rollback transaction
            $db->rollBack();
            return $this->_helper->json(array('success' => false, 'msg' => $e->getMessage()));
        }
    }
	
	// tagged gift to event
	public function taggiftforeventAction(){
	
		$db = Zend_Registry::get("db");
		$config = Zend_Registry::get("config");
		$server = Zend_Registry::get("server");
		
		$params = $this->getRequest()->getParams();
		
		$event_id =  $params['event_id'];
		$user_id  =  $params['user_id'];		
		$item_id  =  $params['item_id'];
		$tag_date =  date('Y-m-d H:i:s');
		
		try{
		
			$db->beginTransaction();			
			$is_taggift=event::tagGiftForEvent($event_id);
			if(!empty($is_taggift)){
			$all_item_id=$is_taggift.",".$item_id;
			}else{
			$all_item_id=$item_id;
			}
			
			$item_ids=explode(",",$all_item_id);
			
			foreach($item_ids as $tagitem_id){
				if($tagitem_id !=""){
					$product_item_id[]=$tagitem_id;
					$unique_tag_item=array_unique($product_item_id);
				}
			}
			
			if($is_taggift){
				if(!empty($product_item_id)){
				$update_taggift =array('event_id'=>$event_id,'item_id'=>implode(",",$unique_tag_item),'user_id'=>$user_id,'tag_date'=>$tag_date);
				$db->update('taggifts',$update_taggift,'event_id = '.$event_id);				
				$event_date_update=array("updated_date"=>$tag_date);
				$db->update('events',$event_date_update,'id = '.$event_id);					
				}
				else{
				$where = array("event_id = ?" => $event_id,"user_id = ?" => $user_id);	
				$db->delete("taggifts",$where);
				}
			}
			else{
				// create a tag 
				$event_title=event::getEventTitle($event_id);
				$ctg_nm=category::checkCategoryName($event_title,$user_id);				
				
				if(!$ctg_nm){
					$tag=array("name"=>$event_title,"created_by"=>$user_id);
					$db->insert("category",$tag);
					$ctg_nm=$db->lastInsertId();			
				}
				
				$insert_taggift=array('event_id'=>$event_id,'item_id'=>implode(",",$product_item_id),'user_id'=>$user_id,'tag_id'=>$ctg_nm,'tag_date'=>$tag_date);	
				$db->insert('taggifts',$insert_taggift);				
			}
			$db->commit(); 
			$is_taggift=event::tagGiftForEvent($event_id);			
			if(!empty($is_taggift)){
				$tag_gift=explode(",",$is_taggift);
				$total_tag_gift=count($tag_gift);				
				echo "".$total_tag_gift." gift has been tagged";
			}
			else{
				echo "You have tagged no gifts";
			}
			exit;
		}
		catch(Exception $e)
		{
			$this->view->errors[] = $e->getMessage();
			$info["error"] = $e->getMessage();	
		}		
	}
	
	// pick a gift for event	
	public function pickgiftforeventAction(){
	
		$db = Zend_Registry::get("db");
		$config = Zend_Registry::get("config");
		$server = Zend_Registry::get("server");	
	        $logger = Zend_Registry::get("logger");
	
		$params = $this->getRequest()->getParams();		
		$add_date=date('Y-m-d H:i:s');

                $eventId = $params["event_id"];
		if( $eventId != '' && $eventId != 0)
		{
			$receiver_id=event::getEventCreatedUserIDByEventID($params["event_id"]);
		}else{
                        $eventId = 0;
			$receiver_id = $params['reciver'];
		}
	
		try{
		
			$db->beginTransaction();							
			$datetime=date('Y-m-d H:i:s');
			/*
			$purchased_gift=array("item_id"=>$params["item_id"],"amount"=>str_replace(',','',$params["amount"]),"user_id"=>$_SESSION["user_id"],
			"receiver_id"=>$receiver_id,"event_id"=>$eventId,"purchased_date"=>$add_date,'received_date' => '00/00/0000 00:00');
			$db->insert('giftsexchanged', $purchased_gift);
			$item_required_no=event::getItemRequiredNo($receiver_id,$params["item_id"]);
			*/
			$item_required_no = 1;
			$update_userwishlist = array("items_required_no"=>($item_required_no-1), "reserved_on" => $datetime, "gift_state" => 1,"reserved_by" => $_SESSION["user_id"]);
			
			$where_uwishlist=array("user_id =?" =>$receiver_id,"item_id= ?"=>$params["item_id"]);
				
			$db->update('userwishlist',$update_userwishlist,$where_uwishlist);
			
			// activity
			/*$activity_data=array("activity_type"=>"Gift type","sender"=>$_SESSION["user_id"],"receiver"=>$receiver_id,"item_id"=>$params["item_id"],"event_id"=>$params["event_id"],"activity_date"=>$add_date, "deleted_user"=>$receiver_id);
			$db->insert('activity', $activity_data);	*/									
			$db->commit();	 
			return $this->_helper->json(array('success' => true, 'msg' =>  "You have successfully reserved a gifts for event"));
		}
		catch(Exception $e)
		{
            $db->rollBack();
            return $this->_helper->json(array('success' => false, 'msg' => $e->getMessage()));
		}
	}

        // Unreserve a gift        
        public function unreservegiftsAction(){

                $db = Zend_Registry::get("db");
                $config = Zend_Registry::get("config");
                $server = Zend_Registry::get("server");

                $params = $this->getRequest()->getParams();

                $receiver_id = $_SESSION["friend_id"]; 

                try{

                        $db->beginTransaction();

                        $update_userwishlist = array("gift_state" => 0);

                        $where_uwishlist=array("user_id =?" =>$receiver_id,"item_id= ?"=>$params["item_id"], "reserved_by= ?"=> $_SESSION["user_id"]);
                                
                        $db->update('userwishlist',$update_userwishlist,$where_uwishlist);
                        
                        $db->commit();          
                        return $this->_helper->json(array('success' => true, 'msg' =>  "You have successfully unreserved this gift"));
                }
                catch(Exception $e)
                {
            $db->rollBack();
            return $this->_helper->json(array('success' => false, 'msg' => $e->getMessage()));
                }
        }

	// gift as received
	public function giftreceivedAction(){
	
		$db = Zend_Registry::get("db");
		$config = Zend_Registry::get("config");
		$server = Zend_Registry::get("server");	
			
		$params = $this->getRequest()->getParams();		
		$add_date=date('Y-m-d H:i:s');
		
		$eventId=(empty($params["event_id"])) ? 0 : $params["event_id"];//event::getEventCreatedUserIDByEventID($params["event_id"]);

		try
		{		
			$db->beginTransaction();							
			$datetime=date('Y-m-d H:i:s');
			//check gift purchased or not

			$sql="select  user_id  from giftsexchanged  where item_id='".$params["item_id"]."' and receiver_id='".$_SESSION["user_id"]."' and status='0'  order by id desc";
            $giftdetail=$db->fetchRow($sql);
		    if($giftdetail->user_id!='')
			{   
				$where = array("user_id"=>$params["sender_id"], "item_id"=> $params["item_id"],"receiver_id"=>$_SESSION["user_id"]);										
				$update_gift_status=array("received_date"=>$datetime,"status"=>'1');			
				$db->update('giftsexchanged',$update_gift_status,array("user_id = ?" => $params["sender_id"], "item_id = ?" => $params["item_id"], "receiver_id=?" =>$_SESSION["user_id"])); 	

			}else{
				$itemDetails = wishlistitmes::getProductDetail($params["item_id"]);
                $purchased_gift=array("item_id"=>$params["item_id"],"amount"=>number_format($itemDetails[0]->item_price,2,'.',''),"user_id"=>0,
                    "receiver_id"=>$_SESSION["user_id"],"event_id"=>$eventId,"purchased_date"=>$add_date,'received_date' => '00/00/0000 00:00', 'status'=>'1');
                $db->insert('giftsexchanged', $purchased_gift);
                $items_required_no='0';
            }
              $db->query("UPDATE userwishlist  SET gift_state = ? WHERE user_id = ? and  item_id= ?", array(2, $_SESSION["user_id"],$params["item_id"]));
			//record activity
			$activity_data=array("activity_type"=>"Gift type","sender"=>$params["sender_id"],"receiver"=>$_SESSION["user_id"],"item_id"=>$params["item_id"], "activity_date"=>$add_date, "deleted_user"=>$params["sender_id"], 'event_id' => 0);
			$db->insert('activity', $activity_data);	
			//

			$db->commit();
            return $this->_helper->json(array('success' => true, 'msg' => 'Gift received!'));
		}
		catch(Exception $e)
		{
			//Rollback transaction
            $db->rollBack();
            return $this->_helper->json(array('success' => false, 'msg' =>  $e->getMessage()));
		}
	}

        // gift as received
        public function giftunreceivedAction(){

                $db = Zend_Registry::get("db");
                $config = Zend_Registry::get("config");
                $server = Zend_Registry::get("server");

                $params = $this->getRequest()->getParams();
                try
                {
                        $db->beginTransaction();
                        $db->query("UPDATE userwishlist  SET gift_state = ? WHERE user_id = ? and  item_id= ?", array(3, $_SESSION["user_id"],$params["item_id"]));
                        $db->commit();
                        return $this->_helper->json(array('success' => true, 'msg' => 'Gift unreceived!'));
                }
                catch(Exception $e)
                {
                        //Rollback transaction
            $db->rollBack();
            return $this->_helper->json(array('success' => false, 'msg' =>  $e->getMessage()));
                }
        }

	
	// get going users list	
	public function goingusersAction(){
	
		$db = Zend_Registry::get("db");
		$config = Zend_Registry::get("config");
		$server = Zend_Registry::get("server");	
			
		$params = $this->getRequest()->getParams();		
		$event_id=$params["event_id"];
		$add_date=date('Y-m-d H:i:s');
		
		$this->view->event_id = $event_id;
		
		$sql=$db->query("SELECT invited_user from eventrsvp where RSVP_status='1' AND event_id='".$event_id."'");
		$going_users=$sql->fetchAll();	
		
		try{		
			$db->beginTransaction();							
			if(!empty($going_users))
			{
				for($i=0; $i < count($going_users); $i++)
				{
					$list = array();
					$going_users_details = users::getUserDetails($going_users[$i]->invited_user);
					$list['user_id'] = $going_users_details->user_id;
					$list['username'] = $going_users_details->first_name.' '.$going_users_details->last_name;
					$list['profile_pic'] = $going_users_details->profile_pic;
					
					$result[] = $list;
				}
				$this->view->going_user_list = $result;
			}
			
			$db->commit();	 				
			
		}
		catch(Exception $e)
		{
			//Rollback transaction
			$this->view->errors[] = $e->getMessage();
			$info["error"] = $e->getMessage();	
		}
		
	}
	
	// get maybe users
	public function maybeuserAction(){
	
		$db = Zend_Registry::get("db");
		$config = Zend_Registry::get("config");
		$server = Zend_Registry::get("server");	
			
		$params = $this->getRequest()->getParams();		
		$event_id=$params["event_id"];
		$back=$params["by"];
		$this->view->by = $back;
		$this->view->event_id = $event_id;
		
		$add_date=date('Y-m-d H:i:s');
		
		$sql=$db->query("SELECT invited_user from eventrsvp where RSVP_status='2' AND event_id='".$event_id."'");
		$going_users=$sql->fetchAll();	
		
		try{		
			$db->beginTransaction();							
			if(!empty($going_users))
			{
				for($i=0; $i < count($going_users); $i++)
				{
					$list = array();
					$going_users_details = users::getUserDetails($going_users[$i]->invited_user);
					$list['user_id'] = $going_users_details->user_id;
					$list['username'] = $going_users_details->first_name.' '.$going_users_details->last_name;
					$list['profile_pic'] = $going_users_details->profile_pic;
					
					$result[] = $list;
				}
				$this->view->going_user_list = $result;
			}
			
			$db->commit();	 				
			
		}
		catch(Exception $e)
		{
			//Rollback transaction
			$this->view->errors[] = $e->getMessage();
			$info["error"] = $e->getMessage();	
		}
		
	}
	
	// get declined users
	public function declineduserAction(){
	
		$db = Zend_Registry::get("db");
		$config = Zend_Registry::get("config");
		$server = Zend_Registry::get("server");	
			
		$params = $this->getRequest()->getParams();		
		$event_id=$params["event_id"];
		$back=$params["by"];
		$this->view->by = $back;
		
		$add_date=date('Y-m-d H:i:s');
		
		$sql=$db->query("SELECT invited_user from eventrsvp where RSVP_status='0' AND event_id='".$event_id."'");
		$going_users=$sql->fetchAll();	
		$this->view->event_id = $event_id;
		
		try{		
			$db->beginTransaction();							
			if(!empty($going_users))
			{
				for($i=0; $i < count($going_users); $i++)
				{
					$list = array();
					$going_users_details = users::getUserDetails($going_users[$i]->invited_user);
					$list['user_id'] = $going_users_details->user_id;
					$list['username'] = $going_users_details->first_name.' '.$going_users_details->last_name;
					$list['profile_pic'] = $going_users_details->profile_pic;
					
					$result[] = $list;
				}
				$this->view->going_user_list = $result;
			}
			
			$db->commit();	 				
			
		}
		catch(Exception $e)
		{
			//Rollback transaction
			$this->view->errors[] = $e->getMessage();
			$info["error"] = $e->getMessage();	
		}
		
	}
	
	// get invited users
	public function inviteduserAction(){
	
		$db = Zend_Registry::get("db");
		$config = Zend_Registry::get("config");
		$server = Zend_Registry::get("server");	
			
		$params = $this->getRequest()->getParams();		
		$event_id=$params["event_id"];
		$add_date=date('Y-m-d H:i:s');
		
		$back=$params["by"];
		$this->view->by = $back;
		$this->view->event_id = $event_id;
		
		$sql=$db->query("SELECT invited_user from eventrsvp WHERE event_id='".$event_id."'");
		$going_users=$sql->fetchAll();	
		
		try{		
			$db->beginTransaction();							
			if(!empty($going_users))
			{
				for($i=0; $i < count($going_users); $i++)
				{
					$list = array();
					$going_users_details = users::getUserDetails($going_users[$i]->invited_user);
					$list['user_id'] = $going_users_details->user_id;
					$list['username'] = $going_users_details->first_name.' '.$going_users_details->last_name;
					$list['profile_pic'] = $going_users_details->profile_pic;
					
					$result[] = $list;
				}
				$this->view->going_user_list = $result;
			}
			
			$db->commit();	 				
			
		}
		catch(Exception $e)
		{
			//Rollback transaction
			$this->view->errors[] = $e->getMessage();
			$info["error"] = $e->getMessage();	
		}
		
	}
	
	// gift purchase by user for friends
	public function giftgivenAction(){
		$db = Zend_Registry::get("db");
		$config = Zend_Registry::get("config");
		$server = Zend_Registry::get("server");	
		
                if ($_SESSION['logged_in'] != true) {
                      $this->_redirect(PS_Util::redirect("https://" . $server->apphost));
                      exit;
                }
	
		$params = $this->getRequest()->getParams();
		$user_id = $params['user_id'];
		try{
			$items = event::giftGiven($user_id);
			$this->view->items = $items;
			
		}catch(Exception $e)
		{
			print_r($e->getMessage());
			exit;
		}
	}
	
	public function giftgetAction(){
		
		$db = Zend_Registry::get("db");
		$config = Zend_Registry::get("config");
		$server = Zend_Registry::get("server");	
			
		$params = $this->getRequest()->getParams();
		$user_id = $params['user_id'];
		try{
			$items = event::giftRecived($user_id);
			$this->view->items = $items;
			
			
		}catch(Exception $e)
		{
			print_r($e->getMessage());
		}
	}

    public function eventslistAction(){
        $config = Zend_Registry::get("config");
        $server = Zend_Registry::get("server");
        $logger = Zend_Registry::get("logger");

        $this->_helper->layout()->disableLayout();
        $this->view->events = event::getEventDate($_SESSION["user_id"]);
    }

    public function addeventcommentAction(){
        $db = Zend_Registry::get("db");

		$this->view->headTitle(PS_Translate::_("Add Event"));
        $this->_helper->layout()->disableLayout();
        $comment = $this->_getParam("comment");
        $event_id = $this->_getParam("event_id");
        try{

            $db->beginTransaction();
            if($comment){
                $comment_date=date("Y-m-d H:i:s");
                $insertComment=array("user_id"=>$_SESSION["user_id"],"event_id"=>$event_id,"comments"=>$comment,"comment_date"=>$comment_date);
                $db->insert("eventcomments",$insertComment);
                $db->commit();
                $comments = event::getAllEventComments($event_id);
                $this->view->event_comments_count = count($comments);
                $this->view->userDetail = users::getUserDetails($_SESSION['user_id']);
                $this->view->date = $comment_date;
                $this->view->comment = $comment;
                $this->render('eventcomment');
            } else {
                return $this->_helper->json(array('fail' => true, 'msg' => 'Please instert your comment.'));
            }

        }catch(Exception $e)
        {
            $db->rollBack();
            return $this->_helper->json(array('fail' => true, 'msg' => $e->getMessage()));
        }
    }

    public function eventcommentsAction() {
        //disable layouts for this action
        $this->_helper->layout()->disableLayout();

        $event_id = $this->getRequest()->getParam("eventId", false);
        $next_start = $this->getRequest()->getParam("next_start", false);
        // Set the value for view to use it
        $this->view->event_comment_offset = $next_start;
        $this->view->number_of_comments_displayed = 4;

        //comments
        try {
            $comments = event::getAllEventComments($event_id);
            $this->view->event_comments = $comments;
            $this->view->event_comments_count = count($comments);
        } catch(Exception $e){
            return $this->_helper->json(array('fail'=>true, 'msg'=> $e->getMessage()));
        }
    }

    public function displaydayeventsAction()
    {
        //disable layouts for this action
        $this->_helper->layout()->disableLayout();
        $day = $this->getRequest()->getParam("day", false);
        try {
            $this->view->events = event::gettodayevents($_SESSION["user_id"],date("Y-m-d", strtotime(str_replace('_','/',$day))), '');
        } catch(Exception $e){
        }
       
        $this->view->is_events_month = false;
        $this->render('eventslist');
    }

    public function displaymontheventsAction()
    {
        //disable layouts for this action
        $this->_helper->layout()->disableLayout();
        $month = $this->getRequest()->getParam("month", false);
        $year = $this->getRequest()->getParam("year", false);
        try {
            $events = event::getmonthevents($_SESSION["user_id"],$month,$year, '');
            $this->view->events = $events;
        } catch(Exception $e){
        }
        $this->view->events = $events;
        $this->view->is_events_month = true;
        $this->render('eventslist');
    }

    public function filterbyeventlistAction()
    {
        //disable layouts for this action
        $this->_helper->layout()->disableLayout();
        try{
            $this->view->filter_bar_events = event::getUserCreatedEvents($_SESSION["user_id"]);
        } catch(Exception $e){
            return $this->_helper->json(array('fail'=>true, 'msg'=> $e->getMessage()));
        }
    }

    public function itemeventlistAction(){
        //disable layouts for this action
        $this->_helper->layout()->disableLayout();
        $itemId = $this->getRequest()->getParam("item_id", false);

        try{
            $this->view->events = event::getItemEvents($_SESSION["user_id"], $itemId);
        } catch(Exception $e){
            return $this->_helper->json(array('fail'=>true, 'msg'=> $e->getMessage()));
        }
    }

    public function viewtutorialAction(){
        $db = Zend_Registry::get("db");

        //disable layouts for this action
        $this->_helper->layout()->disableLayout();
        $page = $this->getRequest()->getParam("page", false);

        try{
            $db->beginTransaction();
            $userDetail = users::getUserDetails($_SESSION['user_id']);

            if ($page == 'home') {
               $db->query("UPDATE users  SET tutorial = ? WHERE user_id = ?", array($userDetail->tutorial | 1, $_SESSION["user_id"]));
            } else  if ($page == 'feed') {
               $db->query("UPDATE users  SET tutorial = ? WHERE user_id = ?", array($userDetail->tutorial | 2, $_SESSION["user_id"]));
            } else {
               $db->query("UPDATE users  SET tutorial = ? WHERE user_id = ?", array($userDetail->tutorial, $_SESSION["user_id"]));
            }
            $db->commit();
        } catch(Exception $e){
            return $this->_helper->json(array('fail'=>true, 'msg'=> $e->getMessage()));
        }
        return $this->_helper->json(array('success'=>true, 'msg'=> $e->getMessage()));
    }

	public function eventcurateAction()
	{
		$logger = Zend_Registry::get("logger");

		$params=$this->getRequest()->getParams();
		$success = true;
		try {
			if($params['event_id'] != null) {
				$msg = event::eventcuratorcuratedinfo($params['event_id'], $params['description']);
			}
		} catch( Exception $e) {
			$msg = $e->getMessage();
			$success = false;
		}

		return $this->_helper->json(array('success' => $success, 'msg' => $msg));
	}

        public function eventcuratetitleAction()
        {
                $logger = Zend_Registry::get("logger");

                $params=$this->getRequest()->getParams();
                $success = true;
                try {
                        if($params['event_id'] != null) {
                                $msg = event::eventcuratorcuratedtitle($params['event_id'], $params['title']);
                        }
                } catch( Exception $e) {
                        $msg = $e->getMessage();
                        $success = false;
                }

                return $this->_helper->json(array('success' => $success, 'msg' => $msg));
        }

         public function eventcuratetagsAction()
        {
                $logger = Zend_Registry::get("logger");

                $params=$this->getRequest()->getParams();
                $logger->info("Chikke ".$params['event_id'] . " ". $params['description']);
                $success = true;
                try {
                        if($params['event_id'] != null) {
                                $msg = event::eventcuratorcuratedtags($params['event_id'], $params['description']);
                        }
                } catch( Exception $e) {
                        $msg = $e->getMessage();
                        $success = false;
                }

                return $this->_helper->json(array('success' => $success, 'msg' => $msg));
        }

}//class
