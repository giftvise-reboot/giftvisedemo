<?php
/*------------------------------------------------------------------ 
 * Copyright (c) 2013-2014 by Giftvise, Inc.
 * All rights reserved. 
 *------------------------------------------------------------------
 */

class ProductsController extends PS_Controller_Action
{
	public function preDispatch()
	{		
		$this->view->headTitle(PS_Translate::_("Wishlist"));
	}
	
	public function indexAction()
	{
	
	}
	
	public function addAction()
	{
		$db = Zend_Registry::get("db");
		$config = Zend_Registry::get("config");
		$server = Zend_Registry::get("server");
		
		$categories = category::categoryList();
		$this->view->categories = $categories;
		
		//$this->_helper->viewRenderer("index");
		//$this->_helper->resources("index");

		$this->_init();
		$this->errorOnNoscript ();
	}//add

         public function addproducturlAction()
        {
            $db = Zend_Registry::get("db");
            $config = Zend_Registry::get("config");
            $server = Zend_Registry::get("server");
            $logger = Zend_Registry::get("logger");

            $params = $this->getRequest()->getParams();
            $this->_helper->layout()->disableLayout();
            $logger->info("Add from - product url:". $params['producturl']);
            $url = str_replace("%3C%3E", "<>",$params['producturl']);
            $logger->info("Add from - product url:" . $url);
            $page = file_get_contents($url);
            $page = str_replace("\n", '', $page);
            $page = str_replace('http:', 'http:', $page);
             if (strlen($page) > 0 ) {
                 $this->view->page = $page;
             } else {
                 $this->_helper->json(array('fail' => true, 'msg' =>"failed"));
             }
        }	

	public function addproductAction()
	{
            $vendor_link_for_add_manual_items = 'http://www.amazon.com/s?url=search-alias&field-keywords=';
            $db = Zend_Registry::get("db");
            $config = Zend_Registry::get("config");
            $server = Zend_Registry::get("server");
            $logger = Zend_Registry::get("logger");

            $params = $this->getRequest()->getParams();
            $this->_init();

            $categories = category::categoryList();
            $this->view->categories = $categories;

            $data['user_id'] = $_SESSION['user_id'];
            $data['item_name'] = addslashes($params['name']);
            $data['description'] = addslashes($params['description']);
            $data['item_price'] = $params['price'];
            $data['item_count'] = 1;
            $data['state'] = 0;
            //$data['item_type'] = $params['category'];
            if ($params['added_from'] == '') {
                $data['added_from'] = 'manual';
            } else {
                $data['added_from'] = $params['added_from'];
            }
            $data['added_date'] = date('Y-m-d H:i:s');
            if ($params['currency'] !=  '') {
               $data['item_price_currency'] = $params['currency'];
            }
            try{
                $db->beginTransaction();
                //$this->fields->validate();

                $pname = $db->quote($params['name']);
                $chkitem = 0; //wishlistitmes::chkExistItem($pname);
                
                if ($chkitem) {
                    $msg = "This Product is already added.";
                } else {
                        //move image
                        $upload = new fileupload();
                        if ($upload->moveImageFromTemp('productimg', $params['image'])){
                            $data['image'] = $params['image'];
                        } else {   
                                 if($params['imageurl']  != '') {
                                          $date = date("Y.m.d");
                                          $time = time();
                                          $image_name = $_SESSION['user_id'] . "_" . $params['price'] . "_" . "manual".$date.$time;
                                          $target_path= $_SERVER["DOCUMENT_ROOT"]."/productimg/".$image_name;
                                          $thumb_path = $_SERVER["DOCUMENT_ROOT"]."/productimg/thumb/".$image_name;
                                          $ret = file_put_contents($target_path, file_get_contents($params['imageurl']));
                                          if ($ret != NULL && $ret != FALSE) {
                                                  copy($target_path, $thumb_path);
                                                  $image = new SimpleImage();
                                                  $image->load($target_path);
                                                  // resize big image
                                                  $size = getimagesize($target_path);
                                                  $width = $size[0];
                                                  $height = $size[1];
                                                  if($width > 620)
                                                  {
                                                      $image->resizeToWidth(620);
                                                      $image->save($target_path);
                                                   }else{
                                                      if($width < 197)
                                                      {
                                                             $image->resizeToWidth(197);
                                                             $image->save($target_path);
                                                      } else {
                                                              $image->save($target_path);
                                                      }
                                                   }

                                                   $image->resizeToWidth(197);
                                                   $image->save($thumb_path);
                                                   $data['image'] = $image_name; 
                                            } else {
                                                   $data['image'] = 'no-image-available';
                                                   $msg = "Failed to read image from the specified URL";
                                            }
                                } else {
                                         //$msg = "Failed to upload file please try again";
                                         $msg = '';
                                         $data['image'] = 'no-image-available';
                                }
                    }
                    if($msg == '') {
                        $data['item_type'] = '1';
                        if ($params['producturl'] == '') {
                            $data['vendor_link'] = $vendor_link_for_add_manual_items . $params['name'];
                        } else {
                            $data['vendor_link'] = $params['producturl'];
                        }
                        $data['vendor_link'] = $data['vendor_link'].'&tag='.$params['url_tag'];
                        $data['isbn'] = '';
                        $data['barcode_itemid'] = '';
                        $db->insert('wishlistitmes', $data);
                        $item_id = $db->lastInsertId();

                        if ($item_id != '') {
                            $dataW['user_id'] = $_SESSION['user_id'];
                            $dataW['item_id'] = $item_id;
                            $dataW['added_date'] = $data['added_date'];
                            $dataW['sharewith_group'] = '';
                            $dataW['share_individual'] = '';
                            $dataW['reserved_by'] = 0;
                            $dataW['gift_state'] = 0;
                            if ($params['privacy'] == '') {
                                $dataW['public'] = 1;
                            } else {
                                $dataW['public'] = $params['privacy'];
                            }
                            $db->insert('userwishlist', $dataW);

                            $msg = "Product has been added successfully";
                            $success = true;

                            //add tags
                            $user_id = $_SESSION['user_id'];

                            if (trim($params['txt_tags']) != '') {
                                $tagsArr = explode(",", trim($params['txt_tags']));
                                if (count($tagsArr) > 5 ) {
                                    $tlimit = 5;
                                } else {
                                    $tlimit = count($tagsArr);
                                }
                                for($t = 0; $t < $tlimit; $t++) {
                                    if(trim($tagsArr[$t]) != '') {
                                        if (strlen($tagsArr[$t]) > 25) {
                                            $tagNames = substr($tagsArr[$t],0,25);
                                        } else {
                                            $tagNames = $tagsArr[$t];
                                        }

                                        $dataT = array();
                                        $dataT['name'] = $tagNames;
                                        $dataT['created_by'] = $user_id;
                                        $dataT['status'] = 1;

                                        $checkTagID = category::checkTags($tagsArr[$t], $user_id);
                                        if ($checkTagID != '') {
                                            // already added
                                            $tag_id = $checkTagID;
                                        } else {
                                            // add
                                            $db->insert('category',$dataT);
                                            $tag_id =  $db->lastInsertId();
                                        }
                                        $dataIT['item_id'] = $item_id;
                                        $dataIT['tag_id'] = $tag_id; // tag_id is the category id
                                        $dataIT['user_id'] = $user_id;
                                        $dataIT['tag_date'] = $data['added_date'];

                                        $checkID = category::checkItemTags($item_id, $tag_id, $user_id);

                                        if ($checkID != '') {
                                            // do nothing
                                        } else {
                                            // tag item
                                            $db->insert('item_tags', $dataIT);
                                        }
                                    } //if
                                }// for
                            //
                            }
                        } else {
                            $msg = "Error! in adding product, please try again.";
                        }
                        $db->commit();
                        $logger->info("--- ACCESS-INFO --- ::MANUAL-ADD::USER-ID::"    
                                      .$_SESSION['user_id']. "::DEVICE-INFO:: ".$_SERVER['HTTP_USER_AGENT'] . 
                                      "IP ADDRESS ". $_SERVER['REMOTE_ADDR'] ."\n");        
                    }
                } // else
            } catch(Exception $e) {
                //Rollback transaction
                $db->rollBack();
                //print_r($e->getMessage());
                //exit;
                $msg = $e->getMessage();
            }
            return $this->_helper->json(array('success' => ($success) ? true : false, 'msg' => $msg));
        }//add
	
	protected function _init()
	{		
		$productname = new PS_Validate_NotEmpty($this->_getParam("name"));
		$productname->setMessage(PS_Translate::_("Please enter product's name"));
		
		$price = new PS_Validate_NotEmpty($this->_getParam("price"));
		$price->setMessage(PS_Translate::_("Please enter price"));
		
		$category = new PS_Validate_NotEmpty($this->_getParam("category"));
		$category->setMessage(PS_Translate::_("Please select category"));
		
		$description = new PS_Validate_NotEmpty($this->_getParam("description"));
		$description->setMessage(PS_Translate::_("Please enter description"));
		  
		$this->fields = new PS_Form_Fields(
			array(		
				"name" => array(
					$productname,
					"required" => true
				),				
				
				"price" => array(
					array($price, new PS_Validate_Currency()),
					"required" => true
				)
				/*"category" => array(
					$category,
					"required" => true
				),
				"description" => array(
					$description,
					"required" => true
				)*/
			)
		);
		$params = $this->getRequest()->getParams();
		$this->fields->setData($params);
		$this->view->fields 		= $this->fields;
		
	}///end function
	
	public function productdetailAction()
	{
		$db = Zend_Registry::get("db");
		$config = Zend_Registry::get("config");
		$server = Zend_Registry::get("server");
	        $logger = Zend_Registry::get("logger");
	
		$params = $this->getRequest()->getParams();
		$product_id = $params['pid'];
		//$this->view->buyfr = $params['buyfr'];
		//$this->view->by = $params['by'];
		
		try{
		///product
		$productDetail = wishlistitmes::getProductDetail($product_id);	
		
		//print_r($productDetail);	
		$this->view->productDetail = $productDetail[0];

		$userwished = wishlistitmes::countWished($product_id);
		$this->view->userwished = $userwished;
		
		$itemwhished = wishlistitmes::chkWishedItem($product_id,$_SESSION['user_id']);
		
		if($itemwhished)
		{
			$this->view->itemwhished = 'Y';
		}else{
			$this->view->itemwhished = 'N';
		}		
		/// added by latest friend  
		//$latestfrnd = wishlistitmes::latestItemUser($product_id, $_SESSION['user_id']);
		$latestfrnd = wishlistitmes::latestItemHome($product_id, $_SESSION['user_id']);
		$altestAddedfrnd = $latestfrnd['user_id'];
		$this->view->receiverfriend = $altestAddedfrnd;
		
		///
		
		/// latest item
		$timeAdded = wishlistitmes::latestItem($product_id);
		$this->view->timeAdded = $timeAdded['time'];
		$addeduser = users::getUserDetails($timeAdded['user_id']);
		
		$this->view->addedby = $addeduser->first_name.' '.$addeduser->last_name;
		$this->view->addedbyuserID = $addeduser->user_id;
		
		if($addeduser->last_name != '')
		{
			$this->view->addeduserpic = $addeduser->profile_pic;
		}else{
		}
		
		//user
		$user_details = users::getUserDetails($_SESSION['user_id']);
		$this->view->userDetail = $user_details;
		
		//comments
		$comments = itemcomments::showComments($product_id);
		$this->view->comments = $comments;
		
		/// Get list of tags which are created by logged in user
		$usertags = category::usersTags($_SESSION['user_id']);
		$this->view->usertags = $usertags;
		//print_r($usertags);
		
		//get item's taglist
		$taglist = category::getItemTags($product_id, $_SESSION['user_id']);
		$this->view->taglist = $taglist;
		// is buy gift for event
		$event_id = $params['event_id'];
		
		if($event_id != ""){
			$receiver_id = event::getEventCreatedUserIDByEventID($event_id);			
			$is_buyed = event::isbuyGiftForEvent($_SESSION["user_id"],$product_id,$receiver_id,$event_id);
			
			if($is_buyed){			
				$this->view->is_buyed='Yes';
			}
			else{
				$this->view->is_buyed='No';
			}
			$this->view->event_id=$event_id;
		}else{
			if($params['rceiver'] != '')
			{
				$receiver_id = $params['rceiver'];
			}else{
				$receiver_id = $altestAddedfrnd;
			}
			$is_buyed = event::isbuyGiftItem($_SESSION["user_id"],$product_id,$receiver_id);
			$quantity = wishlistitmes::getQty($product_id,$receiver_id);
			if($is_buyed && $quantity == 0){			
				$this->view->is_buyed='Yes';
			}
			else{
				$this->view->is_buyed='No';
			}
		}
		
		/// Get Quantity
		
		if($event_id !=""){
			$quantity = wishlistitmes::getQty($product_id,$receiver_id);
		}
		else if($params['rceiver'] != '')
		{
			$quantity = wishlistitmes::getQty($product_id,$params['rceiver']);
		}
		else{
			//$quantity = wishlistitmes::getQty($product_id,$_SESSION['user_id']);
			$quantity = wishlistitmes::getQty($product_id,$altestAddedfrnd);
		}
		$this->view->quantity = $quantity;
		
		// check received any gift for any event
		$is_received=event::checkReceivedGift($_SESSION["user_id"],$product_id);
		
		if($is_received){
			$this->view->is_received = $is_received;
		}
		
		$is_frienditem = usersfriend::chkFriendItem($product_id, $_SESSION["user_id"]);
		
		if($is_frienditem){
			$this->view->is_frienditem = 'Y';
		}else{
			$this->view->is_frienditem = 'N';
		}
		
		if($params['latuser'] != '')
		{
			$this->view->latestaddeduser = $params['latuser'];
			
			/// from home it 
			//check it is current user's item
			if($params['latuser'] == $_SESSION['user_id'])
			{
				$this->view->myitem = 'Y';
			}else{
				$this->view->myitem = 'N';
			}
			
			// check is latest user is friend of current user
			$isfriendlatestuser = usersfriend::checkExistfollower($_SESSION['user_id'], $params['latuser']);
			
			if($isfriendlatestuser)
			{
				if($isfriendlatestuser->status == 1)
				{
					$this->view->isfriendlatestuser = 'Y';
				}else{
					$this->view->isfriendlatestuser = 'N';
				}
			}else{
				$this->view->isfriendlatestuser = 'N';
			}
		}
		
		}catch(Exception $e){
			//Rollback transaction
			$db->rollBack();
			print_r($e->getMessage());
			$this->view->errors[] = $e->getMessage();
			exit;
			
		}
	}//
	
	public function addcommentAction()
	{
		$db = Zend_Registry::get("db");
		$config = Zend_Registry::get("config");
		$server = Zend_Registry::get("server");
		
		$params = $this->getRequest()->getParams();

		//user
		$user_details = users::getUserDetails($_SESSION['user_id']);
		$this->view->userDetail = $user_details;
		
		try{
			$db->beginTransaction();
			if($params['comment'] != '')
			{
				$data['item_id'] = $params['product_id'];
				$data['user_id'] = $_SESSION['user_id'];
				$data['comments'] = $params['comment'];
				$data['comment_date'] = date('Y-m-d H:i:s');
				
				$db->insert('itemcomments',$data);
				$result = $db->lastInsertId();
				$db->commit();
			}
			
			$product_id = $params['product_id'];
			///product
			$productDetail = wishlistitmes::getProductDetail($params['product_id']);
			$this->view->productDetail = $productDetail[0];
			
			$itemwhished = wishlistitmes::chkWishedItem($params['product_id'],$_SESSION['user_id']);
			
			if($itemwhished)
			{
				$this->view->itemwhished = 'Y';
			}else{
				$this->view->itemwhished = 'N';
			}	
			
			/// latest item
			$timeAdded = wishlistitmes::latestItem($product_id);
			$this->view->timeAdded = $timeAdded['time'];
			
			$addeduser = users::getUserDetails($timeAdded['user_id']);	
			$this->view->addedby = $addeduser->first_name.' '.$addeduser->last_name;
			
			if($addeduser->last_name != '')
			{
				$this->view->addeduserpic = $addeduser->profile_pic;
			}else{
			}
			
			//
			$userwished = wishlistitmes::countWished($product_id);
			$this->view->userwished = $userwished;
			
			//comments
			$comments = itemcomments::showComments($params['product_id']);
			$this->view->comments = $comments;
			
		}catch(Exception $e){
			//Rollback transaction
			$db->rollBack();
			$this->view->errors[] = $e->getMessage();
			
		}
		$this->_helper->viewRenderer("productdetail");
		$this->_helper->resources("productdetail");
		
	}//add
	
	public function feedAction()
	{
		$db = Zend_Registry::get("db");
		$config = Zend_Registry::get("config");
		$server = Zend_Registry::get("server");
		
		$params = $this->getRequest()->getParams();
		$product_id = $params['pid'];
		
		//user
		$user_details = users::getUserDetails($_SESSION['user_id']);
		$this->view->userDetail = $user_details;
		
		///categories
		$categories = category::categoryList();
		$this->view->categories = $categories;
		
		//get products	
		$param = $this->getRequest()->getParams();
		
		try{
			$ord = $param['order'];
			$catVal = $param['item_type'];
		
		if($param['pricekey'] == 'price')
		{
			if($orderBy != '')
				$orderBy .= ', item_price ASC';
			else
				$orderBy = ' item_price ASC';
		}
		
		if($param['datekey'] == 'date')
		{
			if($orderBy != '')
				$orderBy .= ', uw.added_date DESC';
			else
				$orderBy = 'uw.added_date DESC';
		}
		
		if($param['popularityKey'] == 'popularity')
		{
			if($orderBy != '')
				$orderBy .= ', w.item_count DESC';
			else
				$orderBy = 'w.item_count DESC';
		}
		
		//$products = wishlistitmes::getAllFeedSite($_SESSION['user_id'],$orderBy,$catVal);
		$products = wishlistitmes::getFriendsFeed($_SESSION['user_id'],$orderBy,$catVal);
		
		if($catVal != '')
		{
			$catName = category::categoryName($catVal);
		}else{
			$catName = 'Category';
		}
		
		for($i=0;$i < count($products);$i++)
		{
			$item = array();
			
			$totalComments = itemcomments::countComments($products[$i]->id);
			$userwished = wishlistitmes::countWished($products[$i]->id);
			$showChk = wishlistitmes::showItem($products[$i]->id, $_SESSION['user_id'],$friend_details->user_id);
			
			/// latest item
			//$timeAdded = wishlistitmes::latestItem($products[$i]->id);
			$timeAdded = wishlistitmes::latestItemUser($products[$i]->id, $_SESSION['user_id']);
		 	$addeduser = users::getUserDetails($timeAdded['user_id']);
			
			$itemwhished = wishlistitmes::chkWishedItem($products[$i]->id,$_SESSION['user_id']);
			
			if($itemwhished)
			{
				$item['youwhished'] = 'Y';
			}else{
				$item['youwhished'] = 'N';
			}	
			
			$item['comment'] = $totalComments;
			$item['wished'] = $userwished;
			$item['added_time'] = $timeAdded['time'];
			//$item['addedby'] = $addeduser->first_name.' '.$addeduser->last_name;
			$item['first_name'] = $addeduser->first_name;
			$item['last_name'] = $addeduser->last_name;
			$item['user_id'] = $addeduser->user_id;
			$items[] = $item;
		}
		//print_r($items);
		//exit;
		$this->view->items = $products;
		$this->view->extval = $items;
		$this->view->categoryName = $catName;
		
		}catch(Exception $e){
			//Rollback transaction
			//print_r($e->getMessage());
			//exit;
			$this->view->errors[] = $e->getMessage();
		}
	}//
	
	public function addwishlistAction()
	{
		$db = Zend_Registry::get("db");
		$config = Zend_Registry::get("config");
		$server = Zend_Registry::get("server");
	        $logger = Zend_Registry::get("logger");
	
		$param = $this->getRequest()->getParams();
		
		$data['item_id'] = $param['item_id'];
		$data['user_id'] = $param['user_id'];
		$data['added_date'] = date('Y-m-d H:i:s');

		$datai = array(
			'item_count'      => new Zend_DB_Expr('item_count + 1')
		);

		try{
			$db->beginTransaction();
			
			$chkWished = wishlistitmes::chkWishedItem($param['item_id'],$param['user_id']);
						
			if(!$chkWished)
			{
				$db->insert('userwishlist',$data);
				$result = $db->lastInsertId();
				$db->update('wishlistitmes',$datai,'id='.$param['item_id']);
				if($result)
				{				
					//$msg = 'Item is added in your wishlist';
				}else{
					$msg = 'Error! in adding';
				}
			}
		}catch(Exception $e){
			//Rollback transaction
			$db->rollBack();
			$this->view->errors[] = $e->getMessage();
		}
		exit;
	}
	public function deletewishlistAction()
	{
		$db = Zend_Registry::get("db");
		$config = Zend_Registry::get("config");
		$server = Zend_Registry::get("server");
		
		$param = $this->getRequest()->getParams();
		
		
		$datai = array(
			'item_count'      => new Zend_DB_Expr('item_count - 1')
		);

		try{
			$db->beginTransaction();
			
			$res = $db->delete('userwishlist', array("user_id = ?" => $param['user_id'], "item_id = ?" => $param['item_id']));
			
			$db->update('wishlistitmes',$datai,'id='.$param['item_id']);
			
		
			// delete items from item_tags table, if item exits  
			$res = $db->delete('item_tags', array("user_id = ?" => $param['user_id'], "item_id = ?" =>$param['item_id']));
			
			// delete items from taggifts table, if item exits  
			$sql=$db->query("SELECT event_id,item_id FROM taggifts WHERE FIND_IN_SET('".$param['item_id']."',item_id) AND user_id='".$param['user_id']."'");
			$tag_item_id=$sql->fetchAll();
			if(count($tag_item_id)>0){
				foreach($tag_item_id as $remove_item_id){
					$item_id =explode(",",$remove_item_id->item_id);
					foreach($item_id as $i_id){
						if($i_id !=$param['item_id']){
							$update_item_id[]=$i_id;
						}											
					}	
					if(count($update_item_id)>0){
						$update_taggifts=array("item_id"=>implode(",",$update_item_id));
						$db->update('taggifts',$update_taggifts,'event_id='.$remove_item_id->event_id);						
						}
						else{
						$where = array("event_id = ?" => $remove_item_id->event_id);	
						$db->delete("taggifts",$where);
					}
					unset($update_item_id);					
				}
			}
			
			$isWished = wishlistitmes::anyUserWished($param['item_id']);
			if($isWished)
			{
				// do nothing
			}else{
				// delete from master table
				$detail = wishlistitmes::getProductDetail($param['item_id']);
				/// Delete images
				if($detail[0]->image != '')
				{
					//unlink($_SERVER["DOCUMENT_ROOT"]."/productimg/".$detail[0]->image);
					//unlink($_SERVER["DOCUMENT_ROOT"]."/productimg/thumb/".$detail[0]->image);
				}
				//
				//$res = $db->delete('wishlistitmes', array("id = ?" => $param['item_id']));
			}
			
			$products = wishlistitmes::getAllProducts($_SESSION['user_id'],'','');

			$result = '<ul>';
			if(count($products) > 0)
			{
				for($i=0;$i < count($products);$i++)
				{
					$item = array();
					
					$totalComments = itemcomments::countComments($products[$i]->item_id);
					$userwished = wishlistitmes::countWished($products[$i]->item_id);
					
					/// latest item
					$timeAdded = wishlistitmes::latestItem($products[$i]->item_id);
					$addeduser = users::getUserDetails($timeAdded['user_id']);
					/// check item is received or not
					$ispurchased = event::isReceiveGiftForUser($_SESSION['user_id'], $products[$i]->item_id);			
					$quantity = wishlistitmes::getQty($products[$i]->item_id,$_SESSION['user_id']);
					/*if($ispurchased && $quantity == 0)
					{
						$item['isuserpurchased']='Y';
					}else{ 
						$item['isuserpurchased']='N';
					}*/
					
					if($ispurchased)
					{
						if($quantity == 0)
						{
							$item['isuserpurchased']='Y';
						}else{
							$item['isuserpurchased']='N';
						}
					}else{ 
						$sql="select  user_id  from giftsexchanged  where item_id='".$products[$i]->item_id."' and receiver_id='".$_SESSION["user_id"]."' and status='0'  order by id desc";
						$giftdetail=$db->fetchRow($sql);    
						if($giftdetail->user_id!='')
						{
							$item['isuserpurchased']='N';
						}else{
							if($quantity == 0)
								$item['isuserpurchased']='Y';
							else
								$item['isuserpurchased']='N';
						}
					}
			
					$item['comment'] = $totalComments;
					$item['wished'] = $userwished;
					$item['added_time'] = $timeAdded['time'];
					$item['addedby'] = $addeduser->first_name.' '.$addeduser->last_name;
					$items[] = $item;
					
					////
					if($products[$i]->item_id != '')
					{
		
			$result .= '<li class="ab"><div class="cross"></div> 
						<div class="mosaic-block bar" >  
							<div class="details mosaic-overlay">
							 <p style="float:left; width:96px;">';
			 
			  
				if(strlen($products[$i]->item_name) > 14 )
				{
					$name = substr($products[$i]->item_name,0,14).'';
				}else{
					$name =  $products[$i]->item_name;
				}	
			 
		$result .= $name . ' </p>	           <p style="float:right; width:35px;"> $'.wishlistitmes::format_price($products[$i]->item_price).'</p>
			 
		  </div>
		  <a href="javascript:void(0);" class="cross" onClick="deletewish('.$products[$i]->item_id.','.$_SESSION['user_id'].')">&nbsp;</a>
			 <div class="mosaic-backdrop" >';
			 
			 if($products[$i]->image != '')
			 {
			 	//if($ispurchased && $quantity == 0)
				if($item['isuserpurchased'] == 'Y')
				{
					$result .= '<img src="http://'.$server->apphost.'/productimg/thumb/'.$products[$i]->image.'" alt="" class="practive" width="175">';
				}else{
					$result .= '<a class="iframe " href="http://'.$server->apphost.'/products/productdetail?pid='.$products[$i]->item_id.'" title="" id="a_'.$products[$i]->item_id.'">
						<img src="http://'.$server->apphost.'/productimg/thumb/'.$products[$i]->image.'" alt="" id="img_'.$products[$i]->item_id.'" width="175">
					</a>';
				}
			
			 }else{
			 //	if($ispurchased && $quantity == 0)
			 	if($item['isuserpurchased'] == 'Y')
				{
					$result .= '<img src="http://'.$server->apphost.'/productimg/no-img.png" alt="" width="175">';
				}else{
					$result .= '<a class="iframe " href="http://'.$server->apphost.'/products/productdetail?pid='.$products[$i]->item_id.'" title="" id="a_'.$products[$i]->item_id.'">
			 		<img src="http://'.$server->apphost.'/productimg/no-img.png" alt="" id="img_'.$products[$i]->item_id.'" width="175">
				 </a>';
				}
				
			 
			 }
			 
			$result .= '</div>
			 
		  </div>		  
		  <div class="details">
		  
		<span class="member"style="padding-top:5px; padding-left:0;" ><img src="http://'.$server->apphost.'/images/pin1-price-b.png" alt="#" align="center" style="margin-left:9px; cursor: auto;"  height="20" /><span>'.$userwished.'</span></span>
		 <span class="Chat" style="width:40px; padding-top:6px;"><img src="http://'.$server->apphost.'/images/pin1-comments.png" height="19" alt="#" align="center" style="margin-left:8px;" /><span>'.$totalComments.'</span></span>';
		 //* add share option in list
		$usersgroups = groups::groupList($_SESSION['user_id']);
		$followingList = usersfriend::getFollowList($_SESSION['user_id']);

		$shareStr = '';
		$shareStr = '<span class="multiDrop">
                <a href="javascript:void(0)"><img src="http://'.$server->apphost.'/images/more.png"  alt=""></a>
                <ul class="firstLavel">';
				
				if($products[$i]->public == '1' && $products[$i]->sharewith_group == '' && $products[$i]->share_individual == '')
				{
					$shareCss =  'class="active"';
					$publiVal = 0;
				}else{
					$shareCss = '';
					$publiVal = 1;
				}
			
                $shareStr .= '<li '.$shareCss.' id="public_'.$products[$i]->item_id.'" ><a href="javascript:void(0)" onClick="sharewishlist(\'Public\', '.$products[$i]->item_id.', \'\', \'\',\'public_'.$products[$i]->item_id.'\', '.$publiVal.')">Everyone</a> </li>
                   <li><a href="javascript:void(0)" >Group</a>';
                   
				   if($dropCnt == 2)
				   {
				   	$dropCss = 'style="left:-186px;"';
					$dropCnt = 0;
				   }else{
				   	$dropCss = '';
				   	$dropCnt++;
				   		
				   }
				   $itemGroupsArr = explode(',',$products[$i]->sharewith_group);
				   $itemFriendsArr = explode(',',$products[$i]->share_individual);
				   
				   
                    $shareStr .= '<ul class="secondLavel" '.$dropCss.'> ';
                    	
						if(count($usersgroups) > 0)
				   		{					
							for($g=0; $g < count($usersgroups); $g++)
							{
						     
							if(in_array($usersgroups[$g]->id, $itemGroupsArr))
							{
								$shareCss =  'class="active"';
								$publiVal = 0;
							}else{
								$shareCss = '';
								$publiVal = 1;
							} 
                             
                            $shareStr .= '<li '.$shareCss.' id="group_'.$products[$i]->item_id.'_'.$usersgroups[$g]->id.'" >';
							
							if($usersgroups[$g]->group_image != '')
							{
							
                               $shareStr .= '<img border="0" src="http://'.$server->apphost.'/groupimg/thumb/'.$usersgroups[$g]->group_image.'" width="32" height="32">';
                            
							}else{
							
                            	$shareStr .= '<img border="0" src="http://'.$server->apphost.'/images/icon.jpg" border="0" width="32">';
                            
							}
							
							$shareStr .= '<div class="slcontent"><a href="javascript:void(0)" onClick="sharewishlist(\''.Groups.'\', \''.$products[$i]->item_id.'\', \''.$usersgroups[$g]->id.'\', \'\',\'group_'.$products[$i]->item_id.'_'.$usersgroups[$g]->id.'\','.$publiVal.')">'.$usersgroups[$g]->group_name.'</a></div>
                            </li>';                        
							}
						}else{
						
                        	$shareStr .= '<li>You have not created any group</li>';
                        
						}
                    $shareStr .= '</ul>                   
                   			</li>';
							
                   $shareStr .= '<li><a href="javascript:void(0)">Individual</a>
                        <ul class="secondLavel" id="share_'.$products[$i]->item_id.'" 
						'.$dropCss.'>';                     
                        
						if(count($followingList) > 0)
				   		{					
							for($f=0; $f < count($followingList); $f++)
							{
						        $follower_details = users::getUserDetails($followingList[$f]->friend_id);								
								
								if(in_array($follower_details->user_id, $itemFriendsArr))
								{									
									$shareCss =  'class="active"';
									$publiVal = 0;
								}else{
									$shareCss = '';
									$publiVal = 1;
								} 
								
                            $shareStr .= '<li '.$shareCss.' id="friend_'.$products[$i]->item_id.'_'.$follower_details->user_id.'" >';
							
							if($follower_details->profile_pic != '')
							{
							
								$shareStr .= '<img src="http://'.$server->apphost.'/userpics/thumb/'.$follower_details->profile_pic.'">';
							
							}else{
							
								$shareStr .= '<img src="http://'.$server->apphost.'/userpics/top-img.png">';
							
							}
							$shareStr .= '<div class="slcontent"><a href="javascript:void(0)" onClick="sharewishlist(\'Friend\', \''.$products[$i]->item_id.'\', \'\', \''.$follower_details->user_id.'\',\'friend_'.$products[$i]->item_id.'_'.$follower_details->user_id.'\', '.$publiVal.')">'.$follower_details->first_name.' '.$follower_details->last_name.'</a></div></li>';
                        
							}
						}else{
						
                        	 $shareStr .= '<li>You do not have any friend</li>';
                        
						}
						
                    $shareStr .= '</ul>
                    </li>';
					if($products[$i]->public == '0')
						$onlyChk = 'checked = "checked"'; 
					else
						$onlyChk = '';
					if($products[$i]->public == '0')
					{
						$shareCss =  'class="active"';
						$publiVal = 0;
					}else{
						$shareCss = '';
						$publiVal = 1;
					}
                    $shareStr .= '<li '.$shareCss.' id="private_'.$products[$i]->item_id.'"><a href="" onClick="sharewishlist(\'Private\', \''.$products[$i]->item_id.'\', \'\', \'\', \'private_'.$products[$i]->item_id.'\', '.$publiVal.')">Only me</a></li>              
                </ul>
            </span>';
/*$shareStr = '<span class="multiDrop">
                <a href="#"><img src="http://'.$server->apphost.'/images/more.png"  alt=""></a>
                <ul class="firstLavel">';
				
				if($products[$i]->public == '1' && $products[$i]->sharewith_group == '' && $products[$i]->share_individual == '')
					$checked = 'checked = "checked"';
				else
					$checked = ''; 
			
                $shareStr .= '<li class="flabel">Everyone <input type="checkbox" id="public_'.$products[$i]->item_id.'" value="1" onClick="sharewishlist(\'Public\', '.$products[$i]->item_id.', \'\', \'\',\'public_'.$products[$i]->item_id.'\')"   /></li>
                   <li><a href="#">Group</a>';
                   
				   if($dropCnt == 2)
				   {
				   	$dropCss = 'style="left:-186px;"';
					$dropCnt = 0;
				   }else{
				   	$dropCss = '';
				   	$dropCnt++;
				   		
				   }
				   $itemGroupsArr = explode(',',$products[$i]->sharewith_group);
				   $itemFriendsArr = explode(',',$products[$i]->share_individual);
				   
				   
                    $shareStr .= '<ul class="secondLavel" '.$dropCss.'> ';
                    	
						if(count($usersgroups) > 0)
				   		{					
							for($g=0; $g < count($usersgroups); $g++)
							{
						     if(in_array($usersgroups[$g]->id, $itemGroupsArr ))
							 	$groupChk = 'checked = "checked"'; 
							 else
							 	$groupChk = '';
                             
                            $shareStr .= '<li><input type="checkbox" id="group_'.$products[$i]->item_id.'_'.$usersgroups[$g]->id.'" value="'.$usersgroups[$g]->id.'"  onClick="sharewishlist(\''.Groups.'\', \''.$products[$i]->item_id.'\', \''.$usersgroups[$g]->id.'\', \'\',\'group_'.$products[$i]->item_id.'_'.$usersgroups[$g]->id.'\')" '.$groupChk.' />';
							
							if($usersgroups[$g]->group_image != '')
							{
							
                               $shareStr .= '<img border="0" src="http://'.$server->apphost.'/groupimg/thumb/'.$usersgroups[$g]->group_image.'" width="32" height="32">';
                            
							}else{
							
                            	$shareStr .= '<img border="0" src="http://'.$server->apphost.'/images/icon.jpg" border="0" width="32">';
                            
							}
							
							$shareStr .= '<div class="slcontent">'.$usersgroups[$g]->group_name.'</div>
                            </li>';
                        
							}
						}else{
						
                        	$shareStr .= '<li>You have not created any group</li>';
                        
						}
                    $shareStr .= '</ul>                   
                   			</li>';
							
                   $shareStr .= '<li><a href="#">Individual</a>
                        <ul class="secondLavel" id="share_'.$products[$i]->item_id.'" 
						'.$dropCss.'>';                     
                        
						if(count($followingList) > 0)
				   		{					
							for($f=0; $f < count($followingList); $f++)
							{
						        $follower_details = users::getUserDetails($followingList[$f]->friend_id);
								
								if(in_array($follower_details->user_id, $itemFriendsArr ))
									$frChk = 'checked = "checked"';
								else
									$frChk = '';
								    
                            $shareStr .= '<li><input type="checkbox" id="friend_'.$products[$i]->item_id.'_'.$follower_details->user_id.'" value="'.$follower_details->user_id.'"  onClick="sharewishlist(\'Friend\', \''.$products[$i]->item_id.'\', \'\', \''.$follower_details->user_id.'\',\'friend_'.$products[$i]->item_id.'_'.$follower_details->user_id.'\')" '.$frChk.' />';
							
							if($follower_details->profile_pic != '')
							{
							
								$shareStr .= '<img src="http://'.$server->apphost.'/userpics/thumb/'.$follower_details->profile_pic.'">';
							
							}else{
							
								$shareStr .= '<img src="http://'.$server->apphost.'/userpics/top-img.png">';
							
							}
							$shareStr .= '<div class="slcontent">'.$follower_details->first_name.' '.$follower_details->last_name.'</div></li>';
                        
							}
						}else{
						
                        	 $shareStr .= '<li>You do not have any friend</li>';
                        
						}
						
                    $shareStr .= '</ul>
                    </li>';
					if($products[$i]->public == '0')
						$onlyChk = 'checked = "checked"'; 
					else
						$onlyChk = '';
						
                    $shareStr .= '<li class="flabel">Only me <input type="checkbox" id="private_'.$products[$i]->item_id.'" value="0" onClick="sharewishlist(\'Private\', \''.$products[$i]->item_id.'\', \'\', \'\', \'private_'.$products[$i]->item_id.'\')"  /></li>              
                </ul>
            </span>';*/
		
		$result .= $shareStr;
		 // end of share option
		  
		
		
		}				//
				}
			}else{
				$result .= '<li style="width:90%; min-height:300px;" >
				<div style="text-align:center; vertical-align:middle; margin-top:70px; ">There are no items in your wishlist</div>
				</li>';
			}
			
			$result .= '</ul>';
			
			/// regenerate tag list
			$listTag = category::showTag($_SESSION['user_id']);
			if(count($listTag) > 0)
			{
				$tag_str = '<ul>
			   <li style="padding:8px 6px;" class="first">Filter by Tags:</li>';
			   
			   $total_tag=count($listTag);
			   for($t=0; $t < count($listTag); $t++)
			   {
					if($t<10){					
					//
					if($_GET['item_type'] == $listTag[$t]->id)
					{ 
						$style = 'class="active"';
						$clickLoc = "window.location.href= 'http://".$server->apphost."/login/home'";
					}else{
						$style = '';
						$clickLoc = "window.location.href='?catkey=category&amp;item_type=".$listTag[$t]->id."'";
					}
					//
						
																	  
				$tag_str .= '<li style="margin-bottom: 10px; "><a href="javascript:void(0);" onclick="'.$clickLoc.'" '.$style.'>'.$listTag[$t]->name.'</a></li>';
				
					} else{ 
						if($t==11){ 					
							$tag_str .= '<li class="moretags"><a href="#"></a>
							<ul class="moretagWrap">';
						
						}
						$tag_str .= '<li><a href="javascript:void(0);" onclick="window.location.href=\'?catkey=category&amp;item_type='.$listTag[$t]->id.'\'">'.$listTag[$t]->name.'</a></li>';                    
						if($t==$total_tag){
						
							$tag_str .= '	 </ul>
							</li>';
						
						}
					}
				}//for
						   
			   $tag_str .= ' </ul>';
		   }
			///
			$msg = 'Item is deleted from your wishlist';
			
			$db->commit();
		}catch(Exception $e){
			//Rollback transaction
			$db->rollBack();
			$this->view->errors[] = $e->getMessage();
		}
		
		//echo $msg;
		echo $result.'::'.$tag_str;
		exit;
	}
	
	public function friendswishlistAction()
	{
		$db = Zend_Registry::get("db");
		$config = Zend_Registry::get("config");
		$server = Zend_Registry::get("server");
		
		$params = $this->getRequest()->getParams();
		$product_id = $params['pid'];
		
		//user
		$user_details = users::getUserDetails($_SESSION['user_id']);
		$this->view->userDetail = $user_details;
		
		///categories
		$categories = category::categoryList();
		$this->view->categories = $categories;
		
		//get products	
		$param = $this->getRequest()->getParams();
		
		$ord = $param['order'];
		$catVal = $param['item_type'];
		
		$orderBy = '';
		
		if($param['pricekey'] == 'price')
		{
			if($orderBy != '')
				$orderBy .= ', item_price ASC';
			else
				$orderBy = ' item_price ASC';
		}
		
		if($param['datekey'] == 'date')
		{
			if($orderBy != '')
				$orderBy .= ', uw.added_date DESC';
			else
				$orderBy = 'uw.added_date DESC';
		}
		
		$products = wishlistitmes::getFriendsFeed($_SESSION['user_id'],$orderBy,$catVal);
		
		if($catVal != '')
		{
			$catName = category::categoryName($catVal);
		}else{
			$catName = 'Category';
		}
		
		for($i=0;$i < count($products);$i++)
		{
			$item = array();
			
			$showChk = wishlistitmes::showItem($products[$i]->item_id, $_SESSION['user_id'],$friend_details->user_id,'');
			
			$totalComments = itemcomments::countComments($products[$i]->id);
			$userwished = wishlistitmes::countWished($products[$i]->id);
			
			/// latest item
			//$timeAdded = wishlistitmes::latestItem($products[$i]->id);
		 	//$addeduser = users::getUserDetails($timeAdded['user_id']);
			$timeAdded = wishlistitmes::latestItemUser($products[$i]->id, $_SESSION['user_id']);
			$addeduser = users::getUserDetails($timeAdded['user_id']);		
			
			$item['comment'] = $totalComments;
			$item['wished'] = $userwished;
			$item['added_time'] = $timeAdded['time'];
			//$item['addedby'] = $addeduser->first_name.' '.$addeduser->last_name;
			$item['first_name'] = $addeduser->first_name;
			$item['last_name'] = $addeduser->last_name;
			$item['user_id'] = $addeduser->user_id;
			$items[] = $item;
		}
		//print_r($items);
		//exit;
		$this->view->items = $products;
		$this->view->extval = $items;
		$this->view->categoryName = $catName;
	}
	
	/// Search for gift
	/// Get search as key and find item name like this key
	
	public function searchresultAction()
	{
		$db = Zend_Registry::get("db");
		$config = Zend_Registry::get("config");
		$server = Zend_Registry::get("server");
		
		//user
		$user_details = users::getUserDetails($_SESSION['user_id']);
		$this->view->userDetail = $user_details;
		
		///categories
		$categories = category::categoryList();
		$this->view->categories = $categories;
		
		$param = $this->getRequest()->getParams();
		
		$searchKey = trim($param['search']);	
		$catVal = $param['item_type'];	
		
		if($param['pricekey'] == 'price')
		{
			if($orderBy != '')
				$orderBy .= ', item_price ASC';
			else
				$orderBy = ' item_price ASC';
		}
		
		if($param['datekey'] == 'date')
		{
			if($orderBy != '')
				$orderBy .= ', uw.added_date ASC';
			else
				$orderBy = 'uw.added_date ASC';
		}
		
		try{
		if($searchKey != '')
		{
			
			$products = wishlistitmes::searchResult($_SESSION['user_id'], $searchKey, $orderBy,$catVal);
			
			if($catVal != '')
			{
				$catName = category::categoryName($catVal);
			}else{
				$catName = 'Category';
			}
			
			$keych = '%'.$searchKey.'%';
			$key1 = $db->quote($keych);
				
			/// search people/users
			$nameArr = explode(" ", $searchKey);
			
			if(count($nameArr) > 1)
			{
				$fname = '%'.$nameArr[0].'%';
				$lname = '%'.$nameArr[1].'%';
				
				$fname1 = $db->quote($fname);
				$lname1 = $db->quote($lname);
				
			}else{
				$fname = '%'.$searchKey.'%';
				$fname1 = $db->quote($fname);
			}
			
			if($_SESSION['user_id'] != '')
			{
			
			$userResult = usersfriend::searhUser($fname1, $_SESSION['user_id'], $lname1);
			//$userResult = usersfriend::searhUser($key1, $_SESSION['user_id'], '');
			
			if(!empty($userResult))
				{
					for($i=0; $i < count($userResult); $i++)
					{
						$list = array();
						$chk = usersfriend::checkExistFriendslist($_SESSION['user_id'], $userResult[$i]->user_id);
						
						$list['user_id'] = $userResult[$i]->user_id;
						$list['username'] = $userResult[$i]->username;
						$list['profile_pic'] = $userResult[$i]->profile_pic;
						if($chk)
						{
							$list['isfriend'] = 'Y';
						}else{
							$list['isfriend'] = 'N';
						}
						$userresult[] = $list;
					}
					$this->view->userResult = $userresult;
					//$this->view->total_user = count($result);
				}
			}
			///
			/// Search for tags
			// End Tag search
			$tags = category::usersTagsName($_SESSION['user_id'], $key1);
			$this->view->tags = $tags;			
			
			$totalProduct = count($products);
			$pcnt = 0;
			for($i=0;$i<count($products);$i++)
			{
				$item = array();
				
				$totalComments = itemcomments::countComments($products[$i]->id);		
				$userwished = wishlistitmes::countWished($products[$i]->id);
				
				/// latest item
				//$timeAdded = wishlistitmes::latestItem($products[$i]->id);
				$timeAdded = wishlistitmes::latestItemHome($products[$i]->id);
				$addeduser = users::getUserDetails($timeAdded['user_id']);		
				
				//
				$itemwhished = wishlistitmes::chkWishedItem($products[$i]->id,$_SESSION['user_id']);		
				if($itemwhished)
				{
					$item['youwhished'] = 'Y';
				}else{
					$item['youwhished'] = 'N';
				}		
				//
				$item['comment'] = $totalComments;
				$item['wished'] = $userwished;
				$item['added_time'] = $timeAdded['time'];
				//$item['addedby'] = $addeduser->first_name.' '.$addeduser->last_name;
				$item['first_name'] = $addeduser->first_name;
				$item['last_name'] = $addeduser->last_name;
				$item['addedby'] = $addeduser->first_name.' '.substr($addeduser->last_name, 0,1);
				$item['first_name'] =  $addeduser->first_name;
				$item['user_id'] = $addeduser->user_id;
				$items[] = $item;
				
				if($addeduser->first_name != '')
				{
					$pcnt++;
				}
			}
			//print_r($items);
			//exit;
			$this->view->items = $products;
			//$this->view->totalProduct = $totalProduct;
			$this->view->totalProduct = $pcnt;
			$this->view->extval = $items;
			$this->view->categoryName = $catName;
		}else{
			$this->view->error = 'Please enter value to search';
		}
		}catch(Exception $e){	
			$this->view->errors[] = $e->getMessage();
		}
	}
	
	/// send popup will be displayed from here	
	// Logged in user will not be displayed here	
	public function sendsearchAction()
	{
		$db = Zend_Registry::get("db");
		$config = Zend_Registry::get("config");
		$server = Zend_Registry::get("server");
		
		$params = $this->getRequest()->getParams();
		$key = $params['key'];
		$item_id = $params['item_id'];
		$this->view->item_id = $item_id;
		
		if($key != '')
		{
			try{
				$searchResult = usersfriend::searchFollowList($_SESSION['user_id'], $key);		
					
				if(!empty($searchResult))
				{
					for($i=0; $i < count($searchResult); $i++)
					{
						$list = array();
						
						$user_detail = users::getUserDetails($searchResult[$i]->friend_id);
						
						$list['user_id'] = $searchResult[$i]->friend_id;
						$list['username'] = $user_detail->first_name.' '.$user_detail->last_name;
						$list['profile_pic'] = $user_detail->profile_pic;					
						$result[] = $list;
					}
					$this->view->result = $result;
					$this->view->total_user = count($result);
					$this->view->item_id = $item_id;
				}else{
					$this->view->msg = 'Search data not found.';
				}
			}catch(Exception $e)
			{			  
			  print_r($e->getMessage());
			   $this->view->errors[] = $e->getMessage();
			   
			}
		}
	}
	
	/* get item_id as input and selected user_id */
	/* fetch user's email form DB based on user_id and send mail */
	public function senditemAction()
	{
		$db = Zend_Registry::get("db");
		$config = Zend_Registry::get("config");
		$server = Zend_Registry::get("server");
		
		$params = $this->getRequest()->getParams();
		$product_id = $params['item_id'];
		$user_id = $params['send_userID'];
		
		try{
			$db->beginTransaction();
			///Get product detail
			$productDetail = wishlistitmes::getProductDetail($product_id);
					
			if($productDetail[0]->image != '')
			{
				$image = 'http://'.$server->apphost.'/productimg/thumb/'.$productDetail[0]->image;
			}else{
				$image = 'http://'.$server->apphost.'/productimg/no-img.png';
			}
			
			/*for($i=0; $i < count($user_id); $i++)
			{
			$msg = '';
			$userdeatil = users::getUserDetails($user_id[$i]);	
			$to = $userdeatil->email_id;
			//exit;
			//$to = 'anu.sagi03@gmail.com';
			$mailText = '<table width="100%" border="0" cellspacing="1" cellpadding="5">
						  <tr>
							<td colspan="2">Hi '.$userdeatil->first_name.',<br><br>
							One of your friend has send the this item.
							</td>
						  </tr>
						  <tr>
							<td width="230"><img src="'.$image.'"></td>
							<td style="vertical-align:top;">'.$productDetail[0]->item_name.'<br>
							Price: $'.number_format($productDetail[0]->item_price,2).'<br><br>
							'.stripslashes($productDetail[0]->description).'						
							</td>
						  </tr>
						  <tr>
							<td colspan="2"><a href="http://'.$server->apphost.'">Click here</a> to view more</td>
						  </tr>
						  <tr>
							<td colspan="2">&nbsp;</td>
						  </tr>
						   <tr>
							<td colspan="2">Happy gifting!<br>Giftvise</td>
						  </tr>
						</table>
						';
			//echo $mailText;
						if($to != '' && $user_id[$i] != '')
						{
							$transport = new Zend_Mail_Transport_Sendmail($config->supportEmail);
							Zend_Mail::setDefaultTransport($transport);
							$mail = new Zend_Mail();
							$mail->setSubject('Recomended wishlist item - Giftvise');
							$mail->setFrom($config->supportEmail, 'Admin');
							$mail->addTo($to, $name);
							$mail->setBodyHtml($mailText);
							$mail->send();
							
							$msg = "Message has been send successfully";
						}else{
							if($to == '')
							{
								$msg = "Email id of '".$userdeatil->first_name."' is missing";
							}else{
								$msg = "Error! in sending mail, please try again";
							}
							
						}
			
			}//for*/
			
			
			$msg = '';
			$userdeatil = users::getUserDetails($user_id);	
			
			$to = $userdeatil->email_id;
			//exit;
			//$to = 'anu.sagi03@gmail.com';
			//exit;
			$mailText = '<table width="100%" border="0" cellspacing="1" cellpadding="5">
						  <tr>
							<td colspan="2">Hi '.$userdeatil->first_name.',<br><br>
							One of your friend has been recommended this item.
							</td>
						  </tr>
						  <tr>
							<td width="230"><img src="'.$image.'"></td>
							<td style="vertical-align:top;">'.$productDetail[0]->item_name.'<br>
							Price: $'.number_format($productDetail[0]->item_price,2).'<br><br>
							'.stripslashes($productDetail[0]->description).'						
							</td>
						  </tr>
						  <tr>
							<td colspan="2"><a href="http://'.$server->apphost.'/content/index?pid='.$product_id.'&send=email">Click here</a> to view more</td>
						  </tr>
						  <tr>
							<td colspan="2">&nbsp;</td>
						  </tr>
						   <tr>
							<td colspan="2">Happy gifting!<br>Giftvise</td>
						  </tr>
						</table>
						';
			//echo $mailText;
						if($to != '' && $user_id[$i] != '')
						{
							$transport = new Zend_Mail_Transport_Sendmail($config->supportEmail);
							Zend_Mail::setDefaultTransport($transport);
							$mail = new Zend_Mail();
							$mail->setSubject('Recomended wishlist item - Giftvise');
							$mail->setFrom($config->supportEmail, 'Giftvise');
							$mail->addTo($to, $name);
							$mail->setBodyHtml($mailText);
							//$mail->send();
							
							$msg = "Message has been send successfully";
						}else{
							if($to == '')
							{
								//$msg = "Email id of '".$userdeatil->first_name."' is missing";
								$msg = '';
							}else{
								$msg = "Error! in sending mail, please try again";
							}
							
						}
			
			
			/// record activity					
					$data = array();
					$data['sender'] = $_SESSION['user_id'];
					$data['receiver'] = $user_id;
					$data['item_id'] = $product_id;
					$data['activity_type'] = 'recommend item';
					$data['activity_date'] = date('Y-m-d H:i:s');
					
					$db->insert('activity',$data);
					$db->commit();
			//$msg = "Message has been send successfully";
			//$this->view->msg = $msg;
			echo $msg;
			exit;
		
		}catch(Exception $e)
			{			  
			  print_r($e->getMessage());
			  exit;
			// $this->view->msg = $e->getMessage();
			  // $this->view->errors[] = $e->getMessage();
			   
		}
		//$this->_helper->viewRenderer("sendsearch");
		//$this->_helper->resources("sendsearch");
	}//end send
	
	/// Get friend name list
	public function getfriendhintAction()
	{
		$db = Zend_Registry::get("db");
		$config = Zend_Registry::get("config");
		$server = Zend_Registry::get("server");
		
		$params = $this->getRequest()->getParams();
		$key = $params['key'];
		$item_id = $params['item_id'];
		
		$searchResult = usersfriend::searchFollowList($_SESSION['user_id'], $key);;			
					
				$listStr = '';
				if(!empty($searchResult))
				{
					for($i=0; $i < count($searchResult); $i++)
					{
						$list = array();
						
						$user_detail = users::getUserDetails($searchResult[$i]->friend_id);
						$listStr .= '<li style="display: block; min-height: 40px;" id="c_'.$searchResult[$i]->friend_id.'" onclick="sendtoFriend('.$searchResult[$i]->friend_id.', '.$item_id.', \' c_'.$searchResult[$i]->friend_id.'\')" >';
						//$listStr .= '<input type="checkbox" value="'.$searchResult[$i]->friend_id.'"  style="float:left;" >'; 
						if($user_detail->profile_pic != '')
						{
							$listStr .='<img src="http://'.$server->apphost.'/userpics/thumb/'.$user_detail->profile_pic.'" style="float:left" width="32">';						
						}else{						
                            $listStr .='<img src="http://'.$server->apphost.'/userpics/top-img.png" style="float:left" width="32">';						
						}
						
						$listStr .= $user_detail->first_name.' '.$user_detail->last_name;;
						$listStr .= '</li>';
						
						/*$list['user_id'] = $searchResult[$i]->friend_id;
						$list['username'] = $user_detail->first_name.' '.$user_detail->last_name;
						$list['profile_pic'] = $user_detail->profile_pic;					
						$result[] = $list;*/
					}
			}else{
				$listStr .= '<li style="display: block; min-height: 40px;">You do not have any friend.</li>';
			}
			echo $listStr;
			exit;
	}
	
	
	///// load home page on scrolling 
	public function loadscrollAction()
	{
		$db = Zend_Registry::get("db");
		$config = Zend_Registry::get("config");
		$server = Zend_Registry::get("server");
		
		$param = $this->getRequest()->getParams();
		$limit  = $param['startp'];
		
		try{
		$products = wishlistitmes::getAllFeedSiteScroll($_SESSION['user_id'],'','',$limit);
		
		$result = '';
		
			if(count($products) > 0)
			{
				for($i=0;$i<count($products);$i++)
				{
					
					$item = array();
					
					$totalComments = itemcomments::countComments($products[$i]->item_id);
					$userwished = wishlistitmes::countWished($products[$i]->item_id);
					
					/// latest item
					$timeAdded = wishlistitmes::latestItemHome($products[$i]->item_id);
					$addeduser = users::getUserDetails($timeAdded['user_id']);		
				
					$item['comment'] = $totalComments;
					$item['wished'] = $userwished;
					$item['added_time'] = $timeAdded['time'];
					$item['addedby'] = $addeduser->first_name.' '.$addeduser->last_name;
					$item['user_id'] = $addeduser->user_id;
					$items[] = $item;
					
					////
					
					if($products[$i]->item_id != '')
					{
						if($addeduser->first_name != '')
						{
							if($products[$i]->image != '')
							{
											
								
									$result .= '<div class="pin pin1" style="width:212px;">
												<div class="pinholder">
												
													<figure>
													  <div class="mosaic-block bar" >  
													<div class="details mosaic-overlay">
													 <p style="float:left; width:118px; padding-left:5px; padding-right:5px;">';
													 
												$name = '';
												if(strpos($products[$i]->item_name, ' '))
												{
													$string = substr($products[$i]->item_name,0,15);
													$tmpProductName = substr($string, 0, strrpos($string, ' '));
													//if(strlen($products[$i]->item_name) > 15 )
													if(strlen(trim($tmpProductName)) > 15 )
													{
														//$name = substr($products[$i]->item_name,0,15);
														$name = '...';
													}else{
														$name = $tmpProductName;
													}
												 
												 }else{
												 	$name = substr($products[$i]->item_name,0,15);
												 }
												 $result .= $name.' </p> <p style="float:right">$';
												// $result .= $products[$i]->item_price.'</p> </div>';
												 $result .= wishlistitmes::format_price($products[$i]->item_price).'</p> </div>';
												 $result .= '<div class="mosaic-backdrop">';
																		
													$image = '';
													 if($products[$i]->image != '')
													 {
													
													$image = '<a class="iframe " href="http://'.$server->apphost.'/products/productdetail?pid='.$products[$i]->id.'&latuser='.$addeduser->user_id.'" title="">
															<img src="http://'.$server->apphost.'/productimg/thumb/'.$products[$i]->image.'" alt="">
														</a>';
													 
													 }else{							 
													 $image = '<a class="iframe " href="http://'.$server->apphost.'/products/productdetail?pid='.$products[$i]->id.'" title="">
													 <img src="http://'.$server->apphost.'/productimg/no-img.png" alt="">
													 </a>';
													 
													 }							 	 
																	
													$result .= $image.' </div>
												  </div>							
													 </figure>
														<div class="details">
															<span class="price" id="ws_'.$products[$i]->id.'">';
															
															if($_SESSION['user_id'] != '')
															{															
															$isitemwhished = wishlistitmes::chkWishedItem($products[$i]->id,$_SESSION['user_id']);		
															if($isitemwhished)
															{
																$result .= '<a href="javascript:void(0);" style="cursor:auto;">
																<img src="http://'.$server->apphost.'/images/pin1-price-b.png" width="21" height="23" alt="#" align="center" style="margin-left:10px; cursor:auto;" /></a>';
															}else{
																$result .= '<a href="javascript:void(0);" onclick="addwish(\''.$products[$i]->id.'\',\''.$_SESSION['user_id'].'\',\''.$userwished.'\')" >
																<img src="http://'.$server->apphost.'/images/pin1-price.png" width="21" height="23" alt="#" align="center" style="margin-left:10px;" /></a>';
															}
															}else{
																$result .= '<a href="javascript:void(0);" style="cursor:auto;">
																<img src="http://'.$server->apphost.'/images/pin1-price.png" width="21" height="23" alt="#" align="center" style="margin-left:10px; cursor:auto;" /></a>';
															}
															
															$result .= '<span>'.$userwished.'</span></span>
															
															<span class="comments"><a href="javascript:void(0);"><img src="http://'.$server->apphost.'/images/pin1-comments.png" width="20" height="17" alt="#" align="center" style="margin-left:10px; cursor:text;" /></a>
															<span>'.$totalComments.'</span></span>
														</div><!--details-->
														<div class="details">
														<div class="client-comment">
														  <p class="client-name">
														  <span class="timer"><img src="http://'.$server->apphost.'/images/timer.png" width="14" height="13" alt="Timer" /></span>&nbsp;'.
															$timeAdded['time'].' by <span> <a href="#">'.substr($addeduser->first_name,0,8).' '.substr($addeduser->last_name,0,1).'</a></span></p>
															</p>
														</div>
													</div>
												</div><!--pinholder-->
												
											</div>
											<div class="clear"></div>
											';
							}
						}
					}	//
				}
				
				
			
			}else{
				//$result = '';
			}
			
			echo $result;
			exit;
		}catch(Exception $e)
			{			  
			  print_r($e->getMessage());
			  exit;
			   
		}
	}
	
	// load feed page on scrolling
	public function loadfeedscrollAction()
	{
		$db = Zend_Registry::get("db");
		$config = Zend_Registry::get("config");
		$server = Zend_Registry::get("server");
		
		$param = $this->getRequest()->getParams();
		$limit  = $param['startp'];
		
		try{
		
		//$products = wishlistitmes::getAllFeedPageScroll($_SESSION['user_id'],'','',$limit);
		$products = wishlistitmes::getAllFeedPageScroll($_SESSION['user_id'],'','',$limit);
		/*echo '<pre>';
		print_r($products);
		echo '</pre>';*/
		
		$result = '';
		
			if(count($products) > 0)
			{
				
				for($i=0;$i<count($products);$i++)
				{
					
					$item = array();
					
					$totalComments = itemcomments::countComments($products[$i]->item_id);
					$userwished = wishlistitmes::countWished($products[$i]->item_id);
					
					/// latest item
					$timeAdded = wishlistitmes::latestItemUser($products[$i]->item_id, $_SESSION['user_id']);
					$addeduser = users::getUserDetails($timeAdded['user_id']);		
				
					$item['comment'] = $totalComments;
					$item['wished'] = $userwished;
					$item['added_time'] = $timeAdded['time'];
					$item['addedby'] = $addeduser->first_name.' '.$addeduser->last_name;
					$item['user_id'] = $addeduser->user_id;
					$items[] = $item;
					
					////					
					if($products[$i]->item_id != '')
					{
						if($addeduser->first_name != '')
						{
							if($products[$i]->image != '')
							{
									$result .= '<div class="pin pin1" style="width:212px;">
												<div class="pinholder">												
													<figure>
													  <div class="mosaic-block bar" >  
													<div class="details mosaic-overlay">
													 <p style="float:left; width:118px; padding-left:5px; padding-right:5px;">';
													 
												/*$name = '';
												if(strlen($products[$i]->item_name) > 15 )
												{
													$name = substr($products[$i]->item_name,0,15);
												}else{
													$name = $products[$i]->item_name;
												}*/
												$name = '';
												if(strpos($products[$i]->item_name, ' '))
												{
													$string = substr($products[$i]->item_name,0,15);
													$tmpProductName = substr($string, 0, strrpos($string, ' '));
													//if(strlen($products[$i]->item_name) > 15 )
													if(strlen(trim($tmpProductName)) > 15 )
													{
														//$name = substr($products[$i]->item_name,0,15);
														$name = '...';
													}else{
														$name = $tmpProductName;
													}
												 
												 }else{
												 	$name = substr($products[$i]->item_name,0,15);
												 }
												 
												 $result .= $name.' </p> <p style="float:right">$';
												// $result .= $products[$i]->item_price.'</p> </div>';
												 $result .= wishlistitmes::format_price($products[$i]->item_price).'</p> </div>';
												 $result .= '<div class="mosaic-backdrop">';
																		
													$image = '';
													 if($products[$i]->image != '')
													 {
													
													$image = '<a class="iframe " href="http://'.$server->apphost.'/products/productdetail?pid='.$products[$i]->id.'&latuser='.$addeduser->user_id.'" title="">
															<img src="http://'.$server->apphost.'/productimg/thumb/'.$products[$i]->image.'" alt="">
														</a>';
													 
													 }else{							 
													 $image = '<a class="iframe " href="http://'.$server->apphost.'/products/productdetail?pid='.$products[$i]->id.'" title="">
													 <img src="http://'.$server->apphost.'/productimg/no-img.png" alt="">
													 </a>';
													 
													 }
													
													$result .= $image.' </div>
												  </div>					
													 </figure>
														<div class="details">
															<span class="price" id="ws_'.$products[$i]->id.'">';
															
															if($_SESSION['user_id'] != '')
															{															
															$isitemwhished = wishlistitmes::chkWishedItem($products[$i]->id,$_SESSION['user_id']);		
															if($isitemwhished)
															{
																$result .= '<a href="javascript:void(0);" style="cursor:auto;">
																<img src="http://'.$server->apphost.'/images/pin1-price-b.png" width="21" height="23" alt="#" align="center" style="margin-left:10px; cursor:auto;" /></a>';
															}else{
																$result .= '<a href="javascript:void(0);" onclick="addwish(\''.$products[$i]->id.'\',\''.$_SESSION['user_id'].'\',\''.$userwished.'\')" >
																<img src="http://'.$server->apphost.'/images/pin1-price.png" width="21" height="23" alt="#" align="center" style="margin-left:10px;" /></a>';
																}
															}else{
																$result .= '<a href="javascript:void(0);" style="cursor:auto;">
																<img src="http://'.$server->apphost.'/images/pin1-price.png" width="21" height="23" alt="#" align="center" style="margin-left:10px; cursor:auto;" /></a>';
															}
															
															$result .= '<span>'.$userwished.'</span></span>
															
															<span class="comments"><a href="javascript:void(0);"><img src="http://'.$server->apphost.'/images/pin1-comments.png" width="20" height="17" alt="#" align="center" style="margin-left:10px; cursor:text;" /></a>
															<span>'.$totalComments.'</span></span>
														</div><!--details-->
														<div class="details">
														<div class="client-comment">
														  <p class="client-name">
														  <span class="timer"><img src="http://'.$server->apphost.'/images/timer.png" width="14" height="13" alt="Timer" /></span>&nbsp;'.
															$timeAdded['time'].' by <span> <a href="#">'.substr($addeduser->first_name,0,8).' '.substr($addeduser->last_name,0,1).'</a></span></p>
															</p>
														</div>
													</div>
												</div><!--pinholder-->
												
											</div>
											<div class="clear"></div>
											';
							}
						}
					}	//
					
					//echo '<br>id:'.$products[$i]->item_id;
				}
				
				
			
			}else{
				//$result = '';
			}
			
			echo $result;	
			exit;
		}catch(Exception $e)
			{			  
			  print_r($e->getMessage());
			  exit;
			   
		}
	}
	
	/// save already created tag to the item
	/// tag_id and item_id as input
	public function savetagAction()
	{
		$db = Zend_Registry::get("db");
		$config = Zend_Registry::get("config");
		$server = Zend_Registry::get("server");
		
		$param = $this->getRequest()->getParams();
		
		$tag_id = $param['tag_id'];
		$item_id = $param['item_id'];
		$user_id = $_SESSION['user_id'];
		
		try{
			$db->beginTransaction();
			$data['item_id'] = $item_id;
			$data['tag_id'] = $tag_id; // tag_id is the category id
			$data['user_id'] = $user_id;
			$data['tag_date'] = date('Y-m-d H:i:s');
			
			$totalTags = category::countItemTags($item_id, $user_id);
			
			if(count($totalTags) < 5)
			{
			//check for already added
			$checkID = category::checkItemTags($item_id, $tag_id, $user_id);
			if($checkID != '')
			{
				// user already tagged this item in this tag
				echo 'user already tagged item in this tag';
				exit;
			}else{
				// tag item
				$db->insert('item_tags', $data);
				$db->commit();
				
				$taglist = category::getItemTags($item_id, $user_id);
				$listStr = '<ul>';
				$listStr .= '<li><a href="javascript:void(0)" style="background: none repeat scroll 0 0 transparent;  border: 0 none; cursor: default;">Tags:</a></li>';
				for($i=0; $i < count($taglist); $i++)
				{
					$listStr .= '<li><a href="javascript:void(0)" class="deletetag">'.$taglist[$i]->name.'<span onclick="deleteTag('.$taglist[$i]->id.','.$item_id.','.$user_id.')"></span></a></li>';
				}
				$listStr .= '</ul>';
				$listStr .= '<a href="javascript:void(0)" onclick="document.getElementById(\'newtag\').style.display=\'block\'" class="admota"><img src="http://'.$server->apphost.'/images/plus.png" border="0" /></a>';
				//echo 'success';
				echo "S::".$listStr;
				exit;
			}
			
			}else{
				echo 'This Item has maximum no. (5) of tags, please remove tag to add new Tag';
			}
			exit;
		}catch(Exception $e)
		{			  
			$db->rollBack();
			print_r($e->getMessage());
			exit;
		}
		
		
	}
	
	/// Add new tag and tag assign it to the item
	public function addsavetag1Action()
	{
	
		$db = Zend_Registry::get("db");
		$config = Zend_Registry::get("config");
		$server = Zend_Registry::get("server");
		
		$param = $this->getRequest()->getParams();
		
		$tag_name = $param['tag_name'];
		$item_id = $param['item_id'];
		$user_id = $_SESSION['user_id'];
		
		$tname = $db->quote($param['tag_name']);
		try{
			$db->beginTransaction();
			
			$dataT['name'] = $tag_name;
			$dataT['created_by'] = $user_id;
			$dataT['status'] = 1;
			
			$totalTags = category::countItemTags($item_id, $user_id);
			if(count($totalTags) < 5)
			{
			// check it it is already added
			$checkTagID = category::checkTags($tag_name, $user_id);
			if($checkTagID != '')
			{
				// already added
				$tag_id = $checkTagID;
			}else{
				// add
				$db->insert('category',$dataT);
				$tag_id =  $db->lastInsertId();
			}
			//add this tag to DB
			
			//
			$data['item_id'] = $item_id;
			$data['tag_id'] = $tag_id; // tag_id is the category id
			$data['user_id'] = $user_id;
			$data['tag_date'] = date('Y-m-d H:i:s');
			
			//check for already added
			$checkID = category::checkItemTags($item_id, $tag_id, $user_id);
			if($checkID != '')
			{
				// user already tagged this item in this tag
				echo 'user already tagged item in this tag';
				exit;
			}else{
				// tag item
				$db->insert('item_tags', $data);				
				
				$taglist = category::getItemTags($item_id, $user_id);
				$listStr = '<ul>';
				$listStr .= '<li><a href="javascript:void(0)" style="background: none repeat scroll 0 0 transparent;  border: 0 none; cursor: default;">Tags:</a></li>';
				for($i=0; $i < count($taglist); $i++)
				{
					
					$listStr .= '<li><a href="javascript:void(0)" class="deletetag">'.$taglist[$i]->name.'<span onclick="deleteTag('.$taglist[$i]->id.','.$item_id.','.$user_id.')"></span></a></li>';
					
				}				
				
				$listStr .= '</ul>';
				$listStr .= '<a href="javascript:void(0)" onclick="document.getElementById(\'newtag\').style.display=\'block\'" class="admota"><img src="http://'.$server->apphost.'/images/plus.png" border="0" /></a>';
				//echo 'success';
				echo "S::".$listStr;				
			}
			
			}else{
				echo 'This Item has maximum no. (5) of tags, please remove tag to add new Tag';
			}
			$db->commit();			
			
			exit;		
		}catch(Exception $e)
		{			  
			$db->rollBack();
			print_r($e->getMessage());
			exit;
		}
	}////
	
	
	///
	public function addsavetagAction()
	{
	
		$db = Zend_Registry::get("db");
		$config = Zend_Registry::get("config");
		$server = Zend_Registry::get("server");
		
		$param = $this->getRequest()->getParams();
		
		$tag_name = $param['tag_name'];
		$item_id = $param['item_id'];
		$user_id = $_SESSION['user_id'];
		
		$tname = $db->quote($param['tag_name']);
		try{
			$db->beginTransaction();
			
			$tagsArr = explode(",", $tag_name);
			
						if(count($tagsArr) > 5 )
						{
							$tlimit = 5;
						}else{
							$tlimit = count($tagsArr);
						}
						for($t = 0; $t < $tlimit; $t++)
						{
							if(trim($tagsArr[$t]) != '')
							{
							
								if(strlen($tagsArr[$t]) > 25)
								{
									$tagNames = substr($tagsArr[$t],0,25);
								}else{
									$tagNames = $tagsArr[$t];
								}
								$dataT = array();
								//$dataT['name'] = $tagsArr[$t];
								$dataT['name'] = $tagNames;
								$dataT['created_by'] = $user_id;
								$dataT['status'] = 1;
								
								$checkTagID = category::checkTags($tagsArr[$t], $user_id);
								if($checkTagID != '')
								{
									// already added
									$tag_id = $checkTagID;
								}else{
									// add
									$db->insert('category',$dataT);
									$tag_id =  $db->lastInsertId();
								}
								$dataIT['item_id'] = $item_id;
								$dataIT['tag_id'] = $tag_id; // tag_id is the category id
								$dataIT['user_id'] = $user_id;
								$dataIT['tag_date'] = date('Y-m-d H:i:s');
								
								$checkID = category::checkItemTags($item_id, $tag_id, $user_id);
								if($checkID != '')
								{
									// do nothing
								}else{
									// tag item
									$chktaglist = category::getItemTags($item_id, $user_id);
									if(count($chktaglist) < 5)
									{
										$db->insert('item_tags', $dataIT);
									}
								}
							
							}
						}// for			
			$db->commit();
			$taglist = category::getItemTags($item_id, $user_id);
				$listStr = '<ul>';
				$listStr .= '<li><a href="javascript:void(0)"style="background: none repeat scroll 0 0 transparent;  border: 0 none; cursor: default;" >Tags:</a></li>';
				for($i=0; $i < count($taglist); $i++)
				{
					
					$listStr .= '<li><a href="javascript:void(0)" class="deletetag">'.$taglist[$i]->name.'<span onclick="deleteTag('.$taglist[$i]->id.','.$item_id.','.$user_id.')"></span></a></li>';
				}				
				
				$listStr .= '</ul>';
				
				if(count($taglist) < 5)
				{
					$listStr .= '<a href="javascript:void(0)" onclick="document.getElementById(\'newtag\').style.display=\'block\'" class="admota"><img src="http://'.$server->apphost.'/images/plus.png" border="0" /></a>';
				}
				//echo 'success';
			echo "S::".$listStr;
			exit;
				
		}catch(Exception $e)
		{			  
			$db->rollBack();
			print_r($e->getMessage());
			exit;
		}
	}////
	///
	public function userstaglistAction()
	{
		$db = Zend_Registry::get("db");
		$config = Zend_Registry::get("config");
		$server = Zend_Registry::get("server");
		
		$param = $this->getRequest()->getParams();
		$userID = $param['user_id'];
		$tagName = $param['tag_name'];
		$item_id = $param['item_id'];
		
		$tag_name = $tagName.'%';
		$tag_name1 = $db->quote($tag_name);
		
		$usertags = category::usersTagsName($_SESSION['user_id'], $tag_name1);
		
		if(count($usertags) > 0)
		{
			for($i=0; $i < count($usertags); $i++)
			{
				$strList .= '<li value="'.$usertags[$i]->id.'" onClick="shownewTag('.$usertags[$i]->id.', '.$item_id.')">'.$usertags[$i]->name.'</li>';
			}	
		}
		echo $strList;
		exit;
		
	}//end function
	
	/// delete tag of an item
	// user_id, item_id and tag_id are input
	public function deletetagAction()
	{
		$db = Zend_Registry::get("db");
		$config = Zend_Registry::get("config");
		$server = Zend_Registry::get("server");
		
		$param = $this->getRequest()->getParams();
		$userID = $param['user_id'];
		$tag_id = $param['tag_id'];
		$item_id = $param['item_id'];
		
		try{
			$db->beginTransaction();
			
			if($userID != '' && $item_id != '' && $tag_id != '')
			{
				$res = $db->delete('item_tags', array("user_id = ?" => $userID, "item_id = ?" => $item_id, "tag_id = ?" =>$tag_id));
				$db->commit();
			}
			
			$taglist = category::getItemTags($item_id, $userID);
			$listStr = '<ul>';
			$listStr .= '<li><a href="javascript:void(0)" style="background: none repeat scroll 0 0 transparent;  border: 0 none; cursor: default;">Tags:</a></li>';
			for($i=0; $i < count($taglist); $i++)
			{
				$listStr .= '<li><a href="javascript:void(0)" class="deletetag">'.$taglist[$i]->name.'<span onclick="deleteTag('.$taglist[$i]->id.','.$item_id.','.$userID.')"></span></a></li>';
			}
			$listStr .= '</ul>';
			$listStr .= '<a href="javascript:void(0)" onclick="document.getElementById(\'newtag\').style.display=\'block\'" class="admota"><img src="http://'.$server->apphost.'/images/plus.png" border="0" /></a>';
			
			echo $listStr;			
			exit;
		}catch(Exception $e)
		{			  
			$db->rollBack();
			print_r($e->getMessage());
			exit;
		}
		
	}//end function
	
	public function savequantityAction()
	{
		$db = Zend_Registry::get("db");
		$config = Zend_Registry::get("config");
		$server = Zend_Registry::get("server");
		
		$param = $this->getRequest()->getParams();
		$userID = $_SESSION['user_id'];
		$item_id = $param['item_id'];
		$qty = $param['qty'];
		
		try{
			$db->beginTransaction();
			
			$data['items_required_no'] = $qty;
			$db->update('userwishlist',$data, array("user_id = ?" => $userID, "item_id = ?" => $item_id));
			$db->commit();
			
			echo 'Quantity has been updated successfully.';
			exit;
		}catch(Exception $e)
		{			  
			$db->rollBack();
			print_r($e->getMessage());
			exit;
		}
		echo $strList;
		exit;
		
	}//end function
	
	/// event related wishlist
	public function eventwishlistAction()
	{
		$db = Zend_Registry::get("db");
		$config = Zend_Registry::get("config");
		$server = Zend_Registry::get("server");
		
		try{
			//user
			$user_details = users::getUserDetails($_SESSION['user_id']);
			$this->view->userDetail = $user_details;
			
			//
			$events = event::upcomingEvent($_SESSION['user_id']);
			
			for($i=0; $i < count($events); $i++)
			{
				// get items
				$list = array();
				$items = array();
				$itemStr = event::tagGiftForEvent($events[$i]->eventID);
				$list['id'] = $events[$i]->eventID;
				$list['title'] = $events[$i]->title;
				$list['event_date'] = $events[$i]->event_date;
				$list['event_time'] = $events[$i]->event_time;
				$list['created_user'] = $events[$i]->created_user;
				$list['location'] = $events[$i]->location;
				$list['image'] = $events[$i]->image;
				$list['description'] = $events[$i]->description;
				
				if($itemStr != '')
				{
					$items = event::taggedItem($itemStr);
				}else{
					// if item is not tagged then get user's latest 3 wishlist icon
					$items = wishlistitmes::getLatestItemForEvent($events[$i]->created_user);
				}
				
				$extArry = array();
				for($e=0; $e < count($items); $e++)
				{
					$extArrylist = array();
					$totalComments = itemcomments::countComments($items[$e]->id);
					
					/// latest item
					$timeAdded = wishlistitmes::latestItem($items[$e]->id);
					$addeduser = users::getUserDetails($timeAdded['user_id']);
					
					$itemwhished = wishlistitmes::chkWishedItem($items[$e]->id,$_SESSION['user_id']);
					
					if($itemwhished)
					{
						$extArrylist['youwhished'] = 'Y';
					}else{
						$extArrylist['youwhished'] = 'N';
					}	
			
					
			
					$extArrylist['comments'] = $totalComments;
					$extArrylist['wished'] = $itemwhished;
					$extArrylist['added_time'] = $timeAdded['time'];
					$extArrylist['first_name'] = $addeduser->first_name;
					$extArrylist['last_name'] = $addeduser->last_name;
					$extArrylist['added_user_id'] = $addeduser->user_id;
					
					$extArry[] = $extArrylist;
					
				}// extra values
				
				
				$list['items'] = $items;
				$list['extval'] = $extArry;
				$item_list[] = $list;
			}
					
			$this->view->events = $item_list;
			
			
		}catch(Exception $e)
		{			  
			print_r($e->getMessage());
			exit;
		}	
	}
	
	/// delete activity
	public function deleteactivityAction()
	{		
		$db = Zend_Registry::get("db");
		$config = Zend_Registry::get("config");
		$server = Zend_Registry::get("server");
		
		$param = $this->getRequest()->getParams();
		
		$user_id = $param['user_id'];
		$activity_id = $param['activity_id'];
		
		//exit;
		
		try{
				$db->beginTransaction();
				if($user_id != '' && $activity_id != '')
				{
					// check any user is already deleted this activity
					// if one user is deleted the remove this activity from DB,
					// else only set deleted_user value
					$chkid = users::chkActivityRemoved($activity_id);
					
					if($chkid != '' && $chkid != 0)
					{
						if($chkid == $user_id)
						{
							/// do nothing
						}else{
							/// delete this activity from DB
							$res = $db->delete('activity', array("id = ?" => $activity_id));
							echo 'D';
						}
					}else{
						//update DB
						$datau['deleted_user'] = $user_id;
						$res = $db->update('activity', $datau, array("id = ?" => $activity_id));
						echo 'updated';
					}
					$db->commit();
					
					
				}
				exit;
		}catch(Exception $e)
		{			  
			print_r($e->getMessage());
			exit;
		}	
	}
	
	public function sharetoemailAction()
	{
		$db = Zend_Registry::get("db");
		$config = Zend_Registry::get("config");
		$server = Zend_Registry::get("server");
		
		$param = $this->getRequest()->getParams();		
		$productID = $param['item_id'];
		
		$this->_initemail();
		//$this->errorOnNoscript ();
		try{
		
			$productDetail = wishlistitmes::getProductDetail($productID);
			
			$user = users::getUserDetails($_SESSION['user_id']);
			
			if($productDetail[0]->image != '')
			{
				$image = 'http://'.$server->apphost.'/productimg/thumb/'.$productDetail[0]->image;
			}else{
				$image = 'http://'.$server->apphost.'/productimg/no-img.png';
			}
			
			$mailText = '<table width="100%" border="0" cellspacing="1" cellpadding="5">
						  <tr>
							<td colspan="2">Hi ,<br>
							<b>'.$user->first_name.' '.$user->last_name.'</b> wants you to check out this product on Giftvise.com!
							</td>
						  </tr>
						  <tr>
							<td width="230"><img src="'.$image.'"></td>
							<td style="vertical-align:top;"><b>'.$productDetail[0]->item_name.'</b><br><br>
							Price: $'.number_format($productDetail[0]->item_price,2).'
							</td>
						</tr>
											 
						  <tr>
							<td colspan="2">&nbsp;</td>
						  </tr>
						  
						</table>
						';
						
		$this->view->mailText = $mailText;				
		$this->view->item_id = $productID;
			
		}catch(Exception $e)
		{			  
			print_r($e->getMessage());
			exit;
		}
	}
	
	public function sendemailAction()
	{
		$db = Zend_Registry::get("db");
		$config = Zend_Registry::get("config");
		$server = Zend_Registry::get("server");
		
		$param = $this->getRequest()->getParams();		
		$productID = $param['item_id'];
		$to = $param['email_id'];
		
		$this->_initemail();
		try{
			$user = users::getUserDetails($_SESSION['user_id']);
			
			$productDetail = wishlistitmes::getProductDetail($productID);
			
			if($productDetail[0]->image != '')
			{
				$image = 'http://'.$server->apphost.'/productimg/thumb/'.$productDetail[0]->image;
			}else{
				$image = 'http://'.$server->apphost.'/productimg/no-img.png';
			}
			
			//to show on page
		$mailTextPage = '<table width="100%" border="0" cellspacing="1" cellpadding="5">
						  <tr>
							<td colspan="2">Hi ,<br>
							<b>'.$user->first_name.' '.$user->last_name.'</b> wants you to check out this product on Giftvise.com!
							</td>
						  </tr>
						  <tr>
							<td width="230"><img src="'.$image.'"></td>
							<td style="vertical-align:top;"><b>'.$productDetail[0]->item_name.'</b><br><br>
							Price: $'.number_format($productDetail[0]->item_price,2).'
							</td>
						 </tr>
						  <tr>
							<td colspan="2">&nbsp;</td>
						  </tr>
						  
						</table>
						';
			$this->view->mailText = $mailTextPage;
			//
			
			$this->fields->validate();
			
			
			$mailText = '<table width="100%" border="0" cellspacing="1" cellpadding="5">
						  <tr>
							<td colspan="2">Hi ,<br>
							<b>'.$user->first_name.' '.$user->last_name.'</b> wants you to check out this product on Giftvise.com!
							</td>
						  </tr>
						  <tr>
							<td width="230"><img src="'.$image.'"></td>
							<td style="vertical-align:top;"><b>'.$productDetail[0]->item_name.'</b><br><br>
							Price: $'.number_format($productDetail[0]->item_price,2).'
							</td>
						  </tr>
						  <tr>
							<td colspan="2"><a href="http://'.$server->apphost.'/content/index?send=email&pid='.$productID.'">Click here</a> to view more</td>
						  </tr>					 
						  <tr>
							<td colspan="2">&nbsp;</td>
						  </tr>
						  <tr>
							<td colspan="2">'.$param['msg_text'].'</td>
						  </tr>
						   <tr>
							<td colspan="2">Happy gifting!<br>Giftvise</td>
						  </tr>
						  
						</table>
						';
		////
		
		
		// comma separated email id
		$toArr = explode(",", $param['email_id']);
		
		for($e=0; $e < count($toArr); $e++)
		{
			$transport = new Zend_Mail_Transport_Sendmail($config->supportEmail);
			Zend_Mail::setDefaultTransport($transport);
			$mail = new Zend_Mail();
			$mail->setSubject($user->first_name.' '.$user->last_name.' wants you to check out this product on Giftvise.com');
			$mail->setFrom($config->supportEmail, 'Giftvise');
		
			$toemail = trim($toArr[$e]);
			$mail->addTo($toemail, $name);
			$mail->setBodyHtml($mailText);
			$mail->send();	
		}						
		$msg = "Message has been sent successfully.";
		$this->view->msg = $msg;
		////		
		$this->view->item_id = $productID;
		}catch(Exception $e)
		{			  
			$errormsg = $e->getMessage();
			//print_r($e->getMessage());
			//exit;
		}
		
		
		$this->_helper->resources("sharetoemail");
		$this->_helper->viewRenderer("sharetoemail");
	}
	
	protected function _initemail()
	{	
		
		$email = new PS_Validate_NotEmpty($this->_getParam("email_id"));
		$email->setMessage(PS_Translate::_("Please enter Email"));
		
		$msg_text = new PS_Validate_NotEmpty($this->_getParam("msg_text"));
		$msg_text->setMessage(PS_Translate::_("Please enter message"));
		  
		$this->fields = new PS_Form_Fields(
			array(		
				"email_id" => array(
					$email,
					"required" => true
				)			
				
				/*"msg_text" => array(
					$msg_text,
					"required" => true
				)*/
				
			)
		);
		$params = $this->getRequest()->getParams();
		$this->fields->setData($params);
		$this->view->fields 		= $this->fields;
		
	}///end function
	
	////
	public function updatetaglistAction()
	{		
		$db = Zend_Registry::get("db");
		$config = Zend_Registry::get("config");
		$server = Zend_Registry::get("server");
		
		$param = $this->getRequest()->getParams();
		
		try{
				$db->beginTransaction();
				
				$listTag = category::showTag($_SESSION['user_id']);
				if(count($listTag) > 0)
				{
				$tag_str = '<ul>
			   <li style="padding:8px 6px;" class="first">Filter by Tags:</li>';
			   
			   $total_tag=count($listTag);
			   for($t=0; $t < count($listTag); $t++)
			   {
					if($t<10){					
					//
					if($_GET['item_type'] == $listTag[$t]->id)
					{ 
						$style = 'class="active"';
						$clickLoc = "window.location.href= 'http://".$server->apphost."/login/home'";
					}else{
						$style = '';
						$clickLoc = "window.location.href='?catkey=category&amp;item_type=".$listTag[$t]->id."'";
					}
					//					
																	  
				$tag_str .= '<li style="margin-bottom: 10px; "><a href="javascript:void(0);" onclick="'.$clickLoc.'" '.$style.'>'.$listTag[$t]->name.'</a></li>';
				
					} else{ 
						if($t==11){ 					
							$tag_str .= '<li class="moretags"><a href="#"></a>
							<ul class="moretagWrap">';
						
						}
						$tag_str .= '<li><a href="javascript:void(0);" onclick="window.location.href=\'?catkey=category&amp;item_type='.$listTag[$t]->id.'\'">'.$listTag[$t]->name.'</a></li>';                    
						if($t==$total_tag){
						
							$tag_str .= '	 </ul>
							</li>';
						
						}
					}
				}//for
						   
			   $tag_str .= ' </ul>';
		   }
		   echo $tag_str;
				exit;
		}catch(Exception $e)
		{			  
			print_r($e->getMessage());
			exit;
		}	
	}
}//class
