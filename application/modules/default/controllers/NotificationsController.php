<?php
class NotificationsController extends PS_Controller_Action
{
    public function preDispatch()
    {
    }

    /// delete single activity(notifications)
    public function deleteactivityAction()
    {
        $db = Zend_Registry::get("db");

        $param = $this->getRequest()->getParams();
        $success = true;
        $activityId = $param['activity_id'];
        try{
            $db->beginTransaction();
            $msg = activity::delete($activityId, $_SESSION['user_id'], $db);
        }catch(Exception $e)
        {
            $msg = $e->getMessage();
            $success = false;
        }
        return $this->_helper->json(array('success' => $success, 'msg' => $msg));
    }

    /// delete multiple activitys(notifications)
    public function deleteactivitiesAction()
    {

        
        $param = $this->getRequest()->getParams();
        $activityIds = explode(',',$param['activity_ids']);
        $success = true;
        $msg = '';
        try{
            $db = Zend_Registry::get("db");
            $db->beginTransaction();
            foreach($activityIds as $id) {
                $msg .= activity::delete($id, $_SESSION['user_id'], $db);
            }
            $db->commit();
        }catch(Exception $e)
        {
            $msg = $e->getMessage();
            $success = false;
        }
        return $this->_helper->json(array('success' => $success, 'msg' => $msg));
    }

    public function resetcounterAction()
    {
        $result = notifications::resetcounter();
        return $this->_helper->json($result);
    }

    public function displaynotificationsAction()
    {

        $this->_helper->layout()->disableLayout();
        $all = $this->getRequest()->getParam("all");
//        return $this->_helper->json(array('success' => 'test'));
//        var_dump($all); exit;
        $this->view->activityLog = notifications::activitylog(!$all);
    }
}