<?php
/*
All web services will go here
these web services are used by Mobile App

Created by Anu
*/

class WebserviceController extends PS_Controller_Action
{

    public function addbookmarkletAction()
    {
        $db = Zend_Registry::get("db");
        $config = Zend_Registry::get("config");
        $server = Zend_Registry::get("server");
        $logger = Zend_Registry::get("logger");

        /*$handle = fopen('php://input','r');
		$jsonInput = fgets($handle);
		$params = json_decode($jsonInput,true);*/

        $params = $this->getRequest()->getParams();
        /*echo '<pre>';
		print_r($params);
		echo '<pre>';
		print_r($_FILES);*/
        $tagId = $params['category'];
        if($params['user_id'] != '' &&  $params['name'] != '' )
        {
            try
            {
                $db->beginTransaction();
                $data['user_id'] = $params['user_id'];
                $data['item_name'] = base64_decode(urldecode($params['name']));
                if($params['description'] != '')
                {
                    $data['description'] = base64_decode(urldecode($params['description']));
                }else{
                    $data['description'] = '';
                }

                if($params['vendor_link'] != '')
                {
                    $data['vendor_link'] = base64_decode(urldecode($params['vendor_link']));
                }else{
                    $data['vendor_link'] = '';
                }

                $data['item_price'] = $params['price'];
                $data['item_count'] = 1;
                $data['item_price_currency'] = $params['currency'];

                if($params['category'] != '')
                {
                    $data['item_type'] = $params['category'];
                }else{
                    //$data['item_type'] = '';
                }

                if($params['image_url'] != '') {
                    $image_url_64 = $params['image_url'];
                    $image_url = base64_decode(urldecode($image_url_64));
                } else {
                    $image_url = "None";
                }
                if($params['added_from'] != '')
                {
                    $data['added_from'] = base64_decode(urldecode($params['added_from']));
                } else {
                    $data['added_from'] = 'manual';
                }
                $data['added_date'] = date('Y-m-d H:i:s');
                $data['item_type'] = '';
                $data['isbn'] = '';
                $data['barcode_itemid'] = '';

                $error_code = 0;
                $pname = $db->quote($data['item_name']);
                
                /* chkExistItem is not consistent need to debug further */
                $chkitem = 0; //wishlistitmes::chkExistItem($pname);
                if($chkitem)
                {
                    $chkUserItem = wishlistitmes::chkusertem($params['user_id'], $chkitem);
                    $datai = array(
                        'item_count'      => new Zend_DB_Expr('item_count + 1')
                    );
                    if(!$chkUserItem)
                    {
                        $data = array();
                        $data['item_id'] = $chkitem;
                        $data['user_id'] = $params['user_id'];
                        $data['added_date'] = date('Y-m-d H:i:s');
                        $db->update('wishlistitmes',$datai,'id='.$chkitem);
                        $data['reserved_by'] = 0;
                        $data['sharewith_group'] = '';
                        $data['share_individual'] = '';
                        $data['gift_state'] = 0;
                        $db->insert('userwishlist',$data);

                        $msg = "Product has been added.";
                        echo json_encode(array("ErrorCode"=>1, "Error"=>$msg));
                        exit;
                    }
                    $logger->info("--- ACCESS-INFO --- ::BOOKMARK-ADD-EXISTING-ITEM::USER-ID::"
                        .$_SESSION['user_id']. "::ITEM-ID::" .$data['item_id']. "::DEVICE-INFO:: ".$_SERVER['HTTP_USER_AGENT'] .
                        "IP ADDRESS ". $_SERVER['REMOTE_ADDR'] ."\n");
                    $msg = "Product has been added already.";
                    echo json_encode(array("ErrorCode"=>1, "Error"=>$msg));
                    exit;
                }else{
                    require 'SimpleImage.php';
                    if($image_url != "None") {
                        //change image name
                        $date = date("Y.m.d");
                        $time = time();

                        $logger->info("Chikke ". $_SERVER["DOCUMENT_ROOT"]);
                        $logger->info("Chikke ". $image_url);

                        $image_name = $params['user_id'] . "_" . $params['price'] . "_" . "bookmarklet".$date.$time;
                        $target_path= $_SERVER["DOCUMENT_ROOT"]."/images/productimg/".$image_name;
                        $thumb_path = $_SERVER["DOCUMENT_ROOT"]."/images/productimg/thumb/".$image_name;
                        
                                                $str = file_get_contents($image_url);
                                                if ($str == NULL) {
                                                         $logger->info("--- CHIKKE  BM--- :: file copy disabled");
                                                } 
                                                if ($str == FALSE) {
                                                         $logger->info("--- CHIKKE  BM--- :: Failed to open the URL");
                                                } 
  
                        $ret = file_put_contents($target_path, file_get_contents($image_url));
                        if ($ret != NULL && $ret != FALSE) {
                            copy($target_path, $thumb_path);
                            $image = new SimpleImage();
                            $image->load($target_path);
                            // resize big image
                            $size = getimagesize($target_path);
                            $width = $size[0];
                            $height = $size[1];
                            if($width > 620)
                            {
                                $image->resizeToWidth(620);
                                $image->save($target_path);
                            }else{
                                if($width < 197)
                                {
                                    $image->resizeToWidth(197);
                                    $image->save($target_path);
                                } else {
                                    $image->save($target_path);
                                }
                            }

                            $image->resizeToWidth(197);
                            $image->save($thumb_path);
                            $data['image'] = $image_name;
                        } else {
                            $error_code = 1; // failed to copy image
                            $data['image'] = 'no-image-available';
                        }
                    }//image upload	

                    $db->insert('wishlistitmes', $data);
                    $item_id = $db->lastInsertId();

                    $user_id = $params['user_id'];
                    if($item_id != '')
                    {
                        $dataW['user_id'] = $params['user_id'];
                        $dataW['item_id'] = $item_id;
                        $dataW['items_required_no'] = $params['items_required_no'];
                        $dataW['added_date'] = date('Y-m-d H:i:s');
                        $dataW['public'] = $params['privacy'];
                        $dataW['reserved_by'] = 0;
                        $dataW['sharewith_group'] = '';
                        $dataW['share_individual'] = '';
                        $dataW['gift_state'] = 0;
                        $db->insert('userwishlist', $dataW);

                        if($tagId != '')
                        {
                            $chkTag = category::checkItemTags($item_id, $tagId, $user_id);
                            //echo "tag:".$chkTag;							
                            if($chkTag)
                            {
                                // this tag is already added
                                //echo "in if:";											
                            }else{
                                // add tag
                                $dataT['item_id'] = $item_id;
                                $dataT['tag_id'] = $tagId;
                                $dataT['user_id'] = $user_id;

                                $db->insert('item_tags',$dataT);
                                $tagRelation_id = $db->lastInsertId();
                                //echo "in else:";
                            }
                        }
                        ///exit;
                        $db->commit();
                        $logger->info("--- ACCESS-INFO --- ::BOOKMARK-ADD-SUCESS::USER-ID::"
                            .$_SESSION['user_id']. "::DEVICE-INFO:: ".$_SERVER['HTTP_USER_AGENT'] .
                            "IP ADDRESS ". $_SERVER['REMOTE_ADDR'] ."\n");


                        $msg = "Product has been added successfully. " . $image_url . " " . $image_url_64 ;
                        echo "gifvise_dataCallback_addproduct(" . json_encode(array("success"=> true, "ErrorCode"=>$error_code, "msg"=>$msg, "item_id" => $item_id, "user_id" => $user_id)).");";
                        exit;
                    }else{
                        $msg = "Error! in adding product, please try again.";
                        echo json_encode(array("ErrorCode"=>1, "Error"=>$msg));
                        exit;
                    }

                }

            }catch(Exception $e)
            {
                $logger->info("--- ACCESS-INFO --- ::BOOKMARK-ADD-FAIL::USER-ID::"
                    .$_SESSION['user_id']. "::DEVICE-INFO:: ".$_SERVER['HTTP_USER_AGENT'] .
                    "IP ADDRESS ". $_SERVER['REMOTE_ADDR'] ."\n");
                //Rollback transaction
                $db->rollBack();
                echo $e->getMessage();
                $this->view->errors[] = $e->getMessage();
                $info["error"] = "Error!";
                echo json_encode(array("ErrorCode"=>1, "Error"=>$info["error"]));
                exit;
            }
        }else{
            $info["error"] = "Required parameter missing!";
            echo json_encode(array("ErrorCode"=>1, "Error"=>$info["error"]));
            //echo '<pre>'; var_dump($_SERVER); echo '</pre>';
            //echo '<pre>'; var_dump($_REQUEST); echo '</pre>';
            echo '<pre>'; var_dump($this->getRequest()->getParams()); echo '</pre>';
            exit;
        }
        exit;
    } //function
}//end class

?>
