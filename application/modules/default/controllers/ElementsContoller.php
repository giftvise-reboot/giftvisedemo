<?php
/*------------------------------------------------------------------ 
 * Copyright (c) 2013-2014 by Giftvise, Inc.
 * All rights reserved. 
 *------------------------------------------------------------------
 */
/**
 * ElementsController
 * 
 * @author
 * @version 
 */

require_once 'Zend/Controller/Action.php';
class ElementsController extends PS_Controller_Action {
	/**
	 * The default action - show the home page
	 */
	public function indexAction() {
		// TODO Auto-generated ElementsController::indexAction() default action
	}
	
	// Load new calendar month
	public function calendarAction()
	{
		$db  = Zend_Registry::get("db");
		$config = Zend_Registry::get("config");
		$server = Zend_Registry::get("server");
		$logger = Zend_Registry::get("logger");
	
		$this->_helper->layout()->disableLayout();
	
		$prev_month = $this->getRequest()->getParam("prev_month");
		$prev_year = $this->getRequest()->getParam("prev_year");
	
		$next_month = $this->getRequest()->getParam("next_month");
		$next_year = $this->getRequest()->getParam("next_year");
	
		if ($prev_month and $prev_month == 12) {
			$logger->info("prev_month = " . $prev_month);
			$event_month = $this->view->month = 1;
			$event_year = $this->view->year = $prev_year + 1;
		} elseif ($prev_month) {
			$logger->info("prev_month = " . $prev_month);
			$event_month = $this->view->month = $prev_month + 1;
			$event_year = $this->view->year = $prev_year;
		} elseif ($next_month and $next_month == 1) {
			$logger->info("next_month = " . $next_month);
			$event_month = $this->view->month = 12;
			$event_year = $this->view->year = $next_year - 1;
		} elseif ($next_month) {
			$logger->info("next_month = " . $next_month);
			$event_month = $this->view->month = $next_month - 1;
			$event_year = $this->view->year = $next_year;
		} else {
			$logger->info("no_month");
			$event_month = $this->view->month = date('n'); //number of current month
			$event_year = $this->view->year = date('Y'); // four digit year
		}

               $events = event::getmonthevents($_SESSION["user_id"],$event_month,$event_year, '');
               $this->view->events = $events;

                $_SESSION['time_zone'] = (isset($_SESSION['time_zone'])) ? $_SESSION['time_zone'] : 'America/New_York';

                try {
                    $timeZone = new DateTimeZone($_SESSION['time_zone']);
                } catch (Exception $e) {
                }

               $this->view->upcoming_events = event::getUpcomingMonthEventsCalendarDates($_SESSION["user_id"], $event_month,$event_year, $timeZone);
	} 
	
	// Load events for selected date
	public function eventsAction()
	{
		$db  = Zend_Registry::get("db");
		$config = Zend_Registry::get("config");
		$server = Zend_Registry::get("server");
		$logger = Zend_Registry::get("logger");
	
		$this->_helper->layout()->disableLayout();
		
		$this->view->month = $this->getRequest()->getParam("month");
		$this->view->date = $this->getRequest()->getParam("date");
		$this->view->year = $this->getRequest()->getParam("year");
		
//		$logger->info($month + $date + $year);
	}

    protected function _initaddevent()
    {
        $eventName = new PS_Validate_NotEmpty($this->_getParam("eventName"));
        $eventName->setMessage(PS_Translate::_("Please enter event name"));

        $eventDate = new PS_Validate_NotEmpty($this->_getParam("eventDate"));
        $eventDate->setMessage(PS_Translate::_("Please enter event date"));

        $eventTime = new PS_Validate_NotEmpty($this->_getParam("eventTime"));
        $eventTime->setMessage(PS_Translate::_("Please enter event time"));

        $eventLocation = new PS_Validate_NotEmpty($this->_getParam("eventLocation"));
        $eventLocation->setMessage(PS_Translate::_("Please enter event location"));

        $this->fields = new PS_Form_Fields(
            array(
                "eventName" => array(
                    $eventName,
                    "required" => true
                ),
                "eventLocation"	=> array(
                    $eventLocation,
                    "required"	=> true
                ),
                "eventDate"	=> array(
                    $eventDate,
                    "required"	=> true
                ),
                "eventTime"	=> array(
                    $eventTime,
                    "required"	=> true
                )
            )
        );

        $this->fields->setData($this->getRequest()->getParams());
        $this->view->fields = $this->fields;
    }

    public function addeventAction()
    {
        $logger = Zend_Registry::get("logger");
        $db 	= Zend_Registry::get("db");
        $config = Zend_Registry::get("config");
        $server = Zend_Registry::get("server");

        if ($_SESSION['logged_in'] != true) {
           $this->_redirect(PS_Util::redirect("https://" . $server->apphost));
           exit;
        }

        $this->_helper->layout()->disableLayout();
        $params=$this->getRequest()->getParams();


        require 'fb/facebook.php';
        Facebook::$CURL_OPTS[CURLOPT_SSL_VERIFYPEER] = false;
        Facebook::$CURL_OPTS[CURLOPT_SSL_VERIFYHOST] = 2;

        $facebook = new Facebook(array(
            'appId'  => $server->FACEBOOK_APP_ID,
            'secret' => $server->FACEBOOK_SECRET,
        ));


        //echo 'if';
        try
        {
            $userfb_id = $facebook->getUser();
            //echo "test:".$userfb_id;
            //exit;
            if($userfb_id )
            {
                $token = $facebook->getAccessToken();
                /*//$me = $facebook->api('/me');
                if($me)
                {
                    $fb_friend = $facebook->api('/me/friends?fields=id,name,picture,link');
                    $this->view->userfb_id = $userfb_id;
                }else{
                    //echo 'else';
                 }*/

                $this->view->userfb_id = $userfb_id;
            }


            $this->_initaddevent();

            if(count($params)>3){

                $db->beginTransaction();
                $this->fields->validate();
                $eventImg = $params['image'];
                if (!empty($eventImg)){
                    //move image
                    $upload = new fileupload();
                    if (!$upload->moveImageFromTemp('event_images', $eventImg)){
                    //    return $this->_helper->json(array('fail' => true, 'msg' => "Fail to upload file please try again"));
                    }
                } else {
                    $eventImg = '';
                }
                $created_date=date("Y-m-d H:i:s");
                date_default_timezone_set($_SESSION['time_zone']);
                $eventDate = DateTime::createFromFormat('m/j/y h:i A',$params["eventDate"].' '.$params["eventTime"]);
                $eventDate = timezone::setGMTFromUserTimeZone($eventDate);
                $now = new DateTime();
                if($eventDate<$now){
                    return $this->_helper->json(array('fail' => true, 'passed'=>true, 'msg' => 'This event could not be in past. Please insert a future date time!'));
                }
                $eventTime = $eventDate->format('H:i');
                $dateTime = $eventDate->format('Y-m-d H:i:s');
                $eventDate = $eventDate->format('Y-m-d');
                $data=array("created_user"=>$_SESSION['user_id'],"title"=>$params["eventName"],"description"=>$params["description"],
                    "location"=>$params["eventLocation"],"event_date"=> $eventDate,"event_time"=>$eventTime,"date_time"=>$dateTime,
                    "created_date"=>$created_date,"updated_date"=>$created_date,"image"=>$eventImg, 'fb_event_id'=>'', 'fb_uid'=>''
                );
                $db->insert('events', $data);
                unset($_POST);
                $db->commit();
                $eventList = event::gettodayevents($_SESSION["user_id"],$eventDate, '');
                $eventDate= explode('-',$eventDate);
                $prev_month = $eventDate[1] - 1;
                $prev_year = $eventDate[0];

                if ($prev_month and $prev_month == 12) {
                    $logger->info("prev_month = " . $prev_month);
                    $this->view->month = 1;
                    $this->view->year = $prev_year + 1;
                } elseif ($prev_month) {
                    $logger->info("prev_month = " . $prev_month);
                    $this->view->month = $prev_month + 1;
                    $this->view->year = $prev_year;
                } else {
                    $logger->info("no_month");
                    $this->view->month = date('n'); //number of current month
                    $this->view->year = date('Y'); // four digit year
                }
                $_SESSION['time_zone'] = (isset($_SESSION['time_zone'])) ? $_SESSION['time_zone'] : 'America/New_York';
                try {
                   $timeZone = new DateTimeZone($_SESSION['time_zone']);
                } catch (Exception $e) {
                }
                $events = event::getUpcomingEventsDates($_SESSION["user_id"], $timeZone);
                $this->view->upcoming_events = $events;
                $this->view->events = $eventList;
                $this->render("calendar");
            }

        }catch(Exception $e)
        {
            $db->rollBack();
            return $this->_helper->json(array('fail' => true, 'msg' => $e->getMessage()));
        }
    }

    public function editeventAction()
    {
        $db 	= Zend_Registry::get("db");
        $config = Zend_Registry::get("config");
        $server = Zend_Registry::get("server");
        $logger = Zend_Registry::get("logger");

        if ($_SESSION['logged_in'] != true) {
           $this->_redirect(PS_Util::redirect("https://" . $server->apphost));
           exit;
        }

        $this->view->headTitle(PS_Translate::_("Edit Event"));
        $this->_helper->layout()->disableLayout();

        $params=$this->getRequest()->getParams();

        $this->_initaddevent();

        if(count($params)>4){
            try
            {
                $db->beginTransaction();
                $this->fields->validate();

                $comment_date=date("Y-m-d H:i:s");
                //Event image uploading
                $eventImg = $params['image'];
                date_default_timezone_set($_SESSION['time_zone']);
                $eventDate = DateTime::createFromFormat('m/j/y h:i A',$params["eventDate"].' '.$params["eventTime"]);
                $eventDate=timezone::setGMTFromUserTimeZone($eventDate);
                $now = new DateTime();
                if($eventDate<$now){
                    return $this->_helper->json(array('fail' => true, 'passed'=>true, 'msg' => 'This event could not be in past. Please insert a future date time!'));
                }
                $eventTime = $eventDate->format('H:i');
                $dateTime = $eventDate->format('Y-m-d H:i:s');
                $eventDate = $eventDate->format('Y-m-d');
                $data =array("title"=>$params["eventName"],"description"=>$params["description"],"location"=>$params["eventLocation"],
                    "event_date"=>$eventDate,"event_time"=>$eventTime, "date_time"=>$dateTime, "updated_date"=>$comment_date);
                if (!empty($eventImg)){
                    //move image
                    $upload = new fileupload();
                    if (!$upload->moveImageFromTemp('event_images', $eventImg)){
                       // return $this->_helper->json(array('fail' => true, 'msg' => "Fail to upload file please try again"));
                    } else {
                        $data["image"]=$eventImg;
                    }
                }
                $db->update('events',$data,'id = '.$params["eventId"]);

                $db->commit();
                $eventList = event::gettodayevents($_SESSION["user_id"],$eventDate, '');
                $eventDate= explode('-',$eventDate);
                $prev_month = $eventDate[1] - 1;
                $prev_year = $eventDate[0];

                if ($prev_month and $prev_month == 12) {
                    $logger->info("prev_month = " . $prev_month);
                    $this->view->month = 1;
                    $this->view->year = $prev_year + 1;
                } elseif ($prev_month) {
                    $logger->info("prev_month = " . $prev_month);
                    $this->view->month = $prev_month + 1;
                    $this->view->year = $prev_year;
                } else {
                    $logger->info("no_month");
                    $this->view->month = date('n'); //number of current month
                    $this->view->year = date('Y'); // four digit year
                }
                $_SESSION['time_zone'] = (isset($_SESSION['time_zone'])) ? $_SESSION['time_zone'] : 'America/New_York';

                try {
                   $timeZone = new DateTimeZone($_SESSION['time_zone']);
                } catch(Exception $e) {
                }

                $this->view->upcoming_events = event::getUpcomingEventsDates($_SESSION["user_id"], $timeZone);
                $this->view->events = $eventList;
                $this->render("calendar");
            }
            catch(Exception $e)
            {
                return $this->_helper->json(array('fail' => true, 'msg' => $e->getMessage()));
            }
        }
    }

    //import facebook event
    public function importfacebookeventAction()
    {

        if ($_SESSION['logged_in'] != true) {
           $this->_redirect(PS_Util::redirect("https://" . $server->apphost));
           exit;
        }

        $this->_helper->layout()->disableLayout();

        require 'fb/facebook.php';
        require 'amazonQuery.php';

        $token = $this->getRequest()->getParam("access_token");

        try
        {
            $facebookEventsErrorMesage = event::getFacebookEvents($token);
            if ($facebookEventsErrorMesage){
                return $this->_helper->json(array('fail' => true, 'msg' => $facebookEventsErrorMesage));
            }
            $_SESSION['time_zone'] = (isset($_SESSION['time_zone'])) ? $_SESSION['time_zone'] : 'America/New_York';
            try {
               $timeZone = new DateTimeZone($_SESSION['time_zone']);
            } catch(Exception $e) {
            }

            $this->view->upcoming_events = event::getUpcomingEventsDates($_SESSION["user_id"], $timeZone);
            $this->view->events = event::gettodayevents($_SESSION["user_id"],'', '');
            $this->view->month = date('n'); //number of current month
            $this->view->year = date('Y'); // four digit year
            $this->render("calendar");
        }
        catch(Exception $e)
        {
            return $this->_helper->json(array('fail' => true, 'msg' => $e->getMessage()));
        }
    }

    public function filterbytaglistAction()
    {
        $db     = Zend_Registry::get("db");
        $config = Zend_Registry::get("config");
        $server = Zend_Registry::get("server");
        $logger = Zend_Registry::get("logger");

        $this->_helper->layout()->disableLayout();
        $itemIds = $this->getRequest()->getParam("item_ids");
        $page = $this->getRequest()->getParam("page");
    
        try {
            if ($page == 'feed' || $page == 'curate') {
               $this->view->filtertags = wishlistitmes::getItemsCategoryList(null, $itemIds);
            } else {
                $this->view->filtertags = wishlistitmes::getUsersTagList($_SESSION['user_id'], $itemIds);
               
            }
        }
        catch(Exception $e) {
            return $this->_helper->json(array('fail' => true, 'msg' => $e->getMessage()));
        }
    }
}
