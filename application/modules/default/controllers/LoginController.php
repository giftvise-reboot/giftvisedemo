<?php
/*
All login operations will go here

created by Anu
*/

class LoginController extends PS_Controller_Action
{

    public function preDispatch()
    {
//        $this->view->headTitle()->prepend(PS_Translate::_(""));
    }

    /// after login this page will be displayed
    public function homeAction()
    {
        $server = Zend_Registry::get("server");
        $logger = Zend_Registry::get("logger");
        $config = Zend_Registry::get("config");

        $logger->info("--- ACCESS-INFO --- ::HOME-PAGE::USER-ID::" . $_SESSION['user_id']. "::DEVICE-INFO::".
            $_SERVER['HTTP_USER_AGENT'] . "IP ADDRESS/ " . $_SERVER['REMOTE_ADDR'] ."\n");

        $logger->info('user_id: ' . $_SESSION['user_id']. ' role: '. $_SESSION['role']);

        $logger->debug("logged_in = " . $_SESSION['logged_in']);
        if ($_SESSION['logged_in'] != true) {
            $this->_redirect(PS_Util::redirect($server->securehttp . $server->apphost));
            exit;
        }

        $this->view->isGuest = (isset($_SESSION['user_id']) && !empty($_SESSION['user_id']) && $_SESSION['user_id'] != '') ? false : true;
        $this->view->firstEnter = false;

        $this->view->nextload = 0;
        $this->view->headTitle()->prepend(PS_Translate::_("Home"));

        $this->view->month = date('n', time());
        $this->view->year = date('Y', time());
        $this->view->date = date('j', time());

        $user_details = users::getUserDetails($_SESSION['user_id']);
        $this->view->userDetail = $user_details;

        if ($user_details->tutorial > 0){
            //send welcome mail on first login
            $this->sendWelcomeMail($user_details->first_name, $user_details->email_id, $config);
            $this->view->firstEnter = true;
        }
   
        $this->pgid = '0';      

        try {

             /* Update user wishlist link id */
             $wishlinkid = $user_details->first_name . $user_details->last_name . $user_details->user_id;
             users::updateUserWishlinkid($user_details->user_id, $wishlinkid);
             
             /* update number of items in this userwishlist */
	     users::updateUserWishlistcount($user_details->user_id);

            /// Get following/friend list
            $followingList = usersfriend::getFollowList($_SESSION['user_id']);
            for ($i = 0; $i < count($followingList); $i++) {
                $flist = array();
                $follower_details = users::getUserDetails($followingList[$i]->friend_id);
                $flist['user_id'] = $follower_details->user_id;
                $flist['username'] = $follower_details->first_name . ' ' . $follower_details->last_name;
                $flist['profile_pic'] = $follower_details->profile_pic;
                $followinglst[] = $flist;
            }

            $this->view->followingList = $followinglst;
            $event_notification = event::getEventNotification($_SESSION["user_id"]);


            if (count($event_notification) > 0) {
                $this->view->e_notification = count($event_notification);
            }

            // notifications
            $this->view->newNotifications = notifications::countnewnotifications();
            $this->view->activityLog = notifications::activitylog(true);
            $this->view->notificationsCount = notifications::notificationscount();


            //$products = wishlistitmes::getTopWeek($orderBy,$catVal);
       $numberOfItems = 5;
       $orderBy = '';
       $catVal = '';
       $curatetype = 3;
       //$products = wishlistitmes::getLoadMoreCuratePage(0,$numberOfItems, $curatetype);
       $products = wishlistitmes::getFeaturedCategoryProducts($_SESSION['user_id'], $orderBy, $catVal,0, $numberOfItems);

       // number of items displayed at a time, need to have single variable
       // or common place to drive this
       $more_items = (count($products) >= $numberOfItems) ? 1 : 0;
       $this->view->more_items = $more_items;
       
       for ($i = 0; $i < count($products); $i++) {
           $item = array();
           $totalComments = itemcomments::countComments($products[$i]->id);
           $userwished = wishlistitmes::chkWishedItem($products[$i]->id, $_SESSION['user_id']);
           $countwishes = wishlistitmes::countWished($products[$i]->id);
           /// latest item
           //$timeAdded = wishlistitmes::latestItem($products[$i]->id);
           $timeAdded = wishlistitmes::latestItemHome($products[$i]->id);
           $addeduser = users::getUserDetails($timeAdded['user_id']);


           ///check this item is already in you wishlist
           //$itemwhished = wishlistitmes::chkWishedItem($product_id,$_SESSION['user_id']);
           $itemwhished = wishlistitmes::chkWishedItem($products[$i]->id, $_SESSION['user_id']);
           if ($itemwhished) {
               $item['youwhished'] = 'Y';
           } else {
               $item['youwhished'] = 'N';
           }
           //

           $item['comment'] = $totalComments;
           $item['wished'] = $userwished;
           $item['countwishes'] = $countwishes;
           $item['added_time'] = $timeAdded['time'];
           //$item['addedby'] = $addeduser->first_name.' '.$addeduser->last_name;
           $item['first_name'] = $addeduser->first_name;
           $item['last_name'] = $addeduser->last_name;
           $item['user_id'] = $addeduser->user_id;

           $items[] = $item;
       }

            $_SESSION['time_zone'] = (isset($_SESSION['time_zone'])) ? $_SESSION['time_zone'] : 'America/New_York';
            try {
                $timeZone = new DateTimeZone($_SESSION['time_zone']);
            } catch (Exception $e) {
            }
            $this->view->events = event::getmonthevents($_SESSION["user_id"], $this->view->month, $this->view->year, '');
            
       $this->view->items = $products;
       $this->view->extval = $items;

       $featured_curators = users::GetFeaturedProductCurators();

       $logger->info("Chikke featured_curators ". count($featured_curators));
       if (count($featured_curators) > 0) {
       $this->view->featured_curators = $featured_curators; //users::GetFeaturedProductCurators();
          $this->view->nocurators = 0;
       } else {
          $this->view->nocurators = 1;
          $this->view->featured_curators = null;
       }
       
       $featured_curators_t = users::GetFeaturedProductCuratorsTop();
        $this->view->featured_curators_t = $featured_curators_t;

        $topCategory = category::getTopTags();
        $logger->info("Chikke top tags count ". count($topCategory));
        $this->view->topCategory = $topCategory;
 
        } catch (Exception $e) {
            print_r($e->getMessage());
            exit;
        }
    }

    /// twitter login
    public function twitterloginAction() {
        $server = Zend_Registry::get("server");
        $logger = Zend_Registry::get("logger");
        $db = Zend_Registry::get("db");

        $logger->info("[START] " . __FILE__ . ':' . __FUNCTION__);

        require 'twitter/twitteroauth.php';
        require 'amazonQuery.php';
        $params = $this->getRequest()->getParams();

        if (!empty($params['oauth_verifier']) && !empty($_SESSION['oauth_token']) && !empty($_SESSION['oauth_token_secret'])) {
            $twitteroauth = new TwitterOAuth($server->Consumer_key, $server->Consumer_secret, $_SESSION['oauth_token'], $_SESSION['oauth_token_secret']);

            $access_token = $twitteroauth->getAccessToken($params['oauth_verifier']);
            $_SESSION['access_token'] = $access_token;
            $user_info = $twitteroauth->get('account/verify_credentials');

            $data['first_name'] = $user_info->name;
            $data['twitter_id'] = $user_info->id;
            $data['twitter_name'] = $user_info->screen_name;
            $data['twitter_image'] = $user_info->profile_image_url;
            $data['created_date'] = date('Y-m-d H:i:s');
            $data['device_type'] = 'web';
            $data['udid']='';$data['facebook_id'] = '';$data['facebook_image'] = '';$data['last_name'] = '';$data['email_id']='';$data['password']='';
            $data['birth_date'] = '1970-01-01';$data['is_pic_updated'] = null;$data['address']='';$data['fbID']='';$data['user_udid']='';
            $data['current_login']=$data['created_date'];$data['last_login']=$data['created_date'];$data['modified_date']='1970-01-01 00:00:00';
            $data['status'] = 0; $data['upcoming_event'] = 1; $data['shared_gift'] = 1; $data['added_followers'] = 1; $data['facebook_notification'] = 0;
            $data['follow_permission'] = 1; $data['wishlinkid'] = 0;$data['facebook_email'] = '';
            //$data['gender'] = 'M';
            ///
            $image_url = $user_info->profile_image_url;
            $path_arr = explode("/", $image_url);

            $image_name = $path_arr[count($path_arr) - 1];

            $target_path = $_SERVER["DOCUMENT_ROOT"] . "/userpics/" . $image_name;
            $thumb_path = $_SERVER["DOCUMENT_ROOT"] . "/userpics/thumb/" . $image_name;

            phpQuery::save_image($image_url, $target_path);
            phpQuery::save_image($image_url, $thumb_path);

            require 'SimpleImage.php';
            $image = new SimpleImage();
            $image->load($thumb_path);

            $size = getimagesize($thumb_path);
            $width = $size[0];
            if ($width > 50) {
                $image->resizeToWidth(50);
                $image->save($thumb_path);
            }
            $data['profile_pic'] = $image_name;

            try {
                $db->beginTransaction();

                $twitter_user = users::getTwitterUser($user_info->id);
                $user_id = $twitter_user->user_id;
                $isTwitterRegister = false;
                if (isset($user_id) AND !empty($user_id)) {
                    $data['current_login'] = date('Y-m-d H:i:s');
                    $data['last_login'] = users::getLastLogin($user_id);
                    $db->update('users', $data, 'user_id = ' . $user_id);
                } else {
                    $db->insert('users', $data);
                    $user_id = $db->lastInsertId();
                    $isTwitterRegister = true;
                }

                $db->commit();
                $_SESSION['user_id'] = $user_id;
                $_SESSION['user_twitter_id'] = $user_info->id;
                $_SESSION['logged_in'] = true;
                $_SESSION['time_zone'] = (isset($_SESSION['time_zone'])) ? $_SESSION['time_zone'] : 'US/Eastern';
                session::write(session_id(), $_SESSION);

                $logger->info("webapp_style = " . $_SESSION['webapp_style']);
                ($isTwitterRegister) ?
                    PS_Util::redirect($server->securehttp.$server->apphost."/login/registersteptwo?id=".$user_id) :
                    PS_Util::redirect($server->securehttp . $server->apphost . "/login/home")
                ;
            } catch (Exception $e) {
                $logger->info("Inside catch");
                $this->view->errors[] = $e;
                var_dump($e->getMessage());exit;
            }

        } else {
            $logger->info("Inside else");
            echo 'in else';
        }

        $logger->info("[END] " . __FILE__ . __FUNCTION__);
    }
    
    //When user opts for twitter login, user will be redirected here
    public function logintwiterAction() {
        $server = Zend_Registry::get("server");
        require 'twitter/twitteroauth.php';
        $twitteroauth = new TwitterOAuth($server->Consumer_key, $server->Consumer_secret);

        $request_token = $twitteroauth->getRequestToken($server->securehttp . $server->apphost . "/login/twitterlogin");

        $_SESSION['oauth_token'] = $request_token['oauth_token'];
        $_SESSION['oauth_token_secret'] = $request_token['oauth_token_secret'];
        $_SESSION['time_zone'] = (isset($_SESSION['time_zone'])) ? $_SESSION['time_zone'] : 'US/Eastern';
        session::write(session_id(), $_SESSION);
        if ($twitteroauth->http_code == 200) {
            // Let's generate the URL and redirect
            $url = $twitteroauth->getAuthorizeURL($request_token['oauth_token']);
            PS_Util::redirect($url);
        } else {
            // It's a bad idea to kill the script, but we've got to know when there's an error.
            die('Something wrong happened.');
        }
    }

    /// Login validation
    protected function _initLogin() {
        $logger = Zend_Registry::get("logger");

        $params['email'] = $this->getRequest()->getPost('email', null);
        $params['password'] = $this->getRequest()->getPost('password', null);

        //$password = new PS_Validate_NotEmpty($this->_getParam("password"));
        $password = new PS_Validate_NotEmpty($params["password"]);
        $password->setMessage(PS_Translate::_("Please enter password"));

        $logger->info("Username = " . $params['email']. " password = ". $params['password']);
        $this->fields = new PS_Form_Fields(
            array(
                "email" => array(
                    new PS_Validate_EmailAddress(),
                    "filter" => new PS_Filter_RemoveSpaces(),
                    "required" => true
                ),
                "password" => array(
                    $password,
                    "required" => true
                )
            )
        );
        //$params = $this->getRequest()->getParams();
        $this->fields->setData($params);
        $this->view->fields = $this->fields;
    }
    /*
     *  Login action
     */
    public function loginAction() {
        $db = Zend_Registry::get("db");
        $config = Zend_Registry::get("config");
        $server = Zend_Registry::get("server");
        $logger = Zend_Registry::get("logger");

        $this->_initLogin();
        $this->errorOnNoscript();
        $params = $this->getRequest()->getParams();

        /*
        $params['email'] = $this->getRequest()->getPost('email', null);
        $params['password'] = $this->getRequest()->getPost('password', null);
        $params['flow_proc'] = $this->getRequest()->getPost('flow_proc', null);
        $params['remember'] = $this->getRequest()->getPost('remember', '0');
        */
        $logger->info("Username = " . $params['email']. " password = ". $params['password']);
        $logger->info("flow_proc = ". $params['flow_proc']. " remember = ". $params['remember']);

        if ($params['flow_proc'] != '' && ($_COOKIE['gift_username'] == $params["email"]) && ($params['flow_proc'] == $params['password'])) {
            $password = base64_decode($params['flow_proc']);
        } else {
            $password = $params["password"];
        }

        try {
            $db->beginTransaction();
            $this->fields->validate();

            $authAdapter = new Zend_Auth_Adapter_DbTable($db->getSlaveConnection(), "users", "email_id", "password", "MD5(?)");

            //Set identity
            $authAdapter->setIdentity($params["email"])->setCredential($password);

            //Authenticate
            $result = $authAdapter->authenticate();

            switch ($result->getCode()) {
                case Zend_Auth_Result::FAILURE_IDENTITY_NOT_FOUND:
                case Zend_Auth_Result::FAILURE_CREDENTIAL_INVALID:

                default:
                    $this->view->loginError = 'Email / Password combination not registered';
                    throw new Exception(PS_Translate::_("Invalid Credentials. Please try again."));
                case Zend_Auth_Result::SUCCESS:
                    $result = $authAdapter->getResultRowObject();
                    $userID = $result->user_id;

                    if ($result->status == 1) {
                        $this->view->loginError = 'User is not activated';
                        throw new Exception(PS_Translate::_("User is not activated"));
                    } else {

                        $_SESSION['user_id'] = $userID;
                        $_SESSION['logged_in'] = 'true';
                        $_SESSION['logout_url'] = $server->securehttp . $server->apphost . "/login/logout";
                        $_SESSION['login_type'] = 'GiftVise';
                        $_SESSION['time_zone'] = (isset($_SESSION['time_zone'])) ? $_SESSION['time_zone'] : 'US/Eastern';
                        session::write(session_id(), $_SESSION);
                        $db->commit();

                        // set remember me
                        // TODO: doing a base64 encoding of password and storing it in a cookie
                        // is really bad!!!!! Fix this before release!!
                        if (isset($params["remember"]) && $params["remember"] == 'on') {
                            setcookie('gift_username', $params["email"], time() + (60 * 60 * 24 * 7));
                            setcookie('gift_password', base64_encode($password), time() + (60 * 60 * 24 * 7));
                            $logger->info("Saved credentials in cookie");

                            $logger->info("cookie['gift_username'] = " . $_COOKIE['gift_username']);
                            $logger->info("cookie['gift_password'] = " . $_COOKIE['gift_password']);
                        } else {
                            //setcookie ('gift_username',0, time()-1000);
                            //setcookie ('gift_username', 0, time()-1000); 
                            $logger->info("Did not save cookie");
                        }
                        return $this->_helper->json(array('success' => true));
                    }
            }
        } catch (Exception $e) {
            $msg = $e->getMessage();
            $logger->info("Login error = " . $msg);
            return $this->_helper->json(array('success' => false, 'msg' => $msg));
        }
    }
    /// signup validation
    protected function _initSignup() {

        $this->fields = new PS_Form_Fields(
            array(
                "email_id" => array(
                    new PS_Validate_EmailAddress(),
                    "filter" => new PS_Filter_RemoveSpaces(),
                    "required" => true
                ),
                "password" => array(
                    //$password,
                    new PS_Validate_Password(array("min" => 6, "digitRequired" => true)),
                    "required" => true
                )
            )
        );
        $params = $this->getRequest()->getParams();
        $this->fields->setData($params);
        $this->view->fields = $this->fields;
    }

    ///local sign-up
    public function signupAction() {
        $db = Zend_Registry::get("db");

        $params = $this->getRequest()->getParams();
        $this->_initSignup();
        $this->errorOnNoscript();
        $success = false;
        $passwordError = '';
        $user_id = '';
        
        try {
            $db->beginTransaction();
            $this->fields->validate();
            $emailExist = users::checkExistsEmail($params['email_id']);

            if (empty($emailExist)) {
                //add user
                //$data = $params;
                $data['first_name'] = '';
                $data['last_name'] = '';
                $data['email_id'] = trim($params["email_id"]);
                $data['gender'] = 'M';
                $data['status'] = 0;//no need for activate account
                $birthDate = '1970-01-01';
                $data['birth_date'] = $birthDate;
                $data['created_date'] = date('Y-m-d H:i:s');
                $data['password'] = md5($params["password"]);
                $data['device_type'] = 'web';
                $data['udid'] = '';
                $data['twitter_name'] = '';
                $data['twitter_id'] = '';
                $data['twitter_image'] = '';
                $data['facebook_id'] = '';
                $data['facebook_image'] = '';
                $data['address'] = '';
                $data['fbID'] = '';
                $data['modified_date'] = '1970-01-01 00:00:00';
                $data['current_login'] = date('Y-m-d H:i:s');
                $data['last_login'] = date('Y-m-d H:i:s');
                $data['shared_gift'] = '0';
                $data['added_followers'] = '0';
                $data['facebook_notification'] = '0';
                $data['upcoming_event'] = '0';
                $data['user_udid'] = '';
                $data['profile_type'] = 'Public';
                $data['follow_permission'] = '1';
                $data['facebook_email'] = '';
                $data['wishlinkid'] = 0;
                $data['total_wishlist_items'] = 0;
                $data['blog_link'] = '';

                $data['profile_pic'] = 'default-profile-pic.png';

                $db->insert('users', $data);
                $user_id = $db->lastInsertId();

                $db->commit();

                if (isset($user_id) && $user_id > 0) {
                    $msg = 'You have successfully registered with Giftvise! Please check your email to complete your registration. Happy Gifting!';
                    $success = true;
                    // follow friend reciprocal
                    if ($params['friend_id']) {
                        $friendId = users::getUserIdByHashCode($params['friend_id']);
                        if ($friendId != '') {
                            usersfriend::followFriend($friendId, $user_id, true);
                            usersfriend::followFriend($user_id, $friendId, true);
                        }
                    }
                    //add to the group
                    if ($params['group_id']) {
                        $groupId = groups::getGroupIdByHashCode($params['group_id']);
                        if ($groupId != '') {
                            groups::addFriendToGroup($user_id, $groupId);
                        }
                    }
                } else {

                    $msg = 'Error in processsing, please try again later.';
                }
            } else {
                $emailExist = true;
                $msg = 'THIS EMAIL IS ALREADY REGISTERED, PLEASE SIGN IN';
            }
        } catch (Exception $e) {
            //Rollback transaction
            $db->rollBack();

            $msg = $e->getMessage();
            $passwordError = substr($this->view->printError("password"), 32, -6);
        }
        return $this->_helper->json(array('success' => $success, 'msg' => $msg, 'pwdErr' => $passwordError, 'id' => $user_id, 'emailExist' => $emailExist));
    }

    public function registerAction() {
        $this->view->headTitle()->prepend(PS_Translate::_("Sign up"));
        $this->_helper->layout()->disableLayout();
        $server = Zend_Registry::get("server");
        $params = $this->getRequest()->getParams();
        $this->view->email = $params['email'];
        $this->view->friend_id = $params['friend_id'];
        $this->view->group_id = $params['group'];
        $this->view->fbAppID = $server->FACEBOOK_APP_ID;
    }

    public function registersteptwoAction() {
        $server = Zend_Registry::get("server");
        $this->view->headTitle()->prepend(PS_Translate::_("Almost there"));
        $params = $this->getRequest()->getParams();
        $userDetails = users::getUserDetails($params['id']);
        $this->view->userDetails = $userDetails;
        if (!empty($userDetails->first_name) && ($userDetails->birth_date != '1970-01-01') && (($userDetails->gender == 'M') || ($userDetails->gender == 'F'))) {
            //we have almost there data
            $this->_redirect(PS_Util::redirect($server->securehttp.$server->apphost."/login/registerstepthree?id=".$userDetails->user_id ));
        }
        $this->_helper->layout()->disableLayout();
    }
    
    protected function _initSignupStepTwo() {

        $this->fields = new PS_Form_Fields(
            array(
                "user_id" => array(
                    "required" => true
                ),
                "name" => array(
                    "required" => true
                ),
                "dob" => array(
                    "required" => true
                ),
                "gender" => array(
                    "required" => true
                )
            )
        );
        $params = $this->getRequest()->getParams();
        $this->fields->setData($params);
        $this->view->fields = $this->fields;
    }

    public function signupsteptwoAction() {

        $db = Zend_Registry::get("db");
        $config = Zend_Registry::get("config");
        $server = Zend_Registry::get("server");

        $params = $this->getRequest()->getParams();
        $userId = $params['user_id'];
        $this->_initSignupStepTwo();
        $this->errorOnNoscript();
        $success = false;
        try {
            $db->beginTransaction();
            $this->fields->validate();

            $user = users::get($userId);
            if ($user) {
                $data['first_name'] = $params['name'];
                $data['birth_date'] = date('Y-m-d', strtotime($params['dob']));
                $data['gender'] = $params['gender'];
                
                $db->update('users', $data, 'user_id = ' . $userId);

                event::addBirthdayEvents($userId, $data['birth_date'], $data['first_name']);
                
                notifications::addWellcomeNotification($userId);
                ////
                $request = usersfriend::checkRquest($params['email_id']);
                if (!empty($request)) {
                    $requestedUserid = $request->user_id;
                    $frienddata['user_id'] = $requestedUserid;
                    $frienddata['friend_id'] = $params['user_id'];
                    $frienddata['status'] = '1';

                    $db->insert('usersfriend', $frienddata);
                }
                $db->commit();
                //write user in session
                $_SESSION['user_id'] = $userId;
                $_SESSION['logged_in'] = 'true';
                $_SESSION['logout_url'] = $server->securehttp . $server->apphost . "/login/logout";
                $_SESSION['login_type'] = 'GiftVise';
                $_SESSION['time_zone'] = (isset($_SESSION['time_zone'])) ? $_SESSION['time_zone'] : 'US/Eastern';
                session::write(session_id(), $_SESSION);
                
                $msg = 'You have successfully registered with Giftvise! Please check your email to complete your registration. Happy Gifting!';
                $success = true;
            } else {
                $msg = 'User invalid id';
            }
        } catch (Exception $e) {
            //Rollback transaction
            $db->rollBack();

            $msg = $e->getMessage();
            $dobError = substr($this->view->printError("birth_date"), 32, -6);
        }
        return $this->_helper->json(array('success' => $success, 'msg' => $msg, 'dobErr' => $dobError, 'id' =>$userId));
    }

    public function registerstepthreeAction() {
        $this->view->headTitle()->prepend(PS_Translate::_("Life's events"));
        $params = $this->getRequest()->getParams();
        $this->view->userId = $params['id'];
        $this->_helper->layout()->disableLayout();
    }

    protected function _initSignupStepThree($params) {
        $eventsCount = $params['events_count'];
        $errors = array();
        for ($i=1; $i<=$eventsCount; $i++) {
            $eventI = 'event-' . $i;
            if (empty($params[$eventI])) {
                array_push($errors, array('field' => $eventI, 'errMsg' => 'This field is required'));
            } else {
                $haveSameNameErr = false;
                for ($j=$i + 1; $j<=$eventsCount; $j++) {
                    $eventJ = 'event-' . $j;
                    if ($params[$eventI] === $params[$eventJ]) {
                        array_push($errors, array('field' =>$eventJ, 'errMsg' => 'Event names should be unique'));
                        if (!$haveSameNameErr) {
                            array_push($errors, array('field' => $eventI, 'errMsg' => 'Event names should be unique'));
                            $haveSameNameErr = true;
                        }
                    }
                }
            }
            $eventDate = 'event-date-' . $i;
            if (empty($params[$eventDate])) {
                array_push($errors, array('field' => $eventDate, 'errMsg' => 'This field is required'));
            }
        }
        return $errors;
    }
    
    public function signupstepthreeAction() {
        $params = $this->getRequest()->getParams();
        $userId = $params['user_id'];
        $success = false;
        $msg = 'Events created successfully!';
        $errors = $this->_initSignupStepThree($params);
        if (empty($errors)) {
            try {
                $eventsCount = $params['events_count'];
                $user = users::get($userId);
                if (!empty($user)) {
                    for ($i=1; $i<=$eventsCount; $i++) {
                        $eventName = $params['event-' . $i];
                        $eventDate = $params['event-date-' . $i];
                        if (!empty($eventName) && !empty($eventDate)){
                            (strpos($eventName, 'Anniversary')) ?
                                event::addAnniversaryEvents($userId, $eventDate, $eventName) :
                                event::addEvent($userId, $eventDate, $eventName, 'life-event')
                            ;
                        }
                    }
                    $success = true;
                }
            } catch (Exception $e) {
                $msg = $e->getMessage();
            }
        } 
        return $this->_helper->json(array('success' => $success, 'msg'=> $msg, 'errors' => $errors));
        
    }
    /*
     *  logout process
     */
    public function logoutAction() {
        $db = Zend_Registry::get("db");
        $server = Zend_Registry::get("server");

        $db->beginTransaction();
        session::destroy(session_id());
        $db->commit();
        unset($_SESSION);
        session_destroy();

        $this->_redirect(PS_Util::redirect($server->securehttp . $server->apphost."/"));
    }
    
    public function forgotAction() {
        $this->_helper->layout()->disableLayout();
    }

    public function forgotprocessAction() {
        $db = Zend_Registry::get("db");
        $config = Zend_Registry::get("config");
        $server = Zend_Registry::get("server");

        $params = $this->getRequest()->getParams();

        $email = $params['email'];
        $success = false;
        if (empty($email)) {
            $msg = 'Please enter your Email';
        } else {
            try {
                $db->beginTransaction();
                $emailExist = users::checkExistsEmail($email);
                //$email = 'anu.sagi03@gmail.com';
                if ($emailExist) {
                    ///generate random password
                    $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
                    $pass = array();
                    for ($i = 0; $i < 8; $i++) {
                        $n = rand(0, strlen($alphabet) - 1);
                        $pass[$i] = $alphabet[$n];
                    }
                    $password = implode($pass);
                    $data['password'] = md5($password);

                    $result = $db->update('users', $data, 'user_id = ' . $emailExist);

                    if ($result) {
                        //send email
                        $userName = users::getName($emailExist);

                        $mailText = 'Hi ' . $userName . ',<br>Your new password is - <b>' . $password . '</b>, please login with new password.
						<br><br>
						<a href="' . $server->securehttp . $server->apphost . '">Click here</a> to login.

						<br><br> Thanks,<br>GiftVise';

                        $transport = new Zend_Mail_Transport_Sendmail($config->supportEmail);
                        Zend_Mail::setDefaultTransport($transport);
                        $mail = new Zend_Mail();
                        $mail->setSubject('Forgot Password - GiftVise');
                        $mail->setFrom($config->supportEmail, 'Giftvise');
                        $mail->addTo($email);
                        //$mail->setBodyText($mailText);
                        $mail->setBodyHtml($mailText);
                        $mail->send();
                        $success = true;
                        $msg = 'An email has been sent to your email-id with the new password.';
                    } else {
                        $msg = 'Error in processing, please try again later.';
                    }
                } else {
                    $msg = 'Account does not exist';
                }
                $db->commit();
            } catch (Exception $e) {
                $db->rollBack();
                $msg = $e->getMessage();
            }
        }
        $this->_helper->json(array('success' => $success, 'msg' => $msg));
    }
    
    private function sendWelcomeMail($name, $email, $config) {
        $mailMsg = "Hi " . $name . ",<br><br>
                
                Welcome to Giftvise.<br><br>
                
                Happy gifting!<br>
                Giftvise
                ";
        $transport = new Zend_Mail_Transport_Sendmail($config->supportEmail);
        Zend_Mail::setDefaultTransport($transport);
        $mail = new Zend_Mail();
        $mail->setSubject('Registration - GiftVise');
        $mail->setFrom($config->supportEmail, 'Giftvise');
        $mail->addTo($email, $email);
        $mail->setBodyHtml($mailMsg);
//        $mail->send();
    }

    public function updateaboutmeAction()
    {
        $db = Zend_Registry::get("db");

        $params = $this->getRequest()->getParams();
        $this->errorOnNoscript();
        $success = false;
        try {
            $db->beginTransaction();
            $data['about_me'] = $params["about-me-info"];
            $user_id = $_SESSION['user_id'];
            $db->update('users', $data, 'user_id = ' . $user_id);
            $db->commit();

            if (isset($user_id) && $user_id > 0) {
                $msg = 'You have successfully updated your profile!';
                $success = true;
            } else {
                $msg = 'Error in processsing, please try again later.';
            }
        } catch (Exception $e) {
            //Rollback transaction
            $db->rollBack();
            $msg = $e->getMessage();
        }
        return $this->_helper->json(array('success' => $success, 'msg' => $msg));
    }

    public function updatebloglinkAction()
    {
        $db = Zend_Registry::get("db");

        $params = $this->getRequest()->getParams();
        $this->errorOnNoscript();
        $success = false;
        try {
            $db->beginTransaction();
            $data['blog_link'] = $params["bloglink"];
            $user_id = $_SESSION['user_id'];
            $db->update('users', $data, 'user_id = ' . $user_id);
            $db->commit();

            if (isset($user_id) && $user_id > 0) {
                $msg = 'You have successfully updated your profile!';
                $success = true;
            } else {
                $msg = 'Error in processsing, please try again later.';
            }
        } catch (Exception $e) {
            //Rollback transaction
            $db->rollBack();
            $msg = $e->getMessage();
        }
        return $this->_helper->json(array('success' => $success, 'msg' => $msg));
    }

}
