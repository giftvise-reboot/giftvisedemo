<?php

/*
All login operations will go here

created by Anu
*/

class WishlistController extends PS_Controller_Action
{

    public function preDispatch()
    {
//        $this->view->headTitle()->prepend(PS_Translate::_(""));
    }

    /// after login this page will be displayed
    public function mywishlistAction()
    {
        $server = Zend_Registry::get("server");
        $logger = Zend_Registry::get("logger");

        $logger->info("--- ACCESS-INFO --- ::HOME-PAGE::USER-ID::" . $_SESSION['user_id'] . "::DEVICE-INFO::" .
            $_SERVER['HTTP_USER_AGENT'] . "IP ADDRESS/ " . $_SERVER['REMOTE_ADDR'] . "\n");

        $logger->info('user_id: ' . $_SESSION['user_id'] . ' role: ' . $_SESSION['role']);

        $logger->debug("logged_in = " . $_SESSION['logged_in']);
        if ($_SESSION['logged_in'] != true) {
            $this->_redirect(PS_Util::redirect($server->securehttp . $server->apphost));
            exit;
        }

        $this->view->pgid = '1';

        $this->view->nextload = 0;
        $this->view->headTitle()->prepend(PS_Translate::_("My Wishlist"));
        $this->view->isGuest = (isset($_SESSION['user_id']) && !empty($_SESSION['user_id']) && $_SESSION['user_id'] != '') ? false : true;
        $this->view->firstEnter = false;

        $this->view->month = date('n', time());
        $this->view->year = date('Y', time());
        $this->view->date = date('j', time());

        $user_details = users::getUserDetails($_SESSION['user_id']);
        $this->view->userDetail = $user_details;
        $this->view->rank_pos = users::GetUserRanking($_SESSION['user_id']);

        try {

             /* update number of items in this userwishlist */
             users::updateUserWishlistcount($user_details->user_id);

            /// Get following/friend list
            $followingList = usersfriend::getFollowList($_SESSION['user_id']);
            for ($i = 0; $i < count($followingList); $i++) {
                $flist = array();
                $follower_details = users::getUserDetails($followingList[$i]->friend_id);
                $flist['user_id'] = $follower_details->user_id;
                $flist['username'] = $follower_details->first_name . ' ' . $follower_details->last_name;
                $flist['profile_pic'] = $follower_details->profile_pic;
                $followinglst[] = $flist;
            }

            $this->view->followingList = $followinglst;
            $event_notification = event::getEventNotification($_SESSION["user_id"]);


            if (count($event_notification) > 0) {
                $this->view->e_notification = count($event_notification);
            }

            // notifications
            $this->view->newNotifications = notifications::countnewnotifications();
            $this->view->activityLog = notifications::activitylog(true);
            $this->view->notificationsCount = notifications::notificationscount();


            //$products = wishlistitmes::getTopWeek($orderBy,$catVal);
            $numberOfItems = 10;
            $orderBy = '';
            $catVal = '';
            $products = wishlistitmes::getLoadMoreHomePage($_SESSION['user_id'], $orderBy, $catVali, 0, $numberOfItems);

            // number of items displayed at a time, need to have single variable
            // or common place to drive this
            if (count($products) > 0) {
                $this->view->more_items = 1;
            } else {
                $this->view->more_items = 0;
            }

            $noids = true;
            $productIds = array();
            for ($i = 0; $i < count($products); $i++) {
                $productIds[] = $products[$i]->id;
                $noids = false;
            }
            if ($noids == false) {
                $this->view->filtertags = wishlistitmes::getUsersTagList($_SESSION['user_id'], "('" . implode("','", $productIds) . "')");
            }

            $_SESSION['time_zone'] = (isset($_SESSION['time_zone'])) ? $_SESSION['time_zone'] : 'US/Eastern';
            try {
                $timeZone = new DateTimeZone($_SESSION['time_zone']);
            } catch (Exception $e) {
            }
            $this->view->events = event::getmonthevents($_SESSION["user_id"], $this->view->month, $this->view->year, '');

            $this->view->items = $products;
            $this->view->extval = wishlistitmes::prepareExtVal($products, $_SESSION['user_id']);

        } catch (Exception $e) {
            print_r($e->getMessage());
            exit;
        }
    }

    public function wishlistAction()
    {
        $db = Zend_Registry::get("db");
        $config = Zend_Registry::get("config");
        $server = Zend_Registry::get("server");
        $logger = Zend_Registry::get("logger");

        $logger->info("--- ACCESS-INFO --- ::FRIEND-PROFILE-PAGE::USER-ID::" . $_SESSION['user_id'] . " DEVICE-INFO :: " . $_SERVER['HTTP_USER_AGENT'] . " IP ADDRESS/ " . $_SERVER['REMOTE_HOST'] . "\n");

        if ($_SESSION['logged_in'] != true) {
            $this->_redirect(PS_Util::redirect("https://" . $server->apphost));
            exit;
        }
        $params = $this->getRequest()->getParams();
        $friend_id = $params['friend_id'];
        $shared = $params['shared'];
        //$shared = (isset($params['shared'])) ? $params['shared'] : 0;
        $catVal = $params['item_type'];
        $wishlinkid = $params['wid'];

        $this->view->pgid = '2';

        $this->view->isGuest = (isset($_SESSION['user_id']) && !empty($_SESSION['user_id']) && $_SESSION['user_id'] != '') ? false : true;
        $this->view->firstEnter = false;

        try {
            //user
            $user_details = users::getUserDetails($_SESSION['user_id']);
            $this->view->userDetail = $user_details;

            if ($friend_id != '' || $friend_id != null) {
                $friend_details = usersfriend::frienDeatil($friend_id);
            } else if ($wishlinkid != '') {
                $friend_details = usersfriend::frienDeatilByWlink($wishlinkid);
            }

             /* update number of items in this userwishlist */
             users::updateUserWishlistcount($friend_details->user_id);

            $_SESSION['friend_id'] = $friend_details->user_id;
            session::write(session_id(), $_SESSION);

            /// follows
            $followCount = usersfriend::countFollowList($friend_details->user_id);
            $this->view->follow = $followCount;

            /// followers
            $followerCount = usersfriend::countFollowerList($friend_details->user_id);
            $this->view->followers = $followerCount;

            $this->view->friendDetail = $friend_details;

            // total gift send by user
            $this->view->giftsend = event::giftSend($friend_details->user_id);

            // total gift recived by user
            $this->view->giftget = event::giftGet($friend_details->user_id);

            //user tags
            $category = category::showTag($friend_details->user_id);
            $this->view->tags = $category;

            /// friend wishlist
            if (!$shared) {
                $numberOfItems = 20;
                $this->view->next_start = $numberOfItems;
                $products = wishlistitmes::getLoadMoreFriendPage($friend_details->user_id, $orderBy, $catVal, 0, $numberOfItems);
                $more_items = (count($products) >= $numberOfItems) ? 1 : 0;
                $this->view->more_items = $more_items;
            } else {
                $itemIds = $params['itemIds'];
                $products = wishlistitmes::getSharedItemsLandingPage($friend_details->user_id, $itemIds);
                $this->view->more_items = 0;
            }

            $this->view->vpage = 4;
            /*
            $this->view->more_items = 0;
            $products = wishlistitmes::getAllProducts($friend_details->user_id, $orderBy, $catVal);
            */
            $isfollower = usersfriend::checkExistFriendStatus($friend_details->user_id, $_SESSION['user_id']);
            $tmp_follow = ($isfollower != '' && $isfollower->status == '1') ? 'Y' : 'N';
            $this->view->isfollower = $tmp_follow;
            $this->view->request_status = usersfriend::getRequestStatus($friend_details->user_id, $_SESSION['user_id']);

            $this->view->rank_pos = users::GetUserRanking($friend_details->user_id);

            /* filter bar tags */
            $noids = true;
            $productIds = array();
            for ($i = 0; $i < count($products); $i++) {
                $productIds[] = $products[$i]->id;
                $noids = false;
            }
            if ($noids == false) {
                $this->view->filtertags = wishlistitmes::getUsersTagList($friend_details->user_id, "('" . implode("','", $productIds) . "')");
            }
            if ($tmp_follow == 'Y') {
                $this->view->filter_bar_events = event::getUserCreatedEvents($friend_details->user_id);
            } else {
                $this->view->filter_bar_events = null;
            }
            //user's created groups
            $groups = groups::groupList($_SESSION['user_id']);
            $friendGroups = groups::getFriendsGroup($_SESSION['user_id'], $friend_details->user_id);

            /*echo '<pre>';
            print_r($items);
            echo '</pre>';*/

            // user's activity
            $activity = users::getActivity($friend_details->user_id);

            $activityLog = array();
            for ($i = 0; $i < count($activity); $i++) {
                $listA = array();

                $sender = users::getUserDetails($activity[$i]->sender);
                $receiver = users::getUserDetails($activity[$i]->receiver);
                $event_title = event::getEventTitle($activity[$i]->event_id);

                $item_name = wishlistitmes::getProductName($activity[$i]->id);

                $time = users::getTime(strtotime($activity[$i]->activity_date));

                $listA['sender_id'] = $activity[$i]->sender;
                $listA['sender_name'] = $sender->first_name . ' ' . $sender->last_name;
                $listA['sender_image'] = $sender->profile_pic;
                $listA['event_title'] = $event_title;
                $listA['item_name'] = $item_name;
                $listA['item_id'] = $activity[$i]->id;

                $listA['receiver_id'] = $activity[$i]->receiver;
                $listA['receiver_name'] = $receiver->first_name . ' ' . $receiver->last_name;
                $listA['receiver_image'] = $receiver->profile_pic;

                $listA['activity_type'] = $activity[$i]->activity_type;
                $listA['activity_time'] = $time;

                $activityLog[] = $listA;
            }

            /// All activity
            $allActivity = users::getAllActivity($friend_details->user_id);


            for ($i = 0; $i < count($allActivity); $i++) {
                $listA = array();

                $sender = users::getUserDetails($allActivity[$i]->sender);
                $event_title = event::getEventTitle($allActivity[$i]->event_id);
                $receiver = users::getUserDetails($allActivity[$i]->receiver);

                $time = users::getTime(strtotime($allActivity[$i]->activity_date));

                $item_name = wishlistitmes::getProductName($allActivity[$i]->id);

                $listA['id'] = $allActivity[$i]->id;
                $listA['item_id'] = $allActivity[$i]->id;
                $listA['item_name'] = $item_name;
                $listA['event_title'] = $event_title;
                $listA['sender_id'] = $allActivity[$i]->sender;
                $listA['sender_name'] = $sender->first_name . ' ' . $sender->last_name;
                $listA['sender_image'] = $sender->profile_pic;

                $listA['receiver_id'] = $allActivity[$i]->receiver;
                $listA['receiver_name'] = $receiver->first_name . ' ' . $receiver->last_name;
                $listA['receiver_image'] = $receiver->profile_pic;

                $listA['activity_type'] = $allActivity[$i]->activity_type;
                $listA['activity_time'] = $time;

                $allActivityList[] = $listA;
            }
            $this->view->allActivityLog = $allActivityList;

            $isfollow = usersfriend::checkExistFriendStatus($_SESSION['user_id'], $friend_details->user_id);

            if ($isfollow != '') {
                if ($isfollow->status == '1') {
                    $status = 'Unfollow';
                }
                if ($isfollow->status == '0') {
                    $status = 'Pending';
                }
            } else {
                $status = 'Follow';
            }
            ///
            $this->view->status = $status;

            $this->view->activityLog = $activityLog;

            $this->view->friendGroups = $friendGroups;
            $this->view->usersgroups = $groups;

            $createdGroups = groups::groupListWithCheckFriend($_SESSION['user_id'], $friend_details->user_id);
            $this->view->created_groups = $createdGroups;
            $this->view->items = $products;
            $this->view->extval = wishlistitmes::prepareExtVal($products, $friend_details->user_id);
            // notifications
            $this->view->newNotifications = notifications::countnewnotifications();
            $this->view->activityLog = notifications::activitylog(true);
            $this->view->notificationsCount = notifications::notificationscount();

            $_SESSION['friend_id'] = $friend_details->user_id;
            session::write(session_id(), $_SESSION);


            $db->beginTransaction();

            $datai = array(
                'profile_views' => new Zend_DB_Expr('profile_views + 1'),
                'influence_score' => new Zend_DB_Expr('influence_score + 1')
            );

            $db->update('users', $datai, 'user_id = ' . $friend_details->user_id);
            $db->commit();

        } catch (Exception $e) {
            print_r($e->getMessage());
            $this->view->errors[] = $e->getMessage();
        }
    }

    public function friendsAction()
    {
        $server = Zend_Registry::get("server");

        if ($_SESSION['logged_in'] != true) {
            $this->_redirect(PS_Util::redirect("https://" . $server->apphost));
            exit;
        }

        if ($_SESSION['show_facebook_friends']) {
            $_SESSION['show_facebook_friends'] = false;
            session::write(session_id(), $_SESSION);
            $this->_redirect(PS_Util::redirect("https://" . $server->apphost . "/friends/invitefacebook"));
            exit;
        }

        $params = $this->getRequest()->getParams();
        $key = $params['key'];

        //user
        $user_details = users::getUserDetails($_SESSION['user_id']);
        $this->view->userDetail = $user_details;

        $groups = groups::groupList($_SESSION['user_id']);
        $this->view->groups = $groups;
        /* disable it in alpah0.1 */
        /*
        $groups = groups::getFriendsGroupIBelong($_SESSION['user_id']);
        $this->view->GroupIBelong = $groups;
        */
        $this->view->GroupIBelong = null;
        $this->view->allfriends = usersfriend::prepareForViewAllFriends($key);
        // notifications
        $this->view->newNotifications = notifications::countnewnotifications();
        $this->view->activityLog = notifications::activitylog(true);
        $this->view->notificationsCount = notifications::notificationscount();
        $this->view->fbAppID = $server->FACEBOOK_APP_ID;
    }

    // add new group
    public function addoreditgroupAction()
    {
        $db = Zend_Registry::get("db");
        $config = Zend_Registry::get("config");
        $server = Zend_Registry::get("server");
        $logger = Zend_Registry::get("logger");

        $params = $this->getRequest()->getParams();
        // $this->_initnewgroup();
        $groupName = $params['group_name'];
        $name = $db->quote($params['group_name']);
        $groupId = $params['group_id'];
        $group_desc = $params['group_description'];
        //$logger->info("Chikke ".  $group_desc );
        try {
            /*        $existGroup = groups::checkExistGroup($_SESSION['user_id'],$name);
               if($existGroup && empty($groupId))
               {
                     return $this->_helper->json(array('success' => false, 'msg' => 'This group name is already added, please try with other name'));
               }else{
            */
            $db->beginTransaction();
            /*
             $this->fields->validate();
             //move image from temp to group
             $upload = new fileupload();
             if ($upload->moveImageFromTemp('groupimg', $params['image'])){
                 $Gdata['group_image'] = $params['image'];
             } else if (empty($groupId)){
                 //return $this->_helper->json(array('success' => false, 'msg' => "Fail to upload file please try again"));
                 $Gdata['group_image'] = '';
             }
             */
            $Gdata['group_name'] = $groupName;
            $Gdata['description'] = $group_desc;
            //  if (empty($groupId) || 1){
            $Gdata['created_by'] = $_SESSION['user_id'];
            $Gdata['created_date'] = date('Y-m-d');

            $db->insert('groups', $Gdata);
            $Gdata1['user_id'] = $_SESSION['user_id'];
            $group_id = $db->lastInsertId();
            $Gdata1['group_id'] = $group_id;
            //  $logger->info("Chikke ". $Gdata['group_name'] . ' '. $Gdata['description']);                   
            $db->insert('friendsgroup', $Gdata1);
            //  } else {
            //      $db->update('groups', $Gdata, 'id = '. $groupId);
            //  }
            /// add this user in this group

            $db->commit();
            $msg = "Group has been " . (empty($groupId) ? 'added' : 'modified') . " successfully";
            return $this->_helper->json(array('success' => true, 'msg' => $msg, 'id' => $itemId, 'name' => $groupName, 'img' => $params['image'], 'description' => $group_desc));
            // }

        } catch (Exception $e) {
            //Rollback transaction
            $db->rollBack();
            return $this->_helper->json(array('success' => false, 'msg' => $e->getMessage()));
        }
    }

}
