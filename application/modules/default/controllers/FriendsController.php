
<?php
/*------------------------------------------------------------------ 
 * Copyright (c) 2013-2014 by Giftvise, Inc.
 * All rights reserved. 
 *------------------------------------------------------------------
 */
?>

<?php
/*
All friend related operation will be handelled from here
created by Anu
*/

class FriendsController extends PS_Controller_Action
{
	public function preDispatch()
	{
		$this->view->headTitle()->prepend(PS_Translate::_("Friends"));
	}

	/*
	Display friendlist of loggedin user
	display notification - new followers
	*/
	public function indexAction()
	{
		$server = Zend_Registry::get("server");

		if ($_SESSION['logged_in'] != true) {
			$this->_redirect(PS_Util::redirect($server->securehttp . $server->apphost));
			exit;
		}

		if($_SESSION['show_facebook_friends']){
			$_SESSION['show_facebook_friends'] = false;
			session::write(session_id(), $_SESSION);
			$this->_redirect(PS_Util::redirect($server->securehttp . $server->apphost . "/newfriends/invitefacebook"));
			exit;
		}

		$params = $this->getRequest()->getParams();
		$key = $params['key'];

		//user
		try {

	        	$this->view->isGuest = (isset($_SESSION['user_id']) && !empty($_SESSION['user_id']) && $_SESSION['user_id'] != '') ? false : true;
		        $this->view->firstEnter = false;

			$user_details = users::getUserDetails($_SESSION['user_id']);
			$this->view->userDetail = $user_details;

			$groups = groups::groupList($_SESSION['user_id']);
			$this->view->groups = $groups;

			$groups = groups::getFriendsGroupIBelong($_SESSION['user_id']);
			$this->view->GroupIBelong = $groups;

			$this->view->allfriends = usersfriend::prepareForViewAllFriends($key);
			// notifications
			$this->view->newNotifications = notifications::countnewnotifications();
			$this->view->activityLog = notifications::activitylog(true);
			$this->view->notificationsCount = notifications::notificationscount();
			$this->view->fbAppID = $server->FACEBOOK_APP_ID;
			$this->view->groupInfo = empty($_SESSION['group_id']) ? null : groups::getGroupInfo(md5($_SESSION['group_id']));
		} catch (Exception $e) {
			var_dump($e->getMessage());exit;
		}
		
    }

  public function friendspAction()
        {
        $server = Zend_Registry::get("server");

                if ($_SESSION['logged_in'] != true) {
                     $this->_redirect(PS_Util::redirect("https://" . $server->apphost));
                     exit;
                }

        if($_SESSION['show_facebook_friends']){
            $_SESSION['show_facebook_friends'] = false;
            session::write(session_id(), $_SESSION);
            $this->_redirect(PS_Util::redirect("https://" . $server->apphost . "/friends/invitefacebook"));
            exit;
        }

                $params = $this->getRequest()->getParams();
                $key = $params['key'];

                //user
                $user_details = users::getUserDetails($_SESSION['user_id']);
                $this->view->userDetail = $user_details;

                $groups = groups::groupList($_SESSION['user_id']);
                $this->view->groups = $groups;
                /* disable it in alpah0.1 */
                /*
                $groups = groups::getFriendsGroupIBelong($_SESSION['user_id']);
                $this->view->GroupIBelong = $groups;
                */
                $this->view->GroupIBelong = null;
                $this->view->allfriends = usersfriend::prepareForViewAllFriends($key);
        // notifications
        $this->view->newNotifications = notifications::countnewnotifications();
        $this->view->activityLog = notifications::activitylog(true);
        $this->view->notificationsCount = notifications::notificationscount();
        $this->view->fbAppID = $server->FACEBOOK_APP_ID;
    }

	public function friendscontentAction() {
		$db = Zend_Registry::get("db");
		$config = Zend_Registry::get("config");
		$server = Zend_Registry::get("server");
		$logger = Zend_Registry::get("logger");

		$params = $this->getRequest()->getParams();
                $this->view->allfriends = usersfriend::prepareForViewAllFriends();

		$this->_helper->layout()->disableLayout();
	}

        public function friendscontentyouknowAction() {
                $db = Zend_Registry::get("db");
                $config = Zend_Registry::get("config");
                $server = Zend_Registry::get("server");
                $logger = Zend_Registry::get("logger");

                $facebookAccessToken = $this->getRequest()->getParam("access_token");
                $this->_helper->layout()->disableLayout();
                $this->view->userDetail = users::getUserDetails($_SESSION['user_id']);
                $youknow  = usersfriend::prepareFacebookFriendsyouknow($facebookAccessToken); 
                if (count($youknow) <= 0 ) {
                      $this->_helper->json(array('fail' => true, 'msg' =>"failed"));
                } else {
                      $this->view->allfriends = $youknow;
                }
        }

	public function friendscontentgroupAction() {
		$db = Zend_Registry::get("db");
		$config = Zend_Registry::get("config");
		$server = Zend_Registry::get("server");
		$logger = Zend_Registry::get("logger");

		$groupId = $this->getRequest()->getParams("group_id", false);
        $friends = groups::getFriendsFromGroup($groupId['group_id'], $_SESSION['user_id']);
        $group = groups::getGroupInfo(md5($groupId['group_id']));

        $this->view->friends = $friends;
        $this->view->group = $group;
		$this->_helper->layout()->disableLayout();
	}

       public function friendscontentgroupbelongAction() {
                $db = Zend_Registry::get("db");
                $config = Zend_Registry::get("config");
                $server = Zend_Registry::get("server");
                $logger = Zend_Registry::get("logger");

                $groupId = $this->getRequest()->getParams("group_id", false);
        $friends = groups::getGroupMembers($groupId['group_id']);
        $group = groups::getGroupInfo(md5($groupId['group_id']));

        $this->view->friends = $friends;
        $this->view->group = $group;

        $this->view->createrDetails = users::getUserDetails($group->created_by);
        $this->view->you = users::getUserDetails($_SESSION['user_id']);
                $this->_helper->layout()->disableLayout();
        }

	/*
	search user to follow them
	input - friend name as 'key'
	get list of users with flag - this user is your friend or not
	*/
	public function searchuserAction()
	{
		$db = Zend_Registry::get("db");
		$config = Zend_Registry::get("config");
		$server = Zend_Registry::get("server");

		$params = $this->getRequest()->getParams();
		$key = $params['key'];

		if($key != '')
		{
			try{
			$searchResult = usersfriend::searhUser($key, $_SESSION['user_id']);

			if(!empty($searchResult))
			{
				for($i=0; $i < count($searchResult); $i++)
				{
					$list = array();
					$chk = usersfriend::checkExistFriendslist($_SESSION['user_id'], $searchResult[$i]->user_id);

					$list['user_id'] = $searchResult[$i]->user_id;
					$list['username'] = $searchResult[$i]->username;
					$list['profile_pic'] = $searchResult[$i]->profile_pic;
					if($chk)
					{
						$list['isfriend'] = 'Y';
					}else{
						$list['isfriend'] = 'N';
					}
					$result[] = $list;
				}
				$this->view->result = $result;
				$this->view->total_user = count($result);
			}else{
				$this->view->msg = 'Search data not found.';
			}


			}
			catch(Exception $e)
			{
			  print_r($e->getMessage());
			   $this->view->errors[] = $e->getMessage();
			}
		  $this->_helper->viewRenderer("searchuser");
		  $this->_helper->resources("searchuser");

		}

	}//add


	/*	follow a user
		input user_id - who want to follow
	 	input friend_id - whome to follow
	*/
        public function followfriendAction()
        {

			$params = $this->getRequest()->getParams();
			try{
				usersfriend::followFriend($_SESSION['user_id'], $params["friend_id"]);
				$friend = users::getUserFriendDetails($_SESSION['user_id'], $params["friend_id"]);
				return $this->_helper->json(array('success' => true, 'friend' => $friend));
            }
            catch(Exception $e)
            {
				return $this->_helper->json(array('success' => false, 'msg' => $e->getMessage()));
             }
        }

	/*
	delete/unfollow friend
	input - user_id, friend_id
	*/
	public function deletefriendAction()
	{
		$db = Zend_Registry::get("db");

		$params = $this->getRequest()->getParams();
		$succes = true;
		try{
			$db->beginTransaction();
			$data['status'] = 2;
			$db->update('usersfriend', $data, array("user_id = ?" => $_SESSION['user_id'], "friend_id = ?" => $params['friend_id']));
			//$db->delete('usersfriend', array("user_id = ?" => $params['user_id'], "friend_id = ?" => $params['friend_id']));
			$db->commit();
			$message = 'Friend unfollowed successfully!';
		}
		catch(Exception $e)
		{
			$succes = false;
			$message = $e->getMessage();
		}
		return $this->_helper->json(array('success' => $succes, 'msg' =>$message));
	}

	/*
	list all notification in popup
	*/
	public function notificationsAction()
	{
		$db = Zend_Registry::get("db");
		$config = Zend_Registry::get("config");
		$server = Zend_Registry::get("server");

		$params = $this->getRequest()->getParams();

		try{
			$db->beginTransaction();
			$notify_list = usersfriend::latestNotification($_SESSION['user_id']);

			// get newly joined FB friend notification
			$fb_newusersList = users::getnewFBjoinNotify($_SESSION['user_id']);
			//

			for($i=0; $i < count($notify_list); $i++)
			{
				$userDetail = users::getUserDetails($_SESSION['user_id']);
				$username = users::getName($notify_list[$i]->user_id);
				//$username = $userDetail->first_name.' '. $userDetail->last_name;
				$list['message'] = '<span class="bulecolor">'.$username.'</span> is now following you ';
				$list['follow_permission'] = $userDetail->follow_permission;
				$list['user_id'] = $notify_list[$i]->user_id;
				$list['status'] = $notify_list[$i]->status;
				$list['friend_id'] = $_SESSION['user_id'];

				$messageList[] = $list;
			}

			$this->view->notiflist = $messageList;
			$this->view->FBnotifylist = $fb_newusersList;

			$data['notify'] = 0;
			$db->update('usersfriend', $data,'friend_id = '.$_SESSION['user_id']);

			/*echo '<pre>';
			print_r($fb_newusersList);
			echo '</pre>';*/

			$db->commit();
		}
		catch(Exception $e)
		{
		  print_r($e->getMessage());
		  $this->view->errors[] = $e->getMessage();
		}

		$this->_helper->viewRenderer("notifications");
		$this->_helper->resources("notifications");
	}

       public function curateusersAction()
        {
                $db = Zend_Registry::get("db");
                $config = Zend_Registry::get("config");
                $server = Zend_Registry::get("server");
                $logger = Zend_Registry::get("logger");

                $params = $this->getRequest()->getParams();
   //             $user_id = $params['userID'];

                $logger->info("[START] " . __CLASS__ . ' : ' . __FUNCTION__);

                try{
                        $searchResult = users::getAllforcurate();
                        $logger->info("[START] " . count($searchResult));
           /*
                        if(!empty($searchResult))
                        {
                                for($i=0; $i < count($searchResult); $i++)
                                {
                                        $list = array();
                                        $follower_details = users::getUserDetails($searchResult[$i]->user_id);
                                        $list['user_id'] = $follower_details->user_id;
                                        $list['username'] = $follower_details->first_name.' '.$follower_details->last_name;
                                        $list['profile_pic'] = $follower_details->profile_pic;

                                        $result[] = $list;
                                }
                                $this->view->result = $result;
                        }
            */

                      $this->view->users = $searchResult;   
                      $this->view->curatePageType = 1;
                      $this->view->more_items = 1;
		$this->view->curate = 1;

                }
                catch(Exception $e)
                {

                  $this->view->errors[] = $e->getMessage();
                }

                //$this->_helper->viewRenderer("followers");
                //$this->_helper->resources("followers");
        }


	/// Get followers of logged in user
	public function followersAction()
	{
		$db = Zend_Registry::get("db");
		$config = Zend_Registry::get("config");
		$server = Zend_Registry::get("server");

		$params = $this->getRequest()->getParams();
		$user_id = $params['userID'];

		try{
			if($user_id != '')
			{
				$searchResult = usersfriend::getFollowerListWeb($user_id);
			}else{
				$searchResult = usersfriend::getFollowerListWeb($_SESSION['user_id']);
			}


			if(!empty($searchResult))
			{
				for($i=0; $i < count($searchResult); $i++)
				{
					$list = array();
					$follower_details = users::getUserDetails($searchResult[$i]->user_id);
					$list['user_id'] = $follower_details->user_id;
					$list['username'] = $follower_details->first_name.' '.$follower_details->last_name;
					$list['profile_pic'] = $follower_details->profile_pic;

					$result[] = $list;
				}
				$this->view->result = $result;
			}

		}
		catch(Exception $e)
		{

		  $this->view->errors[] = $e->getMessage();
		}

		$this->_helper->viewRenderer("followers");
		$this->_helper->resources("followers");
	}

	//// get list of followings
	public function followingAction()
	{
		$db = Zend_Registry::get("db");
		$config = Zend_Registry::get("config");
		$server = Zend_Registry::get("server");

		$params = $this->getRequest()->getParams();
		$user_id = $params['userID'];

		try{
			if($user_id != '')
			{
				$searchResult = usersfriend::getFollowListWeb($user_id);
			}else{
				$searchResult = usersfriend::getFollowListWeb($_SESSION['user_id']);
			}


			if(!empty($searchResult))
			{
				for($i=0; $i < count($searchResult); $i++)
				{
					$list = array();
					$follower_details = users::getUserDetails($searchResult[$i]->friend_id);
					$list['user_id'] = $follower_details->user_id;
					$list['username'] = $follower_details->first_name.' '.$follower_details->last_name;
					$list['profile_pic'] = $follower_details->profile_pic;

					$result[] = $list;
				}
				$this->view->result = $result;
			}

		}
		catch(Exception $e)
		{

		  $this->view->errors[] = $e->getMessage();
		}

		$this->_helper->viewRenderer("following");
		$this->_helper->resources("following");
	}

	/*
	list groups
	*/

	public function groupsAction()
	{
		$db = Zend_Registry::get("db");
		$config = Zend_Registry::get("config");
		$server = Zend_Registry::get("server");

		$params = $this->getRequest()->getParams();
		$this->view->isnewgrp = $params['cre'];

		$groupID = $params['group_id'];
		//user
		$user_details = users::getUserDetails($_SESSION['user_id']);
		$this->view->userDetail = $user_details;

		try{
			$db->beginTransaction();

			/// group detail
			$groups = groups::groupList($_SESSION['user_id']);
			if($groupID == '')
			{
				$groupID = md5($groups[0]->id);
			}
			if(isset($params['cre']) && $params['cre'] == 's')
			{
				/// create new group
			}else{
				$groupInfo = groups::getGroupInfo($groupID);
			}


			/// update group image
			require 'SimpleImage.php';
			if(!empty($_FILES["group_img"]["name"]))
			{
				if((strtolower($_FILES["group_img"]["type"]) == "image/gif") || (strtolower($_FILES["group_img"]["type"]) == "image/jpeg") || (strtolower($_FILES["group_img"]["type"]) == "image/pjpeg") || (strtolower($_FILES["group_img"]["type"]) == "image/png"))
				{
				///delete old image
				if($groupInfo->group_image != '')
				{
					unlink($_SERVER["DOCUMENT_ROOT"]."/groupimg/".$groupInfo->group_image);
					unlink($_SERVER["DOCUMENT_ROOT"]."/groupimg/thumb/".$groupInfo->group_image);
				}
				///
				$imgExtsn = explode(".",$_FILES["group_img"]["name"]);
				$countExtsn = count($imgExtsn)-1;
				$file = $imgExtsn[$countExtsn-1].'_'.$groupInfo->group_name.rand(100,100000).".".$imgExtsn[$countExtsn];
				$newfilename=$_SERVER["DOCUMENT_ROOT"]."/groupimg/".$file;
				$thumb_path = $_SERVER["DOCUMENT_ROOT"]."/groupimg/thumb/".$file;

				/// resize image
				$image = new SimpleImage();
				$image->load($_FILES["group_img"]["tmp_name"]);
				$image->save($newfilename);
				$size = getimagesize($newfilename);
				$width = $size[0];
				$height = $size[1];
				if($width > 120)
				{
					$image->resizeToWidth(120);
					$image->save($newfilename);
				}
				if($height > 120)
				{
					$image->resizeToHeight(120);
					$image->save($newfilename);
				}
				////////////
				$image->load($_FILES["group_img"]["tmp_name"]);
				$image->save($thumb_path);
				$size = getimagesize($thumb_path);
				$width = $size[0];
				$height = $size[1];
				if($width > 50)
				{
					$image->resizeToWidth(50);
					$image->save($thumb_path);
				}
				if($height > 50)
				{
					$image->resizeToHeight(50);
					$image->save($thumb_path);
				}
				////

				$data['group_image'] = $file;
				$db->update('groups', $data,'id = '.$groupInfo->id);
				$db->commit();

				$groupInfo = groups::getGroupInfo($groupID);
				$groups = groups::groupList($_SESSION['user_id']);

				}/// image type
				else{
					$this->view->group_imgErr = 'Please select image of type jpg/png/gif/';
				}
			}
			///


			$this->view->groups = $groups;

			/// get total friends

			$allfriendList = usersfriend::getAllFriends($_SESSION['user_id']);
			$totalFriend = 0;
			for($i = 0; $i < count($allfriendList); $i++)
			{
				$list = array();

				//if($allfriendList[$i]->status != 0)
				//{
					if($allfriendList[$i]->user_id == $_SESSION['user_id'])
					{
						/// following
						$friend_idf = $allfriendList[$i]->friend_id;
						$friendType = 'following';
						/// check this user is in your follower list
						$isfollower = usersfriend::checkExistFriends($allfriendList[$i]->friend_id, $_SESSION['user_id']);
						/// check You are following this user
						$isfollow = usersfriend::checkExistFriends($_SESSION['user_id'], $allfriendList[$i]->friend_id);



					}else{
						// follower
						$friend_idf = $allfriendList[$i]->user_id;
						$friendType = 'Follower';

						$isfollow = usersfriend::checkExistFriends($_SESSION['user_id'], $allfriendList[$i]->user_id);
						if($isfollow)
						{
							continue;
						}
					}

					if(($allfriendList[$i]->status == 0) && ($isfollower == ''))
					{
							/// do noting
					}else{
						$totalFriend++;
					}
					$friend_relationID = $allfriendList[$i]->id;
					$list['id'] = $friend_idf;
					$allFriends[] = $list;
				//}//status
			}


			$groupFriends = groups::getGroupFriend($groupInfo->id, $_SESSION['user_id']);


			$this->view->groupusers = $groupFriends;
			$this->view->groupInfo = $groupInfo;
			$this->view->groupID = $groupInfo->id;

			$this->view->totalfriend = $totalFriend;

		}catch(Exception $e){
		  print_r($e->getMessage());
		  $this->view->errors[] = $e->getMessage();
		}

		$this->view->allfriends = $allFriends;

		$this->_helper->viewRenderer("groups");
		$this->_helper->resources("groups");
	}



	public function creategroupsaveAction()
	{
		$db = Zend_Registry::get("db");
		$config = Zend_Registry::get("config");
		$server = Zend_Registry::get("server");

		$params = $this->getRequest()->getParams();

		$friendIds = $params['friend_id'];

		if(count($friendIds) == 0)
		{
			 $this->view->frError = 'Please select friends';
		}

		if($params['group'] == '')
		{
			$this->view->msg = 'Please enter group name';
		}

		if(count($friendIds) > 0 && $params['group'] != '')
		{
			try{
				$db->beginTransaction();

				$Gdata['group_name'] = $params["group"];
				$Gdata['created_by'] = $_SESSION['user_id'];
				$Gdata['created_date'] = date('Y-m-d');

				$existGroup = groups::checkExistGroup($_SESSION['user_id'],$params["group"]);

				if($existGroup == '')
				{
					$db->insert('groups', $Gdata);
					$group_id=$db->lastInsertId();

					for($i=0; $i< count($friendIds);$i++)
					{
						$data['user_id'] = $friendIds[$i];
						$data['group_id'] = $group_id;

						$inexist = groups::checkfriendInGroup($friendIds[$i],$group_id);

						if($inexist == '')
						{
							$db->insert('friendsgroup', $data);
						}
					}

					$db->commit();
					$this->view->msg = 'Group has been created sucessfully.';
				}else{
					$this->view->msg = 'This group name is alread added.';
				}

			}catch(Exception $e){
			  print_r($e->getMessage());
			  $this->view->errors[] = $e->getMessage();
			}
		}

		$this->_helper->viewRenderer("creategroup");
		$this->_helper->resources("creategroup");
	}

	/*
	serach friends
	input - key (name), loggedin user_id
	*/
	public function searchfriendAction()
	{
		$db = Zend_Registry::get("db");
		$config = Zend_Registry::get("config");
		$server = Zend_Registry::get("server");

		$params = $this->getRequest()->getParams();
		$key = $params['key'];

		try{
			$list = usersfriend::searchFriend($key, $_SESSION['user_id']);

			$resultStr = '<ul>';
			if(count($list) > 0)
			{
				for($i=0; $i < count($list);$i++)
				{
					$resultStr .= '<li><input type="checkbox" name="friend_id[]" value="'.$list[$i]->user_id.'" style="width:25px; float:left">
					 <span class="client-cmnt" style="width:auto; padding-left: 0px; padding-top: 0; line-height:17px !important; ">'.$list[$i]->username.' </span>

					</li>';
				}
			}else{
				$resultStr .= '<li>No friend found.</li>';
			}
			$resultStr .= '</ul>';

		}catch(Exception $e){
		  print_r($e->getMessage());
		  $this->view->errors[] = $e->getMessage();
		}
		echo $resultStr;

		exit;
	}

	public function groupusersAction()
	{
		$db = Zend_Registry::get("db");
		$config = Zend_Registry::get("config");
		$server = Zend_Registry::get("server");

		$params = $this->getRequest()->getParams();

		$group_id = $params['grp_id'];

		$groupData = groups::getGroupFriend($group_id, $_SESSION['user_id']);

		$this->view->users = $groupData;
		$this->view->groupName = $params['grp'];

		$this->_helper->viewRenderer("groupusers");
		$this->_helper->resources("groupusers");
	}

	public function deletegroupAction()
	{

		$params = $this->getRequest()->getParams();

		$group_id = $params['grpid'];
        try{
            $res = groups::deleteGroup($group_id);
            return $this->_helper->json($res);
        }catch(Exception $e){
            return $this->helper->json(array('fail' => true, 'msg' => $e->getMessage()));
        }


		exit;
	}


	public function friendprofileAction()
	{
		$db = Zend_Registry::get("db");
		$config = Zend_Registry::get("config");
		$server = Zend_Registry::get("server");
                $logger = Zend_Registry::get("logger");

                $logger->info("--- ACCESS-INFO --- ::FRIEND-PROFILE-PAGE::USER-ID::" .$_SESSION['user_id']. " DEVICE-INFO :: ". $_SERVER['HTTP_USER_AGENT'] . " IP ADDRESS/ " . $_SERVER['REMOTE_HOST'] . "\n");

                if ($_SESSION['logged_in'] != true) {
                  $this->_redirect(PS_Util::redirect("https://" . $server->apphost));
                  exit;
                }
		$params = $this->getRequest()->getParams();
		$friend_id = $params['friend_id'];
                $shared = $params['shared'];
                //$shared = (isset($params['shared'])) ? $params['shared'] : 0;
		$catVal = $params['item_type'];

		try{
		//user
		$user_details = users::getUserDetails($_SESSION['user_id']);
		$this->view->userDetail = $user_details;

		$friend_details = usersfriend::frienDeatil($friend_id);

		/// follows
		$followCount = usersfriend::countFollowList($friend_details->user_id);
		$this->view->follow = $followCount;

		/// followers
		$followerCount = usersfriend::countFollowerList($friend_details->user_id);
		$this->view->followers = $followerCount;

		$this->view->friendDetail = $friend_details;

                // total gift send by user
                $this->view->giftsend = event::giftSend($friend_details->user_id);

                // total gift recived by user
                $this->view->giftget = event::giftGet($friend_details->user_id);

		//user tags
		$category=category::showTag($friend_details->user_id);
		$this->view->tags=$category;

		/// friend wishlist
                if (!$shared) {
                    $numberOfItems = 100;
                    $this->view->next_start = $numberOfItems;
		    $products = wishlistitmes::getLoadMoreFriendPage($friend_details->user_id, $orderBy, $catVal, 0, $numberOfItems);
                    $more_items = (count($products) >= $numberOfItems) ? 1 : 0;
                    $this->view->more_items = $more_items;
                } else {
                   $itemIds = $params['itemIds'];
                   $products = wishlistitmes::getSharedItemsLandingPage($friend_details->user_id, $itemIds);
                   $this->view->more_items = 0;
                }

                $this->view->vpage = 4;
                /*
                $this->view->more_items = 0;
                $products = wishlistitmes::getAllProducts($friend_details->user_id, $orderBy, $catVal);
                */
                $isfollower = usersfriend::checkExistFriendStatus($friend_details->user_id, $_SESSION['user_id']);
                $tmp_follow = ($isfollower != '' && $isfollower->status == '1') ? 'Y' : 'N';
                $this->view->isfollower =  $tmp_follow;
                $this->view->request_status = usersfriend::getRequestStatus($friend_details->user_id, $_SESSION['user_id']);

        /* filter bar tags */
        $noids = true;
        $productIds = array();
        for ($i = 0; $i < count($products); $i++) {
            $productIds[] = $products[$i]->id;
            $noids =  false;
        }
        if ($noids == false) {
            $this->view->filtertags = wishlistitmes::getUsersTagList($friend_details->user_id, "('" . implode("','",$productIds) . "')");
        }
                if ($tmp_follow == 'Y') {
                     $this->view->filter_bar_events = event::getUserCreatedEvents($friend_details->user_id);
                } else  {
                     $this->view->filter_bar_events = null;
                }
		//user's created groups
		$groups = groups::groupList($_SESSION['user_id']);
		$friendGroups = groups::getFriendsGroup($_SESSION['user_id'], $friend_details->user_id);

		/*echo '<pre>';
		print_r($items);
		echo '</pre>';*/

		// user's activity
		$activity = users::getActivity($friend_details->user_id);

		$activityLog = array();
		for($i=0; $i < count($activity); $i++)
		{
			$listA = array();

			$sender = users::getUserDetails($activity[$i]->sender);
			$receiver = users::getUserDetails($activity[$i]->receiver);
			$event_title = event::getEventTitle($activity[$i]->event_id);

			$item_name=wishlistitmes::getProductName($activity[$i]->id);

			$time = users::getTime(strtotime($activity[$i]->activity_date));

			$listA['sender_id'] = $activity[$i]->sender;
			$listA['sender_name'] = $sender->first_name.' '.$sender->last_name;
			$listA['sender_image'] = $sender->profile_pic;
			$listA['event_title'] = $event_title;
			$listA['item_name'] = $item_name;
			$listA['item_id'] = $activity[$i]->id;

			$listA['receiver_id'] = $activity[$i]->receiver;
			$listA['receiver_name'] = $receiver->first_name.' '.$receiver->last_name;
			$listA['receiver_image'] = $receiver->profile_pic;

			$listA['activity_type'] = $activity[$i]->activity_type;
			$listA['activity_time'] = $time;

			$activityLog[] = $listA;
		}

		/// All activity
		$allActivity = users::getAllActivity($friend_details->user_id);


		for($i=0; $i < count($allActivity); $i++)
		{
			$listA = array();

			$sender = users::getUserDetails($allActivity[$i]->sender);
			$event_title = event::getEventTitle($allActivity[$i]->event_id);
			$receiver = users::getUserDetails($allActivity[$i]->receiver);

			$time = users::getTime(strtotime($allActivity[$i]->activity_date));

			$item_name=wishlistitmes::getProductName($allActivity[$i]->id);

			$listA['id'] = $allActivity[$i]->id;
			$listA['item_id'] = $allActivity[$i]->id;
			$listA['item_name'] = $item_name;
			$listA['event_title'] = $event_title;
			$listA['sender_id'] = $allActivity[$i]->sender;
			$listA['sender_name'] = $sender->first_name.' '.$sender->last_name;
			$listA['sender_image'] = $sender->profile_pic;

			$listA['receiver_id'] = $allActivity[$i]->receiver;
			$listA['receiver_name'] = $receiver->first_name.' '.$receiver->last_name;
			$listA['receiver_image'] = $receiver->profile_pic;

			$listA['activity_type'] = $allActivity[$i]->activity_type;
			$listA['activity_time'] = $time;

			$allActivityList[] = $listA;
		}
		$this->view->allActivityLog = $allActivityList;

		$isfollow = usersfriend::checkExistFriendStatus($_SESSION['user_id'], $friend_details->user_id);

		if($isfollow != '')
		{
			if($isfollow->status == '1')
			{
				$followingthis = 'Y';
			}
			if($isfollow->status == '0')
			{
				$followingthis = 'P';
			}
		}else{
			$followingthis = 'N';
		}
		///
		$this->view->followthis = $followingthis;

		$this->view->activityLog = $activityLog;

		$this->view->friendGroups = $friendGroups;
		$this->view->usersgroups = $groups;

                $createdGroups = groups::groupListWithCheckFriend($_SESSION['user_id'], $friend_details->user_id);
                $this->view->created_groups = $createdGroups;

		$this->view->items = $products;
		$this->view->extval = wishlistitmes::prepareExtVal($products, $friend_details->user_id);
                // notifications
                $this->view->newNotifications = notifications::countnewnotifications();
                $this->view->activityLog = notifications::activitylog(true);
                $this->view->notificationsCount = notifications::notificationscount();

                $_SESSION['friend_id'] = $friend_details->user_id;
                session::write(session_id(), $_SESSION);

		}catch(Exception $e)
		{
			 print_r($e->getMessage());
		  	$this->view->errors[] = $e->getMessage();
		}


		$this->_helper->viewRenderer("friendprofile");
		$this->_helper->resources("friendprofile");
	}

       public function wishlistAction() 
       {
        $server = Zend_Registry::get("server");
        $config = Zend_Registry::get("config");
        $logger = Zend_Registry::get("logger");
        $db = Zend_Registry::get("db");

        /* facebook login informtaion */
        require 'fb/facebook.php';
        require 'amazonQuery.php';
        Facebook::$CURL_OPTS[CURLOPT_SSL_VERIFYPEER] = false;
        Facebook::$CURL_OPTS[CURLOPT_SSL_VERIFYHOST] = 2;
        $facebook = new Facebook(array(
                    'appId' => $server->FACEBOOK_APP_ID,
                    'secret' => $server->FACEBOOK_SECRET,
                ));
        $this->view->fbAppID = $server->FACEBOOK_APP_ID;


        $wishlist = $this->getRequest()->getParam("wid");

        try{
                $user_details = users::getUserDetails($_SESSION['user_id']);
                $this->view->userDetail = $user_details;

                $friend_details = usersfriend::frienDeatilByWlink($wishlist);
                $w_error = '';
                if (!$friend_details)
                {
                     $w_error= "Wishlist link is not valid";
                }
                $this->view->error_msg = $w_error;

                /// follows
                $followCount = usersfriend::countFollowList($friend_details->user_id);
                $this->view->follow = $followCount;

                /// followers
                $followerCount = usersfriend::countFollowerList($friend_details->user_id);
                $this->view->followers = $followerCount;

                $this->view->friendDetail = $friend_details;

                // total gift send by user
                $this->view->giftsend = event::giftSend($friend_details->user_id);

                // total gift recived by user
                $this->view->giftget = event::giftGet($friend_details->user_id);

                //user tags
                $category=category::showTag($friend_details->user_id);
                $this->view->tags=$category;

                $numberOfItems = 18;
                $this->view->next_start = $numberOfItems;
                $products = wishlistitmes::getLoadMoreFriendPage($friend_details->user_id, $orderBy, $catVal, 0, $numberOfItems);
                $more_items = (count($products) >= $numberOfItems) ? 1 : 0;
                $this->view->more_items = $more_items;
         
                $this->view->vpage = 4;
                $isfollower = usersfriend::checkExistFriendStatus($friend_details->user_id, $_SESSION['user_id']);
                $tmp_follow = ($isfollower != '' && $isfollower->status == '1') ? 'Y' : 'N';
                $this->view->isfollower =  $tmp_follow;
                $this->view->request_status = usersfriend::getRequestStatus($friend_details->user_id, $_SESSION['user_id']);

        /* filter bar tags */
        $noids = true;
        $productIds = array();
        for ($i = 0; $i < count($products); $i++) {
            $productIds[] = $products[$i]->id;
            $noids =  false;
        }
        if ($noids == false) {
            $this->view->filtertags = wishlistitmes::getUsersTagList($friend_details->user_id, "('" . implode("','",$productIds) . "')");
        }
                if ($tmp_follow == 'Y') {
                     $this->view->filter_bar_events = event::getUserCreatedEvents($friend_details->user_id);
                } else  {
                     $this->view->filter_bar_events = null;
                }
                //user's created groups
                $groups = groups::groupList($_SESSION['user_id']);
                $friendGroups = groups::getFriendsGroup($_SESSION['user_id'], $friend_details->user_id);

                /*echo '<pre>';
                print_r($items);
                echo '</pre>';*/

                // user's activity
                $activity = users::getActivity($friend_details->user_id);

                $activityLog = array();
                for($i=0; $i < count($activity); $i++)
                {
                        $listA = array();

                        $sender = users::getUserDetails($activity[$i]->sender);
                        $receiver = users::getUserDetails($activity[$i]->receiver);
                        $event_title = event::getEventTitle($activity[$i]->event_id);
                        $item_name=wishlistitmes::getProductName($activity[$i]->id);

                        $time = users::getTime(strtotime($activity[$i]->activity_date));

                        $listA['sender_id'] = $activity[$i]->sender;
                        $listA['sender_name'] = $sender->first_name.' '.$sender->last_name;
                        $listA['sender_image'] = $sender->profile_pic;
                        $listA['event_title'] = $event_title;
                        $listA['item_name'] = $item_name;
                        $listA['item_id'] = $activity[$i]->id;

                        $listA['receiver_id'] = $activity[$i]->receiver;
                        $listA['receiver_name'] = $receiver->first_name.' '.$receiver->last_name;
                        $listA['receiver_image'] = $receiver->profile_pic;

                        $listA['activity_type'] = $activity[$i]->activity_type;
                        $listA['activity_time'] = $time;

                        $activityLog[] = $listA;
                }

                /// All activity
                $allActivity = users::getAllActivity($friend_details->user_id);


                for($i=0; $i < count($allActivity); $i++)
                {
                        $listA = array();

                        $sender = users::getUserDetails($allActivity[$i]->sender);
                        $event_title = event::getEventTitle($allActivity[$i]->event_id);
                        $receiver = users::getUserDetails($allActivity[$i]->receiver);

                        $time = users::getTime(strtotime($allActivity[$i]->activity_date));

                        $item_name=wishlistitmes::getProductName($allActivity[$i]->id);

                        $listA['id'] = $allActivity[$i]->id;
                        $listA['item_id'] = $allActivity[$i]->id;
                        $listA['item_name'] = $item_name;
                        $listA['event_title'] = $event_title;
                        $listA['sender_id'] = $allActivity[$i]->sender;
                        $listA['sender_name'] = $sender->first_name.' '.$sender->last_name;
                        $listA['sender_image'] = $sender->profile_pic;

                        $listA['receiver_id'] = $allActivity[$i]->receiver;
                        $listA['receiver_name'] = $receiver->first_name.' '.$receiver->last_name;
                        $listA['receiver_image'] = $receiver->profile_pic;

                        $listA['activity_type'] = $allActivity[$i]->activity_type;
                        $listA['activity_time'] = $time;

                        $allActivityList[] = $listA;
                }
                $this->view->allActivityLog = $allActivityList;

                $isfollow = usersfriend::checkExistFriendStatus($_SESSION['user_id'], $friend_details->user_id);

                if($isfollow != '')
                {
                        if($isfollow->status == '1')
                        {
                                $followingthis = 'Y';
                        }
                        if($isfollow->status == '0')
                        {
                                $followingthis = 'P';
                        }
                }else{
                        $followingthis = 'N';
                }
                ///
                $this->view->followthis = $followingthis;

                $this->view->activityLog = $activityLog;

                $this->view->friendGroups = $friendGroups;
                $this->view->usersgroups = $groups;

                $createdGroups = groups::groupListWithCheckFriend($_SESSION['user_id'], $friend_details->user_id);
                $this->view->created_groups = $createdGroups;

                $this->view->items = $products;
                $this->view->extval = wishlistitmes::prepareExtVal($products, $friend_details->user_id);
                // notifications
                $this->view->newNotifications = notifications::countnewnotifications();
                $this->view->activityLog = notifications::activitylog(true);
                $this->view->notificationsCount = notifications::notificationscount();

                $_SESSION['friend_id'] = $friend_details->user_id;
                session::write(session_id(), $_SESSION);

                }catch(Exception $e)
                {
                         print_r($e->getMessage());
                        $this->view->errors[] = $e->getMessage();
                }
                $this->_helper->viewRenderer("friendwishlist");
                $this->_helper->resources("friendwishlist");
    }

	public function emailinviteAction()
	{
		$db = Zend_Registry::get("db");
		$config = Zend_Registry::get("config");
		$server = Zend_Registry::get("server");

		$this->_initinvite();
	}

	public function sendinvitationAction()
	{
                $db  = Zend_Registry::get("db");
                $config = Zend_Registry::get("config");
                $server = Zend_Registry::get("server");

                $params = $this->getRequest()->getParams();

                $m = $this->_initinvite();
                $user_details = users::getUserDetails($_SESSION['user_id']);
				$success = true;
                try{
                        // $this->fields->validate();

					$friendId = users::checkExistsEmail($params['email_id']);
					if (!empty($friendId)) {
						usersfriend::followFriend($_SESSION['user_id'], $friendId);
						if (!empty($params['group'])) {
							$groupId = groups::getGroupIdByHashCode($params['group']);
							if ($groupId != '') {
								groups::addFriendToGroup($friendId, $groupId);
							};
						}
						$friend = users::getUserFriendDetails($_SESSION['user_id'], $friendId);
						return $this->_helper->json(array('success' => $success, 'friendExist' => true, 'friend' => $friend, 'msg' => 'user invited'));
					}
					$hashId =  md5($_SESSION['user_id']);
					$hashGroup = (empty($params['group'])) ? '' : '&group='.$params['group'];
					$inviteLink = $server->securehttp . $server->apphost .'/login/register?email='. $params['email_id'] . '&friend_id='. $hashId . $hashGroup;

                        $mailText = '


<html><head>

<!-- Define Charset -->
        <meta http-equiv="Content-Type" content="text/html;" charset="UTF-8">
        
        <!-- Responsive Meta Tag -->
        <meta name="viewport" content="width=device-width;" initial-scale="1.0;" maximum-scale="1.0;">
        <link href="http://fonts.googleapis.com/css?family=Questrial" rel="stylesheet" type="text/css">
        
        <!--- Importing Twitter Bootstrap icons -->
        <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">

</head><body>
<div marginheight="0" marginwidth="0">
	<table style="background-color:rgb(21,18,18)" cellpadding="0" cellspacing="0" bgcolor="ecebeb" border="0" width="100%">
		<tbody><tr>
		<td>
				<table cellpadding="0" cellspacing="0" align="center" border="0" width="100%">
					<tbody><tr><td style="font-size:30px;line-height:30px" height="30"> </td></tr>
					<tr>
						<td>
							<table style="border-collapse:collapse" cellpadding="0" cellspacing="0" align="left" border="0"i width="45%">
								<tbody><tr>
									<td align="center">
										<table cellpadding="0" cellspacing="0" align="center" border="0">
											<tbody><tr>
												<td style="line-height:21px" align="center">

	<a href="' . $server->securehttp . $server->apphost . '" style="display:block;border-style:none!important;border:0!important" target="_blank"><img style="display:block;width:128px" src="' . $server->securehttp . $server->apphost . '/images/logo.png" width="180"></a>
												</td>
											</tr>
										</tbody></table>
									</td>
								</tr>
								
							</tbody></table>
							
							<table style="border-collapse:collapse" cellpadding="0" cellspacing="0" align="left" border="0" width="5px">
								<tbody><tr><td style="font-size:20px;line-height:20px" height="20" width="5px"> </td></tr>
							</tbody></table>
							
							<table style="border-collapse:collapse" cellpadding="10" cellspacing="0" align="right" border="0" width="30%">
								
								<tbody><tr>
									<td>
										<table style="border-collapse:collapse" cellpadding="0" cellspacing="0" align="left" border="0">
			
				                			<tbody><tr>
				                				<td>
				                					<table style="border-collapse:collapse" cellpadding="0" cellspacing="0" align="center">
				                						<tbody><tr>
				                							
							                				<td style="color:#ffffff;font-size:14px;font-family:\'Questrial\',Helvetica,sans-serif;line-height:24px" align="center">
							                					<div style="line-height:24px">
							                						<span>
<a href="' . $server->securehttp . $server->apphost . '/modals/modalshowhelpcenter?tab=about" style="color:#878b99;text-decoration:none">About</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="' . $server->securehttp . $server->apphost . '/modals/modalshowhelpcenter?tab=faq" style="color:#878b99;text-decoration:none">Help</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="' . $server->securehttp . $server->apphost . '/modals/modalshowhelpcenter?tab=contact" style="color:#878b99;text-decoration:none">Contact</a>
								                					
							                						</span>
							                					</div>	
							                				</td>
				                						</tr>
				                					</tbody></table>
				                				</td>
				                			</tr>
			                			</tbody></table>
									</td>
								</tr>	
							</tbody></table>	
						</td>
					</tr>
					
					<tr><td style="font-size:25px;line-height:25px" height="25"> </td></tr>
					<tr>
						<td style="line-height:21px" align="center">
						</td>			
					</tr>
					
					<tr><td style="font-size:50px;line-height:50px" height="50"> </td></tr>
					
					<tr>
						<td style="color:#ffffff;font-size:40px;font-family:\'Questrial\',Helvetica,sans-serif;line-height:28px" align="center">
							
							
							
							<div style="line-height:28px">
								<span>Introducing <span>Gift</span>vise!</span>
							</div>
        				</td>
					</tr>
				        <tr><td style="font-size:25px;line-height:25px"  height="25" > </td></tr> 	
					<tr>
						<td align="center">
							<table cellpadding="0" cellspacing="0" align="center" border="0" width="540">
								<tbody><tr>
									
									<td style="color:#878b99;font-size:18px;font-family:\'Questrial\',Helvetica,sans-serif;line-height:30px" align="center">
										<div style="line-height:30px">
											
											
											<span>The perfect way to give and receive the right gift for the right event</span>
										</div>
			        				</td>	
								</tr>
							</tbody></table>
						</td>
					</tr>
					
					<tr><td style="font-size:35px;line-height:35px" height="35"> </td></tr>
					
				</tbody></table>
			</td></tr>
		
	</tbody></table>
	
<!--	
	<table cellpadding= 0  cellspacing= 0  bgcolor= 363b49  border= 0  width= 100% >
							
		<tbody><tr><td style= font-size:20px;line-height:20px  height= 20 > </td></tr>
					
		<tr>
			<td align= center >
				
				<table cellpadding= 0  cellspacing= 0  align= center  border= 0  width= 590 >
					
					<tbody><tr>
						<td align= center >
								                		
	                		<table cellpadding= 0  cellspacing= 0  align= center  border= 0 >
	                			<tbody><tr>
	                				<td style= color:#5a5f70;font-size:14px;font-family:&#39;Questrial&#39;,Helvetica,sans-serif;line-height:25px  align= center >
	                					<div style= line-height:25px >
	                						<span>
		                					
		                						Copyright @ Giftvise 2014
		                					
	                						</span>
	                					</div>	
	                				</td>
	                			</tr>
	                			
                			</tbody></table>
                			
						</td>
					</tr>
					
				</tbody></table>
			</td>
		</tr>
		<tr><td style= font-size:20px;line-height:20px  height= 20 > </td></tr>
	</tbody></table>
-->
	
	
	<table class="bg2_color" style="background-color: rgb(255, 255, 255);" width="100%" bgcolor="f3f4f5" border="0" cellpadding="10" cellspacing="0">
		<tbody><tr>
			<td>
                            <table width="30%"  border="0" cellpadding="0" cellspacing="0">
                            <tbody><tr><td style="font-size: 30px; line-height: 20px;" height="30">&nbsp;</td></tr>
                            </table>
				<table class="container590" width="100%" align="center" border="0" cellpadding="0" cellspacing="0">
					
					<tbody>
					<tr><td style="font-size: 40px; line-height: 40px;" height="40">&nbsp;</td></tr>
					
					<tr>
						<td style="color: rgb(86, 94, 120); font-size: 28px; font-family: \'Questrial\',Helvetica,sans-serif; line-height: 22px;" class="title_color" align="center">
							<!-- ======= section text ====== -->
							
							<div class="editable_text" style="line-height: 22px">
								<span class="text_container">You\'ve been invited...</span>
							</div>
        				</td>	
					</tr>
					
					<tr><td style="font-size: 20px; line-height: 20px;" height="20">&nbsp;</td></tr>
					
					<tr>
						<td align="center">
							<table class="main_color" style="border-radius: 4px;" width="24" align="center" bgcolor="f06e6a" border="0" cellpadding="0" cellspacing="0">
								<tbody><tr><td style="font-size: 4px; line-height: 4px;" height="4">&nbsp;</td></tr>
							</tbody></table>
						</td>
					</tr>
					
					<tr><td style="font-size: 20px; line-height: 20px;" height="20">&nbsp;</td></tr>
					
					<tr>
						<td align="center">
							<table class="container580" width="580" align="center" border="0" cellpadding="0" cellspacing="0">
								<tbody><tr>
									<td style="color: #878b99; font-size: 14px; font-family: \'Questrial\', Helvetica, sans-serif; line-height: 24px;" class="text_color" align="center">
										<!-- ======= section subtitle ====== -->
										
										<div class="editable_text" style="line-height: 24px;">
											<span class="text_container">... to experience Giftvise - the perfect way to give and receive the right gift for the right event. You are receiving this email because someone on our site really wants you to share in the Giftvise experience. 
										</div>
			        				</td>
								</tr>
							</tbody></table>
						</td>
					</tr>
				<tr><td style="font-size: 20px; line-height: 20px;" height="20">&nbsp;</td></tr>
                                        
                                        <tr>
                                                <td align="center">
                                                        <table class="container580" width="580" align="center" border="0" cellpadding="0" cellspacing="0">
                                                                <tbody><tr>
                                                                        <td style="color: #878b99; font-size: 14px; font-family: \'Questrial\', Helvetica, sans-serif; line-height: 24px;" class="text_color" align="center">
                                                                                <!-- ======= section subtitle ====== -->
                                                                                
                                                                                <div class="editable_text" style="line-height: 24px;">
                                                                                        <span class="text_container">Curious? We would be, too. Well, what are you waiting for? Click the button below and see what all the hubbub is about...
                                                                                </div>
                                                                </td>
                                                                </tr>
                                                        </tbody></table>
                                                </td>
                                        </tr>
	
					<tr class="hide"><td style="font-size: 30px; line-height: 30px;" height="30">&nbsp;</td></tr>
					
						<tr><td align="center">
							
							<table class="main_color main-button" style="border-radius: 50px;" width="240" align="center" bgcolor="f06e6a" border="0" cellpadding="0" cellspacing="0">
								
								<tbody><tr><td style="font-size: 18px; line-height: 18px;" height="18">&nbsp;</td></tr>
								
								<tr>
	                				<td style="color: #ffffff; font-size: 18px; font-family: \'Questrial\', Helvetica, sans-serif;" align="center">
	                					<!-- ======= main section button ======= -->
	                					
		                    			<div class="editable_text" style="line-height: 24px;">
		                    				<span class="text_container">
			                    			<a href="'. $inviteLink . '" style="color: #ffffff; text-decoration: none;">Register here ...</a> 
		                    				</span>
										</div>
		                    		</td>								
								</tr><tr><td style="font-size: 48px; line-height: 18px;" height="18">&nbsp;</td></tr>
							</tbody></table>
						</td></tr>
					
					<tr><td style="font-size: 20px; line-height: 20px;" height="40">&nbsp;</td></tr>
					
					<tr>
							
					</tr>
				</tbody></table>
			</td>
		</tr>		
	</tbody></table>
	<!-- ======= end section ======= -->

	<!-- ======= features section ======= -->
	<!-- ======= end section ======= -->
	
	<!-- ======= 1/2 text 1/2 bg image ======= -->
	
	<!-- ======= end section ======= -->
	
	
	<!-- ======= 2 columns testimonials ======= -->
	
	<!-- ======= end section ======= -->
	
	
	<!-- ======= 2 columns image and text ======= -->
	
	<!-- ======= end section ======= -->
	
	
	<!-- ======= section with big button ====== -->
	
	<!-- ======= end section ======= -->
	
	
	<!-- ======= 1/2 text 1/2 image ======= -->
	
	<!-- ======= end section ======= -->

	
	<!-- ======= gallery section ======= -->
	
	<!-- ======= end section ======= -->

	
	<!-- ======= CTA section ======= -->
	
	<!-- ======= end main section ======= -->

	
	<!-- ======= 2 columns headline and text ======= -->
	
	<!-- ======= end section ======= -->
	
	
	<!-- ======= large headline and text ======= -->
	
	<!-- ======= end section ======= -->
	
 <!-- ======= contact section ======= -->


<table style="background-color:rgb(21,18,18)" cellpadding="0" cellspacing="0" bgcolor="ecebeb" border="0" width="100%">

 <tbody><tr><td style="font-size: 20px; line-height: 20px;" height="20">&nbsp;</td></tr>

<tr> <td> <table class="container590" width="90%" align="center" border="0" cellpadding="0" cellspacing="0">

 <tbody><tr> <td> <table style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="container590" width="35%" align="left" border="0" cellpadding="0" cellspacing="0">

 <tbody><tr> <td style="color: #ffffff; font-size: 18px; font-family: \'Questrial\', Helvetica, sans-serif; line-height: 22px;" class="white_color" align="right"> 
         <!-- ======= section text ====== -->

 <div class="editable_text" style="line-height: 22px"> <span class="text_container">

 Follow us

 </span> </div> </td> </tr>

 

 <tr>  </tr>

 

 <tr> <td align="left"> <table align="right" border="0" cellpadding="0" cellspacing="0"> <tbody><tr>  <td> <a style="display: block; width: 30px;
height: 30px; border-style: none !important; border: 0 !important;" href="https://www.facebook.com/Giftvise" class="editable_img"><img style="display:
block; width: 30px; height: 30px;" src="https://giftvise.com/newdesign/img/tpl/SM_icons_48x48-02_FB.png" alt="facebook" class="" height="30" width="30" border="0"></a> </td> <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td> <td> <a style="display:
block; width: 30px; height: 30px; border-style: none !important; border: 0 !important;" href="https://www.twitter.com/giftvise" class="editable_img"><img style="display: block; width: 30px; height: 30px; align=left" src="https://giftvise.com/newdesign/img/tpl/SM_icons_48x48-03_TW.png" alt="twitter" height="30" width="30" border="0"></a> </td> </tr>
<tr><td style="font-size: 15px; line-height: 15px;" height="20">&nbsp;</td></tr>
 </tbody></table> </td> </tr>
</tbody></table>

 <table style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="container590" width="30%" align="right" border="0" cellpadding="0" cellspacing="0">

 <tbody><tr> <td style="color: #ffffff; font-size: 18px; font-family: \'Questrial\', Helvetica, sans-serif; line-height: 22px;" class="white_color" align="left"> 
         <!-- ======= section text ====== -->

 <div class="editable_text" style="line-height: 22px"> <span class="text_container">

 Get in touch
</span> </div> </td> </tr>


 <tr> <td style="color: #878b99; font-size: 14px; font-family: \'Questrial\', Helvetica, sans-serif; line-height: 25px;" class="text_color" align="left"> 
         <!-- ======= section subtitle ====== -->

 <div class="editable_text" style="line-height: 25px;"> <span class="text_container">feedback @ giftvise.com<br></span> </div> </td> </tr>

 </tbody></table> </td> </tr> </tbody></table> </td> </tr>

 </tbody></table>

<!-- copy right secion -->
<table border="0" width="100%" cellpadding="0" cellspacing="0" bgcolor="#ffffff" class="bg4_color">

 <tbody><tr><td height="20" style="font-size:20px;line-height:20px;">&nbsp;</td></tr>

 <tr> <td align="center">

 <table border="0" align="center" width="590" cellpadding="0" cellspacing="0" class="container590">

 <tbody><tr> <td align="center">

 <table border="0" align="center" cellpadding="0" cellspacing="0"> <tbody><tr> 
<td style="color:#5a5f70;font-size:14px;font-family:\'Questrial\',Helvetica,sans-serif;line-height:25px" align="center">
<div class="editable_text" style="line-height:25px;"> <span class="text_container">

 Copyright @ Giftvise 2014

 </span> </div> </td> </tr>

 </tbody></table>

 </td> </tr>

 </tbody></table> </td> </tr>

 <tr><td height="20" style="font-size:" 20px;="" line-height:="">&nbsp;</td></tr>

 </tbody></table>
</div>


</body></html>

                           ';
                        $to = $params['email_id'];

                        //send invitation
                        $requestData['user_id'] = $_SESSION['user_id'];
                        $requestData['email'] = $to;

                        //check for invitation
                        $chkExistRequest = usersfriend::checkSentRquest($params['user_id'], $params['email_id']);

                        $subject = $user_details->first_name.' '. $user_details->last_name.' sent you an invitation to join Giftvise';
                        if(empty($chkExistRequest))
                        {
								$db->beginTransaction();
                                $db->insert('invitedRequest', $requestData);
                                $request_id=$db->lastInsertId();
                                $db->commit();

                                $transport = new Zend_Mail_Transport_Sendmail($config->supportEmail);
                                Zend_Mail::setDefaultTransport($transport);
                                $mail = new Zend_Mail();
                                $mail->setSubject($subject);
                                $mail->setFrom($config->supportEmail, 'Giftvise');
                                $mail->addTo($to);
                                $mail->setBodyHtml($mailText);
                                $mail->send();
                                $msg = "Invitation has been sent.";
								$success = true;
                        }else{
                                $msg = "You have already sent request to this user";
                        }
                }
                catch(Exception $e)
                {
					$msg = $e->getMessage();
                }
                return $this->_helper->json(array('success' => $success, 'msg' => $msg));
	}

 public function shareitemsAction()
        {
                $db  = Zend_Registry::get("db");
                $config = Zend_Registry::get("config");
                $server = Zend_Registry::get("server");

                $params = $this->getRequest()->getParams();
                $m = $this->_initinvite();
                $user_details = users::getUserDetails($_SESSION['user_id']);

                try{
                        $db->beginTransaction();
                        // $this->fields->validate();

                        $mailText = '

<html><head>

<!-- Define Charset -->
        <meta http-equiv="Content-Type" content="text/html;" charset="UTF-8">
        
        <!-- Responsive Meta Tag -->
        <meta name="viewport" content="width=device-width;" initial-scale="1.0;" maximum-scale="1.0;">
        <link href="http://fonts.googleapis.com/css?family=Questrial" rel="stylesheet" type="text/css">
        
        <!--- Importing Twitter Bootstrap icons -->
        <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">

</head><body>
<div marginheight="0" marginwidth="0" align="center">
	<table style="background-color:rgb(21,18,18)" cellpadding="0" cellspacing="0" bgcolor="ecebeb" border="0" width="100%">
		<tbody><tr>
		<td>
				<table cellpadding="0" cellspacing="0" align="center" border="0" width="100%">
					<tbody><tr><td style="font-size:30px;line-height:30px" height="30"> </td></tr>
					<tr>
						<td>
							<table style="border-collapse:collapse" cellpadding="0" cellspacing="0" align="left" border="0" width="45%">
								<tbody><tr>
									<td align="center">
										<table cellpadding="0" cellspacing="0" align="center" border="0">
											<tbody><tr>
												<td style="line-height:21px" align="center">

	<a href="https://giftvise.com/" style="display:block;border-style:none!important;border:0!important" target="_blank"><img style="display:block;width:128px" src="https://www.giftvise.com/newdesign/img/tpl/new-giftvise-logo.png" width="180"></a>
												</td>			
											</tr>
										</tbody></table>		
									</td>
								</tr>
								
							</tbody></table>
							
							<table style="border-collapse:collapse" cellpadding="0" cellspacing="0" align="left" border="0" width="5px">
								<tbody><tr><td style="font-size:20px;line-height:20px" height="20" width="5px"> </td></tr>
							</tbody></table>
							
							<table style="border-collapse:collapse" cellpadding="10" cellspacing="0" align="right" border="0" width="30%">
								
								<tbody><tr>
									<td>
										<table style="border-collapse:collapse" cellpadding="0" cellspacing="0" align="left" border="0">
			
				                			<tbody><tr>
				                				<td>
				                					<table style="border-collapse:collapse" cellpadding="0" cellspacing="0" align="center">
				                						<tbody><tr>
				                							
							                				<td style="color:#ffffff;font-size:14px;font-family:\'Questrial\',Helvetica,sans-serif;line-height:24px" align="center">
							                					<div style="line-height:24px">
							                						<span>
<a href="https://'.$server->apphost.'/modals/modalshowhelpcenter?tab=about" style="color:#878b99;text-decoration:none">About</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="https://'.$server->apphost.'/modals/modalshowhelpcenter?tab=faq" style="color:#878b99;text-decoration:none">Help</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="https://'.$server->apphost.'/modals/modalshowhelpcenter?tab=contact" style="color:#878b99;text-decoration:none">Contact</a>
								                					
							                						</span>
							                					</div>	
							                				</td>
				                						</tr>
				                					</tbody></table>
				                				</td>
				                			</tr>
			                			</tbody></table>
									</td>
								</tr>	
							</tbody></table>	
						</td>
					</tr>
					
					<tr><td style="font-size:25px;line-height:25px" height="25"> </td></tr>
					<tr>
						<td style="line-height:21px" align="center">
						</td>			
					</tr>
					
					<tr><td style="font-size:50px;line-height:50px" height="50"> </td></tr>
					<tr>
						<td style="color:#ffffff;font-size:40px;font-family:\'Questrial\',Helvetica,sans-serif;line-height:28px" align="center">
							
							
							
							<div style="line-height:28px">
								<span>'.$user_details->first_name.' shared a wishlist!</span>
							</div>
        				</td>
					</tr>
				        <tr><td style="font-size:25px;line-height:25px"  height="25" > </td></tr> 	
					<tr>
						<td align="center">
							<table cellpadding="0" cellspacing="0" align="center" border="0" width="540">
								<tbody><tr>
									
									<td style="color:#878b99;font-size:18px;font-family:\'Questrial\',Helvetica,sans-serif;line-height:30px" align="center">
										<div style="line-height:30px">
											
											
											<span>
Looks like someone is thinking of you! Click the button below to check out the wishlist '.$user_details->first_name.' filtered just for you...
										</div>
			        				</td>	
								</tr>
							</tbody></table>
						</td>
					</tr>
					
					<tr><td style="font-size:35px;line-height:35px" height="35"> </td></tr>
					
				</tbody></table>
			</td></tr>
					
					<tr>
						<td align="center">
							
							<table width="240" align="center" bgcolor="f06e6a" border="0" cellpadding="0" cellspacing="0" style="border-radius:50px;">
								
								<tbody><tr><td style="font-size: 18px; line-height: 18px;" height="18">&nbsp;</td></tr>
								
								<tr>
	                				<td style="color: #ffffff; font-size: 18px; font-family: \'Questrial\', Helvetica, sans-serif;" align="center">
	                					<!-- ======= main section button ======= -->
	                					
		                    			<div class="editable_text" style="line-height: 24px;">
		                    				<span class="text_container"><a href=https://' . $server->apphost. '/Friends/shareditems?sender-id='. $_SESSION["user_id"]. '&item_ids='. $params["item_ids"].' style="color: #ffffff; text-decoration: none;">View Wishlist</a></span>
		                    			</div>
		                    		</td>								
								</tr>
								
								<tr><td style="font-size: 18px; line-height: 18px;" height="18">&nbsp;</td></tr>
							
							</tbody></table>
						</td>
					</tr>
                              <tr><td style="font-size: 35px; line-height: 35px;" height="35">&nbsp;</td></tr>
        </tbody></table>
	
		<!-- ======= features section ======= -->

<table border="0" width="100%" cellpadding="0" cellspacing="0" bgcolor="f3f4f5" class="bg_color">
		<tbody><tr>
			<td>
				<table border="0" align="center" width="100%" cellpadding="0" cellspacing="0" class="container590 bg_color">
					
					<tbody><tr class="hide"><td height="30" style="font-size: 30px; line-height: 30px;">&nbsp;</td></tr>
					<tr><td height="50" style="font-size: 50px; line-height: 50px;">&nbsp;</td></tr>
					
					<tr>
						<td align="center" style="color: #414655; font-size: 28px; font-family: \'Questrial\', Helvetica, sans-serif; line-height: 22px;" class="title_color">
							<!-- ======= section text ====== -->
							
							<div style="line-height: 22px">
	        					
	        					Gifting made simple with Giftvise
	        					
							</div>
        				</td>	
					</tr>
					
					<tr><td height="20" style="font-size: 20px; line-height: 20px;">&nbsp;</td></tr>
					
					<tr>
						<td align="center">
							<table border="0" width="24" align="center" cellpadding="0" cellspacing="0" bgcolor="f06e6a" style="border-radius: 4px;">
								<tbody><tr><td height="4" style="font-size: 4px; line-height: 4px;">&nbsp;</td></tr>
							</tbody></table>
						</td>
					</tr>
					
					<tr><td height="20" style="font-size: 20px; line-height: 20px;">&nbsp;</td></tr>
					
					<tr>
						<td align="center">
							<table border="0" width="100%" align="center" cellpadding="0" cellspacing="0" class="container580">
								<tbody><tr>
									<td align="center" style="color: #878b99; font-family: \'Questrial\', Helvetica, sans-serif; line-height: 24px;" class="text_color">
										<!-- ======= section subtitle ====== -->
										
								<div style="line-height: 24px;">
									       <div style="color: #414655;font-size: 20px;">		
					        				Discover new products
                                                                               </div>
                                                                         <div style="font-size: 14px;">
                                                                           Explore curated products, see what\'s trending, and get recommendations from friends and family
						                     	</div>	
								</div>
                                                                <br>
                                                                <div style="line-height: 24px;">
                                                                               <div style="color: #414655;font-size: 20px;">            
                                                                               Create a universal wishlist on-the-go 
                                                                               </div>
                                                                         <div style="font-size: 14px;">
                                                                           Create a single wishlist for all your events with products from any retailer using any device
                                                                        </div>  
                                                                </div><br>
                                                                <div style="line-height: 24px;">
                                                                               <div style="color: #414655;font-size: 20px;">            
                                                                               Give and receive the perfect gift 
                                                                               </div>
                                                                         <div style="font-size: 14px;">
                                                                           Reserve the perfect gift for your friends and family. They just may return the favor!
                                                                         </div> 
                                                                </div>
			        				</td>
								</tr>
							</tbody></table>
						</td>
					</tr>
					
					<tr class="hide"><td height="30" style="font-size: 30px; line-height: 30px;">&nbsp;</td></tr>
					<tr><td height="50" style="font-size: 50px; line-height: 50px;">&nbsp;</td></tr>
					
				</tbody></table>
			</td>
		</tr>
	</tbody></table>



		<!-- ======= end section ======= -->
	
	
	<!-- ======= 1/2 text 1/2 bg image ======= -->
	
	<!-- ======= end section ======= -->
	
	
	<!-- ======= 2 columns testimonials ======= -->
	
	<!-- ======= end section ======= -->
	
	
	<!-- ======= 2 columns image and text ======= -->
	
	<!-- ======= end section ======= -->
	
	
	<!-- ======= section with big button ====== -->
	
	<!-- ======= end section ======= -->
	
	
	<!-- ======= 1/2 text 1/2 image ======= -->
	
	<!-- ======= end section ======= -->

	
	<!-- ======= gallery section ======= -->
	
	<!-- ======= end section ======= -->

	
	<!-- ======= CTA section ======= -->
	
	<!-- ======= end main section ======= -->

	
	<!-- ======= 2 columns headline and text ======= -->
	
	<!-- ======= end section ======= -->
	
	
	<!-- ======= large headline and text ======= -->
	
	<!-- ======= end section ======= -->
	
 <!-- ======= contact section ======= -->

<table style="background-color:rgb(21,18,18)" cellpadding="0" cellspacing="0" bgcolor="ecebeb" border="0" width="100%">

 <tbody><tr><td style="font-size: 20px; line-height: 20px;" height="20">&nbsp;</td></tr>

<tr> <td> <table class="container590" width="90%" align="center" border="0" cellpadding="0" cellspacing="0">

 <tbody><tr> <td> <table style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="container590" width="35%" align="left" border="0" cellpadding="0" cellspacing="0">

 <tbody><tr> <td style="color: #ffffff; font-size: 18px; font-family: \'Questrial\', Helvetica, sans-serif; line-height: 22px;" class="white_color" align="right"> 
         <!-- ======= section text ====== -->

 <div class="editable_text" style="line-height: 22px"> <span class="text_container">

 Follow us

 </span> </div> </td> </tr>

 

 <tr>  </tr>

 

 <tr> <td align="left"> <table align="right" border="0" cellpadding="0" cellspacing="0"> <tbody><tr>  <td> <a style="display: block; width: 30px;
height: 30px; border-style: none !important; border: 0 !important;" href="https://www.facebook.com/Giftvise" class="editable_img"><img style="display:
block; width: 30px; height: 30px;" src="https://giftvise.com/newdesign/img/tpl/SM_icons_48x48-02_FB.png" alt="facebook" class="" height="30" width="30" border="0"></a> </td> <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td> <td> <a style="display:
block; width: 30px; height: 30px; border-style: none !important; border: 0 !important;" href="https://www.twitter.com/giftvise" class="editable_img"><img style="display: block; width: 30px; height: 30px; align=left" src="https://giftvise.com/newdesign/img/tpl/SM_icons_48x48-03_TW.png" alt="twitter" height="30" width="30" border="0"></a> </td> </tr>
<tr><td style="font-size: 15px; line-height: 15px;" height="20">&nbsp;</td></tr>
 </tbody></table> </td> </tr>
</tbody></table>

 <table style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="container590" width="30%" align="right" border="0" cellpadding="0" cellspacing="0">

 <tbody><tr> <td style="color: #ffffff; font-size: 18px; font-family: \'Questrial\', Helvetica, sans-serif; line-height: 22px;" class="white_color" align="left"> 
         <!-- ======= section text ====== -->

 <div class="editable_text" style="line-height: 22px"> <span class="text_container">

 Get in touch
</span> </div> </td> </tr>

 

 <tr> <td style="color: #878b99; font-size: 14px; font-family: \'Questrial\', Helvetica, sans-serif; line-height: 25px;" class="text_color" align="left"> 
         <!-- ======= section subtitle ====== -->

 <div class="editable_text" style="line-height: 25px;"> <span class="text_container">feedback @ giftvise.com<br></span> </div> </td> </tr>

 </tbody></table> </td> </tr> </tbody></table> </td> </tr>

 </tbody></table>

<!-- copy right secion -->
<table border="0" width="100%" cellpadding="0" cellspacing="0" bgcolor="#ffffff" class="bg4_color">

 <tbody><tr><td height="20" style="font-size:20px;line-height:20px;">&nbsp;</td></tr>

 <tr> <td align="center">

 <table border="0" align="center" width="590" cellpadding="0" cellspacing="0" class="container590">

 <tbody><tr> <td align="center">

 <table border="0" align="center" cellpadding="0" cellspacing="0"> <tbody><tr> 
<td style="color:#5a5f70;font-size:14px;font-family:\'Questrial\',Helvetica,sans-serif;line-height:25px" align="center">
<div class="editable_text" style="line-height:25px;"> <span class="text_container">

 Copyright @ Giftvise 2014

 </span> </div> </td> </tr>

 </tbody></table>

 </td> </tr>

 </tbody></table> </td> </tr>

 <tr><td height="20" style="font-size:" 20px;="" line-height:="">&nbsp;</td></tr>

 </tbody></table>
</div>


</body></html>

                        ';

                        $to = $params['email_id'];

                        $subject = $user_details->first_name.' '. $user_details->last_name.' Shared a Wishlist With You on Giftvise';
                        $transport = new Zend_Mail_Transport_Sendmail($config->supportEmail);
                        Zend_Mail::setDefaultTransport($transport);
                        $mail = new Zend_Mail();
                        $mail->setSubject($subject);
                        $mail->setFrom($config->supportEmail, 'Giftvise');
                        $mail->addTo($to);
                        $mail->setBodyHtml($mailText);
                        $mail->send();
                        $msg = "Gifts have been shared.";
                }
                catch(Exception $e)
                {
                      return $this->_helper->json(array('msg' => $e->getMessage()));
                }
                return $this->_helper->json(array('msg' => $msg));
        }

 public function shareditemsAction() {
        ///BOF FBtwi
        //require 'openinviter/facebook.php';
        $server = Zend_Registry::get("server");
        $config = Zend_Registry::get("config");
        $logger = Zend_Registry::get("logger");
        $db = Zend_Registry::get("db");

        $logger->info("[START] " . __CLASS__ . ' : ' . __FUNCTION__);

        $logger->info("--- ACCESS-INFO --- ::FRIEND-PAGE::USER-ID::" .$_SESSION['user_id']. "DEVICE-INFO:: ".
                      $_SERVER['HTTP_USER_AGENT'] . "IP ADDRESS ". $_SERVER['REMOTE_ADDR'] ."\n");

/*
        if ($_SESSION['logged_in'] == true) {
           $params = $this->getRequest()->getParams();
           $userid = $params['sender-id'];
           $itemIds = $params['item_ids'];
           $this->_redirect(PS_Util::redirect("https://" . $server->apphost . "/newfriends/friendprofile?shared=1&itemIds=" .$itemIds. "&friend_id=" . md5($userid) . ""));
           exit;
        }
*/
        require 'fb/facebook.php';
        require 'amazonQuery.php';

        Facebook::$CURL_OPTS[CURLOPT_SSL_VERIFYPEER] = false;
        Facebook::$CURL_OPTS[CURLOPT_SSL_VERIFYHOST] = 2;

        $FBparams = array(
                'scope' => 'user_events, user_birthday',
                );

        $facebook = new Facebook(array(
                'appId' => $server->FACEBOOK_APP_ID,
                'secret' => $server->FACEBOOK_SECRET,
                ));

       $loginUrl = $facebook->getLoginUrl($FBparams);
       $_SESSION['fb_url'] = $loginUrl;
       $logger->info("userfb is False. fb_url = " . $_SESSION['fb_url']);
    
       $params = $this->getRequest()->getParams();
       $userid = $params['sender-id'];
       $itemIds = $params['item_ids'];

       $products = wishlistitmes::getSharedItemsLandingPage($userid, $itemIds);
       $this->view->more_items = 0;

       for ($i = 0; $i < count($products); $i++) {
           $item = array();
           $totalComments = itemcomments::countComments($products[$i]->id);
           $countwishes = wishlistitmes::countWished($products[$i]->id);
           $timeAdded = wishlistitmes::latestItemHome($products[$i]->id);
           $addeduser = users::getUserDetails($timeAdded['user_id']);
           $item['comment'] = $totalComments;
           $item['wished'] = ''; 
           $item['countwishes'] = $countwishes;
           $item['added_time'] = $timeAdded['time'];
           $item['first_name'] = $addeduser->first_name;
           $item['last_name'] = $addeduser->last_name;
           $item['user_id'] = $addeduser->user_id;
           $items[] = $item;
       }
       $senduser = users::getUserDetails($userid);
       $this->view->senderfirstname = $senduser->first_name;
       $this->view->items = $products;
       //$this->view->extval = $items;
       $this->view->extval = wishlistitmes::prepareExtVal($products, $_SESSION['user_id']);
       $this->view->vpage=1;
       $this->view->shared_items = 1;
       $this->view->fbAppID = $server->FACEBOOK_APP_ID;

       $user_details = users::getUserDetails($_SESSION['user_id']);
       $this->view->userDetail = $user_details;

       $w_error = '';
       $this->view->error_msg = $w_error;

       /// follows
       $followCount = usersfriend::countFollowList($senduser->user_id);
       $this->view->follow = $followCount;

       /// followers
       $followerCount = usersfriend::countFollowerList($senduser->user_id);
       $this->view->followers = $followerCount;

       $this->view->friendDetail = $senduser; 

        // total gift send by user
        $this->view->giftsend = event::giftSend($senduser->user_id);

        // total gift recived by user
        $this->view->giftget = event::giftGet($senduser->user_id);

        $this->view->vpage = 4;
        $isfollower = usersfriend::checkExistFriendStatus($senduser->user_id, $_SESSION['user_id']);
        $tmp_follow = ($isfollower != '' && $isfollower->status == '1') ? 'Y' : 'N';
        $this->view->isfollower =  $tmp_follow;
        $this->view->request_status = usersfriend::getRequestStatus($senduser->user_id, $_SESSION['user_id']);

       $this->_helper->viewRenderer("friendwishlist");
       $this->_helper->resources("friendwishlist");
}
	protected function _initinvite()
	{
		$invite_text = new PS_Validate_NotEmpty($this->_getParam("invite_text"));
		$invite_text->setMessage(PS_Translate::_("Please enter message"));

		$this->fields = new PS_Form_Fields(
			array(
				"email_id" => array(
					new PS_Validate_EmailAddress(),
					"filter" => new PS_Filter_RemoveSpaces(),
					"required" => true
				)
			)
		);
		$params = $this->getRequest()->getParams();

		$this->fields->setData($params);
		$this->view->fields 		= $this->fields;

	}///end function
	///


	// sent invitation to your Facebook friends
	public function fbinviteAction()
	{
		$server = Zend_Registry::get("server");
        $facebookAccessToken = $this->getRequest()->getParam("access_token");

		try{
            $this->view->friendlist = usersfriend::prepareFacebookFriends();
			$this->view->userDetail = users::getUserDetails($_SESSION['user_id']);
			
			$groups = groups::groupList($_SESSION['user_id']);
			$this->view->groups = $groups;
			$this->view->twitterFriend = true;
			// notifications
			$this->view->newNotifications = notifications::countnewnotifications();
			$this->view->activityLog = notifications::activitylog(true);
			$this->view->notificationsCount = notifications::notificationscount();

			$groups = groups::getFriendsGroupIBelong($_SESSION['user_id']);
			$this->view->GroupIBelong = $groups;
			$this->view->group_id = $_SESSION['group_id'];

			$this->view->fbAppID = $server->FACEBOOK_APP_ID;
			$this->view->fbSecret = $server->FACEBOOK_SECRET;

		}catch(FacebookApiException $e)
  		{
            $this->_helper->json(array('fail' => true, 'msg' => $e->getMessage()));
		}
		
	}

    public function invitefacebookAction(){
        $server = Zend_Registry::get("server");
        try{
            $this->view->friendlist = usersfriend::prepareFacebookFriends($server);
        }catch(FacebookApiException $e)
        {
            $this->view->friendlist = '';
        }
        //user
        $this->view->userDetail = users::getUserDetails($_SESSION['user_id']);
        $groups = groups::groupList($_SESSION['user_id']);
        $this->view->groups = $groups;
        $this->view->twitterFriend = true;
        // notifications
        $this->view->newNotifications = notifications::countnewnotifications();
        $this->view->activityLog = notifications::activitylog(true);
        $this->view->notificationsCount = notifications::notificationscount();
        $this->view->fbAppID = $server->FACEBOOK_APP_ID;
        $this->view->fbSecret = $server->FACEBOOK_SECRET;
    }

	/// send invitation to your Facebook friends
	/// invitaion will be post on their wall
	public function sendinvitaionfbAction()
	{
		$db  = Zend_Registry::get("db");
		$config = Zend_Registry::get("config");
  		$server = Zend_Registry::get("server");

		$params = $this->getRequest()->getParams();
		$friendFBID = $params['fbid'];

		require 'fb/facebook.php';

		Facebook::$CURL_OPTS[CURLOPT_SSL_VERIFYPEER] = false;
		Facebook::$CURL_OPTS[CURLOPT_SSL_VERIFYHOST] = 2;

		try{
			$facebook = new Facebook(array(
			  'appId'  => $server->FACEBOOK_APP_ID,
			  'secret' => $server->FACEBOOK_SECRET,
			));

			$link = 'https://'.$server->apphost;
			$picture = 'https://'.$server->apphost.'/images/drag-icon.png';

			$message = 'Giftvise - Happy gifting!';
			//$statusUpdate = $facebook->api('/'.$friendFBID.'/feed', 'post', array('message'=> $message, 'cb' => ''));
			$statusUpdate = $facebook->api('/'.$friendFBID.'/feed', 'post', array('description'=> $message, 'link'=> $link,'name'=>'Giftvise', 'cb' => '', 'picture'=>$picture));

			//array(?access_token?=>[ACCESS TOKEN], ?picture?=>$picture, ?link?=> $link, ?name?=>$name, ?caption?=>$caption, ?description?=>$description, ?message?=> $message, ?cb? => ?)
			echo 'Invitation has been sent.';

		}catch(FacebookApiException $e)
  		{
		   $this->view->errors[] = $e->getMessage();
		   print_r($e->getMessage());
		}
		exit;
	}

	// sent invitation to your Twitter friends
	public function invitetwitterAction()
	{
  		$server = Zend_Registry::get("server");

		require 'twitter/twitteroauth.php';

		$twitteroauth = new TwitterOAuth($server->Consumer_key, $server->Consumer_secret, $_SESSION['oauth_token'], $_SESSION['oauth_token_secret']);
		$access_token = $twitteroauth->getAccessToken($_REQUEST['oauth_verifier']);

		$_SESSION['access_token'] = $access_token;
		session::write(session_id(), $_SESSION);

		$user_info = $twitteroauth->get('account/verify_credentials');

		if(isset($_GET['denied']))
		{
			$this->_redirect(PS_Util::redirect($server->securehttp.$server->apphost."/friends/"));
		}else{
			/// get friends followers
			//$frienURL = 'https://api.twitter.com/1/statuses/friends.json?user_id='.$user_info->id;
			//$frienURL = 'https://api.twitter.com/1.1/statuses/followers.json?user_id='.$user_info->id;
			/*
			$frienURL = 'https://api.twitter.com/1.1/friends/list.json?user_id='.$user_info->id;
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $frienURL);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			$curlout = curl_exec($ch);
			curl_close($ch);
			$response = json_decode($curlout, true);
			*/
			/*echo '<pre>';
		print_r($response);
		exit;*/
// 			$logger->log($response, Zend_Log::INFO);

			$friendsListURL = 'friends/list';
			$friends_params = array(
					'screen_name' => $user_info->screen_name,
					'cursor' => -1, //start with -1
					'skip_status' => 'true',
					'include_user_entities' => 'false',
					);
			$friendlist = $this->twitterGet($friendsListURL, $friends_params, $twitteroauth);

            $followersListURL = 'followers/list';
            $followers_params = array(
                'screen_name' => $user_info->screen_name,
                'cursor' => -1, //start with -1
                'skip_status' => 'true',
                'include_user_entities' => 'false',
            );

            $followerslist = $this->twitterGet($followersListURL, $followers_params, $twitteroauth);

			///
			$sortArT = sort($friendlist);
		}

		$this->view->friendlist = array_intersect($followerslist, $friendlist);
		//user
		$user_details = users::getUserDetails($_SESSION['user_id']);
		$this->view->userDetail = $user_details;
        $groups = groups::groupList($_SESSION['user_id']);
        $this->view->groups = $groups;
        $this->view->twitterFriend = true;
        // notifications
        $this->view->newNotifications = notifications::countnewnotifications();
        $this->view->activityLog = notifications::activitylog(true);
        $this->view->notificationsCount = notifications::notificationscount();

		$groups = groups::getFriendsGroupIBelong($_SESSION['user_id']);
		$this->view->GroupIBelong = $groups;
		$this->view->group_id = $_SESSION['group_id'];
    }

    // generate twitter login URL and redirect to the Twitter login page.
    public function twiterinviteAction()
    {
        $server = Zend_Registry::get("server");
        require 'twitter/twitteroauth.php';
        $twitteroauth = new TwitterOAuth($server->Consumer_key, $server->Consumer_secret);
        $request_token = $twitteroauth->getRequestToken($server->securehttp.$server->apphost."/friends/invitetwitter");

        $_SESSION['oauth_token'] = $request_token['oauth_token'];
        $_SESSION['oauth_token_secret'] = $request_token['oauth_token_secret'];
        session::write(session_id(), $_SESSION);

        if ($twitteroauth->http_code == 200) {
            // Let's generate the URL and redirect
            $url = $twitteroauth->getAuthorizeURL($request_token['oauth_token']);
            PS_Util::redirect($url);
        } else {
            // It's a bad idea to kill the script, but we've got to know when there's an error.
            die('Something wrong happened.');
        }
    }///end function

	/// send message to twitter followers
	public function sendtwitterinvitationAction()
	{
  		$server = Zend_Registry::get("server");

		$params = $this->getRequest()->getParams();
		$twitterId = $params['id'];
		require 'twitter/twitteroauth.php';
		$twitteroauth = new TwitterOAuth($server->Consumer_key, $server->Consumer_secret, $_SESSION['access_token']['oauth_token'], $_SESSION['access_token']['oauth_token_secret']);

		try{
			$hashId =  md5($_SESSION['user_id']);
			$hashGroup = (empty($_SESSION['group_id'])) ? '' : '&group='.md5($_SESSION['group_id']);
			$inviteLink = $server->securehttp . $server->apphost .'/login/register?friend_id='. $hashId . $hashGroup;

			$message='Check this site:';
			$message .= " (copy this url in your browser" . $inviteLink. ")";

			$method = 'direct_messages/new';
			$parameters = array('user_id' => $twitterId, 'text' => $message );
			$dm = $twitteroauth->post($method, $parameters);
            return $this->_helper->json(array('success' => true, 'result' => $dm));
		}catch(Exception $e)
  		{
		   $this->view->errors[] = $e->getMessage();
		   print_r($e->getMessage());
           return $this->_helper->json(array('success' => false, 'error' => $e->getMessage()));
		}
	}

	// Block friend
	public function blockfriendAction()
	{
		$db = Zend_Registry::get("db");
		$config = Zend_Registry::get("config");
		$server = Zend_Registry::get("server");
                $logger = Zend_Registry::get("logger");

		$params = $this->getRequest()->getParams();

		$user_id = $params['user_id'];
		$friend_id = $params['friend_id'];

		try{
			$db->beginTransaction();

                        $groups = groups::groupList($user_id);
                        foreach($groups as $group) {
                               $group_id = $group->id;
                              $member = groups::checkfriendInGroup($friend_id, $group_id);
                               if($member) {
                                     groups::deleteFriendGroup($group_id, $friend_id);
                              }
                        }

			$data['status'] = 3; 

			$db->update('usersfriend', $data,'user_id = '.$user_id.' AND friend_id ='.$friend_id);
                        $db->update('usersfriend', $data,'user_id = '.$friend_id.' AND friend_id ='.$user_id);

			$db->commit();
		}
		catch(Exception $e)
		{
		  print_r($e->getMessage());
		  $this->view->errors[] = $e->getMessage();
		}
		echo $params['status'];
		exit;
	}

	/// search friend to add in group
	// Ajax search, input is friends's name
	public function searchfriendtogroupAction()
	{
		$db = Zend_Registry::get("db");
		$config = Zend_Registry::get("config");
		$server = Zend_Registry::get("server");

		$params = $this->getRequest()->getParams();
		$key = $params['key'];
		$keych = '%'.$params['key'].'%';
		$key1 = $db->quote($keych);
		$group_id = $params['group_id'];

		try{
			//$searchResult = usersfriend::searhUser($key1, $_SESSION['user_id']);
			$searchResult = usersfriend::searhfriendsg($key1, $_SESSION['user_id']);

			$result = '<ul>';
			$cnt = 0;
			if(count($searchResult) > 0)
			{
				for($i=0; $i < count($searchResult); $i++)
				{
					$cnt++;
					if($cnt == 4)
					{
						$result .= '<li style="width:220px!important; clear:both;">';
						$cnt = 1;
					}else{
						$result .= '<li style="width:220px!important;">';
					}

					$result .= '<div class="photo">';

					if($searchResult[$i]->profile_pic != '')
					{
						$result .= '<img src="https://'.$server->apphost.'/userpics/thumb/'.$searchResult[$i]->profile_pic.'" border="0" />';
					}else{
						$result .= '<img src="https://'.$server->apphost.'/images/icon.jpg" border="0" />';
					}

					$result .= '</div>';

					$inexist = groups::checkfriendInGroup($searchResult[$i]->user_id,$group_id);

					$result .= '<div class="listtxt" style="width:150px;">
							   <h4>'.$searchResult[$i]->username.'</h4>';
					if($inexist)
					{
						//$result .= '<div class="link"><a href="javascript:void(0);">Already in group</a></div>
						$result .= '<div class="link">&nbsp;</div>
									</div>';
					}else{
						$result .= '<div class="link" id="g_'.$i.'"><a href="javascript:void(0);" onclick="addtoGroup('.$group_id.', '.$searchResult[$i]->user_id.', \'g_'.$i.' \')">Add to group</a></div>
									</div>';
					}

					$result .= '</li>';
				}
				$result .= '</ul>';
				//$result .=	'<input type="button" onclick="window.location.reload();" value="Done"/>';
				$result .= '<div style="clear:both;"></div>';
			}///
			else{
				$result .= '<li style="width:270px!important; margin-left:0px;">No friend with that name was found in your friends list</li>
							</ul>
							<div style="clear:both;"></div>
				';
			}
			echo $result;
			//print_r($searchResult);
		}catch(Exception $e)
		{
			  print_r($e->getMessage());
		}
		exit;
	}

	/* Add friend in a group
	input - user_id, group_id
	*/
	public function addfriendtogroupAction()
	{

        $this->_helper->layout()->disableLayout();
        $params = $this->getRequest()->getParams();

		$friend_id = $params['friend_id'];
		$group_id = $params['group_id'];
        $return = $params['return_null'];

		try{
			
			$msg = groups::addFriendToGroup($friend_id, $group_id);

			if (!$return){
				//// populate new members list
				$this->view->group =groups::getGroupInfo(md5($group_id));
				$this->view->friends = groups::getFriendsFromGroup($group_id, $_SESSION['user_id']);
				$this->render("friendslist");
			} else {
				return $this->_helper->json(array('fail' => false, 'msg' => $msg));
			}
            
        }
		catch(Exception $e)
		{
            return $this->_helper->json(array('fail' => true, 'msg' => $e->getMessage()));
		}
	}

	/* Add friend in a group
	input - user_id, group_id
	*/
	public function addnamefriendtogroupAction()
	{

		$this->_helper->layout()->disableLayout();
		$params = $this->getRequest()->getParams();

		try{

			$friend_id = users::getUserIdByFullName($params['friend_name']);
			$msg = groups::addFriendToGroup($friend_id, $params['group_id']);
			return $this->_helper->json(array('fail' => false, 'msg' => $msg, 'friendId' => $friend_id, 'rr' => $params['friend_name']));

		}
		catch(Exception $e)
		{
			return $this->_helper->json(array('fail' => true, 'msg' => $e->getMessage()));
		}
	}

	/*
	Create new group with name 'Unknown'
	*/
	public function creategroupAction()
	{
		$db = Zend_Registry::get("db");
		$config = Zend_Registry::get("config");
		$server = Zend_Registry::get("server");

		$url = "https://".$server->apphost."/friends/groups/cre/s/";
		PS_Util::redirect($url);
		exit;
	}
	/*public function creategroupAction()
	{
		$db = Zend_Registry::get("db");
		$config = Zend_Registry::get("config");
		$server = Zend_Registry::get("server");

		try{
		$db->beginTransaction();

		$name = 'Unknown';
		$existGroup = groups::checkExistGroup($_SESSION['user_id'],$name);

		if($existGroup)
		{
			$newname = 'Unknown1';
			$existGroupName = groups::chkForGroupName($name, $_SESSION['user_id']);

			if($existGroupName == 'Unknown')
			{
				$name = 'Unknown1';
			}else{
				$existGroupName++;
				$name = $existGroupName;
			}

		}//existgroup

		$Gdata['group_name'] = $name;
		$Gdata['description'] = 'Enter description...';
		$Gdata['created_by'] = $_SESSION['user_id'];
		$Gdata['created_date'] = date('Y-m-d');

			$db->insert('groups', $Gdata);

			$group_id = $db->lastInsertId();
			/// add this user in this group

			$Gdata1['user_id'] = $_SESSION['user_id'];
			$Gdata1['group_id'] = $group_id;
			$db->insert('friendsgroup', $Gdata1);

			$db->commit();

			$url = "http://".$server->apphost."/friends/groups/cre/s/group_id/".md5($group_id);
			PS_Util::redirect($url);

			exit;
		}catch(Exception $e)
		{
		  $db->rollBack();
		  print_r($e);
		  exit;
		}
	}*/

	/* update Group name
	input group_id and group_name
	*/
	public function updategroupnameAction()
	{
		$db = Zend_Registry::get("db");
		$config = Zend_Registry::get("config");
		$server = Zend_Registry::get("server");

		$params = $this->getRequest()->getParams();

		$group_id = $params['group_id'];
		$group_name = $params['group_name'];
		$group_desc = $params['group_desc'];

		$name = $db->quote($params['group_name']);

		try{
			$db->beginTransaction();

			if($group_name != '')
			{
				$data['group_name'] = $group_name;
			}

			if($group_desc != '')
			{
				$data['description'] = $group_desc;
			}

			$existGroup = groups::checkExistGroup($_SESSION['user_id'],$name);
			if($existGroup)
			{
				echo 'err';
				exit;
			}else{
				$db->update('groups', $data,'id = '.$group_id);

			}
			$db->commit();
			echo $group_name;
			exit;

		}catch(Exception $e){
		  $db->rollBack();
		  print_r($e);
		  exit;
		}
	}

	///// search friendlist to share wishlist
	public function searchfriendshareAction()
	{
		$db = Zend_Registry::get("db");
		$config = Zend_Registry::get("config");
		$server = Zend_Registry::get("server");

		$params = $this->getRequest()->getParams();
		$key = $params['key'];
		$item_id = $params['item_id'];

		$searchResult = usersfriend::searchFollowList($_SESSION['user_id'], $key);

				$listStr = '<li><input type="text" name="txtKey" value="'.$key.'" onkeypress="getFriend(this.value, \''.$item_id.'\', \'share_'.$item_id.'\');"></li>';
				if(!empty($searchResult))
				{
					for($i=0; $i < count($searchResult); $i++)
					{
						$list = array();

						$user_detail = users::getUserDetails($searchResult[$i]->friend_id);

						$listStr .= '<li id="c_'.$searchResult[$i]->friend_id.'"><input type="checkbox" id="friend_'.$item_id.'_'.$searchResult[$i]->friend_id.'"  onClick="sharewishlist(\'Friend\', \''.$item_id.'\', \'\', \''.$searchResult[$i]->friend_id.'\',\'friend_'.$item_id.'_'.$searchResult[$i]->friend_id.'\')" value="'.$searchResult[$i]->friend_id.'" />';

						if($user_detail->profile_pic != '')
						{
							$listStr .= '<img src="https://'.$server->apphost.'/userpics/thumb/'.$user_detail->profile_pic.'" style="float:left">';
						}else{
                            $listStr .= '<img src="https://'.$server->apphost.'/userpics/top-img.png" style="float:left">';
						}

						$listStr .= '<div class="slcontent">'.$user_detail->first_name.' '.$user_detail->last_name.'</div>';
						$listStr .= '</li>';
					}
			}else{
				$listStr .= '<li>You do not have any friend.</li>';
			}

			echo $listStr;
			exit;
	}

	// Add friend in Group
	public function addfriendingroupAction()
	{
		$db = Zend_Registry::get("db");
		$config = Zend_Registry::get("config");
		$server = Zend_Registry::get("server");

		$params = $this->getRequest()->getParams();

		$user_id = $params['user_id'];
		$group_id = $params['group_id'];
		$processType = $params['process_type'];

		try{
			$db->beginTransaction();
			if($processType == 'add')
			{
				// add friend in group
				$data['user_id'] = $user_id;
				$data['group_id'] = $group_id;

				$db->insert('friendsgroup',$data);
				$latest_id = $db->lastInsertId();
				//echo $latest_id;

				// record user's activity
				$groupInfo = groups::getGroupInfo(md5($group_id));
				$dataA['sender'] = $_SESSION['user_id'];
				$dataA['receiver'] = $user_id;
				$dataA['activity_date'] = date('Y-m-d H:i:s');
				$dataA['activity_type'] = 'Added in group - '.$groupInfo->group_name;
				//$db->insert('activity', $dataA);
				//

				$db->commit();
			}else{
				// remove friend in group
				$res = $db->delete('friendsgroup', array("user_id = ?" => $user_id, "group_id = ?" => $group_id));

				// record user's activity
				$groupInfo = groups::getGroupInfo(md5($group_id));
				$dataA['sender'] = $_SESSION['user_id'];
				$dataA['receiver'] = $user_id;
				$dataA['activity_date'] = date('Y-m-d H:i:s');
				$dataA['activity_type'] = 'Removed from group - '.$groupInfo->group_name;
				//$db->insert('activity', $dataA);
				//
				$db->commit();
				//echo $res;
			}

			$groupStr = '';
			$friendGroups = groups::getFriendsGroup($_SESSION['user_id'], $user_id);

			for($f=0; $f < count($friendGroups); $f++)
			{
             	$groupStr .= '<span>'.$friendGroups[$f]['groupName'].'</span>';
			}
			echo $groupStr;
			exit;

		}catch(Exception $e){
			//Rollback transaction
			$db->rollBack();
			print_r($e->getMessage());
			exit;
		}
	}

	// delete member from Group
	public function deletefriendfromgroupAction()
	{
		$db = Zend_Registry::get("db");

		$params = $this->getRequest()->getParams();
        $this->_helper->layout()->disableLayout();

        $friend_id = $params['friend_id'];
		$group_id = $params['group_id'];
        $return = $params['return_null'];

		try{
			$db->beginTransaction();

			$res = $db->delete('friendsgroup', array("user_id = ?" => $friend_id, "group_id = ?" => $group_id));

			$groupInfo = groups::getGroupInfo(md5($group_id));
			$dataA['sender'] = $_SESSION['user_id'];
			$dataA['receiver'] = $friend_id;
			$dataA['activity_date'] = date('Y-m-d H:i:s');
			$dataA['activity_type'] = 'Removed from group - '.$groupInfo->group_name;
			//$db->insert('activity', $dataA);
				//
			$db->commit();

	 	}catch(Exception $e){
			//Rollback transaction
			$db->rollBack();
			return $this->_helper->json(array('fail' => true, 'msg' => $e->getMessage()));
		}
        if(!$return){
            $this->view->friends = groups::getFriendsFromGroup($group_id, $_SESSION['user_id']);
            $this->render("friendslist");
        } else {
            return $this->_helper->json(array('fail' => false, 'msg' => 'The friend was removed from group'));
        }
	}

        public function exitgroupAction()
        {
                $db = Zend_Registry::get("db");
                $config = Zend_Registry::get("config");
                $server = Zend_Registry::get("server");

                $params = $this->getRequest()->getParams();
                $this->_helper->layout()->disableLayout();

                $user_id = $params['user_id'];
                $group_id = $params['group_id'];
                $return = $params['return_null'];

                try{
                        $db->beginTransaction();

                        $res = $db->delete('friendsgroup', array("user_id = ?" => $user_id, "group_id = ?" => $group_id));
                        $db->commit();
         //              return $this->_helper->json(array('fail' => false, 'msg' => $e->getMessage()));
                }catch(Exception $e){
                        //Rollback transaction
                        $db->rollBack();
                        return $this->_helper->json(array('fail' => true, 'msg' => $e->getMessage()));
                }
                return $this->_helper->json(array('fail' => false, 'msg' =>"success"));
            
         }

	/// Accept or Reject follower's request
	public function changestatusAction()
	{
		$db = Zend_Registry::get("db");
		$config = Zend_Registry::get("config");
		$server = Zend_Registry::get("server");

		$params = $this->getRequest()->getParams();

		$user_id = $params['user_id'];
		$friend_id = $params['friend_id'];
		$status = $params['status'];
        $activityId = $params['notif_id'];

        $success = true;
		try{
			$db->beginTransaction();
            $data['status'] = $status;
            $chkrelation = usersfriend::checkExistFriends($user_id, $friend_id);
            if($chkrelation != '')
            {
                // update status
                $db->update('usersfriend', $data,'user_id = '.$user_id.' AND friend_id ='.$friend_id);
            }else{
                // insert relation
                $data['user_id'] = $user_id;
                $data['friend_id'] = $friend_id;
                $data['notify'] = '0';
                $db->insert('usersfriend', $data);

                /// delete data from newfbuser_notify_tmp
                $db->delete('newfbuser_notify_tmp', array("friend_id = ?" => $friend_id, "user_id = ?" => $user_id));
            }
            $datau['deleted_user'] = $friend_id;
            $res = $db->update('activity', $datau, array("id = ?" => $activityId));
            $msg = ($status == '2') ? 'Ignored' : 'Allowed';
			$db->commit();

		}catch(Exception $e){
			//Rollback transaction
			$db->rollBack();
			$msg = $e->getMessage();
            $success = false;
		}
        return $this->_helper->json(array('success' => $success, 'msg' => $msg));
	}//function

	// add new group
	public function addgroupAction()
	{
		$db = Zend_Registry::get("db");
		$config = Zend_Registry::get("config");
		$server = Zend_Registry::get("server");

		$params = $this->getRequest()->getParams();
        $this->_initnewgroup();
		$groupName = $params['group_name'];
		$name = $db->quote($params['group_name']);

		$group_desc = $params['description'];

		try{
			$existGroup = groups::checkExistGroup($_SESSION['user_id'],$name);
			if($existGroup && empty($groupId))
			{
                return $this->_helper->json(array('success' => false, 'msg' => 'This group name is already added, please try with other name'));
            }else{
				$db->beginTransaction();
                $this->fields->validate();
				$Gdata['group_image'] = '';
				$Gdata['group_name'] = $groupName;
				$Gdata['description'] = $group_desc;
				$Gdata['created_by'] = $_SESSION['user_id'];
				$Gdata['created_date'] = date('Y-m-d');

				$db->insert('groups', $Gdata);
				$groupId = $db->lastInsertId();
				
				$Gdata1['user_id'] = $_SESSION['user_id'];
				$Gdata1['group_id'] = $groupId;

				$db->insert('friendsgroup', $Gdata1);
				/// add this user in this group

				$db->commit();
                $msg = "Group has been added successfully";
				$code = md5($groupId);
                return $this->_helper->json(array('success' => true, 'msg' => $msg, 'id' => $groupId, 'name' => $groupName, 'img' => $params['image'], 'description' => $group_desc, 'code' => $code));
			}

		}catch(Exception $e){
			//Rollback transaction
			$db->rollBack();
            return $this->_helper->json(array('success' => false, 'msg' => $e->getMessage()));
        }
	}

    public function friendslistAction(){
        $config = Zend_Registry::get("config");
        $server = Zend_Registry::get("server");
        $logger = Zend_Registry::get("logger");

        $this->_helper->layout()->disableLayout();
    }

    public function userlistAction(){
        $config = Zend_Registry::get("config");
        $server = Zend_Registry::get("server");
        $logger = Zend_Registry::get("logger");

        $this->_helper->layout()->disableLayout();
    }

    protected function _initnewgroup()
    {

        $groupName = new PS_Validate_NotEmpty($this->_getParam("group_name"));
        $groupName->setMessage(PS_Translate::_("Please select group's name"));

        $description = new PS_Validate_NotEmpty($this->_getParam("description"));
        $description->setMessage(PS_Translate::_("Please enter description"));

        $this->fields = new PS_Form_Fields(
            array(
                "group_name" => array(
                    $groupName,
                    "required" => true
                ),
                "description" => array(
                    $description,
                    "required" => true
                )
            )
        );
        $params = $this->getRequest()->getParams();
        $this->fields->setData($params);
        $this->view->fields = $this->fields;

    }///end function

    public function autocompletegetallfriendsAction(){
        $db = Zend_Registry::get("db");
        $config = Zend_Registry::get("config");
        $server = Zend_Registry::get("server");

        $term = $this->getRequest()->getParam("term");

        //disable layout for modals
        $this->_helper->layout()->disableLayout();
        $friends = usersfriend::getAllFriendsNamesForAutocomplete($_SESSION['user_id'], $term);

        return $this->_helper->json($friends);
    }

    public function autocompletegetallgroupsAction(){
        $term = $this->getRequest()->getParam("term");
        //disable layout for modals
        $this->_helper->layout()->disableLayout();
        $groups = usersfriend::getAllGroupsNamesForAutocomplete($_SESSION['user_id'], $term);

        return $this->_helper->json($groups);
    }

    public function showfacebookfriendsAction(){
        $this->_helper->layout()->disableLayout();
        $_SESSION['show_facebook_friends'] = true;
        session::write(session_id(), $_SESSION);
        return $this->_helper->json(array('success' => true));
    }

    public function twitterGet($url, $params, $twitteroauth){
        $friendlist = array();
        while ($params['cursor'] != 0) {
            $response = $twitteroauth->get($url, $params);
            $params['cursor'] = $response->next_cursor;
            $users = $response->users;

            foreach ($users as $user) {
                $list = array();
                $list['name'] = $user->name;
                $list['fbid'] = $user->id;
                $list['pic'] = str_replace('normal', 'bigger', $user->profile_image_url);

                $friendUserId = users::checkExistsTwitterID($user->id);
                if (!empty($friendUserId)) {
                    $list['insite'] = 'Y';
                    $list['id'] = $friendUserId;

                    $friendStatus = usersfriend::getRequestStatus($_SESSION['user_id'], $friendUserId);
                    if ($friendStatus == 1) {
                        $list['isFriend'] = 'Y';
                    }elseif($friendStatus == 0)
                    {
                        $list['isFriend'] = 'P';
                    }else{
                        $list['isFriend'] = 'N';
                    }
                } else {
                    $list['insite'] = 'N';
                    $list['id'] = 0;
                    $list['isFriend'] = 'N';
                }
                array_push($friendlist, $list);
            }
        }
        return $friendlist;

        /*
			foreach($response as $friends)
			{
				$list = array();
				$list['name'] = $friends['name'];
				$list['fbid'] = $friends['id'];
				$list['pic'] = $friends['profile_image_url'];

				$friendUsser_id = users::checkExistsTwitterID($friends['id']);
				if(!empty($friendUsser_id))
				{
						$list['insite'] = 'Y';
						$list['id'] = $friendUsser_id;

						$friend_status = usersfriend::getRequestStatus($_SESSION['user_id'], $friendUsser_id);
						if($friend_status == 1)
						{
							$list['isFriend'] = 'Y';
						}else{
							$list['isFriend'] = 'N';
						}
				}else{
						$list['insite'] = 'N';
						$list['isFriend'] = 'N';
						$list['id'] = 0;
				}
				$friendlist[] = $list;
			}*/
    }

	/**
	 * Sets group code in session
	 */
	public function setgroupdataAction() {
		try{
			$params = $this->getRequest()->getParams();
			$_SESSION['group_id'] = ($params['id'] == '0') ? null : $params['id'];
			session::write(session_id(), $_SESSION);
			return $this->_helper->json(array('success' => true));
		} catch (Exception $e) {
			return $this->_helper->json(array('success' => false, 'err' => $e->getMessage()));
		}
	}

}//class
