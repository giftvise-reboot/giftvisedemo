<?php

/**
 * IndexController
 *
 * @author
 * @version
 */

require_once 'Zend/Controller/Action.php';
class IndexController extends PS_Controller_Action {
    /*
     * Page Title
     */

    public function preDispatch() {
        $this->view->headTitle()->prepend(PS_Translate::_("Welcome"));
    }

    protected function _init() {
        $this->fields = new PS_Form_Fields(
            array(
                "email" => array(
                    new PS_Validate_EmailAddress(),
                    "filter" => new PS_Filter_RemoveSpaces(),
                    "required" => true
                ),
                "password" => array(
                    new PS_Validate_NotEmpty(),
                    "required" => true
                )
            )
        );
        $params = $this->getRequest()->getParams();
        $this->fields->setData($params);
        $this->view->fields = $this->fields;
    }

///end function

    /**
     * The default action - show the home page
     */
    public function indexAction() {

        $server = Zend_Registry::get("server");
        $logger = Zend_Registry::get("logger");
        $db = Zend_Registry::get("db");

        $logger->info("[START] " . __CLASS__ . ' : ' . __FUNCTION__);

        if(isset($_SESSION['user_id'])){
            $logger->info("--- ACCESS-INFO --- ::LANDING-PAGE::USER-ID::" .$_SESSION['user_id']. "DEVICE-INFO:: ".
                $_SERVER['HTTP_USER_AGENT'] . "IP ADDRESS ". $_SERVER['REMOTE_ADDR'] ."\n");
        }

        require 'fb/facebook.php';
        require 'amazonQuery.php';
        
        Facebook::$CURL_OPTS[CURLOPT_SSL_VERIFYPEER] = false;
        Facebook::$CURL_OPTS[CURLOPT_SSL_VERIFYHOST] = 2;

        try {
            if (isset($_SESSION['user_id']) && !empty($_SESSION['user_id']) && $_SESSION['user_id'] != '') {
                //user is logged
                $logger->debug("user_id = " . $_SESSION['user_id']);
                $user_details = users::getUserDetails($_SESSION['user_id']);
                $this->view->userDetail = $user_details;
                $this->_redirect(PS_Util::redirect($server->securehttp.$server->apphost."/login/home"));
            } else {
                $db->beginTransaction();
                $facebook = new Facebook(array(
                    'appId' => $server->FACEBOOK_APP_ID,
                    'secret' => $server->FACEBOOK_SECRET,
                ));
                $accessToken = $this->getRequest()->getParam("access_token");
                if(!empty($accessToken)){
                    $facebook->setAccessToken($accessToken);

                    $userfb = $facebook->getUser();
                    $FBparams = array(
                        'scope' => 'user_events, user_birthday, email'
                    );
                    if ($userfb) {
                        $logger->info("userfb is True");
                        $me = $facebook->api('/me');
                        if ($me) {
                            $logoutUrl = $server->securehttp . $server->apphost . "/login/logout";
                            $_SESSION['fb_logout'] = $logoutUrl;
                            $_SESSION['fb_url'] = $logoutUrl;
                            $_SESSION['login_type'] = 'FB';
                            $_SESSION['time_zone'] = (isset($_SESSION['time_zone'])) ? $_SESSION['time_zone'] : 'US/Eastern';
                            session::write(session_id(), $_SESSION);
                            $logger->info("me is True. fb_url = " . $_SESSION['fb_url']);
                            $logger->debug(__CLASS__.":".__FUNCTION__.":".__LINE__.":"."SESSION[logged_out] = ".$_SESSION['logged_out']);
                        } else {
                            $_SESSION['fb_logout'] = '';
                            $_SESSION['login_type'] = '';
                            $loginUrl = $facebook->getLoginUrl($FBparams);
                            $_SESSION['fb_url'] = $loginUrl;
                            $_SESSION['logged_out'] = 'true';
                            $logger->debug(__CLASS__.":".__FUNCTION__.":".__LINE__.":"."SESSION[logged_out] = ".$_SESSION['logged_out']);
                            unset($userfb);
                            $logger->info("me is False. fb_url = " . $_SESSION['fb_url']);
                        }
                    } else {
                        $loginUrl = $facebook->getLoginUrl($FBparams);
                        $_SESSION['fb_url'] = $loginUrl;
                        $_SESSION['logged_out'] = 'true';
                        $logger->debug(__CLASS__.":".__FUNCTION__.":".__LINE__.":"."SESSION[logged_out] = ".$_SESSION['logged_out']);
                        $logger->info("userfb is False. fb_url = " . $_SESSION['fb_url']);
                    }

                    if ($userfb) {

                        // Proceed knowing you have a logged in user who's authenticated.
                        $user_profilefb = $facebook->api('/' . $userfb . '?fields=name,email,first_name,middle_name,last_name,hometown,location,gender,picture.width(270).height(270),birthday');
                        $this->view->user_profile = $user_profilefb;

                        /////////////// DB operation /////
                        $data['first_name'] = empty($user_profilefb['first_name']) ? ' ' : $user_profilefb['first_name'];
                        $data['last_name'] = $user_profilefb['last_name'];
                        $data['device_type'] = 'web';
                        $data['email_id']= (empty($user_profilefb['email'])) ? '' : $user_profilefb['email'];

                        if (strtolower($user_profilefb['gender']) == 'male') {
                            $data['gender'] = 'M';
                        } else {
                            $data['gender'] = 'F';
                        }

                        $data['facebook_id'] = $user_profilefb['id'];
                        $data['birth_date'] = (empty($user_profilefb['birthday'])) ? '1970-01-01' : date('Y-m-d', strtotime($user_profilefb['birthday']));

                        if (!empty($user_profilefb['picture']['data']['url'])) {
                            $image_url = $user_profilefb['picture']['data']['url'];
                            $path_arr = explode("/", $image_url);

                            $lastItem = $path_arr[count($path_arr) - 1];

                            $image_name = md5($lastItem).$this->getImageTypeExt($user_profilefb['picture']['data']['url']);

                            $target_path = $_SERVER["DOCUMENT_ROOT"] . "/images/userpics/" . $image_name;
                            $thumb_path = $_SERVER["DOCUMENT_ROOT"] . "/images/userpics/thumb/" . $image_name;

                            $logger->debug("document_root = " . $_SERVER["DOCUMENT_ROOT"]);

                            $bigimage = $facebook->api('/me', array('fields' => 'picture.height(270).width(270)'));
                            $thumbImageFB = $facebook->api('/me', array('fields' => 'picture.height(160).width(160)'));

                            $bigimage_url = $bigimage['picture']['data']['url'];
                            $thumbImageFB_url = $thumbImageFB['picture']['data']['url'];

                            phpQuery::save_image($bigimage_url, $target_path);
                            phpQuery::save_image($thumbImageFB_url, $thumb_path);

                            $data['facebook_image'] = $user_profilefb['picture']['data']['url'];
                            $data['profile_pic'] = $image_name;
                            $this->view->userDetail->profile_pic = $image_name;
                        } else {
                            $data['profile_pic'] = 'default-profile-pic.png';
                        }
                        
                        $data['created_date'] = date('Y-m-d H:i:s');

                        $logger->info("before new sql query");
                        $fb_user = users::getFBUser($user_profilefb['id']);
                        $logger->info("after new sql query");
                        $user_id = $fb_user->user_id;
                        $role = $fb_user->role;
                        $logger->info("after new sql query");
                        $logger->info("User ID: ".$user_id." Role: ".$role);
                        $isFacebookRegister = true;
                        if (isset($user_id) AND !empty($user_id)) {
                            $data['current_login'] = date('Y-m-d H:i:s');
                            $data['last_login'] = users::getLastLogin($user_id);
                            $db->update('users', $data, 'user_id = ' . $user_id);
                            $isFacebookRegister = false;                            
                        } else {
                            //insert user
                            $data['udid']='';$data['twitter_name']='';$data['twitter_id']='';$data['twitter_image']='';$data['password']='';
                            $data['address']='';$data['fbID']='';$data['user_udid']='';$data['current_login']=$data['created_date'];$data['last_login']=$data['created_date'];
                            $data['modified_date']='1970-01-01 00:00:00';
                            $data['facebook_email']='';$data['wishlinkid']=0; $data['total_wishlist_items'] = 0; $data['blog_link'] = '';
                                //No need to set role while inserting a new user. New user default value 0-regular user
                            $role = 0;

                            $data['udid']= '';
                            try{
                                $db->insert('users', $data);
                            } catch (Exception $e){
                                var_dump($e->getMessage());exit;
                            }
                            $user_id = $db->lastInsertId();
                        }
                    }

                    /// create event for user's Birthday
                    if ($user_id != '' && $user_profilefb['birthday'] != '') {

                        event::addBirthdayEvents($user_id, $user_profilefb['birthday'], $user_profilefb['first_name'] . ' ' . $user_profilefb['last_name'], $user_profilefb['id']);

                    }/// event

                    $db->commit();
                    $_SESSION['user_id'] = $user_id;
                    $_SESSION['role'] = $role;
                    $_SESSION['user_fb_id'] = $data['facebook_id'];
                    $_SESSION['time_zone'] = (isset($_SESSION['time_zone'])) ? $_SESSION['time_zone'] : 'US/Eastern';
                    $logger->info("User (" . $user_id . ") role - ". $role);

                    ///EOF FB
                    $this->_helper->viewRenderer("index");
                    $this->_helper->resources("index");
                    $this->_init();
                    $this->errorOnNoscript();
                    $logger->debug(__CLASS__.":".__FUNCTION__.":".__LINE__.":"."user_id = " . $user_id." logged_in = ". $_SESSION['logged_in']);
                    if (!empty($user_id) && $_SESSION['logged_in'] != true) {
                        $logger->debug(__CLASS__.":".__FUNCTION__.":".__LINE__.":"."Redirecting to profile page. SESSION[logged_out] = ".$_SESSION['logged_out']);
                        $_SESSION['logged_out'] = false;
                        $_SESSION['logged_in'] = true;
                        session::write(session_id(), $_SESSION);
                        $facebookEventsErrorMessage = event::getFacebookEvents($data['facebook_id'], $accessToken);
                        $logger->debug($facebookEventsErrorMessage);
                        ($isFacebookRegister) ? 
                            $this->_redirect(PS_Util::redirect($server->securehttp.$server->apphost."/login/registersteptwo?id=".$user_id)) :
                            $this->_redirect(PS_Util::redirect($server->securehttp.$server->apphost."/login/home")) 
                        ;
                    } else {
                        session::write(session_id(), $_SESSION);
                    }
                }
            }
        }
        catch (Exception $e) {
            $logger->debug($e);
            $_SESSION['fb_logout'] = '';
            $_SESSION['login_type'] = '';
            $loginUrl = $facebook->getLoginUrl($FBparams);
            $_SESSION['fb_url'] = $loginUrl;
            $userfb = null;
            $logger->info("[END] " . __CLASS__ . ' : ' . __FUNCTION__);
            $this->_redirect(PS_Util::redirect($server->securehttp . $server->apphost));
            exit;
        }
        $this->view->fbAppID = $server->FACEBOOK_APP_ID;
      
        $this->_helper->layout()->disableLayout();
        $logger->info("[END] " . __CLASS__ . ' : ' . __FUNCTION__.':'.__LINE__);
    }

    private function getImageTypeExt($img){
        switch (exif_imagetype($img)){
            case IMAGETYPE_GIF:
                return '.gif';
                break;
            case IMAGETYPE_JPEG:
                return '.jpeg';
                break;
            case IMAGETYPE_PNG:
                return '.png';
                break;
            case IMAGETYPE_BMP:
                return '.bmp';
                break;
            case IMAGETYPE_GIF:
                return '.gif';
                break;
            case IMAGETYPE_TIFF_II:
                return '.tiff';
                break;
            case IMAGETYPE_TIFF_MM:
                return '.tiff';
                break;
        }

    }
}
