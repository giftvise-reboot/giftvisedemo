<?php
/*------------------------------------------------------------------ 
 * Copyright (c) 2013-2014 by Giftvise, Inc.
 * All rights reserved. 
 *------------------------------------------------------------------
 */

/*
All login operations will go here

created by Anu
*/

class WishlistController extends PS_Controller_Action {

    public function preDispatch() {
        $this->view->headTitle(PS_Translate::_("Login"));
    }

    public function settimezoneAction(){
        $this->_helper->layout()->disableLayout();
        $param_time_zone = $this->getRequest()->getParam("time_zone");
        $_SESSION['time_zone'] = $param_time_zone;
        session::write(session_id(), $_SESSION);
        return $this->_helper->json($param_time_zone);
    }

    public function indexAction() {
        ///BOF FBtwi
        //require 'openinviter/facebook.php';
        $server = Zend_Registry::get("server");
        $config = Zend_Registry::get("config");
        $logger = Zend_Registry::get("logger");
        $db = Zend_Registry::get("db");

        if ($_SESSION['logged_in'] != true) {
           $this->_redirect(PS_Util::redirect("https://" . $server->apphost));
           exit;
        }
        $logger->info("[START] " . __CLASS__ . ' : ' . __FUNCTION__);

        require 'fb/facebook.php';
        require 'amazonQuery.php';

        //echo "FB::".$server->FACEBOOK_SECRET;

        Facebook::$CURL_OPTS[CURLOPT_SSL_VERIFYPEER] = false;
        Facebook::$CURL_OPTS[CURLOPT_SSL_VERIFYHOST] = 2;

        /* $facebook = new Facebook(array(
          'appId'  => '409155555814120',
          'secret' => 'fb5600dcbf3913e4b3e8c66a50751e43',
          )); */

        try {
            $db->beginTransaction();
            $facebook = new Facebook(array(
                'appId' => $server->FACEBOOK_APP_ID,
                'secret' => $server->FACEBOOK_SECRET,
            ));

            $userfb = $facebook->getUser();


            //exit;
            $FBparams = array(
                'scope' => 'user_events, user_birthday'
            );
            if ($userfb) {
                $logger->info("userfb is True");
                $token = $facebook->getAccessToken();
                $me = $facebook->api('/me');

                //exit;
                if ($me) {

                    //$logoutUrl = $facebook->getLogoutUrl(array( 'next' => ("http://".$server->apphost."/login/logout") ));
                    $logoutUrl = "https://" . $server->apphost . "/login/logout";
                    $_SESSION['fb_logout'] = $logoutUrl;
                    $_SESSION['fb_url'] = $logoutUrl;
                    $_SESSION['login_type'] = 'FB';
                    session::write(session_id(), $_SESSION);
                    $logger->info("me is True. fb_url = " . $_SESSION['fb_url']);
                } else {
                    $_SESSION['fb_logout'] = '';
                    $_SESSION['login_type'] = '';
                    $loginUrl = $facebook->getLoginUrl($FBparams);
                    $_SESSION['fb_url'] = $loginUrl;
                    unset($userfb);
                    $logger->info("me is False. fb_url = " . $_SESSION['fb_url']);
                }
            } else {
                $loginUrl = $facebook->getLoginUrl($FBparams);
                $_SESSION['fb_url'] = $loginUrl;
                $logger->info("userfb is False. fb_url = " . $_SESSION['fb_url']);
            }

            if ($userfb) {

                // Proceed knowing you have a logged in user who's authenticated.
                $user_profilefb = $facebook->api('/' . $userfb . '?fields=name,email,first_name,middle_name,last_name,hometown,location,gender,picture,birthday');
                $this->view->user_profile = $user_profilefb;
                $user_profile1 = $facebook->api($userfb);

                /////////////// DB operation /////			
                $data['first_name'] = $user_profilefb['first_name'];
                $data['last_name'] = $user_profilefb['last_name'];
                $data['device_type'] = 'web';

                if (strtolower($user_profilefb['gender']) == 'male') {
                    $data['gender'] = 'M';
                } else {
                    $data['gender'] = 'F';
                }

                $data['facebook_id'] = $user_profilefb['id'];
                if (!empty($user_profilefb['birthday'])) {
                    $data['birth_date'] = date('Y-m-d', strtotime($user_profilefb['birthday']));
                }
                if (!empty($user_profilefb['picture']['data']['url'])) {
                    ///
                    $image_url = $user_profilefb['picture']['data']['url'];
                    $path_arr = explode("/", $image_url);

                    $image_name = $path_arr[count($path_arr) - 1];

                    $target_path = $_SERVER["DOCUMENT_ROOT"] . "/userpics/" . $image_name;
                    $thumb_path = $_SERVER["DOCUMENT_ROOT"] . "/userpics/thumb/" . $image_name;

                    $bigimage = $facebook->api('/me', array('fields' => 'picture.height(150).width(150)'));
                    $thumbImageFB = $facebook->api('/me', array('fields' => 'picture.height(50).width(50)'));

                    $bigimage_url = $bigimage['picture']['data']['url'];
                    $thumbImageFB_url = $thumbImageFB['picture']['data']['url'];

                    //phpQuery::save_image($image_url,$target_path);
                    phpQuery::save_image($bigimage_url, $target_path);
                    phpQuery::save_image($thumbImageFB_url, $thumb_path);
                    //phpQuery::save_image($image_url,$thumb_path);
                    ///

                    $data['facebook_image'] = $user_profilefb['picture']['data']['url'];
                    $data['profile_pic'] = $image_name;
                }
                $data['created_date'] = date('Y-m-d H:i:s');

                $user_id = users::checkExistsFBid($user_profilefb['id']);


                if (isset($user_id) AND !empty($user_id)) {
                    $db->update('users', $data, 'user_id = ' . $user_id);
                } else {
                    //insert user
                    $db->insert('users', $data);
                    $user_id = $db->lastInsertId();

                    // get friend of this new user
                    $fb_friend = $facebook->api('/me/friends?fields=id');
                    $friendFBid = array();
                    for ($f = 0; $f < count($fb_friend['data']); $f++) {
                        $friedFBid = $fb_friend['data'][$f]['id'];
                        $friends_userID = users::checkExistsFBid($friedFBid);

                        if ($friends_userID != '') {
                            // insert record for notification
                            $notifyData = array();
                            $notifyData['user_id'] = $user_id;
                            $notifyData['friend_id'] = $friends_userID;
                            $db->insert('newfbuser_notify_tmp', $notifyData);

                            $actity_data['sender'] = $friends_userID;
                            $actity_data['receiver'] = $user_id;
                            $actity_data['activity_type'] = 'Ask Permission';
                            $actity_data['activity_date'] = date('Y-m-d H:i:s');
                            $actity_data['item_id'] = '0';
                            $actity_data['event_id'] = '0';
                            $actity_data['deleted_user'] = '0';
                            $actity_data['new'] = true;
                            $db->insert('activity', $actity_data);
                        }
                    }

                    //exit;				
                }

                /// create event for user's Birthday
                if ($user_id != '' && $user_profilefb['birthday'] != '') {

                    $event_datetmp = date('Y-m-d', strtotime($user_profilefb['birthday']));
                    $edtArr = explode('-', $event_datetmp);
                    $event_date = date('Y') . '-' . $edtArr[1] . '-' . $edtArr[2];

                    $event_name = $user_profilefb['first_name'] . ' ' . $user_profilefb['last_name'] . ' - Birthday';
                    $datetime = date("Y-m-d H:i:s");
                    $event = array(
                        "created_user" => $user_id,
                        "fb_uid" => $user_profilefb['id'],
                        "event_date" => $event_date,
                        "title" => $event_name,
                        "event_type" => "Birthday",
                        "created_date" => $datetime,
                    );

                    $update_event = array(
                        "event_date" => $event_date,
                        "title" => $event_name,
                        "event_type" => "Birthday",
                        "updated_date" => $datetime,
                    );

                    $event_id = event::isExistFBBirthdayEvent($user_profilefb['id'], $user_id);

                    if ($event_id) {
                        //update event
                        $db->update('events', $update_event, 'id = ' . $event_id);
                    } else {
                        // create event
                        $db->insert('events', $event);
                    }

                    /// automatically invite friends in user's birthday event
                    //get friends
                    $friendlist = usersfriend::getFollowList($user_id);
                    //invite friend
                    for ($f = 0; $f < count($friendlist); $f++) {
                        // check for already invited
                        $chkInvite = event::isCheckFriendEventInviteOpt($friendlist[$f]->friend_id, $event_id);
                        if (!$chkInvite) {
                            $invite_friend = array("user_id" => $user_id, "invited_user" => $friendlist[$f]->friend_id, "event_id" => $event_id, "RSVP_status" => '3', 'notification_status' => 1);
                            $db->insert('eventrsvp', $invite_friend);
                        }
                    }
                }/// event
            }

            $db->commit();
            $_SESSION['user_id'] = $user_id;

            session::write(session_id(), $_SESSION);



            ///EOF FB			
            $this->_helper->viewRenderer("index");
            $this->_helper->resources("index");
            $this->_init();
            $this->errorOnNoscript();


            if ($_SESSION['fb_logout'] != '') {
                //$this->_redirect(PS_Util::redirect("http://".$server->apphost."/login/home"));
                ?>
                <script type="text/javascript">
                        window.parent.location.href = '<?php echo "https://" . $server->apphost . "/login/home"; ?>';
                </script>
                <?php
            }
        }//try
        //catch (FacebookApiException $e) 
        catch (Exception $e) {
            //error_log($e);			
            $_SESSION['fb_logout'] = '';
            $_SESSION['login_type'] = '';
            $loginUrl = $facebook->getLoginUrl($FBparams);
            $_SESSION['fb_url'] = $loginUrl;
            $userfb = null;
            $logger->info("[END] " . __CLASS__ . ' : ' . __FUNCTION__);
            /* echo '<pre>';
              print_r($e);
              echo '</pre>';
              exit; */
            $this->_redirect(PS_Util::redirect("https://" . $server->apphost . "/login/"));
            exit;
        }

        $logger->info("[END] " . __CLASS__ . ' : ' . __FUNCTION__);
    }

//function
    /// Login validation
/*
    protected function _init() {
        $logger = Zend_Registry::get("logger");
        
        $params['email'] = $this->getRequest()->getPost('email', null);
        $params['password'] = $this->getRequest()->getPost('password', null);
        
        //$password = new PS_Validate_NotEmpty($this->_getParam("password"));
        $password = new PS_Validate_NotEmpty($params["password"]);
        $password->setMessage(PS_Translate::_("Please enter password"));

        $logger->info("Username = " . $params['email']. " password = ". $params['password']);
        $this->fields = new PS_Form_Fields(
                array(
            "email" => array(
                new PS_Validate_EmailAddress(),
                "filter" => new PS_Filter_RemoveSpaces(),
                "required" => true
            ),
            "password" => array(
                $password,
                "required" => true
            )
                )
        );
        //$params = $this->getRequest()->getParams();
        $this->fields->setData($params);
        $this->view->fields = $this->fields;
    }
*/
///end function

    /*
     * New Login action
     */
    public function newloginAction() {

        $db = Zend_Registry::get("db");
        $config = Zend_Registry::get("config");
        $server = Zend_Registry::get("server");
        $logger = Zend_Registry::get("logger");

        $this->_init();
        $this->errorOnNoscript();
        $params = $this->getRequest()->getParams();
        
        /*
        $params['email'] = $this->getRequest()->getPost('email', null);
        $params['password'] = $this->getRequest()->getPost('password', null);
        $params['flow_proc'] = $this->getRequest()->getPost('flow_proc', null);
        $params['remember'] = $this->getRequest()->getPost('remember', '0');
        */
        $logger->info("Username = " . $params['email']. " password = ". $params['password']);
        $logger->info("flow_proc = ". $params['flow_proc']. " remember = ". $params['remember']);
        
        if ($params['flow_proc'] != '' && ($_COOKIE['gift_username'] == $params["email"]) && ($params['flow_proc'] == $params['password'])) {
            $password = base64_decode($params['flow_proc']);
        } else {
            $password = $params["password"];
        }
        try {
            $db->beginTransaction();
            $this->fields->validate();

            $authAdapter = new Zend_Auth_Adapter_DbTable($db->getSlaveConnection(), "users", "email_id", "password", "MD5(?)");

            //Set identity
            $authAdapter->setIdentity($params["email"])->setCredential($password);

            //Authenticate
            $result = $authAdapter->authenticate();

            switch ($result->getCode()) {
                case Zend_Auth_Result::FAILURE_IDENTITY_NOT_FOUND:
                case Zend_Auth_Result::FAILURE_CREDENTIAL_INVALID:

                default:
                    $this->view->loginError = 'Email / Password combination not registered';
                    $err = 'Email / Password combination not registered';
                    $url = "https://" . $server->apphost . "/?logerr=Invalid Details";
                    // PS_Util::redirect($url);
                    throw new Exception(PS_Translate::_("Invalid Details"));
                case Zend_Auth_Result::SUCCESS:
                    $result = $authAdapter->getResultRowObject();
                    $userID = $result->user_id;

                    if ($result->status == 1) {
                        $this->view->loginError = 'User is not activated';
                        $err = 'Invalid Details';
                        throw new Exception(PS_Translate::_("User is not activated"));
                    } else {

                        $_SESSION['user_id'] = $userID;
                        $_SESSION['logged_in'] = 'true';
                        $_SESSION['logout_url'] = "https://" . $server->apphost . "/login/logout";
                        $_SESSION['login_type'] = 'Giftvise';
                        session::write(session_id(), $_SESSION);
                        $db->commit();

                        // set remember me
                        // TODO: doing a base64 encoding of password and storing it in a cookie
                        // is really bad!!!!! Fix this before release!!
                        if (isset($params["remember"]) && $params["remember"] == 'on') {
                            setcookie('gift_username', $params["email"], time() + (60 * 60 * 24 * 7));
                            setcookie('gift_password', base64_encode($password), time() + (60 * 60 * 24 * 7));
                            $logger->info("Saved credentials in cookie");
                            
                            $logger->info("cookie['gift_username'] = " . $_COOKIE['gift_username']);
                            $logger->info("cookie['gift_password'] = " . $_COOKIE['gift_password']);
                        } else {
                            //setcookie ('gift_username',0, time()-1000);
                            //setcookie ('gift_username', 0, time()-1000); 
                            $logger->info("Did not save cookie");
                        }
                        return $this->_helper->json(array('success' => true));
                    }
            }
        } catch (Exception $e) {
            $msg = $e->getMessage();
            $logger->info("Login error = " . $msg);
            return $this->_helper->json(array('success' => false, 'msg' => $msg));
        }
    }
        
    /// login process
    public function loginAction() {

        $db = Zend_Registry::get("db");
        $config = Zend_Registry::get("config");
        $server = Zend_Registry::get("server");
        $logger = Zend_Registry::get("logger");

        $this->_init();
        $this->errorOnNoscript();
        $params = $this->getRequest()->getParams();

        if ($params['flow_proc'] != '' && ($_COOKIE['gift_username'] == $params["email"]) && ($params['flow_proc'] == $params['password'])) {
            $password = base64_decode($params['flow_proc']);
        } else {
            $password = $params["password"];
        }

        //echo 'pass:'.$password;
        //exit;
        try {
            $db->beginTransaction();
            $this->fields->validate();

            $authAdapter = new Zend_Auth_Adapter_DbTable($db->getSlaveConnection(), "users", "email_id", "password", "MD5(?)");

            //Set identity
            $authAdapter->setIdentity($params["email"])->setCredential($password);

            //Authenticate
            $result = $authAdapter->authenticate();

            switch ($result->getCode()) {
                case Zend_Auth_Result::FAILURE_IDENTITY_NOT_FOUND:
                case Zend_Auth_Result::FAILURE_CREDENTIAL_INVALID:

                default:
                    $this->view->loginError = 'Email / Password combination not registered';
                    $err = 'Invalid Details';
                    $url = "https://" . $server->apphost . "/?logerr=Invalid Details";
                    // PS_Util::redirect($url);
                    throw new Exception(PS_Translate::_("Invalid Details"));
                case Zend_Auth_Result::SUCCESS:
                    $result = $authAdapter->getResultRowObject();
                    $userID = $result->user_id;

                    if ($result->status == 1) {
                        $this->view->loginError = 'User is not activated';
                        $err = 'Invalid Details';
                        throw new Exception(PS_Translate::_("User is not activated"));
                    } else {

                        $_SESSION['user_id'] = $userID;
                        session::write(session_id(), $_SESSION);
                        $db->commit();

                        // set remember me

                        if (isset($params["remember"]) && $params["remember"] == '1') {
                            setcookie('gift_username', $params["email"], time() + (60 * 60 * 24 * 7));
                            setcookie('gift_password', base64_encode($password), time() + (60 * 60 * 24 * 7));
                        } else {
                            //setcookie ('gift_username',0, time()-1000);
                            //setcookie ('gift_username', 0, time()-1000); 
                        }
                        //
                        //$this->_redirect(PS_Util::redirect("http://".$server->apphost."/login/home"));
                        ?>	
                            <script type="text/javascript">
                                window.parent.location.href = '<?php echo "https://" . $server->apphost . "/login/newhome"; ?>';
                            </script>
                        <?php
                    }
                    exit;
            }
        } catch (Exception $e) {
            //Rollback transaction			   
            $this->view->errors[] = $e->getMessage();
            $this->_helper->viewRenderer("index");
            $this->_helper->resources("index");
            $err = $this->view->errors[0];
        }

        if ($err != '') {
            $this->_helper->viewRenderer("index");
            $this->_helper->resources("index");
        } else {
            $this->_helper->viewRenderer("login");
            $this->_helper->resources("login");
        }
        //exit;
    }

//end function
    /// after login this page will be displayed
    public function mywishlistAction() {
        $db = Zend_Registry::get("db");
        $config = Zend_Registry::get("config");
        $server = Zend_Registry::get("server");
        $logger = Zend_Registry::get("logger");

        $logger->info("Chike 123 --- ACCESS-INFO --- ::HOME-PAGE::USER-ID::" . $_SESSION['user_id']. "::DEVICE-INFO::". 
                      $_SERVER['HTTP_USER_AGENT'] . "IP ADDRESS/ " . $_SERVER['REMOTE_ADDR'] ."\n"); 

        $logger->info('user_id: ' . $_SESSION['user_id']. ' role: '. $_SESSION['role']);

        $accessToken = $this->getRequest()->getParam("access_token");
        if ($_SESSION['logged_in'] != true) {
           $this->_redirect(PS_Util::redirect("https://" . $server->apphost."/?access_token=".$accessToken));
           exit;
        }
        
        $ret = stripos($_SERVER['HTTP_USER_AGENT'], 'phone');
        if (!ret == false) {
            $this->view->need_tutorial = 1;
        } else {
            $this->view->need_tutorial = 0;
        }

        $this->view->nextload = 0;
        $this->view->headTitle(PS_Translate::_("Home"));

        $this->view->month = date('n', time());
        $this->view->year = date('Y', time());
        $this->view->date = date('j', time());

        $user_details = users::getUserDetails($_SESSION['user_id']);
        $this->view->userDetail = $user_details;
       
        /* Update user wishlist link id */
        $wishlinkid = $user_details->first_name . $user_details->last_name . $user_details->user_id;
        users::updateUserWishlinkid($user_details->user_id, $wishlinkid);
 
        ///categories
        $categories = category::categoryList();
        $this->view->categories = $categories;

        //get products
        $param = $this->getRequest()->getParams();

        $ord = $param['order'];
        $catVal = $param['item_type'];

        $orderBy = '';
        if ($param['pricekey'] == 'price') {
            if ($orderBy != '')
                $orderBy .= ', item_price ASC';
            else
                $orderBy = ' item_price ASC';
        }

        if ($param['datekey'] == 'date') {
            if ($orderBy != '') {
                $orderBy .= ', uw.added_date DESC';
            } else {
                $orderBy = 'uw.added_date DESC';
            }
        }

        try {
            // number of items displayed at a time, need to have single variable
            // or common place to drive this
            $numberOfItems = 100; 
            $this->view->next_start = $numberOfItems;

            $products = wishlistitmes::getLoadMoreHomePage($_SESSION['user_id'], $orderBy, $catVali, 0, $numberOfItems);
            $more_items = (count($products) >= $numberOfItems) ? 1 : 0;
            $this->view->more_items = $more_items;

            if ($catVal != '') {
                $catName = category::categoryName($catVal);
            } else {
                $catName = 'Category';
            }
       $noids = true;
       $productIds = array();
       for ($i = 0; $i < count($products); $i++) {
           $productIds[] = $products[$i]->id;
           $noids = false;
       }
       if ($noids == false) {
           $this->view->filtertags = wishlistitmes::getUsersTagList($_SESSION['user_id'], "('" . implode("','",$productIds) . "')");
       }
       $this->view->items = $products;
       $this->view->extval = wishlistitmes::prepareExtVal($products, $_SESSION['user_id']);
       $this->view->categoryName = $catName;
       $this->view->vpage = 2;
      
        $_SESSION['time_zone'] = (isset($_SESSION['time_zone'])) ? $_SESSION['time_zone'] : 'America/New_York';
        try {
               $timeZone = new DateTimeZone($_SESSION['time_zone']);
        } catch (Exception $e) {
        }
            $this->view->events = event::getmonthevents($_SESSION["user_id"], $this->view->month, $this->view->year, '');
            $this->view->upcoming_events = event::getUpcomingMonthEventsCalendarDates($_SESSION["user_id"], $this->view->month, $this->view->year, $timeZone);
            /* User created events for filter bar */
            $this->view->filter_bar_events = event::getUserCreatedEvents($_SESSION["user_id"]);

            /// followos
            $followCount = usersfriend::countFollowList($_SESSION['user_id']);
            $this->view->follow = $followCount;

            /// follows
            $followerCount = usersfriend::countFollowerList($_SESSION['user_id']);
            $this->view->followers = $followerCount;

            // total gift send by user
            $this->view->giftsend = event::giftSend($_SESSION['user_id']);

            // total gift recived by user
            $this->view->giftget = event::giftGet($_SESSION['user_id']);

            /// Display tags
            $listTag = category::showTag($_SESSION['user_id']);

            //$userTag = category::getUserTags($_SESSION['user_id']);

            $this->view->taglist = $listTag;

            /// budget
            $totalExpense = budget::totalExpense($_SESSION['user_id']);
            $yearlyExpense = budget::totalExpenseYearly($_SESSION['user_id'], date('Y'));

            if ($totalExpense->total != '') {
                $this->view->totalExpense = $totalExpense->total;
            } else {
                $this->view->totalExpense = 0;
            }

            if ($yearlyExpense->total != '')
                $this->view->yearlyExpense = $yearlyExpense->total;
            else
                $this->view->yearlyExpense = 0;

            //user's created groups
            $groups = groups::groupList($_SESSION['user_id']);

            $this->view->usersgroups = $groups;

            /// Get following/friend list
            $followingList = usersfriend::getFollowList($_SESSION['user_id']);
            for ($i = 0; $i < count($followingList); $i++) {
                $flist = array();
                $follower_details = users::getUserDetails($followingList[$i]->friend_id);
                $flist['user_id'] = $follower_details->user_id;
                $flist['username'] = $follower_details->first_name . ' ' . $follower_details->last_name;
                $flist['profile_pic'] = $follower_details->profile_pic;
                $followinglst[] = $flist;
            }

            $this->view->followingList = $followinglst;
            $event_notification = event::getEventNotification($_SESSION["user_id"]);


            if (count($event_notification) > 0) {
                $this->view->e_notification = count($event_notification);
            }

            // notifications
            $this->view->newNotifications = notifications::countnewnotifications();
            $this->view->activityLog = notifications::activitylog(true);
            $this->view->notificationsCount = notifications::notificationscount();
            $this->view->fbAppID = $server->FACEBOOK_APP_ID;
            /* Send email notifications to fb friends who are already in giftvise */
            if ($user_details->tutorial == 0) {
                $youknow  = usersfriend::prepareFacebookFriendsyouknow($accessToken);
                if (count($youknow)) {
                     $logger->info("number of FB friend in giftvise  ". count($youknow));
                     for ($i = 0; $i < count($youknow); $i++) {
                           // $logger->info("FB friend-s email ". $youknow[$i]['facebook_email']); 
                          if ($youknow[$i]['facebook_email'] != '') {
                              $this->send_fb_gv_friend_invitation($youknow[$i]['facebook_email'], $youknow[$i]['first_name']);
                          }
                     }
                }
            }
      } catch (Exception $e) {
            print_r($e->getMessage());
            exit;
      }
    }

    //function
    /// after login this page will be displayed
    public function homeAction() {
        /*if ($_SESSION['webapp_style'] == 'new_design') {
            PS_Util::redirect("http://" . $server->apphost . "/login/newhome");
        }*/

        $this->view->headTitle(PS_Translate::_("Home"));
        $user_details = users::getUserDetails($_SESSION['user_id']);
        $this->view->userDetail = $user_details;
        $server = Zend_Registry::get("server");
        $db = Zend_Registry::get("db");
        $logger = Zend_Registry::get("logger");
        
        $logger->debug("[START] " . __CLASS__ . ":" . __FUNCTION__);
        
        //print_r($_SESSION);
        //exit;
        ///categories
        $categories = category::categoryList();
        $this->view->categories = $categories;

        //get products	
        $param = $this->getRequest()->getParams();

        $ord = $param['order'];
        $catVal = $param['item_type'];



        $orderBy = '';
        if ($param['pricekey'] == 'price') {
            if ($orderBy != '')
                $orderBy .= ', item_price ASC';
            else
                $orderBy = ' item_price ASC';
        }

        if ($param['datekey'] == 'date') {
            if ($orderBy != '')
                $orderBy .= ', uw.added_date DESC';
            else
                $orderBy = 'uw.added_date DESC';
        }
        try {
            $products = wishlistitmes::getAllProducts($_SESSION['user_id'], $orderBy, $catVal);

            if ($catVal != '') {
                $catName = category::categoryName($catVal);
            } else {
                $catName = 'Category';
            }

            for ($i = 0; $i < count($products); $i++) {
                $item = array();

                $totalComments = itemcomments::countComments($products[$i]->item_id);
                $userwished = wishlistitmes::countWished($products[$i]->item_id);

                /// latest item
                $timeAdded = wishlistitmes::latestItem($products[$i]->item_id);
                $addeduser = users::getUserDetails($timeAdded['user_id']);

                /// check item is received or not
                $ispurchased = event::isReceiveGiftForUser($_SESSION['user_id'], $products[$i]->item_id);
                $quantity = wishlistitmes::getQty($products[$i]->item_id, $_SESSION['user_id']);

                //if($ispurchased && $quantity == 0)
                if ($ispurchased) {
                    if ($quantity == 0) {
                        $item['isuserpurchased'] = 'Y';
                    } else {
                        $item['isuserpurchased'] = 'N';
                    }
                } else {
                    $sql = "select  user_id  from giftsexchanged  where item_id='" . $products[$i]->item_id . "' and receiver_id='" . $_SESSION["user_id"] . "' and status='0'  order by id desc";
                    $giftdetail = $db->fetchRow($sql);
                    if ($giftdetail->user_id != '') {
                        $item['isuserpurchased'] = 'N';
                    } else {
                        if ($quantity == 0)
                            $item['isuserpurchased'] = 'Y';
                        else
                            $item['isuserpurchased'] = 'N';
                    }
                    /* if($quantity == 0)
                      $item['isuserpurchased']='Y';
                      else
                      $item['isuserpurchased']='N'; */
                }
                $item['item_id'] = $products[$i]->item_id;
                ///
                $item['comment'] = $totalComments;
                $item['wished'] = $userwished;
                $item['added_time'] = $timeAdded['time'];
                $item['addedby'] = $addeduser->first_name . ' ' . $addeduser->last_name;
                $items[] = $item;
            }


            if ($param['allevent'] != "") {
                $event_limit = '';
            } else {
                //$event_limit="LIMIT 3";
                $event_limit = "";
            }

            // set event show limit
            if ($param["data"] != "") {
                $eventdate = date('Y-m-d', strtotime($param["data"]));
                //$eventdate=date('Y-m-d',strtotime(03/27/2013));
                $eventList = event::gettodayevents($_SESSION["user_id"], $eventdate, $event_limit);
                if (count($eventList) > 0) {
                    echo "<ul>";
                    foreach ($eventList as $event) {
                        if (strlen($event->title) > 24) {
                            $title = substr(ucfirst($event->title), 0, 24) . '...';
                        } else {
                            $title = $event->title;
                        }
                        if ($_SESSION['user_id'] == $event->created_user_id) {
                            echo "<li class='noarrow'>";
                        } else {
                            echo "<li>";
                        }

                        if ($_SESSION['user_id'] == $event->created_user_id) {
                            echo "<a href='" . $this->server->apphost . "/event/myeventdescription/event_id/" . $event->event_id . "' class='iframe noarrow'>";
                        } else {
                            echo "<a href='" . $this->server->apphost . "/event/inviteeventdescription/event_id/" . $event->event_id . "' class='iframe'>";
                        }
                        echo "<span class='date'>" . date('d', strtotime($event->event_date)) . '<br>' . date('M', strtotime($event->event_date)) . "</span>
					<span class='lefttext'>" . $title . "
					<br>
					<span class='lightgray'>" . $event->location . "</span></span>
					</a>";
                        if ($_SESSION['user_id'] != $event->created_user_id) {
                            echo "<ul class='sub-level'>
						 <div class='eventsubbox'>";
                            if ($event->profile_pic == "") {
                                echo "<span class='imgbox'><img src='" . $this->server->apphost . "/images/icon.jpg' alt=''></span>";
                            } else {
                                echo "<span class='imgbox'><img src='" . $this->server->apphost . "/userpics/thumb/" . $event->profile_pic . "' alt=''></span>";
                            }
                            echo "<div class='eventsubboxtext'><span class='font14'>" . ucfirst($event->title) . "</span><br>
						 " . date('dS M Y', strtotime($event->event_date)) . ' at ' . date('h:i a', strtotime($event->event_time)) . ', Location: ' . $event->location . "
						<br />
						<form action='https://" . $this->server->apphost . "/event/inviteeventdescription' method='post'>";
                            //<span class='font12'>".$event->first_name.' '.$event->last_name."</span>";
                            //echo " invited you ";
                            //  if($event->RSVP_status =='' OR $event->RSVP_status=='3'){
                            if ($event->RSVP_status == '0')
                                $decline = '<b style="color:#000000;">Decline</b>';
                            else
                                $decline = 'Decline';

                            if ($event->RSVP_status == '1')
                                $Attend = '<b style="color:#000000;">Attend</b>';
                            else
                                $Attend = 'Attend';

                            if ($event->RSVP_status == '2')
                                $Maybe = '<b style="color:#000000;">Maybe</b>';
                            else
                                $Maybe = 'Maybe';

                            echo "<span id='eventrsvp_" . $event->event_id . "'>
						<a href='' onClick='return eventrsvp(1," . $event->event_id . ")'>" . $Attend . "</a> |
						<a href='' onClick='return eventrsvp(2," . $event->event_id . ")'>" . $Maybe . "</a> |
						<a href='' onClick='return eventrsvp(0," . $event->event_id . ")'>" . $decline . "</a></span>";

                            /* }
                              else
                              {
                              echo "<br />";
                              if($event->RSVP_status=='0'){
                              echo "<p>You have <span style='color:#04aeee;float:none;'>declined</span> this event</p>";
                              }
                              if($event->RSVP_status=='1'){
                              echo "<p>You are <span style='color:#04aeee;float:none;'>going</span> to attend this event</p>";
                              }
                              if($event->RSVP_status=='2'){
                              echo "<p><span style='color:#04aeee;float:none;'>Maybe</span> you are going to attend this event</p>";
                              }
                              }
                             */

                            echo "</form></div>";
                            echo
                            "<input name='comment' type='text' value='write a comment' id='comment_" . $event->event_id . "' class='textfield' onKeyPress='return submitenter(this,event," . $event->event_id . ")' onBlur=\"javaScript: if (this.value=='') {this.value='write a comment';}\" onClick=\"javaScript: if (this.value=='write a comment') {this.value='';}\">
					<input type='hidden' name='event_id' value='" . $event->event_id . "'/>
					</form>
					</div>
					</ul>";
                        }
                        echo "</li>";
                    }
                    echo "</ul>";
                    exit;
                }
                else {
                    echo "<ul><li class='noarrow'>No events for this date</li></ul>";
                    exit;
                }
            } else {
                if ($param["editdt"] != '') {
                    $eventdate = $param["editdt"];
                } else {
                    $eventdate = '';
                }
                //$eventList=event::gettodayevents($_SESSION["user_id"],$eventdate,$event_limit);
                $eventList = event::gettodayevents($_SESSION["user_id"], $eventdate, $event_limit);
                $this->view->events = $eventList;
            }

            $event_dates = event::getEventDate($_SESSION["user_id"]);
            $this->view->upcoming_event = $event_dates;

            $this->view->items = $products;
            $this->view->extval = $items;
            $this->view->categoryName = $catName;

            /// followos
            $followCount = usersfriend::countFollowList($_SESSION['user_id']);
            $this->view->follow = $followCount;

            /// follows
            $followerCount = usersfriend::countFollowerList($_SESSION['user_id']);
            $this->view->followers = $followerCount;

            // total gift send by user
            $giftSend = event::giftSend($_SESSION['user_id']);
            $this->view->giftsend = $giftSend;

            // total gift recived by user
            $giftRecieved = event::giftGet($_SESSION['user_id']);
            $this->view->giftget = $giftRecieved;

            /// Display tags
            $listTag = category::showTag($_SESSION['user_id']);

            //$userTag = category::getUserTags($_SESSION['user_id']);

            $this->view->taglist = $listTag;

            /// budget		
            $totalExpense = budget::totalExpense($_SESSION['user_id']);
            $yearlyExpense = budget::totalExpenseYearly($_SESSION['user_id'], date('Y'));

            if ($totalExpense->total != '')
                $this->view->totalExpense = $totalExpense->total;
            else
                $this->view->totalExpense = 0;

            if ($yearlyExpense->total != '')
                $this->view->yearlyExpense = $yearlyExpense->total;
            else
                $this->view->yearlyExpense = 0;

            // user's activity
            $activity = users::getActivity($_SESSION['user_id']);

            $activityLog = array();
            for ($i = 0; $i < count($activity); $i++) {
                $listA = array();

                $sender = users::getUserDetails($activity[$i]->sender);
                $event_title = event::getEventTitle($activity[$i]->event_id);
                $receiver = users::getUserDetails($activity[$i]->receiver);

                $time = users::getTime(strtotime($activity[$i]->activity_date));

                $item_name = wishlistitmes::getProductName($activity[$i]->item_id);

                if ($activity[$i]->activity_type == 'Follow') {
                    /// check status it confirm or not
                    $isfollower = usersfriend::checkExistFriends($activity[$i]->sender, $activity[$i]->receiver);
                    if ($isfollower->status == 0) {
                        $listA['fstatus'] = 'P';
                    } else {
                        $listA['fstatus'] = '';
                    }
                } else {
                    $listA['fstatus'] = '';
                }

                $listA['id'] = $activity[$i]->id;
                $listA['item_id'] = $activity[$i]->item_id;
                $listA['item_name'] = $item_name;
                $listA['event_title'] = $event_title;
                $listA['sender_id'] = $activity[$i]->sender;
                $listA['sender_name'] = $sender->first_name . ' ' . $sender->last_name;
                $listA['sender_image'] = $sender->profile_pic;

                $listA['receiver_id'] = $activity[$i]->receiver;
                $listA['receiver_name'] = $receiver->first_name . ' ' . $receiver->last_name;
                $listA['receiver_image'] = $receiver->profile_pic;


                $listA['activity_type'] = $activity[$i]->activity_type;
                $listA['activity_time'] = $time;

                $activityLog[] = $listA;
            }
            $this->view->activityLog = $activityLog;



            /// All activity
            $allActivity = users::getAllActivity($_SESSION['user_id']);


            for ($i = 0; $i < count($allActivity); $i++) {
                $listA = array();

                $sender = users::getUserDetails($allActivity[$i]->sender);
                $event_title = event::getEventTitle($allActivity[$i]->event_id);
                $receiver = users::getUserDetails($allActivity[$i]->receiver);

                $time = users::getTime(strtotime($allActivity[$i]->activity_date));

                $item_name = wishlistitmes::getProductName($allActivity[$i]->item_id);

                if ($allActivity[$i]->activity_type == 'Follow') {
                    /// check status it confirm or not
                    $isfollower = usersfriend::checkExistFriends($allActivity[$i]->sender, $allActivity[$i]->receiver);
                    if ($isfollower->status == 0) {
                        $listA['fstatus'] = 'P';
                    } else {
                        $listA['fstatus'] = '';
                    }
                } else {
                    $listA['fstatus'] = '';
                }

                $listA['id'] = $allActivity[$i]->id;
                $listA['item_id'] = $allActivity[$i]->item_id;
                $listA['item_name'] = $item_name;
                $listA['event_title'] = $event_title;
                $listA['sender_id'] = $allActivity[$i]->sender;
                $listA['sender_name'] = $sender->first_name . ' ' . $sender->last_name;
                $listA['sender_image'] = $sender->profile_pic;

                $listA['receiver_id'] = $allActivity[$i]->receiver;
                $listA['receiver_name'] = $receiver->first_name . ' ' . $receiver->last_name;
                $listA['receiver_image'] = $receiver->profile_pic;

                $listA['activity_type'] = $allActivity[$i]->activity_type;
                $listA['activity_time'] = $time;

                $allActivityList[] = $listA;
            }
            $this->view->allActivityLog = $allActivityList;

            //user's created groups
            $groups = groups::groupList($_SESSION['user_id']);

            $this->view->usersgroups = $groups;

            /// Get following/friend list
            $followingList = usersfriend::getFollowList($_SESSION['user_id']);
            for ($i = 0; $i < count($followingList); $i++) {
                $flist = array();
                $follower_details = users::getUserDetails($followingList[$i]->friend_id);
                $flist['user_id'] = $follower_details->user_id;
                $flist['username'] = $follower_details->first_name . ' ' . $follower_details->last_name;
                $flist['profile_pic'] = $follower_details->profile_pic;
                $followinglst[] = $flist;
            }

            $this->view->followingList = $followinglst;

            $event_notification = event::getEventNotification($_SESSION["user_id"]);


            if (count($event_notification) > 0) {
                $this->view->e_notification = count($event_notification);
            }
        } catch (Exception $e) {
            print_r($e->getMessage());
            exit;
        }
    }

    /// logout process
    public function logoutAction() {
        $db = Zend_Registry::get("db");
        $server = Zend_Registry::get("server");

        $db->beginTransaction();
        session::destroy(session_id());
        $db->commit();
        unset($_SESSION);
        session_destroy();

        $this->_redirect(PS_Util::redirect("https://" . $server->apphost));
    }

    /// facebook login
    public function fbloginAction() {
        //$this->view->headTitle(PS_Translate::_("Facebook"));
        ///BOF FB
        $db = Zend_Registry::get("db");
        $config = Zend_Registry::get("config");
        $server = Zend_Registry::get("server");

        try {
            require 'fb/facebook.php';

            Facebook::$CURL_OPTS[CURLOPT_SSL_VERIFYPEER] = false;
            Facebook::$CURL_OPTS[CURLOPT_SSL_VERIFYHOST] = 2;

            $facebook = new Facebook(array(
                'appId' => '409155555814120',
                'secret' => 'fb5600dcbf3913e4b3e8c66a50751e43',
            ));

            $userfb = $facebook->getUser();
            //$fb_friend = $facebook->api('/me/friends');
            ///wall post

            /* $user_profile1 = $facebook->api($userfb);
              $token = $facebook->getAccessToken();
              $wall = $facebook->api('/'.$user_profile1['id'].'/feed/',array('access_token' => $token,'limit'=>5));
              print_r($wall); */
            ///
            ///EOF FB
            //
		//$events = $facebook->api('/100000508660479/events');
            $token = $facebook->getAccessToken();
            $fql = "SELECT eid, name, pic, creator, description,location FROM event WHERE eid IN (SELECT eid FROM event_member WHERE uid=100000508660479) AND creator=100000508660479";


            //$fql = "select eid, name, pic, creator FROM event WHERE eid in (select eid from event_member where uid = '100000508660479')";
            //$fql = "SELECT uid, rsvp_status, inviter FROM event_member WHERE uid='100000508660479'";
            $ret_obj = $facebook->api(array(
                'method' => 'fql.query',
                'query' => $fql,
                'access_token' => $token
            ));

            echo '<pre>';
            print_r($ret_obj);
            echo '</pre>';

            //$fql_member = "SELECT inviter_type, eid, inviter, uid, rsvp_status, start_time FROM event_member WHERE eid = 231592063644384";
            $fql_member = "SELECT inviter_type, eid, inviter, uid, rsvp_status, start_time FROM event_member WHERE uid = 100000508660479";
            $mem_obj = $facebook->api(array(
                'method' => 'fql.query',
                'query' => $fql_member,
                'access_token' => $token
            ));
            echo '-------<pre>';
            print_r($mem_obj);
            echo '</pre>';
            for ($i = 0; $i < count($mem_obj); $i++) {
                echo '<br>test::' . $mem_obj[$i]['rsvp_status'];
            }


            exit;
        } catch (Exception $e) {
            //Rollback transaction			   
            $this->view->errors[] = $e->getMessage();
            print_r($e->getMessage());
            exit;
        }
        //$this->view->friendlist=$fb_friend;
    }

///fblogin
    /// twitter login
    public function twitterloginAction() {
        $server = Zend_Registry::get("server");
        $config = Zend_Registry::get("config");
        $logger = Zend_Registry::get("logger");
        $db = Zend_Registry::get("db");

        $logger->info("[START] " . __FILE__ . ':' . __FUNCTION__);

        require 'twitter/twitteroauth.php';
        require 'amazonQuery.php';
        $params = $this->getRequest()->getParams();

        if (!empty($params['oauth_verifier']) && !empty($_SESSION['oauth_token']) && !empty($_SESSION['oauth_token_secret'])) {
            //$twitteroauth = new TwitterOAuth('CiLrPbQcM3t92ptRgw2YA', 'P2MVXJByFPllI4eNhVsDjXQiYsou2jSGnfUc4nEmk', $_SESSION['oauth_token'], $_SESSION['oauth_token_secret']);
            $twitteroauth = new TwitterOAuth($server->Consumer_key, $server->Consumer_secret, $_SESSION['oauth_token'], $_SESSION['oauth_token_secret']);

            $access_token = $twitteroauth->getAccessToken($params['oauth_verifier']);
            $_SESSION['access_token'] = $access_token;
            $user_info = $twitteroauth->get('account/verify_credentials');

            $data['first_name'] = $user_info->name;
            $data['twitter_id'] = $user_info->id;
            $data['twitter_name'] = $user_info->screen_name;
            $data['twitter_image'] = $user_info->profile_image_url;
            $data['created_date'] = date('Y-m-d H:i:s');
            $data['device_type'] = 'web';
            $data['udid']='';$data['facebook_id'] = '';$data['facebook_image'] = '';$data['facebook_email']='';$data['last_name'] = '';$data['email_id']='';$data['password']='';
            $data['google_id']='';$data['google_image']='';$data['google_email']='';
            $data['address']='';$data['fbID']='';$data['user_udid']='';
            $data['current_login']=$data['created_date'];$data['last_login']=$data['created_date'];$data['modified_date']='0000-00-00 00:00:00';
            $data['status'] = 0; $data['upcoming_event'] = 1; $data['shared_gift'] = 1; $data['added_followers'] = 1; $data['facebook_notification'] = 0;
            $data['follow_permission'] = 1;

            $twitter_user = users::getTwitterUser($user_info->id);
            $user_id = $twitter_user->user_id;
            $changeImage = true;
            if (!empty($user_id) && users::checkImageUserUpdated($user_id)) {
                $changeImage = false;
            }
            if ($changeImage) {
               $data['is_pic_updated'] = null;
               $data['birth_date'] = '0000-00-00';
               $image_url = str_replace('_normal', '',$user_info->profile_image_url);
               $path_arr = explode("/", $image_url);

               $image_name = $path_arr[count($path_arr) - 1];

               $target_path = $_SERVER["DOCUMENT_ROOT"] . "/userpics/" . $image_name;
               $thumb_path = $_SERVER["DOCUMENT_ROOT"] . "/userpics/thumb/" . $image_name;

               phpQuery::save_image($image_url, $target_path);
               phpQuery::save_image($image_url, $thumb_path);
            
               require 'SimpleImage.php';
               $image = new SimpleImage();
               $image->load($thumb_path);
               ////$image->resize(32,32);
               //$image->save($thumb_path);

               $size = getimagesize($thumb_path);
               $width = $size[0];
               $height = $size[1];
               if ($width > 50) {
                  $image->resizeToWidth(50);
                  $image->save($thumb_path);
               }
               /* if($height > 50)
               {
                  $image->resizeToHeight(50);
                  $image->save($thumb_path);
               } */
               $data['profile_pic'] = $image_name;
            } 
            try {
                $db->beginTransaction();
                if (empty($user_id)) {
                     $db->insert('users', $data);
                     $user_id = $db->lastInsertId();
                     notifications::addWellcomeNotification($user_id);
                } else {
                    $data['current_login'] = date('Y-m-d H:i:s');
                    $data['last_login'] = users::getLastLogin($user_id);

                    $db->update('users', $data, 'user_id = ' . $user_id);
                }

                $db->commit();
                $_SESSION['user_id'] = $user_id;
                $_SESSION['user_twitter_id'] = $user_info->id;
                $_SESSION['logged_in'] = true;
                session::write(session_id(), $_SESSION);
                //echo 'user id:'.$user_id;

                $logger->info("webapp_style = " . $_SESSION['webapp_style']);
//                if ($_SESSION['webapp_style'] == 'new_design') {
                    PS_Util::redirect("https://" . $server->apphost . "/login/newhome");
//                } else {
//                    PS_Util::redirect("http://" . $server->apphost . "/login/home");
//                }
            } catch (Exception $e) {
                $logger->info("Inside catch");
                /* echo '<pre>';
                  print_r($e->getMessage());
                  exit; */
                $this->view->errors[] = $e;
            }

            //// add in db
        } else {
            $logger->info("Inside else");
            echo 'in else';
        }

        $logger->info("[END] " . __FILE__ . __FUNCTION__);
    }

///end function
    //When user opts for twitter login, user will be redirected here
    public function logintwiterAction() {
        $server = Zend_Registry::get("server");
        require 'twitter/twitteroauth.php';
        $twitteroauth = new TwitterOAuth($server->Consumer_key, $server->Consumer_secret);

        $request_token = $twitteroauth->getRequestToken("https://" . $server->apphost . "/login/twitterlogin");

        $_SESSION['oauth_token'] = $request_token['oauth_token'];
        $_SESSION['oauth_token_secret'] = $request_token['oauth_token_secret'];
        session::write(session_id(), $_SESSION);
        if ($twitteroauth->http_code == 200) {
            // Let's generate the URL and redirect
            $url = $twitteroauth->getAuthorizeURL($request_token['oauth_token']);
            PS_Util::redirect($url);
        } else {
            // It's a bad idea to kill the script, but we've got to know when there's an error.
            die('Something wrong happened.');
        }
    }

///end function
    /// signup validation
    protected function _initSignup() {
        $identical = new PS_Validate_Identical($this->_getParam("password"));
        $identical->setMessage(PS_Translate::_("Password Mismatch"));

        //$password = new PS_Validate_NotEmpty($this->_getParam("password"));
        //$password->setMessage(PS_Translate::_("Please enter password"));

        $cpassword = new PS_Validate_NotEmpty($this->_getParam("confirm-password"));
        $cpassword->setMessage(PS_Translate::_("Please enter confirm password"));

        $dob = new PS_Validate_NotEmpty($this->_getParam("birth_date"));
        $dob->setMessage(PS_Translate::_("Please enter date of birth"));

        $first_name = new PS_Validate_NotEmpty($this->_getParam("first_name"));
        $first_name->setMessage(PS_Translate::_("Please enter first name"));

        $last_name = new PS_Validate_NotEmpty($this->_getParam("last_name"));
        $last_name->setMessage(PS_Translate::_("Please enter last name"));

        //$password = new PS_Validate_Password(array("min" => 6, "digitRequired" => true, "specialRequired" => true));
        ////$password->setMessage(PS_Translate::_("Please enter "));

        $this->fields = new PS_Form_Fields(
                array(
            "first_name" => array(
                $first_name,
                "required" => true
            ),
            "last_name" => array(
                $last_name,
                "required" => true
            ),
            "email_id" => array(
                new PS_Validate_EmailAddress(),
                "filter" => new PS_Filter_RemoveSpaces(),
                "required" => true
            ),
            "password" => array(
                //$password,
                new PS_Validate_Password(array("min" => 6, "digitRequired" => true)),
                "required" => true
            ),
            "confirm_password" => array(
                //$cpassword,
                $identical,
                "required" => true
            ),
            "birth_date" => array(
                array($dob, new PS_Validate_Birthdate()),
                "required" => true
            ),
            "tos" => array(
                new PS_Validate_NotEmpty(),
                "required" => true
            )
                )
        );
        $params = $this->getRequest()->getParams();
        $this->fields->setData($params);
        $this->view->fields = $this->fields;
    }
/*
    public function mailtestAction() {
        $db = Zend_Registry::get("db");
        $config = Zend_Registry::get("config");
        $server = Zend_Registry::get("server");
        
        $tomail = "chikkegowda20@gmail.com";
        $mailMsg = "
              <html><head><META http-equiv= Content-Type  content= text/html; charset=utf-8 ></head><body>
<div marginheight= 0  marginwidth= 0 >
	<table style= background-color:rgb(21,18,18)  cellpadding= 0  cellspacing= 0  bgcolor= ecebeb  border= 0  width= 100% >
		<tbody><tr>
		<td>
				<table cellpadding= 0  cellspacing= 0  align= center  border= 0  width= 590 >
					<tbody><tr><td style= font-size:30px;line-height:30px  height= 30 > </td></tr>
					<tr>
						<td>
							<table style= border-collapse:collapse  cellpadding= 0  cellspacing= 0  align=left  border=0 >
								<tbody><tr>
									<td align= center >
										<table cellpadding= 0  cellspacing= 0  align= center  border=0 >
											<tbody><tr>
												<td style= line-height:21px  align= center >

	<a href= https://giftvise.com/  style=display:block;border-style:none!important;border:0!important  target= _blank ><img style= display:block;width:128px  src=https://www.giftvise.com/newdesign/img/tpl/new-giftvise-logo.png width=76 ></a>
												</td>			
											</tr>
										</tbody></table>		
									</td>
								</tr>
								
							</tbody></table>
							
							<table style=border-collapse:collapse  cellpadding= 0  cellspacing=0  align=left  border=0  width=5px>
								<tbody><tr><td style= font-size:20px;line-height:20px  height=20  width=5px > </td></tr>
							</tbody></table>
							
							<table style=border-collapse:collapse  cellpadding= 0  cellspacing= 0  align= right  border=0 >
								
								<tbody><tr>
									<td>
										<table style=border-collapse:collapse  cellpadding= 0  cellspacing= 0  align= left  border= 0 >
			
				                			<tbody><tr>
				                				<td>
				                					<table style=border-collapse:collapse  cellpadding= 0  cellspacing= 0  align=center>
				                						<tbody><tr>
				                							
							                				<td style= color:#ffffff;font-size:14px;font-family:&#39;Questrial&#39;,Helvetica,sans-serif;line-height:24px  align= center >
							                					<div style= line-height:24px >
							                						<span>
<a href='https://".$server->apphost."/modals/modalshowhelpcenter?tab=about' style=color:#878b99;text-decoration:none >About</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href='https://".$server->apphost."/modals/modalshowhelpcenter?tab=faq' style= color:#878b99;text-decoration:none>Help</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a><a href='https://".$server->apphost."/modals/modalshowhelpcenter?tab=contact' style= color:#878b99;text-decoration:none >Contact</a>
								                					
							                						</span>
							                					</div>	
							                				</td>
				                						</tr>
				                					</tbody></table>
				                				</td>
				                			</tr>
			                			</tbody></table>
									</td>
								</tr>	
							</tbody></table>	
						</td>
					</tr>
					
					<tr><td style= font-size:25px;line-height:25px  height= 25 > </td></tr>
					<tr>
						<td style= line-height:21px  align= center >
						</td>			
					</tr>
					
					<tr><td style= font-size:50px;line-height:50px  height= 50 > </td></tr>
					<tr><td style= font-size:60px;line-height:60px  height= 60 > </td></tr>
					
					<tr>
						<td style= color:#ffffff;font-size:40px;font-family:&#39;Questrial&#39;,Helvetica,sans-serif;line-height:28px  align= center >
							
							
							
							<div style= line-height:28px >
								<span>Welcome to <span>Gift</span>vise!</span>
							</div>
        				</td>
					</tr>
					
					<tr><td style= font-size:25px;line-height:25px  height= 25 > </td></tr>
					
					<tr>
						<td align= center >
							<table cellpadding= 0  cellspacing= 0  align= center  border= 0  width= 540 >
								<tbody><tr>
									
									<td style= color:#878b99;font-size:18px;font-family:&#39;Questrial&#39;,Helvetica,sans-serif;line-height:30px  align= center >
										<div style= line-height:30px >
											
											
											<span>The perfect way to give and receive the right gift for the right event</span>
										</div>
			        				</td>	
								</tr>
							</tbody></table>
						</td>
					</tr>
					
					<tr><td style= font-size:35px;line-height:35px  height= 35 > </td></tr>
					
					<tr>
						<td align= center >
							
							<table style= border-radius:50px  cellpadding= 0  cellspacing= 0  align= center  bgcolor= f06e6a  border= 0  width= 240 >
								
								<tbody><tr><td style= font-size:18px;line-height:18px  height= 18 > </td></tr>
								
								<tr>
	                				<td style= color:#ffffff;font-size:18px;font-family:&#39;Questrial&#39;,Helvetica,sans-serif  align= center >
	                					
	                					
		                    			<div style= line-height:24px >
<a style=text-decoration:none href='https://" . $server->apphost . "/login/activate/id/" . md5($user_id) . "'>
		                    				<span style= color:#ffffff;text-decoration:none >Confirm Email</span>
		                    	             </a>		</div>
		                    		</td>								
								</tr>
								
								<tr><td style= font-size:18px;line-height:18px  height= 18 > </td></tr>
							
							</tbody></table>
						</td>
					</tr>
					
					<tr><td style= font-size:65px;line-height:65px  height= 65 > </td></tr>
					<tr><td style= font-size:25px;line-height:25px  height= 50 > </td></tr>
					
				</tbody></table>
			</td></tr>
		
	</tbody></table>
	
	<table style= background-color:rgb(255,255,255)  cellpadding= 0  cellspacing= 0  bgcolor= f3f4f5  border= 0  width= 100% >
		<tbody><tr>
			<td>
				<table cellpadding= 0  cellspacing= 0  align= center  border= 0  width= 590 >
					
					<tbody><tr><td style= font-size:30px;line-height:30px  height= 30 > </td></tr>
					<tr><td style= font-size:50px;line-height:50px  height= 50 > </td></tr>
					
					<tr>
						<td style= color:rgb(86,94,120);font-size:28px;font-family:&#39;Questrial&#39;,Helvetica,sans-serif;line-height:22px  align= center >
							
							
							<div style= line-height:22px >
								<span>Happy gifting!</span>
							</div>
        				</td>	
					</tr>
					
					<tr><td style= font-size:20px;line-height:20px  height= 20 > </td></tr>
					
					<tr>
						<td align= center >
							<table style= border-radius:4px  cellpadding= 0  cellspacing= 0  align= center  bgcolor= f06e6a  border= 0  width= 24 >
								<tbody><tr><td style= font-size:4px;line-height:4px  height= 4 > </td></tr>
							</tbody></table>
						</td>
					</tr>
					
					<tr><td style= font-size:20px;line-height:20px  height= 20 > </td></tr>
					
					<tr>
						<td align= center >
							<table cellpadding= 0  cellspacing= 0  align= center  border= 0  width= 580 >
								<tbody><tr>
									<td style= color:#878b99;font-size:14px;font-family:&#39;Questrial&#39;,Helvetica,sans-serif;line-height:24px  align= center >
										<div style= line-height:24px >
											<span>Thank you for signing up for 
Giftvise, the best way to give and receive the right gift for the right 
event. We're ecstatic you decided to join us and are dedicated to making
 your experience a most enjoyable one.
Time to get out there and start gifting! If you like what you see, be 
sure to tell your friends. The more friends you tell, the more valuable 
the service is for you and everyone else.</span>
										</div>
			        				</td>
								</tr>
							</tbody></table>
						</td>
					</tr>
					
					<tr><td style= font-size:30px;line-height:30px  height= 30 > </td></tr>
					<tr><td style= font-size:50px;line-height:50px  height= 50 > </td></tr>
					
				</tbody></table>
			</td>
		</tr>
	</tbody></table>
	
	<table cellpadding= 0  cellspacing= 0  bgcolor= 414655  border= 0  width= 100% >
		
		</table>
	
<!--
	<table cellpadding= 0  cellspacing= 0  bgcolor= 363b49  border= 0  width= 100% >
							
		<tbody><tr><td style= font-size:20px;line-height:20px  height= 20 > </td></tr>
					
		<tr>
			<td align= center >
				
				<table cellpadding= 0  cellspacing= 0  align= center  border= 0  width= 590 >
					
					<tbody><tr>
						<td align= center >
								                		
	                		<table cellpadding= 0  cellspacing= 0  align= center  border= 0 >
	                			<tbody><tr>
	                				<td style= color:#5a5f70;font-size:14px;font-family:&#39;Questrial&#39;,Helvetica,sans-serif;line-height:25px  align= center >
	                					<div style= line-height:25px >
	                						<span>
		                					
		                						Copyright @ Giftvise 2014
		                					
	                						</span>
	                					</div>	
	                				</td>
	                			</tr>
	                			
                			</tbody></table>
                			
						</td>
					</tr>
					
				</tbody></table>
			</td>
		</tr>
		<tr><td style= font-size:20px;line-height:20px  height= 20 > </td></tr>
	</tbody></table>
-->
<br><br>
<table border=0 width=100% cellpadding=0 cellspacing=0 bgcolor=414655 class=bg3_color>

 <tbody><tr><td height=20 style=font-size:20px;line-height:20px;>&nbsp;</td></tr>

 <tr> <td> <table border=0 align=center width=590 cellpadding=0 cellspacing=0 class=container590>

 <tbody><tr> <td> <table border=0 width=270 align=left cellpadding=0 cellspacing=0 style=border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;
class=container590>

 <tbody><tr> <td align=left style=color:#ffffff;font-size:18px;font-family:'Questrial',Helvetica,sans-serif;line-height:22px;>
         <!-- ======= section text ====== -->

 <div class=editable_text style=line-height:22px> <span class=text_container>

 Follow us

 </span> </div> </td> </tr>

 <tr><td height=15 style=font-size:15px;line-height:15px;>&nbsp;</td></tr>

 <tr> <td align=left style=color:#878b99;font-size:14px;font-family:'Questrial',Helvetica,sans-serif;line-height:25px; >
         <!-- ======= section subtitle ====== -->
 
 <div class=editable_text style=line-height:25px;> <span class=text_container>
 
 We're pretty active on social media.
 
 </span> </div> </td> </tr>
 
 <tr><td height=15 style=font-size:15px;line-height:15px;>&nbsp;</td></tr>

 <tr> <td align=left> <table border=0 align=left cellpadding=0 cellspacing=0> <tbody><tr> <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td> <td> <a style=display:block;width:50px;height:13px;border-style:none!important;border:0!important; href=https://www.facebook.com/Giftvise class=editable_img><img width=8 height=13 border=0 style=display:block;width:40px;height:40px; src=https://www.giftvise.com/newdesign/img/tpl/sign-in_facebook.png alt=facebook class=></a> </td> <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td> <td> <a style=display:block;width:40px;height:40px;border-style:none!important;border:0!important; href=https://www.twitter.com/giftvise class=editable_img><img width=13 height=10
border=0 style=display:block;width:40px;height:40px; src=http://www.giftvise.com/newdesign/img/tpl/sign-in_twitter.png alt=twitter></a> </td> </tr> </tbody></table> </td> </tr>
</tbody></table>
 <table border=0 width=2 align=left cellpadding=0 cellspacing=0 style=border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt; class=container590>
<tbody><tr><td width=2 height=30 style=font-size:30px;line-height:30px;></td></tr> </tbody></table>

 <table border=0 width=180 align=right cellpadding=0 cellspacing=0 style=border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt; class=container590>

 <tbody><tr> <td align=left style=color:#ffffff;font-size:18px;font-family:'Questrial',Helvetica,sans-serif;line-height:22px; class=white_color> 
         <!-- ======= section text ====== -->

 <div class=editable_text style=line-height:22px> <span class=text_container>

 Get in touch



 </span> </div> </td> </tr>

 <tr><td height=15 style=font-size:15px;line-height:15px;>&nbsp;</td></tr>

 <tr> <td align=left style=color:#878b99;font-size:14px;font-family:'Questrial',Helvetica,sans-serif;line-height:25px; class=text_color>
         <!-- ======= section subtitle ====== -->

 <div style=line-height:25px;text-decoration:none;> <span>

 feedback @ giftvise.com <br>
 <br>
 </span> </div> </td> </tr>

 </tbody></table> </td> </tr> </tbody></table> </td> </tr>

 </tbody></table>
<!-- copy right secion -->
<table border=0 width=100% cellpadding=0 cellspacing=0 bgcolor=363b49 class=bg4_color>

 <tbody><tr><td height=20 style=font-size:20px;line-height:20px;>&nbsp;</td></tr>

 <tr> <td align=center>

 <table border=0 align=center width=590 cellpadding=0 cellspacing=0 class=container590>

 <tbody><tr> <td align=center>

 <table border=0 align=center cellpadding=0 cellspacing=0> <tbody><tr> 
<td style=color:#5a5f70;font-size:14px;font-family:&#39;Questrial&#39;,Helvetica,sans-serif;line-height:25px align=center>
<div class=editable_text style=line-height:25px;> <span class=text_container>

 Copyright @ Giftvise 2014

 </span> </div> </td> </tr>

 </tbody></table>

 </td> </tr>

 </tbody></table> </td> </tr>

 <tr><td height=20 style=font-size: 20px; line-height: 20px;>&nbsp;</td></tr>

 </tbody></table>

</div></body></html>

       ";

        $transport = new Zend_Mail_Transport_Sendmail($config->supportEmail);
                    Zend_Mail::setDefaultTransport($transport);
                    $mail = new Zend_Mail();
                    $mail->setSubject('Giftvise - Confirm your email address');
                    $mail->setFrom($config->supportEmail, 'Giftvise');
                    $mail->addTo($tomail, $name);
                    $mail->setBodyHtml($mailMsg);
                    $mail->send();

        $this->_helper->layout()->disableLayout();
        exit;
    }
*/
///end function
    ///local sign-up
    public function signupAction() {
        $db = Zend_Registry::get("db");
        $config = Zend_Registry::get("config");
        $server = Zend_Registry::get("server");

        $params = $this->getRequest()->getParams();
        $this->_initSignup();
        $this->errorOnNoscript();
        $errFlag = 0;
        $success = false;
        if ($params["tos"] == '') {
            $msg = 'Please review and accept our Terms of Service and Privacy Policy';
        }
        try {
            $db->beginTransaction();
            $this->fields->validate();
            $emailExist = users::checkExistsEmail($params['email_id']);

            if (empty($emailExist)) {
                //add user
                //$data = $params;
                $data['first_name'] = trim($params["first_name"]);
                $data['last_name'] = trim($params["last_name"]);
                $data['email_id'] = trim($params["email_id"]);
                $data['gender'] = $params["gender"];
                $data['status'] = 1;
                $birthDate = DateTime::createFromFormat('m/j/Y',$params['birth_date']);
                $data['birth_date'] = $birthDate->format('Y-m-d');
                $data['created_date'] = date('Y-m-d H:i:s');
                $data['password'] = md5($params["password"]);
                $data['device_type'] = 'web';
                $data['udid'] = '';
                $data['twitter_name'] = '';
                $data['twitter_id'] = '';
                $data['twitter_image'] = '';
                $data['facebook_id'] = '';
                $data['facebook_image'] = '';
                $data['address'] = '';
                $data['fbID'] = '';
                $data['modified_date'] = '0000-00-00 00:00:00';
                $data['current_login'] = date('Y-m-d H:i:s');
                $data['last_login'] = date('Y-m-d H:i:s');
                $data['shared_gift'] = '0';
                $data['added_followers'] = '0';
                $data['facebook_notification'] = '0';
                $data['upcoming_event'] = '0';
                $data['user_udid'] = '';
                $data['profile_type'] = 'Public';
                $data['follow_permission'] = '1';

                $upload = new fileupload();
                if ($upload->moveImageFromTemp('userpics', $params['image'])) {
                    $data['profile_pic'] = $params['image'];
                } else {
                    
                    $data['profile_pic'] = 'default-profile-pic.png';
                }

                $db->insert('users', $data);
                $user_id = $db->lastInsertId();

                /// create event for user's Birthday
                if ($user_id != '' && $params['birth_date'] != '') {

                    event::addBirthdayEvents($user_id, $params['birth_date'], $data['first_name'] . ' ' . $data['last_name']);

                }/// event

                notifications::addWellcomeNotification($user_id);

                if ($user_id != '') {
                    //// send activation link to user
                    //$mailBody = '';

/*  ---------------------------------------------------------- email body start------------------------------------------------  */
                   $mailMsg = "
<html><head>

<!-- Define Charset -->
        <meta http-equiv=Content-Type content=text/html; charset=UTF-8 />
        
        <!-- Responsive Meta Tag -->
        <meta name=viewport content=width=device-width; initial-scale=1.0; maximum-scale=1.0; />
        <link href='http://fonts.googleapis.com/css?family=Questrial' rel='stylesheet' type='text/css'>
        
        <!--- Importing Twitter Bootstrap icons -->
        <link href=http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css rel=stylesheet>

</head><body>
<div marginheight= 0  marginwidth= 0 >
	<table style= background-color:rgb(21,18,18)  cellpadding= 0  cellspacing= 0  bgcolor= ecebeb  border= 0  width= 100% >
		<tbody><tr>
		<td>
				<table cellpadding= 0  cellspacing= 0  align= center  border= 0  width=100% >
					<tbody><tr><td style= font-size:30px;line-height:30px  height= 30 > </td></tr>
					<tr>
						<td>
							<table style= border-collapse:collapse  cellpadding= 0  cellspacing= 0  align=left  border=0 width=45% >
								<tbody><tr>
									<td align= center >
										<table cellpadding= 0  cellspacing= 0  align= center  border=0 >
											<tbody><tr>
												<td style= line-height:21px  align= center >

	<a href= https://giftvise.com/  style=display:block;border-style:none!important;border:0!important  target= _blank ><img style= display:block;width:128px  src=https://www.giftvise.com/newdesign/img/tpl/new-giftvise-logo.png width=180 ></a>
												</td>			
											</tr>
										</tbody></table>		
									</td>
								</tr>
								
							</tbody></table>
							
							<table style=border-collapse:collapse  cellpadding= 0  cellspacing=0  align=left  border=0  width=5px>
								<tbody><tr><td style= font-size:20px;line-height:20px  height=20  width=5px > </td></tr>
							</tbody></table>
							
							<table style=border-collapse:collapse  cellpadding=10  cellspacing= 0  align= right  border=0 width=30%>
								
								<tbody><tr>
									<td>
										<table style=border-collapse:collapse  cellpadding= 0  cellspacing= 0  align= left  border= 0 >
			
				                			<tbody><tr>
				                				<td>
				                					<table style=border-collapse:collapse  cellpadding= 0  cellspacing= 0  align=center>
				                						<tbody><tr>
				                							
							                				<td style= color:#ffffff;font-size:14px;font-family:&#39;Questrial&#39;,Helvetica,sans-serif;line-height:24px  align= center >
							                					<div style= line-height:24px >
							                						<span>
<a href='https://".$server->apphost."/modals/modalshowhelpcenter?tab=about' style=color:#878b99;text-decoration:none >About</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href='https://".$server->apphost."/modals/modalshowhelpcenter?tab=faq' style= color:#878b99;text-decoration:none>Help</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a><a href='https://".$server->apphost."/modals/modalshowhelpcenter?tab=contact' style= color:#878b99;text-decoration:none >Contact</a>
								                					
							                						</span>
							                					</div>	
							                				</td>
				                						</tr>
				                					</tbody></table>
				                				</td>
				                			</tr>
			                			</tbody></table>
									</td>
								</tr>	
							</tbody></table>	
						</td>
					</tr>
					
					<tr>
						<td style= line-height:21px  align= center >
						</td>			
					</tr>
					
					<tr><td style= font-size:50px;line-height:50px  height= 50 > </td></tr>
					
					<tr>
						<td style= color:#ffffff;font-size:40px;font-family:&#39;Questrial&#39;,Helvetica,sans-serif;line-height:28px  align= center >
							
							
							
							<div style= line-height:28px >
								<span>Welcome to <span>Gift</span>vise!</span>
							</div>
        				</td>
					</tr>
					
					<tr><td style= font-size:25px;line-height:25px  height= 25 > </td></tr>
					
					<tr>
						<td align= center >
							<table cellpadding= 0  cellspacing= 0  align= center  border= 0  width= 540 >
								<tbody><tr>
									
									<td style= color:#878b99;font-size:18px;font-family:&#39;Questrial&#39;,Helvetica,sans-serif;line-height:30px  align= center >
										<div style= line-height:30px >
											
											
											<span>The perfect way to give and receive the right gift for the right event</span>
										</div>
			        				</td>	
								</tr>
							</tbody></table>
						</td>
					</tr>
					
					<tr><td style= font-size:35px;line-height:35px  height= 35 > </td></tr>
					
					<tr>
						<td align= center >
							
							<table style= border-radius:50px  cellpadding= 0  cellspacing= 0  align= center  bgcolor= f06e6a  border= 0  width= 240 >
								
								<tbody><tr><td style= font-size:18px;line-height:18px  height= 18 > </td></tr>
								
								<tr>
	                				<td style= color:#ffffff;font-size:18px;font-family:&#39;Questrial&#39;,Helvetica,sans-serif  align= center >
	                					
	                					
		                    			<div style= line-height:24px >
<a style=text-decoration:none href='https://" . $server->apphost . "/login/activate/id/" . md5($user_id) . "'>
		                    				<span style= color:#ffffff;text-decoration:none >Confirm Email</span>
		                    	             </a>		</div>
		                    		</td>								
								</tr>
								
								<tr><td style= font-size:18px;line-height:18px  height= 18 > </td></tr>
							
							</tbody></table>
						</td>
					</tr>
					
					<tr><td style= font-size:65px;line-height:65px  height= 65 > </td></tr>
					
				</tbody></table>
			</td></tr>
		
	</tbody></table>
	
<!--	
	<table cellpadding= 0  cellspacing= 0  bgcolor= 363b49  border= 0  width= 100% >
							
		<tbody><tr><td style= font-size:20px;line-height:20px  height= 20 > </td></tr>
					
		<tr>
			<td align= center >
				
				<table cellpadding= 0  cellspacing= 0  align= center  border= 0  width= 590 >
					
					<tbody><tr>
						<td align= center >
								                		
	                		<table cellpadding= 0  cellspacing= 0  align= center  border= 0 >
	                			<tbody><tr>
	                				<td style= color:#5a5f70;font-size:14px;font-family:&#39;Questrial&#39;,Helvetica,sans-serif;line-height:25px  align= center >
	                					<div style= line-height:25px >
	                						<span>
		                					
		                						Copyright @ Giftvise 2014
		                					
	                						</span>
	                					</div>	
	                				</td>
	                			</tr>
	                			
                			</tbody></table>
                			
						</td>
					</tr>
					
				</tbody></table>
			</td>
		</tr>
		<tr><td style= font-size:20px;line-height:20px  height= 20 > </td></tr>
	</tbody></table>
-->
<!-- copy right secion -->
<table border=0 width=100% cellpadding=0 cellspacing=0 bgcolor=#ffffff class=bg4_color>

 <tbody><tr><td height=20 style=font-size:20px;line-height:20px;>&nbsp;</td></tr>

 <tr> <td align=center>

 <table border=0 align=center width=590 cellpadding=0 cellspacing=0 class=container590>

 <tbody><tr> <td align=center>

 <table border=0 align=center cellpadding=0 cellspacing=0> <tbody><tr> 
<td style=color:#5a5f70;font-size:14px;font-family:&#39;Questrial&#39;,Helvetica,sans-serif;line-height:25px align=center>
<div class=editable_text style=line-height:25px;> <span class=text_container>

 Copyright @ Giftvise 2014

 </span> </div> </td> </tr>

 </tbody></table>

 </td> </tr>

 </tbody></table> </td> </tr>

 <tr><td height=20 style=font-size: 20px; line-height: 20px;>&nbsp;</td></tr>

 </tbody></table>
</div></body></html>
                         ";

/* ---------------------------------------------------- email body end ------------------------------------------------- */ 
                    $transport = new Zend_Mail_Transport_Sendmail($config->supportEmail);
                    Zend_Mail::setDefaultTransport($transport);
                    $mail = new Zend_Mail();
                    $mail->setSubject('Confirm your Giftvise registration');
                    $mail->setFrom($config->supportEmail, 'Giftvise');
                    $mail->addTo($params["email_id"], $name);
                    $mail->setBodyHtml($mailMsg);
                    $mail->send();
                    ////
                    $request = usersfriend::checkRquest($params['email_id']);
                    //print_r($request);
                    if (!empty($request)) {
                        $requestedUserid = $request->user_id;
                        $frienddata['user_id'] = $requestedUserid;
                        $frienddata['friend_id'] = $user_id;
                        $frienddata['status'] = '1';

                        $db->insert('usersfriend', $frienddata);
                    }
                }

                $db->commit();

               // if (isset($user_id) && $user_id > 0) {
                    $msg = 'Please check your email to activate the account';
                    $success = true;
              /*   } else {
                    $msg = 'Error in processsing, please try again later.';
                }
              */
            } else {
                $msg = 'This email is already registered';
            }
        } catch (Exception $e) {
            //Rollback transaction
            $db->rollBack();

            $msg = $e->getMessage();
            $passwordError = substr($this->view->printError("password"), 32, -6);
            $dobError = substr($this->view->printError("birth_date"), 32, -6);
            $success = false;
            $msg = 'Error in processsing, please try again later.';
            return $this->_helper->json(array('success' => $success, 'msg' => $msg, 'pwdErr' => $passwordError, 'dobErr' => $dobError));
        }
        // echo "test:".$errFlag;
        return $this->_helper->json(array('success' => $success, 'msg' => $msg, 'pwdErr' => $passwordError, 'dobErr' => $dobError));
    }

    // display profile popup
    public function profileAction() {
        $db = Zend_Registry::get("db");
        $config = Zend_Registry::get("config");
        $server = Zend_Registry::get("server");

        $user_details = users::getUserDetails($_SESSION['user_id']);
        $this->view->userDetail = $user_details;

        /// budget
        $budget = budget::showBudget($_SESSION['user_id']);
        $this->view->budget = $budget[0]->amount;



        $this->_initProfile();
        $this->_initPassword();
    }

    /// update profile
    public function updateprofileAction() {
        $db = Zend_Registry::get("db");
        $config = Zend_Registry::get("config");
        $server = Zend_Registry::get("server");

        $params = $this->getRequest()->getParams();
        $this->_initProfile();

        $data['first_name'] = $params["first_name"];
        $data['last_name'] = $params["last_name"];
        $data['gender'] = $params["gender"];
        $data['birth_date'] = date('Y-m-d', strtotime($params['birth_date']));
        $data['modified_date'] = date('Y-m-d H:i:s');

        try {
            $db->beginTransaction();
            $this->fields->validate();
            ///upload pic			
            require 'SimpleImage.php';
            if (!empty($_FILES["profile_pic"]["name"])) {
                if ((strtolower($_FILES["profile_pic"]["type"]) == "image/gif") || (strtolower($_FILES["profile_pic"]["type"]) == "image/jpeg") || (strtolower($_FILES["profile_pic"]["type"]) == "image/pjpeg") || (strtolower($_FILES["profile_pic"]["type"]) == "image/png")) {

                    ///delete old image
                    if ($params["old_img"] != '') {
                        unlink($_SERVER["DOCUMENT_ROOT"] . "/userpics/" . $params["old_img"]);
                        unlink($_SERVER["DOCUMENT_ROOT"] . "/userpics/thumb/" . $params["old_img"]);
                    }
                    ///
                    $imgExtsn = explode(".", $_FILES["profile_pic"]["name"]);
                    $countExtsn = count($imgExtsn) - 1;
                    $file = $imgExtsn[$countExtsn - 1] . '_' . str_replace(' ', '_', $params["username"]) . rand(100, 100000) . "." . $imgExtsn[$countExtsn];
                    $newfilename = $_SERVER["DOCUMENT_ROOT"] . "/userpics/" . $file;
                    $thumb_path = $_SERVER["DOCUMENT_ROOT"] . "/userpics/thumb/" . $file;

                    /// resize image 
                    $image = new SimpleImage();
                    $image->load($_FILES["profile_pic"]["tmp_name"]);
                    $image->save($newfilename);
                    $image->save($thumb_path);

                    $size = getimagesize($newfilename);
                    $width = $size[0];
                    $height = $size[1];
                    if ($width > 150) {
                        $image->resizeToWidth(150);
                        $image->save($newfilename);
                    }
                    /* if($height > 80)
                      {
                      $image->resizeToHeight(80);
                      $image->save($newfilename);
                      } */

                    //// resize thumb image
                    $size = getimagesize($thumb_path);
                    $width = $size[0];
                    $height = $size[1];
                    if ($width > 50) {
                        $image->resizeToWidth(50);
                        $image->save($thumb_path);
                    }
                    /* if($height > 50)
                      {
                      $image->resizeToHeight(50);
                      $image->save($thumb_path);
                      } */
                    ////
                    //$image->load($_FILES["profile_pic"]["tmp_name"]);		
                    //$image->save($newfilename);	

                    $data['profile_pic'] = $file;
                } else {
                    $imgMsg = 'Please upload image in JPEG/PNG/GIF/PGPEG format';
                }
            }

            //update
            $result = $db->update('users', $data, 'user_id = ' . $_SESSION['user_id']);


            /// event
            if ($params['birth_date'] != '') {

                $event_datetmp = date('Y-m-d', strtotime($params['birth_date']));
                $edtArr = explode('-', $event_datetmp);
                $event_date = date('Y') . '-' . $edtArr[1] . '-' . $edtArr[2];

                $event_name = $params['first_name'] . ' ' . $params['last_name'] . ' - Birthday';
                $datetime = date("Y-m-d H:i:s");
                $event = array(
                    "created_user" => $_SESSION['user_id'],
                    "event_date" => $event_date,
                    "title" => $event_name,
                    //"description"=>$event_name,
                    "created_date" => $datetime,
                );
                $update_event = array(
                    "event_date" => $event_date,
                    "title" => $event_name,
                    //"description"=>$event_name,
                    "updated_date" => $datetime,
                );

                /// check if usre's birthday event already exist then update it
                $event_id = event::getExistBirthdayEvent($_SESSION['user_id'], date('Y'));

                if ($event_id) {
                    //update event
                    $db->update('events', $update_event, 'id = ' . $event_id);
                } else {
                    // create event
                    $db->insert('events', $event);
                }
                /// automatically invite friends in user's birthday event
                //get friends
                $friendlist = usersfriend::getFollowList($_SESSION['user_id']);
                //invite friend
                for ($f = 0; $f < count($friendlist); $f++) {
                    // check for already invited
                    $chkInvite = event::isCheckFriendEventInviteOpt($friendlist[$f]->friend_id, $event_id);
                    if (!$chkInvite) {
                        $invite_friend = array("user_id" => $_SESSION['user_id'], "invited_user" => $friendlist[$f]->friend_id, "event_id" => $event_id, "RSVP_status" => '3', 'notification_status' => 1);
                        $db->insert('eventrsvp', $invite_friend);
                    }
                }
            }/// event
            ///
            $db->commit();
            if ($result) {
                $msg = "Information has been updated successfully";
            } else {
                $msg = "Error in updating information, please try again later.";
            }
            $msg = "Information has been updated successfully" . '<br>' . $imgMsg;
            $this->view->msg = $msg;
        } catch (Exception $e) {
            //Rollback transaction
            $db->rollBack();
            $this->view->errors[] = $e->getMessage();
            echo $e->getMessage();
            exit;
        }
        $user_details = users::getUserDetails($_SESSION['user_id']);
        $this->view->userDetail = $user_details;

        $this->_helper->viewRenderer("profile");
        $this->_helper->resources("profile");
    }

    //validate profile page	
    protected function _initProfile() {

        $dob = new PS_Validate_NotEmpty($this->_getParam("birth_date"));
        $dob->setMessage(PS_Translate::_("Please enter date of birth"));

        $first_name = new PS_Validate_NotEmpty($this->_getParam("first_name"));
        $first_name->setMessage(PS_Translate::_("Please enter first name"));

        $last_name = new PS_Validate_NotEmpty($this->_getParam("last_name"));
        $last_name->setMessage(PS_Translate::_("Please enter last name"));

        $this->fields = new PS_Form_Fields(
                array(
            "first_name" => array(
                $first_name,
                "required" => true
            ),
            "last_name" => array(
                $last_name,
                "required" => true
            ),
            "birth_date" => array(
                array($dob, new PS_Validate_Birthdate()),
                "required" => true
            )
                )
        );
        $params = $this->getRequest()->getParams();
        $this->fields->setData($params);
        $this->view->fields = $this->fields;
    }

///end function
    // validate change password page
    protected function _initPassword() {
        $identical = new PS_Validate_Identical($this->_getParam("password"));
        $identical->setMessage(PS_Translate::_("Password Mismatch"));

        $this->fields = new PS_Form_Fields(
            array(
                "password" => array(
                    //$password,
                    new PS_Validate_Password(array("min" => 6, "digitRequired" => true)),
                    "required" => true
                ),
                "confirm_password" => array(
                    //$cpassword,
                    $identical,
                    "required" => true
                )
            )
        );
        $params = $this->getRequest()->getParams();
        $this->fields->setData($params);
        $this->view->fields = $this->fields;
    }

///end function
    //************* BOF update password *****************//
    public function updatepasswordAction() {
        $db = Zend_Registry::get("db");

        $params = $this->getRequest()->getParams();
        $this->_initPassword();
//        return $this->_helper->json($params);

        $user_details = users::getUserDetails($_SESSION['user_id']);
        $this->view->userDetail = $user_details;

        $data['password'] = md5($params['password']);
        $success = false;
        try {
            $db->beginTransaction();
            $this->fields->validate();
           /* if ((!empty($user_details->password)) && ($user_details->password != md5($params['old_password']))) {
                $msg = 'Old password does not match';
            } else {
            */
                $result = $db->update('users', $data, 'user_id = ' . $_SESSION['user_id']);
                // update cookie
                if (isset($_COOKIE['gift_password']) != '' && ($_COOKIE['gift_username'] == $user_details->email_id)) {
                    setcookie('gift_password', base64_encode($params['password']), time() + (60 * 60 * 24 * 7));
                    setcookie('gift_username', $user_details->email_id, time() + (60 * 60 * 24 * 7));
                }
                $msg = "Password has been updated successfully";
                $success = true;
          //  }
            $db->commit();
        } catch (Exception $e) {
            $db->rollBack();
            $msg = $e->getMessage();
        }
        return $this->_helper->json(array('success' => $success, 'msg' => $msg));
    }
    //************* EOF update password *****************//

//	
    //************* EOF update password *****************//
    //************* BOF update Notify settings *********//
    public function notificationAction() {
        $db = Zend_Registry::get("db");
        $config = Zend_Registry::get("config");
        $server = Zend_Registry::get("server");

        $params = $this->getRequest()->getParams();
        $this->_initProfile();
        if ($params['notify_event'] != '') {
            $data ['upcoming_event'] = $params['notify_event'];
        } else {
            $data ['upcoming_event'] = 0;
        }

        if ($params['shared_gift'] != '') {
            $data ['shared_gift'] = $params['shared_gift'];
        } else {
            $data ['shared_gift'] = 0;
        }
        if ($params['added_followers'] != '') {
            $data ['added_followers'] = $params['added_followers'];
        } else {
            $data ['added_followers'] = 0;
        }
        try {
            $db->beginTransaction();

            $result = $db->update('users', $data, 'user_id = ' . $_SESSION['user_id']);
            $db->commit();
            $msg = "Information has been updated successfully";
            return $this->_helper->json(array('success'=>true, 'msg' => $msg));
        } catch (Exception $e) {
            $db->rollBack();
            $this->view->errors[] = $e->getMessage();
            return $this->_helper->json(array('success'=>false, 'msg' => $e->getMessage()));
        }
    }

///
    //************* EOF update Notify settings *****************//

    public function fbnotificationAction() {
        $db = Zend_Registry::get("db");
        $config = Zend_Registry::get("config");
        $server = Zend_Registry::get("server");

        $params = $this->getRequest()->getParams();
        $this->_initProfile();
        /* if($params['notify_fb'] != '')
          {
          $data ['facebook_notification'] = $params['notify_fb'];
          }else{
          $data ['facebook_notification']  = 0;
          } */

        $data['profile_type'] = $params['profile_type'];
        $data['follow_permission'] = $params['follow_permission'];
        try {
            $db->beginTransaction();

            $result = $db->update('users', $data, 'user_id = ' . $_SESSION['user_id']);
            $db->commit();
            if ($result) {
                $msg = "Information has been updated successfully";
            } else {
                $msg = "Error in updating information, please try again later.";
            }
            $msg = "Information has been updated successfully";
            $this->view->msg = $msg;
        } catch (Exception $e) {
            $db->rollBack();
            $this->view->errors[] = $e->getMessage();
        }
        $user_details = users::getUserDetails($_SESSION['user_id']);
        $this->view->userDetail = $user_details;

        $this->_helper->viewRenderer("profile");
        $this->_helper->resources("profile");
    }

///

    public function forgotpasswordAction() {
        
    }

    public function forgotprocessAction() {
        $db = Zend_Registry::get("db");
        $config = Zend_Registry::get("config");
        $server = Zend_Registry::get("server");

        $params = $this->getRequest()->getParams();

        $email = $params['email'];
        $success = false;
        if (empty($email)) {
            $msg = 'Please enter your Email';
        } else {
            try {
                $db->beginTransaction();
                $emailExist = users::checkExistsEmail($email);
                //$email = 'anu.sagi03@gmail.com';
                if ($emailExist) {
                    ///generate random password
                    $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
                    $pass = array();
                    for ($i = 0; $i < 8; $i++) {
                        $n = rand(0, strlen($alphabet) - 1);
                        $pass[$i] = $alphabet[$n];
                    }
                    $password = implode($pass);
                    $data['password'] = md5($password);

                    $result = $db->update('users', $data, 'user_id = ' . $emailExist);

                    if ($result) {
                        //send email
                        $userName = users::getName($emailExist);

                        $mailText = 'Hi ' . $userName . ',<br>Your new password is - <b>' . $password . '</b>, please login with new password.
						<br><br>
						<a href="https://' . $server->apphost . '">Click here</a> to login.
                                                <br> Please use account settings options to change the pasword.
						<br><br> Thanks,<br>Giftvise';

                        $transport = new Zend_Mail_Transport_Sendmail($config->supportEmail);
                        Zend_Mail::setDefaultTransport($transport);
                        $mail = new Zend_Mail();
                        $mail->setSubject('Forgot Password - Giftvise');
                        $mail->setFrom($config->supportEmail, 'Giftvise');
                        $mail->addTo($email, $name);
                        //$mail->setBodyText($mailText);
                        $mail->setBodyHtml($mailText);
                        $mail->send();
                        $success = true;
                        $msg = 'An email has been sent to your email-id with the new password.';
                    } else {
                        $msg = 'Error in processing, please try again later.';
                    }
                } else {
                    $msg = 'Account does not exist';
                }
                $db->commit();
            } catch (Exception $e) {
                $db->rollBack();
                $this->view->errors[] = $e->getMessage();
            }
        }
        $this->_helper->json(array('success' => $success, 'msg' => $msg));
    }

    ///// activate account
    public function activateAction() {
        $db = Zend_Registry::get("db");
        $config = Zend_Registry::get("config");
        $server = Zend_Registry::get("server");
        $logger = Zend_Registry::get("logger");

        $params = $this->getRequest()->getParams();
        $user_id = $params['id'];
        
        $logger->info("Activating user with id = " . $user_id);

        $data['status'] = 0;

        try {
            if ($user_id != '') {
                $db->beginTransaction();
                $userobj = usersfriend::frienDeatil($user_id);
                if ($userobj != '') {
                    $result = $db->update('users', $data, 'user_id = ' . $userobj->user_id);
                    
                    $msg = 'Your account is now active. <br>Please sign-in with your email address and password.';
                    if ($result) {
                        //$msg = 'Your account is now active. Please sign-in with your email address and password.';
                    } else {
                        //$msg = "Error! please try again";
                    }
                    $mailMsg = '


<html><head>

<!-- Define Charset -->
        <meta http-equiv="Content-Type" content="text/html;" charset="UTF-8">
        
        <!-- Responsive Meta Tag -->
        <meta name="viewport" content="width=device-width;" initial-scale="1.0;" maximum-scale="1.0;">
        <link href="http://fonts.googleapis.com/css?family=Questrial" rel="stylesheet" type="text/css">
        
        <!--- Importing Twitter Bootstrap icons -->
        <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">

</head><body>
<div marginheight="0" marginwidth="0">
	<table style="background-color:rgb(21,18,18)" cellpadding="0" cellspacing="0" bgcolor="ecebeb" border="0" width="100%">
		<tbody><tr>
		<td>
				<table cellpadding="0" cellspacing="0" align="center" border="0" width="100%">
					<tbody><tr><td style="font-size:30px;line-height:30px" height="30"> </td></tr>
					<tr>
						<td>
                                                        <table style="border-collapse:collapse" cellpadding="0" cellspacing="0" align="left" border="0" width="45%">
								<tbody><tr>
									<td align="center">
										<table cellpadding="0" cellspacing="0" align="center" border="0">
											<tbody><tr>
												<td style="line-height:21px" align="center">

	<a href="https://giftvise.com/" style="display:block;border-style:none!important;border:0!important" target="_blank"><img style="display:block;width:128px" src="https://www.giftvise.com/newdesign/img/tpl/new-giftvise-logo.png" width="180"></a>
												</td>			
											</tr>
										</tbody></table>		
									</td>
								</tr>
								
							</tbody></table>
							
							<table style="border-collapse:collapse" cellpadding="0" cellspacing="0" align="left" border="0" width="5px">
								<tbody><tr><td style="font-size:20px;line-height:20px" height="20" width="5px"> </td></tr>
							</tbody></table>
							
							<table style="border-collapse:collapse" cellpadding="10" cellspacing="0" align="right" border="0" width="30%">	
								<tbody><tr>
									<td>
										<table style="border-collapse:collapse" cellpadding="0" cellspacing="0" align="left" border="0">
			
				                			<tbody><tr>
				                				<td>
				                					<table style="border-collapse:collapse" cellpadding="0" cellspacing="0" align="center">
				                						<tbody><tr>
				                							
							                				<td style="color:#ffffff;font-size:14px;font-family:\'Questrial\',Helvetica,sans-serif;line-height:24px" align="center">
							                					<div style="line-height:24px">
							                						<span>
<a href="https://'.$server->apphost.'/modals/modalshowhelpcenter?tab=about" style="color:#878b99;text-decoration:none">About</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="https://'.$server->apphost.'/modals/modalshowhelpcenter?tab=faq" style="color:#878b99;text-decoration:none">Help</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="https://'.$server->apphost.'/modals/modalshowhelpcenter?tab=contact" style="color:#878b99;text-decoration:none">Contact</a>
								                					
							                						</span>
							                					</div>	
							                				</td>
				                						</tr>
				                					</tbody></table>
				                				</td>
				                			</tr>
			                			</tbody></table>
									</td>
								</tr>	
							</tbody></table>	
						</td>
					</tr>
					
					<tr><td style="font-size:25px;line-height:25px" height="25"> </td></tr>
					<tr>
						<td style="line-height:21px" align="center">
						</td>			
					</tr>
					
					<tr><td style="font-size:50px;line-height:50px" height="50"> </td></tr>
					
					<tr>
						<td style="color:#ffffff;font-size:40px;font-family:\'Questrial\',Helvetica,sans-serif;line-height:28px" align="center">
							
							
							
							<div style="line-height:28px">
								<span>Welcome to <span>Gift</span>vise!</span>
							</div>
        				</td>
					</tr>
				        <tr><td style="font-size:25px;line-height:25px"  height="25" > </td></tr>	
					<tr>
						<td align="center">
							<table cellpadding="0" cellspacing="0" align="center" border="0" width="540">
								<tbody><tr>
									
									<td style="color:#878b99;font-size:18px;font-family:\'Questrial\',Helvetica,sans-serif;line-height:30px" align="center">
										<div style="line-height:30px">
											
											
											<span>The perfect way to give and receive the right gift for the right event</span>
										</div>
			        				</td>	
								</tr>
							</tbody></table>
						</td>
					</tr>
					
					<tr><td style="font-size:35px;line-height:35px" height="35"> </td></tr>
					
				</tbody></table>
			</td></tr>
		
	</tbody></table>
	
<!--	
	<table cellpadding= 0  cellspacing= 0  bgcolor= 363b49  border= 0  width= 100% >
							
		<tbody><tr><td style= font-size:20px;line-height:20px  height= 20 > </td></tr>
					
		<tr>
			<td align= center >
				
				<table cellpadding= 0  cellspacing= 0  align= center  border= 0  width= 590 >
					
					<tbody><tr>
						<td align= center >
								                		
	                		<table cellpadding= 0  cellspacing= 0  align= center  border= 0 >
	                			<tbody><tr>
	                				<td style= color:#5a5f70;font-size:14px;font-family:&#39;Questrial&#39;,Helvetica,sans-serif;line-height:25px  align= center >
	                					<div style= line-height:25px >
	                						<span>
		                					
		                						Copyright @ Giftvise 2014
		                					
	                						</span>
	                					</div>	
	                				</td>
	                			</tr>
	                			
                			</tbody></table>
                			
						</td>
					</tr>
					
				</tbody></table>
			</td>
		</tr>
		<tr><td style= font-size:20px;line-height:20px  height= 20 > </td></tr>
	</tbody></table>
-->
	
	
	<table class="bg2_color" style="background-color: rgb(255, 255, 255);" width="100%" bgcolor="f3f4f5" border="0" cellpadding="10" cellspacing="0">
		<tbody><tr>
			<td>
       			<table class="container590" width="100%" align="center" border="0" cellpadding="0" cellspacing="0">

					
					<tbody>
					<tr><td style="font-size: 40px; line-height: 40px;" height="40">&nbsp;</td></tr>
					
					<tr>
						<td style="color: rgb(86, 94, 120); font-size: 28px; font-family: \'Questrial\',Helvetica,sans-serif; line-height: 22px;" class="title_color" align="center">
							<!-- ======= section text ====== -->
							
							<div class="editable_text" style="line-height: 22px">
								<span class="text_container">Happy gifting!</span>
							</div>
        				</td>	
					</tr>
					
					<tr><td style="font-size: 20px; line-height: 20px;" height="20">&nbsp;</td></tr>
					
					<tr>
						<td align="center">
							<table class="main_color" style="border-radius: 4px;" width="24" align="center" bgcolor="f06e6a" border="0" cellpadding="0" cellspacing="0">
								<tbody><tr><td style="font-size: 4px; line-height: 4px;" height="4">&nbsp;</td></tr>
							</tbody></table>
						</td>
					</tr>
					
					<tr><td style="font-size: 20px; line-height: 20px;" height="20">&nbsp;</td></tr>
					
					<tr>
						<td align="center">
							<table class="container580" width="580" align="center" border="0" cellpadding="0" cellspacing="0">
								<tbody><tr>
									<td style="color: #878b99; font-size: 14px; font-family: \'Questrial\', Helvetica, sans-serif; line-height: 24px;" class="text_color" align="center">
										<!-- ======= section subtitle ====== -->
										
										<div class="editable_text" style="line-height: 24px;">
											<span class="text_container">Thank you for signing up for 
											Giftvise, the perfect way to give and receive the right gift for the 
											right event. We\'re ecstatic you decided to join us and are dedicated to 
											making
											your experience a most enjoyable one. If you like what you see, be sure 
											to tell your friends. The more friends you tell, the more valuable 
											the service is for you and everyone else.</span>
										</div>
			        				</td>
								</tr>
							</tbody></table>
						</td>
					</tr>
					
					<tr class="hide"><td style="font-size: 30px; line-height: 30px;" height="30">&nbsp;</td></tr>
					
						<tr><td align="center">
							
							<table class="main_color main-button" style="border-radius: 50px;" width="240" align="center" bgcolor="f06e6a" border="0" cellpadding="0" cellspacing="0">
								
								<tbody><tr><td style="font-size: 18px; line-height: 18px;" height="18">&nbsp;</td></tr>
								
								<tr>
	                				<td style="color: #ffffff; font-size: 18px; font-family: \'Questrial\', Helvetica, sans-serif;" align="center">
	                					<!-- ======= main section button ======= -->
	                					
		                    			<div class="editable_text" style="line-height: 24px;">
		                    				<span class="text_container">
			                    			<a href="https://www.giftvise.com/newfriends" style="color: #ffffff; text-decoration: none;">Invite friends</a> 
		                    				</span>
										</div>
		                    		</td>								
								</tr><tr><td style="font-size: 48px; line-height: 18px;" height="18">&nbsp;</td></tr>
							</tbody></table>
						</td></tr>
					
					<tr><td style="font-size: 20px; line-height: 20px;" height="40">&nbsp;</td></tr>
					
					<tr>
							
					</tr>
				</tbody></table>
			</td>
		</tr>		
	</tbody></table>
	<!-- ======= end section ======= -->
	
	
		<!-- ======= features section ======= -->
<table border="0" width="100%" cellpadding="0" cellspacing="0" bgcolor="f3f4f5" class="bg_color">
                <tbody><tr>
                        <td>
                                <table border="0" align="center" width="100%" cellpadding="0" cellspacing="0" class="container590 bg_color">
                                        
                                        <tbody><tr class="hide"><td height="30" style="font-size: 30px; line-height: 30px;">&nbsp;</td></tr>
                                        <tr><td height="50" style="font-size: 50px; line-height: 50px;">&nbsp;</td></tr>
                                        
                                        <tr>
                                                <td align="center" style="color: #414655; font-size: 28px; font-family: \'Questrial\', Helvetica, sans-serif; line-height: 22px;" class="title_color">
                                                        <!-- ======= section text ====== -->
                                                        
                                                        <div style="line-height: 22px">
                                                        
                                                        Gifting made simple with Giftvise
                                                        
                                                        </div>
                                        </td>   
                                        </tr>
                                        
                                        <tr><td height="20" style="font-size: 20px; line-height: 20px;">&nbsp;</td></tr>
                                        
                                        <tr>
                                                <td align="center">
                                                        <table border="0" width="24" align="center" cellpadding="0" cellspacing="0" bgcolor="f06e6a" style="border-radius: 4px;">
                                                                <tbody><tr><td height="4" style="font-size: 4px; line-height: 4px;">&nbsp;</td></tr>
                                                        </tbody></table>
                                                </td>
                                        </tr>
                                        
                                        <tr><td height="20" style="font-size: 20px; line-height: 20px;">&nbsp;</td></tr>
                                        
                                        <tr>
                                                <td align="center">
                                                        <table border="0" width="100%" align="center" cellpadding="0" cellspacing="0" class="container580">
                                                                <tbody><tr>
                                                                        <td align="center" style="color: #878b99; font-family: \'Questrial\', Helvetica, sans-serif; line-height: 24px;" class="text_color">
                                                                                <!-- ======= section subtitle ====== -->
                                                                                
                                                                <div style="line-height: 24px;">
                                                                               <div style="color: #414655;font-size: 20px;">            
                                                                                Discover new products
                                                                               </div>
                                                                         <div style="font-size: 14px;">
                                                                           Explore curated products, see what\'s trending, and get recommendations from friends and family
                                                                        </div>  
                                                               </div>
                                                                <br>
                                                                <div style="line-height: 24px;">
                                                                               <div style="color: #414655;font-size: 20px;">            
                                                                               Create a universal wishlist on-the-go 
                                                                               </div>
                                                                         <div style="font-size: 14px;">
                                                                           Create a single wishlist for all your events with products from any retailer using any device
                                                                        </div>  
                                                                </div><br>
                                                                <div style="line-height: 24px;">
                                                                               <div style="color: #414655;font-size: 20px;">            
                                                                               Give and receive the perfect gift 
                                                                               </div>
                                                                         <div style="font-size: 14px;">
                                                                           Reserve the perfect gift for your friends and family. They just may return the favor!
                                                                         </div> 
                                                                </div>
                                                                </td>
                                                                </tr>
                                                        </tbody></table>
                                                </td>
                                        </tr>
                                        
                                        <tr class="hide"><td height="30" style="font-size: 30px; line-height: 30px;">&nbsp;</td></tr>
                                        <tr><td height="50" style="font-size: 50px; line-height: 50px;">&nbsp;</td></tr>
                                        
                                </tbody></table>
                        </td>
                </tr>
        </tbody></table>

		<!-- ======= end section ======= -->
	
	
	<!-- ======= 1/2 text 1/2 bg image ======= -->
	
	<!-- ======= end section ======= -->
	
	
	<!-- ======= 2 columns testimonials ======= -->
	
	<!-- ======= end section ======= -->
	
	
	<!-- ======= 2 columns image and text ======= -->
	
	<!-- ======= end section ======= -->
	
	
	<!-- ======= section with big button ====== -->
	
	<!-- ======= end section ======= -->
	
	
	<!-- ======= 1/2 text 1/2 image ======= -->
	
	<!-- ======= end section ======= -->

	
	<!-- ======= gallery section ======= -->
	
	<!-- ======= end section ======= -->

	
	<!-- ======= CTA section ======= -->
	
	<!-- ======= end main section ======= -->

	
	<!-- ======= 2 columns headline and text ======= -->
	
	<!-- ======= end section ======= -->
	
	
	<!-- ======= large headline and text ======= -->
	
	<!-- ======= end section ======= -->
	
 <!-- ======= contact section ======= -->
<table style="background-color:rgb(21,18,18)" cellpadding="0" cellspacing="0" bgcolor="ecebeb" border="0" width="100%">

 <tbody><tr><td style="font-size: 20px; line-height: 20px;" height="20">&nbsp;</td></tr>

<tr> <td> <table class="container590" width="90%" align="center" border="0" cellpadding="0" cellspacing="0">

 <tbody><tr> <td> <table style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="container590" width="35%" align="left" border="0" cellpadding="0" cellspacing="0">

 <tbody><tr> <td style="color: #ffffff; font-size: 18px; font-family: \'Questrial\', Helvetica, sans-serif; line-height: 22px;" class="white_color" align="right"> 
         <!-- ======= section text ====== -->

 <div class="editable_text" style="line-height: 22px"> <span class="text_container">

 Follow us

 </span> </div> </td> </tr>

 

 <tr>  </tr>

 

 <tr> <td align="left"> <table align="right" border="0" cellpadding="0" cellspacing="0"> <tbody><tr>  <td> <a style="display: block; width: 30px;
height: 30px; border-style: none !important; border: 0 !important;" href="https://www.facebook.com/Giftvise" class="editable_img"><img style="display:
block; width: 30px; height: 30px;" src="https://giftvise.com/newdesign/img/tpl/SM_icons_48x48-02_FB.png" alt="facebook" class="" height="30" width="30" border="0"></a> </td> <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td> <td> <a style="display:
block; width: 30px; height: 30px; border-style: none !important; border: 0 !important;" href="https://www.twitter.com/giftvise" class="editable_img"><img style="display: block; width: 30px; height: 30px; align=left" src="https://giftvise.com/newdesign/img/tpl/SM_icons_48x48-03_TW.png" alt="twitter" height="30" width="30" border="0"></a> </td> </tr>
<tr><td style="font-size: 15px; line-height: 15px;" height="20">&nbsp;</td></tr>
 </tbody></table> </td> </tr>
</tbody></table>

 <table style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="container590" width="30%" align="right" border="0" cellpadding="0" cellspacing="0">

 <tbody><tr> <td style="color: #ffffff; font-size: 18px; font-family: \'Questrial\', Helvetica, sans-serif; line-height: 22px;" class="white_color" align="left"> 
         <!-- ======= section text ====== -->

 <div class="editable_text" style="line-height: 22px"> <span class="text_container">

 Get in touch
</span> </div> </td> </tr>

 <tr> <td style="color: #878b99; font-size: 14px; font-family: \'Questrial\', Helvetica, sans-serif; line-height: 25px;" class="text_color" align="left"> 
         <!-- ======= section subtitle ====== -->

 <div class="editable_text" style="line-height: 25px;"> <span class="text_container">feedback @ giftvise.com<br></span> </div> </td> </tr>

 </tbody></table> </td> </tr> </tbody></table> </td> </tr>

 </tbody></table>

<!-- copy right secion -->
<table border="0" width="100%" cellpadding="0" cellspacing="0" bgcolor="#ffffff" class="bg4_color">

 <tbody><tr><td height="20" style="font-size:20px;line-height:20px;">&nbsp;</td></tr>

 <tr> <td align="center">

 <table border="0" align="center" width="590" cellpadding="0" cellspacing="0" class="container590">

 <tbody><tr> <td align="center">

 <table border="0" align="center" cellpadding="0" cellspacing="0"> <tbody><tr> 
<td style="color:#5a5f70;font-size:14px;font-family:\'Questrial\',Helvetica,sans-serif;line-height:25px" align="center">
<div class="editable_text" style="line-height:25px;"> <span class="text_container">

 Copyright @ Giftvise 2014

 </span> </div> </td> </tr>

 </tbody></table>

 </td> </tr>

 </tbody></table> </td> </tr>

 <tr><td height="20" style="font-size:" 20px;="" line-height:="">&nbsp;</td></tr>

 </tbody></table>
</div>


</body></html>

                    ';

                    $transport = new Zend_Mail_Transport_Sendmail($config->supportEmail);
                    Zend_Mail::setDefaultTransport($transport);
                    $mail = new Zend_Mail();
                    $mail->setSubject('Welcome to Giftvise!');
                    $mail->setFrom($config->supportEmail, 'Giftvise');
                    $mail->addTo($userobj->email_id, $name);
                    $mail->setBodyHtml($mailMsg);
                    $mail->send();
                } else {
                    $msg = "Incorrect activation link";
                }

                $this->view->msg = $msg;
                $db->commit();
                /* redirect to login page */
                //$this->_redirect(PS_Util::redirect("https://" . $server->apphost. "/?reg_confirm=true"));



                $this->_redirect(PS_Util::redirect("https://" . $server->apphost. "/modals/modalshowhelpcenter?tab=register"));
            } else {
                $this->view->msg = 'Incorrect link';
                $this->_redirect(PS_Util::redirect("https://" . $server->apphost));
            }
        } catch (Exception $e) {
            //Rollback transaction
            $db->rollBack();
            echo $e->getMessage();
            $logger->info($e->getTraceAsString());
        }
    }

    ////
    public function loaddeleteAction() {
        $db = Zend_Registry::get("db");
        $config = Zend_Registry::get("config");
        $server = Zend_Registry::get("server");

        $params = $this->getRequest()->getParams();
        $user_id = $params['id'];

        $data['status'] = 0;

        try {
            $products = wishlistitmes::getAllProducts($_SESSION['user_id'], '', '');

            for ($i = 0; $i < count($products); $i++) {
                $item = array();

                $totalComments = itemcomments::countComments($products[$i]->item_id);
                $userwished = wishlistitmes::countWished($products[$i]->item_id);

                /// latest item
                $timeAdded = wishlistitmes::latestItem($products[$i]->item_id);
                $addeduser = users::getUserDetails($timeAdded['user_id']);

                $item['comment'] = $totalComments;
                $item['wished'] = $userwished;
                $item['added_time'] = $timeAdded['time'];
                $item['addedby'] = $addeduser->first_name . ' ' . $addeduser->last_name;
                $items[] = $item;
            }
            $this->view->items = $products;
            $this->view->extval = $items;
        } catch (Exception $e) {
            //Rollback transaction
            $db->rollBack();
            echo $e->getMessage();
        }
    }

    //after deleting an item reload updated items on my profile page
    public function loaddelete1Action() {
        $db = Zend_Registry::get("db");
        $config = Zend_Registry::get("config");
        $server = Zend_Registry::get("server");

        $params = $this->getRequest()->getParams();
        $user_id = $params['id'];

        $data['status'] = 0;

        try {
            $products = wishlistitmes::getAllProducts($_SESSION['user_id'], '', '');

            $result = '<ul>';
            if (count($products) > 0) {
                for ($i = 0; $i < count($products); $i++) {
                    $item = array();

                    $totalComments = itemcomments::countComments($products[$i]->item_id);
                    $userwished = wishlistitmes::countWished($products[$i]->item_id);

                    /// latest item
                    $timeAdded = wishlistitmes::latestItem($products[$i]->item_id);
                    $addeduser = users::getUserDetails($timeAdded['user_id']);

                    $item['comment'] = $totalComments;
                    $item['wished'] = $userwished;
                    $item['added_time'] = $timeAdded['time'];
                    $item['addedby'] = $addeduser->first_name . ' ' . $addeduser->last_name;
                    $items[] = $item;

                    ////
                    if ($products[$i]->item_id != '') {

                        $result .= '<li class="ab"><div class="cross"></div> 
						<div class="mosaic-block bar" >  
							<div class="details mosaic-overlay">
							 <p style="float:left; width:100px;">';


                        if (strlen($products[$i]->item_name) > 15) {
                            $name = substr($products[$i]->item_name, 0, 10) . '...';
                        } else {
                            $name = $products[$i]->item_name;
                        }



                        $result .= $name . ' </p>	           <p style="float:right"> $<?php $this->items[$i]->item_price?></p>
			 
		  </div>
		  <a href="javascript:void(0);" class="cross" onClick="deletewish(' . $products[$i]->item_id . ',' . $_SESSION['user_id'] . ')">&nbsp;</a>
			 <div class="mosaic-backdrop" >';

                        if ($products[$i]->image != '') {

                            $result .= '<a class="iframe " href="https://' . $server->apphost . '/products/productdetail?pid=' . $products[$i]->item_id . '" title="">
					<img src="https://' . $server->apphost . '/productimg/thumb/' . $products[$i]->image . '" alt="" >
				</a>';
                        } else {

                            $result .= '<a class="iframe " href="https://' . $server->apphost . '/products/productdetail?pid=' . $products[$i]->item_id . '" title="">
			 <img src="https://' . $server->apphost . '/productimg/no-img.png" alt="">
			 </a>';
                        }

                        $result .= '</div>
			 
		  </div>
		<div class="details">
		<span class="member">' . $userwished . '</span>
		 <span class="Chat">' . $totalComments . '</span>
		  <span class="profiledroupdown">
			 <ul>
			   <li>
				<span class="nav"><span></span></span><a href="#"><img src="https://' . $server->apphost . '/images/more.png"  alt=""></a>
				 <ul>
				   <li><a href="#">Group</a></li>
				   <li><a href="#">Private</a></li>
				   <li><a href="#">Public</a></li>
				  </ul>
			 </li>
			 </ul>   </span>
			</div>
		</li>';
                    }    //
                }
            } else {
                $result .= '<li style="width:90%; min-height:300px;" >
				<div >There are no items in your wishlists.</div>
				</li>';
            }

            $result .= '</ul>';
        } catch (Exception $e) {
            //Rollback transaction
            $db->rollBack();
            echo $e->getMessage();
        }
        echo $result;
        exit;
    }

    //// set privacy to show wishlist
    public function updateprivacysetingAction() {
        $db = Zend_Registry::get("db");
        $config = Zend_Registry::get("config");
        $server = Zend_Registry::get("server");

        $params = $this->getRequest()->getParams();

        $shareType = $params['share_type'];
        $data = array();
        $newGroups = '';

        try {
            $db->beginTransaction();
            if ($shareType == 'Public' || $shareType == 'Private') {
                if ($shareType == 'Public' && $params['privacy_val'] == '1') {
                    $privacyVal = '1';
                } elseif ($shareType == 'Private' && $params['privacy_val'] == '0') {
                    $privacyVal = '0';
                    /* privcayval - 0 shows that this item is shared only for me, therefore other options should be blank
                      remove share with friend option, share_individual coulmn should be blank
                      remove share with group option, sharewith_group coulmn should be blank
                     */
                    $data['sharewith_group'] = '';
                    $data['share_individual'] = '';
                } else {
                    $privacyVal = '0';
                    $data['sharewith_group'] = '';
                    $data['share_individual'] = '';
                }
                /// update database
                $data['public'] = $privacyVal;
                $res = $db->update('userwishlist', $data, array("user_id = ?" => $_SESSION['user_id'], "item_id = ?" => $params['item_id']));


                $db->commit();
            }/// public/private
            /// share with Group			
            if ($shareType == 'Groups') {
                $groupID = $params['group_id'];
                $processType = $params['process_type'];
                $wishedItemDetail = wishlistitmes::getUserWishesItem($params['item_id'], $_SESSION['user_id']);
                //

                $alreadyinGroup = $wishedItemDetail->sharewith_group;

                if ($processType == 'add') {
                    if ($alreadyinGroup != '') {
                        $alreadyinGroupArr = explode(',', $alreadyinGroup);
                        if (in_array($groupID, $alreadyinGroupArr)) {
                            // this group is exist so no need to add
                            $newGroups = $alreadyinGroup;
                        } else {
                            //$alreadyinGroupArr[] = $groupID;
                            $newGroups = $alreadyinGroup . ',' . $groupID;
                            // update Database
                        }
                    } else {
                        $newGroups = $groupID;
                    }
                }
                //
                if ($processType == 'remove') {
                    $alreadyinGroupArr = explode(',', $alreadyinGroup);

                    $key = array_search($groupID, $alreadyinGroupArr);
                    if ($key !== false) {
                        unset($alreadyinGroupArr[$key]);
                    }
                    $newGroups = implode(',', $alreadyinGroupArr);
                }

                $data['sharewith_group'] = $newGroups;
                $data['public'] = '';
                $data['share_individual'] = '';
                $res = $db->update('userwishlist', $data, array("user_id = ?" => $_SESSION['user_id'], "item_id = ?" => $params['item_id']));

                $db->commit();
            }// groups

            if ($shareType == 'Friend') {
                $friend_id = $params['friend_id'];
                $processType = $params['process_type'];
                $wishedItemDetail = wishlistitmes::getUserWishesItem($params['item_id'], $_SESSION['user_id']);
                //

                $sharedfriends = $wishedItemDetail->share_individual;

                if ($processType == 'add') {
                    if ($sharedfriends != '') {
                        $alreadyinFriendArr = explode(',', $sharedfriends);
                        if (in_array($friend_id, $alreadyinFriendArr)) {
                            // this group is exist so no need to add
                            $newFriedsids = $sharedfriends;
                        } else {
                            //$alreadyinGroupArr[] = $groupID;
                            $newFriedsids = $sharedfriends . ',' . $friend_id;
                            // update Database
                        }
                    } else {
                        $newFriedsids = $friend_id;
                    }
                }
                //
                if ($processType == 'remove') {
                    $alreadyinFriendArr = explode(',', $sharedfriends);

                    $key = array_search($friend_id, $alreadyinFriendArr);
                    if ($key !== false) {
                        unset($alreadyinFriendArr[$key]);
                    }
                    $newFriedsids = implode(',', $alreadyinFriendArr);
                }

                $data['share_individual'] = $newFriedsids;
                $data['public'] = '';
                $data['sharewith_group'] = '';

                $res = $db->update('userwishlist', $data, array("user_id = ?" => $_SESSION['user_id'], "item_id = ?" => $params['item_id']));

                $db->commit();
            }// groups
            /// if no privacy item is selected then set 'Only Me' option
            $wishedItemPrivacy = wishlistitmes::getUserWishesItem($params['item_id'], $_SESSION['user_id']);

            if ($wishedItemPrivacy->share_individual == '' && $wishedItemPrivacy->sharewith_group == '' && $wishedItemPrivacy->public == '') {
                $dataP['share_individual'] = '';
                $dataP['public'] = 0;
                $dataP['sharewith_group'] = '';

                $res = $db->update('userwishlist', $dataP, array("user_id = ?" => $_SESSION['user_id'], "item_id = ?" => $params['item_id']));
                //$db->commit();
            }
            ///
            //// populate share dropdown
            $products = wishlistitmes::getAllProducts($_SESSION['user_id'], '', '');

            $result = '<ul>';
            if (count($products) > 0) {
                for ($i = 0; $i < count($products); $i++) {
                    $item = array();

                    $totalComments = itemcomments::countComments($products[$i]->item_id);
                    $userwished = wishlistitmes::countWished($products[$i]->item_id);

                    /// latest item
                    $timeAdded = wishlistitmes::latestItem($products[$i]->item_id);
                    $addeduser = users::getUserDetails($timeAdded['user_id']);

                    $item['comment'] = $totalComments;
                    $item['wished'] = $userwished;
                    $item['added_time'] = $timeAdded['time'];
                    $item['addedby'] = $addeduser->first_name . ' ' . $addeduser->last_name;
                    $items[] = $item;

                    ////
                    if ($products[$i]->item_id != '') {

                        $result .= '<li class="ab"><div class="cross"></div> 
						<div class="mosaic-block bar" >  
							<div class="details mosaic-overlay">
							 <p style="float:left; width:100px;">';


                        if (strlen($products[$i]->item_name) > 15) {
                            $name = substr($products[$i]->item_name, 0, 10) . '...';
                        } else {
                            $name = $products[$i]->item_name;
                        }



                        $result .= $name . ' </p>	           <p style="float:right"> $' . number_format($products[$i]->item_price, 2) . '</p>
			 
		  </div>
		  <a href="javascript:void(0);" class="cross" onClick="deletewish(' . $products[$i]->item_id . ',' . $_SESSION['user_id'] . ')">&nbsp;</a>
			 <div class="mosaic-backdrop" >';

                        if ($products[$i]->image != '') {

                            $result .= '<a class="iframe " href="https://' . $server->apphost . '/products/productdetail?pid=' . $products[$i]->item_id . '" title="">
					<img src="https://' . $server->apphost . '/productimg/thumb/' . $products[$i]->image . '" alt="" >
				</a>';
                        } else {

                            $result .= '<a class="iframe " href="https://' . $server->apphost . '/products/productdetail?pid=' . $products[$i]->item_id . '" title="">
			 		<img src="https://' . $server->apphost . '/productimg/no-img.png" alt="">
				 </a>';
                        }

                        $result .= '</div>
			 
		  </div>
		  <div class="details">
		  
		<span class="member">' . $userwished . '</span>
		 <span class="Chat">' . $totalComments . '</span>';
                        //* add share option in list
                        $usersgroups = groups::groupList($_SESSION['user_id']);
                        $followingList = usersfriend::getFollowList($_SESSION['user_id']);

                        $shareStr = '';
                        $shareStr = '<span class="multiDrop">
                <a href="javascript:void(0)"><img src="https://' . $server->apphost . '/images/more.png"  alt=""></a>
                <ul class="firstLavel">';

                        if ($products[$i]->public == '1' && $products[$i]->sharewith_group == '' && $products[$i]->share_individual == '') {
                            $shareCss = 'class="active"';
                            $publiVal = 0;
                        } else {
                            $shareCss = '';
                            $publiVal = 1;
                        }

                        if ($products[$i]->sharewith_group != '') {
                            $gshareCss = 'class="active"';
                        } else {
                            $gshareCss = '';
                        }

                        if ($products[$i]->share_individual != '') {
                            $IshareCss = 'class="active"';
                        } else {
                            $IshareCss = '';
                        }

                        $shareStr .= '<li ' . $shareCss . ' id="public_' . $products[$i]->item_id . '" ><a href="javascript:void(0)" onClick="sharewishlist(\'Public\', ' . $products[$i]->item_id . ', \'\', \'\',\'public_' . $products[$i]->item_id . '\', ' . $publiVal . ')">Everyone</a> </li>
                   <li ' . $gshareCss . '><a href="javascript:void(0)" >Group</a>';

                        if ($dropCnt == 2) {
                            $dropCss = 'style="left:-186px;"';
                            $dropCnt = 0;
                        } else {
                            $dropCss = '';
                            $dropCnt++;
                        }
                        $itemGroupsArr = explode(',', $products[$i]->sharewith_group);
                        $itemFriendsArr = explode(',', $products[$i]->share_individual);


                        $shareStr .= '<ul class="secondLavel" ' . $dropCss . '> ';

                        if (count($usersgroups) > 0) {
                            for ($g = 0; $g < count($usersgroups); $g++) {

                                if (in_array($usersgroups[$g]->id, $itemGroupsArr)) {
                                    $shareCss = 'class="active"';
                                    $publiVal = 0;
                                } else {
                                    $shareCss = '';
                                    $publiVal = 1;
                                }

                                $shareStr .= '<li ' . $shareCss . ' id="group_' . $products[$i]->item_id . '_' . $usersgroups[$g]->id . '" >';

                                if ($usersgroups[$g]->group_image != '') {

                                    $shareStr .= '<img border="0" src="https://' . $server->apphost . '/groupimg/thumb/' . $usersgroups[$g]->group_image . '" width="32" height="32">';
                                } else {

                                    $shareStr .= '<img border="0" src="https://' . $server->apphost . '/images/icon.jpg" border="0" width="32">';
                                }

                                $shareStr .= '<div class="slcontent"><a href="javascript:void(0)" onClick="sharewishlist(\'' . Groups . '\', \'' . $products[$i]->item_id . '\', \'' . $usersgroups[$g]->id . '\', \'\',\'group_' . $products[$i]->item_id . '_' . $usersgroups[$g]->id . '\',' . $publiVal . ')">' . $usersgroups[$g]->group_name . '</a></div>
                            </li>';
                            }
                        } else {

                            $shareStr .= '<li>You have not created any group</li>';
                        }
                        $shareStr .= '</ul>                   
                   			</li>';

                        $shareStr .= '<li ' . $IshareCss . '><a href="javascript:void(0)">Individual</a>
                        <ul class="secondLavel" id="share_' . $products[$i]->item_id . '" 
						' . $dropCss . '>';

                        if (count($followingList) > 0) {
                            for ($f = 0; $f < count($followingList); $f++) {
                                $follower_details = users::getUserDetails($followingList[$f]->friend_id);


                                if (in_array($follower_details->user_id, $itemFriendsArr)) {
                                    $shareCss = 'class="active"';
                                    $publiVal = 0;
                                } else {
                                    $shareCss = '';
                                    $publiVal = 1;
                                }

                                $shareStr .= '<li ' . $shareCss . ' id="friend_' . $products[$i]->item_id . '_' . $follower_details->user_id . '" >';

                                if ($follower_details->profile_pic != '') {

                                    $shareStr .= '<img src="https://' . $server->apphost . '/userpics/thumb/' . $follower_details->profile_pic . '" width="32">';
                                } else {

                                    $shareStr .= '<img src="https://' . $server->apphost . '/userpics/top-img.png">';
                                }
                                $shareStr .= '<div class="slcontent"><a href="javascript:void(0)" onClick="sharewishlist(\'Friend\', \'' . $products[$i]->item_id . '\', \'\', \'' . $follower_details->user_id . '\',\'friend_' . $products[$i]->item_id . '_' . $follower_details->user_id . '\', ' . $publiVal . ')">' . $follower_details->first_name . ' ' . $follower_details->last_name . '</a></div></li>';
                            }
                        } else {

                            $shareStr .= '<li>You do not have any friend</li>';
                        }

                        $shareStr .= '</ul>
                    </li>';
                        if ($products[$i]->public == '0')
                            $onlyChk = 'checked = "checked"';
                        else
                            $onlyChk = '';
                        if ($products[$i]->public == '0') {
                            $shareCss = 'class="active"';
                            $publiVal = 0;
                        } else {
                            $shareCss = '';
                            $publiVal = 1;
                        }
                        $shareStr .= '<li ' . $shareCss . ' id="private_' . $products[$i]->item_id . '"><a href="" onClick="sharewishlist(\'Private\', \'' . $products[$i]->item_id . '\', \'\', \'\', \'private_' . $products[$i]->item_id . '\', ' . $publiVal . ')">Only me</a></li>              
                </ul>
            </span>';
                        /* $shareStr = '<span class="multiDrop">
                          <a href="#"><img src="https://'.$server->apphost.'/images/more.png"  alt=""></a>
                          <ul class="firstLavel">';

                          if($products[$i]->public == '1' && $products[$i]->sharewith_group == '' && $products[$i]->share_individual == '')
                          $checked = 'checked = "checked"';
                          else
                          $checked = '';

                          $shareStr .= '<li class="flabel">Everyone <input type="checkbox" id="public_'.$products[$i]->item_id.'" value="1" onClick="sharewishlist(\'Public\', '.$products[$i]->item_id.', \'\', \'\',\'public_'.$products[$i]->item_id.'\')"   /></li>
                          <li><a href="#">Group</a>';

                          if($dropCnt == 2)
                          {
                          $dropCss = 'style="left:-186px;"';
                          $dropCnt = 0;
                          }else{
                          $dropCss = '';
                          $dropCnt++;

                          }
                          $itemGroupsArr = explode(',',$products[$i]->sharewith_group);
                          $itemFriendsArr = explode(',',$products[$i]->share_individual);


                          $shareStr .= '<ul class="secondLavel" '.$dropCss.'> ';

                          if(count($usersgroups) > 0)
                          {
                          for($g=0; $g < count($usersgroups); $g++)
                          {
                          if(in_array($usersgroups[$g]->id, $itemGroupsArr ))
                          $groupChk = 'checked = "checked"';
                          else
                          $groupChk = '';

                          $shareStr .= '<li><input type="checkbox" id="group_'.$products[$i]->item_id.'_'.$usersgroups[$g]->id.'" value="'.$usersgroups[$g]->id.'"  onClick="sharewishlist(\''.Groups.'\', \''.$products[$i]->item_id.'\', \''.$usersgroups[$g]->id.'\', \'\',\'group_'.$products[$i]->item_id.'_'.$usersgroups[$g]->id.'\')" '.$groupChk.' />';

                          if($usersgroups[$g]->group_image != '')
                          {

                          $shareStr .= '<img border="0" src="https://'.$server->apphost.'/groupimg/thumb/'.$usersgroups[$g]->group_image.'" width="32" height="32">';

                          }else{

                          $shareStr .= '<img border="0" src="https://'.$server->apphost.'/images/icon.jpg" border="0" width="32">';

                          }

                          $shareStr .= '<div class="slcontent">'.$usersgroups[$g]->group_name.'</div>
                          </li>';

                          }
                          }else{

                          $shareStr .= '<li>You have not created any group</li>';

                          }
                          $shareStr .= '</ul>
                          </li>';

                          $shareStr .= '<li><a href="#">Individual</a>
                          <ul class="secondLavel" id="share_'.$products[$i]->item_id.'"
                          '.$dropCss.'>';

                          if(count($followingList) > 0)
                          {
                          for($f=0; $f < count($followingList); $f++)
                          {
                          $follower_details = users::getUserDetails($followingList[$f]->friend_id);

                          if(in_array($follower_details->user_id, $itemFriendsArr ))
                          $frChk = 'checked = "checked"';
                          else
                          $frChk = '';

                          $shareStr .= '<li><input type="checkbox" id="friend_'.$products[$i]->item_id.'_'.$follower_details->user_id.'" value="'.$follower_details->user_id.'"  onClick="sharewishlist(\'Friend\', \''.$products[$i]->item_id.'\', \'\', \''.$follower_details->user_id.'\',\'friend_'.$products[$i]->item_id.'_'.$follower_details->user_id.'\')" '.$frChk.' />';

                          if($follower_details->profile_pic != '')
                          {

                          $shareStr .= '<img src="https://'.$server->apphost.'/userpics/thumb/'.$follower_details->profile_pic.'">';

                          }else{

                          $shareStr .= '<img src="https://'.$server->apphost.'/userpics/top-img.png">';

                          }
                          $shareStr .= '<div class="slcontent">'.$follower_details->first_name.' '.$follower_details->last_name.'</div></li>';

                          }
                          }else{

                          $shareStr .= '<li>You do not have any friend</li>';

                          }

                          $shareStr .= '</ul>
                          </li>';
                          if($products[$i]->public == '0')
                          $onlyChk = 'checked = "checked"';
                          else
                          $onlyChk = '';

                          $shareStr .= '<li class="flabel">Only me <input type="checkbox" id="private_'.$products[$i]->item_id.'" value="0" onClick="sharewishlist(\'Private\', \''.$products[$i]->item_id.'\', \'\', \'\', \'private_'.$products[$i]->item_id.'\')"  /></li>
                          </ul>
                          </span>'; */

                        $result .= $shareStr;
                        // end of share option
                    }    //
                }
            } else {
                $result .= '<li style="width:90%; min-height:300px;" >
				<div >There are no items in your wishlists.</div>
				</li>';
            }

            $result .= '</ul>';
            ////
            //echo $res;

            echo $result;
            exit;
        } catch (Exception $e) {
            //Rollback transaction
            $db->rollBack();
            print_r($e->getMessage());
            exit;
        }
    }

    public function privacysettingsAction() {
        $db = Zend_Registry::get("db");
        $config = Zend_Registry::get("config");
        $server = Zend_Registry::get("server");

        $params = $this->getRequest()->getParams();
        $this->_initProfile();
        /* if($params['notify_fb'] != '')
          {
          $data ['facebook_notification'] = $params['notify_fb'];
          }else{
          $data ['facebook_notification']  = 0;
          } */

        $data['profile_type'] = 'Public';
        $data['follow_permission'] = $params['follow_permission'];
        $success = false;
        try {
            $db->beginTransaction();

            $result = $db->update('users', $data, 'user_id = ' . $_SESSION['user_id']);
            $db->commit();
            if ($result) {
                $msg = "Information has been updated successfully";
                $success = true;
            } else {
                $msg = "Error in updating information, please try again later.";
            }
        } catch (Exception $e) {
            $db->rollBack();
            $msg = $e->getMessage();
        }
        return $this->_helper->json(array('success' => $success, 'msg' => $msg));
    }

    /// change profile validation
    protected function _initChangeProfile($isFacebookUser) {

        $dob = new PS_Validate_NotEmpty($this->_getParam("birth_date"));
        $dob->setMessage(PS_Translate::_("Please enter date of birth"));

        $first_name = new PS_Validate_NotEmpty($this->_getParam("first_name"));
        $first_name->setMessage(PS_Translate::_("Please enter first name"));

        $last_name = new PS_Validate_NotEmpty($this->_getParam("last_name"));
        $last_name->setMessage(PS_Translate::_("Please enter last name"));

        $this->fields = new PS_Form_Fields(
            array(
                "first_name" => array(
                    $first_name,
                    "required" => true
                ),
                "last_name" => array(
                    $last_name,
                    "required" => true
                ),
                "email_id" => array(
                    new PS_Validate_EmailAddress(),
                    "filter" => new PS_Filter_RemoveSpaces(),
                    "required" => ($isFacebookUser) ? false : true
                ),
                "birth_date" => array(
                    array($dob, new PS_Validate_Birthdate()),
                    "required" => true
                ),
            )
        );
        $params = $this->getRequest()->getParams();
        $this->fields->setData($params);
        $this->view->fields = $this->fields;
    }
    public function changeprofileAction()
    {
        $db = Zend_Registry::get("db");

        $params = $this->getRequest()->getParams();
        $userDetail = users::getUserDetails($_SESSION['user_id']);
        $isFacebookUser = (empty($userDetail->facebook_id))? false : true;
        $this->_initChangeProfile($isFacebookUser);
        $this->errorOnNoscript();
        $success = false;
        $user_id = $_SESSION['user_id'];
        try {
            $db->beginTransaction();
            // $this->fields->validate();

                $datetime = date("Y-m-d H:i:s");
                //add user
                //$data = $params;
                $data['first_name'] = trim($params["first_name"]);
                $data['last_name'] = trim($params["last_name"]);

                if ($userDetail->facebook_id == '' || empty($userDetail->facebook_id)) {
                    if(!empty($params["email_id"])) {
                        $data['email_id'] = trim($params["email_id"]);
                    } else {
                        $data['email_id'] = $userDetail->email_id;
                    }
                } else {
                   if(!empty($params["email_id"])) {
                        $data['facebook_email'] = trim($params["email_id"]);
                   } else {
                        $data['facebook_email'] = $userDetail->facebook_email;
                   }
               }
                $data['gender'] = $params["gender"];
                $birthDate = DateTime::createFromFormat('m/j/Y',$params['birth_date']);
                $data['birth_date'] = $birthDate->format('Y-m-d');
                $data['device_type'] = 'web';
                $data['modified_date'] = $datetime;
                if(!empty($params['image'])){
                    $upload = new fileupload();
                    if ($upload->moveImageFromTemp('userpics', $params['image'])) {
                        $data['profile_pic'] = $params['image'];
                        $data['is_pic_updated'] = true;
                    } else {
                        $data['profile_pic'] = '';
                    }
                }

                $db->update('users', $data, 'user_id = ' . $user_id);

                /// update event for user's Birthday
                if ($params['birth_date'] != '') {

                    $event_datetmp = date('Y-m-d', strtotime($params['birth_date']));
                    $edtArr = explode('-', $event_datetmp);
                    $event_date = date('Y') . '-' . $edtArr[1] . '-' . $edtArr[2];

                    $event_name = $params['first_name'] . ' ' . $params['last_name'] . ' - Birthday';

                    $eventData = array(
                        "event_date" => $event_date,
                        "title" => $event_name,
                        "updated_date" => $datetime
                    );
                    $db->update('events',$eventData,'created_user = '.$user_id.' AND event_type = "Birthday"');
                }/// event

                $db->commit();

                if (isset($user_id) && $user_id > 0) {
                    $msg = 'You have successfully updated your profile!';
                    $success = true;
                } else {

                    $msg = 'Error in processsing, please try again later.';
                }
        } catch (Exception $e) {
            //Rollback transaction
            $db->rollBack();

            $msg = $e->getMessage();
            $dobError = substr($this->view->printError("birth_date"), 32, -6);
        }
        return $this->_helper->json(array('success' => $success, 'msg' => $msg, 'dobErr' => $dobError, 'id' => $user_id));
    }

 public function updateaboutmeAction()
    {
        $db = Zend_Registry::get("db");

        $params = $this->getRequest()->getParams();
        $this->errorOnNoscript();
        $success = false;
        try {
            $db->beginTransaction();
            $data['about_me'] = $params["about-me-info"];
            $user_id = $_SESSION['user_id'];
            $db->update('users', $data, 'user_id = ' . $user_id);
            $db->commit();

            if (isset($user_id) && $user_id > 0) {
                $msg = 'You have successfully updated your profile!';
                $success = true;
            } else {
                $msg = 'Error in processsing, please try again later.';
            }
        } catch (Exception $e) {
            //Rollback transaction
            $db->rollBack();
            $msg = $e->getMessage();
        }
        return $this->_helper->json(array('success' => $success, 'msg' => $msg));
    }

    protected function send_fb_gv_friend_invitation($emailID, $reciver_first_name)
    {
                $db  = Zend_Registry::get("db");
                $config = Zend_Registry::get("config");
                $server = Zend_Registry::get("server");


                $user_details = users::getUserDetails($_SESSION['user_id']);

                try{
                        $db->beginTransaction();
                        // $this->fields->validate();

                        $mailText = '
     <html><head>

<!-- Define Charset -->
        <meta http-equiv=Content-Type content=text/html; charset=UTF-8 />
        
        <!-- Responsive Meta Tag -->
        <meta name=viewport content=width=device-width; initial-scale=1.0; maximum-scale=1.0; />
        <link href="http://fonts.googleapis.com/css?family=Questrial" rel="stylesheet" type="text/css">
        
        <!--- Importing Twitter Bootstrap icons -->
        <link href=http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css rel=stylesheet>

</head><body>
<div marginheight= 0  marginwidth= 0 >
	<table style= background-color:rgb(21,18,18)  cellpadding= 0  cellspacing= 0  bgcolor= ecebeb  border= 0  width= 100% >
		<tbody><tr>
		<td>
				<table cellpadding= 0  cellspacing= 0  align= center  border= 0  width=100% >
					<tbody><tr><td style= font-size:30px;line-height:30px  height= 30 > </td></tr>
					<tr>
						<td>
							<table style= border-collapse:collapse  cellpadding= 0  cellspacing= 0  align=left  border=0 width=45% >
								<tbody><tr>
									<td align= center >
										<table cellpadding= 0  cellspacing= 0  align= center  border=0 >
											<tbody><tr>
												<td style= line-height:21px  align= center >

	<a href= https://giftvise.com/  style=display:block;border-style:none!important;border:0!important  target= _blank ><img style= display:block;width:128px  src=https://www.giftvise.com/newdesign/img/tpl/new-giftvise-logo.png width=180 ></a>
												</td>			
											</tr>
										</tbody></table>		
									</td>
								</tr>
								
							</tbody></table>
							
							<table style=border-collapse:collapse  cellpadding= 0  cellspacing=0  align=left  border=0  width=5px>
								<tbody><tr><td style= font-size:20px;line-height:20px  height=20  width=5px > </td></tr>
							</tbody></table>
							
							<table style=border-collapse:collapse  cellpadding=10  cellspacing= 0  align= right  border=0 width=30%>
								
								<tbody><tr>
									<td>
										<table style=border-collapse:collapse  cellpadding= 0  cellspacing= 0  align= left  border= 0 >
			
				                			<tbody><tr>
				                				<td>
				                					<table style=border-collapse:collapse  cellpadding= 0  cellspacing= 0  align=center>
				                						<tbody><tr>
				                							
							                				<td style= color:#ffffff;font-size:14px;font-family:&#39;Questrial&#39;,Helvetica,sans-serif;line-height:24px  align= center >
							                						<div style= line-height:24px >
							                						<span>
<a href="https://'.$server->apphost.'/modals/modalshowhelpcenter?tab=about" style=color:#878b99;text-decoration:none >About</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="https://'.$server->apphost.'/modals/modalshowhelpcenter?tab=faq" style= color:#878b99;text-decoration:none>Help</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a><a href="https://'.$server->apphost.'/modals/modalshowhelpcenter?tab=contact" style= color:#878b99;text-decoration:none >Contact</a>
								                					
							                						</span>
							                					</div>	
							                				</td>
				                						</tr>
				                					</tbody></table>
				                				</td>
				                			</tr>
			                			</tbody></table>
									</td>
								</tr>	
							</tbody></table>	
						</td>
					</tr>
					
					<tr>
						<td style= line-height:21px  align= center >
						</td>			
					</tr>
					
					<tr><td style= font-size:50px;line-height:50px  height= 50 > </td></tr>
					
					<tr>
						<td style= color:#ffffff;font-size:40px;font-family:&#39;Questrial&#39;,Helvetica,sans-serif;line-height:28px  align= center >
							
							
							
							<div style= line-height:28px >
								<span>Hi '.$reciver_first_name .'!</span>
							</div>
        				</td>
					</tr>
					
					<tr><td style= font-size:25px;line-height:25px  height= 25 > </td></tr>
					
					<tr>
						<td align= center >
							<table cellpadding= 0  cellspacing= 0  align= center  border= 0  width= 540 >
								<tbody><tr>
									
									<td style= color:#878b99;font-size:18px;font-family:&#39;Questrial&#39;,Helvetica,sans-serif;line-height:30px  align= center >
										<div style= line-height:30px >
											
											
											<span>We\'re happy to let you know that your Facebook friend '. $user_details->first_name.' '. $user_details->last_name.' just joined Giftvise. Welcome '. $user_details->first_name. ' to the community!</span>
										</div>
			        				</td>	
								</tr>
							</tbody></table>
						</td>
					</tr>
					
					<tr><td style= font-size:35px;line-height:35px  height= 35 > </td></tr>
					
					<tr>
						<td align= center >
							
							<table style= border-radius:50px  cellpadding= 0  cellspacing= 0  align= center  bgcolor= f06e6a  border= 0  width= 240 >
								
								<tbody><tr><td style= font-size:18px;line-height:18px  height= 18 > </td></tr>
								
								<tr>
	                				<td style= color:#ffffff;font-size:18px;font-family:&#39;Questrial&#39;,Helvetica,sans-serif  align= center >
	                					
	                					
		                    			<div style= line-height:24px >
<a style=text-decoration:none href="https://' . $server->apphost . '/newfriends/friendprofile/friend_id/' . md5($user_details->user_id) . '">
		                    				<span style= color:#ffffff;text-decoration:none >See Profile</span>
		                    	             </a>		</div>
		                    		</td>								
								</tr>
								
								<tr><td style= font-size:18px;line-height:18px  height= 18 > </td></tr>
							
							</tbody></table>
						</td>
					</tr>
					
					<tr><td style= font-size:65px;line-height:65px  height= 65 > </td></tr>
					
				</tbody></table>
			</td></tr>
		
	</tbody></table>
	
<!--	
	<table cellpadding= 0  cellspacing= 0  bgcolor= 363b49  border= 0  width= 100% >
							
		<tbody><tr><td style= font-size:20px;line-height:20px  height= 20 > </td></tr>
					
		<tr>
			<td align= center >
				
				<table cellpadding= 0  cellspacing= 0  align= center  border= 0  width= 590 >
					
					<tbody><tr>
						<td align= center >
								                		
	                		<table cellpadding= 0  cellspacing= 0  align= center  border= 0 >
	                			<tbody><tr>
	                				<td style= color:#5a5f70;font-size:14px;font-family:&#39;Questrial&#39;,Helvetica,sans-serif;line-height:25px  align= center >
	                					<div style= line-height:25px >
	                						<span>
		                					
		                						Copyright @ Giftvise 2014
		                					
	                						</span>
	                					</div>	
	                				</td>
	                			</tr>
	                			
                			</tbody></table>
                			
						</td>
					</tr>
					
				</tbody></table>
			</td>
		</tr>
		<tr><td style= font-size:20px;line-height:20px  height= 20 > </td></tr>
	</tbody></table>
-->
<!-- copy right secion -->
<table border=0 width=100% cellpadding=0 cellspacing=0 bgcolor=#ffffff class=bg4_color>

 <tbody><tr><td height=20 style=font-size:20px;line-height:20px;>&nbsp;</td></tr>

 <tr> <td align=center>

 <table border=0 align=center width=590 cellpadding=0 cellspacing=0 class=container590>

 <tbody><tr> <td align=center>

 <table border=0 align=center cellpadding=0 cellspacing=0> <tbody><tr> 
<td style=color:#5a5f70;font-size:14px;font-family:&#39;Questrial&#39;,Helvetica,sans-serif;line-height:25px align=center>
<div class=editable_text style=line-height:25px;> <span class=text_container>

 Copyright @ Giftvise 2014

 </span> </div> </td> </tr>

 </tbody></table>

 </td> </tr>

 </tbody></table> </td> </tr>

 <tr><td height=20 style=font-size: 20px; line-height: 20px;>&nbsp;</td></tr>

 </tbody></table>
</div></body></html>

                           ';
                        $to = $emailID;

                        //send invitation
                        $requestData['user_id'] = $_SESSION['user_id'];
                        $requestData['email'] = $to;

                        //check for invitation
                       // $chkExistRequest = usersfriend::checkSentRquest($params['user_id'], $params['email']);

                        $subject = 'Your Facebook friend ' . $user_details->first_name.' '. $user_details->last_name.' joined Giftvise';
                        if(empty($chkExistRequest))
                        {
                                $db->insert('invitedRequest', $requestData);
                                $request_id=$db->lastInsertId();
                                $db->commit();

                                $transport = new Zend_Mail_Transport_Sendmail($config->supportEmail);
                                Zend_Mail::setDefaultTransport($transport);
                                $mail = new Zend_Mail();
                                $mail->setSubject($subject);
                                $mail->setFrom($config->supportEmail, 'Giftvise');
                                $mail->addTo($to);
                                $mail->setBodyHtml($mailText);
                                $mail->send();
                                $msg = "Invitation has been sent.";
                        }else{
                                $msg = "You have already sent request to this user";
                        }
                }
                catch(Exception $e)
                {
                     // return $this->_helper->json(array('msg' => $e->getMessage()));
                }
               // return $this->_helper->json(array('msg' => $msg));
	}

}

