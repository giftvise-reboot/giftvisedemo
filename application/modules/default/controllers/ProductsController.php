<?php
/*------------------------------------------------------------------ 
 * Copyright (c) 2013-2014 by Giftvise, Inc.
 * All rights reserved. 
 *------------------------------------------------------------------
 */

/**
 * ProductsController
 * 
 * @author
 * @version 
 */

require_once 'Zend/Controller/Action.php';
class ProductsController extends PS_Controller_Action {

    public function preDispatch()
    {
        // set title of the page
        $this->view->headTitle()->prepend(PS_Translate::_("Explore"));
    }
	/**
	 * The default action - show the home page
	 */
	public function indexAction() {
		// TODO Auto-generated NewProductsController::indexAction() default
		// action
                $server = Zend_Registry::get("server");
                $config = Zend_Registry::get("config");
                $logger = Zend_Registry::get("logger");

                if ($_SESSION['logged_in'] != true) {
                   $this->_redirect(PS_Util::redirect("https://" . $server->apphost));
                   exit;
                }

                //disable layouts for this action
                $this->_helper->layout()->disableLayout();

                $ajax = $this->getRequest()->getParam("ajax", false);
                $next_start = $this->getRequest()->getParam("next_start", false);
                /*
                $logger->info("ajax = " . $ajax);
                $logger->info("next_start = " . $next_start);
                */
                // Set the value for view to use it
                $this->view->ajax = $ajax;
                ///categories
                $categories = category::categoryList();
                $this->view->categories = $categories;

                //get products
                $param = $this->getRequest()->getParams();

                //$ord = $param['order'];
                $catVal = $param['item_type'];

                ///new sorting
                $orderBy = '';

                if($param['pricekey'] == 'price')
                {
                        if($orderBy != '')
                                $orderBy .= ', item_price ASC';
                        else
                                $orderBy = ' item_price ASC';
                }

                if($param['datekey'] == 'date')
                {
                        if($orderBy != '')
                                $orderBy .= ', uw.added_date DESC';
                        else
                                $orderBy = 'uw.added_date DESC';
                }
                if($param['popularityKey'] == 'popularity')
                {
                        if($orderBy != '')
                                $orderBy .= ', w.item_count DESC';
                        else
                                $orderBy = 'w.item_count DESC';
                }

                $products = wishlistitmes::getAllFeedSiteNew($_SESSION['user_id'],$orderBy,$catVal,$next_start, $numberOfItems);
                if($catVal != '')
                {
                        $catName = category::categoryName($catVal);
                }else{
                        $catName = 'Category';
                }
                for($i=0;$i<count($products);$i++)
                {
                        $item = array();
                        $totalComments = itemcomments::countComments($products[$i]->id);
                        $userwished = wishlistitmes::countWished($products[$i]->id);

                        /// latest item
                        //$timeAdded = wishlistitmes::latestItem($products[$i]->id);
                        $timeAdded = wishlistitmes::latestItemHome($products[$i]->id);
                        $addeduser = users::getUserDetails($timeAdded['user_id']);


                        ///check this item is already in you wishlist
                        //$itemwhished = wishlistitmes::chkWishedItem($product_id,$_SESSION['user_id']);
                        $itemwhished = wishlistitmes::chkWishedItem($products[$i]->id,$_SESSION['user_id']);
                        if($itemwhished)
                        {
                                $item['youwhished'] = 'Y';
                        }else{
                                $item['youwhished'] = 'N';
                                 $userwished = 0;
                        }
                        //

                        $item['comment'] = $totalComments;
                        $item['wished'] = $userwished;
                        $item['added_time'] = $timeAdded['time'];
                        //$item['addedby'] = $addeduser->first_name.' '.$addeduser->last_name;
                        $item['first_name'] = $addeduser->first_name;
                        $item['last_name'] = $addeduser->last_name;
                        $item['user_id'] = $addeduser->user_id;

                        $items[] = $item;
                }
                //print_r($products);
                //exit;//
                $this->view->items = $products;
                $this->view->extval = $items;
                $this->view->categoryName = $catName;
                //exit;
	}
        
        public function itemcuratedAction() {
            $db = Zend_Registry::get("db");
            $logger = Zend_Registry::get("logger");

            $params = $this->getRequest()->getParams();
            $logger->info("Product: " . $params['product_id'] . " curated: ". $params['selected'] . " by user: " . $params['user_id']);

            $productDetail = wishlistitmes::getProductDetail($params['product_id']);

            if($productDetail) {
                if ($params['selected']) {
                    $curr_state = ($productDetail[0]->state & 0x3) | ($productDetail[0]->state | 0x1); 
                } else {
                    $curr_state = ($productDetail[0]->state & 0x2); // | ($productDetail[0]->state & 0x0);
                }
            }
 
            $data = array (
                'state' => $curr_state,
            );

            if(isset($params['product_id']) && $params['product_id'] != '') {
                // Mark product as curated
                $db->update("wishlistitmes", $data, "id=" . $params['product_id']);
            }

            return $this->_helper->json(array('success' => true));
        }

         public function itemfeaturedAction() {
            $db = Zend_Registry::get("db");
            $logger = Zend_Registry::get("logger");

            $params = $this->getRequest()->getParams();
            $logger->info("Product: " . $params['product_id'] . " curated: ". $params['selected'] . " by user: " . $params['user_id']);

            $productDetail = wishlistitmes::getProductDetail($params['product_id']);
            
            if($productDetail) {
                if ($params['selected']) {
                    $curr_state = ($productDetail[0]->state & 0x3) | ($productDetail[0]->state | 0x2); 
                } else {
                    $curr_state = ($productDetail[0]->state & 0x1); // | ($productDetail[0]->state & 0x0);
                }
            }

            $modified_date = date('Y-m-d H:i:s');

            $data = array (
                'state' => $curr_state, 'updated_date' => $modified_date,
            );

            if(isset($params['product_id']) && $params['product_id'] != '') {
                // Mark product as curated
                // $logger->info("Product: going to feature a product "); 
                $db->update("wishlistitmes", $data, "id=" . $params['product_id']);
            }

            return $this->_helper->json(array('success' => true));
        }

        public function userfeaturedAction() {
            $db = Zend_Registry::get("db");
            $logger = Zend_Registry::get("logger");

            $params = $this->getRequest()->getParams();

            $logger->info($params['feature_user_id']); 

            $userDetail =  users::getUserDetails($params['feature_user_id']);
            $curate_state = 0;

            if($userDetail) {
                if ($params['selected']) {
                    $curate_state = 1; 
                } else {
                    $curate_state = 0;
                }
            }

            $modified_date = date('Y-m-d H:i:s');

            $data = array (
                'curator' => $curate_state, 'modified_date' => $modified_date,
            );
            
            $logger->info("Chikke ".$params['feature_user_id']);

            if(isset($params['feature_user_id']) && $params['feature_user_id'] != '') {
                $db->update("users", $data, "user_id=" . $params['feature_user_id']);
            }

            return $this->_helper->json(array('success' => true));
        }

 public function curateusersAction()
        {
                $db = Zend_Registry::get("db");
                $config = Zend_Registry::get("config");
                $server = Zend_Registry::get("server");
                $logger = Zend_Registry::get("logger");

                $params = $this->getRequest()->getParams();
                $logger->info("[START] " . __CLASS__ . ' : ' . __FUNCTION__);

                try{
                        $searchResult = users::getAllforcurate();
                        $logger->info("[START] " . count($searchResult));
                        /*
                        if(!empty($searchResult))
                        {
                                for($i=0; $i < count($searchResult); $i++)
                                {
                                        $list = array();
                                        $follower_details = users::getUserDetails($searchResult[$i]->user_id);
                                        $list['user_id'] = $follower_details->user_id;
                                        $list['username'] = $follower_details->first_name.' '.$follower_details->last_name;
                                        $list['profile_pic'] = $follower_details->profile_pic;

                                        $result[] = $list;
                                }
                                $this->view->result = $result;
                        }
                        */

                      $this->view->users = $searchResult;
                      $this->view->curatePageType = 1;
                      $this->view->more_items = 1;
                      $this->view->curate = 1;

                }
                catch(Exception $e)
                {
                  $this->view->errors[] = $e->getMessage();
                }
        }

        public function itemignoredAction() {
            $db = Zend_Registry::get("db");
            $logger = Zend_Registry::get("logger");

            $params = $this->getRequest()->getParams();
            $logger->info("Product: " . $params['product_id'] . " curated: ". $params['selected'] . " by user: " . $params['user_id']);

            $curr_state = 4; 
            $data = array (
                'state' => $curr_state,
            );

            if(isset($params['product_id']) && $params['product_id'] != '') {
                // Mark product as curated
                $db->update("wishlistitmes", $data, "id=" . $params['product_id']);
            }

            return $this->_helper->json(array('success' => true));
        }

        public function curateAction() {
            $db = Zend_Registry::get("db");
            $config = Zend_Registry::get("config");
            $server = Zend_Registry::get("server");
            $logger = Zend_Registry::get("logger");

            if ($_SESSION['logged_in'] != true) {
               $this->_redirect(PS_Util::redirect("https://". $server->apphost));
               exit;
            }
            /* curate newly added products */
            $this->view->curate = 1;
            $this->view->page = 'curate';

            $params = $this->getRequest()->getParams();
            $product_id = $params['pid'];

            //user
            $user_details = users::getUserDetails($_SESSION['user_id']);
            $this->view->userDetail = $user_details;

            ///categories
            $categories = category::categoryList();
            $this->view->categories = $categories;

            //get products
            $param = $this->getRequest()->getParams();

            try {
                $ord = $param['order'];
                $catVal = $param['item_type'];

            if ($param['pricekey'] == 'price') {
                if ($orderBy != '') {
                    $orderBy .= ', item_price ASC';
                } else {
                    $orderBy = ' item_price ASC';
                }
            }

            if ($param['datekey'] == 'date') {
                if ($orderBy != '') {
                    $orderBy .= ', uw.added_date DESC';
                } else {
                    $orderBy = 'uw.added_date DESC';
                }
            }

            if($param['popularityKey'] == 'popularity') {
                if ($orderBy != '') {
                    $orderBy .= ', w.item_count DESC';
                } else {
                    $orderBy = 'w.item  _count DESC';
                }
            }

            $numberOfItems = 36;
            $products = wishlistitmes::getLoadMoreCuratePage(0,$numberOfItems, $param['curatetype']);
            if ($param['hash']) {
                $recomendedItem = wishlistitmes::getItemIdByHash($param['hash']);
                $products = (empty($products)) ? $recomendedItem : $recomendedItem + $products;
                $this->view->recomended = true;
            }
            $this->view->more_items = count($products) < $numberOfItems ? 0 : 1;

            if ($catVal != '') {
                $catName = category::categoryName($catVal);
            } else {
                $catName = 'Category';
            }
            if ($param['curatetype'] == 4) {
                $this->view->get_topp = 1;
            }
            $curre_user = users::getUserDetails($_SESSION['user_id']);
            $filter_tags = array();
            $f_index = 0;
            $productIds = array();
            for ($i = 0; $i < count($products);$i++) {
                $item = array();
                $productIds[] = $products[$i]->id;

                $totalComments = itemcomments::countComments($products[$i]->id);
                $userwished = wishlistitmes::chkWishedItem($products[$i]->id, $_SESSION['user_id']);
                $countwishes = wishlistitmes::countWished($products[$i]->id);

                $showChk = wishlistitmes::showItem($products[$i]->id, $_SESSION['user_id'],$friend_details->user_id);

                $timeAdded = wishlistitmes::latestItem($products[$i]->id);
                //$timeAdded = wishlistitmes::latestItemUser($products[$i]->id, $_SESSION['user_id']);
                $addeduser = users::getUserDetails($timeAdded['user_id']);

                $itemwhished = wishlistitmes::chkWishedItem($products[$i]->id,$_SESSION['user_id']);

                if ($itemwhished) {
                    $item['youwhished'] = 'Y';
                } else {
                    $item['youwhished'] = 'N';
                }

                if ($products[$i]->user_id != $_SESSION['user_id']) {
                    $item['item_load_type'] = 'filter-recommoded';
                } else if ($addeduser->user_id != $curre_user->user_id) {
                    $item['item_load_type'] = 'filter-friend';
                } else {
                    $item['item_load_type']  = 'filter-all';
                }

                $index_e = 0;
                $events = event::getItemEvents($_SESSION["user_id"], $products[$i]->id);
                for ($j = 0; $j < count($events); $j++) {
                    if ($events[$j]->selected) {
                       $index='events' . $index_e;
                       $item[$index] = $events[$j]->title;
                       $index_e++;
                    }
                }

                $taglist = category::getItemTags($products[$i]->id, $_SESSION['user_id']);
                for ($j = 0; $j < count($taglist); $j++) {
                    $index='tags' . $j;
                    $item[$index] = $taglist[$j]->name;
                }
                $item['n_tags'] = count($taglist);
                $item['n_events'] = $index_e;
                $item['comment'] = $totalComments;
                $item['wished'] = $userwished;
                $item['countwishes'] = $countwishes;
                $item['added_time'] = $timeAdded['time'];
                //$item['addedby'] = $addeduser->first_name.' '.$addeduser->last_name;
                $item['first_name'] = $addeduser->first_name;
                $item['last_name'] = $addeduser->last_name;
                $item['user_id'] = $addeduser->user_id;
                $items[] = $item;
            }
            $this->view->vpage=3;
            $this->view->filtertags = wishlistitmes::getItemsCategoryList(null, "('" . implode("','",$productIds) . "')");
            $this->view->curatePageType = $param['curatetype'];
            $logger->info("curatePageType " . $param['curatetype']); 

            /* User created events for filter bar */
            $filter_bar_events = event::getUserCreatedEvents($_SESSION["user_id"]);
            $this->view->filter_bar_events = $filter_bar_events;

            $this->view->items = $products;
            $this->view->extval = $items;
            $this->view->categoryName = $catName;
            $this->view->newNotifications = notifications::countnewnotifications();
            $this->view->activityLog = notifications::activitylog(true);
            $this->view->notificationsCount = notifications::notificationscount();

            /* add currency covertors here */
            $amount = urlencode('1');
            $from = 'USD';
 
            $db->beginTransaction();

            $to = 'INR';
            $from_Currency = urlencode($from);
            $to_Currency = urlencode($to);
            $get = file_get_contents("https://www.google.com/finance/converter?a=$amount&from=$from_Currency&to=$to_Currency");
            $get = explode("<span class=bld>",$get);
            $get = explode("</span>",$get[1]);  
            $converted_amount = preg_replace("/[^0-9\.]/", null, $get[0]);
            $logger->info("USD to  INR :  " . $converted_amount);

            $datau['c_value'] = $converted_amount;
            $res = $db->update('dollar_value', $datau, array("currency = ?" => $to));

            $db->commit();

            } catch(Exception $e){
                    //Rollback transaction
                    //print_r($e->getMessage());
                    //exit;
                    $this->view->errors[] = $e->getMessage();
            }
        }

        public function exploreAction()
	{
		$db = Zend_Registry::get("db");
		$config = Zend_Registry::get("config");
		$server = Zend_Registry::get("server");
		$logger = Zend_Registry::get("logger");

                if ($_SESSION['logged_in'] != true) {
                   $this->_redirect(PS_Util::redirect("https://" . $server->apphost));
                   exit;
                }
 
                $logger->info("--- ACCESS-INFO --- ::FEED-PAGE::USER-ID::" .$_SESSION['user_id']. "::DEVICE-INFO:: ". 
                              $_SERVER['HTTP_USER_AGENT'] . "IP ADDRESS ". $_SERVER['REMOTE_ADDR'] ."\n");

		$params = $this->getRequest()->getParams();
		$product_id = $params['pid'];

                $ret = stripos($_SERVER['HTTP_USER_AGENT'], 'phone');
                if (!ret == false) {
                   $this->view->need_tutorial = 1;
                } else {
                   $this->view->need_tutorial = 0;
                }

		//user
		$user_details = users::getUserDetails($_SESSION['user_id']);
		$this->view->userDetail = $user_details;
		
		///categories
		$categories = category::categoryList();
		$this->view->categories = $categories;
	        $this->view->recomended = 1;	 /* All products */
		//get products	
		$param = $this->getRequest()->getParams();
		
		try{
	                $ord = $param['order'];
			$catVal = $param['item_type'];
		if($param['pricekey'] == 'price')
		{
			if($orderBy != '')
				$orderBy .= ', item_price ASC';
			else
				$orderBy = ' item_price ASC';
		}
		
		if($param['datekey'] == 'date')
		{
			if($orderBy != '')
				$orderBy .= ', uw.added_date DESC';
			else
				$orderBy = 'uw.added_date DESC';
		}
		
		if($param['popularityKey'] == 'popularity')
		{
			if($orderBy != '')
				$orderBy .= ', w.item_count DESC';
			else
				$orderBy = 'w.item_count DESC';
		}

                $this->view->productCategory = $param['item_type'];

                $this->view->feedPageEventNoItems = 0;
                $numberOfItems = 10;
                $this->view->next_start = $numberOfItems;

/*
                $recomendedItem = array(); //null;
   
                if ($param['hash']){
                    $recomendedItem = wishlistitmes::getItemIdByHash($param['hash']);
                    if($recomendedItem) {
                       $this->view->recommendedProductImage = $recomendedItem[0]->image;
                    }
                }
                if($params['given']){
                    $products = event::getGiftGiven($_SESSION['user_id'], $recomendedItem[0]->id);
                } else if ($params['received']){
                    $products = event::getGiftRecived($_SESSION['user_id'], $recomendedItem[0]->id);
                } else if ($param['hash']) {
                    $this->view->recomended = 3;
                    $products = wishlistitmes::getLoadMoreFeedPageReco($_SESSION['user_id'],$orderBy,$catVal,0,$numberOfItems, $recomendedItem[0]->id);
                } else {
                   if ($param['subpage'] == "events") {
                        $this->view->filterEventsPage = 1;
                        $numberOfItems = 20000;
                        $this->view->filterEventID = $param['eventID'];                       
                        $products = wishlistitmes::getLoadMoreFeedPageEvents($_SESSION['user_id'], 0, $numberOfItems, $param['eventID']);
                        if (!count($products)) {
                             $this->view->feedPageEventNoItems = 1;
                             $products = wishlistitmes::getLoadMoreFriendPage($param['createdUserId'],$orderBy,$catVal,0,$numberOfItems);
                             //$products = wishlistitmes::getLoadMoreFeedPageFriends($param['createdUserId'],$orderBy,$catVal,0,$numberOfItems,0);
                        }
                   }else if ($param['subpage'] == "friends") {
                        $this->view->recomended = 2;
                        $products = wishlistitmes::getLoadMoreFeedPageFriends($_SESSION['user_id'],$orderBy,$catVal,0,$numberOfItems,0);
                    } else if ($param['subpage'] == "reco") {
                        $this->view->recomended = 3;
                        $products = wishlistitmes::getLoadMoreFeedPageReco($_SESSION['user_id'],$orderBy,$catVal,0,$numberOfItems,0);
                    } else {
                        $this->view->recomended = 1;
                        $products = wishlistitmes::getLoadMoreFeedPage($_SESSION['user_id'],$orderBy,$catVal,0,$numberOfItems,0);
                    }
                }

                if ($param['hash']){
                     if($recomendedItem) { 
                          $products = array_merge(array($recomendedItem[0]), $products);
                     }
                }
*/
                $products = wishlistitmes::getLoadMoreFeedPage($_SESSION['user_id'],$orderBy,$catVal,0,$numberOfItems,0);

                $this->view->more_items = 1; //count($products) < $numberOfItems ? 0 : 1;
                   $logger->info("Query result count: " . count($products));


		if($catVal != '') {
			$catName = category::categoryName($catVal);
		} else {
			$catName = 'Category';
		}

               $curre_user = users::getUserDetails($_SESSION['user_id']);

               $this->view->vpage=3;

               /* filter bar tags */
               $noids = true;
               $productIds = array();
               for ($i = 0; $i < count($products); $i++) {
                   $productIds[] = $products[$i]->id;
                   $noids =  false;
               }
               if ($noids == false) {
                   $this->view->filtertags = wishlistitmes::getUsersTagList($_SESSION['user_id'], "('" . implode("','",$productIds) . "')");
               }

               /* User created events for filter bar */
                $filter_bar_events = event::getFeedPageEvents($_SESSION["user_id"]);
                $this->view->filter_bar_events = $filter_bar_events;

		$this->view->items = $products;
                $this->view->extval = wishlistitmes::prepareExtVal($products, $_SESSION['user_id']);
		$this->view->categoryName = $catName;
                $this->view->newNotifications = notifications::countnewnotifications();
                $this->view->activityLog = notifications::activitylog(true);
                $this->view->notificationsCount = notifications::notificationscount();
		
		} catch(Exception $e){
			//Rollback transaction
			//print_r($e->getMessage());
			//exit;
			$this->view->errors[] = $e->getMessage();
		}
	}//
       
       public function productstagsAction()
       {
        $db     = Zend_Registry::get("db");
        $config = Zend_Registry::get("config");
        $server = Zend_Registry::get("server");
        $logger = Zend_Registry::get("logger");

        $this->_helper->layout()->disableLayout();
        $itemIds = $this->getRequest()->getParam("item_ids");
        $page = $this->getRequest()->getParam("page");

        try {
            if ($page == 'feed' || $page == 'curate') {
               $this->view->filtertags = wishlistitmes::getItemsCategoryList(null, $itemIds);
            } else {
                $this->view->filtertags = wishlistitmes::getUsersTagList($_SESSION['user_id'], $itemIds);

            }
        }
        catch(Exception $e) {
            return $this->_helper->json(array('fail' => true, 'msg' => $e->getMessage()));
        }
     }

    public function getproductstagsasarrayAction() {

        $this->_helper->layout()->disableLayout();
        $itemIds = $this->getRequest()->getParam("item_ids");
        $page = $this->getRequest()->getParam("page");

        $fail = false;
        $error = false;
        $tags = array();
        try {
            if ($page == 'feed' || $page == 'curate') {
                $tags = wishlistitmes::getItemsCategoryList(null, $itemIds);
            } else {
                $tags = wishlistitmes::getUsersTagList($_SESSION['user_id'], $itemIds);
            }
        }
        catch(Exception $e) {
            $fail = true;
            $error =$e->getMessage();
        }
        return $this->_helper->json(array('fail' => $fail, 'msg' => $error, 'tags' => $tags));
    }
 
        public function fbshareshowAction()
	{

		$db = Zend_Registry::get("db");
		$config = Zend_Registry::get("config");
		$server = Zend_Registry::get("server");
		$logger = Zend_Registry::get("logger");
               
		$params = $this->getRequest()->getParams();
           
               try {

                if (!empty($_SESSION['user_id']) && $_SESSION['user_id'] != '') {
                     $logger->debug("user_id = " . $_SESSION['user_id']);
                     $user_details = users::getUserDetails($_SESSION['user_id']);
                     $this->view->userDetail = $user_details;
                }

                $numberOfItems = 18;
                $recomendedItem = null;

                $hashItemId = $this->_request->getQuery('hash');
                if ($hashItemId){
                    $recomendedItem = wishlistitmes::getItemIdByHash($hashItemId);
                }

                $orderBy = '';
                $catVal = ''; 
                $products = wishlistitmes::getLoadMoreLandingPage(0,$orderBy,$catVal,0,$numberOfItems); 

                if ($hashItemId){
                     array_unshift($products, '');
                    $products = $recomendedItem + $products;
                }

                $this->view->more_items = count($products) < $numberOfItems ? 0 : 1;
                                $logger->info("Query result count: " . count($products));

                $this->view->vpage=1;

		$this->view->items = $products;
                $this->view->extval = wishlistitmes::prepareExtVal($products, $_SESSION['user_id']);
		
		} catch(Exception $e){
			$this->view->errors[] = $e->getMessage();
		}
	}

	public function productsgridAction() {
		$server = Zend_Registry::get("server");
		$config = Zend_Registry::get("config");
		$logger = Zend_Registry::get("logger");
	
		//disable layouts for this action
		$this->_helper->layout()->disableLayout();
		
		$ajax = $this->getRequest()->getParam("ajax", false);
                $next_start = $this->getRequest()->getParam("next_start", false);
                $page = $this->getRequest()->getParam("page", false);

		$logger->info("ajax = " . $ajax);
                $logger->info("next_start = " . $next_start);                
                $logger->info("page = " . $page);

                $this->view->pgid = $this->getRequest()->getParam("pgid", false);; 

                if ($page == "curate") {
                    $this->view->curate = 1;
                }
                
		// Set the value for view to use it
		$this->view->ajax = $ajax;
                $this->view->nextload = 1;

                 ///categories
                $categories = category::categoryList();
                $this->view->categories = $categories;

                //get products  
                $param = $this->getRequest()->getParams();

                //$ord = $param['order'];
                $catVal = $param['item_type'];
                $numberOfItems = 5;

                $rem = $next_start % $numberOfItems;
                $adjust = $rem ? $numberOfItems - $rem : 0;
                $next_start += $adjust;  
                $logger->info("After normalizing, next_start = " . $next_start);
                $this->view->next_start = $next_start + $numberOfItems;

                ///new sorting
                $orderBy = '';

                if($param['pricekey'] == 'price')
                {
                        if($orderBy != '')
                                $orderBy .= ', item_price ASC';
                        else
                                $orderBy = ' item_price ASC';
                }

                if($param['datekey'] == 'date')
                {
                        if($orderBy != '')
                                $orderBy .= ', uw.added_date DESC';
                        else
                                $orderBy = 'uw.added_date DESC';
                }
                if($param['popularityKey'] == 'popularity')
                {
                        if($orderBy != '')
                                $orderBy .= ', w.item_count DESC';
                        else
                                $orderBy = 'w.item_count DESC';
                }

                $user_id = $_SESSION['user_id'];
                // $this->view->vpage = $page;
                if ($page == 'landing') {
                    $this->view->vpage = 1;
                    $products = wishlistitmes::getLoadMoreLandingPage($user_id,$orderBy,$catVal,$next_start,$numberOfItems);
                } else if ($page == 'home') {
                    $this->view->vpage = 2;
                    $products = wishlistitmes::getLoadMoreHomePage($user_id,$orderBy,$catVal,$next_start,$numberOfItems);
                } else if ($page == 'friends') {
                    $this->view->vpage = 4;
                    $user_id = $page = $this->getRequest()->getParam("friend_id", false); 
                    $products = wishlistitmes::getLoadMoreFriendPage($user_id,$orderBy,$catVal,$next_start,$numberOfItems);
                } else if ($page == 'curate') {
                    $this->view->vpage = 5;
                    $products = wishlistitmes::getLoadMoreCuratePage($next_start, $numberOfItems, $param['curatetype']);
                    $logger->info("curatetype  " . $param['curatetype']);
                    $this->view->curatePageType = $param['curatetype'];
                } else /*feed */ {
                    $this->view->vpage = 3;
                    if ($param['subpage'] == "friends") {
                        $products = wishlistitmes::getLoadMoreFeedPageFriends($user_id,$orderBy,$catVal,$next_start,$numberOfItems,0);
                    } else if ($param['subpage'] == "reco") {
                        $products = wishlistitmes::getLoadMoreFeedPageReco($user_id,$orderBy,$catVal,$next_start,$numberOfItems,0);
                    } else {
                        $products = wishlistitmes::getLoadMoreFeedPage($user_id,$orderBy,$catVal,$next_start,$numberOfItems,0);
                    }
                }
                $this->view->more_items = count($products) < $numberOfItems ? 0 : 1;

                $logger->info("Query result count: " . count($products));

                if($catVal != '')
                {
                        $catName = category::categoryName($catVal);
                }else{
                        $catName = 'Category';
                }
                $curre_user = users::getUserDetails($user_id);
                $height[1] = '';
                $this->view->items = $products;
                $logger->info("Products count = " . count($this->view->items));
                $this->view->extval = wishlistitmes::prepareExtVal($products, $user_id);
                $this->view->append = $param['append'];
                $this->view->prepend = $param['prepend'];
                $this->view->imageHeight = $height[1];
                $this->view->categoryName = $catName;
                //exit;
	}

        public function checkfornewAction() {
                $server = Zend_Registry::get("server");
                $config = Zend_Registry::get("config");
                $logger = Zend_Registry::get("logger");

                //disable layouts for this action
                $this->_helper->layout()->disableLayout();

                $ajax = $this->getRequest()->getParam("ajax", false);
                $cur_id = $this->getRequest()->getParam("id", false);

                $user_id = $_SESSION['user_id'];
                if($user_id) {
                    $orderBy = '';
                    $catVal = '';
                    $products = wishlistitmes::getLoadMoreHomePage($user_id,$orderBy,$catVal,0,2);
                    if (count($products) > 0) {
                         if ($cur_id != $products[0]->id && $products[0]->id > $cur_id) {
                              return $this->_helper->json(array('success' => true));
                         } else {
                              return $this->_helper->json(array('success' => false));
                         }
                    }
                 }
                 return $this->_helper->json(array('success' => false));
        }
	public function loadmoreAction() {
		$server = Zend_Registry::get("server");
		$config = Zend_Registry::get("config");
		$logger = Zend_Registry::get("logger");
		
		//disable layouts for this action
		$this->_helper->layout()->disableLayout();
		
		$ajax = $this->getRequest()->getParam("ajax", false);
		$logger->info("ajax = " . $ajax);
		// Set the value for view to use it
		$this->view->ajax = $ajax;

                  ///categories
                $categories = category::categoryList();
                $this->view->categories = $categories;

                //get products  
                $param = $this->getRequest()->getParams();

                //$ord = $param['order'];
                $catVal = $param['item_type'];

                ///new sorting
                $orderBy = '';

                if($param['pricekey'] == 'price')
                {
                        if($orderBy != '')
                                $orderBy .= ', item_price ASC';
                        else
                                $orderBy = ' item_price ASC';
                }

                if($param['datekey'] == 'date')
                {
                        if($orderBy != '')
                                $orderBy .= ', uw.added_date DESC';
                        else
                                $orderBy = 'uw.added_date DESC';
                }
                if($param['popularityKey'] == 'popularity')
                {
                        if($orderBy != '')
                                $orderBy .= ', w.item_count DESC';
                        else
                                $orderBy = 'w.item_count DESC';
                }
                //$products = wishlistitmes::getTopWeek($orderBy,$catVal);
                $products = wishlistitmes::getAllFeedSite($_SESSION['user_id'],$orderBy,$catVal);

                if($catVal != '')
                {
                        $catName = category::categoryName($catVal);
                }else{
                        $catName = 'Category';
                }

                for($i=0;$i<count($products);$i++)
                {
                        $item = array();
                        $totalComments = itemcomments::countComments($products[$i]->id);
                        $userwished = wishlistitmes::countWished($products[$i]->id);

                        /// latest item
                        //$timeAdded = wishlistitmes::latestItem($products[$i]->id);
                        $timeAdded = wishlistitmes::latestItemHome($products[$i]->id);
                        $addeduser = users::getUserDetails($timeAdded['user_id']);


                        ///check this item is already in you wishlist
                        //$itemwhished = wishlistitmes::chkWishedItem($product_id,$_SESSION['user_id']);
                        $itemwhished = wishlistitmes::chkWishedItem($products[$i]->id,$_SESSION['user_id']);
                        if($itemwhished)
                        {
                                $item['youwhished'] = 'Y';
                        }else{
                                $item['youwhished'] = 'N';
                        }

                        $item['comment'] = $totalComments;
                        $item['wished'] = $userwished;
                        $item['added_time'] = $timeAdded['time'];
                        //$item['addedby'] = $addeduser->first_name.' '.$addeduser->last_name;
                        $item['first_name'] = $addeduser->first_name;
                        $item['last_name'] = $addeduser->last_name;
                        $item['user_id'] = $addeduser->user_id;

                        $items[] = $item;
                }
                //print_r($products);
                //exit;//
                $this->view->items = $products;
                $this->view->extval = $items;
                $this->view->categoryName = $catName;
                //exit;

                $logger->info("[END] " . __CLASS__ . ' : ' . __FUNCTION__);
	}
	
        public function productdetailAction()
	{
		$db = Zend_Registry::get("db");
		$config = Zend_Registry::get("config");
		$server = Zend_Registry::get("server");
	        $logger = Zend_Registry::get("logger");
	
		$params = $this->getRequest()->getParams();
		$product_id = $params['pid'];
		//$this->view->buyfr = $params['buyfr'];
		//$this->view->by = $params['by'];
	        $this->view->pgid = $params['pgid'];

                $page = $params['pgid'];
                $rceiver = 0;
                 $usrId = 0;
                if ($page == '1') {
                    $usrId = $_SESSION['user_id'];
                } else if ($page == '2') {
                    $usrId = $_SESSION['friend_id'];
                    $logger->info("Chikke friend_id = ". $_SESSION['friend_id']);
		    $rceiver =  $_SESSION['friend_id'];

                    $isfollow = usersfriend::checkExistFriendStatus($_SESSION['user_id'], $_SESSION['friend_id']);
                    if(($isfollow != '') && ($isfollow->status == '1'))
                    {
                        $followingthis = 'Y';
                    }else{
                        $followingthis = 'N';
                    }
                    $this->view->followthis = $followingthis;
                }

                $this->view->rceiver =  $rceiver;

		try{
		///product
		$productDetail = wishlistitmes::getProductDetailUserId($product_id, $usrId);
		//$productDetail = wishlistitmes::getProductDetail($product_id);	
		
		//print_r($productDetail);	
		$this->view->productDetail = $productDetail[0];
		$userwished = wishlistitmes::countWished($product_id);
		$this->view->userwished = $userwished;
		
		$itemwhished = wishlistitmes::chkWishedItem($product_id,$_SESSION['user_id']);
		
                /*
		if($itemwhished)
		{
			$this->view->itemwhished = 'Y';
		}else{
			$this->view->itemwhished = 'N';
		}
                */
                $this->view->itemwhished = $itemwhished;		
		/// added by latest friend  
		//$latestfrnd = wishlistitmes::latestItemUser($product_id, $_SESSION['user_id']);
		$latestfrnd = wishlistitmes::latestItemHome($product_id, $_SESSION['user_id']);
		$altestAddedfrnd = $latestfrnd['user_id'];
		$this->view->receiverfriend = $altestAddedfrnd;
		
	          //get data for show to dropdown
                $shareOptions = wishlistitmes::getShareOption($_SESSION['user_id'], $product_id);
                $showTo = $shareOptions->privacy;
                $sharedWithGroups = explode(',',$shareOptions->sharewith_group);
                $createdGroups = groups::groupList($_SESSION['user_id']);
                if ($showTo == '3') {
                   $individual = users::getUserDetails($shareOptions->share_individual);
                   $this->view->individual = $individual;
                }
                $this->view->showto = $showTo;
                $this->view->created_groups = $createdGroups;
                $this->view->shared_with_groups = $sharedWithGroups;
	
		/// latest item
		$timeAdded = wishlistitmes::latestItem($product_id);
		$this->view->timeAdded = $timeAdded['time'];
                $this->view->timeReserved = $timeAdded['reserved_on'];
		$addeduser = users::getUserDetails($timeAdded['user_id']);
	        $logger->info("CHikke timeAdded ". $timeAdded['time'] . " timeReserved ". $timeAdded['reserved_on']);
		$this->view->addedby = $addeduser->first_name.' '.$addeduser->last_name;
		$this->view->addedbyuserID = $addeduser->user_id;
		
		if($addeduser->last_name != '')
		{
			$this->view->addeduserpic = $addeduser->profile_pic;
		}else{
		}
		
		//user
		$user_details = users::getUserDetails($_SESSION['user_id']);
		$this->view->userDetail = $user_details;
		
		//comments
		$comments = itemcomments::showComments($product_id);
		$this->view->comments = $comments;
		
		/// Get list of tags which are created by logged in user
		$usertags = category::usersTags($_SESSION['user_id']);
		$this->view->usertags = $usertags;
		//print_r($usertags);
		
		//get item's taglist
		$taglist = category::getItemTags($product_id, $_SESSION['user_id']);
		$this->view->taglist = $taglist;
                $related_tags = '';
                for ($j = 0; $j < count($taglist); $j++) {
                     if ($related_tags == '') {
                        $related_tags = $taglist[$j]->name;
                     } else {
                        $related_tags = $related_tags . '|' . $taglist[$j]->name; 
                     }
                }

                $numberOfItems = 20;
                $orderBy = '';

                $products = wishlistitmes::getLoadMoreFeedPage($_SESSION['user_id'],$orderBy,$related_tags,0,$numberOfItems,0);
                $this->view->items_related = $products;

		// is buy gift for event
		$event_id = $params['event_id'];
		
		if($event_id != ""){
			$receiver_id = event::getEventCreatedUserIDByEventID($event_id);			
			$is_buyed = event::isbuyGiftForEvent($_SESSION["user_id"],$product_id,$receiver_id,$event_id);
			
			if($is_buyed){			
				$this->view->is_buyed='Yes';
			}
			else{
				$this->view->is_buyed='No';
			}
			$this->view->event_id=$event_id;
		}else{
			if($params['rceiver'] != '')
			{
				$receiver_id = $params['rceiver'];
			}else{
				$receiver_id = $altestAddedfrnd;
			}
			$is_buyed = event::isbuyGiftItem($_SESSION["user_id"],$product_id,$receiver_id);
			$quantity = wishlistitmes::getQty($product_id,$receiver_id);
			if($is_buyed && $quantity == 0){			
				$this->view->is_buyed='Yes';
			}
			else{
				$this->view->is_buyed='No';
			}
		}
		
		/// Get Quantity
		
		if($event_id !=""){
			$quantity = wishlistitmes::getQty($product_id,$receiver_id);
		}
		else if($params['rceiver'] != '')
		{
			$quantity = wishlistitmes::getQty($product_id,$params['rceiver']);
		}
		else{
			//$quantity = wishlistitmes::getQty($product_id,$_SESSION['user_id']);
			$quantity = wishlistitmes::getQty($product_id,$altestAddedfrnd);
		}
		$this->view->quantity = $quantity;
		
		// check received any gift for any event
		$is_received=event::checkReceivedGift($_SESSION["user_id"],$product_id);
		
		if($is_received){
			$this->view->is_received = $is_received;
		}
		
		$is_frienditem = usersfriend::chkFriendItem($product_id, $_SESSION["user_id"]);
		
		if($is_frienditem){
			$this->view->is_frienditem = 'Y';
		}else{
			$this->view->is_frienditem = 'N';
		}
		
		if($params['latuser'] != '')
		{
			$this->view->latestaddeduser = $params['latuser'];
			
			/// from home it 
			//check it is current user's item
			if($params['latuser'] == $_SESSION['user_id'])
			{
				$this->view->myitem = 'Y';
			}else{
				$this->view->myitem = 'N';
			}
			
			// check is latest user is friend of current user
			$isfriendlatestuser = usersfriend::checkExistfollower($_SESSION['user_id'], $params['latuser']);
			
			if($isfriendlatestuser)
			{
				if($isfriendlatestuser->status == 1)
				{
					$this->view->isfriendlatestuser = 'Y';
				}else{
					$this->view->isfriendlatestuser = 'N';
				}
			}else{
				$this->view->isfriendlatestuser = 'N';
			}
        $this->view->isProductDetailPage = true;
		}
		
		}catch(Exception $e){
			//Rollback transaction
			$db->rollBack();
			print_r($e->getMessage());
			$this->view->errors[] = $e->getMessage();
			exit;
			
		}
	}//

	public function itemdetailsAction() {
		$server = Zend_Registry::get("server");
		$config = Zend_Registry::get("config");
		$logger = Zend_Registry::get("logger");
		
		//disable layouts for this action
		$this->_helper->layout()->disableLayout();
		
		$nValue = $this->getRequest()->getParam("n", false);
                $by =  $this->getRequest()->getParam("by", false);
                $buyfr =  $this->getRequest()->getParam("buyfr", false);
                $rceiver = $this->getRequest()->getParam("rceiver", false);
                $curate = $this->getRequest()->getParam("curate", 0);

                $usrId = 0;
                $page = $this->getRequest()->getParam("page", false);
                if ($page == 'landing') {
                    $this->view->vpage = 1;
                } else if ($page == 'home') {
                    $usrId = $_SESSION['user_id'];
                    $this->view->vpage = 2;
                } else if ($page == 'friends') {
                    $usrId = $_SESSION['friend_id'];
                    $this->view->vpage = 4;

                    $isfollow = usersfriend::checkExistFriendStatus($_SESSION['user_id'], $_SESSION['friend_id']);
                    if(($isfollow != '') && ($isfollow->status == '1')) 
                    {
                        $followingthis = 'Y';
                    }else{
                        $followingthis = 'N';
                    }
                    $this->view->followthis = $followingthis;
                } else /*feed */ {
                    $this->view->vpage = 3;
                }


		// Set the value for view to use it
                $this->view->by = $by;
		$this->view->buyfr = $buyfr;
                $this->view->rceiver = $_SESSION['friend_id']; //$rceiver;
                $this->view->comment_offset = 0;
                $this->view->number_of_comments_displayed = 3;
                $this->view->curate = $curate;
                $product_id = $nValue;

		try{
            $productDetail = wishlistitmes::getProductDetailUserId($product_id, $usrId);
            $this->view->productDetail = $productDetail; 
            /* vendor link modification for mobile and desktop broswers */  
            if (strpos($productDetail[0]->vendor_link, "amazon.com") != false) {
                $link = str_replace("gp/aw/d","dp",$productDetail[0]->vendor_link);
                $this->view->productDetail[0]->vendor_link = $link;
            } 

            $userwished = wishlistitmes::chkWishedItem($product_id, $_SESSION['user_id']);
            $this->view->userwished = $userwished;
            $countwishes = wishlistitmes::countWished($product_id);
            $this->view->countwishes = $countwishes;
            $itemwhished = wishlistitmes::chkWishedItem($product_id,$_SESSION['user_id']);

            if($itemwhished)
            {
                $this->view->itemwhished = 'Y';
            }else{
                $this->view->itemwhished = 'N';
            }
            /// added by latest friend
            $latestfrnd = wishlistitmes::latestItemHome($product_id, $_SESSION['user_id']);
            $altestAddedfrnd = $latestfrnd['user_id'];
            $this->view->receiverfriend = $altestAddedfrnd;
            $this->view->events = event::getItemEvents($_SESSION["user_id"], $product_id, ($this->view->vpage == 3) ? true : false);

            //get data for show to dropdown
            $shareOptions = wishlistitmes::getShareOption($_SESSION['user_id'], $product_id);
            $showTo = $shareOptions->privacy;
            $sharedWithGroups = explode(',',$shareOptions->sharewith_group);
            $createdGroups = groups::groupList($_SESSION['user_id']);
            if ($showTo == '3') {
                $individual = users::getUserDetails($shareOptions->share_individual);
                $this->view->individual = $individual;
            }
            $this->view->showto = $showTo;
            $this->view->created_groups = $createdGroups;
            $this->view->shared_with_groups = $sharedWithGroups;
//            $events =
//            $this->view->events = $events;


            /// latest item
            $timeAdded = wishlistitmes::latestItem($product_id);
            $this->view->timeAdded = $timeAdded['time'];
            $addeduser = users::getUserDetails($timeAdded['user_id']);

            $this->view->addedby = $addeduser->first_name.' '.$addeduser->last_name;
            $this->view->addedbyuserID = $addeduser->user_id;

            if($addeduser->last_name != '')
            {
                $this->view->addeduserpic = $addeduser->profile_pic;
            }else{
            }

            //user
            $user_details = users::getUserDetails($_SESSION['user_id']);
            $this->view->userDetail = $user_details;
            
            //Original user 
            $user_details_original = users::getUserDetails($productDetail[0]->user_id);
            $this->view->userDetailOriginalUser = $user_details_original->first_name.' '.$user_details_original->last_name; //$user_details_original;

            //comments
            $comments = itemcomments::showComments($product_id);
            $this->view->comments = $comments;
            $this->view->count_comments = count($comments);

            if ($page == 'home') {
                 // Get list of tags which are created by logged in user
                 //$taglist = category::usersTags($_SESSION['user_id']);
                 $taglist = category::getItemTags($product_id, $_SESSION['user_id']);
            } else {
                //get item's taglist
                 $taglist = category::getItemTags($product_id, null);
            }
            $this->view->taglist = $taglist;

            // is buy gift for event
            $event_id = $params['event_id'];

            if($event_id != ""){
                $receiver_id = event::getEventCreatedUserIDByEventID($event_id);
                $is_buyed = event::isbuyGiftForEvent($_SESSION["user_id"],$product_id,$receiver_id,$event_id);

                if($is_buyed){
                    $this->view->is_buyed='Yes';
                }
                else{
                    $this->view->is_buyed='No';
                }
                $this->view->event_id=$event_id;
            }else{
                if($params['rceiver'] != '')
                {
                    $receiver_id = $params['rceiver'];
                }else{
                    $receiver_id = $altestAddedfrnd;
                }
                $is_buyed = event::isbuyGiftItem($_SESSION["user_id"],$product_id,$receiver_id);
                $quantity = wishlistitmes::getQty($product_id,$receiver_id);
                if($is_buyed && $quantity == 0){
                    $this->view->is_buyed='Yes';
                }
                else{
                    $this->view->is_buyed='No';
                }
            }

            /// Get Quantity

            if($event_id !=""){
                $quantity = wishlistitmes::getQty($product_id,$receiver_id);
            }
            else if($params['rceiver'] != '')
            {
                $quantity = wishlistitmes::getQty($product_id,$params['rceiver']);
            }
            else{
                //$quantity = wishlistitmes::getQty($product_id,$_SESSION['user_id']);
                $quantity = wishlistitmes::getQty($product_id,$altestAddedfrnd);
            }
            $this->view->quantity = $quantity;

            // check received any gift for any event
            $is_received=event::checkReceivedGift($_SESSION["user_id"],$product_id);

            if($is_received){
                $this->view->is_received = $is_received;
            }

            $is_frienditem = usersfriend::chkFriendItem($product_id, $_SESSION["user_id"]);

            if($is_frienditem){
                $this->view->is_frienditem = 'Y';
            }else{
                $this->view->is_frienditem = 'N';
            }

            if($params['latuser'] != '')
            {
                $this->view->latestaddeduser = $params['latuser'];

                /// from home it
                //check it is current user's item
                if($params['latuser'] == $_SESSION['user_id'])
                {
                    $this->view->myitem = 'Y';
                }else{
                    $this->view->myitem = 'N';
                }

                // check is latest user is friend of current user
                $isfriendlatestuser = usersfriend::checkExistfollower($_SESSION['user_id'], $params['latuser']);

                if($isfriendlatestuser)
                {
                    if($isfriendlatestuser->status == 1)
                    {
                        $this->view->isfriendlatestuser = 'Y';
                    }else{
                        $this->view->isfriendlatestuser = 'N';
                    }
                }else{
                    $this->view->isfriendlatestuser = 'N';
                }
            }
		}catch(Exception $e){
			//Rollback transaction
//			$db->rollBack();
			print_r($e->getMessage());
			$this->view->errors[] = $e->getMessage();
			exit;
			
		}
	}
	
	public function itemdetailscommentsAction() {
		$server = Zend_Registry::get("server");
		$config = Zend_Registry::get("config");
		$logger = Zend_Registry::get("logger");
		
		//disable layouts for this action
		$this->_helper->layout()->disableLayout();

        $product_id = $this->getRequest()->getParam("prodId", false);
        $next_start = $this->getRequest()->getParam("next_start", false);

        $logger->info("product-id = " . $product_id);
        $logger->info("next_start = " . $next_start);

        // Set the value for view to use it
        $this->view->comment_offset = $next_start;
        $this->view->number_of_comments_displayed = 4;

         //comments
        try {
            $comments = itemcomments::showComments($product_id);
            $this->view->comments = $comments;
            $this->view->count_comments = count($comments);
        } catch(Exception $e){
                    //Rollback transaction
                    $db->rollBack();
                    print_r($e->getMessage());
                    $this->view->errors[] = $e->getMessage();
                    exit;
            }
	}

    // save changes made on show to dropdown
    public function changeitemprivateAction() {
        $server = Zend_Registry::get("server");
        $config = Zend_Registry::get("config");
        $logger = Zend_Registry::get("logger");
        $db = Zend_Registry::get("db");

        //disable layouts for this action
        //$this->_helper->layout()->disableLayout();

        //get params
        $param = $this->getRequest()->getParams();
        $itemId = $param["item_id"];
        $public = $param["public"];
        $groupIds = $param["group_ids"];
        $individualId = $param["individual_id"];

        $db->beginTransaction();
        try {

            $chkitem = wishlistitmes::chkusertem($_SESSION["user_id"], $itemId);
            if (!empty($groupIds)){
                $data = array('public' => '2', 'sharewith_group' => $groupIds, 'share_individual' => '');
            } else if(!empty($individualId)){
                $data = array('public' => '3', 'sharewith_group' => '', 'share_individual' => $individualId);
            } else {
                $data = array('public' => $public, 'sharewith_group' => '', 'share_individual' => '');
            }
            //if there is an item update it else create a new one
            if($chkitem){
                //update
                $where['user_id = ?'] = $_SESSION["user_id"];
                $where['item_id = ?'] = $itemId;
                $db->update('userwishlist', $data, $where);
            } else {
                //create
                $data['user_id'] = $_SESSION["user_id"];
                $data['item_id'] = $itemId;
                $data['added_date'] = date('Y-m-d H:i:s');
                $data['share_individual'] = '';
                $db->insert('userwishlist', $data);
            }
            $db->commit();
            return $this->_helper->json(array('success' => true));
        } catch(Exception $e){
            //Rollback transaction
            $db->rollBack();
            return $this->_helper->json(array('success' => false, 'msg' => $e->getMessage()));
        }
    }

    public function addcommentAction()
    {
        $db = Zend_Registry::get("db");
        $this->_helper->layout()->disableLayout();
        //check if user is logged
        if(!empty($_SESSION['user_id'])){
            $params = $this->getRequest()->getParams();
            try{

                $db->beginTransaction();
                if($params['comment'] != '')
                {
                    $data['item_id'] = $params['product_id'];
                    $data['user_id'] = $_SESSION['user_id'];
                    $data['comments'] = $params['comment'];
                    $data['comment_date'] = date('Y-m-d H:i:s');

                    $db->insert('itemcomments',$data);
                    $result = $db->lastInsertId();

                    /// record activity
                    $dataC['item_id'] = $params['product_id'];
                    $dataC['activity_type'] = 'comment item';
                    $dataC['activity_date'] = date('Y-m-d H:i:s');
                    $dataC['event_id'] = 0;
                    $dataC['deleted_user'] = 0;
                    $dataC['new'] = true;
                    $dataC['sender'] = $_SESSION['user_id'];
                   
                    $key = '';

                    $shareOptions = wishlistitmes::getShareOption($_SESSION['user_id'], $params['product_id']);
                    switch($shareOptions->privacy){
                        case 1:
                            //everyone
                            $allfriends = usersfriend::prepareForViewAllFriends($key);
                            for ($idx = 0; $idx < count($allfriends); $idx++) {
                                $dataC['receiver'] = $allfriends[$idx]['id']; //$user_id;
                                $db->insert('activity',$dataC);
                            }
                            break;
                        case 2:
                            //groups
                            $friends = usersfriend::getFriendsFromGroupIds($shareOptions->sharewith_group, $_SESSION['user_id']);
                            foreach($friends as $friend){
                                $dataC['receiver'] = $friend->friend_id; //$user_id;
                                $db->insert('activity',$dataC);
                            }
                            break;
                        case 3:
                            //individual
                            $dataC['receiver'] = $shareOptions->share_individual; //$user_id;
                            $db->insert('activity',$dataC);
                            break;
                    }
                 
                    $db->commit();

                    $comments = itemcomments::showComments($params['product_id']);
                    $this->view->comment_offset = 0;
                    $this->view->number_of_comments_displayed = 1;
                    $this->view->comments = $comments;
                    $this->view->count_comments = count($comments);
                    $this->render('itemdetailscomments');
                }
            }catch(Exception $e){
                //Rollback transaction
                $db->rollBack();
                $this->view->errors[] = $e->getMessage();
                return $this->_helper->json(array('fail'=> true, 'msg' => $e->getMessage()));
            }
        } else {
            return $this->_helper->json(array('fail'=> true, 'msg' => 'Please login first!'));
        }
    }//add

    /// Add new tag and tag assign it to the item
    public function addsavetagAction()
    {

        $db = Zend_Registry::get("db");
        $config = Zend_Registry::get("config");
        $server = Zend_Registry::get("server");

        $this->_helper->layout()->disableLayout();
        $param = $this->getRequest()->getParams();

        if(!empty($_SESSION['user_id'])){
            $tag_name = $param['tag_name'];
            $item_id = $param['item_id'];
            $user_id = $_SESSION['user_id'];

            $tname = $db->quote($param['tag_name']);
            try{
                $db->beginTransaction();

                $dataT['name'] = $tag_name;
                $dataT['created_by'] = $user_id;
                $dataT['status'] = 1;

                $totalTags = category::countItemTags($item_id, $user_id);
                if(count($totalTags) < 5)
                {
                    // check it it is already added
                    $checkTagID = category::checkTags($tag_name, $user_id);
                    if($checkTagID != '')
                    {
                        // already added
                        $tag_id = $checkTagID;
                    }else{
                        // add
                        $db->insert('category',$dataT);
                        $tag_id =  $db->lastInsertId();
                    }
                    //add this tag to DB

                    //
                    $data['item_id'] = $item_id;
                    $data['tag_id'] = $tag_id; // tag_id is the category id
                    $data['user_id'] = $user_id;
                    $data['tag_date'] = date('Y-m-d H:i:s');

                    //check for already added
                    $checkID = category::checkItemTags($item_id, $tag_id, $user_id);
                    if(!empty($checkID))
                    {
                        // user already tagged this item in this tag
                        return $this->_helper->json(array('fail' => true, 'msg' => 'user already tagged item in this tag'));
                    }else{
                        // tag item
                        $db->insert('item_tags', $data);
                        $db->commit();
                        $taglist = array();
                        $taglist[0]->id = $tag_id;
                        $taglist[0]->name = $tag_name;
                        $taglist[0]->created_by = $user_id;
                        $productDetail = array();
                        $productDetail[0]->id = $item_id;
                        $userDetail = new stdClass();
                        $userDetail->user_id = $user_id;
                        $this->view->taglist = $taglist;
                        $this->view->productDetail = $productDetail;
                        $this->view->userDetail = $userDetail;
                        return $this->render('taglist');
                    }

                }else{
                    return $this->_helper->json(array('fail' => true, 'msg' => 'This Item has maximum no. (5) of tags, please remove tag to add new Tag'));
                }
                $db->commit();

                exit;
            }catch(Exception $e)
            {
                $db->rollBack();
                return $this->_helper->json(array('fail' => true, 'msg' => 'There are some problems. Please try later', 'err' => $e->getMessage()));
            }
        } else {
            return $this->_helper->json(array('fail' => true, 'msg' => 'Please login first!'));
        }
    }////

    /// Add new tag and tag assign it to the item
    public function addtagsAction()
    {
        $this->_helper->layout()->disableLayout();
        $param = $this->getRequest()->getParams();

        if(!empty($_SESSION['user_id'])){
            $tag_names = $param['tag_names'];
            $item_id = $param['item_id'];
            $user_id = $_SESSION['user_id'];

            try{
                $result = array();
                if($item_id != '' && $tag_names != '') {
                    $tag_names = explode(',', $tag_names);
                    foreach($tag_names as $tag_name) {
                        $result[] = wishlistitmes:: addItemTag($item_id, $tag_name, $user_id);
                    }
                    return $this->_helper->json(array('fail' => false, 'tags' => $result));
                }
            }catch(Exception $e)
            {
                return $this->_helper->json(array('fail' => true, 'msg' => 'There are some problems. Please try later', 'err' => $e->getMessage()));
            }
        } else {
            return $this->_helper->json(array('fail' => true, 'msg' => 'Please login first!'));
        }
    }////
    
    /// delete tag of an item
    // user_id, item_id and tag_id are input
    public function deletetagAction()
    {
        $db = Zend_Registry::get("db");
        $config = Zend_Registry::get("config");
        $server = Zend_Registry::get("server");
        if(!empty($_SESSION['user_id'])){
            $param = $this->getRequest()->getParams();
            $userID = $param['user_id'];
            $tag_id = $param['tag_id'];
            $item_id = $param['item_id'];

            try{
                $db->beginTransaction();

                if($userID != '' && $item_id != '' && $tag_id != '')
                {
                    $res = $db->delete('item_tags', array("user_id = ?" => $userID, "item_id = ?" => $item_id, "tag_id = ?" =>$tag_id));
                    $db->commit();
                    return $this->_helper->json(array('success' =>true, 'msg' => 'Tag removed successfully!'));
                }
            }catch(Exception $e)
            {
                $db->rollBack();
                return $this->_helper->json(array('success' =>false, 'msg' => $e->getMessage()));
            }
        } else{
            return $this->_helper->json(array('success' => false, 'msg' => 'Please login first!'));
        }
    }//end function

    /// delete tag of an item
    // user_id, item_id and tag_id are input
    public function deletetagsAction()
    {
        $db = Zend_Registry::get("db");
        $config = Zend_Registry::get("config");
        $server = Zend_Registry::get("server");
        if(!empty($_SESSION['user_id'])){
            $param = $this->getRequest()->getParams();
            $userID = $_SESSION['user_id'];
            $tag_ids = $param['tag_ids'];
            $item_id = $param['item_id'];

            try{
                $db->beginTransaction();
                if($userID != '' && $item_id != '' && $tag_ids != '')
                {   
                    $tag_ids = explode(',', $tag_ids);
                    foreach ($tag_ids as $tag_id) {
                        $db->delete('item_tags', array("user_id = ?" => $userID, "item_id = ?" => $item_id, "tag_id = ?" =>$tag_id));
                    }
                    $db->commit();
                    return $this->_helper->json(array('success' =>true, 'msg' => 'Tag removed successfully!'));
                }
            }catch(Exception $e)
            {
                $db->rollBack();
                return $this->_helper->json(array('success' =>false, 'msg' => $e->getMessage()));
            }
        } else{
            return $this->_helper->json(array('success' => false, 'msg' => 'Please login first!'));
        }
    }//end function

    public function getitemidbyhashAction(){
        $server = Zend_Registry::get("server");
        //disable layouts for this action
        $this->_helper->layout()->disableLayout();
        //get params
        $hash = $this->getRequest()->getParam("hash", false);
        $products = wishlistitmes::getItemIdByHash($hash);
        for($i=0;$i<count($products);$i++)
        {
            $item = array();
            $size = getimagesize('https://' . $server->apphost . '/productimg/thumb/'.$products[$i]->image);
            $height = explode(' ', $size[3]);

            $totalComments = itemcomments::countComments($products[$i]->id);
            $userwished = wishlistitmes::countWished($products[$i]->id);

            //$timeAdded = wishlistitmes::latestItem($products[$i]->id);
            $timeAdded = wishlistitmes::latestItemHome($products[$i]->id);
            $addeduser = users::getUserDetails($timeAdded['user_id']);


            ///check this item is already in you wishlist
            //$itemwhished = wishlistitmes::chkWishedItem($product_id,$_SESSION['user_id']);
            $itemwhished = wishlistitmes::chkWishedItem($products[$i]->id,$_SESSION['user_id']);
            if($itemwhished)
            {
                $item['youwhished'] = 'Y';
            }else{
                $item['youwhished'] = 'N';
                $userwished = 0;
            }
            //

            $item['comment'] = $totalComments;
            $item['wished'] = $userwished;
            $item['added_time'] = $timeAdded['time'];
            //$item['addedby'] = $addeduser->first_name.' '.$addeduser->last_name;
            $item['first_name'] = $addeduser->first_name;
            $item['last_name'] = $addeduser->last_name;
            $item['user_id'] = $addeduser->user_id;

            $items[] = $item;
        }
        $this->view->ajax = 1;
        $this->view->items = $products;
        $this->view->extval = $items;
        $this->view->imageHeight = $height[1];
        $this->view->categoryName = 'Category';
        $this->render("productsgrid");
    }

    public function addproducturlAction()
        {
            $db = Zend_Registry::get("db");
            $config = Zend_Registry::get("config");
            $server = Zend_Registry::get("server");
            $logger = Zend_Registry::get("logger");

            $params = $this->getRequest()->getParams();
            $this->_helper->layout()->disableLayout();
            $logger->info("Add from - product url:". $params['producturl']);
            $url = str_replace("%3C%3E", "<>",$params['producturl']);
            $logger->info("Add from - product url:" . $url);
            $page = file_get_contents($url);
            $page = str_replace("\n", '', $page);
            $page = str_replace('http:', 'http:', $page);
             if (strlen($page) > 0 ) {
                 $logger->info(strlen($page));
                 $this->view->page = $page;
		 //$this->_helper->json(array('success' => true, 'msg' =>"CHikke"));
             } else {
                 $this->_helper->json(array('fail' => true, 'msg' =>"failed"));
             }
        }

    public function showproductAction(){
        $server = Zend_Registry::get("server");
//        get params

        $hash = $this->getRequest()->getParam("hash", false);
        $params = $_GET;
        $params['hash'] = '#'.$hash;
        $paramsString = http_build_query($params);
        http_build_query($params);
        $products = wishlistitmes::getItemIdByHash($hash);
        for($i=0;$i<count($products);$i++)
        {
            $item = array();
            $size = getimagesize('https://' . $server->apphost . '/productimg/thumb/'.$products[$i]->image);
            $height = explode(' ', $size[3]);

            $totalComments = itemcomments::countComments($products[$i]->id);
            $userwished = wishlistitmes::countWished($products[$i]->id);

            //$timeAdded = wishlistitmes::latestItem($products[$i]->id);
            $timeAdded = wishlistitmes::latestItemHome($products[$i]->id);
            $addeduser = users::getUserDetails($timeAdded['user_id']);


            ///check this item is already in you wishlist
            //$itemwhished = wishlistitmes::chkWishedItem($product_id,$_SESSION['user_id']);
            $itemwhished = wishlistitmes::chkWishedItem($products[$i]->id,$_SESSION['user_id']);
            if($itemwhished)
            {
                $item['youwhished'] = 'Y';
            }else{
                $item['youwhished'] = 'N';
                $userwished = 0;
            }
            //

            $item['comment'] = $totalComments;
            $item['wished'] = $userwished;
            $item['added_time'] = $timeAdded['time'];
            //$item['addedby'] = $addeduser->first_name.' '.$addeduser->last_name;
            $item['first_name'] = $addeduser->first_name;
            $item['last_name'] = $addeduser->last_name;
            $item['user_id'] = $addeduser->user_id;

            $items[] = $item;
        }
        $this->view->ajax = 1;
        $this->view->items = $products;
        $this->view->extval = $items;
        $this->view->imageHeight = $height[1];
        $this->view->categoryName = 'Category';
        $this->view->showOG = true;
    }

    public function changeitemeventAction(){

        $db = Zend_Registry::get("db");
        $this->_helper->layout()->disableLayout();
        $param = $this->getRequest()->getParams();
        $itemId = $param["item_id"];
        $add = $param["add"];
        $eventId = $param["event_id"];
        $success = false;
        if(!empty($_SESSION['user_id'])){
            try{
                $db->beginTransaction();
                if($add){
                    $data['event_id'] = $eventId;
                    $data['item_id'] = $itemId;
                    $db->insert('event_items', $data);
                    //check if item is on wishlist
                    $chkWished = wishlistitmes::chkWishedItem($param['item_id'],$_SESSION['user_id']);
                    if(!$chkWished){
                        $itemData['item_id'] = $itemId;
                        $itemData['user_id'] = $_SESSION['user_id'];
                        $itemData['added_date'] = date('Y-m-d H:i:s');
                        $itemData['sharewith_group'] = '';
                        $itemData['share_individual'] = '';

                        $datai = array(
                            'item_count'      => new Zend_DB_Expr('item_count + 1')
                        );

                        $db->insert('userwishlist',$itemData);
                        $result = $db->lastInsertId();
                        $db->update('wishlistitmes',$datai,'id='.$param['item_id']);
                        if($result)
                        {
                            $where = array("item_id = ?" => $param['item_id'],"user_id = ?" => $param['user_id']);
                            $db->update('userwishlist', $where);
                            $success = true;
                            $msg = 'Item added on event and in your wishlist';
                        }else{
                            $msg = 'Error! in adding item to wishlist';
                        }

                    }
                } else {
                    $res = $db->delete('event_items', array("item_id = ?" => $itemId, "event_id = ?" => $eventId));
                    $success = true;
                    $msg = 'Item removed from event';
                }
                $db->commit();
            } catch(Exception $e) {
                $db->rollBack();
                $msg = $e->getMessage();
            }
        } else {
            $msg = 'Please login first';
        }
        $countwishes = wishlistitmes::countWished($param['item_id']);
        return $this->_helper->json(array('success' =>$success, 'msg' => $msg, 'countwishes' => $countwishes));
    }

    /// delete activity
    public function deleteactivityAction()
    {
        $db = Zend_Registry::get("db");
        $config = Zend_Registry::get("config");
        $server = Zend_Registry::get("server");

        $param = $this->getRequest()->getParams();

        $user_id = $param['user_id'];
        $activity_id = $param['activity_id'];

        //exit;

        try{
            $db->beginTransaction();
            if($user_id != '' && $activity_id != '')
            {
                // check any user is already deleted this activity
                // if one user is deleted the remove this activity from DB,
                // else only set deleted_user value
                $chkid = users::chkActivityRemoved($activity_id);

                if($chkid != '' && $chkid != 0)
                {
                    if($chkid == $user_id)
                    {
                        /// do nothing
                    }else{
                        /// delete this activity from DB
                        $res = $db->delete('activity', array("id = ?" => $activity_id));
                        echo 'D';
                    }
                }else{
                    //update DB
                    $datau['deleted_user'] = $user_id;
                    $res = $db->update('activity', $datau, array("id = ?" => $activity_id));
                    echo 'updated';
                }
                $db->commit();


            }
            exit;
        }catch(Exception $e)
        {
            print_r($e->getMessage());
            exit;
        }
    }

    protected function recommenditemuseremail($itemID, $touser) {
                $to_user = users::getUserDetails($touser);
                if ($to_user->email_id != '') {
                    $email_id = $to_user->email_id;
                } else {
                    $email_id = $to_user->facebook_email;
                }
                if ($email_id == '') {
                    return;
                }
                try{
                       $this->recommenditememail($itemID, $email_id, $to_user->first_name);
                }
                catch(Exception $e)
                {
                    throw $e;
                }
    }

    protected function recommenditememail($itemID, $email_id, $name) {
        $server = Zend_Registry::get("server");
        $config = Zend_Registry::get("config");
        try{
            $user_details = users::getUserDetails($_SESSION['user_id']);
            $mailText = '
 <html><head>

<!-- Define Charset -->
        <meta http-equiv=Content-Type content=text/html; charset=UTF-8 />
        
        <!-- Responsive Meta Tag -->
        <meta name=viewport content=width=device-width; initial-scale=1.0; maximum-scale=1.0; />
        <link href="http://fonts.googleapis.com/css?family=Questrial" rel="stylesheet" type="text/css">
        
        <!--- Importing Twitter Bootstrap icons -->
        <link href=http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css rel=stylesheet>

</head><body>
<div marginheight= 0  marginwidth= 0 >
	<table style= background-color:rgb(21,18,18)  cellpadding= 0  cellspacing= 0  bgcolor= ecebeb  border= 0  width= 100% >
		<tbody><tr>
		<td>
				<table cellpadding= 0  cellspacing= 0  align= center  border= 0  width=100% >
					<tbody><tr><td style= font-size:30px;line-height:30px  height= 30 > </td></tr>
					<tr>
						<td>
							<table style= border-collapse:collapse  cellpadding= 0  cellspacing= 0  align=left  border=0 width=45% >
								<tbody><tr>
									<td align= center >
										<table cellpadding= 0  cellspacing= 0  align= center  border=0 >
											<tbody><tr>
												<td style= line-height:21px  align= center >

	<a href="'.$server->securehttp.$server->apphost.'"  style=display:block;border-style:none!important;border:0!important  target= _blank ><img style= display:block;width:128px  src="'.$server->securehttp.$server->apphost.'/newdesign/img/tpl/new-giftvise-logo.png" width=180 ></a>
												</td>			
											</tr>
										</tbody></table>		
									</td>
								</tr>
								
							</tbody></table>
							
							<table style=border-collapse:collapse  cellpadding= 0  cellspacing=0  align=left  border=0  width=5px>
								<tbody><tr><td style= font-size:20px;line-height:20px  height=20  width=5px > </td></tr>
							</tbody></table>
							
							<table style=border-collapse:collapse  cellpadding=10  cellspacing= 0  align= right  border=0 width=30%>
								
								<tbody><tr>
									<td>
										<table style=border-collapse:collapse  cellpadding= 0  cellspacing= 0  align= left  border= 0 >
			
				                			<tbody><tr>
				                				<td>
				                					<table style=border-collapse:collapse  cellpadding= 0  cellspacing= 0  align=center>
				                						<tbody><tr>
				                							
							                				<td style= color:#ffffff;font-size:14px;font-family:&#39;Questrial&#39;,Helvetica,sans-serif;line-height:24px  align= center >
							                						<div style= line-height:24px >
							                						<span>
<a href="'.$server->securehttp.$server->apphost.'/modals/modalshowhelpcenter?tab=about" style=color:#878b99;text-decoration:none >About</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="'.$server->securehttp.$server->apphost.'/modals/modalshowhelpcenter?tab=faq" style= color:#878b99;text-decoration:none>Help</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a><a href="'.$server->securehttp.$server->apphost.'/modals/modalshowhelpcenter?tab=contact" style= color:#878b99;text-decoration:none >Contact</a>
								                					
							                						</span>
							                					</div>	
							                				</td>
				                						</tr>
				                					</tbody></table>
				                				</td>
				                			</tr>
			                			</tbody></table>
									</td>
								</tr>	
							</tbody></table>	
						</td>
					</tr>
					
					<tr>
						<td style= line-height:21px  align= center >
						</td>			
					</tr>
					
					<tr><td style= font-size:50px;line-height:50px  height= 50 > </td></tr>
					
					<tr>
						<td style= color:#ffffff;font-size:40px;font-family:&#39;Questrial&#39;,Helvetica,sans-serif;line-height:28px  align= center >
							
							
							
							<div style= line-height:28px >
								<span>Hi '.$name .'!</span>
							</div>
        				</td>
					</tr>
					
					<tr><td style= font-size:25px;line-height:25px  height= 25 > </td></tr>
					
					<tr>
						<td align= center >
							<table cellpadding= 0  cellspacing= 0  align= center  border= 0  width= 540 >
								<tbody><tr>
									
									<td style= color:#878b99;font-size:18px;font-family:&#39;Questrial&#39;,Helvetica,sans-serif;line-height:30px  align= center >
										<div style= line-height:30px >
											<span>'. $user_details->first_name.' '. $user_details->last_name.' thinks this is just your style! <br>Click the button below to view it on Giftvise. If you like it, add it to your wishlist!</span>
                                                                                
										</div>
			        				</td>	
								</tr>
							</tbody></table>
						</td>
					</tr>
					
						<tr><td style= font-size:35px;line-height:35px  height= 35 > </td></tr>
					
					<tr>
						<td align= center >
							
							<table style= border-radius:50px  cellpadding= 0  cellspacing= 0  align= center  bgcolor= f06e6a  border= 0  width= 240 >
								
								<tbody><tr><td style= font-size:18px;line-height:18px  height= 18 > </td></tr>
								
								<tr>
	                				<td style= color:#ffffff;font-size:18px;font-family:&#39;Questrial&#39;,Helvetica,sans-serif  align= center >
	                					
	                					
		                    			<div style= line-height:24px >
<a style=text-decoration:none href="' . $server->securehttp . $server->apphost. '/products/productdetail?pid='.$itemID.'">
		                    				<span style= color:#ffffff;text-decoration:none >See Product</span>
		                    	             </a>		</div>
		                    		</td>								
								</tr>
								
								<tr><td style= font-size:18px;line-height:18px  height= 18 > </td></tr>
							
							</tbody></table>
						</td>
					</tr>
					
					<tr><td style= font-size:65px;line-height:65px  height= 65 > </td></tr>
					
				</tbody></table>
			</td></tr>
		
	</tbody></table>
	
<!--	
	<table cellpadding= 0  cellspacing= 0  bgcolor= 363b49  border= 0  width= 100% >
							
		<tbody><tr><td style= font-size:20px;line-height:20px  height= 20 > </td></tr>
					
		<tr>
			<td align= center >
				
				<table cellpadding= 0  cellspacing= 0  align= center  border= 0  width= 590 >
					
					<tbody><tr>
						<td align= center >
								                		
	                		<table cellpadding= 0  cellspacing= 0  align= center  border= 0 >
	                			<tbody><tr>
	                				<td style= color:#5a5f70;font-size:14px;font-family:&#39;Questrial&#39;,Helvetica,sans-serif;line-height:25px  align= center >
	                					<div style= line-height:25px >
	                						<span>
		                					
		                						Copyright @ Giftvise 2014
		                					
	                						</span>
	                					</div>	
	                				</td>
	                			</tr>
	                			
                			</tbody></table>
                			
						</td>
					</tr>
					
				</tbody></table>
			</td>
		</tr>
		<tr><td style= font-size:20px;line-height:20px  height= 20 > </td></tr>
	</tbody></table>
-->
<!-- copy right secion -->
<table border=0 width=100% cellpadding=0 cellspacing=0 bgcolor=#ffffff class=bg4_color>

 <tbody><tr><td height=20 style=font-size:20px;line-height:20px;>&nbsp;</td></tr>

 <tr> <td align=center>

 <table border=0 align=center width=590 cellpadding=0 cellspacing=0 class=container590>

 <tbody><tr> <td align=center>

 <table border=0 align=center cellpadding=0 cellspacing=0> <tbody><tr> 
<td style=color:#5a5f70;font-size:14px;font-family:&#39;Questrial&#39;,Helvetica,sans-serif;line-height:25px align=center>
<div class=editable_text style=line-height:25px;> <span class=text_container>

 Copyright @ Giftvise 2014

 </span> </div> </td> </tr>

 </tbody></table>

 </td> </tr>

 </tbody></table> </td> </tr>

 <tr><td height=20 style=font-size: 20px; line-height: 20px;>&nbsp;</td></tr>

 </tbody></table>
</div></body></html>


                        ';

            $to = $email_id;
            $subject = $user_details->first_name.' '. $user_details->last_name.' thinks this would be perfect for you!'; // . $productDetail[0]->item_name .' to you on Giftvise';
            $transport = new Zend_Mail_Transport_Sendmail($config->supportEmail);
            Zend_Mail::setDefaultTransport($transport);
            $mail = new Zend_Mail();
            $mail->setSubject($subject);
            $mail->setFrom($config->supportEmail, 'Giftvise');
            $mail->addTo($to);
            $mail->setBodyHtml($mailText);
            $mail->send();
        }
        catch(Exception $e)
        {
            throw $e;
        }
    }


    /* get item_id as input and selected user_id */
    /* fetch user's email form DB based on user_id and send mail */
    public function recommenditemAction()
    {
        $db = Zend_Registry::get("db");
        $config = Zend_Registry::get("config");
        $server = Zend_Registry::get("server");

        $params = $this->getRequest()->getParams();
        $product_id = $params['item_id'];
        $user_id = $params['send_userID'];

        try{
            $db->beginTransaction();
            ///Get product detail
            $productDetail = wishlistitmes::getProductDetail($product_id);

            /// record activity
            $data = array();
            $data['sender'] = $_SESSION['user_id'];
            $data['receiver'] = $user_id;
            $data['item_id'] = $product_id;
            $data['activity_type'] = 'recommend item';
            $data['activity_date'] = date('Y-m-d H:i:s');
            $data['event_id'] = 0;
            $data['deleted_user'] = 0;
            $data['new'] = true;

            $db->insert('activity',$data);
            $db->commit();

            return $this->_helper->json(array('success' => true, 'msg' => 'Recommended!'));

        }catch(Exception $e)
        {
            return $this->_helper->json(array('success' => false, 'msg' => $e->getMessage()));
            // $this->view->msg = $e->getMessage();
            // $this->view->errors[] = $e->getMessage();

        }
        //$this->_helper->viewRenderer("sendsearch");
        //$this->_helper->resources("sendsearch");
    }//end send

    /// signup validation
    protected function _initRecommendItemByMail() {

        $this->fields = new PS_Form_Fields(
            array(
                "email" => array(
                    new PS_Validate_EmailAddress(),
                    "filter" => new PS_Filter_RemoveSpaces(),
                    "required" => true
                ),
                'item_id' => array(
                    "required" => true
                )
            )
        );
        $params = $this->getRequest()->getParams();
        $this->fields->setData($params);
        $this->view->fields = $this->fields;
    }

    /* get item_id as input and selected user_id */
    /* fetch user's email form DB based on user_id and send mail */
    public function recommenditembymailAction()
    {
        $db = Zend_Registry::get("db");

        $params = $this->getRequest()->getParams();
        $product_id = $params['item_id'];
        
        try{
            //full name case
            $user_id = users::getUserIdByFullName($params['email']);
            //email case
            if (empty($user_id)){
                $this->_initRecommendItemByMail();
                $this->fields->validate();
                $user_id = users::getUserIdByEmail($params['email']);
            }
            
            ///Get product detail
            $productDetail = wishlistitmes::getProductDetail($product_id);

            /// record activity
            if(!empty($user_id)) {
                $db->beginTransaction();
                $data = array();
                $data['sender'] = $_SESSION['user_id'];
                $data['receiver'] = $user_id;
                $data['item_id'] = $product_id;
                $data['activity_type'] = 'recommend item';
                $data['activity_date'] = date('Y-m-d H:i:s');
                $data['event_id'] = 0;
                $data['deleted_user'] = 0;
                $data['new'] = true;

                $db->insert('activity',$data);
                $db->commit();
            } else {
                $this->recommenditememail($product_id, $params['email'], $params['email']);
            }
            return $this->_helper->json(array('success' => true, 'msg' => 'Recommended!'));

        }catch(Exception $e)
        {
            $emailError = substr($this->view->printError("email"), 32, -16);
            return $this->_helper->json(array('success' => false, 'msg' => $e->getMessage(), 'emailError' => $emailError));
        }
    }//end send

    public function addwishlistAction()
    {
        $db = Zend_Registry::get("db");
        $config = Zend_Registry::get("config");
        $server = Zend_Registry::get("server");
        $logger = Zend_Registry::get("logger");

        $param = $this->getRequest()->getParams();

        $data['item_id'] = $param['item_id'];
        $data['user_id'] = $param['user_id'];
        $data['added_date'] = date('Y-m-d H:i:s');
        $data['sharewith_group'] = '';
        $data['share_individual'] = '';
        $data['items_required_no'] = 1;
        $data['reserved_by'] = 0;
        $data['gift_state'] = 0;

        $datai = array(
            'item_count'      => new Zend_DB_Expr('item_count + 1')
        );
        $updated_date= array(
                        'updated_date'      => date('Y-m-d')
                );

        $success = false;
        try{
            $db->beginTransaction();

            $chkWished = wishlistitmes::chkWishedItem($param['item_id'],$param['user_id']);
            if(!$chkWished)
            {
                $db->insert('userwishlist',$data);
                $result = $db->lastInsertId();
                $db->update('wishlistitmes',$datai,'id='.$param['item_id']);
                if($result)
                {
                    //$where = array("item_id = ?" => $param['item_id'],"user_id = ?" => $param['user_id']);
                    //$db->update('userwishlist', $updated_date , $where);

                    // Copy tags
                    $taglist = category::getItemTags($param['item_id'], null); //$param['user_id']);
                    for ($j = 0; $j < count($taglist); $j++) {
                         $isExist =  category::checkItemTags($param['item_id'],$taglist[$j]->id, $param['user_id']);
                         if ($isExist) {
                             continue;
                         }
               
                         $dataT['name'] = $taglist[$j]->name; //$params['cat_sub'];
                         $dataT['created_by'] = $param['user_id'];
                         $dataT['status'] = 1;
                         $dataT['parent'] = 0;
                         $db->insert('category', $dataT);
                         $tagId =  $db->lastInsertId();

                         $dataIT['item_id'] = $param['item_id'];
                         $dataIT['tag_id'] = $tagId;
                         $dataIT['user_id'] = $param['user_id'];
                         $dataIT['tag_date'] = date('Y-m-d H:i:s');
                         $db->insert('item_tags',$dataIT);
                    }
                    $prod_detail = wishlistitmes::getProductDetail($param['item_id']);
                    if ($prod_detail == null) { $logger->info("prod_detail null");}
                    $user_details = users::getUserDetails($prod_detail[0]->user_id);
                    $num_item_add = $user_details->num_item_add + 1;
                    $influence_score = ($num_item_add * 10) + ($user_details->num_item_buy * 20) + $user_details->profile_views;
                    $datap = array(
                             'num_item_add'      => new Zend_DB_Expr('num_item_add + 1'),
                             'influence_score'  => $influence_score
                            );
                    $db->update('users',$datap,'user_id='.$prod_detail[0]->user_id);

                    $success = true;
                    $msg = 'Item is added in your wishlist';
                }else{ 
                    $success = false;
                    $msg = 'Error! in adding';
                }
                $db->commit();
            }
        }catch(Exception $e){
            //Rollback transaction
            $db->rollBack();
            $msg = $e->getMessage();
        }
        $countwishes = wishlistitmes::countWished($param['item_id']);
        return $this->_helper->json(array('success' => $success, 'msg' => $msg, 'countwishes' => $countwishes));
    }
    public function deletewishlistAction()
    {
        $db = Zend_Registry::get("db");
        $config = Zend_Registry::get("config");
        $server = Zend_Registry::get("server");

        $param = $this->getRequest()->getParams();


        $datai = array(
            'item_count'      => new Zend_DB_Expr('item_count - 1')
        );

        try{
            $db->beginTransaction();

            $res = $db->delete('userwishlist', array("user_id = ?" => $param['user_id'], "item_id = ?" => $param['item_id']));

            $db->update('wishlistitmes',$datai,'id='.$param['item_id']);


            // delete items from item_tags table, if item exits
            $res = $db->delete('item_tags', array("user_id = ?" => $param['user_id'], "item_id = ?" =>$param['item_id']));
            // delete items from taggifts table, if item exits
            $sql=$db->query("SELECT event_id,item_id FROM taggifts WHERE FIND_IN_SET('".$param['item_id']."',item_id) AND user_id='".$param['user_id']."'");
            $tag_item_id=$sql->fetchAll();
            if(count($tag_item_id)>0){
                foreach($tag_item_id as $remove_item_id){
                    $item_id =explode(",",$remove_item_id->item_id);
                    foreach($item_id as $i_id){
                        if($i_id !=$param['item_id']){
                            $update_item_id[]=$i_id;
                        }
                    }
                    if(count($update_item_id)>0){
                        $update_taggifts=array("item_id"=>implode(",",$update_item_id));
                        $db->update('taggifts',$update_taggifts,'event_id='.$remove_item_id->event_id);
                    }
                    else{
                        $where = array("event_id = ?" => $remove_item_id->event_id);
                        $db->delete("taggifts",$where);
                    }
                    unset($update_item_id);
                }
            }
            $countwishes = wishlistitmes::countWished($param['item_id']);

            if (!$countwishes) {
                 /* Don't delete an item from the databse */              
                 // $res = $db->delete('wishlistitmes', array("user_id = ?" => $param['user_id'], "id = ?" => $param['item_id']));
            }
            $msg = 'Item is deleted from your wishlist';
            $success = true;

            $db->commit();
        }catch(Exception $e){
            //Rollback transaction
            $db->rollBack();
            $success = false;
            $countwishes = false;
            $msg = $e->getMessage();
        }

        return $this->_helper->json(array('success'=>$success, 'msg'=>$msg, 'countwishes' => $countwishes));
    }
    
    /// Search for gift
    /// Get search as key and find item name like this key

    public function searchAction() {
        $db = Zend_Registry::get("db");
        $config = Zend_Registry::get("config");
        $server = Zend_Registry::get("server");
        $logger = Zend_Registry::get("logger");

        //user
        $user_details = users::getUserDetails($_SESSION['user_id']);
        $this->view->userDetail = $user_details;

        ///categories
        $categories = category::categoryList();
        $this->view->categories = $categories;

        $this->view->vpage = 5;
        $param = $this->getRequest()->getParams();

        $searchKey = trim($param['search']);
        $catVal = $param['item_type'];
        
        $this->view->search_key = $searchKey;
        $logger->info("Search key = ". $searchKey);

        if ($param['pricekey'] == 'price') {
            if ($orderBy != '')
                $orderBy .= ', item_price ASC';
            else
                $orderBy = ' item_price ASC';
        }

        if ($param['datekey'] == 'date') {
            if ($orderBy != '')
                $orderBy .= ', uw.added_date ASC';
            else
                $orderBy = 'uw.added_date ASC';
        }

        try {
            if ($searchKey != '') {

                $keych = '%' . $searchKey . '%';
                //$db_searchkey = $db->quote($keych);
                $db_searchkey = $keych;
                
		$logger->info("DB search Key: ". $keych);
                $products = wishlistitmes::searchResult($_SESSION['user_id'], $db_searchkey, $orderBy, $catVal);
                $logger->info("Products based on search key: ". count($products));

                if ($catVal != '') {
                    $catName = category::categoryName($catVal);
                } else {
                    $catName = 'Category';
                }

                /// search people/users
                $nameArr = explode(" ", $searchKey);

                if (count($nameArr) > 1) {
                    $fname = '%' . $nameArr[0] . '%';
                    $lname = '%' . $nameArr[1] . '%';

                    $fname1 = $db->quote($fname);
                    $lname1 = $db->quote($lname);
                } else {
                    $fname = '%' . $searchKey . '%';
                    $fname1 = $db->quote($fname);
                }
                
                $logger->info("User ID = ". $_SESSION['user_id']);

                if ($_SESSION['user_id'] != '') {

                    /* Search for all users in the database
                     * and not just user's friends
                     */
                   
                    $usernames = explode(" ", $db_searchkey); 
                    if ($usernames[0] != '' &&$usernames[1] != '') {
                        $logger->info("db search for first name" . $usernames[0]. "last name ".$usernames[1]);
                        $matching_users = users::searchUsersByFullName($usernames[0], $usernames[1]);
                    }
                    if (count($matching_users) <= 0) {
                        /* treating searchKey as a username */
                        $logger->info("db search for " . $db_searchkey);
                        $matching_users = users::searchUsersByName($db_searchkey); 
                        $logger->info("Found ". count($matching_users) . " matching users among all users of Giftvise");
                    }
                    $matching_users_friends_map = usersfriend::searchFriendsByNameMap($_SESSION['user_id'], $db_searchkey);
                    $logger->info("Found ". count($matching_users_friends_map) . " matching friends among all friends of the user");
		    //$logger->info(print_r($matching_users_friends_map, true));
                    
                    foreach ($matching_users as $user) {

                        if ($user->user_id == $_SESSION['user_id']) continue;

                        $list[] = array();
                        $list['id'] = $user->user_id;
                        $list['name'] = $user->first_name . ' ' . $user->last_name;

                        if($user->profile_pic != '')
                        {

                                     $list['profile_pic'] = "https://".$server->apphost."/userpics/thumb/".$user->profile_pic;
                        }else{
                                     $list['profile_pic'] = "https://".$server->apphost."/userpics/thumb/default-profile-pic.png";
                        }

                        $list['created_date'] = $user->created_date;
                        
                        if ($matching_users_friends_map[$user->user_id] != '') {
                            $user_friend = $matching_users_friends_map[$user->user_id];
                            $list['type'] = $user_friend['type'];
                            $list['request_status'] = $user_friend['request_status'];
                            $list['status'] = $user_friend['status'];
                            $list['is_yourfollower'] = $user_friend['is_your_follower'];
                        } else {
                            $list['type'] = 'follower';
                            $list['status'] = 4; // 0-active, 1-inactive, 4-unknown
                            $list['request_status'] = '1'; // 0-active, 1-inactive
                            $list['is_yourfollower'] = 'N';
                        }
                        $userresult[] = $list;

			//$logger->info(print_r($list, true));
                    }
                    
                    $this->view->allfriends = $userresult;
                    $this->view->users_count = count($userresult);
 
                    /*
                    $userResult = usersfriend::searhUser($fname1, $_SESSION['user_id'], $lname1);
                    //$userResult = usersfriend::searhUser($key1, $_SESSION['user_id'], '');

                    if (!empty($userResult)) {
                        for ($i = 0; $i < count($userResult); $i++) {
                            $list = array();
                            $chk = usersfriend::checkExistFriendslist($_SESSION['user_id'], $userResult[$i]->user_id);

                            $list['user_id'] = $userResult[$i]->user_id;
                            $list['username'] = $userResult[$i]->username;
                            $list['profile_pic'] = $userResult[$i]->profile_pic;
                            if ($chk) {
                                $list['isfriend'] = 'Y';
                            } else {
                                $list['isfriend'] = 'N';
                            }
                            $userresult[] = $list;
                        }
                        
                        //$this->view->userResult = $userresult;
                        $this->view->allfriends = $userresult;
                        $this->view->users_count = count($userresult);
                    }
                    */
                }
                ///
                /// Search for tags
                // End Tag search
                //$tags = category::usersTagsName($_SESSION['user_id'], $key1);
                //$this->view->tags = $tags;

                $totalProduct = count($products);
                $pcnt = 0;
                $skip_ct = 0;
                for ($i = 0; $i < count($products); $i++) {
                    $item = array();
                    if ($products[$i]->image == 'no-image-available') {
                        $skip_ct++;
                        continue;
                    }
                    $totalComments = itemcomments::countComments($products[$i]->id);
                    $userwished = wishlistitmes::countWished($products[$i]->id);

                    /// latest item
                    //$timeAdded = wishlistitmes::latestItem($products[$i]->id);
                    $timeAdded = wishlistitmes::latestItemHome($products[$i]->id);
                    $addeduser = users::getUserDetails($timeAdded['user_id']);

                    //
                    $itemwhished = wishlistitmes::chkWishedItem($products[$i]->id, $_SESSION['user_id']);
                    if ($itemwhished) {
                        $item['youwhished'] = 'Y';
                    } else {
                        $item['youwhished'] = 'N';
                    }
                    //
                    $item['comment'] = $totalComments;
                    $item['wished'] = $userwished;
                    $item['added_time'] = $timeAdded['time'];
                    //$item['addedby'] = $addeduser->first_name.' '.$addeduser->last_name;
                    $item['first_name'] = $addeduser->first_name;
                    $item['last_name'] = $addeduser->last_name;
                    $item['addedby'] = $addeduser->first_name . ' ' . substr($addeduser->last_name, 0, 1);
                    $item['first_name'] = $addeduser->first_name;
                    $item['user_id'] = $addeduser->user_id;
                    $items[] = $item;

                    if ($addeduser->first_name != '') {
                        $pcnt++;
                    }
                }
                //print_r($items);
                //exit;
                $this->view->items = $products;
                $this->view->items_count = count($products) - $skip_ct;
                //$this->view->totalProduct = $totalProduct;
                $this->view->totalProduct = $pcnt;
                $this->view->extval = wishlistitmes::prepareExtVal($products, $_SESSION['user_id']);
                $this->view->categoryName = $catName;
            } else {
                $this->view->error = 'Please enter value to search';
            }
        } catch (Exception $e) {
            $this->view->errors[] = $e->getMessage();
        }
    }


    public function addproductimageAction(){
        $db = Zend_Registry::get("db");

        $params = $this->getRequest()->getParams();
        $productId = $params['product_id'];
        $db->beginTransaction();
        try{
            //move image from temp to group
            $upload = new fileupload();
            if ($upload->moveImageFromTemp('productimg', $params['image'])){
               $data['image'] = $params['image'];
                $result = $db->update('wishlistitmes', $data, 'id = '. $productId);
                $db->commit();
                return $this->_helper->json(array('success' => true, 'result' => $result, 'msg' => 'Image added successfuly!'));
            } else{
                return $this->_helper->json(array('success' => false, 'msg' => 'There are some problems. Please try later!'));
            }
        }catch(Exception $e){
           //Rollback transaction
            $db->rollBack();
            return $this->_helper->json(array('success' => false, 'msg' => $e->getMessage()));
        }
    }

    protected function _initAddProduct()
    {
        $productname = new PS_Validate_NotEmpty($this->_getParam("name"));
        $productname->setMessage(PS_Translate::_("Please enter product's name"));

        $price = new PS_Validate_NotEmpty($this->_getParam("price"));
        $price->setMessage(PS_Translate::_("Please enter price"));

        $category = new PS_Validate_NotEmpty($this->_getParam("category"));
        $category->setMessage(PS_Translate::_("Please select category"));

        $description = new PS_Validate_NotEmpty($this->_getParam("description"));
        $description->setMessage(PS_Translate::_("Please enter description"));

        $this->fields = new PS_Form_Fields(
            array(
                "name" => array(
                    $productname,
                    "required" => true
                ),

                "price" => array(
                    array($price, new PS_Validate_Currency()),
                    "required" => true
                )
                /*"category" => array(
                    $category,
                    "required" => true
                ),
                "description" => array(
                    $description,
                    "required" => true
                )*/
            )
        );
        $params = $this->getRequest()->getParams();
        $this->fields->setData($params);
        $this->view->fields 		= $this->fields;

    }///end function

	public function addproductAction()
	{
        $vendor_link_for_add_manual_items = 'http://www.amazon.com/s?url=search-alias&field-keywords=';
        $db = Zend_Registry::get("db");
        $config = Zend_Registry::get("config");
        $server = Zend_Registry::get("server");
        $logger = Zend_Registry::get("logger");

        $params = $this->getRequest()->getParams();
//        return $this->_helper->json(array($params));
        $this->_initAddProduct();

//        return $this->_helper->json(array('success' => $params));
        $categories = category::categoryList();
        $this->view->categories = $categories;

        $data['user_id'] = $_SESSION['user_id'];
        $data['item_name'] = addslashes($params['name']);
        $data['description'] = addslashes($params['description']);
        $data['item_price'] = $params['price'];
        $data['item_count'] = 1;
        $data['state'] = 0;
        //$data['item_type'] = $params['category'];
        if ($params['added_from'] == '') {
            $data['added_from'] = 'manual';
        } else {
            $data['added_from'] = $params['added_from'];
        }
        $data['added_date'] = date('Y-m-d H:i:s');
        if ($params['currency'] !=  '') {
            $data['item_price_currency'] = $params['currency'];
        }
        try{
            $db->beginTransaction();
            //$this->fields->validate();

            $pname = $db->quote($params['name']);
            $chkitem = 0; //wishlistitmes::chkExistItem($pname);

            if ($chkitem) {
                $msg = "This Product is already added.";
            } else {
                //move image
                $upload = new fileupload();
                $generatedFileName = $_SESSION['user_id'] . "_" . $params['price'] . "_" . "manual";
                $fileOptions = array('url' => $params['imageurl'], 'generatedName' => $generatedFileName);
                $fileData = $upload->handleFiles($params['image'], 'productimg', $fileOptions);
                $data['image'] = $fileData['image'];
                $msg = $fileData['message'];
                
                if($msg == '') {
                    $data['item_type'] = '1';
                    if ($params['producturl'] == '') {
                        $data['vendor_link'] = $vendor_link_for_add_manual_items . $params['name'];
                    } else {
                        $data['vendor_link'] = $params['producturl'];
                    }
                    $data['vendor_link'] = $data['vendor_link'].'&tag='.$params['url_tag'];
                    $data['isbn'] = '';
                    $data['barcode_itemid'] = '';
                    $db->insert('wishlistitmes', $data);
                    $item_id = $db->lastInsertId();

                    if ($item_id != '') {
                        $dataW['user_id'] = $_SESSION['user_id'];
                        $dataW['item_id'] = $item_id;
                        $dataW['added_date'] = $data['added_date'];
                        $dataW['sharewith_group'] = '';
                        $dataW['share_individual'] = '';
                        $dataW['reserved_by'] = 0;
                        $dataW['reserved_on'] = date('Y-m-d H:i:s');
                        $dataW['gift_state'] = 0;
                        $dataW['public'] = ($params['privacy'] == '') ? 1 : 0;
                        $db->insert('userwishlist', $dataW);

                        $msg = "Product has been added successfully";
                        $success = true;

                        //add tags
                        $user_id = $_SESSION['user_id'];

                        if (trim($params['txt_tags']) != '') {
                            $tagsArr = explode(",", trim($params['txt_tags']));
                            if (count($tagsArr) > 5 ) {
                                $tlimit = 5;
                            } else {
                                $tlimit = count($tagsArr);
                            }
                            for($t = 0; $t < $tlimit; $t++) {
                                if(trim($tagsArr[$t]) != '') {
                                    if (strlen($tagsArr[$t]) > 25) {
                                        $tagNames = substr($tagsArr[$t],0,25);
                                    } else {
                                        $tagNames = $tagsArr[$t];
                                    }

                                    $dataT = array();
                                    $dataT['name'] = $tagNames;
                                    $dataT['created_by'] = $user_id;
                                    $dataT['status'] = 1;

                                    $checkTagID = category::checkTags($tagsArr[$t], $user_id);
                                    if ($checkTagID != '') {
                                        // already added
                                        $tag_id = $checkTagID;
                                    } else {
                                        // add
                                        $db->insert('category',$dataT);
                                        $tag_id =  $db->lastInsertId();
                                    }
                                    $dataIT['item_id'] = $item_id;
                                    $dataIT['tag_id'] = $tag_id; // tag_id is the category id
                                    $dataIT['user_id'] = $user_id;
                                    $dataIT['tag_date'] = $data['added_date'];

                                    $checkID = category::checkItemTags($item_id, $tag_id, $user_id);

                                    if ($checkID != '') {
                                        // do nothing
                                    } else {
                                        // tag item
                                        $db->insert('item_tags', $dataIT);
                                    }
                                } //if
                            }// for
                        //
                        }
                    } else {
                        $msg = "Error! in adding product, please try again.";
                    }
                    $db->commit();
                    $logger->info("--- ACCESS-INFO --- ::MANUAL-ADD::USER-ID::"    
                                  .$_SESSION['user_id']. "::DEVICE-INFO:: ".$_SERVER['HTTP_USER_AGENT'] . 
                                  "IP ADDRESS ". $_SERVER['REMOTE_ADDR'] ."\n");        
                }
            } // else
        } catch(Exception $e) {
            //Rollback transaction
            $db->rollBack();
            //print_r($e->getMessage());
            //exit;
            $msg = $e->getMessage();
        }
        return $this->_helper->json(array('success' => ($success) ? true : false, 'msg' => $msg));
    }//add

    public function itemcuratedinfoAction() {
        $logger = Zend_Registry::get("logger");

        $params = $this->getRequest()->getParams();
        $logger->info("Product: " . $params['product_id'] . " info: ". $params['description'] . " by user: " . $_SESSION['user_id']);
        
        $success = false;
        $msg = '';
        try {
            $success = wishlistitmes::itemcuratedinfo($params['product_id'], $params['description']);
            $msg = 'Updated Successfuly';
        } catch( Exception $e) {
            $msg = $e->getMessage(); 
        }

        return $this->_helper->json(array('success' => $success, 'msg' => $msg, 'desc' => $params['description']));
    }

    public function curatorcuratedinfoAction() {
        $logger = Zend_Registry::get("logger");

        $params = $this->getRequest()->getParams();
        $logger->info("user: " . $params['user_id'] . " info: ". $params['description'] . " by user: " . $_SESSION['user_id']);

        $success = false;
        $msg = '';
        try {
            $success = users::curatorcuratedinfo($params['user_id'], $params['description']);
            $msg = 'Updated Successfuly';
        } catch( Exception $e) {
            $msg = $e->getMessage();
        }

        return $this->_helper->json(array('success' => $success, 'msg' => $msg, 'desc' => $params['description']));
    }

     // save changes made on show to dropdown
    public function changeshowtoAction() {
        $server = Zend_Registry::get("server");
        $config = Zend_Registry::get("config");
        $logger = Zend_Registry::get("logger");
        $db = Zend_Registry::get("db");

        //disable layouts for this action
        $this->_helper->layout()->disableLayout();

        //get params
        $param = $this->getRequest()->getParams();
        $itemId = $param["item_id"];
        $public = $param["public"];
        $groupIds = $param["group_ids"];
        $individualId = $param["individual_id"];

        $db->beginTransaction();
        try {

            $chkitem = wishlistitmes::chkusertem($_SESSION["user_id"], $itemId);
            if (!empty($groupIds)){
                $data = array('public' => '2', 'sharewith_group' => $groupIds, 'share_individual' => '');
            } else if(!empty($individualId)){
                $data = array('public' => '3', 'sharewith_group' => '', 'share_individual' => $individualId);
            } else {
                $data = array('public' => $public, 'sharewith_group' => '', 'share_individual' => '');
            }
            //if there is an item update it else create a new one
            if($chkitem){
                //update
                $where['user_id = ?'] = $_SESSION["user_id"];
                $where['item_id = ?'] = $itemId;
                $db->update('userwishlist', $data, $where);
            } else {
                //create
                $data['user_id'] = $_SESSION["user_id"];
                $data['item_id'] = $itemId;
                $data['added_date'] = date('Y-m-d H:i:s');
                $data['share_individual'] = '';
                $db->insert('userwishlist', $data);
            }
            $db->commit();
            return $this->_helper->json(array('success' => true));
        } catch(Exception $e){
            //Rollback transaction
            $db->rollBack();
            return $this->_helper->json(array('success' => false, 'msg' => $e->getMessage()));
        }
    }

}

