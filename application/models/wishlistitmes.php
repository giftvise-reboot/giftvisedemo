<?php
/*------------------------------------------------------------------ 
 * Copyright (c) 2013-2014 by Giftvise, Inc.
 * All rights reserved. 
 *------------------------------------------------------------------
 */
?>

<?php

class wishlistitmes extends Zend_Db_Table_Abstract
{
	public static function chkExistItem($name)
	{
		$sql = "SELECT id 
				FROM wishlistitmes 
				WHERE item_name = ".$name."";
		//echo '<br>test1:::'.$sql;
		//exit;
		$db = Zend_Registry::get("db");	
		$result = $db->fetchOne($sql);
		return $result;
	}

         public static function chkExistItemNameUserID($name, $userid)
        {
                $sql = "SELECT id 
                                FROM wishlistitmes 
                                WHERE item_name = ".$name." AND user_id = ".$userid."";
                $db = Zend_Registry::get("db"); 
                $result = $db->fetchOne($sql); 
                return $result;
        } 
	
	public static function chkExistItemWithBarcode($name,$barcode)
	{
		$sql = "SELECT id,item_count
				FROM wishlistitmes 
				WHERE item_name = '".$name."' AND barcode_itemid = '".$barcode."'";	
						
		$db = Zend_Registry::get("db");	
		$result = $db->fetchRow($sql);
		return $result;
	}
	
	public static function searchbyitem($item){
		$db = Zend_Registry::get("db");	
		$sql = "SELECT * FROM wishlistitmes WHERE  item_name LIKE '%".$item."%' ORDER BY id";
		$result = $db->fetchAll($sql);
		return $result;
	}
	
	public static function chkusertem($user_id,$item_id)
	{
		$sql = "SELECT id 
				FROM userwishlist 
				WHERE user_id = '".$user_id."' AND item_id = '".$item_id."'";
		//echo 'test1:::'.$sql;
						
		$db = Zend_Registry::get("db");	
		$result = $db->fetchOne($sql);
		return $result;
	}

        /* Function add description to the product when added through bookmarklet */
        public static function bookmarkletUpdateDesc($userID, $itemID, $description)
        {
                $db = Zend_Registry::get("db");
                $logger = Zend_Registry::get("logger");

                $db->beginTransaction();
                $data=array('description'=>$description);
                $where = array("id = ?" => $itemID,"user_id = ?" => $userID);
                $db->update('wishlistitmes',$data,$where);
                $db->commit();
        }
	
	public static function getAllProducts($userID, $orderBy, $category)
	{
		$db = Zend_Registry::get("db");	
		
		if($orderBy != '')
		{
			$orderby = ' ORDER BY '.$orderBy;
		}else{
			$orderby = ' ORDER BY item_name ASC';
		}
	
		if($category != '')
		{
			$categorySql = ' AND w.item_type ='.$category;
			$sqlTag = "SELECT item_id FROM taggifts WHERE tag_id='".$category."' AND user_id='".$userID."' AND item_id != ''
				UNION All 
				SELECT item_id FROM item_tags WHERE tag_id='".$category."' AND user_id='".$userID."' AND item_id != '' ";
			$item_idArray = $db->fetchAll($sqlTag);
			$itemList = array();
			for($i = 0; $i < count($item_idArray); $i++)
			{
				$itemList[] = $item_idArray[$i]->item_id;
			}
			$itemStr = implode(",",$itemList);
			if($itemStr == '')
			{
				$itemStr = 0;
			}
			
			$sql = "SELECT w.item_name, w.id AS item_id, w.item_type, w.image, w.description, w.item_price, w.item_count, w.vendor_link, w.isbn, w.added_from, uw.added_date, uw.id AS wid,uw.added_date AS uadded_date,barcode_number, uw.public, uw.sharewith_group, uw.share_individual 
		FROM userwishlist uw JOIN wishlistitmes w ON w.id = uw.item_id WHERE uw.user_id = '".$userID."' AND  item_id IN (".$itemStr.")".$orderby;
		
		}else{
			$sql = "SELECT w.item_name, w.id AS item_id, w.item_type, w.image, w.description, w.item_price, w.item_count, w.vendor_link, w.isbn, w.added_from, uw.added_date, uw.id AS wid,uw.added_date AS uadded_date,barcode_number, uw.public, uw.sharewith_group, uw.share_individual 
		FROM userwishlist uw JOIN wishlistitmes w ON w.id = uw.item_id WHERE uw.user_id = '".$userID."' ".$categorySql." ".$orderby;
		}
		
					
		//echo $sql;
		//exit;
		
		$result = $db->fetchAll($sql);
		return $result;
	}
	
	public static function getProductDetail($productID)
	{
		$sql = "SELECT * 
				FROM wishlistitmes 
				WHERE id = '".$productID."'";
		
		$db = Zend_Registry::get("db");	
		$result = $db->fetchAll($sql);
		return $result;
	}

        public static function getProductDetailUserId($productID, $user_id)
        {
                if ($user_id) {
                     $sql = "SELECT DISTINCT(w.id),w.*, uw.gift_state AS gift_state, uw.reserved_by AS reserved_by 
                            FROM wishlistitmes w
                            LEFT JOIN userwishlist uw ON w.id = uw.item_id
                            WHERE w.id='".$productID."' AND uw.user_id = '".$user_id."'";
                } else {
                    $sql = "SELECT * 
                                FROM wishlistitmes 
                                WHERE id = '".$productID."'";
                }
                $db = Zend_Registry::get("db");
                $result = $db->fetchAll($sql);
                return $result;
        }
	
	public static function getProductDetailForTaggedItems($productID)
	{
		$sql = "SELECT w.item_name, w.id AS item_id, w.item_type, w.image, w.description, w.item_price, w.item_count, w.vendor_link, w.isbn, w.added_from, barcode_number FROM wishlistitmes w WHERE ID IN (".$productID.")";
		
		$db = Zend_Registry::get("db");	
		$result = $db->fetchAll($sql);
		return $result;
	}
	
	public static function countWished($productID)
	{
		$sql = "SELECT COUNT(user_id) AS total
				FROM userwishlist 
				WHERE item_id = '".$productID."'";
		
		$db = Zend_Registry::get("db");	
		$result = $db->fetchOne($sql);
		return $result;
	}
	
	public static function getAllFeed($userID,$col, $ord)
	{	
		if($col != '')
		{
			if($col == 'item_type')
			{
				$orderby = ' WHERE '.$col.'='.$ord;
			}else{
				$orderby = ' ORDER BY '.$col.' '.$ord;
			}
		}else{
			$orderby = ' ORDER BY item_count DESC';
		}
		$db = Zend_Registry::get("db");	
		///user's item
		$sqlItemU = "SELECT item_id
                             FROM userwishlist 
                             WHERE user_id = '".$userID."'". "AND state=1";
		$resitemids = $db->fetchAll($sqlItemU);
		
		for($i=0; $i < count($resitemids); $i++)
		{
			$ids[] = $resitemids[$i]->item_id;
		}
		if(count($ids) > 0)
		{			
			$idsStr = implode(",",$ids);
		}else{			
			$idsStr = '0';
		}
		
		///
		/*$sql = "SELECT DISTINCT(w.id),w.*, w.id AS item_id
				FROM wishlistitmes w
				LEFT JOIN userwishlist uw ON w.id = uw.item_id
				WHERE uw.user_id != '".$userID."'
				  ".$orderby;*/
		$sql = "SELECT DISTINCT(w.id),w.*, w.id AS item_id
                        FROM wishlistitmes w
                        LEFT JOIN userwishlist uw ON w.id = uw.item_id
                        WHERE uw.item_id NOT IN (".$idsStr.")
				  ".$orderby;
			
		//echo $sql.'<br>';
		//exit;
		
		$result = $db->fetchAll($sql);
		return $result;
	}
	
	public static function chkWishedItem($item_id,$user_id)
	{
		$sql = "SELECT * 
                        FROM userwishlist 
                        WHERE user_id = '".$user_id."' AND item_id = '".$item_id."'";
		//echo $sql;
		$db = Zend_Registry::get("db");	
		$result = $db->fetchOne($sql);
		return $result;
	}
	
	public static function latestItem($itemID)
	{
		//$sql = "SELECT UNIX_TIMESTAMP(added_date) AS wishdate, user_id FROM userwishlist WHERE item_id = '".$itemID."' ORDER BY id DESC LIMIT 0,1";
		
		$sql = "SELECT added_date AS wishdate, reserved_on AS rs_on, user_id FROM userwishlist WHERE item_id = '".$itemID."' ORDER BY id DESC LIMIT 0,1";
		
		$db = Zend_Registry::get("db");	
		$result = $db->fetchRow($sql);
		$today = time();
		
		$idate = $result->wishdate;
		
		$diff_seconds  = $today - $idate;
		
		
		$postedc =  time() - strtotime($result->wishdate);
		///
//		if($postedc < 3600) {
//         if(round($postedc/60) > 1)
//          $postedcdisplay = round($postedc/60)." ".PS_Translate::_("minutes ago");
//         else
//          $postedcdisplay = "1 ".PS_Translate::_("minute ago");
//        }
//		else if($postedc < 86000) {
//         if(round($postedc/3600) > 1)
//          $postedcdisplay = round($postedc/3600)." ".PS_Translate::_("hours")." ago ";
//         else
//          $postedcdisplay = "1 ".PS_Translate::_("hour")." ago ";
//        }
//		 else if($postedc < 430000) {
//         if(round($postedc/86000) > 1)
//          $postedcdisplay = round($postedc/86000)." ".PS_Translate::_("days")." ago ";
//         else
//          $postedcdisplay = "1 ".PS_Translate::_("day")." ago ";
//        }
//        else {
         $postedcdisplay = date("r", strtotime($result->wishdate));
         $rpostedcdisplay = date("r", strtotime($result->rs_on));
//        }
		
		
		$resultArr['time'] = $postedcdisplay;
                $resultArr['reserved_on'] = $rpostedcdisplay;
		$resultArr['user_id'] = $result->user_id;
		
		return $resultArr;
		
		
	}
	
	public static function latestItemUserApp1($itemID)
	{
		$sql = "SELECT w.added_date, w.user_id, CONCAT(u.first_name,' ',u.last_name) AS fullName, u.profile_pic FROM userwishlist AS w 
		JOIN users AS u ON w.user_id = u.user_id WHERE w.item_id = '".$itemID."' ORDER BY id DESC LIMIT 0,1"; 
		
		$db = Zend_Registry::get("db");	
		return $result = $db->fetchRow($sql);
	}
	///
	
	public static function latestItemUser($itemID, $user_id)
	{
		
		$db = Zend_Registry::get("db");	
		$sqlf = "SELECT friend_id 
				FROM usersfriend 
				WHERE (user_id = '".$user_id."' AND status = '1')";		
		
		$follow = $db->fetchAll($sqlf);
		$friendlist = array();
		
		for($i=0; $i < count($follow); $i++)
		{
			
			$friendlist[] = $follow[$i]->friend_id;
		}
		if(count($friendlist) > 0)
		{		
			$friendlistStr = implode(",",$friendlist);
		}else{
			$friendlistStr = '0';
		}
			
		 $sql = "SELECT w.added_date, w.user_id, u.first_name AS fullName, u.profile_pic, u.last_name FROM userwishlist AS w 
		LEFT JOIN users AS u ON w.user_id = u.user_id WHERE w.item_id = '".$itemID."' AND w.user_id IN (".$friendlistStr.")
		 ORDER BY id DESC LIMIT 0,1"; 	 
		
		//echo $sql;
		//exit;
		
		$result = $db->fetchRow($sql);
		
		$postedc =  time() - strtotime($result->added_date);
        
		///
		if($postedc < 3600) {
         if(round($postedc/60) > 1)
          $postedcdisplay = round($postedc/60)." ".PS_Translate::_("minutes ago");
         else
          $postedcdisplay = "1 ".PS_Translate::_("minute ago");
        }
		else if($postedc < 86000) {
         if(round($postedc/3600) > 1)
          $postedcdisplay = round($postedc/3600)." ".PS_Translate::_("hours")." ago ";
         else
          $postedcdisplay = "1 ".PS_Translate::_("hour")." ago ";
        }
		 else if($postedc < 430000) {
         if(round($postedc/86000) > 1)
          $postedcdisplay = round($postedc/86000)." ".PS_Translate::_("days")." ago ";
         else 
          $postedcdisplay = "1 ".PS_Translate::_("day")." ago ";
        }
        else {
         $postedcdisplay = " on ".date("m/d/y", strtotime($result->added_date));
        }
		
		
		$resultArr['time'] = $postedcdisplay;
		$resultArr['user_id'] = $result->user_id;
		return $resultArr;
	}
	
	public static function latestItemUserApp($itemID, $friendList, $user_id,$loggediUser )
	{
		
		/*$sql = "SELECT w.added_date, w.user_id, CONCAT(u.first_name, ' ',u.last_name) AS fullName, u.profile_pic FROM userwishlist AS w 
		LEFT JOIN users AS u ON w.user_id = u.user_id WHERE w.item_id = '".$itemID."' AND (w.user_id != '".$user_id."' AND w.user_id != '".$loggediUser."')		 ORDER BY id DESC LIMIT 0,1";*/
		
		$sql = "SELECT w.added_date, w.user_id, CONCAT(u.first_name, ' ',u.last_name) AS fullName, u.profile_pic FROM userwishlist AS w 
		LEFT JOIN users AS u ON w.user_id = u.user_id WHERE w.item_id = '".$itemID."' AND (w.user_id != '".$user_id."' AND w.user_id != '".$loggediUser."') AND w.user_id IN (".$friendList.") ORDER BY id DESC LIMIT 0,1";		
		
		$db = Zend_Registry::get("db");	
		$result = $db->fetchRow($sql);
		return $result;
	}
	
	public static function getFriendsWishlist($user_id)
	{
		
		$sql = "SELECT friend_id 
				FROM usersfriend 
				WHERE (user_id = '".$user_id."' AND status = '1')";	
		$sql1 = "SELECT user_id 
				FROM usersfriend 
				WHERE (friend_id = '".$user_id."' AND status = '1')";	
		
		$db = Zend_Registry::get("db");	
		$follow = $db->fetchAll($sql);
		$follower = $db->fetchAll($sql1);
		
		$friendlist = array();
		
		for($i=0; $i < count($follow); $i++)
		{			
			$friendlist[] = $follow[$i]->friend_id;
		}
		
		/* for($i=0; $i < count($follower); $i++)
		{
			
			$friendlist[] = $follower[$i]->user_id;
		}*/	
		
		$friendlistStr = implode(",",$friendlist);
		if(count($friendlist) > 0)
		{
			$sqlitem = "SELECT uw.added_date AS wishdate, w.id AS item_id, uw.user_id AS friendid, w.* FROM userwishlist uw  
						JOIN wishlistitmes w ON w.id = uw.item_id
			WHERE uw.user_id IN (".$friendlistStr.")";
			//echo $sqlitem.'<br>';
			$result = $db->fetchAll($sqlitem);
		}
		return $result;
	}
	
	public static function getUserFriendsWishlist($user_id,$sortby,$start,$end)
	{		
		$sql = "SELECT  friend_id 
				FROM usersfriend 
				WHERE (user_id = '".$user_id."' AND status = '1')";	
		$db = Zend_Registry::get("db");	
		$follow = $db->fetchAll($sql);	
		$friendlist = array();		
		for($i=0; $i < count($follow); $i++)
		{			
			$friendlist[] = $follow[$i]->friend_id;
		}

		$friendlistStr = implode(",",$friendlist);
		if(count($friendlist) > 0)
		{
				$sqlitem = "SELECT distinct w.id AS item_id, w.* FROM userwishlist uw  
			JOIN wishlistitmes w ON w.id = uw.item_id
			WHERE uw.user_id IN (".$friendlistStr.") ORDER BY  ".$sortby." LIMIT ".$start.",".$end." ";
			//echo $sqlitem.'<br>';
			$result = $db->fetchAll($sqlitem);

		}
		return $result;
}
	public static function getallUserFriendsWishlist($user_id,$sortby)
	{		
		$sql = "SELECT  friend_id 
				FROM usersfriend 
				WHERE (user_id = '".$user_id."' AND status = '1')";	
		$db = Zend_Registry::get("db");	
		$follow = $db->fetchAll($sql);	
		$friendlist = array();		
		for($i=0; $i < count($follow); $i++)
		{			
			$friendlist[] = $follow[$i]->friend_id;
		}

		$friendlistStr = implode(",",$friendlist);
		if(count($friendlist) > 0)
		{
				$sqlitem = "SELECT distinct w.id AS item_id, w.* FROM userwishlist uw  
			JOIN wishlistitmes w ON w.id = uw.item_id
			WHERE uw.user_id IN (".$friendlistStr.") ORDER BY  ".$sortby."  ";
			//echo $sqlitem.'<br>';
			$result = $db->fetchAll($sqlitem);

		}
		return $result;
	}
	public static function countUserFriendsWishlist($user_id)
	{		
		$sql = "SELECT friend_id 
				FROM usersfriend 
				WHERE (user_id = '".$user_id."' AND status = '1')";	
	
		$db = Zend_Registry::get("db");	
		$follow = $db->fetchAll($sql);
	
		$friendlist = array();		
		for($i=0; $i < count($follow); $i++)
		{			
			$friendlist[] = $follow[$i]->friend_id;
		}
		$friendlistStr = implode(",",$friendlist);
		if(count($friendlist) > 0)
		{
			$sqlitem = "SELECT count(distinct w.id) AS totalCount FROM wishlistitmes w  
						INNER JOIN userwishlist  uw ON w.id = uw.item_id
			WHERE uw.user_id IN (".$friendlistStr.") ";
			$result = $db->fetchRow($sqlitem);
		}
		return $result;
	}
	public static function getFriendTags($user_id){
		$sql = "SELECT friend_id 
				FROM usersfriend 
				WHERE user_id = '".$user_id."' AND status = '1'";	
	
		$db = Zend_Registry::get("db");	
		$follow = $db->fetchAll($sql);
		$friendlist = array();		
		for($i=0; $i < count($follow); $i++)
		{			
			$friendlist[] = $follow[$i]->friend_id;
		}
		$friendlistStr = implode(",",$friendlist);
		if(count($friendlist) > 0)
		{
			 $sql = "SELECT * 
				FROM category 
				WHERE created_by IN ('".$friendlistStr."')  AND status = '1'";
			
			$result = $db->fetchAll($sql);
		}
		return $result;
	}
	/// Top of the week
	/*public static function getTopWeek($col, $ord)
	{
		$today = date('Y-m-d');
		$predate = date("Y-m-d", mktime(0, 0, 0, date('m'), date('d')-7, date("Y")));
		//echo $nxtdate;
		if($col != '')
		{
			if($col == 'item_type')
			{
				$orderby = ' AND '.$col.'='.$ord;
			}else{
				$orderby = ' ORDER BY '.$col.' '.$ord;
			}
		}else{
			$orderby = ' ORDER BY item_name ASC';
		}
		$sql = "SELECT DISTINCT(uw.item_id), w.* 
				FROM wishlistitmes w
				LEFT JOIN userwishlist uw ON w.id = uw.item_id
				WHERE DATE_FORMAT(uw.added_date, '%Y-%m-%d') BETWEEN '".$predate."' AND '".$today."' ".$orderby;
		
		$db = Zend_Registry::get("db");	
		$result = $db->fetchAll($sql);
		
		if(empty($result))
		{
			$sql = "SELECT DISTINCT(uw.item_id), w.*
				FROM wishlistitmes w
				LEFT JOIN userwishlist uw ON w.id = uw.item_id
				 ".$orderby." LIMIT 0,10";
			$db = Zend_Registry::get("db");	
			$result = $db->fetchAll($sql);
		}
		//echo $sql;
		return $result;
	}*/
	
	public static function getTopWeek($orderBy, $category)
	{
		$today = date('Y-m-d');
		$predate = date("Y-m-d", mktime(0, 0, 0, date('m'), date('d')-7, date("Y")));
		//echo $nxtdate;
		if($category != '')
		{
				$categorySql = ' AND w.item_type ='.$category;
				$AllcategorySql = ' WHERE w.item_type ='.$category;
		}
			
		if($orderBy != '')
		{
			$orderby = ' ORDER BY '.$orderBy;
		}else{
			$orderby = ' ORDER BY item_name ASC';
		}
		
		$sql = "SELECT DISTINCT(uw.item_id), w.* 
				FROM wishlistitmes w
				LEFT JOIN userwishlist uw ON w.id = uw.item_id
				WHERE uw.public = '1' AND DATE_FORMAT(uw.added_date, '%Y-%m-%d') BETWEEN '".$predate."' AND '".$today."' ". $categorySql ." ". $orderby;
		
		//echo $sql;
		$db = Zend_Registry::get("db");	
		$result = $db->fetchAll($sql);
		
		if(empty($result))
		{
			$sql = "SELECT DISTINCT(uw.item_id), w.*
				FROM wishlistitmes w
				LEFT JOIN userwishlist uw ON w.id = uw.item_id ".$AllcategorySql." AND uw.public = '1'"
				 .$orderby." LIMIT 0,10";
			$db = Zend_Registry::get("db");	
			$result = $db->fetchAll($sql);
		}
		//echo $sql;
		return $result;
	}
	//check udid for a user
	public static function chkUserUDID($user_id, $table)
	{
		$sql = "SELECT * FROM ".$table." WHERE user_id = '".$user_id."'";
		
		$db = Zend_Registry::get("db");	
		$result = $db->fetchOne($sql);
		return $result;
	}
	
	public static function chkUserExistUDID($user_id, $udid, $table)
	{
		//return same UDID/ diffrent
		$sql = "SELECT id FROM ".$table." WHERE user_id = '".$user_id."' AND udid = '".$udid."'";
		$db = Zend_Registry::get("db");	
		$result = $db->fetchOne($sql);
		if($result)
		{
			return '1'; //same
		}else{
			return '0'; //different
		}
	}
        
        public static function markProductCurated($id) {
            
        }
	
	public static function getSomeFeed($userID, $max_item_id)
	{	
		$db = Zend_Registry::get("db");	
		$idsStr = '';
		///user's item
		$sqlItemU = "SELECT item_id FROM userwishlist WHERE user_id = '".$userID."'";
		$resitemids = $db->fetchAll($sqlItemU);
		
		for($i=0; $i < count($resitemids); $i++)
		{
			$ids[] = $resitemids[$i]->item_id;
		}
		if(count($ids) > 0)
		{			
			$idsStr = implode(",",$ids);
		}else{			
			$idsStr = '0';
		}
		//$idsStr = implode(",",$ids);
		/*
		$sql = "SELECT DISTINCT(w.id),w.*, w.id AS item_id
				FROM wishlistitmes w
				LEFT JOIN userwishlist uw ON w.id = uw.item_id
				WHERE uw.user_id != '".$userID."' AND uw.item_id > '".$max_item_id."' ORDER BY item_count DESC";*/
		
		$sql = "SELECT DISTINCT(w.id),w.*, w.id AS item_id
				FROM wishlistitmes w
				LEFT JOIN userwishlist uw ON w.id = uw.item_id
				WHERE uw.item_id NOT IN (".$idsStr.") AND uw.item_id > '".$max_item_id."' ORDER BY item_count DESC";
				
				  
		//echo $sql;
		//exit;
		
		$result = $db->fetchAll($sql);
		return $result;
	}
	
	public static function getAllProductslist($userID,$item_idx)
	{		
		$orderby = ' ORDER BY item_name ASC';
		/*$sql = "SELECT DISTINCT(w.id), w.*, w.id AS item_id
				FROM wishlistitmes w
				LEFT JOIN userwishlist uw ON w.id = uw.item_id
				WHERE uw.user_id = '".$userID."' AND uw.item_id > '".$item_idx."' ".$orderby;*/
				
		$sql = "SELECT w.item_name, w.id AS item_id, w.item_type, w.item_price, w.image, w.description,	w.item_count, w.vendor_link, w.isbn, w.added_from, w.added_date, uw.id AS wid,uw.added_date AS uadded_date,barcode_number FROM userwishlist uw LEFT JOIN wishlistitmes w ON w.id = uw.item_id WHERE uw.user_id = '".$userID."' AND uw.id > '".$item_idx."' ".$orderby;
		
		//echo $sql;
		$db = Zend_Registry::get("db");	
		$result = $db->fetchAll($sql);
		return $result;
	}
	
	public static function getFriendsItems($user_id, $max_item_id)
	{
		
		$sql = "SELECT friend_id 
				FROM usersfriend 
				WHERE (user_id = '".$user_id."' AND status = '1')";				
		$sql1 = "SELECT user_id 
				FROM usersfriend 
				WHERE (friend_id = '".$user_id."' AND status = '1')";	
		
		$db = Zend_Registry::get("db");	
		$follow = $db->fetchAll($sql);
		$follower = $db->fetchAll($sql1);
		
		$friendlist = array();
		
		for($i=0; $i < count($follow); $i++)
		{
			
			$friendlist[] = $follow[$i]->friend_id;
		}
		/* for($i=0; $i < count($follower); $i++)
		{
			
			$friendlist[] = $follower[$i]->user_id;
		} */
		
		$friendlistStr = implode(",",$friendlist);
		if(count($friendlist) > 0)
		{
			$sqlitem = "SELECT uw.added_date AS wishdate, w.id AS item_id, uw.user_id AS friendid, w.* FROM userwishlist uw  
						LEFT JOIN wishlistitmes w ON w.id = uw.item_id
			WHERE uw.item_id > '".$max_item_id."' AND uw.user_id IN (".$friendlistStr.")";
			//echo $sqlitem.'<br>';
			$result = $db->fetchAll($sqlitem);
		}
		return $result;
	}
	
	
	public static function getFriendsFeed($user_id, $orderby, $category)
	{
		
		$sql = "SELECT friend_id 
				FROM usersfriend 
				WHERE (user_id = '".$user_id."' AND status = '1')";	
				
		$db = Zend_Registry::get("db");	
		$follow = $db->fetchAll($sql);
		//echo $orderby.'<br>';
		
		$friendlist = array();
		
		for($i=0; $i < count($follow); $i++)
		{
			
			$friendlist[] = $follow[$i]->friend_id;
		}
		
		
		
		$friendlistStr = implode(",",$friendlist);
		if(count($friendlist) > 0)
		{
			////
		if($category != '')
		{
			$categorySql = ' AND w.item_type ='.$category;
		}
		if($orderby != '')
		{
			$orderby = ' ORDER BY '.$orderby;
		}else{
			$orderby = ' ORDER BY uw.added_date DESC';
		}
		
			$sqlitem = "SELECT DISTINCT(w.id), w.*
					FROM wishlistitmes w
					LEFT JOIN userwishlist uw ON w.id = uw.item_id
					WHERE uw.public = '1' AND uw.user_id IN (".$friendlistStr.") ".$categorySql." ".$orderby. ' LIMIT 0,9';
			////
			//echo $sqlitem;
			$result = $db->fetchAll($sqlitem);
		}
		return $result;
	}
	
	public static function getAllFeedSite($userID, $orderBy, $category)
	{			
		if($category != '')
		{
				$categorySql = ' AND w.item_type ='.$category;
		}
		if($orderBy != '')
		{
			$orderby = ' ORDER BY '.$orderBy;
		}else{
			$orderby = ' ORDER BY uw.added_date DESC';
		}
				  
		$sql = "SELECT DISTINCT(w.id),w.* 
				FROM wishlistitmes w
				LEFT JOIN userwishlist uw ON w.id = uw.item_id
				WHERE    uw.user_id = '" . $userID . "'" .$categorySql."
				  ".$orderby." LIMIT 0, 9";

		//echo $sql;
		$db = Zend_Registry::get("db");	
		$result = $db->fetchAll($sql);
		return $result;
	}

        /* Show products irrespective of whether its curted */
        public static function getLoadMoreHomePage($userID,$orderBy, $category, $start, $numberOfItems)
        {
                $orderby = ' ORDER BY uw.added_date DESC';

                $sql = "SELECT DISTINCT(w.id),w.*, uw.gift_state AS gift_state, uw.reserved_by AS reserved_by 
                                FROM wishlistitmes w
                                LEFT JOIN userwishlist uw ON w.id = uw.item_id
                                WHERE    uw.user_id = '".$userID."' AND w.item_count > 0 ".$orderby." LIMIT ".$start.",".$numberOfItems;

                $db = Zend_Registry::get("db");
                $result = $db->fetchAll($sql);
                return $result;
        }

        public static function getLoadMoreLandingPage($userID,$orderBy, $category, $start, $numberOfItems)
        {
            $orderby = ' ORDER BY uw.added_date DESC';

            /* Select only curted products : w.state = 1*/
            $sql = "SELECT DISTINCT(w.id),w.*  
                    FROM wishlistitmes w LEFT JOIN userwishlist uw ON w.id = uw.item_id 
                    WHERE   (w.state='2' OR w.state='3') AND uw.public = '1'  ".$orderby." LIMIT ".$start.",".$numberOfItems;
            $db = Zend_Registry::get("db");
            $result = $db->fetchAll($sql);
            return $result;
        }

        public static function getFeaturedCategoryProducts($userID,$orderBy, $category, $start, $numberOfItems)
        {
            $orderby = ' ORDER BY w.updated_date DESC';

            /* Select only curted products : w.state = 1*/
            $sql = "SELECT DISTINCT(w.id),w.*  
                    FROM wishlistitmes w LEFT JOIN userwishlist uw ON w.id = uw.item_id 
                    WHERE   (w.state='2' OR w.state='3') AND uw.public = '1'  ".$orderby." LIMIT ".$start.",".$numberOfItems;
            $db = Zend_Registry::get("db");
            $result = $db->fetchAll($sql);
            return $result;
        }

 
        public static function getSharedItemsLandingPage($userID, $ItemIds)
        {
            $orderby = ' ORDER BY uw.added_date DESC';
            /* Select only curted products : w.state = 1*/
            $sql = "SELECT DISTINCT(w.id),w.*  
                    FROM wishlistitmes w LEFT JOIN userwishlist uw ON w.id = uw.item_id 
                    WHERE    uw.public = '1' AND uw.user_id = ". $userID ." AND uw.item_id IN ". $ItemIds. $orderby ;
            $db = Zend_Registry::get("db");
            $result = $db->fetchAll($sql);
            return $result;
        }
 
        public static function getLoadMoreCuratePage ($start, $numberOfItems, $type) {
            $db = Zend_Registry::get("db");    
            $orderby = ' ORDER BY w.added_date DESC';

            /* No need of left join - just query for all products not curated.
             * $sql = "SELECT DISTINCT(w.id),w.*  
                    FROM wishlistitmes w LEFT JOIN userwishlist uw ON w.id = uw.item_id AND w.state=0
                    WHERE    uw.public = '1' ".$orderby." LIMIT ".$start.",".$numberOfItems;
             */

             /* all user items are available for curation and featuring except private items */
              if ($type == '1') {
                  $sql = "SELECT DISTINCT(w.id), w.*, w.id AS item_id
                       FROM wishlistitmes w LEFT JOIN userwishlist uw ON w.id = uw.item_id
                       WHERE (w.state = '0') AND (uw.public = '1') " .$orderby." LIMIT ".$start."," .$numberOfItems;
              } else if($type == '2') {
                  $sql = "SELECT DISTINCT(w.id), w.*, w.id AS item_id
                       FROM wishlistitmes w LEFT JOIN userwishlist uw ON w.id = uw.item_id
                       WHERE (w.state = '1' OR w.state = '3') AND (uw.public = '1') " .$orderby." LIMIT ".$start."," .$numberOfItems;
              } else if($type == '3') {
                  $sql = "SELECT DISTINCT(w.id), w.*, w.id AS item_id
                       FROM wishlistitmes w LEFT JOIN userwishlist uw ON w.id = uw.item_id
                       WHERE (w.state = '2' OR w.state = '3') AND (uw.public = '1') " .$orderby." LIMIT ".$start."," .$numberOfItems;
              } else if ($type == '4') {
                     $sql = "SELECT DISTINCT(w.id), w.*, w.id AS item_id FROM wishlistitmes w
                       WHERE w.item_type LIKE '%top_items_%'" .$orderby." LIMIT ".$start."," .$numberOfItems;
              }else {
                    $sql = "SELECT DISTINCT(w.id), w.*, w.id AS item_id
                       FROM wishlistitmes w LEFT JOIN userwishlist uw ON w.id = uw.item_id
                       WHERE (w.state = '0') AND (uw.public = '1') " .$orderby." LIMIT ".$start."," .$numberOfItems;
              }
            $result = $db->fetchAll($sql);
            return $result;
        }

        public static function getLoadMoreFeedPage($userID,$orderBy, $category, $start, $numberOfItems, $except)
        {
           $db = Zend_Registry::get("db");
           $logger = Zend_Registry::get("logger");
           $orderby = ' ORDER BY uw.added_date DESC';

           /* Select only curted products for feed-all */
           if ($category == 'tech' || $category == 'electronic') {
                $category = $category.'|electronic|tv|computer|laptops|tablets|cameras|phones';
           } else if ($category == 'kid') {
                $category = $category.'|boy|girl|toy|game|strollers|Cradle|Crib';
           } else if ($category == 'men') {
                #$category = $category.'|accessories|clothing|shoes';
           }else if($category == 'other') {
               $category = '';
           }
 
          $sql = "SELECT  DISTINCT tg.item_id AS id_item FROM category AS t LEFT JOIN item_tags AS tg ON t.id = tg.tag_id WHERE t.name REGEXP '^".$category."' GROUP BY tg.item_id ORDER BY tg.tag_date DESC";

          $stmt = $db->fetchAll($sql);  

          //$logger->info("Chikke count ". count($stmt));
          $itemIds='';
          foreach ($stmt as $item) {
                //$logger->info("item id ". $item->t_name);
                if($item->id_item != '') {
                    $itemIds = $item->id_item . ',' . $itemIds;
                }
          }
          // $logger->info("Chikke count ". $itemIds);
          $arrayIds = array_map('intval', explode(',', $itemIds));
          $arrayIds = implode("','",$arrayIds);

          $sqlitem = "SELECT DISTINCT(w.id), w.*, w.id AS item_id
                       FROM wishlistitmes w LEFT JOIN userwishlist uw ON w.id = uw.item_id
                       WHERE w.id IN ('".$arrayIds."') AND uw.user_id != '".$userID."' AND (uw.public = '1') " .$orderby." LIMIT ".$start."," .$numberOfItems;

            $results = $db->fetchAll($sqlitem);
            $result = [];
                foreach($results as $item){
                    $userwished = wishlistitmes::chkWishedItem($item->id, $_SESSION['user_id']);
                    if ($userwished) {
                        continue; /* user already has this item */
                    }
                    $result[] = $item;
                }
            return $result;
      }

  public static function getLoadMoreFeedPageFriends($userID,$orderBy, $category, $start, $numberOfItems, $except)
        {
            $sql = "SELECT friend_id
                    FROM usersfriend
                    WHERE (user_id = '".$userID."' AND status = '1')";

            $db = Zend_Registry::get("db");
            $logger = Zend_Registry::get("logger");

            $follow = $db->fetchAll($sql);
            $friendlist = array();

            for($i=0; $i < count($follow); $i++)
            {
                    $friendlist[] = $follow[$i]->friend_id;
            }
            $friendlistStr = implode(",",$friendlist);
            if(count($friendlist) > 0)
            {
                $orderby = ' ORDER BY uw.added_date DESC';
                $sqlitem = "SELECT DISTINCT(w.id), w.*, w.id AS item_id
                            FROM wishlistitmes w LEFT JOIN userwishlist uw ON w.id = uw.item_id 
                            WHERE uw.user_id IN (".$friendlistStr.") AND w.item_count > 0 AND (uw.public != '0') AND (uw.public != '2') AND (uw.public != '3')
                            AND w.id != '".$except."' ".$orderby." LIMIT ".$start."," .$numberOfItems;

                $results = $db->fetchAll($sqlitem);
            }
            $result = [];
                foreach($results as $item){
                    $userwished = wishlistitmes::chkWishedItem($item->id, $_SESSION['user_id']);
                    if ($userwished) {
                         continue; /* user already has this item */
                    }
                    if ($item->sharewith_group == ''){
                        $result[] = $item;
                    } else if(groups::checkUserInGroups($_SESSION['user_id'], $item->sharewith_group)){
                       $result[] = $item;
                    }
                }

            return $result;
      }

    public static function getLoadMoreFeedPageReco($userID,$orderBy, $category, $start, $numberOfItems, $except)
    {
        $db = Zend_Registry::get("db");

        $sql = "SELECT DISTINCT(w.id), w.*, w.id AS item_id FROM wishlistitmes w LEFT JOIN activity a ON w.id = a.item_id WHERE receiver = '".$userID."' 
                 AND sender <> '".$userID."'  AND w.id != '".$except."' AND activity_type IN ('recommend item') 
                 ORDER BY a.activity_date DESC LIMIT ".$start."," .$numberOfItems;

        return $db->fetchAll($sql);
    }

      // get user events for each item
    public static function getLoadMoreFeedPageEvents($userID, $start, $numberOfItems, $eventID)
        {
            $db = Zend_Registry::get("db");
            $logger = Zend_Registry::get("logger");

            $items = $db->fetchAll( "SELECT DISTINCT(item_id) FROM event_items WHERE event_id = '" . $eventID . "'");  
            if (count($items)) {
                for($i=0; $i < count($items); $i++)
                {
                    $itemid_list[] = $items[$i]->item_id;
                }
                $itemid_list_str = implode(",",$itemid_list);

                $sqlitem = "SELECT DISTINCT(w.id), w.*, w.id AS item_id
                            FROM wishlistitmes w  
                            WHERE w.id IN (".$itemid_list_str.")";
                $results = $db->fetchAll($sqlitem);
                return $results;
            } else {
              return NULL;
            }
        }

      /* Show only curated products added by friends */
        public static function getLoadMoreFriendPage($userID,$orderBy, $category, $start, $numberOfItems)
        {
                $db = Zend_Registry::get("db");

                $orderby = ' ORDER BY uw.added_date DESC';

                $sql = "SELECT w.item_name, w.*, uw.added_date, uw.gift_state AS gift_state, uw.reserved_by AS reserved_by, uw.id AS wid,uw.added_date AS uadded_date,barcode_number,
                        uw.public, uw.sharewith_group, uw.share_individual 
                        FROM userwishlist uw
                        JOIN wishlistitmes w ON w.id = uw.item_id
                        WHERE ((uw.user_id = '".$userID."') AND (uw.public != '0')
                            AND ((uw.share_individual = '') OR (uw.share_individual = '". $_SESSION['user_id']."')))
                        ".$orderby." LIMIT ".$start.",".$numberOfItems;
                $results = $db->fetchAll($sql);
                $result = [];
                foreach($results as $item){
                    if ($item->sharewith_group == ''){
                        $result[] = $item;
                    } else if(groups::checkUserInGroups($_SESSION['user_id'], $item->sharewith_group)){
                        $result[] = $item;
                    }
                }
                return $result;
        }

	/// load items on scrolling
	public static function getAllFeedSiteScroll($userID,$orderBy, $category, $limit)
	{			
		if($category != '')
		{
				$categorySql = ' AND w.item_type ='.$category;
		}
		if($orderBy != '')
		{
			$orderby = ' ORDER BY '.$orderBy;
		}else{
			$orderby = ' ORDER BY uw.added_date DESC';
		}
		$sql = "SELECT DISTINCT(w.id),w.* , w.id AS item_id
				FROM wishlistitmes w
				LEFT JOIN userwishlist uw ON w.id = uw.item_id
				WHERE uw.public = '1' ".$categorySql." 
				  ".$orderby." LIMIT ".$limit. ", 5";
		//echo '<br>'.$sql;
		//exit;
		$db = Zend_Registry::get("db");	
		$result = $db->fetchAll($sql);
		return $result;
	}
	
	// load feed page item on scrolling
	public static function getAllFeedPageScroll($user_id,$orderBy, $category, $limit)
	{			
		
		$sql = "SELECT friend_id 
				FROM usersfriend 
				WHERE (user_id = '".$user_id."' AND status = '1')";	
				
		$db = Zend_Registry::get("db");	
		$follow = $db->fetchAll($sql);	
		$friendlist = array();
		
		for($i=0; $i < count($follow); $i++)
		{			
			$friendlist[] = $follow[$i]->friend_id;
		}
		$friendlistStr = implode(",",$friendlist);
		if(count($friendlist) > 0)
		{
			////
			if($category != '')
			{
				$categorySql = ' AND w.item_type ='.$category;
			}
			if($orderBy != '')
			{
				$orderby = ' ORDER BY '.$orderBy;
			}else{
				$orderby = ' ORDER BY uw.added_date DESC';
			}
		
			$sqlitem = "SELECT DISTINCT(w.id), w.*, w.id AS item_id
					FROM wishlistitmes w
					LEFT JOIN userwishlist uw ON w.id = uw.item_id
					WHERE uw.user_id IN (".$friendlistStr.") ".$categorySql." ".$orderby .' LIMIT '.$limit.', 5';
			////
			
			$result = $db->fetchAll($sqlitem);
		}
		return $result;
	}
	
	///
	
	public static function getDeleteLog()
	{
		$sql = "SELECT item_id, user_id, deleted_date FROM delete_log ORDER BY deleted_date DESC";
		$db = Zend_Registry::get("db");	
		$result = $db->fetchAll($sql);
		return $result;
	}
	
	public static function searchResult($user_id, $searchKey, $orderBy, $category)
	{
		
		//echo "test:".$category.'<br>';
		if($orderBy != '')
		{
			if($category != '')
			{
				$orderby = ' AND item_type ='.$category.' ORDER BY '.$orderBy;
			}else{
				$orderby = ' ORDER BY '.$orderBy;
			}
		}else{
			$orderby = ' ORDER BY item_name ASC';
		}
		/*$sql = "SELECT DISTINCT(w.id),w.* 
				FROM wishlistitmes w
				LEFT JOIN userwishlist uw ON w.id = uw.item_id
				WHERE  (w.item_name LIKE '%".$searchKey."%' || w.description LIKE '%".$searchKey."%') AND uw.user_id != '".$user_id."'".$orderby;*/
		$sql = "SELECT DISTINCT(w.id),w.* 
				FROM wishlistitmes w
				LEFT JOIN userwishlist uw ON w.id = uw.item_id
                                LEFT JOIN item_tags AS it ON it.item_id = w.id
                                LEFT JOIN category AS ct ON ct.id = it.tag_id
				WHERE  (w.item_name LIKE '%".$searchKey."%' || w.description LIKE '%".$searchKey."%' || ct.name LIKE '%".$searchKey."%') ".$orderby;
				
		//echo $sql;
		//exit;
		$db = Zend_Registry::get("db");	
		$result = $db->fetchAll($sql);
		return $result;
		
	}
	
	public static function getOneUserWishlist($user_id, $currentUser)
	{
		$db = Zend_Registry::get("db");	
						
		  $sql = "SELECT w.item_name, w.id AS item_id, w.item_type, w.image, w.description, w.item_price, w.item_count, w.vendor_link, w.isbn, w.added_from, uw.added_date, uw.id AS wid, uw.items_required_no AS items_required_no,w.barcode_number FROM userwishlist uw JOIN wishlistitmes w ON w.id = uw.item_id WHERE uw.user_id = '".$user_id."' ORDER BY w.item_name";		
		 
		 // $sql = "SELECT w.item_name, w.id AS item_id, w.item_type, w.image, w.description, w.item_price, w.item_count, w.vendor_link, w.isbn, w.added_from, uw.*, uw.id AS wid, uw.items_required_no AS items_required_no,w.barcode_number FROM userwishlist uw JOIN wishlistitmes w ON w.id = uw.item_id WHERE uw.user_id = '".$user_id."' ORDER BY w.item_name";	
		$result = $db->fetchAll($sql);
		return $result;
	}
	
	
	
	public static function isUserPurchaseItem($userwishlist_id, $user_id)
	{
		$db = Zend_Registry::get("db");	
		
		$sql = "SELECT purchase_id FROM purchase_items WHERE userwishlist_id='".$userwishlist_id."' AND user_id = '".$user_id."'";
		$result = $db->fetchOne($sql);		
		
		return $result;
	}
	
	public static function getUserListOfPurchaseItems($userwishlist_id, $user_id)
	{
		$db = Zend_Registry::get("db");	
		
		$sql = "SELECT CONCAT(first_name, ' ',last_name) AS name, pi.user_id as user_id,purchase_date FROM purchase_items as pi
				JOIN users as u ON pi.user_id=u.user_id WHERE userwishlist_id='".$userwishlist_id."' AND pi.user_id != '".$user_id."'";		
		$result = $db->fetchAll($sql);		
		
		return $result;
	}
	
	/// item is added by any user
	public static function anyUserWished($itemID)
	{
		$sql = "SELECT id FROM userwishlist WHERE item_id = '".$itemID."'";
		$db = Zend_Registry::get("db");	
		$result = $db->fetchOne($sql);
	}
	
	public static function getItemCount($itemid)
	{
		$db = Zend_Registry::get("db");	
		$result = $db->fetchOne("SELECT item_count FROM wishlistitmes WHERE id = ?", $itemid);
		return $result;
	}
	
	public static function getUserWishesItem($itemid, $user_id)
	{
		$db = Zend_Registry::get("db");
		$result = $db->fetchRow("SELECT * FROM userwishlist WHERE item_id = ? AND user_id = ?",  array($itemid, $user_id));
		
		return $result;
	}
	
	// getproduct name
	public static function getProductName($itemid)
	{
		$db = Zend_Registry::get("db");
		$result = $db->fetchOne("SELECT item_name FROM wishlistitmes WHERE id = ?",$itemid);
		
		return $result;
	}
	
	/// get privacy setting of wishlist, based on this item will be showed
	// $user_id is friend ID
	public static function showItem($itemid, $loggedInuser_id, $user_id)
	{
		$db = Zend_Registry::get("db");
		$item = $db->fetchRow("SELECT *, public AS privacy FROM userwishlist WHERE item_id = ? AND user_id = ?",  array($itemid, $user_id));
		
		$showFlag = 0;
		
		/// user's detail
		$userDetail = users::getUserDetails($user_id);
		$isfollower = usersfriend::checkExistFriends($_SESSION['user_id'], $user_id);
		//$isfollower = usersfriend::checkExistFriends($loggedInuser_id, $user_id);
		
		if($userDetail->profile_type == 'Private' && $isfollower != '')
		{
			/// only follower can view my wishlist
			$showFlag = 1;
			//echo '<br>test:1';
		}
		else if($item->privacy == '1')
		{
			$showFlag = 1;
			//echo '<br>test:2';
		}
		else if($item->privacy == '0')
		{
			$showFlag = 0;
			//echo '<br>test:3';
		}
		else{
			// check for loggedIn user's Group;
			/// check for logged in use' ID with shared user's ID
			//$groups = $db->fetchAll("SELECT fg.group_id FROM friendsgroup fg LEFT JOIN groups g ON g.id = fg.group_id WHERE fg.user_id = ? AND  g.created_by = ? ",  array($user_id, $loggedInuser_id));
			//$groups = $db->fetchAll("SELECT group_id FROM friendsgroup WHERE user_id = ?",  $user_id);
			$groups = $db->fetchAll("SELECT group_id FROM friendsgroup WHERE user_id = ?",  $loggedInuser_id);
			for($i=0; $i < count($groups); $i++)
			{
				$user_groups[] = $groups[$i]->group_id;
			}
			$sharedWith_group = explode(',',$item->sharewith_group);
			$sharedWith_user = explode(',',$item->share_individual);
			
			$showGroup = array_intersect($user_groups, $sharedWith_group);
			//echo "group:".$showGroup;
			if(!empty($showGroup))
			{
				$showFlag = 1;
				//echo '<br>test:4';
			}
			else if(in_array($loggedInuser_id, $sharedWith_user))
			{
				$showFlag = 1;
				//echo '<br>test:6';
			}else{
				$showFlag = 0;
				//echo '<br>test71';
			}
		}// else
		
		return $showFlag;
	}// function
	public static function showItemApp($itemid, $loggedInuser_id, $user_id)
	{
		$db = Zend_Registry::get("db");
	 	$item = $db->fetchRow("SELECT *, public AS privacy FROM userwishlist WHERE item_id = ? AND user_id = ?",  array($itemid, $user_id));
		
		$showFlag = 0;
		
		/// user's detail
		//$userDetail = users::getUserDetails($user_id);
		
		//$isfollower = usersfriend::checkExistFriends($_SESSION['user_id'], $user_id);
	//	$isfollower = usersfriend::checkExistFriends($loggedInuser_id, $user_id);
		
	//	if($userDetail->profile_type == 'Private' && $isfollower != '')
	//	{
			/// only follower can view my wishlist
			
		//	$showFlag = 1;	
			//echo '<br>Test:1';
			
		//}
		//else if($item->privacy == '1')
    
		if($item->privacy == '1')
		{
			$showFlag = 1;
			//echo '<br>Test:2';
		}
		else if($item->privacy == '0')
		{
			$showFlag = 0;	
			//echo '<br>Test:6';				
		}
		
		else{
			// check for loggedIn user's Group;
			/// check for logged in use' ID with shared user's ID
			//$groups = $db->fetchAll("SELECT fg.group_id FROM friendsgroup fg LEFT JOIN groups g ON g.id = fg.group_id WHERE fg.user_id = ? AND  g.created_by = ? ",  array($user_id, $loggedInuser_id));
			//$groups = $db->fetchAll("SELECT group_id FROM friendsgroup WHERE user_id = ?",  $user_id);
			$groups = $db->fetchAll("SELECT group_id FROM friendsgroup WHERE user_id = ?",  $loggedInuser_id);
			for($i=0; $i < count($groups); $i++)
			{
				$user_groups[] = $groups[$i]->group_id;
			}
			$sharedWith_group = explode(',',$item->sharewith_group);
			$sharedWith_user = explode(',',$item->share_individual);
			
			$showGroup = array_intersect($user_groups, $sharedWith_group);
			/*echo 'user_group';
			print_r($user_groups);
			echo 'item group';
			print_r($sharedWith_group);*/
			if(!empty($showGroup))
			{
				$showFlag = 1;	
				//echo '<br>Test:3';					
			}
			else if(in_array($loggedInuser_id, $sharedWith_user))
			{
				$showFlag = 1;
				//echo '<br>Test:4';
			}else{
				$showFlag = 0;	
				//echo '<br>Test:5';
			}
		}// else	
		

return $showFlag;
		
}
public static function showItemAppforfollwers($itemid, $loggedInuser_id, $user_id)
	{
		$db = Zend_Registry::get("db");
	 	$item = $db->fetchRow("SELECT *, public AS privacy FROM userwishlist WHERE item_id = ? AND user_id = ?",  array($itemid, $user_id));
		
		$showFlag = 0;
		
		/// user's detail
		//$userDetail = users::getUserDetails($user_id);
		
		//$isfollower = usersfriend::checkExistFriends($_SESSION['user_id'], $user_id);
		$isfollower = usersfriend::checkExistFriends($loggedInuser_id, $user_id);
		
	//	if($userDetail->profile_type == 'Private' && $isfollower != '')
	//	{
			/// only follower can view my wishlist
			
		//	$showFlag = 1;	
			//echo '<br>Test:1';
			
		//}
		//else if($item->privacy == '1')
        if($isfollower!=''){
		if($item->privacy == '1')
		{
			$showFlag = 1;
			//echo '<br>Test:2';
		}
		else if($item->privacy == '0')
		{
			$showFlag = 0;	
			//echo '<br>Test:6';				
		}
		
		else{
			// check for loggedIn user's Group;
			/// check for logged in use' ID with shared user's ID
			//$groups = $db->fetchAll("SELECT fg.group_id FROM friendsgroup fg LEFT JOIN groups g ON g.id = fg.group_id WHERE fg.user_id = ? AND  g.created_by = ? ",  array($user_id, $loggedInuser_id));
			//$groups = $db->fetchAll("SELECT group_id FROM friendsgroup WHERE user_id = ?",  $user_id);
			$groups = $db->fetchAll("SELECT group_id FROM friendsgroup WHERE user_id = ?",  $loggedInuser_id);
			for($i=0; $i < count($groups); $i++)
			{
				$user_groups[] = $groups[$i]->group_id;
			}
			$sharedWith_group = explode(',',$item->sharewith_group);
			$sharedWith_user = explode(',',$item->share_individual);
			
			$showGroup = array_intersect($user_groups, $sharedWith_group);
			/*echo 'user_group';
			print_r($user_groups);
			echo 'item group';
			print_r($sharedWith_group);*/
			if(!empty($showGroup))
			{
				$showFlag = 1;	
				//echo '<br>Test:3';					
			}
			else if(in_array($loggedInuser_id, $sharedWith_user))
			{
				$showFlag = 1;
				//echo '<br>Test:4';
			}else{
				$showFlag = 0;	
				//echo '<br>Test:5';
			}
		}// else	
		
}
return $showFlag;
		
}
// function
	//Get total amount of item
	public static function getQty($itemid, $user_id)
	{
		$db = Zend_Registry::get("db");
		//echo "SELECT items_required_no FROM userwishlist WHERE user_id = '".$user_id."' AND  item_id = '".$itemid."'";
		$qty = $db->fetchOne("SELECT items_required_no FROM userwishlist WHERE user_id = ? AND  item_id = ?",  array($user_id,$itemid));
		
		return $qty;
	}
	
	/// get user's tags
	public static function getusertag($user_id)
	{
		$db = Zend_Registry::get("db");
		$sql = "SELECT * 
				FROM category 
				WHERE created_by = '".$user_id."' AND status = '1'";
			
		$result = $db->fetchAll($sql);
		return $result;
	}
	
	
	public static function getOneUserTaggedWishlist($friend_id, $tagid)
	{
		$db = Zend_Registry::get("db");
		
		//Get items from which are tagged in event
		$itemArrI = array();
		$TagedItem = array();
		$itemArr = array();
		
		$eventTagedItem = $db->fetchOne("SELECT item_id FROM taggifts WHERE tag_id  = ? AND user_id = ?", array($tagid,$friend_id) );
		if($eventTagedItem != '')
		{
			$itemArr = explode(',',$eventTagedItem);
		}
		
		$TagedItem = $db->fetchAll("SELECT item_id FROM item_tags WHERE tag_id  = ? AND user_id = ?", array($tagid,$friend_id));
		
		for($i = 0; $i < count($TagedItem); $i++)
		{
			$itemArrI[] = $TagedItem[$i]->item_id;
		}
		
		if($itemArr != '' && $itemArrI != '')
		{
			
			$finalItems = array_merge($itemArr,$itemArrI);
		}
		if(!empty($itemArr) && empty($itemArrI))
			$finalItems = $itemArr;
		if(empty($itemArr) && !empty($itemArrI))
			$finalItems = $itemArrI;
		
		$itemStr = implode(",", $finalItems);

		/// get items wich are directly tagged
		if($itemStr != '')
		{		
		$sql = "SELECT w.item_name, w.id AS item_id, w.item_type, w.image, w.description, w.item_price, w.item_count, w.vendor_link, w.isbn, w.added_from, uw.added_date, uw.id AS wid, uw.items_required_no AS items_required_no,w.barcode_number FROM userwishlist uw JOIN wishlistitmes w ON w.id = uw.item_id WHERE uw.user_id = '".$friend_id."' AND w.id IN(".$itemStr.") ORDER BY w.item_name";	
	//	echo $sql;
		$result = $db->fetchAll($sql);
			return $result;
		}else{
			$result = '';
			return $result;
		}
		
	}
	
	//// get sharing option values for an item
	public static function getShareOption($user_id, $item_id)
	{
		$db = Zend_Registry::get("db");
		$result = $db->fetchRow("SELECT *, public AS privacy FROM userwishlist WHERE item_id = ? AND user_id = ?",  array($item_id, $user_id));
		return $result;
	}
	
	/// get count of recntly added items
	public static function getRecentItemsCount($date)
	{
		$db = Zend_Registry::get("db");
		/*$sql = "SELECT count(id) as total 
				FROM wishlistitmes 
				WHERE added_date > '".$date."'";*/
		$currentdate = date('Y-m-d H:i:s');		
		$sql = "SELECT count(id) as total 
				FROM wishlistitmes 
				WHERE added_date BETWEEN '".$date."' AND '".$currentdate."'";
				
		//echo $sql;
		//exit;
		$result = $db->fetchRow($sql);
		return $result;
	}
	
	public static function isbuyGiftForMe($user_id,$item_id)
	{
		$db = Zend_Registry::get("db");
		//echo "SELECT * FROM giftsexchanged WHERE item_id= '".$item_id."' AND receiver_id='".$user_id."'";
		$results=$db->fetchRow("SELECT * FROM giftsexchanged WHERE item_id= '".$item_id."' AND receiver_id='".$user_id."'");
		return $results;	
	}
	
	/// list all products
	public static function listitem($filterSql)
	{
		$db = Zend_Registry::get("db");
		$sql = "SELECT id, item_price, item_name, added_from, item_count FROM wishlistitmes ".$filterSql;
		//echo $sql;
		$results=$db->fetchAll($sql);
		return $results;	
	}
	
	/// count items's comment and tags
	public static function totalItemComment($item_id)
	{
		$db = Zend_Registry::get("db");
		$sql = "SELECT count(id) As total FROM itemcomments WHERE item_id = ".$item_id;
		//echo $sql;
		$results=$db->fetchOne($sql);
			return $results;	
	}
	
	public static function totalItemTag($item_id)
	{
		$db = Zend_Registry::get("db");
		$sqlTag = "SELECT tag_id FROM taggifts WHERE item_id = '".$item_id."'
				UNION All 
				SELECT tag_id FROM item_tags WHERE item_id = '".$item_id."' ";
		$results = $db->fetchAll($sqlTag);
			
		return $results;	
	}

	
	
	public function sortByPricePopularity($user_id,$sortby,$start,$end){
		$db = Zend_Registry::get("db");			
		$sql="SELECT w.item_name, w.id AS item_id, w.item_type, w.item_price,
		w.image, w.description,	w.item_count, w.vendor_link, w.isbn, w.added_from,
		w.added_date, uw.id AS wid,uw.added_date AS uadded_date,w.barcode_number,uw.items_required_no
		FROM userwishlist uw LEFT JOIN wishlistitmes w ON w.id = uw.item_id
		WHERE uw.user_id='".$user_id."' 
		AND uw.item_id = w.id 	
		order by ".$sortby." 
		LIMIT ".$start.",".$end; 	
	
		$results = $db->fetchAll($sql);
		return $results;
	}
	
	/*public function sortByPricePopularitySdate($userid,$sortby,$start,$end,$serverDate){
		$db = Zend_Registry::get("db");			
		$sql="SELECT w.item_name, w.id AS item_id, w.item_type, w.item_price,
		w.image, w.description,	w.item_count, w.vendor_link, w.isbn, w.added_from,
		w.added_date, uw.id AS wid,uw.added_date AS uadded_date,w.barcode_number
		FROM userwishlist uw LEFT JOIN wishlistitmes w ON w.id = uw.item_id
		WHERE uw.user_id='".$userid."' AND updated_date > '".$serverDate."'
		order by '".$sortby."' Desc
		LIMIT ".$start.",".$end; 		
		$results = $db->fetchAll($sql);
		return $results;
	}*/
	
	public function countItems($serverDate,$user_id){
		$db = Zend_Registry::get("db");
		$sql="SELECT count(id)	AS total  from userwishlist where user_id='".$user_id."' AND updated_date > '".$serverDate."' ";
		$result= $db->fetchRow($sql);
		return $result;

	}
	
	public function countFeeds($serverDate,$user_id){
		$db = Zend_Registry::get("db");
		$sql="SELECT  user_id , friend_id from usersfriend where user_id='".$user_id."' AND status=1 ";
		$friends = $db->fetchAll($sql);
		
		$friend_id='';
		foreach($friends  AS $friend){
			$friend_id .= ' user_id ='. $friend . ' OR'; 
		}
		$s=$friend_id.'1' ;
		$newcon = str_replace("OR1",' ',$s);
		$condiation='('.$newcon.')';
		
		$sql="SELECT count(id) AS total FROM  userwishlist WHERE ".$condiation." AND updated_date > '".$serverDate."' ";
		 
		$result=$db->fetchRow($sql);
		
		return $result;
	}
	
		public function countTotalFriendFeeds($user_id){
		$db = Zend_Registry::get("db");
		$sqlItemU = "SELECT item_id FROM userwishlist WHERE user_id =".$user_id;
		$resitemids = $db->fetchAll($sqlItemU);
		
		for($i=0; $i < count($resitemids); $i++)
		{
			$ids[] = $resitemids[$i]->item_id;
		}
		if(count($ids) > 0)
		{			
			$idsStr = implode(",",$ids);
		}else{			
			$idsStr = '0';
		}

		$sql = "SELECT COUNT( DISTINCT (w.id) ) AS totalCount
				FROM wishlistitmes w
				LEFT JOIN userwishlist uw ON w.id = uw.item_id
				WHERE uw.item_id NOT IN (".$idsStr.") ";
		
	

		$result=$db->fetchRow($sql);
		
		return $result;
	}
	
	public static function getAllByTag($userID,  $category,$start,$end)
	{
		$db = Zend_Registry::get("db");	
		
		if($category != '')
		{
			$categorySql = ' AND w.item_type ='.$category;
			$sqlTag = "SELECT item_id FROM taggifts WHERE tag_id='".$category."' AND user_id='".$userID."' AND item_id != ''
				UNION All 
				SELECT item_id FROM item_tags WHERE tag_id='".$category."' AND user_id='".$userID."' AND item_id != '' ";
			$item_idArray = $db->fetchAll($sqlTag);
			$itemList = array();
			for($i = 0; $i < count($item_idArray); $i++)
			{
				$itemList[] = $item_idArray[$i]->item_id;
			}
			$itemStr = implode(",",$itemList);
			if($itemStr == '')
			{
				$itemStr = 0;
			}
			
		$sql = "
                    SELECT w.item_name, w.id AS item_id, w.item_type, w.image, w.description, w.item_price, w.item_count, w.vendor_link, w.isbn, w.added_from, uw.added_date, uw.id AS wid,uw.added_date AS uadded_date,w.barcode_number, uw.public, uw.sharewith_group, uw.share_individual ,uw.items_required_no
                    FROM userwishlist uw JOIN wishlistitmes w ON w.id = uw.item_id 
                    WHERE uw.user_id = '".$userID."' AND  item_id IN (".$itemStr.") 
                        ORDER BY w.item_count Desc LIMIT
		$start,$end";
		
		}else{
                    $sql = "
                        SELECT w.item_name, w.id AS item_id, w.item_type, w.image, w.description, w.item_price, w.item_count, w.vendor_link, w.isbn, w.added_from, uw.added_date, uw.id AS wid,uw.added_date AS uadded_date,w.barcode_number, uw.public, uw.sharewith_group, uw.share_individual,uw.items_required_no 
                        FROM userwishlist uw JOIN wishlistitmes w ON w.id = uw.item_id 
                        WHERE uw.user_id = '".$userID."' ".$categorySql." 
                            ORDER BY w.item_count Desc LIMIT
		$start,$end";
		}
	
		$result = $db->fetchAll($sql);
		return $result;
	}
	public static function getusertagbyId($user_id, $tagId)
	{
		$db = Zend_Registry::get("db");
		$sql = "SELECT * 
                        FROM category 
                        WHERE created_by = '".$user_id."' AND id='". $tagId ."' AND status = '1'";
			
		$result = $db->fetchAll($sql);
		return $result;
	}
	
	public function getTotalWish($user_id){
		$db = Zend_Registry::get("db");
		$sql="SELECT count(uw.id) AS totalCount from userwishlist uw LEFT JOIN wishlistitmes w ON w.id = uw.item_id where w.id = uw.item_id AND uw.user_id='".$user_id."'  ";
		$result= $db->fetchRow($sql);
		return $result;
	}
	
	public function getTotalWishBytagid($userID, $category)
	{
		$db = Zend_Registry::get("db");	
		if($category != '')
		{
			$categorySql = ' AND w.item_type ='.$category;
			$sqlTag = "SELECT item_id FROM taggifts WHERE tag_id='".$category."' AND user_id='".$userID."' AND item_id != ''
				UNION All 
				SELECT item_id FROM item_tags WHERE tag_id='".$category."' AND user_id='".$userID."' AND item_id != '' ";
			$item_idArray = $db->fetchAll($sqlTag);
			$itemList = array();
			for($i = 0; $i < count($item_idArray); $i++)
			{
				$itemList[] = $item_idArray[$i]->item_id;
			}
			$itemStr = implode(",",$itemList);
			if($itemStr == '')
			{
				$itemStr = 0;
			}
			
		$sql = "SELECT  count(uw.id) AS totalCount
		FROM userwishlist uw JOIN wishlistitmes w ON w.id = uw.item_id WHERE uw.user_id = '".$userID."' AND  item_id IN (".$itemStr.") ORDER BY w.item_count Desc";
		
		}else{
			$sql = "SELECT count(uw.id) AS totalCount
		FROM userwishlist uw JOIN wishlistitmes w ON w.id = uw.item_id WHERE uw.user_id = '".$userID."' ".$categorySql." ORDER BY w.item_count Desc";
		}
		
		$result = $db->fetchRow($sql);
		return $result;
	
	}	
	
	public static function getAllUserFeed($user_id,$sortby,$start,$end)
	{

		$db = Zend_Registry::get("db");	
		///user's item
		$sqlItemU = "SELECT item_id FROM userwishlist WHERE user_id =".$user_id;
		$resitemids = $db->fetchAll($sqlItemU);
		
		for($i=0; $i < count($resitemids); $i++)
		{
			$ids[] = $resitemids[$i]->item_id;
		}
		if(count($ids) > 0)
		{			
			$idsStr = implode(",",$ids);
		}else{			
			$idsStr = '0';
		}

		 $sql = "SELECT DISTINCT(w.id),w.*, w.id AS item_id
				FROM wishlistitmes w
				LEFT JOIN userwishlist uw ON w.id = uw.item_id
				WHERE uw.item_id NOT IN (".$idsStr.") ORDER BY
				".$sortby." LIMIT ".$start.",".$end;
	
		//echo $sql.'<br>';
		
		
		$result = $db->fetchAll($sql);
		return $result;
}


    public static function getAllUsersFeedlist($user_id,$sortby)
	{

		$db = Zend_Registry::get("db");	
		///user's item
		$sqlItemU = "SELECT item_id FROM userwishlist WHERE user_id =".$user_id;
		$resitemids = $db->fetchAll($sqlItemU);
		
		for($i=0; $i < count($resitemids); $i++)
		{
			$ids[] = $resitemids[$i]->item_id;
		}
		if(count($ids) > 0)
		{			
			$idsStr = implode(",",$ids);
		}else{			
			$idsStr = '0';
		}

		 $sql = "SELECT DISTINCT(w.id),w.*, w.id AS item_id
				FROM wishlistitmes w
				LEFT JOIN userwishlist uw ON w.id = uw.item_id
				WHERE uw.item_id NOT IN (".$idsStr.") ORDER BY
				".$sortby." ";
	
		//echo $sql.'<br>';
		
		
		$result = $db->fetchAll($sql);
		return $result;
	}

	/// get users latest 3 items
	public function getLatestItemForEvent($userID)
	{
		$sql = "SELECT w.* FROM userwishlist uw JOIN wishlistitmes w ON w.id = uw.item_id WHERE uw.user_id = '".$userID."' ORDER BY uw.added_date DESC LIMIT 0, 3";
		
		
		$db = Zend_Registry::get("db");		
		$results=$db->fetchAll($sql);
		return $results;
	}
	
	/// latest ite for home
	public static function latestItemHome($itemID)
	{		
		//$sql = "SELECT added_date AS wishdate, user_id FROM userwishlist WHERE item_id = '".$itemID."' ORDER BY id DESC LIMIT 0,1";
		$sql = "SELECT w.added_date AS wishdate, w.user_id FROM userwishlist AS w JOIN users AS u ON  u.user_id = w.user_id			
					WHERE w.item_id = '".$itemID."' AND u.profile_type = 'Public'
					ORDER BY id DESC LIMIT 0,1";
		
		
		$db = Zend_Registry::get("db");	
		$result = $db->fetchRow($sql);
		$today = time();
		
		$idate = $result->wishdate;
		
		$diff_seconds  = $today - $idate;
		
		
		$postedc =  time() - strtotime($result->wishdate);
        
		///
//		if($postedc < 3600) {
//         if(round($postedc/60) > 1)
//          $postedcdisplay = round($postedc/60)." ".PS_Translate::_("minutes ago");
//         else
//          $postedcdisplay = "1 ".PS_Translate::_("minute ago");
//        }
//		else if($postedc < 86000) {
//         if(round($postedc/3600) > 1)
//          $postedcdisplay = round($postedc/3600)." ".PS_Translate::_("hours")." ago ";
//         else
//          $postedcdisplay = "1 ".PS_Translate::_("hour")." ago ";
//        }
//		 else if($postedc < 430000) {
//         if(round($postedc/86000) > 1)
//          $postedcdisplay = round($postedc/86000)." ".PS_Translate::_("days")." ago ";
//         else
//          $postedcdisplay = "1 ".PS_Translate::_("day")." ago ";
//        }
//        else {
         $postedcdisplay = date("r", strtotime($result->wishdate));
//        }
		
		
		$resultArr['time'] = $postedcdisplay;
		$resultArr['user_id'] = $result->user_id;
		
		return $resultArr;
		
		
}

	public static function getfriendgift($sender,$receiver,$activity_id,$activity_type)
	{
		$db = Zend_Registry::get("db");
		$sql = "select wt.* from  wishlistitmes wt inner join activity at ON at.item_id =wt.id  where at.id=".$activity_id."
        and at.sender=".$sender." and  at.receiver=".$receiver."  ";
		$result = $db->fetchRow($sql);
		return $result;
	}
	
	/// display price im money format
	public static function format_price($price)
	{
		// filter and format it 
			if($price >= 1000000000000){
				$newPrice = '$$$';
			}
			else if($price>=1000000000){ 
				//$newPrice =  round(($price/1000000000),1).' B';
				$newPrice = ($price/1000000000);
				$newPrice = substr($newPrice, 0, ((strpos($newPrice, '.')+1)+1)).'B'; 

			}else if($price>=1000000){ 
				$newPrice = round(($price/1000000),1).'M';
			}else if($price>=1000){ 
				$newPrice = round(($price/1000),1).'K';
			}else{
				$newPrice = number_format($price,1);
			}	 
			//return number_format($price);
			//return number_format($newPrice);
			return $newPrice;
	}

    public static function getItemIdByHash($hash){
        //echo $sql;
        $db = Zend_Registry::get("db");
        
        /*
        $result = $db->fetchAll("SELECT DISTINCT(w.id),w.*
                            FROM wishlistitmes w
                            LEFT JOIN userwishlist uw ON w.id = uw.item_id
                            WHERE w.state=1 AND md5(w.id) = ?", $hash);
        */
        $result = $db->fetchAll("SELECT DISTINCT(w.id),w.*
                            FROM wishlistitmes w WHERE md5(w.id) = ?", $hash);
       
        return $result;
    }

    /*
    public static function getUsersTagList($userId, $itemIds){
        $db = Zend_Registry::get("db");
        $sql = "SELECT t.name, count(*) AS count
				FROM wishlistitmes w
				LEFT JOIN userwishlist uw ON w.id = uw.item_id
				LEFT JOIN item_tags AS tg ON tg.item_id = w.id AND tg.user_id = '" . $userId . "'
				LEFT JOIN category AS t ON t.id = tg.tag_id
				WHERE    uw.user_id = '" . $userId . "' AND uw.item_id IN " . $itemIds . "
				GROUP BY t.name
				ORDER BY t.name ASC";

        return $db->fetchAll($sql);
    }
    */

    /* Tags can be made visible to other users as well in the filterbar, but do not allow to delete
     * HSo, redifining this function as below
     */
    public static function getUsersTagList($userId, $itemIds){
        $db = Zend_Registry::get("db");
        $result = $db->fetchAll("SELECT cg.name, cg.*  FROM category AS cg, item_tags AS it WHERE cg.id = it.tag_id AND it.item_id IN " . $itemIds . " GROUP BY cg.name ORDER BY cg.name ASC");
        return $result;
    }

    public static function getItemsCategoryList($userId, $itemIds){
        $db = Zend_Registry::get("db");
        $result = $db->fetchAll("SELECT w.category AS name FROM wishlistitmes AS w WHERE w.id IN " . $itemIds . " GROUP BY w.category ORDER BY w.category ASC");
        return $result;
    }   

    public static function getDollarConversion($from) {
        $db = Zend_Registry::get("db");
        $sql = "SELECT c_value FROM dollar_value d WHERE d.currency LIKE '%".$from."%'";
        $result = $db->fetchRow($sql);
        return $result;
    }

    public static function prepareExtVal($products, $userId){
        $db = Zend_Registry::get("db");
        $logger = Zend_Registry::get("logger");

        /* get the currency value conversion */
        $cur = "INR";
        $inr = wishlistitmes::getDollarConversion($cur);

        for ($i = 0; $i < count($products); $i++) {
            $item = array();
            $productIds[] = $products[$i]->id;
            $totalComments = itemcomments::countComments($products[$i]->id);
            $userwished = wishlistitmes::chkWishedItem($products[$i]->id, $_SESSION['user_id']);
            $countwishes = wishlistitmes::countWished($products[$i]->id);
            $timeAdded = wishlistitmes::latestItemHome($products[$i]->id);
            $addeduser = users::getUserDetails($timeAdded['user_id']);
            $itemwhished = wishlistitmes::chkWishedItem($products[$i]->id, $userId);
            if ($itemwhished) {
                $item['youwhished'] = 'Y';
            } else {
                $item['youwhished'] = 'N';
            }

            $index_e = 0;
            $events = event::getItemEvents($userId, $products[$i]->id);
            for ($j = 0; $j < count($events); $j++) {
                if ($events[$j]->selected) {
                    $index='events' . $index_e;
                    $item[$index] = $events[$j]->title;
                    $index_e++;
                }
            }

            $taglist = category::getItemTags($products[$i]->id, null);
            for ($j = 0; $j < count($taglist); $j++) {
                $index='tags' . $j;
                $item[$index] = $taglist[$j]->name;
            }
            $index='tags' . $j;
            $item[$index] = $products[$i]->category;
 
            $item['privacy_text'] = 'show_filter_home_all';
            $shareOptions = wishlistitmes::getShareOption($userId, $products[$i]->id);
            $showTo = $shareOptions->privacy;
            if ($showTo == '0') {
                $item['privacy_text'] = 'show_filter_home_private';
            }

            if ($products[$i]->gift_state == '1') {
                $item['privacy_text'] = $item['privacy_text'] . ' show_filter_home_reserved';
            }

            $shareOptions = wishlistitmes::getShareOption($_SESSION['user_id'], $products[$i]->id);
            $sharedWithGroups = explode(',',$shareOptions->sharewith_group);
            $createdGroups = groups::groupList($_SESSION['user_id']);
            $item['groups'] = '';
            for ($j = 0; $j < count($createdGroups); $j++) {
                if (in_array($createdGroups[$j]->id, $sharedWithGroups)) {
                    $item['groups'] = $item['groups'] . " " . "showgroupID". $createdGroups[$j]->id;
                }
            }
            if($products[$i]->item_price_currency == "INR") {
                  $item['n_price_filter'] = $products[$i]->item_price / $inr->c_value;
            } else {
                  $item['n_price_filter'] = $products[$i]->item_price;
            }
            $item['n_tags'] = count($taglist) + 1;
            $item['n_events'] = $index_e;
            $item['comment'] = $totalComments;
            $item['wished'] = $userwished;
            $item['countwishes'] = $countwishes;
            $item['added_time'] = $timeAdded['time'];
            $item['first_name'] = $addeduser->first_name;
            $item['last_name'] = $addeduser->last_name;
            $item['user_id'] = $addeduser->user_id;
            $items[] = $item;
        }
        return $items;
    }
	
	public static function itemcuratedinfo($productId, $curatedDescription) {
		$db = Zend_Registry::get("db");
		$data['curated_description'] = $curatedDescription;
		if(isset($productId) && $productId != '') {
			// update curated_description;
			try {
				$db->update("wishlistitmes", $data, "id=" . $productId);
				return true;
			} catch (Exception $e) {
				throw $e;
			}
		}
		return false;
	}
	
	public static function addItemTag($item_id, $tag_name, $user_id) {
		$db = Zend_Registry::get("db");
		try{
			$db->beginTransaction();
			
	
			$dataT['name'] = $tag_name;
			$dataT['created_by'] = $user_id;
			$dataT['status'] = 1;
	
			$totalTags = category::countItemTags($item_id, $user_id);
			if(count($totalTags) < 5) {
				// check it it is already added
				$checkTagID = category::checkTags($tag_name, $user_id);
				if($checkTagID != '') {
					// already added
					$tag_id = $checkTagID;
				}else{
					// add
					$db->insert('category',$dataT);
					$tag_id =  $db->lastInsertId();
				}
				//add this tag to DB
				$data['item_id'] = $item_id;
				$data['tag_id'] = $tag_id; // tag_id is the category id
				$data['user_id'] = $user_id;
				$data['tag_date'] = date('Y-m-d H:i:s');
	
				//check for already added
				$checkID = category::checkItemTags($item_id, $tag_id, $user_id);
				if(!empty($checkID))
				{
					// user already tagged this item in this tag
					throw new Exception('user already tagged item with this tag');
				}else{
					// tag item
					$db->insert('item_tags', $data);
					$db->commit();
					$taglist = array();
					$taglist['id'] = $tag_id;
					$taglist['name'] = $tag_name;
					return $taglist;
				}
			}
			else{
				throw new Exception('This Item has maximum no. (5) of tags, please remove tag to add new Tag');
			}
		}catch(Exception $e)
		{
			$db->rollBack();
		}
	}

/**
 
 * @param array        $array      The array to sort.
 * @param string|array $key        The index(es) to sort the array on.
 * @param int          $sort_flags The optional parameter to modify the sorting 
 *                                 behavior. This parameter does not work when 
 *                                 supplying an array in the $key parameter. 
 * 
 * @return array The sorted array.
 */
function msort($array, $key, $sort_flags = SORT_REGULAR) {
    if (is_array($array) && count($array) > 0) {
        if (!empty($key)) {
            $mapping = array();
            foreach ($array as $k => $v) {
                $sort_key = '';
                if (!is_array($key)) {
                    $sort_key = $v[$key];
                } else {
                    // @TODO This should be fixed, now it will be sorted as string
                    foreach ($key as $key_key) {
                        $sort_key .= $v[$key_key];
                    }
                    $sort_flags = SORT_STRING;
                }
                $mapping[$k] = $sort_key;
            }
            asort($mapping, $sort_flags);
            $sorted = array();
            foreach ($mapping as $k => $v) {
                $sorted[] = $array[$k];
            }
            return $sorted;
        }
    }
    return $array;
}//

}

