<?php
/*------------------------------------------------------------------ 
 * Copyright (c) 2013-2014 by Giftvise, Inc.
 * All rights reserved. 
 *------------------------------------------------------------------
 */
?>

<?php
class itemcomments extends Zend_Db_Table_Abstract
{
	public static function showComments($product_id)
	{
		$sql = "SELECT i.* , u.profile_pic,u.first_name, u.last_name
				FROM itemcomments i
				LEFT JOIN users u ON u.user_id = i.user_id
				WHERE item_id = '".$product_id."' ORDER BY i.id DESC";
		
		$db = Zend_Registry::get("db");	
		$result = $db->fetchAll($sql);
		return $result;
	}
	
	public static function countComments($productID)
	{
		$sql = "SELECT COUNT(user_id) AS total
				FROM itemcomments 
				WHERE item_id = '".$productID."'";
		
		//echo $sql;
		//exit;
		$db = Zend_Registry::get("db");	
		$result = $db->fetchOne($sql);
		return $result;
	}
	
	
}///end class
