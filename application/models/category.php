<?php
/*------------------------------------------------------------------ 
 * Copyright (c) 2013-2014 by Giftvise, Inc.
 * All rights reserved. 
 *------------------------------------------------------------------
 */
?>

<?php
//// categories are now converted in tags.

class category extends Zend_Db_Table_Abstract
{
	public static function categoryList()
	{
		$sql = "SELECT * 
				FROM category 
				WHERE status = '1' ORDER BY name";
		
		$db = Zend_Registry::get("db");	
		$result = $db->fetchAll($sql);
		return $result;
	}
	
	public static function categoryName($categoryId)
	{
		$sql = "SELECT name 
				FROM category 
				WHERE id = '".$categoryId."'";
		
		$db = Zend_Registry::get("db");	
		$result = $db->fetchOne($sql);
		return $result;
	}
	
	///// get user's created taggs
	public static function usersTags($user_id)
	{	
		$db = Zend_Registry::get("db");	
		$result = $db->fetchAll("SELECT * FROM category WHERE status = '1' AND created_by = ? ORDER BY name", $user_id);
		return $result;
	}
	public static function searchbytag($tagName){
		$db = Zend_Registry::get("db");	
        
		$sql = "SELECT * FROM category WHERE name LIKE '%".$tagName."%' ORDER BY name";
		$result = $db->fetchAll($sql);
		return $result;
	}
	
	public static function usersTagsName($user_id, $tagName)
	{	
		$db = Zend_Registry::get("db");	
		$sql = "SELECT * FROM category WHERE status = '1' AND created_by = '".$user_id."' AND name LIKE ".$tagName." ORDER BY name";
		$result = $db->fetchAll($sql);
		return $result;
	}
	
	/// check if item is allready tagged in a tag
	public static function checkItemTags($item_id, $tag_id, $user_id)
	{	
		$db = Zend_Registry::get("db");	
		$result = $db->fetchOne("SELECT id FROM item_tags WHERE item_id = ? AND tag_id = ? AND 	user_id = ?", array($item_id, $tag_id, $user_id));
		return $result;
	}
	
	//check for 5 tags
	public static function countItemTags($item_id, $user_id)
	{	
		$db = Zend_Registry::get("db");	
		$result = $db->fetchAll("SELECT id FROM item_tags WHERE item_id = ? AND user_id = ?", array($item_id, $user_id));
		return $result;
	}
	
	//get item's tags
	public static function getItemTags($item_id, $user_id)
	{	
		$db = Zend_Registry::get("db");	
                if ($user_id == null) { 
	           $result = $db->fetchAll("SELECT t.* FROM item_tags AS tg INNER JOIN category AS t ON t.id = tg.tag_id  WHERE tg.item_id = ? GROUP BY t.name ORDER BY t.name", array($item_id));
                } else {
                   $result = $db->fetchAll("SELECT t.* FROM item_tags AS tg INNER JOIN category AS t ON t.id = tg.tag_id  WHERE tg.item_id = ? AND tg.user_id = ? GROUP BY t.name ORDER BY t.name", array($item_id, $user_id)); 
                }
		return $result;
	}

         public static function getTopTags() {
               $sql = "SELECT  DISTINCT t.name FROM category AS t LEFT JOIN item_tags AS tg ON t.id = tg.tag_id GROUP BY tg.item_id ORDER BY tg.tag_date DESC";
               $db = Zend_Registry::get("db");
               $result = $db->fetchAll($sql);
               return $result;
         }	
	// check if user is already created this tag
	public static function checkTags($tagName, $user_id)
	{	
		$db = Zend_Registry::get("db");	
		$result = $db->fetchOne("SELECT id FROM category WHERE name = ? AND created_by = ?", array($tagName, $user_id));
		return $result;
	}
	
	public static function checkCategoryName($ctg_nm,$user_id)
	{
		$db = Zend_Registry::get("db");	
		$ctg= $db->quote($ctg_nm);
		$sql = "SELECT id 
			FROM category 
			WHERE name = ".$ctg." AND created_by='".$user_id."'";	
		$result = $db->fetchOne($sql);
		return $result;
	}
	
	/* public static function getUserTags($user_id)
	{
		$sql = "SELECT category.id AS cat_id,name,count(item_type) AS total FROM category 
				JOIN wishlistitmes ON item_type=category.id
				WHERE created_by='".$user_id."' GROUP BY item_type ORDER BY total DESC";
		
		$db = Zend_Registry::get("db");	
		$result = $db->fetchAll($sql);
		return $result;
	} */
	
	public static function getUserTags($tag_id,$user_id,$event_id)
	{
		/* $sql = "SELECT item_id FROM taggifts WHERE tag_id='".$tag_id."' AND user_id='".$user_id."' AND item_id != '' AND event_id='".$event_id."'
				UNION All 
				SELECT item_id FROm item_tags WHERE tag_id='".$tag_id."' AND user_id='".$user_id."' AND item_id != ''"; */
				
		$sql = "SELECT item_id FROM taggifts WHERE tag_id='".$tag_id."' AND user_id='".$user_id."' AND item_id != ''
				UNION All 
				SELECT item_id FROm item_tags WHERE tag_id='".$tag_id."' AND user_id='".$user_id."' AND item_id != ''";
		//echo $sql;
		$db = Zend_Registry::get("db");	
		$result = $db->fetchAll($sql);
		return $result;
	}
    public static function getTagDetail($tag_id)
	{
		$sql = "SELECT item_id FROM taggifts WHERE tag_id='".$tag_id."'  AND item_id != ''
				UNION All 
				SELECT item_id FROm item_tags WHERE tag_id='".$tag_id."'  AND item_id != ''";	
		$db = Zend_Registry::get("db");	
		$result = $db->fetchAll($sql);
		return $result;
	}
	// show tag by popularity	
	public static function showTag($user_id)
	{			
		$db = Zend_Registry::get("db");	
		$sql = "SELECT tag_id FROM taggifts WHERE user_id='".$user_id."' AND item_id != ''
				UNION All 
				SELECT tag_id FROM item_tags WHERE user_id='".$user_id."' AND item_id != ''";
		//echo 'tag:'.$sql;
		$result = $db->fetchAll($sql);
		for($i=0; $i < count($result); $i++)
		{
			$tagidArr[] = $result[$i]->tag_id;
		}
		
		$tagStr = implode(",",$tagidArr);
		if($tagStr != '')
		{
		$sql1 = 'SELECT c.name, c.id, c.created_by FROM category c  WHERE c.id IN ('.$tagStr.')';
		//$sql = "SELECT c.name, c.id, c.created_by, count( t.item_id ) AS total FROM category AS c LEFT JOIN item_tags AS t ON c.id = t.tag_id WHERE t.user_id = '".$user_id."' GROUP BY t.tag_id ORDER BY total DESC";
		//echo "<br>lis::".$sql1;		
		$result1 = $db->fetchAll($sql1);
		}
		
		return $result1;
	}
	
}///end class
