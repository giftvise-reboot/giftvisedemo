<?php
/*------------------------------------------------------------------ 
 * Copyright (c) 2013-2014 by Giftvise, Inc.
 * All rights reserved. 
 *------------------------------------------------------------------
 */
?>

<?php
class Budget extends Zend_Db_Table_Abstract
{
	public static function showBudget($user_id, $year)
	{
		$sql = "SELECT * 
				FROM budget 
				WHERE user_id = '".$user_id."' AND year = '".$year."'";
		
		$db = Zend_Registry::get("db");	
		$result = $db->fetchRow($sql);
		
		//$result = $db->fetchAll($sql);
		return $result;
	}
	
	// Get spending history from giftsexchanged table
	
	public static function totalExpense($user_id)
	{
		//echo "SELECT sum(amount) AS total FROM giftsexchanged WHERE user_id = '".$user_id."'";
		//exit;
		$db = Zend_Registry::get("db");	
		$result = $db->fetchRow("SELECT sum(amount) AS total FROM giftsexchanged WHERE user_id = '".$user_id."'");		
		return $result;
	}
	
	public static function totalExpenseYearly($user_id, $year)
	{
		$db = Zend_Registry::get("db");	
		$result = $db->fetchRow("SELECT sum(amount) AS total FROM giftsexchanged WHERE user_id = '".$user_id."' AND YEAR(purchased_date) = '".$year."'");		
		return $result;
	}
	
}///end class
