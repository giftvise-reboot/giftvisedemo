<?php
/*------------------------------------------------------------------ 
 * Copyright (c) 2013-2014 by Giftvise, Inc.
 * All rights reserved. 
 *------------------------------------------------------------------
 */

class facebookquery 
{
    public static function getQuery($token, $query, $fields) {
        try {
            $json_link = "https://graph.facebook.com/".$query."?fields=".$fields."&access_token=".$token;

            $json = file_get_contents($json_link);
            return json_decode($json);
        } catch (Exception $e) {
            return $e->getMessage();
        }

    }
}