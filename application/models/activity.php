<?php
/*------------------------------------------------------------------ 
 * Copyright (c) 2013-2016 by Giftvise, Inc.
 * All rights reserved. 
 *------------------------------------------------------------------
 */
?>

<?php
class activity
{
    /**
     * Delete/update activity and return string or throw exception
     * 
     * @param int $activityId
     * @param int $userId
     * @param $db
     * 
     * @return string
     * @throws Exception
     */
    public static function delete($activityId, $userId, $db) {
        try{
            if($userId != '' && $activityId != '')
            {
                // check any user is already deleted this activity
                // if one user is deleted the remove this activity from DB,
                // else only set deleted_user value
                $chkid = users::chkActivityRemoved($activityId);

                if($chkid != '' && $chkid != 0)
                {
                    if($chkid == $userId)
                    {
                        /// do nothing
                    }else{
                        /// delete this activity from DB
                        $res = $db->delete('activity', array("id = ?" => $activityId));
                        return 'Deleted ';
                    }
                }else{
                    //update DB
                    $datau['deleted_user'] = $userId;
                    $res = $db->update('activity', $datau, array("id = ?" => $activityId));
                    return 'Updated ';
                }
            }
        }catch(Exception $e)
        {
            throw $e;
        }
    } 
}
