<?php
/*------------------------------------------------------------------ 
 * Copyright (c) 2013-2014 by Giftvise, Inc.
 * All rights reserved. 
 *------------------------------------------------------------------
 */
?>

<?php
class fileupload
{
    public function imageUploadToTemp($imageClass, $name, $newHeight, $newWidth, $thumbWidth)
    {
        require 'SimpleImage.php';
        if(!empty($_FILES[$imageClass]["name"]))
        {
            if((strtolower($_FILES[$imageClass]["type"]) == "image/gif") || (strtolower($_FILES[$imageClass]["type"]) == "image/jpeg") || (strtolower($_FILES[$imageClass]["type"]) == "image/pjpeg") || (strtolower($_FILES[$imageClass]["type"]) == "image/png"))
            {
                $imgExtsn = explode(".",$_FILES[$imageClass]["name"]);
                $countExtsn = count($imgExtsn)-1;
                //$pname = addslashes(str_replace(' ', '_', $params["name"]));
                $pname = addslashes(str_replace(' ', '_', substr($name,0,30)));
                $file = $imgExtsn[$countExtsn-1].'_'.$pname.rand(100,100000).".".$imgExtsn[$countExtsn];
                $newfilename = $_SERVER["DOCUMENT_ROOT"]."/tempimg/".$file;
                $thumb_path = $_SERVER["DOCUMENT_ROOT"]."/tempimg/thumb/".$file;

                $this->fileResize($_FILES[$imageClass]["tmp_name"], $newfilename, $thumb_path, $newHeight, $newWidth, $thumbWidth);


                return array("success" => true, 'file' => $file);
            }else{
                return array("success" => false, 'msg' => "Please upload image in JPEG/PNG/GIF/PGPEG format.");
            }
        } else {
            return array("success" => false, 'msg' => "Please upload image");
        }
    }

    public function fileResize($oldImage, $path, $thumbPath, $newHeight, $newWidth, $thumbWidth)
    {
        /// resize image
        $image = new SimpleImage();

        $image->load($oldImage);
        $image->save($path);
        $size = getimagesize($path);

        $width = $size[0];
        $height = $size[1];

        if($height > $newHeight)
        {
            $image->resizeToHeight($newHeight);
            $image->save($path);
        }
        if($width > $newWidth)
        {
            $image->resizeToWidth($newWidth);
            $image->save($path);
        }
        //$image->load($newfilename);
        $image->resizeToWidth($thumbWidth);
        $image->save($thumbPath);
    }

    public function moveImageFromTemp($folder, $file)
    {
        if ((!empty($file)) && $_SERVER["DOCUMENT_ROOT"]."/tempimg/".$file && $_SERVER["DOCUMENT_ROOT"]."/tempimg/thumb/".$file){
            return (rename($_SERVER["DOCUMENT_ROOT"]."/tempimg/".$file, $_SERVER["DOCUMENT_ROOT"]."/" . $folder . "/".$file) && rename($_SERVER["DOCUMENT_ROOT"]."/tempimg/thumb/".$file, $_SERVER["DOCUMENT_ROOT"]."/" . $folder . "/thumb/".$file));
        }
        return false;

    }

    /**
     * Handle files by moving the files from temp directory. 
     * If not succeed, recreate the image to the new directory
     *
     * @param string $file
     * @param string $folder - The directory where the files will be moved
     * @param array $options - Image options like url, name
     * 
     * @return array
     */
    public function handleFiles($file, $folder, $options) {
        $msg = '';
        if ($this->moveImageFromTemp($folder, $file)) {
            $uploadedImage = $file;
        } else {
            if($options['url']  != '') {
                $date = date("Y.m.d");
                $time = time();
                $image_name = $options['generatedName'] . $date . $time;
                $target_path= $_SERVER["DOCUMENT_ROOT"] . "/images/" . $folder . "/" . $image_name;
                $thumb_path = $_SERVER["DOCUMENT_ROOT"] . "/images/"  . $folder . "/thumb/" . $image_name;
                chmod($_SERVER["DOCUMENT_ROOT"] . "/images/" . $folder, '755');
                $ret = file_put_contents($target_path, file_get_contents($options['url']));

                if ($ret != NULL && $ret != FALSE) {
                    copy($target_path, $thumb_path);
                    $image = new SimpleImage();
                    $image->load($target_path);
                    // resize big image
                    $size = getimagesize($target_path);
                    $width = $size[0];
                    $height = $size[1];
                    if($width > 620)
                    {
                        $image->resizeToWidth(620);
                        $image->save($target_path);
                    } else {
                        if($width < 197)
                        {
                            $image->resizeToWidth(197);
                            $image->save($target_path);
                        } else {
                            $image->save($target_path);
                        }
                    }

                    $image->resizeToWidth(197);
                    $image->save($thumb_path);
                    $uploadedImage = $image_name;
                } else {
                    $uploadedImage = 'no-image-available';
                    $msg = "Failed to read image from the specified URL";
                }
            } else {
                //$msg = "Failed to upload file please try again";
                $msg = '';
                $uploadedImage = 'no-image-available';
            }
        }
        return array('image' => $uploadedImage);
    }
}
