<?php


class session implements Zend_Session_SaveHandler_Interface
{	
	protected $lifetime;
	protected static $forceWriteToDB = false;
	
	public function __construct($lifetime = 86400)
	{
		$this->lifetime = $lifetime;
	}

	public function open($savePath, $sessionName)
    {
		return true;
    }

    public function close()
    {
		return true;
    }
    
    public function forceWriteToDB()
    {
		self::$forceWriteToDB = true;
    }
	
    public function read($sessionID)
    {
    	$db = Zend_Registry::get("db");
    	//$mc = Zend_Registry::get("mc");
    	
    	//if(($result = $mc->get(__CLASS__.$sessionID)) === false)
    	//{
	      	$result = $db->fetchOne("
	        	SELECT	`data`
	        	FROM	session
	        	WHERE	sessionID = ?",
	        	$sessionID
	        );
		//}
		
		/*
		$fp = fopen("/var/www/sbwell.net/log/dev.txt", "a");
        fwrite($fp, "Read: ".$sessionID." ".$mc->getResultMessage()."\n");
        fclose($fp);
        */
		
        
        return !empty($result) ? json_decode($result, true) : array();
    }
	
    public function write($sessionID, $data)
    {
    	$db = Zend_Registry::get("db");
		$config = Zend_Registry::get("config");
    	//$mc = Zend_Registry::get("mc");
    	
    	//if(!$mc->replace(__CLASS__.$sessionID, $data, $this->lifetime) or self::$forceWriteToDB or (time() % 10 == 0))
    	//{
	        $db->query("
	        	INSERT INTO	session
	        	VALUES(?, NOW(), ?, ?)
	        	ON DUPLICATE KEY UPDATE modified = VALUES(modified), data = VALUES(data)",
	        	array($sessionID, $config->session->app->lifetime, json_encode($data))
	        );
			
	        
	        //if($data){
		      //  $mc->set(__CLASS__.$sessionID, $data, $this->lifetime);
			//}
		//}
        
        return true;
    }
	
    public function destroy($sessionID)
    {
    	$db = Zend_Registry::get("db");
    	//$mc = Zend_Registry::get("mc");
    	
    	//$mc->delete(__CLASS__.$sessionID);
    	
		$db->query("
			DELETE FROM session
			WHERE 		sessionID = ?",
			$sessionID
		);
		
		return true;
    }

    public function gc($maxlifetime)
    {
    	$db = Zend_Registry::get("db");
    	
        $db->query("
			DELETE FROM session
			WHERE 		DATE_ADD(modified, INTERVAL lifetime SECOND) < NOW()
		");
		
		return true;
    }
} 