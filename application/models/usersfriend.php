<?php
/*------------------------------------------------------------------ 
 * Copyright (c) 2013-2014 by Giftvise, Inc.
 * All rights reserved. 
 *------------------------------------------------------------------
 */
?>

<?php
class usersfriend extends Zend_Db_Table_Abstract
{			
	public static function checkExistFriends($userid, $friend_id)
	{
		$sql = "SELECT * 
				FROM usersfriend 
				WHERE (user_id = '".$userid."' AND friend_id = '".$friend_id."')";
		/*$sql = "SELECT * 
				FROM usersfriend 
				WHERE (user_id = '".$userid."' AND friend_id = '".$friend_id."') OR (user_id = '".$friend_id."' AND friend_id = '".$userid."')";
				*/
		
		$db = Zend_Registry::get("db");	
		$result = $db->fetchOne($sql);
		return $result;
	}
	public static function checkExistfollower($userid, $friend_id)
	{
		$sql = "SELECT * 
				FROM usersfriend 
				WHERE (user_id = '".$userid."' AND friend_id = '".$friend_id."')";
		/*$sql = "SELECT * 
				FROM usersfriend 
				WHERE (user_id = '".$userid."' AND friend_id = '".$friend_id."') OR (user_id = '".$friend_id."' AND friend_id = '".$userid."')";
				*/
		
		$db = Zend_Registry::get("db");	
		$result = $db->fetchRow($sql);
		return $result;
	}
	public static function checkExistFriendStatus($userid, $friend_id)
	{
		$sql = "SELECT * 
				FROM usersfriend 
				WHERE (user_id = '".$userid."' AND friend_id = '".$friend_id."')";
		/*$sql = "SELECT * 
				FROM usersfriend 
				WHERE (user_id = '".$userid."' AND friend_id = '".$friend_id."') OR (user_id = '".$friend_id."' AND friend_id = '".$userid."')";
				*/
		
		$db = Zend_Registry::get("db");	
		$result = $db->fetchRow($sql);
		return $result;
	}
	
	public static function checkFollowing($userid, $friend_id)
	{
		$sql = "SELECT * 
				FROM usersfriend 
				WHERE (user_id = '".$friend_id."' AND friend_id = '".$userid."')";
	
		$db = Zend_Registry::get("db");	
		$result = $db->fetchRow($sql);
		return $result;
	}
	public static function checkExistFriendsProfile($userid, $friend_id)
	{
		$sql = "SELECT status 
				FROM usersfriend 
				WHERE (user_id = '".$userid."' AND friend_id = '".$friend_id."')";
		/*$sql = "SELECT * 
				FROM usersfriend 
				WHERE (user_id = '".$userid."' AND friend_id = '".$friend_id."') OR (user_id = '".$friend_id."' AND friend_id = '".$userid."')";
				*/
		
		$db = Zend_Registry::get("db");	
		$result = $db->fetchOne($sql);
		return $result;
	}
	
	
	public static function getRequestStatus($userid, $friend_id)
	{
		$sql = "SELECT status 
				FROM usersfriend 
				WHERE user_id = '".$userid."' AND friend_id = '".$friend_id."'";
				
		$db = Zend_Registry::get("db");	
		$result = $db->fetchOne($sql);
		return $result;
	}
	
	public static function checkRquest($email)
	{
		$sql = "SELECT user_id 
				FROM invitedRequest 
				WHERE email = '".$email."'";
		
		$db = Zend_Registry::get("db");	
		$result = $db->fetchRow($sql);
		return $result;
	}
	
	public static function checkSentRquest($user_id, $email)
	{
		$sql = "SELECT * 
				FROM invitedRequest 
				WHERE email = '".$email."' AND user_id = '".$user_id."'";
		
		$db = Zend_Registry::get("db");	
		$result = $db->fetchOne($sql);
		return $result;
	}
	
	///
	public static function getFollowList($user_id)
	{
		$db = Zend_Registry::get("db");
		
		$sql = "SELECT 		*
			FROM		usersfriend
			WHERE		user_id 		= '".$user_id."' AND status != '2'";
	/*	$sql = "SELECT 		*
			FROM		usersfriend
			WHERE		user_id 		= '".$user_id."' AND status = '1'";*/
		//echo $sql;
		$result = $db->fetchAll($sql);		
		return $result;
	}
	
	public static function searchFollowList($user_id, $key)
	{
		$db = Zend_Registry::get("db");
		
		/*$sql = "SELECT 		uf.*
			FROM		usersfriend uf
			LEFT JOIN users as u ON uf.friend_id = u.user_id
			WHERE		uf.user_id 		= '".$user_id."' AND uf.status != '2' AND (u.first_name LIKE '".$key."%' OR u.last_name LIKE '".$key."%')";*/
		$sql = "SELECT 		uf.*
			FROM		usersfriend uf
			LEFT JOIN users as u ON uf.friend_id = u.user_id
			WHERE		uf.user_id 		= '".$user_id."' AND uf.status = '1' AND (u.first_name LIKE '".$key."%' OR u.last_name LIKE '".$key."%')";
			
		//echo $sql;
		$result = $db->fetchAll($sql);		
		return $result;
	}
	
	public static function getFollowerList($user_id)
	{
		$db = Zend_Registry::get("db");
		
		$sql = "SELECT 		*
			FROM		usersfriend
			WHERE		friend_id  		= '".$user_id."' ";
		//echo $sql.'<br>';
		$result = $db->fetchAll($sql);		
		return $result;
	}
	
	public static function searchFollowerList($user_id, $key)
	{
		$db = Zend_Registry::get("db");
		
		$sql = "SELECT 		uf.*
			FROM		usersfriend AS uf
			LEFT JOIN users as u ON uf.user_id = u.user_id
			WHERE		uf.friend_id  		= '".$user_id."' AND uf.status = '1' AND (u.first_name LIKE '%".$key."%' OR u.last_name LIKE '%".$key."%')";
		//echo $sql.'<br>';
		$result = $db->fetchAll($sql);		
		return $result;
	}
	
	public static function deleteFriend($user_id, $friend_id)
	{
		$db = Zend_Registry::get("db");		
		$result = $db->delete('usersfriend', array("user_id = ?" => $user_id, "friend_id = ?" => $friend_id));
		$db->delete('friendsgroup', 'user_id = '.$friend_id);
		return $result;
	}
	
	///
	public static function countFollowList($user_id)
	{
		$db = Zend_Registry::get("db");
		
		$sql = "SELECT 		COUNT(id) AS total
			FROM		usersfriend
			WHERE		user_id 		= '".$user_id."' AND status = '1'";
		$result = $db->fetchOne($sql);
		return $result;
	}
	
	public static function countFollowerList($user_id)
	{
		$db = Zend_Registry::get("db");		
		/*$sql = "SELECT 		COUNT(id) AS total
			FROM		usersfriend
			WHERE		friend_id  		= '".$user_id."' AND status != '2'";*/
		$sql = "SELECT 		COUNT(id) AS total
			FROM		usersfriend
			WHERE		friend_id  		= '".$user_id."' AND status = '1'";
		//echo $sql.'<br>';
		$result = $db->fetchOne($sql);		
		return $result;
	}
	
	public static function deleteMultiFriends($user_id, $friendidArray)
	{
		$db = Zend_Registry::get("db");	
		
		for($i=0; $i<count($friendidArray); $i++)
		{	
			$friend_id = $friendidArray[$i];
			
			$result = $db->delete('usersfriend', array("user_id = ?" => $user_id, "friend_id = ?" => $friend_id));
			$db->delete('friendsgroup', 'user_id = '.$friend_id);
		}
		return $result;
	}
	
	public static function searhUser($key, $user_id, $lname)
	{
		$db = Zend_Registry::get("db");		
		/*$sql = "SELECT u.user_id, CONCAT(u.first_name,' ', u.last_name) AS username, u.profile_pic
			FROM		users AS u
			WHERE		(u.first_name LIKE ".$key." OR u.last_name LIKE ".$key.") AND u.user_id  != '".$user_id."'";*/
		
		
		if($lname != '' && $key != '')
		{
			$sql = "SELECT u.user_id, CONCAT(u.first_name,' ', u.last_name) AS username, u.profile_pic
					FROM		users AS u
					WHERE		(u.first_name LIKE ".$key." AND u.last_name LIKE ".$lname.") AND 	u.user_id  != '".$user_id."'";
		}else{
			$sql = "SELECT u.user_id, CONCAT(u.first_name,' ', u.last_name) AS username, u.profile_pic
					FROM		users AS u
					WHERE		(u.first_name LIKE ".$key." OR u.last_name LIKE ".$key.") AND 	u.user_id  != '".$user_id."'";
			}
			
		
		//echo $sql.'<br>';
		//exit;
		$result = $db->fetchAll($sql);		
		return $result;
	}
	
	public static function checkExistFriendslist($userid, $friend_id)
	{
		$sql = "SELECT * 
				FROM usersfriend 
				WHERE (user_id = '".$userid."' AND friend_id = '".$friend_id."') OR (user_id = '".$friend_id."' AND friend_id = '".$userid."')";				
				
		$db = Zend_Registry::get("db");	
		$result = $db->fetchOne($sql);
		return $result;
	}
	
	public static function checkNotification($userid)
	{
		$sql = "SELECT count(id) AS notify 
				FROM usersfriend 
				WHERE notify  = '1' AND friend_id = '".$userid."'";				
				
		$db = Zend_Registry::get("db");	
		$result = $db->fetchOne($sql);
		return $result;
	}
	
	public static function latestNotification($userid)
	{
		$sql = "SELECT user_id, status, id
				FROM usersfriend 
				WHERE friend_id = '".$userid."' ORDER BY id DESC LIMIT 0,5";				
				
		$db = Zend_Registry::get("db");	
		$result = $db->fetchAll($sql);
		return $result;
	}
        
        public static function searchFriendsByNameMap($user_id, $name) {
            $db = Zend_Registry::get("db");
            $logger = Zend_Registry::get("logger");
			
			$sql = "SELECT user_id, friend_id, status, notify" .
			       " FROM usersfriend " .
			       " WHERE user_id = " . $user_id ;
			$logger->info($sql);
            $result = $db->fetchAll($sql);
			$logger->info("Result size: " . count($result));
            $map = array();
            foreach ($result as $row) {
                
                if ($row->user_id == $user_id) {
                    $friend_type = 'following';
                    $is_follower = usersfriend::checkExistFriends($row->friend_id, $user_id);
					$logger->info("Is Follower: " . $is_follower);
                    //$is_following = usersfriend::checkExistFriends($user_id, $row->friend_id);
                } else {
                    $friend_type = 'follower';
                    //$is_following = usersfriend::checkExistFriends($userid, $row->friend_id);
                }
                
                if ($is_follower != '') {
                    $is_your_follower = 'Y';
                    $request_status = usersfriend::getRequestStatus($row->friend_id, $user_id);
					$logger->info("Request Status: " . $request_status);
                } else {
                    $is_your_follower = 'N';
                    $request_status = '';
                }
                
                /*
                if ($is_following != '') {
                    $are_you_following = 'Y';
                } else {
                    $are_you_following = 'N';
                }
                */
                
                $list = array(
                    'friend_id' => $row->friend_id,
		    'user_id' => $row->user_id,
                    'status' => $row->status,
                    'notify' => $row->notify,
                    //'first_name' =>$row->first_name,
                    //'last_name' => $row->last_name,
                    //'profile_pic' => $row->profile_pic,
                    'type' => $friend_type,
                    'request_status' => $request_status,
                    'is_your_follower' => $is_your_follower,
                    //'are_you_following' => $are_you_following
                );
                $map[$row->friend_id] = $list;
		unset($list);
            }
            return $map;
        }
	
	public static function searchFriend($key, $user_id)
	{
		
		$db = Zend_Registry::get("db");
		
		$sql = "SELECT friend_id 
				FROM usersfriend 
				WHERE (user_id = '".$user_id."' AND status = '1')";				
		$sql1 = "SELECT user_id 
				FROM usersfriend 
				WHERE (friend_id = '".$user_id."' AND status = '1')";	
		//echo $sql.'<br>';
		$db = Zend_Registry::get("db");	
		$follow = $db->fetchAll($sql);
		$follower = $db->fetchAll($sql1);
		
		$friendlist = array();
		$friendlistStr = '';
		
		for($i=0; $i < count($follow); $i++)
		{
			
			$friendlist[] = $follow[$i]->friend_id;
		}
		for($i=0; $i < count($follower); $i++)
		{
			
			$friendlist[] = $follower[$i]->user_id;
		}
		
		if(count($friendlist) > 0)
		{
			$friendlistStr = implode(",",$friendlist);
		}else{
			$friendlistStr = '0';
		}
		
		$sql = "SELECT u.user_id, CONCAT(u.first_name,' ', u.last_name) AS username, u.profile_pic
			FROM		users AS u
			WHERE		(u.first_name LIKE '%".$key."%' OR u.last_name LIKE '%".$key."%') AND 	u.user_id  IN ( ".$friendlistStr.")";			
				
		
		
		$result = $db->fetchAll($sql);
		return $result;
	}
	
	public static function frienDeatil($friendID)
	{	
		$db = Zend_Registry::get("db");
		$result = $db->fetchRow("
			SELECT 		*
			FROM		users
			WHERE		md5(user_id)	= ?",
			$friendID
		);
		return $result;
	}
	
	public function getFriendIDs($user_id)
	{
		$sql = "SELECT friend_id 
				FROM usersfriend 
				WHERE (user_id = '".$user_id."' AND status = '1')";				
		$sql1 = "SELECT user_id 
				FROM usersfriend 
				WHERE (friend_id = '".$user_id."' AND status = '1')";	
		
		$db = Zend_Registry::get("db");	
		$follow = $db->fetchAll($sql);
		$follower = $db->fetchAll($sql1);
		
		$friendlist = array();
		
		for($i=0; $i < count($follow); $i++)
		{
			
			$friendlist[] = $follow[$i]->friend_id;
		}
		for($i=0; $i < count($follower); $i++)
		{
			
			$friendlist[] = $follower[$i]->user_id;
		}
				
		if(count($friendlist) > 0)
		{		
			$friendlistStr = implode(",",$friendlist);
		}else{
			$friendlistStr = '0';
		}
		
		return $friendlistStr;
	}
	
	/// get All friend list (following and followers)
	public function getAllFriends($user_id)
	{
        $db = Zend_Registry::get("db");
        $sql = "SELECT DISTINCT(u.id),u.*
				FROM usersfriend AS u
				WHERE (status != '3') AND (user_id = '".$user_id."' OR friend_id = '".$user_id."')";

        $result = $db->fetchAll($sql);
        return $result;
	}

       /* get user details by wishlist link id */
       public static function frienDeatilByWlink($wlink)
       {
               $db = Zend_Registry::get("db");
               $result = $db->fetchRow("
                       SELECT          *
                       FROM            users
                       WHERE           wishlinkid    = ?",
                       $wlink
               );
               return $result;
       }

	/// search friends either following or followers
	public static function searchFriends($user_id, $fname1, $lname1)
	{
		$db = Zend_Registry::get("db");
		/*$sql = "SELECT 		uf.*
			FROM		usersfriend uf
			LEFT JOIN users as u ON uf.friend_id = u.user_id
			WHERE		(uf.user_id = '".$user_id."' OR uf.friend_id = '".$user_id."') AND (u.first_name LIKE '%".$key."%' OR u.last_name LIKE '%".$key."%')";*/
		if($fname1 != '' && $lname1 != '')
		{
			$sql = "SELECT 		uf.*
			FROM		usersfriend uf
			LEFT JOIN users as u ON uf.friend_id = u.user_id
			WHERE (uf.user_id = '".$user_id."' OR uf.friend_id = '".$user_id."') AND (u.first_name LIKE ".$fname1." AND u.last_name LIKE ".$lname1.")";

		}else{
			$sql = "SELECT 		uf.*
			FROM		usersfriend uf
			LEFT JOIN users as u ON uf.friend_id = u.user_id
			WHERE (uf.user_id = '".$user_id."' OR uf.friend_id = '".$user_id."') AND (u.first_name LIKE ".$fname1." OR u.last_name LIKE ".$fname1.")";
		}

		$result = $db->fetchAll($sql);
		return $result;
	}
        
	public static function searhfriendsg($key, $user_id)
	{
		$db = Zend_Registry::get("db");
		$sql = "SELECT u.user_id, CONCAT(u.first_name,' ', u.last_name) AS username, u.profile_pic
			FROM		users AS u INNER JOIN usersfriend uf on uf.friend_id = u.user_id
			WHERE		(u.first_name LIKE ".$key." OR u.last_name LIKE ".$key.") AND 	uf.user_id  = '".$user_id."' AND uf.status = '1'";


		//echo $sql.'<br>';
		//exit;
		$result = $db->fetchAll($sql);
		return $result;
	}

	/// check item exist in friend wishlist or not
	public static function chkFriendItem($itemID, $user_id)
	{

		$db = Zend_Registry::get("db");
		$sqlf = "SELECT friend_id
				FROM usersfriend
				WHERE (user_id = '".$user_id."' AND status = '1')";

		$follow = $db->fetchAll($sqlf);
		$friendlist = array();

		for($i=0; $i < count($follow); $i++)
		{

			$friendlist[] = $follow[$i]->friend_id;
		}
		if(count($friendlist) > 0)
		{
			$friendlistStr = implode(",",$friendlist);
		}else{
			$friendlistStr = '0';
		}

		$sql = "SELECT id FROM userwishlist AS w WHERE w.item_id = '".$itemID."' AND w.user_id IN (".$friendlistStr.")";
		$result = $db->fetchOne($sql);
		return $result;
	}
	public static function getuserdetail($userid){
	$db = Zend_Registry::get("db");
	 $sql="select user_id,CONCAT(first_name,' ',last_name) AS fullName,profile_pic from users where user_id=".$userid."";
		$user=$db->fetchRow($sql);

		return $user;

	}

public static function getFollowListWeb($user_id)
	{
		$db = Zend_Registry::get("db");
		$sql = "SELECT 		*
			FROM		usersfriend
			WHERE		user_id 		= '".$user_id."' AND status = '1'";
		//echo $sql;
		$result = $db->fetchAll($sql);
		return $result;
	}

public static function getFollowerListWeb($user_id)
	{
		$db = Zend_Registry::get("db");

		$sql = "SELECT 		*
			FROM		usersfriend
			WHERE		friend_id  		= '".$user_id."' AND status = '1'";
		//echo $sql.'<br>';
		$result = $db->fetchAll($sql);
		return $result;
	}


/*	public static function latestaddedfriend($itemID,$userid)
	{
		$db = Zend_Registry::get("db");
		$sql="SELECT friend_id
FROM usersfriend
WHERE user_id =".$userid."";
$userfriends = $db->fetchAll($sql);
if($userfriends){
			$lastValue=count($userfriends)-1;
			$i=0;
			foreach($userfriends as $userfriend){
				$userfriend .=$item_ids->item_id;
				if($i != $lastValue){
					$userfriend .=",";
				}
				$i++;
			}

		}


	$sql	="select u.user_id,CONCAT(u.first_name,' ',u.last_name) AS fullName, u.profile_pic
 from userwishlist uw inner join users u on u.user_id=uw.user_id where uw.user_id in('5,1,28,4') and uw.item_id=".$itemID." order by uw.user_id desc limit 1";


		return $result = $db->fetchRow($sql);
	}*/

    public static function getallfreindids($userid){
     $db = Zend_Registry::get("db");
	 $sql="select friend_id from usersfriend where user_id=".$userid."";
    $users=$db->fetchAll($sql);


    		if($users){
			$lastValue=count($users)-1;
			$i=0;
			foreach($users as $user){
				$friendid .="'".$user->friend_id."'";
				if($i != $lastValue){
					$friendid .=",";
				}
				$i++;
			}

            }
	return $friendid;

    }

    public function getAllFriendsNamesForAutocomplete($user_id, $term)
    {
        $db = Zend_Registry::get("db");
        $sql = "SELECT CONCAT_WS(' ', u.first_name, u.last_name) AS name, u.user_id
				FROM usersfriend uf
				LEFT JOIN users u ON u.user_id = uf.friend_id
				WHERE ((uf.user_id = '".$user_id."') AND  (u.first_name LIKE '" . $term . "%' OR u.last_name LIKE '" . $term . "%') AND uf.status <> '2')";
        $result = $db->fetchAll($sql);
        foreach ($result as $row){
			if ($row->user_id != $_SESSION['user_id']) {
				$names[]= array( 'label' => $row->user_id, 'value' => $row->name );
			}
        }
        return $names;
    }

    public function getAllGroupsNamesForAutocomplete($user_id, $term)
    {
        $db = Zend_Registry::get("db");
        $sql = "SELECT group_name, id
				FROM groups
				WHERE created_by = '".$user_id."' AND group_name LIKE '" . $term . "%'";
        $result = $db->fetchAll($sql);
        foreach ($result as $row){
            $names[]= array( 'label' => $row->id, 'value' => $row->group_name );
        }
        return $names;
    }


public static function getlastaddedfriend($friendid){
    $db = Zend_Registry::get("db");
   $sql="select u.user_id,CONCAT(u.first_name,' ',u.last_name) AS fullName, u.profile_pic
   from users u inner join  userwishlist  uw on u.user_id=uw.user_id where uw.item_id=124 and uw.user_id in ('".$friendid."') order by uw.id desc limit 1";  
    $users=$db->fetchRow($sql);
    return $user;
}

	public static function getAllUserFeed($user_id,$sortby,$start,$end)
	{

		$db = Zend_Registry::get("db");	
		///user's item
		$sqlItemU = "SELECT item_id FROM userwishlist WHERE user_id =".$user_id;
		$resitemids = $db->fetchAll($sqlItemU);
		
		for($i=0; $i < count($resitemids); $i++)
		{
			$ids[] = $resitemids[$i]->item_id;
		}
		if(count($ids) > 0)
		{			
			$idsStr = implode(",",$ids);
		}else{			
			$idsStr = '0';
		}

		 $sql = "SELECT DISTINCT(w.id),w.*, w.id AS item_id
				FROM wishlistitmes w
				LEFT JOIN userwishlist uw ON w.id = uw.item_id
				WHERE uw.item_id NOT IN (".$idsStr.") ORDER BY
				".$sortby." LIMIT ".$start.",".$end;
	
		//echo $sql.'<br>';
		
		
		$result = $db->fetchAll($sql);
		return $result;
}

	public static function countFeeds($serverDate,$user_id){
		$db = Zend_Registry::get("db");
		$sql="SELECT  user_id , friend_id from usersfriend where user_id='".$user_id."'  ";
		$friends = $db->fetchAll($sql);
     
		$friend_id='';
		foreach($friends  AS $friend){
			$friend_id .= ' user_id ='. $friend->friend_id . ' OR'; 
    }
    
    
		$s=$friend_id.'1' ;
		$newcon = str_replace("OR1",' ',$s);
		$condiation='('.$newcon.')';
		
		$sql="SELECT count(id) AS total FROM  userwishlist WHERE ".$condiation." AND updated_date > '".$serverDate."' ";
	 
		$result=$db->fetchRow($sql);
		
		return $result;
	}

    public static function prepareForViewAllFriends($key = null)
    {
        $db = Zend_Registry::get("db");
        $server = Zend_Registry::get("server");

        /*
        if($key != '')
        {
            $nameArr = explode(" ", $key);

            if(count($nameArr) > 1)
            {
                $fname = '%'.$nameArr[0].'%';
                $lname = '%'.$nameArr[1].'%';

                $fname1 = $db->quote($fname);
                $lname1 = $db->quote($lname);

            }else{
                $fname = '%'.$key.'%';
                $fname1 = $db->quote($fname);
            }

            //$allfriendList = usersfriend::searchFriends($_SESSION['user_id'], $key);
            $allfriendList = usersfriend::searchFriends($_SESSION['user_id'], $fname1, $lname1);
        }else{
            //unset($_SESSION['totalFriends']);
            $allfriendList = usersfriend::getAllFriends($_SESSION['user_id']);
        }
       */
        $allfriendList = usersfriend::getAllFriends($_SESSION['user_id']);
        $totalFriend = 0;

        $logger = Zend_Registry::get("logger");
        for($i = 0; $i < count($allfriendList); $i++)
        {
            $list = array();

            /* check for valid entry */
            if ($allfriendList[$i]->notify == 0 || $allfriendList[$i]->notify == NULL) {
                 continue;
            }

            if($allfriendList[$i]->user_id == $_SESSION['user_id'])
            {
                /// following
                $friend_idf = $allfriendList[$i]->friend_id;
                $friendType = 'following';
                /// check this user is in your follower list
                $isfollower = usersfriend::checkExistFriends($allfriendList[$i]->friend_id, $_SESSION['user_id']);
                /// check You are following this user
                $isfollow = usersfriend::checkExistFriends($_SESSION['user_id'], $allfriendList[$i]->friend_id);

            }else{
                // follower
                $friend_idf = $allfriendList[$i]->user_id;
                $friendType = 'Follower';

                $isfollow = usersfriend::checkExistFriends($_SESSION['user_id'], $allfriendList[$i]->user_id);

                //if($allfriendList[$i]->friend_id == $_SESSION['user_id'])
                if($isfollow)
                {
                    continue;
                }
            }

            // Get group
            $groups = groups::getGroup($_SESSION['user_id'], $friend_idf);
            //
            $friend_relationID = $allfriendList[$i]->id;

            $user_detail = users::getUserDetails($friend_idf);
            if (!$user_detail) {
                 /* skip if user doesn't exist*/
                 continue;
            }

            $list['name'] = $user_detail->first_name.' '.$user_detail->last_name;
            $list['id'] = $friend_idf;
            $list['group_ids'] = $groups;

            $list['email_id'] = $user_detail->email_id;
            $list['gender'] = $user_detail->gender;
            $list['birth_date'] = $user_detail->birth_date;

            if($user_detail->profile_pic != '')
            {
				$list['profile_pic'] = $server->securehttp . $server->apphost . "/images/userpics/" . $user_detail->profile_pic;
            }else{
				$list['profile_pic'] = $server->securehttp . $server->apphost . "/images/userpics/default-profile-pic.png";
            }

            $list['created_date'] = $user_detail->created_date;
            $list['created_date'] = $user_detail->created_date;
            $list['twitter_name'] = $user_detail->twitter_name;
            $list['twitter_image'] = $user_detail->twitter_image;
            $list['facebook_id'] = $user_detail->facebook_id;
            $list['facebook_image'] = $user_detail->facebook_image;
            $list['status'] = $allfriendList[$i]->status;
            $friend = usersfriend::checkExistFriendStatus($friend_idf, $_SESSION['user_id']);
            $list['friend_status'] = $friend->status;

            if($isfollower != '')
            {
                $list['is_yourfollower'] = 'Y';
                $request_status = usersfriend::getRequestStatus($allfriendList[$i]->friend_id, $_SESSION['user_id']);
                $list['request_status'] = $request_status;
            }else{
                $list['is_yourfollower'] = 'N';
                $list['request_status'] = '';
            }

            if($isfollow != '')
            {
                $list['you_follow'] = 'Y';
            }else{
                $list['you_follow'] = 'N';
            }

            $list['type'] = $friendType;
			$list['groups'] = groups::getCommonGroupNamesAndIds($_SESSION['user_id'], $friend_idf);

            $allFriends[] = $list;

            if(($allfriendList[$i]->status == 0) && ($isfollower == ''))
            {
                /// do noting
            }else{
                $totalFriend++;
            }

            //}	// if stattus
        }// for

        $sortAr = asort($allFriends);
        //$totalFriends = 0;
        //$totalFriends = count($allFriends);

        if($_SESSION['totalFriends'] == '')
        {
            $_SESSION['totalFriends'] = $totalFriend;
            session::write(session_id(), $_SESSION);
        }
        return $allFriends;
    }

    public static function getUsersToEventByStatus($event_id, $status)
    {
        $db = Zend_Registry::get("db");
        $query = "SELECT invited_user AS friend_id from eventrsvp where RSVP_status='".$status."' AND event_id='".$event_id."'";
        $sql=$db->query($query);
        return $sql->fetchAll();
    }

    public static function getInvitedUsersToEvent($event_id)
    {
        $db = Zend_Registry::get("db");
        $query = "SELECT invited_user AS friend_id from eventrsvp where event_id='".$event_id."'";
        $sql=$db->query($query);
        return $sql->fetchAll();
    }

    public static function prepareUsersDetails($usersList, $areFollowers = null)
    {
        $result = array();
        for ($i = 0; $i < count($usersList); $i++) {
            $list = array();
            $follower_details = users::getUserDetails(($areFollowers) ? $usersList[$i]->user_id : $usersList[$i]->friend_id);
            if ($follower_details->first_name == '' || $follower_details->last_name == '') {
                  continue;
            }
            $list['user_id'] = $follower_details->user_id;
            $list['username'] = $follower_details->first_name . ' ' . $follower_details->last_name;
            $list['profile_pic'] = $follower_details->profile_pic;
            $result[] = $list;
        }

        return $result;
    }

    public static function prepareFacebookFriends($server, $accessToken)
    {
        $db  = Zend_Registry::get("db");

        require 'fb/facebook.php';

        Facebook::$CURL_OPTS[CURLOPT_SSL_VERIFYPEER] = false;
        Facebook::$CURL_OPTS[CURLOPT_SSL_VERIFYHOST] = 2;

        $facebook = new Facebook(array(
            'appId'  => $server->FACEBOOK_APP_ID,
            'secret' => $server->FACEBOOK_SECRET,
        ));
        $facebook->setAccessToken($accessToken);
        $userfb = $facebook->getUser();
        if($userfb)
        {
            $fb_friend = $facebook->api('/me/friends?fields=id,name,picture.width(70).height(70),link');

                for($i = 0; $i < count($fb_friend['data']); $i++)
            {
                $list = array();
                $list['name'] = $fb_friend['data'][$i]['name'];
                $list['fbid'] = $fb_friend['data'][$i]['id'];
                $list['pic'] = $fb_friend['data'][$i]['picture']['data']['url'];

                // check this friend is registered in site
                // if registered in the site, check he/she in your friendlist

                $friendUsser_id = users::checkExistsFBid($fb_friend['data'][$i]['id']);
                if(!empty($friendUsser_id))
                {
                    $list['insite'] = 'Y';
                    $list['id'] = $friendUsser_id;

                    $friendExists = usersfriend::checkExistFriends($_SESSION['user_id'], $friendUsser_id);
                    if ($friendExists){

                        $friend_status = usersfriend::getRequestStatus($_SESSION['user_id'], $friendUsser_id);

                        $list['request_status'] = $friend_status;

                        if($friend_status == '1')
                        {
                            $list['isFriend'] = 'Y';
                        }
                        elseif($friend_status == '0')
                        {
                            $list['isFriend'] = 'P';
                        }else{
                            $list['isFriend'] = 'N';
                        }
                    }else{
                        $data['user_id'] = $_SESSION['user_id'];
                        $data['friend_id'] = $friendUsser_id;
                        $data['status'] = '2';
                        $data['notify'] = '0';
                        $db->insert('usersfriend',$data);
                        $list['isFriend'] = 'N';
                        $list['request_status'] = 2;
                    }
                }else{
                    $list['insite'] = 'N';
                    $list['isFriend'] = 'N';
                    $list['id'] = 0;
                }
                $friendlist[] = $list;
            }//for


            //$sortedFriedlist = sort($friendlist);
            return wishlistitmes::msort($friendlist, array('name'));
        } else {
            return null;
        }
    }

  public static function prepareFacebookFriendsyouknow($accessToken)
    {
        $db  = Zend_Registry::get("db");
        $server = Zend_Registry::get("server");
        $logger = Zend_Registry::get("logger");

        require 'fb/facebook.php';

        Facebook::$CURL_OPTS[CURLOPT_SSL_VERIFYPEER] = false;
        Facebook::$CURL_OPTS[CURLOPT_SSL_VERIFYHOST] = 2;

        $facebook = new Facebook(array(
            'appId'  => $server->FACEBOOK_APP_ID,
            'secret' => $server->FACEBOOK_SECRET,
        ));
        $facebook->setAccessToken($accessToken);
        $userfb = $facebook->getUser();
        if($userfb)
        {
            $fb_friend = $facebook->api('/me/friends?fields=id,name');
            $logger->info("fb friend id = ". count($fb_friend['data']));
            for($i = 0; $i < count($fb_friend['data']); $i++)
            {
                $list = array();

                $friendUsser_id1 = $fb_friend['data'][$i]['id'];
                $friendUsser_id1 = users::checkExistsFBid($friendUsser_id1);
                $fbname = explode(' ', $fb_friend['data'][$i]['name']);
                $friendUsser_id2 = users::checkExistsFirstName($fbname[0], $fbname[1]);

                $friendUsser_id = '';
                if (!empty($friendUsser_id1)) {
                     $friendUsser_id = $friendUsser_id1;
                } else if (!empty($friendUsser_id2)) {
                     $friendUsser_id = $friendUsser_id2;
                }

                if(!empty($friendUsser_id))
                {
                    $friendExists = usersfriend::checkExistFriends($_SESSION['user_id'], $friendUsser_id);
                    $friend = usersfriend::checkExistFriendStatus($friendUsser_id, $_SESSION['user_id']);
                    $status1 = $friend->status;
                    if ($friend->status == 2 || $friend->status == 1) {   
                        // continue;
                    }   
                    $friend = usersfriend::checkExistFriendStatus($_SESSION['user_id'], $friendUsser_id);
                    if ($friend->status == 2 || $friend->status == 1) {
                        // continue;
                    }
                    $status2 = $friend->status;
                    $user_detail = users::getUserDetails($friendUsser_id);
                    if (!$user_detail) {
                          /* skip if user doesn't exist*/
                          continue;
                    }

                    $list['name'] = $user_detail->first_name.' '.$user_detail->last_name;
                    $list['first_name'] = $user_detail->first_name;
                    $list['id'] = $friendUsser_id;
                    //$list['friend_status'] = $friend->status;
                    $list['group_ids'] = $groups;

                    $list['email_id'] = $user_detail->email_id;
                    $list['facebook_email'] = $user_detail->facebook_email;
                    $list['gender'] = $user_detail->gender;
                    $list['birth_date'] = $user_detail->birth_date;

                    if($user_detail->profile_pic != '')
                    {
                               $list['profile_pic'] = "https://".$server->apphost."/userpics/thumb/".$user_detail->profile_pic;
                    }else{
                               $list['profile_pic'] = "https://".$server->apphost."/userpics/thumb/default-profile-pic.png";
                    }

                    $list['created_date'] = $user_detail->created_date;
                    $list['created_date'] = $user_detail->created_date;
                    $list['twitter_name'] = $user_detail->twitter_name;
                    $list['twitter_image'] = $user_detail->twitter_image;
                    $list['facebook_id'] = $user_detail->facebook_id;
                    $list['facebook_image'] = $user_detail->facebook_image;
                    $friend = usersfriend::checkExistFriendStatus($friendUsser_id, $_SESSION['user_id']);
                    $list['friend_status'] = $friend->status;

                    $list['is_yourfollower'] = 'N';
                    $list['request_status'] = 1;
                    $list['status'] = $status2;
                    $list['you_follow'] = 'N';
                    $list['type'] = 'None';
                    
                    $allFriends[] = $list;
                } // empty($friendUsser_id)
            } /* For loop */
            return wishlistitmes::msort($allFriends, array('name'));
        } else {
            return null;
        }
    }

    public static function getFriendsFromGroupIds($groupIds, $userId){
        $db = Zend_Registry::get("db");
        $query = "SELECT DISTINCT(user_id) AS id FROM friendsgroup WHERE group_id IN ('".$groupIds."') AND user_id <> '".$userId."'";
        $sql=$db->query($query);
        return $sql->fetchAll();
    }
	
	public static function followFriend($userId, $friendId, $follow = null) {
		$db = Zend_Registry::get("db");
		$db->beginTransaction();
		$data['user_id'] = $userId;
		$data['friend_id'] = $friendId;
		$return = '';

		/// check follow permission of user
		/// if follow persion is 1 - need grant permission before follow
		// if permisson is 0 - automatic follow
		$user_detail = users::getUserDetails($friendId);

		if(($user_detail->follow_permission == 1) || !$follow)
		{
			$data['status'] = '0';
			$dataU['status'] = '0';
			$return = 'Pending';
			$actity_data['activity_type'] = 'Ask Permission';
		}else{
			$data['status'] = '1';
			$dataU['status'] = '1';
			$actity_data['activity_type'] = 'Follow';
			$return = 'Unfollow';
		}
		//delete previous activity (if there is)
		notifications::removelastactivity($userId, $friendId);
		// record activity
		$actity_data['sender'] = $userId;
		$actity_data['receiver'] = $friendId;
		$actity_data['activity_date'] = date('Y-m-d H:i:s');
		$actity_data['item_id'] = 0;
		$actity_data['event_id'] = 0;
		$actity_data['deleted_user'] = 0;
		$actity_data['new'] = true;
		$db->insert('activity', $actity_data);
		$activityId = $db->lastInsertId();

		//$data['status'] = '1';
		// check this relation is already exist or not

		$chkrelation = usersfriend::checkExistFriends($userId, $friendId);

		if($chkrelation != '')
		{
			// update status
			$dataU['notify'] = $activityId;
			$db->update('usersfriend', $dataU, array("user_id = ?" => $userId, "friend_id = ?" => $friendId));
		}else{
			// insert relation
			$data['notify'] = $activityId;
			$db->insert('usersfriend', $data);
		}
		$db->commit();
		if($user_detail->email_id != '') {
			self::followfriendemail($user_detail->email_id, $user_detail->first_name, $user_detail->follow_permission);
		} else if ($user_detail->facebook_email != '') {
			self::followfriendemail($user_detail->facebook_email, $user_detail->first_name, $user_detail->follow_permission);
		}
		return $return;
	}

	protected static function followfriendemail($emailID, $reciver_first_name, $ask_perm)
{
	$db  = Zend_Registry::get("db");
	$config = Zend_Registry::get("config");
	$server = Zend_Registry::get("server");
	$logger = Zend_Registry::get("logger");

	$user_details = users::getUserDetails($_SESSION['user_id']);
	if(!$ask_perm) {
		$subject = $user_details->first_name.' '. $user_details->last_name.' is following you on Giftvise';
		$gendertxt = ($user_details->gender == 'M') ? 'his' : 'her';
		$bodyMsg = $user_details->first_name.' has started following you on Giftvise. Click the button below to see '.$gendertxt.' profile.';
	} else {
		$subject = $user_details->first_name.' '. $user_details->last_name.' wants to follow you on Giftvise';
		$bodyMsg = 'Click the button below to login to Giftvise and allow '.$user_details->first_name.' to follow you. You will see the "follow" request in your notification drop down.';
	}
	try{
		$mailText = '
     <html><head>

<!-- Define Charset -->
        <meta http-equiv=Content-Type content=text/html; charset=UTF-8 />
        
        <!-- Responsive Meta Tag -->
        <meta name=viewport content=width=device-width; initial-scale=1.0; maximum-scale=1.0; />
        <link href="http://fonts.googleapis.com/css?family=Questrial" rel="stylesheet" type="text/css">
        
        <!--- Importing Twitter Bootstrap icons -->
        <link href=http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css rel=stylesheet>

</head><body>
<div marginheight= 0  marginwidth= 0 >
	<table style= background-color:rgb(21,18,18)  cellpadding= 0  cellspacing= 0  bgcolor= ecebeb  border= 0  width= 100% >
		<tbody><tr>
		<td>
				<table cellpadding= 0  cellspacing= 0  align= center  border= 0  width=100% >
					<tbody><tr><td style= font-size:30px;line-height:30px  height= 30 > </td></tr>
					<tr>
						<td>
							<table style= border-collapse:collapse  cellpadding= 0  cellspacing= 0  align=left  border=0 width=45% >
								<tbody><tr>
									<td align= center >
										<table cellpadding= 0  cellspacing= 0  align= center  border=0 >
											<tbody><tr>
												<td style= line-height:21px  align= center >

	<a href= https://giftvise.com/  style=display:block;border-style:none!important;border:0!important  target= _blank ><img style= display:block;width:128px  src=https://www.giftvise.com/newdesign/img/tpl/new-giftvise-logo.png width=180 ></a>
												</td>			
											</tr>
										</tbody></table>		
									</td>
								</tr>
								
							</tbody></table>
							
							<table style=border-collapse:collapse  cellpadding= 0  cellspacing=0  align=left  border=0  width=5px>
								<tbody><tr><td style= font-size:20px;line-height:20px  height=20  width=5px > </td></tr>
							</tbody></table>
							
							<table style=border-collapse:collapse  cellpadding=10  cellspacing= 0  align= right  border=0 width=30%>
								
								<tbody><tr>
									<td>
										<table style=border-collapse:collapse  cellpadding= 0  cellspacing= 0  align= left  border= 0 >
			
				                			<tbody><tr>
				                				<td>
				                					<table style=border-collapse:collapse  cellpadding= 0  cellspacing= 0  align=center>
				                						<tbody><tr>
				                							
							                				<td style= color:#ffffff;font-size:14px;font-family:&#39;Questrial&#39;,Helvetica,sans-serif;line-height:24px  align= center >
							                						<div style= line-height:24px >
							                						<span>
<a href="https://'.$server->apphost.'/modals/modalshowhelpcenter?tab=about" style=color:#878b99;text-decoration:none >About</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="https://'.$server->apphost.'/modals/modalshowhelpcenter?tab=faq" style= color:#878b99;text-decoration:none>Help</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a><a href="https://'.$server->apphost.'/modals/modalshowhelpcenter?tab=contact" style= color:#878b99;text-decoration:none >Contact</a>
								                					
							                						</span>
							                					</div>	
							                				</td>
				                						</tr>
				                					</tbody></table>
				                				</td>
				                			</tr>
			                			</tbody></table>
									</td>
								</tr>	
							</tbody></table>	
						</td>
					</tr>
					
					<tr>
						<td style= line-height:21px  align= center >
						</td>			
					</tr>
					
					<tr><td style= font-size:50px;line-height:50px  height= 50 > </td></tr>
					
					<tr>
						<td style= color:#ffffff;font-size:40px;font-family:&#39;Questrial&#39;,Helvetica,sans-serif;line-height:28px  align= center >
							
							
							
							<div style= line-height:28px >
								<span>Hi '.$reciver_first_name .'!</span>
							</div>
        				</td>
					</tr>
					
					<tr><td style= font-size:25px;line-height:25px  height= 25 > </td></tr>
					
					<tr>
						<td align= center >
							<table cellpadding= 0  cellspacing= 0  align= center  border= 0  width= 540 >
								<tbody><tr>
									
									<td style= color:#878b99;font-size:18px;font-family:&#39;Questrial&#39;,Helvetica,sans-serif;line-height:30px  align= center >
										<div style= line-height:30px >
											
											
											<span>'. $bodyMsg .' </span>
										</div>
			        				</td>	
								</tr>
							</tbody></table>
						</td>
					</tr>
					
					<tr><td style= font-size:35px;line-height:35px  height= 35 > </td></tr>
					
					<tr>
						<td align= center >
							
							<table style= border-radius:50px  cellpadding= 0  cellspacing= 0  align= center  bgcolor= f06e6a  border= 0  width= 240 >
								
								<tbody><tr><td style= font-size:18px;line-height:18px  height= 18 > </td></tr>
								
								<tr>
	                				<td style= color:#ffffff;font-size:18px;font-family:&#39;Questrial&#39;,Helvetica,sans-serif  align= center >
	                					
	                					
		                    			<div style= line-height:24px >
<a style=text-decoration:none href="' . $server->securehttp . $server->apphost . '/friends/friendprofile/friend_id/' . md5($user_details->user_id) . '">
		                    				<span style= color:#ffffff;text-decoration:none >See Profile</span>
		                    	             </a>		</div>
		                    		</td>								
								</tr>
								
								<tr><td style= font-size:18px;line-height:18px  height= 18 > </td></tr>
							
							</tbody></table>
						</td>
					</tr>
					
					<tr><td style= font-size:65px;line-height:65px  height= 65 > </td></tr>
					
				</tbody></table>
			</td></tr>
		
	</tbody></table>
	
<!--	
	<table cellpadding= 0  cellspacing= 0  bgcolor= 363b49  border= 0  width= 100% >
							
		<tbody><tr><td style= font-size:20px;line-height:20px  height= 20 > </td></tr>
					
		<tr>
			<td align= center >
				
				<table cellpadding= 0  cellspacing= 0  align= center  border= 0  width= 590 >
					
					<tbody><tr>
						<td align= center >
								                		
	                		<table cellpadding= 0  cellspacing= 0  align= center  border= 0 >
	                			<tbody><tr>
	                				<td style= color:#5a5f70;font-size:14px;font-family:&#39;Questrial&#39;,Helvetica,sans-serif;line-height:25px  align= center >
	                					<div style= line-height:25px >
	                						<span>
		                					
		                						Copyright @ Giftvise 2014
		                					
	                						</span>
	                					</div>	
	                				</td>
	                			</tr>
	                			
                			</tbody></table>
                			
						</td>
					</tr>
					
				</tbody></table>
			</td>
		</tr>
		<tr><td style= font-size:20px;line-height:20px  height= 20 > </td></tr>
	</tbody></table>
-->
<!-- copy right secion -->
<table border=0 width=100% cellpadding=0 cellspacing=0 bgcolor=#ffffff class=bg4_color>

 <tbody><tr><td height=20 style=font-size:20px;line-height:20px;>&nbsp;</td></tr>

 <tr> <td align=center>

 <table border=0 align=center width=590 cellpadding=0 cellspacing=0 class=container590>

 <tbody><tr> <td align=center>

 <table border=0 align=center cellpadding=0 cellspacing=0> <tbody><tr> 
<td style=color:#5a5f70;font-size:14px;font-family:&#39;Questrial&#39;,Helvetica,sans-serif;line-height:25px align=center>
<div class=editable_text style=line-height:25px;> <span class=text_container>

 Copyright @ Giftvise 2014

 </span> </div> </td> </tr>

 </tbody></table>

 </td> </tr>

 </tbody></table> </td> </tr>

 <tr><td height=20 style=font-size: 20px; line-height: 20px;>&nbsp;</td></tr>

 </tbody></table>
</div></body></html>

                           ';
		$to = $emailID;

		//$subject = $user_details->first_name.' '. $user_details->last_name.' is following you on Giftvise';
		if(1/*empty($chkExistRequest)*/)
		{
			$transport = new Zend_Mail_Transport_Sendmail($config->supportEmail);
			Zend_Mail::setDefaultTransport($transport);
			$mail = new Zend_Mail();
			$mail->setSubject($subject);
			$mail->setFrom($config->supportEmail, 'Giftvise');
			$mail->addTo($to);
			$mail->setBodyHtml($mailText);
			$mail->send();
			$logger->info("Follow email has bee sent to " . $reciver_first_name);
			$msg = "Invitation has been sent.";
		}else{
			$msg = "You have already sent request to this user";
		}
	}
	catch(Exception $e)
	{
	}
}

}//end class
