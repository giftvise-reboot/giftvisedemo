<?php
/*------------------------------------------------------------------ 
 * Copyright (c) 2013-2014 by Giftvise, Inc.
 * All rights reserved. 
 *------------------------------------------------------------------
 */
?>

<?php

class groups extends Zend_Db_Table_Abstract
{			
	public static function checkExistGroup($userid,$groupName)
	{
		$sql = "SELECT * 
				FROM groups 
				WHERE group_name = ".$groupName." AND created_by = '".$userid."'";
		
		$db = Zend_Registry::get("db");	
		$result = $db->fetchOne($sql);
		return $result;
	}
	
	public static function checkfriendInGroup($friend_id,$group_id)
	{
		$sql = "SELECT * 
				FROM friendsgroup 
				WHERE user_id = '".$friend_id."' AND group_id = '".$group_id."'";
		
		//echo $sql;
		//exit;
		$db = Zend_Registry::get("db");	
		$result = $db->fetchOne($sql);
		return $result;
	}
	
	public static function groupList($user_id)
	{
		$sql = "SELECT * 
				FROM groups 
				WHERE created_by = '".$user_id."' ORDER BY group_name";

		$db = Zend_Registry::get("db");
		$result = $db->fetchAll($sql);
		return $result;
	}
	
	public static function deleteGroup($group_id)
	{
		$sql = "SELECT id 
				FROM friendsgroup 
				WHERE group_id = '".$group_id."'";
		
		$db = Zend_Registry::get("db");	
		$resChk = $db->fetchOne($sql);
		
		if($resChk)
		{
			$db->delete('friendsgroup', 'group_id = '.$group_id);
		}
		/// delete main group
		
		$res = $db->delete('groups', 'id = '.$group_id);
		return $res;
	}
	
	public static function getGroup($user_id, $friend_id)
	{
		$sql = "SELECT id 
				FROM groups 
				WHERE created_by = '".$user_id."'";
				
		$db = Zend_Registry::get("db");	
		$result_groups = $db->fetchAll($sql);
		
		$mygroup = array();
		for($i=0; $i<count($result_groups); $i++)
		{
			$mygroup[] = $result_groups[$i]->id;
		}
		
		//print_r($mygroup);
		if(count($mygroup) >0)
		{
			$mygroupStr = implode(",",$mygroup);
			
			$sqlFr = "SELECT * FROM friendsgroup WHERE 	user_id = '".$friend_id."' AND group_id IN (".$mygroupStr.")";
		//echo $sqlFr;		
		//exit;
			$resultFR = $db->fetchAll($sqlFr);
			for($i=0; $i<count($resultFR); $i++)
			{
				$friendsgroup[] = $resultFR[$i]->group_id;
			}
			$friendsgroupStr = implode(",",$friendsgroup);
			
		}else{
			$friendsgroupStr = 0;
		}
		return $friendsgroupStr;
	}

	public static function getCommonGroupNamesAndIds($userId, $friendId)
	{
		$sql = "SELECT g.id, g.group_name  
				FROM groups AS g 
				INNER JOIN friendsgroup AS fg ON fg.group_id = g.id 
				WHERE g.created_by = '" . $userId . "' AND fg.user_id = '" . $friendId . "'" ;
		$db = Zend_Registry::get("db");
		return $db->fetchAll($sql);
	}

        public static function getFriendsGroupIBelong($user_id)
        {
                $sql = "SELECT DISTINCT(f.group_id) 
                                FROM friendsgroup AS f
                                WHERE f.user_id = '".$user_id."'";

                $db = Zend_Registry::get("db");
                $result_groups = $db->fetchAll($sql);

                $mygroup = array();
                for($i=0; $i<count($result_groups); $i++)
                {
                        $mygroup[] = $result_groups[$i]->group_id;
                }
                 $logger = Zend_Registry::get("logger");
                $mygroupStr = implode(",",$mygroup);
               $logger->info("Chikke = ". $mygroupStr);

                if(count($mygroup) >0)
                {
                        $sql = "SELECT * 
                                FROM  groups WHERE  created_by != ".$_SESSION['user_id']." AND id IN (".$mygroupStr.")";
                        $resultFR = $db->fetchAll($sql);
                        return $resultFR;
                }
               return null;
        }

	
	public static function deleteFriendGroup($group_id, $user_id)
	{	
		$db = Zend_Registry::get("db");	
		$res = $db->delete('friendsgroup', array("user_id = ?" => $user_id, "group_id = ?" => $group_id));
		return $res;
	}
	
	/// get friedns list of a group
	// send group_id as input
	// return array of the friends which are added in this group
	public static function getGroupFriend($group_id, $user_id)
	{
		$db = Zend_Registry::get("db");	
		
		
		$sql = "SELECT friend_id 
				FROM usersfriend 
				WHERE (user_id = '".$user_id."' AND status = '1')";				
		$sql1 = "SELECT user_id 
				FROM usersfriend 
				WHERE (friend_id = '".$user_id."' AND status = '1')";	
		
		$follow = $db->fetchAll($sql);
		$follower = $db->fetchAll($sql1);
		
		$friendlist = array();
		$friendlistStr = '';
		
		for($i=0; $i < count($follow); $i++)
		{
			
			$friendlist[] = $follow[$i]->friend_id;
		}
		/*for($i=0; $i < count($follower); $i++)
		{
			
			$friendlist[] = $follower[$i]->user_id;
		}*/
		if(count($friendlist) > 0)
		{
			$friendlistStr = implode(",",$friendlist);
		}else{
			$friendlistStr = '0';
		}
		
		$sql = "SELECT u.user_id, CONCAT(u.first_name,' ', u.last_name) AS username, u.profile_pic
				FROM users AS u, groups AS g
				LEFT JOIN friendsgroup AS fg ON g.id = fg.group_id 
				
				WHERE fg.user_id IN (".$friendlistStr.") AND u.user_id = fg.user_id AND fg.group_id = '".$group_id."'";
				
		//echo $sql;
		//exit;
		$result = $db->fetchAll($sql);
		return $result;
	}
	
	/// get all information of a group
	/// send group_id as input
	/// it returns group's nama, description, images etc.
	public static function getGroupInfo($group_id)
	{
		$db = Zend_Registry::get("db");
		
		$result = $db->fetchRow("
			SELECT 		*
			FROM		groups
			WHERE		md5(id) 		= ?",
			$group_id
		);
		
		
		return $result;
	}

	public static function getGroupIdByHashCode($code)
	{
		$db = Zend_Registry::get("db");
		$result = $db->fetchOne("
			SELECT 		id
			FROM		groups
			WHERE		md5(id) 	= ?",
			$code
		);
		return $result;
	}
	
	//// Get group name 'unknown' with highest number
	public static function chkForGroupName($groupName, $userid)
	{
		$sql = "SELECT group_name 
				FROM groups 
				WHERE group_name LIKE '".$groupName."%' AND created_by = '".$userid."' ORDER BY group_name DESC LIMIT 0,1";
		
		$db = Zend_Registry::get("db");	
		$result = $db->fetchOne($sql);
		return $result;
	}
	
	public static function getFriendsGroup($user_id, $friend_id)
	{
		$sql = "SELECT id 
				FROM groups 
				WHERE created_by = '".$user_id."'";
				
		$db = Zend_Registry::get("db");	
		$result_groups = $db->fetchAll($sql);
		
		$mygroup = array();
		for($i=0; $i<count($result_groups); $i++)
		{
			$mygroup[] = $result_groups[$i]->id;
		}
		
		//print_r($mygroup);
		if(count($mygroup) >0)
		{
			$mygroupStr = implode(",",$mygroup);
			
			$sqlFr = "SELECT g.* FROM friendsgroup fg LEFT JOIN groups g ON g.id = fg.group_id WHERE fg.user_id = '".$friend_id."' AND fg.group_id IN (".$mygroupStr.")";
		//echo $sqlFr;		
		//exit;
			$resultFR = $db->fetchAll($sqlFr);
			for($i=0; $i<count($resultFR); $i++)
			{
				$list['group_id'] = $resultFR[$i]->id;
				$list['groupName'] = $resultFR[$i]->group_name;
				
				$friendsgroup[] = $list;
			}
		}else{
			
		}
		return $friendsgroup;
	}

    /**
     * get group names by ids
     *
     * @param $groupIds string (ids separated by comma)
     * @return mixed
     */

    public static function getGroupNameByIds($groupIds)
    {
        $db = Zend_Registry::get("db");
        $sql = 'SELECT g.group_name FROM friendsgroup AS fg LEFT JOIN groups AS g WHERE fg`id` IN (' . implode(',', array_map('intval', $groupIds)) . ')';
        $result = $db->fetchAll($sql);
        return $result;
    }

    public static function getFriendsFromGroup($groupId, $userId)
    {
        $db = Zend_Registry::get("db");
        $sql = 'SELECT *
                FROM users AS u
                LEFT JOIN friendsgroup AS fg ON  fg.user_id = u.user_id
                WHERE (fg.group_id = '. $groupId .' AND u.user_id <> ' . $userId . ')';
//        Zend_Debug::dump($sql);exit;
        $result = $db->fetchAll($sql);
        return $result;
    }

     public static function getGroupMembers($groupId)
    {
        $db = Zend_Registry::get("db");
        $sql = 'SELECT *
                FROM users AS u
                LEFT JOIN friendsgroup AS fg ON  fg.user_id = u.user_id
                WHERE (fg.group_id = '. $groupId .')';
//        Zend_Debug::dump($sql);exit;
        $result = $db->fetchAll($sql);
        return $result;
    }

    public static function groupListWithCheckFriend($user_id,$friend_id)
    {
        $db = Zend_Registry::get("db");
        $sql = "SELECT g.id, g.group_name, (CASE  WHEN fg.user_id = '".$friend_id."' THEN true ELSE false END) AS friend_check
				FROM groups g
				LEFT JOIN friendsgroup fg ON fg.group_id = g.id AND fg.user_id = '".$friend_id."'
				WHERE created_by = '".$user_id."' ORDER BY group_name";

        $result = $db->fetchAll($sql);
        return $result;
    }

    public static function checkUserInGroups($userId, $groupList)
    {
        $sql = "SELECT *
				FROM friendsgroup
				WHERE user_id = '".$userId."' AND group_id IN ('".$groupList."')";

        //echo $sql;
        //exit;
        $db = Zend_Registry::get("db");
        $result = $db->fetchOne($sql);
        return $result;
    }
	
	public static function addFriendToGroup($friend_id, $group_id) {

		$db = Zend_Registry::get("db");
		$inexist = groups::checkfriendInGroup($friend_id, $group_id);

		if ($inexist == ''){
			$db->beginTransaction();
			$data['user_id'] = $friend_id;
			$data['group_id'] = $group_id;

			$db->insert('friendsgroup', $data);

			// record user's activity
			$groupInfo = groups::getGroupInfo(md5($group_id));
			$dataA['sender'] = $_SESSION['user_id'];
			$dataA['receiver'] = $friend_id;
			$dataA['activity_date'] = date('Y-m-d H:i:s');
			$dataA['activity_type'] = 'Added in group - '.$groupInfo->group_name;
			//$db->insert('activity', $dataA);
			//
			$db->commit();

			return 'Friend added successfully!';
		}
		else {
			return 'The friend is already on group';
		}
	}

}///end class
