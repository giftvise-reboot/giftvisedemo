<?php
/*------------------------------------------------------------------ 
 * Copyright (c) 2013-2014 by Giftvise, Inc.
 * All rights reserved. 
 *------------------------------------------------------------------
 */
?>

<?php

class event extends Zend_Db_Table_Abstract
{

	// get all events of user
	public static function getUserEvents($user_id,$event_month,$event_year)
	{
		$db = Zend_Registry::get("db");
		$results=$db->fetchAll("SELECT first_name,last_name,profile_pic,created_user AS created_user_id,event_date,event_time,title,description,location,
							 image AS event_image,ifnull(eventrsvp.invited_user,'') AS invited_user,RSVP_status,events.updated_date as updated_date,events.id as event_id
							 FROM events
							 LEFT JOIN eventrsvp ON eventrsvp.event_id=events.id
							 JOIN users ON users.user_id=events.created_user
							 WHERE (created_user='".$user_id."' OR invited_user='".$user_id."') AND MONTH(event_date)='".$event_month."' AND YEAR(event_date)='".$event_year."'  GROUP BY events.id
");

/* echo "SELECT first_name,last_name,profile_pic,created_user AS created_user_id,event_date,event_time,title,description,location,
							 image AS event_image,ifnull(eventrsvp.invited_user,'') AS invited_user,RSVP_status,events.updated_date as updated_date,events.id as event_id
							 FROM events
							 LEFT JOIN eventrsvp ON eventrsvp.event_id=events.id
							 JOIN users ON users.user_id=events.created_user
							 WHERE (created_user='".$user_id."' OR invited_user='".$user_id."') AND MONTH(event_date)='".$event_month."' AND YEAR(event_date)='".$event_year."'  GROUP BY events.id ";
exit; */

//WHERE (created_user='".$user_id."' OR invited_user='".$user_id."') AND MONTH(event_date)='".$event_month."' AND YEAR(event_date)='".$event_year."' AND event_date>=curdate() GROUP BY events.id

		return $results;
	}

	public static function getUserModifiedEvents($user_id,$event_id_updated_date,$month,$year)
	{
		$db = Zend_Registry::get("db");

		return $db->fetchAll("SELECT first_name,last_name,profile_pic,created_user AS created_user_id,event_date,event_time,title,description,location,
							image AS event_image,ifnull(eventrsvp.invited_user,'') AS invited_user,RSVP_status, events.updated_date as updated_date,events.id as event_id
							FROM events
							LEFT JOIN eventrsvp ON eventrsvp.event_id=events.id
							JOIN users ON users.user_id=events.created_user
							WHERE (created_user='".$user_id."' OR invited_user='".$user_id."') AND CONCAT(events.id,' ',events.updated_date) NOT IN (".$event_id_updated_date.")

								AND MONTH(event_date)='".$month."' AND YEAR(event_date)='".$year."'  GROUP BY events.id");

							//AND MONTH(event_date)='".$month."' AND YEAR(event_date)='".$year."' AND event_date>=curdate() GROUP BY events.id");

	}

	// check delete log
	public static function getEventDeleteLog($event_id)
	{
		$db = Zend_Registry::get("db");
		$sql = "SELECT event_id FROM delete_event_log WHERE event_id IN (".implode(",",$event_id).")";
		$result = $db->fetchAll($sql);
		return $result;
	}

	// check delete log
	public static function isEventExist($event_id)
	{
		$db = Zend_Registry::get("db");
		$result = $db->fetchOne("
			SELECT id, * FROM events WHERE id 	= ?",
			$event_id
		);
		return $result;
	}

	public static function eventrsvp($event_id,$user_id)
	{
		$db = Zend_Registry::get("db");
		return $result = $db->fetchAll("
					SELECT usersfriend.user_id AS user_id,friend_id,first_name,last_name,profile_pic,RSVP_status FROM usersfriend
					JOIN users ON users.user_id=usersfriend.friend_id
					LEFT JOIN (SELECT * FROM eventrsvp where eventrsvp.event_id='".$event_id."') as temp ON temp.invited_user=usersfriend.friend_id
					WHERE usersfriend.user_id='".$user_id."' AND usersfriend.status='1'");

	}
    public static function isbuyGiftForfriend($user_id,$item_id)
	{
		$db = Zend_Registry::get("db");
		$results=$db->fetchOne("SELECT item_id FROM giftsexchanged WHERE item_id= '".$item_id."' AND receiver_id='".$user_id."' and status='0' ");
		return $results;
	}
	// get user events for website
	public static function gettodayevents($user_id,$date,$limit)
	{
		if($date ==""){
			$date = date("Y-m-d");
        }
        date_default_timezone_set($_SESSION['time_zone']);
        $todayStart = DateTime::createFromFormat('Y-m-d H:i', $date.' 00:00');
        $todayEnd = DateTime::createFromFormat('Y-m-d H:i:s', $date.' 23:59:59');
        return self::geteventsbetween($user_id, $todayStart, $todayEnd, $limit);
	}

    // get user events for website
    public static function getmonthevents($user_id, $month, $year, $limit, $type = null)
    {
        date_default_timezone_set($_SESSION['time_zone']);
        $monthStart = DateTime::createFromFormat('Y-m-d H:i', $year .'-'.$month.'-01 00:00');
        $monthEnd = DateTime::createFromFormat('Y-m-d H:i:s', $year + 1 .'-'.$month.'-31 23:59:59');
        return self::geteventsbetween($user_id, $monthStart, $monthEnd, $limit, $type);
    }

    public static function getoneyearfromnowevents($user_id, $limit)
    {
        date_default_timezone_set($_SESSION['time_zone']);
        $yearStart = DateTime::createFromFormat('Y-m-d H:i:s',  date('Y-m-d H:i:s', time()));
        $yearEnd = DateTime::createFromFormat('Y-m-d H:i:s', date('Y-m-d H:i:s', strtotime(date("Y-m-d", time()) . " + 365 day")));
        return self::geteventsbetween($user_id, $yearStart, $yearEnd, $limit);
    }

    public static function geteventsbetween($user_id, $startDate, $endDate, $limit, $type = null)
    {
        $db = Zend_Registry::get("db");
        $timeZone = new DateTimeZone('GMT');
        $startDate->setTimezone($timeZone);
        $endDate->setTimezone($timeZone);
        $now = new DateTime();
        $now->setTimezone($timeZone);
		$whereType = ($type == null) ? '' : " AND description = '".$type."' ";
//        //only future events
//        $startDate = ($startDate > $now) ? $startDate : $now;
//        if ($endDate < $now){
//            return null;
//        }
        $date="date_time>='".$startDate->format('Y-m-d H:i:s')."' AND date_time<='".$endDate->format('Y-m-d H:i:s')."'";

        $sql="SELECT events.id,first_name,last_name,profile_pic, tags, created_user AS created_user_id,event_date,event_time,title,description,e_curated_description AS curated_description,location,
							 image AS event_image,ifnull(eventrsvp.invited_user,'') AS invited_user,RSVP_status,updated_date,events.id as event_id
							 FROM events
							 LEFT JOIN eventrsvp ON eventrsvp.event_id=events.id
							 JOIN users ON users.user_id=events.created_user
							 WHERE (created_user='".$user_id."' OR invited_user='".$user_id."') AND ".$date.$whereType." GROUP BY events.id ".$limit." ORDER BY date_time ASC";
//		var_dump($sql);exit;

         $results=$db->fetchAll($sql);

         $holiday_sql = "SELECT e.* FROM events e 
                        WHERE e.description IN ('holiday') AND e.created_user=0 AND e.event_date>='".$startDate->format('Y-m-d')."' AND e.event_date<='".$endDate->format('Y-m-d')."' ";

         $holidays=$db->fetchAll($holiday_sql);

         try {
              $timeZone = new DateTimeZone($_SESSION['time_zone']);
         } catch (Exception $e) {
         }

        foreach($results as $result){
            $result->time = self::convertToTimeZone($result->event_date, '12:00', $timeZone);
            $return[] = $result;
        }
 
        foreach($holidays as $result){
            $result->time = self::convertToTimeZone($result->event_date, (empty($result->event_time)) ? '12:00' : $result->event_time, $timeZone);
            $return[] = $result;
        }

        return $return;
    }

	// get user events for website
    public static function getEventDate($user_id)
    {
        $db = Zend_Registry::get("db");
        $sql = "SELECT e.id, e.event_date, e.event_time, e.title, e.location FROM events e
                LEFT JOIN eventrsvp er ON er.event_id=e.id
                WHERE (e.created_user='".$user_id."' OR er.invited_user='".$user_id."') AND e.event_date>=curdate()";
        $results=$db->fetchAll($sql);
        return $results;
    }

    // get user events for website calendar
    public static function getUpcomingEventsDates($user_id, $timeZone = null)
    {
        $now = date("Y-m-d");
        $db = Zend_Registry::get("db");
        $sql = "SELECT e.event_date, e.event_time FROM events e
                LEFT JOIN eventrsvp er ON er.event_id=e.id
                WHERE (e.created_user='".$user_id."' OR er.invited_user='".$user_id."') AND e.event_date>='".$now."' ";
        $results=$db->fetchAll($sql);
        foreach($results as $result){
            $eventDate = self::convertToTimeZone($result->event_date, $result->event_time, $timeZone);
            if (!empty($eventDate)){
                $return[] = $eventDate->format('Y-m-d');
            } else {
                $return[] =$result->event_date;
            }

        }
        return $return;
    }

    // get user month events for website calendar
    public static function getUpcomingMonthEventsCalendarDates($user_id, $month, $year, $timeZone = null)
    {
        $now = date("Y-m-d");
        $monthStart = DateTime::createFromFormat('Y-m-d H:i', $year.'-'.$month.'-01 00:00');
        $monthEnd = DateTime::createFromFormat('Y-m-d H:i:s', $year.'-'.$month.'-31 23:59:59');
        $db = Zend_Registry::get("db");
        $sql = "SELECT e.event_date, e.event_time FROM events e
                LEFT JOIN eventrsvp er ON er.event_id=e.id
                WHERE (e.created_user='".$user_id."' OR er.invited_user='".$user_id."') AND e.event_date>='".$now."' AND e.event_date>='".$monthStart->format('Y-m-d H:i:s')."' AND e.event_date<='".$monthEnd->format('Y-m-d H:i:s')."' ";
        $results=$db->fetchAll($sql);

        $return = array();
        foreach($results as $result){
            $eventDate = self::convertToTimeZone($result->event_date, $result->event_time, $timeZone);
            if (!empty($eventDate)){
                $return[] = $eventDate->format('Y-m-d');
            } else {
                $return[] =$result->event_date;
            }
        }

        $holiday_sql = "SELECT e.event_date, e.event_time FROM events e 
                        WHERE e.description IN ('holiday') AND e.created_user=0 AND e.event_date>='".$now."' AND e.event_date>='".$monthStart->format('Y-m-d')."' AND e.event_date<='".$monthEnd->format('Y-m-d')."' ";
         $holidays=$db->fetchAll($holiday_sql);

       foreach($holidays as $result){
            $eventDate = self::convertToTimeZone($result->event_date, $result->event_time, $timeZone);
            if (!empty($eventDate)){
                $return[] = $eventDate->format('Y-m-d');
            } else {
                $return[] =$result->event_date;
            }
        }

          //add holydays;
          /*
           $return = array_merge($return, self::getMonthHolidays($month, $year));
          */
        return $return;
    }

	// event description by event id
	public static function eventDescriptionById($event_id, $timeZone = null)
	{
		$db = Zend_Registry::get("db");
        $sql ="SELECT title,description,event_date,image,event_time,location,events.id AS event_id,created_user,event_type,
               (SELECT COUNT(RSVP_status) FROM eventrsvp WHERE RSVP_status='1' AND event_id='".$event_id."') AS going,
               (SELECT COUNT(RSVP_status) FROM eventrsvp WHERE RSVP_status='0' AND event_id='".$event_id."') AS maybe,
               (SELECT COUNT(RSVP_status) FROM eventrsvp WHERE RSVP_status='2' AND event_id='".$event_id."') AS notgoing,
               (SELECT COUNT(RSVP_status) FROM eventrsvp WHERE event_id='".$event_id."') AS invited
               FROM events LEFT JOIN eventrsvp ON events.id=eventrsvp.event_id WHERE events.id='".$event_id."' GROUP BY eventrsvp.event_id";
		$results=$db->fetchRow($sql);
        $eventDate = self::convertToTimeZone($results->event_date, $results->event_time, $timeZone);
        if(!empty($eventDate)){
            $results->event_date = $eventDate->format('m/d/y');
            $results->event_timeA = $eventDate->format('h:i A');
            $results->event_time = $eventDate->format('H:i');
        }
        return $results;
	}

	// event comments
	public static function eventComments($event_id)
	{
		$db = Zend_Registry::get("db");
		$results=$db->fetchAll("SELECT CONCAT(first_name,' ',last_name) AS name,profile_pic,comments, eventcomments.comment_date FROM eventcomments
								JOIN users on users.user_id=eventcomments.user_id WHERE event_id='".$event_id."' ORDER BY eventcomments.comment_date DESC LIMIT 3");
		return $results;
	}

	// event comments
	public static function getAllEventComments($event_id)
	{
		$db = Zend_Registry::get("db");
		$results=$db->fetchAll("SELECT CONCAT(first_name,' ',last_name) AS name,profile_pic,comments, eventcomments.comment_date, eventcomments.user_id FROM eventcomments
								JOIN users on users.user_id=eventcomments.user_id WHERE event_id='".$event_id."' ORDER BY eventcomments.comment_date DESC");
		return $results;
	}

	// count total comment of an event
	public static function totalCommentOfEvent($event_id)
	{
		$db = Zend_Registry::get("db");
		$results=$db->fetchOne("SELECT count(*) as total_comment FROM eventcomments WHERE event_id='".$event_id."'");
		return $results;
	}

	// count total comment of an event
	public static function getRsvpStatus($user_id,$event_id)
	{
		$db = Zend_Registry::get("db");
		$results=$db->fetchOne("SELECT RSVP_status FROM eventrsvp WHERE invited_user='".$user_id."' AND event_id='".$event_id."'");
		return $results;
	}

	// check invite option for friend
	public static function isCheckFriendEventInviteOpt($user_id,$event_id)
	{
		$db = Zend_Registry::get("db");
		$results=$db->fetchOne("SELECT id FROM eventrsvp WHERE invited_user='".$user_id."' AND event_id='".$event_id."'");
		return $results;
	}

	// get user events for website
	public static function getEventImage($event_id)
	{
		$db = Zend_Registry::get("db");
		$results=$db->fetchOne("SELECT image FROM events WHERE id='".$event_id."'");
		return $results;
	}



	// if exist fb event id
	public static function isExistFBEvent($event_id,$user_id)
	{
		$db = Zend_Registry::get("db");
		$results=$db->fetchOne("SELECT id FROM events where fb_event_id='".$event_id."' AND created_user='".$user_id."'");
		return $results;
	}

	// if exist fb birthday event id
	public static function isExistFBBirthdayEvent($fbid,$user_id, $birthday)
	{
		$db = Zend_Registry::get("db");
		$results=$db->fetchOne("SELECT id FROM events where fb_uid = '".$fbid."' AND created_user='".$user_id."' AND event_date='".$birthday."'");
		return $results;
	}

	// get event comments
	public static function getEventComments($event_id)
	{
		$db = Zend_Registry::get("db");
		$results=$db->fetchAll("SELECT id AS comment_id,comments,profile_pic,first_name,last_name,eventcomments.user_id AS user_id,comment_date FROM eventcomments
							    LEFT JOIN users ON users.user_id=eventcomments.user_id
							    WHERE event_id='".$event_id."' ORDER BY comment_date DESC");
		return $results;
	}

	// get latest comment by event id
	public static function getLatestEventComments($event_id,$last_comment_id)
	{
		$db = Zend_Registry::get("db");
		$results=$db->fetchAll("SELECT id AS comment_id,comments,profile_pic,first_name,last_name,eventcomments.user_id AS user_id,comment_date FROM eventcomments
							    LEFT JOIN users ON users.user_id=eventcomments.user_id
							    WHERE event_id='".$event_id."' AND id>'".$last_comment_id."' ORDER BY comment_date DESC");
		return $results;
	}

	// tag gift list for event
	public static function tagGiftForEvent($event_id)
	{
		//echo "<br>select item_id from taggifts where event_id='".$event_id."'";
		$db = Zend_Registry::get("db");
		$results=$db->fetchOne("select item_id from taggifts where event_id='".$event_id."'");
		return $results;
	}

	public static function istaggedGiftForEvent($event_id)
	{
		$db = Zend_Registry::get("db");
		$results=$db->fetchRow("select * from taggifts where event_id='".$event_id."'");
		return $results;
	}

	// get event title for create a tag
	// tag gift list for event
	public static function getEventTitle($event_id)
	{
		$db = Zend_Registry::get("db");
		$results=$db->fetchOne("select title from events where id='".$event_id."'");
		return $results;
	}

	// tag gift list for event
	public static function isbuyGiftForEvent($user_id,$item_id,$touser_id,$event_id)
	{
		$db = Zend_Registry::get("db");
		$results=$db->fetchOne("SELECT item_id FROM giftsexchanged WHERE user_id= '".$user_id."' AND item_id= '".$item_id."' AND receiver_id='".$touser_id."' AND event_id='".$event_id."'");
		return $results;
	}
	public static function isbuyGiftForFriendEvent($user_id,$item_id,$touser_id)
	{
		$db = Zend_Registry::get("db");
		$results=$db->fetchOne("SELECT item_id FROM giftsexchanged WHERE user_id= '".$user_id."' AND item_id= '".$item_id."' AND receiver_id='".$touser_id."' AND status='0' ");
		return $results;
	}
	public static function totalBuyGiftsForEvent($user_id,$touser_id,$event_id)
	{
		$db = Zend_Registry::get("db");
		$results=$db->fetchOne("SELECT count(item_id) AS total_purchase FROM giftsexchanged WHERE user_id= '".$user_id."' AND receiver_id='".$touser_id."' AND event_id='".$event_id."'");
		return $results;
	}

	// check received gift for event
	public static function checkReceivedGift($user_id,$item_id)
	{
		$db = Zend_Registry::get("db");
		/*$results=$db->fetchRow("SELECT giftsexchanged.event_id AS event_id,giftsexchanged.user_id AS sender_id,item_id,CONCAT(first_name,' ',last_name) AS name,title,giftsexchanged.status AS status
								FROM giftsexchanged	JOIN users ON users.user_id=giftsexchanged.user_id
								JOIN events ON events.id=giftsexchanged.event_id WHERE receiver_id='".$user_id."' AND item_id='".$item_id."'");
		*/

		$results=$db->fetchRow("SELECT giftsexchanged.event_id AS event_id, giftsexchanged.user_id AS sender_id,item_id,CONCAT(first_name,' ',last_name) AS name,giftsexchanged.status AS status
								FROM giftsexchanged	JOIN users ON users.user_id=giftsexchanged.user_id
								WHERE receiver_id='".$user_id."' AND item_id='".$item_id."'");

		return $results;
	}
	/* // is buy gift for the event by user
	public static function isBuyGiftForEvent($user_id,$event_id)
	{
		$db = Zend_Registry::get("db");
		$results=$db->fetchOne("SELECT item_id FROM giftsexchanged WHERE user_id= '".$user_id."' AND event_id='".$event_id."'");
		return $results;
	} */

	// going users
	public static function getGoingUsers($event_id)
	{
		$db = Zend_Registry::get("db");
		$results=$db->fetchOne("SELECT COUNT(RSVP_status) AS going FROM eventrsvp WHERE RSVP_status='1' AND event_id='".$event_id."'");
		return $results;
	}

	// going users
	public static function getMaybeUsers($event_id)
	{
		$db = Zend_Registry::get("db");
		$results=$db->fetchOne("SELECT COUNT(RSVP_status) AS maybe FROM eventrsvp WHERE RSVP_status='2' AND event_id='".$event_id."'");
		return $results;
	}

	// going users
	public static function getInvitedUsers($event_id)
	{
		$db = Zend_Registry::get("db");
		$results=$db->fetchOne("SELECT COUNT(RSVP_status) AS invited FROM eventrsvp WHERE event_id='".$event_id."'");
		return $results;
	}

	// going users
	public static function getEventCreatedUserIDByEventID($event_id)
	{
		$db = Zend_Registry::get("db");
		$results=$db->fetchOne("SELECT created_user FROM events where id='".$event_id."'");
		return $results;
	}

	public static function getEventNotification($user_id)
	{
		$cur_month=date('m');
		$cur_year=date('Y');
		$db = Zend_Registry::get("db");
		$results=$db->fetchAll("SELECT CONCAT(first_name,' ',last_name) AS name,profile_pic,created_user AS 			created_user_id,event_date,event_time,title,description,location,
	image AS event_image,ifnull(eventrsvp.invited_user,'') AS invited_user,RSVP_status,events.updated_date as updated_date,events.id as event_id FROM events LEFT JOIN eventrsvp ON eventrsvp.event_id=events.id JOIN users ON users.user_id=events.created_user
	WHERE invited_user ='".$user_id."' AND MONTH(event_date) ='".$cur_month."' AND YEAR(event_date) ='".$cur_year."' AND event_date>=curdate() AND notification_status='0' ORDER BY updated_date DESC");
		return $results;
	}

	public static function getEventNotificationHistory($user_id)
	{
		$cur_month=date('m');
		$cur_year=date('Y');
		$db = Zend_Registry::get("db");

		$results=$db->fetchAll("SELECT CONCAT(first_name,' ',last_name) AS name,profile_pic,created_user AS 			created_user_id,event_date,event_time,title,description,location,
		image AS event_image,ifnull(eventrsvp.invited_user,'') AS invited_user,RSVP_status,events.updated_date as updated_date,events.id as event_id FROM events LEFT JOIN eventrsvp ON eventrsvp.event_id=events.id JOIN users ON users.user_id=events.created_user
		WHERE invited_user ='".$user_id."' AND MONTH(event_date) ='".$cur_month."' AND YEAR(event_date) ='".$cur_year."' AND event_date>=curdate() ORDER BY updated_date DESC");
		return $results;
	}

	public static function getItemRequiredNo($user_id,$item_id)
	{
		$db = Zend_Registry::get("db");
		$results=$db->fetchOne("SELECT items_required_no FROM userwishlist WHERE user_id='".$user_id."' AND item_id='".$item_id."'");
		return $results;
	}

	public static function getBuyGiftList($user_id,$event_id)
	{
		$db = Zend_Registry::get("db");
		$results=$db->fetchAll("SELECT  item_name, wishlistitmes.id AS item_id, item_type, image, description, item_price, item_count, vendor_link,
								isbn, added_from, added_date, barcode_number FROM giftsexchanged
								JOIN wishlistitmes ON wishlistitmes.id = giftsexchanged.item_id WHERE giftsexchanged.user_id='".$user_id."' AND event_id='".$event_id."'");
		return $results;
	}

	/// Get upcoming event within 1 month
	public static function upcomingEvent($user_id)
	{
		$today = date('Y-m-d');
		$dateAfter1Month = date('Y-m-d', mktime(0,0,0, date('m'), date('d')+30, date('Y')));

		$sql = "SELECT DISTINCT(e.id), e.id As eventID, e.* FROM events AS e
					LEFT  JOIN eventrsvp AS r ON e.id = r.event_id
					WHERE (r.invited_user  = '".$user_id."' OR e.created_user  = '".$user_id."') AND e.event_date >= '".$today."' ORDER BY event_date LIMIT 0, 10";
		//echo 'tt:'.$sql;
		$db = Zend_Registry::get("db");
		$results=$db->fetchAll($sql);
		return $results;
	}

	///  Event's tagged item
	public static function taggedItem($itemStr)
	{
		$sql = "SELECT * FROM wishlistitmes WHERE id IN (".$itemStr.") LIMIT 0,3";
		//echo '<br>'.$sql;

		$db = Zend_Registry::get("db");
		$results=$db->fetchAll($sql);
		return $results;
	}

	/// total gifts exchanged
	public static function totalsendGift($user_id,$receiver_id)
	{
		$db = Zend_Registry::get("db");
		$results=$db->fetchOne("SELECT count(item_id) AS total_purchase FROM giftsexchanged WHERE user_id= '".$user_id."' AND receiver_id='".$receiver_id."' ");
		return $results;
	}

	// total gift sent by a user
	public static function giftSend($user_id)
	{
		$db = Zend_Registry::get("db");
		//$results=$db->fetchOne("SELECT count(item_id) AS total_purchase FROM giftsexchanged WHERE user_id= '".$user_id."'");
		$results=$db->fetchOne("SELECT count(g.item_id) AS total_purchase FROM giftsexchanged AS g, wishlistitmes AS p, users AS u WHERE g.user_id= '".$user_id."' AND g.item_id = p.id AND g.receiver_id = u.user_id");
		return $results;
	}

	///list of gifts
	public static function giftGiven($user_id)
	{
		$db = Zend_Registry::get("db");

		$sql = "SELECT CONCAT(u.first_name, ' ', u.last_name) AS receiver_name, u.first_name, u.last_name, g.item_id, g.purchased_date, p.item_name, p.image FROM giftsexchanged AS g, wishlistitmes AS p, users AS u WHERE g.user_id= '".$user_id."' AND g.item_id = p.id AND g.receiver_id = u.user_id ORDER BY g.purchased_date DESC";

		$results=$db->fetchAll($sql);
		return $results;
	}

    public static function getGiftGiven($user_id, $except)
    {
        $db = Zend_Registry::get("db");

        $sql = "SELECT w.*, w.id AS item_id FROM giftsexchanged AS g, wishlistitmes AS w, users AS u WHERE g.user_id= '".$user_id."' AND g.item_id = w.id AND g.receiver_id = u.user_id AND w.id != '".$except."' ORDER BY g.purchased_date DESC";

        $results=$db->fetchAll($sql);
        return $results;
    }

	// total gift recieved by a user
	public static function giftGet($user_id)
	{
		$db = Zend_Registry::get("db");
		//$results=$db->fetchOne("SELECT count(item_id) AS total_get FROM giftsexchanged WHERE receiver_id = '".$user_id."'");
		$results=$db->fetchOne("SELECT count(g.item_id) AS total_get
		                            FROM giftsexchanged AS g
		                            LEFT JOIN users AS u ON g.user_id = u.user_id
		                            LEFT JOIN wishlistitmes AS p ON	g.item_id = p.id
		                            WHERE g.receiver_id= '".$user_id."' AND g.status = '1'");
		return $results;
	}

	public static function giftRecived($user_id)
	{
		$db = Zend_Registry::get("db");

		$sql = "SELECT CONCAT(u.first_name, ' ', u.last_name) AS sender_name, u.first_name, u.last_name, g.item_id,
		            g.purchased_date, p.item_name, p.image
		            FROM giftsexchanged AS g
		            LEFT JOIN users AS u ON u.user_id = g.user_id
		            LEFT JOIN wishlistitmes AS p ON g.item_id = p.id
		            WHERE g.receiver_id= '".$user_id."' ORDER BY g.purchased_date DESC";

		//$sql = "SELECT g.item_id, g.purchased_date, p.item_name, p.image FROM giftsexchanged AS g, wishlistitmes AS p WHERE g.receiver_id= '".$user_id."' AND g.item_id = p.id";
		$results=$db->fetchAll($sql);
		return $results;
	}

    public static function getGiftRecived($user_id, $except)
    {
        $db = Zend_Registry::get("db");

        $sql = "SELECT w.*, w.id AS item_id FROM giftsexchanged AS g, wishlistitmes AS w, users AS u WHERE g.receiver_id= '".$user_id."' AND g.item_id = w.id AND g.user_id = u.user_id AND w.id != '".$except."' ORDER BY g.purchased_date DESC";

        $results=$db->fetchAll($sql);
        return $results;
    }

	/// check if already some one has buy this item
	public static function isbuyGiftForUser($user_id,$item_id)
	{
		$db = Zend_Registry::get("db");
		$results=$db->fetchOne("SELECT item_id FROM giftsexchanged WHERE item_id= '".$item_id."' AND receiver_id='".$user_id."'");
		return $results;
	}

	public static function isReceiveGiftForUser($user_id,$item_id)
	{
		$db = Zend_Registry::get("db");
		//echo "<br> SELECT item_id FROM giftsexchanged WHERE item_id= '".$item_id."' AND receiver_id='".$user_id."' AND status = '1'";
		$results=$db->fetchOne("SELECT item_id FROM giftsexchanged WHERE item_id= '".$item_id."' AND receiver_id='".$user_id."' AND status = '1'");

		return $results;
	}

	public static function isalreadybuyGift($user_id,$item_id,$touser_id)
	{
		$db = Zend_Registry::get("db");
		$results=$db->fetchOne("SELECT item_id FROM giftsexchanged WHERE user_id= '".$user_id."' AND item_id= '".$item_id."' AND receiver_id='".$touser_id."'");
		return $results;
}
	public static function isalreadybuyGiftforfriend($user_id,$item_id,$touser_id)
	{
		$db = Zend_Registry::get("db");
		$results=$db->fetchOne("SELECT item_id FROM giftsexchanged WHERE user_id= '".$user_id."' AND item_id= '".$item_id."' AND receiver_id='".$touser_id."' and status='0'");
		return $results;
	}

	/// check if user is already invited for any event
	public static function chkInvitedUser($event_id,$sender_id,$invitee)
	{
		$db = Zend_Registry::get("db");
		$results=$db->fetchOne("SELECT id FROM eventrsvp WHERE user_id= '".$sender_id."' AND invited_user = '".$invitee."' AND event_id ='".$event_id."'");
		return $results;
	}

	/// get tag id for the event
	public static function getTagID($tagName)
	{
		$db = Zend_Registry::get("db");
		//$results=$db->fetchOne("SELECT id FROM category WHERE name  = '".$tagName."' ");
		$sql = "SELECT id FROM category WHERE name  LIKE ".$tagName;
		//echo $sql;
		$results=$db->fetchOne($sql);
		return $results;
	}

	/// upcoming events for App
	public static function upcomingEventApp($user_id)
	{
		$today = date('Y-m-d');
		$dateAfter1Month = date('Y-m-d', mktime(0,0,0, date('m'), date('d')+30, date('Y')));

		$sql = "SELECT first_name,last_name,profile_pic,created_user AS created_user_id,event_date,event_time,title,description,location,
							image AS event_image,ifnull(eventrsvp.invited_user,'') AS invited_user,RSVP_status, events.updated_date as updated_date,events.id as event_id
							FROM events
							LEFT JOIN eventrsvp ON eventrsvp.event_id=events.id
							JOIN users ON users.user_id=events.created_user
							WHERE (created_user='".$user_id."' OR invited_user='".$user_id."') AND CONCAT(events.id,' ',events.updated_date)
							 AND event_date  BETWEEN '".$today."' AND '".$dateAfter1Month."' ORDER BY event_date
							";
		//echo $sql;
		$db = Zend_Registry::get("db");
		$results=$db->fetchAll($sql);
		return $results;

	}

	/// upcoming events for App
	public static function eventDetailApp($event_id, $user_id)
	{

		$sql = "SELECT first_name,last_name,profile_pic,created_user AS created_user_id,event_date,event_time,title,description,location,
							image AS event_image,ifnull(eventrsvp.invited_user,'') AS invited_user,RSVP_status, events.updated_date as updated_date,events.id as event_id
							FROM events
							LEFT JOIN eventrsvp ON eventrsvp.event_id=events.id
							JOIN users ON users.user_id=events.created_user
							WHERE events.id = '".$event_id."' AND eventrsvp.invited_user='".$user_id."'";
		//echo $sql.'<br>';
		$db = Zend_Registry::get("db");
		$results=$db->fetchRow($sql);
		return $results;

	}

	public static function isbuyGiftItem($user_id,$item_id,$touser_id)
	{
		$db = Zend_Registry::get("db");
		$results=$db->fetchOne("SELECT item_id FROM giftsexchanged WHERE user_id= '".$user_id."' AND item_id= '".$item_id."' AND receiver_id='".$touser_id."'");
		//echo "SELECT item_id FROM giftsexchanged WHERE user_id= '".$user_id."' AND item_id= '".$item_id."' AND receiver_id='".$touser_id."'";
		return $results;
	}

	/// check already exist birthday events:
	public static function getExistBirthdayEvent($user_id, $eventYear)
	{
		$db = Zend_Registry::get("db");
		$sql = "SELECT id FROM events WHERE created_user = '".$user_id."' AND YEAR(event_date) = '".$eventYear."' AND event_type = 'Birthday'";
		$results=$db->fetchOne($sql);
		return $results;
	}

        // get user events for each item
        public static function getItemEvents($user_id, $item_id, $threeMonths = null)
        {
            $timeInterval = ($threeMonths) ? "DATE_ADD(curdate(), INTERVAL 3 MONTH)" : "DATE_SUB(DATE_ADD(curdate(), INTERVAL 1 YEAR), INTERVAL 1 DAY)";
            $db = Zend_Registry::get("db");
            $sql = "SELECT e.id AS event_id, e.title, ei.item_id AS selected
                                FROM events e
								LEFT JOIN event_items ei ON ei.event_id = e.id AND ei.item_id = '" . $item_id . "'
								WHERE e.created_user='".$user_id."' AND event_date>=curdate() AND event_date <= " . $timeInterval . "
								GROUP BY e.id
								ORDER BY e.date_time";
            $results=$db->fetchAll($sql);
            return $results;
        }

        // Get all events created by user user_id
        public static function getUserCreatedEvents($user_id)
        {
                $db = Zend_Registry::get("db");
                $timeInterval = "DATE_SUB(DATE_ADD(curdate(), INTERVAL 1 YEAR), INTERVAL 1 DAY)";
                $sql = "SELECT id, title, date_time FROM events WHERE created_user = '".$user_id."' AND event_date>=curdate() AND event_date <= " . $timeInterval . " ORDER BY date_time";
                $results=$db->fetchAll($sql);
                return $results;
        }

   public static function getFeedPageEvents($userID)
        {
            $db = Zend_Registry::get("db");
            $sql = "SELECT friend_id
                    FROM usersfriend
                    WHERE (user_id = '".$userID."' AND status = '1')";
           
            //$twomonthsevents = MONTH(curdate()) + 2;
              
            $follow = $db->fetchAll($sql);
            $friendlist = array();
            
            for($i=0; $i < count($follow); $i++)
            {   
                    $friendlist[] = $follow[$i]->friend_id;
            }
            $friendlistStr = implode(",",$friendlist);
            if(count($friendlist) > 0)
            {   

                $sql = "SELECT id, created_user, title, date_time FROM events WHERE created_user IN (".$friendlistStr.") AND event_date>=curdate() AND event_date <= DATE_ADD(curdate(), INTERVAL 3 MONTH) ORDER BY date_time";
                $results=$db->fetchAll($sql);
            }

           /* Check if you are invited */
              if (count($results) > 0) {
               $result = [];
                foreach($results as $item){
                          $invited = event::isCheckFriendEventInviteOpt($userID, $item->id);
                          if($invited)
                          {
                              $created_user = users::getUserDetails($item->created_user);
                              if($created_user) {
                                 $item->title = $item->title . ' - ' . $created_user->first_name;
                              }
                              $result[] = $item;
                          }
                }
                return $result;
             }
             return null;
        }

        public static function convertToTimeZone($date, $time, $timeZone)
        {
            date_default_timezone_set('GMT');
            $eventDate = DateTime::createFromFormat('Y-m-d H:i',$date.' '.$time);
            if (empty($eventDate)){
                $eventDate = DateTime::createFromFormat('Y-m-d H:ia',$date.' '.$time);
            }
            if (empty($eventDate)){
                $eventDate = DateTime::createFromFormat('Y-m-d H:iA',$date.' '.$time);
            }
            if (empty($eventDate)){
                return null;
            } else {
                $eventDate->setTimezone($timeZone);
                return $eventDate;
            }
        }

        public static function getMonthHolidays($month, $year)
        {
            $holidays = ['2014-10-31', '2014-11-27', '2014-12-25', '2015-01-01', '2015-02-14', '2015-10-31', '2015-11-26', '2015-12-25', '2016-01-01', '2016-02-14', '2016-11-24', '2016-12-25'];
            $return = array();

            foreach($holidays as $holiday){
                $holidayDateTime = strtotime($holiday. ' 00:01');
                $monthStart = strtotime($year.'-'.$month.'-01 00:00');
                $monthEnd = strtotime($year.'-'.$month.'-'.date("t",$monthStart).' 23:59:59');
                if (($monthStart < $holidayDateTime) && ($holidayDateTime < $monthEnd))
                    $return[] = $holiday;
            }
            return $return;
        }

        public static function getFacebookEvents($fbUserId, $accessToken)
        {

			$db 	= Zend_Registry::get("db");
			$db->beginTransaction();
			try	{
				$obj = facebookquery::getQuery($accessToken,$fbUserId."/events","name,start_time,end_time,rsvp_status,description,owner");

				if (empty($obj->data)){
					return 'There are no events to import';
				}
				foreach($obj->data as $event){
					if ($event->rsvp_status = 'attending') {
						$event_date_time=explode("T",$event->start_time);
						$today=date('Y-m-d');
						if($event_date_time[0]>=$today) {
							$event_date_time[1] = ($event_date_time[1] == "") ? "12:00" : substr($event_date_time[1], 0, 5);
							date_default_timezone_set($_SESSION['time_zone']);
							$eventDate = DateTime::createFromFormat("Y-m-d H:i", $event_date_time[0] . ' ' . $event_date_time[1]);
							$eventDate = timezone::setGMTFromUserTimeZone($eventDate);
							$now = new DateTime();
							if ($eventDate > $now) {
								$eventTime = $eventDate->format('H:i');
								$dateTime = $eventDate->format('Y-m-d H:i:s');
								$eventDate = $eventDate->format('Y-m-d');
								$datetime = date("Y-m-d H:i:s");
								$hash = strtoupper(substr(md5(time()), 0, 30)) . mt_rand(1, 10000);

								$eventResult = facebookquery::getQuery($accessToken, $event->id, "picture.width(217).height(217)");
								$imageUrl = $eventResult->picture->data->url;
								$userResult = facebookquery::getQuery($accessToken, $fbUserId, "picture.width(270).height(270)");

								if ($imageUrl != "" && $imageUrl != $userResult->picture->data->url) {
									$imagename = explode(".", $imageUrl);
									$tmp = $imagename[count($imagename) - 1];
									$imagename = explode("?", $tmp);
									$image_name = "Event_Img_" . $hash . "." . $imagename[0];
									$rr[] = $image_name;
									$target_path = $_SERVER["DOCUMENT_ROOT"] . "/event_images/" . urldecode($image_name);
									$thumb_path = $_SERVER["DOCUMENT_ROOT"] . "/event_images/thumb/" . urldecode($image_name);
									$file = new fileupload();
									$file->fileResize($imageUrl, $target_path, $thumb_path, 620, 462, 270);
								} else {
									$image_name = 'default-event-pic.jpg';
								}

								$event_id = event::isExistFBEvent($event->id,$_SESSION["user_id"]);

								if($event_id){
									$update_event=array("event_date"=>$eventDate,"event_time"=>$eventTime,
										"title"=>$event->name,"description"=>(empty($event->description)) ? ' ' : $event->description,
										"location"=> '',
										"image"=>$image_name,"updated_date"=>$datetime);
									$where[] = "id = '" . $event_id . "'";
									$db->update('events',$update_event,$where);
								}
								else{
									$insert_event=array("fb_event_id"=>(empty($event->id)) ? '' : $event->id,
										"created_user"=>$_SESSION["user_id"],
										"event_date"=>$eventDate,
										"event_time"=>$eventTime,
										"date_time"=>$dateTime,
										"title"=>$event->name,
										"description"=>(empty($event->description)) ? ' ' : $event->description,
										"e_curated_description"=>'',
										"location"=> '',
										"image"=>$image_name,
										"created_date"=>$datetime,
										"updated_date"=>$datetime,
										"event_type"=>'Event',
										"fb_uid"=>$fbUserId,
										"tags"=>''
									);
									$db->insert('events',$insert_event);
									$event_id = $db->lastInsertId();
//									var_dump($event_id);exit;
								}
							}
						}
					}
				}
				$db->commit();
			} catch(Exception $e) {
				$db->rollBack();
				return $e->getMessage();
			}
        }
	
		
	
	

        public static function addBirthdayEvents($user_id, $birthday, $name,  $fb_id = null)
        {
            $logger = Zend_Registry::get("logger");
            $db 	= Zend_Registry::get("db");
            try {
                $event_datetmp = date('Y-m-d', strtotime($birthday));
                $edtArr = explode('-', $event_datetmp);
                $birthdayYears[0] = date('Y');
                $birthdayYears[1] = date('Y', strtotime('+1 year'));
                $birthdayYears[2] = date('Y', strtotime('-1 year'));
                $eventIds= array();
                foreach($birthdayYears as $birthdayYear){
                    $event_date = $birthdayYear . '-' . $edtArr[1] . '-' . $edtArr[2];
                    $event_name = $name . ' - Birthday';
                    
                    if($fb_id){
                        $event_id = event::isExistFBBirthdayEvent($fb_id, $user_id, $event_date);
                    }
                    $event_id = event::isExistFBBirthdayEvent($fb_id, $user_id, $event_date);
                    if ($event_id) {
                        $datetime = date('Y-m-d H:i:s');
                        $update_event = array(
                            "event_date" => $event_date,
                            "event_time" => "12:00",
                            "date_time" => $event_date.' 12:00:00',
                            "title" => $event_name,
                            "event_type" => "Birthday",
                            "updated_date" => $datetime,
                        );
                        //update event
                        $db->update('events', $update_event, 'id = ' . $event_id);
                    } else {
                        // create event                        
                        $event_id = self::addEvent($user_id, $event_date, $event_name, 'Birthday', $fb_id);
                    }
                    $eventIds[] = $event_id;
                }

                $friendlist = usersfriend::getFollowList($user_id);
                //automatically invite friends in user's birthday event
                //get friends
                for ($f = 0; $f < count($friendlist); $f++) {
                    foreach($eventIds as $event_id){
                        // check for already invited
                        $chkInvite = event::isCheckFriendEventInviteOpt($friendlist[$f]->friend_id, $event_id);
                        if (!$chkInvite) {
                            $invite_friend = array("user_id" => $user_id, "invited_user" => $friendlist[$f]->friend_id, "event_id" => $event_id, "RSVP_status" => '3', 'notification_status' => 1);
                            $db->insert('eventrsvp', $invite_friend);
                        }
                    }
                }
            }catch (Exception $e) {
                $logger->debug($e);
                return $e->getMessage();
            }
        }

	public static function addAnniversaryEvents($user_id, $anniversary, $event_name,  $fb_id = null)
	{
		$logger = Zend_Registry::get("logger");
		$db 	= Zend_Registry::get("db");
		try {
			$event_datetmp = date('Y-m-d', strtotime($anniversary));
			$edtArr = explode('-', $event_datetmp);
            $anniversaryYears[0] = date('Y');
            $anniversaryYears[1] = date('Y', strtotime('+1 year'));
            $anniversaryYears[2] = date('Y', strtotime('-1 year'));
			$eventIds= array();
			foreach($anniversaryYears as $anniversaryYear){
				$event_date = $anniversaryYear . '-' . $edtArr[1] . '-' . $edtArr[2];
				$eventIds[] = self::addEvent($user_id, $event_date, $event_name, 'Birthday', $fb_id);
			}

			$friendlist = usersfriend::getFollowList($user_id);
			//automatically invite friends in user's birthday event
			//get friends
			for ($f = 0; $f < count($friendlist); $f++) {
				foreach($eventIds as $event_id){
					// check for already invited
					$chkInvite = event::isCheckFriendEventInviteOpt($friendlist[$f]->friend_id, $event_id);
					if (!$chkInvite) {
						$invite_friend = array("user_id" => $user_id, "invited_user" => $friendlist[$f]->friend_id, "event_id" => $event_id, "RSVP_status" => '3', 'notification_status' => 1);
						$db->insert('eventrsvp', $invite_friend);
					}
				}
			}
		}catch (Exception $e) {
			$logger->debug($e);
			return $e->getMessage();
		}
	}
	
	public static function addEvent($user_id, $eventDate, $eventName, $type, $fb_id = null) {
        $logger = Zend_Registry::get("logger");
        $db 	= Zend_Registry::get("db");
        try {
            $event_date = date('Y-m-d', strtotime($eventDate));

            $datetime = date('Y-m-d H:i:s');
            $event = array(
                "fb_event_id" => '',
                "fb_uid" => ($fb_id) ? $fb_id : 0,
                "created_user" => $user_id,
                "event_date" => $event_date,
                "event_time" => "12:00",
                "date_time" => $event_date.' 12:00:00',
                "title" => $eventName,
                "description" => $type,
                "location" => '',
                "image" => '',
                "created_date" => $datetime,
                "updated_date" => $datetime,
                "event_type" => "Birthday",
            );

            // create event
            $db->insert('events', $event);
            
            return $db->lastInsertId();
            
        }catch (Exception $e) {
            $logger->debug($e);
            return $e->getMessage();
        }
	}

           public static function eventcuratorcuratedinfo($eventId, $curatedDescription) {
                $db = Zend_Registry::get("db");

		$data = array("e_curated_description" => $curatedDescription);
                $where[] = "id = '" . $eventId . "'";
                if(isset($eventId) && $eventId != '') {
                        // update curated_description;
                        try {
                                $db->beginTransaction();
                                $db->update('events', $data, $where);
				$db->commit();
                                return true;
                        } catch (Exception $e) {
                                throw $e;
                        }
                }
                return false;
        }

         public static function eventcuratorcuratedtitle($eventId, $curatedDescription) {
                $db = Zend_Registry::get("db");

                $data = array("title" => $curatedDescription);
                $where[] = "id = '" . $eventId . "'";
                if(isset($eventId) && $eventId != '') {
                        // update curated_description;
                        try {
                                $db->beginTransaction();
                                $db->update('events', $data, $where);
                                $db->commit();
                                return true;
                        } catch (Exception $e) {
                                throw $e;
                        }
                }
                return false;
        }

        public static function eventcuratorcuratedtags($eventId, $curatedDescription) {
                $db = Zend_Registry::get("db");

                $data = array("tags" => $curatedDescription);
                $where[] = "id = '" . $eventId . "'";
                if(isset($eventId) && $eventId != '') {
                        // update curated_description;
                        try {
                                $db->beginTransaction();
                                $db->update('events', $data, $where);
                                $db->commit();
                                return true;
                        } catch (Exception $e) {
                                throw $e;
                        }
                }
                return false;
        }
	
	public static function updateEventDate($id, $date) {
		$logger = Zend_Registry::get("logger");
		$db 	= Zend_Registry::get("db");
		try {
			$newDate = array(
				"event_date" => date('Y-m-d', strtotime($date)),
			);
			$db->update('events',$newDate,'id = '.$id);
			return true;

		}catch (Exception $e) {
			$logger->debug($e);
			return $e->getMessage();
		}
	}

}///class
