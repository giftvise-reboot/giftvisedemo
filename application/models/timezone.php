<?php
/*------------------------------------------------------------------ 
 * Copyright (c) 2013-2014 by Giftvise, Inc.
 * All rights reserved. 
 *------------------------------------------------------------------
 */
?>

<?php
class timezone
{
    public static function setGMTFromUserTimeZone($time)
    {
        $timeZone = new DateTimeZone('GMT');
        date_default_timezone_set('GMT');
        return $time->setTimezone($timeZone);
    }
}
