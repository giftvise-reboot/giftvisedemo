<?php
/*------------------------------------------------------------------ 
 * Copyright (c) 2013-2014 by Giftvise, Inc.
 * All rights reserved. 
 *------------------------------------------------------------------
 */
?>

<?php
class notifications
{
    /*
     * Add welcome notification
     */
    public static function addWellcomeNotification($user_id)
    {
        $logger = Zend_Registry::get("logger");
        $db 	= Zend_Registry::get("db");
        try{
            //welcome notification
            $actity_data['sender'] = 0;
            $actity_data['receiver'] = $user_id;
            $actity_data['activity_type'] = 'Wellcome';
            $actity_data['activity_date'] = date('Y-m-d H:i:s');
            $actity_data['item_id'] = '0';
            $actity_data['event_id'] = '0';
            $actity_data['deleted_user'] = '0';
            $actity_data['new'] = true;
            $db->insert('activity', $actity_data);
        }  catch (Exception $e) {
            $logger->debug($e);
            return $e->getMessage();
        }

    }

    /*
     * Count new notifications
     */
    public static function countnewnotifications()
    {
        $db = Zend_Registry::get("db");
        $sql = "SELECT	COUNT(new) FROM	activity WHERE (receiver = '".$_SESSION['user_id']."' AND deleted_user != '".$_SESSION['user_id']."' AND new = '1')";
//        echo $sql;exit;
        return $db->fetchOne($sql);
    }

    /*
     * Notification/Activity log
     */
    public static function activitylog($lastfive = null)
    {
        $activity = self::getNotifications($_SESSION['user_id'], $lastfive);

        $activityLog = array();
        for ($i = 0; $i < count($activity); $i++) {
            $listA = array();

            $sender = users::getUserDetails($activity[$i]->sender);
            $event_title = event::getEventTitle($activity[$i]->event_id);
            $receiver = users::getUserDetails($activity[$i]->receiver);
            $item_name=wishlistitmes::getProductName($activity[$i]->item_id);

            if($activity[$i]->activity_type == 'Follow')
            {
                /// check status it confirm or not
                $isfollower = usersfriend::checkExistFriends($activity[$i]->sender, $activity[$i]->receiver);
                if ($isfollower->status == 0) {
                    $listA['fstatus'] = 'P';
                } else {
                    $listA['fstatus'] = '';
                }
            } else {
                $listA['fstatus'] = '';
            }

            $listA['id'] = $activity[$i]->id;
            $listA['item_id'] = $activity[$i]->item_id;
            $listA['event_id'] = $activity[$i]->event_id;
            $listA['item_name'] = $item_name;
            $listA['event_title'] = $event_title;
            $listA['sender_id'] = $activity[$i]->sender;
            $listA['sender_name'] = $sender->first_name.' '.substr($sender->last_name, 0,1);
            $listA['sender_image'] = $sender->profile_pic;

            $listA['receiver_id'] = $activity[$i]->receiver;
            $listA['receiver_name'] = $receiver->first_name.' '.$receiver->last_name;
            $listA['receiver_image'] = $receiver->profile_pic;


            $listA['activity_type'] = $activity[$i]->activity_type;
            $listA['activity_time'] = $activity[$i]->activity_date;

            $activityLog[] = $listA;
        }
        return $activityLog;
    }

    // get user's notifications
    public static function getNotifications($user_id, $lastfive)
    {
        $db = Zend_Registry::get("db");
        $limit = ($lastfive) ? "0,5" : "5, 18446744073709551615";
        $sql = "SELECT	* FROM	activity WHERE receiver = '".$user_id."' AND sender <> '".$user_id."'  AND deleted_user != '".$user_id."' 
                AND deleted_user != '".$user_id."' AND activity_type IN 
                ('comment item', 'recommend item', 'Follow', 'Invitation', 'Ask Permission', 'Wellcome') ORDER BY id DESC LIMIT ".$limit." ";
//        echo $sql; exit;
        return $db->fetchAll($sql);
    }

        // get user's notifications
    public static function getItemRecommendActivity($user_id, $itemId)
    {
        $db = Zend_Registry::get("db");
        $limit = "0, 18446744073709551615";
        $sql = "SELECT  * FROM  activity WHERE receiver = '".$user_id."' AND sender <> '".$user_id."'  AND deleted_user != '".$user_id."' 
                AND item_id = '".$itemId."' AND activity_type IN 
                ('recommend item') ORDER BY id DESC LIMIT ".$limit." ";
        return $db->fetchAll($sql);
    }

    public static function removelastactivity($user, $friend)
    {
        $sql = "UPDATE activity a
                    JOIN usersfriend uf ON uf.notify = a.id
                SET a.deleted_user = '" . $friend . "'
                WHERE (uf.user_id = '" . $user . "' AND uf.friend_id = '" . $friend . "') AND (uf.notify <> '0' OR uf.notify <> '1') ";

        $db = Zend_Registry::get("db");
        $result = $db->query($sql);
        return $result;
    }

    public static function removeaskactivity($user, $friend)
    {
        $sql = "UPDATE activity
                SET deleted_user = '" . $friend . "'
                WHERE sender = '" . $user . "' AND receiver = '" . $friend . "' ";

        $db = Zend_Registry::get("db");
        $result = $db->query($sql);
        return $result;
    }

    /*
     * Count notifications
     */
    public static function notificationscount()
    {
        $db = Zend_Registry::get("db");
        $sql = "SELECT	COUNT(id) FROM	activity WHERE (receiver = '".$_SESSION['user_id']."' AND sender <> '".$_SESSION['user_id']."' AND deleted_user != '".$_SESSION['user_id']."' AND activity_type IN ('recommend item', 'Follow', 'Unfollow', 'Invitation', 'Ask Permission', 'Wellcome'))";
        //echo $sql;
        return $db->fetchOne($sql);
    }
    

    /*
     *  Reset notification counter
     */
    public static function resetcounter()
    {
        $sql = "UPDATE activity SET new = NULL WHERE receiver = '".$_SESSION['user_id']."' ";
        $db = Zend_Registry::get("db");
//        echo $sql; exit;
        return $db->query($sql);
    }

    /*
     * Event notifications
     */
    public static function eventnotifications()
    {
        $db = Zend_Registry::get("db");
        $sql=$db->query("
            SELECT CONCAT(first_name,' ',last_name) AS name,profile_pic,created_user AS created_user_id,event_date,event_time,title,
                description,location,image AS event_image,ifnull(eventrsvp.invited_user,'') AS invited_user,RSVP_status,
                events.updated_date as updated_date,events.id as event_id
                FROM events
                LEFT JOIN eventrsvp ON eventrsvp.event_id=events.id
                JOIN users ON users.user_id=events.created_user
                WHERE invited_user ='".$_SESSION["user_id"]."' AND event_date>=curdate() AND notification_status='0'
                ORDER BY updated_date DESC");
        echo $sql; exit;
        return $sql->fetchAll();
    }


}
