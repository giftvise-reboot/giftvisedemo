<?php
/*------------------------------------------------------------------ 
 * Copyright (c) 2013-2014 by Giftvise, Inc.
 * All rights reserved. 
 *------------------------------------------------------------------
 */
?>

<?php

class users extends Zend_Db_Table_Abstract
{			
	public static function get($userID)
	{
		$db = Zend_Registry::get("db");
		
		$result = $db->fetchRow("
			SELECT 		*
			FROM		users
			WHERE		user_id 		= ?",
			$userID
		);
		return $result;
	}
	
	public static function getfromhash($userID)
	{
		$db = Zend_Registry::get("db");
		
		$result = $db->fetchRow("
			SELECT 		*
			FROM		users
			WHERE		hash 		= ?",
			$userID
		);	
		return $result;
	}
	
	public static function getUserDetails($user_id)
	{
		$db = Zend_Registry::get("db");
		$result = $db->fetchRow("
			SELECT 		*
			FROM		users
			WHERE		user_id	= ?",
			$user_id
		);	
		return $result;
	}
	
	
	public static function getAllByType($userType)
	{
		$db = Zend_Registry::get("db");
		
		$result = $db->fetchAll("
			SELECT 		*
			FROM		users
			WHERE		userType 		= ?",
			$userType
		);
		
		return $result;
	}
       
        public static function getAllforcurate()
        {
                $db = Zend_Registry::get("db");

                $result = $db->fetchAll("
                        SELECT          *
                        FROM            users
                        ORDER BY user_id DESC LIMIT 0, 300"
                );
                return $result;
        }
 
        public static function searchUsersByName($name)
        {
            $db = Zend_Registry::get("db");
	    $sql = "SELECT       user_id, first_name, last_name, created_date, profile_pic 
                    FROM         users
                    WHERE        first_name LIKE '". $name . "'" .
                    " OR           last_name LIKE '" . $name . "'" .
                    " OR           twitter_name LIKE '". $name . "'";
            $result = $db->fetchAll ($sql);
            
            return $result;
        }

        public static function searchUsersByFullName($fname, $lname)
        {
            $db = Zend_Registry::get("db");
            $sql = "SELECT       user_id, first_name, last_name, created_date, profile_pic 
                    FROM         users
                    WHERE        (first_name LIKE '". $fname . "'" .
                    " AND           last_name LIKE '" . $lname . "')" .
                    " OR (last_name LIKE '". $fname . "' AND first_name LIKE '" . $lname . "')";
            $result = $db->fetchAll ($sql);

            return $result;
        }
	
	public static function load($userID)
	{
		$user = self::get($userID);
		$_SESSION["user"] = self::objectToArray($user);
		
		//Save read time
		$_SESSION["user"]['lastReadTime'] = time();
	}
	
	public static function reload()
	{
		//Reload ACL if last read time was 12 or more hours ago
		if(time() - $_SESSION["user"]['lastReadTime'] >= 43200)
		{
			//Load user
			users::load($_SESSION["user"]['userID']);
			
			//Force session sync
			session::forceWriteToDB();
			
			//Log out if user no longer active
			if($_SESSION["user"]['status'] != "active"){
				PS_Util::redirect("/auth/logout");
			}
		}
	}
	
	public static function getName($userID)
	{
		$db = Zend_Registry::get("db");	
		$result = $db->fetchOne("
			SELECT 		CONCAT(first_name,' ',last_name) AS fullName
			FROM		users
			WHERE		user_id 	= ?",
			$userID
		);
		return $result;
	}
	
	public static function getUserUDID($userID)
	{
		$db = Zend_Registry::get("db");	
		$result = $db->fetchRow("
			SELECT 		udid,device_type
			FROM		users
			WHERE		user_id 	= ?",
			$userID
		);
		return $result;
	}

	public static function checkExistsTwittername($twitter_name){
		$db = Zend_Registry::get("db");	
		$result = $db->fetchOne("
			SELECT 		user_id
			FROM		users
			WHERE		twitter_name = ?",
			$twitter_name
		);
		return $result;	
	}
	public static function checkExistsTwitterID($twitter_id){
		$db = Zend_Registry::get("db");	
		$result = $db->fetchOne("
			SELECT 		user_id
			FROM		users
			WHERE		twitter_id = ?",
			$twitter_id
		);
		
		return $result;	
	}
	public static function checkExistsUsername($username, $userid=0)
	{
		$db = Zend_Registry::get("db");
		return $db->fetchOne("
			SELECT	COUNT(userID)
			FROM	users
			WHERE	username = ? AND userID != ?",
			array($username, $userid)
		);
	}
        
	public static function checkExistsEmail($email)
	{
		$db = Zend_Registry::get("db");
		return $db->fetchOne("
			SELECT	user_id
			FROM	users
			WHERE	email_id = '".$email."' OR facebook_email = '".$email."'"
		);
	}
	
	public static function checkExistsFBid($fb_id)
	{
		$db = Zend_Registry::get("db");
		return $db->fetchOne("
			SELECT	user_id
			FROM	users
			WHERE	facebook_id = ?",
			$fb_id
		);
	}

        public static function checkExistsFirstName($fname, $lname)
        {
                $db = Zend_Registry::get("db");
                return $db->fetchOne("
                        SELECT  user_id
                        FROM    users
                        WHERE   first_name = ? AND last_name = ?",
                        array($fname, $lname)
                );
        }

    public static function checkImageUserUpdated($userId)
    {
        $db = Zend_Registry::get("db");
        return $db->fetchOne("
			SELECT	is_pic_updated
			FROM	users
			WHERE	user_id = ?",
            $userId
        );
    }
        
        public static function getFBUser($fb_id) {
                $db = Zend_Registry::get("db");
                $result = $db->fetchRow("
                        SELECT          *
                        FROM            users
                        WHERE           facebook_id = ?",
                        $fb_id
                );
                return $result;
        }
        
        public static function getTwitterUser($twitter_id) {
            $db = Zend_Registry::get("db");
            $result =$db->fetchRow("
                    SELECT *
                    FROM users
                    WHERE twitter_id = ?",
                    $twitter_id
                    );
            return  $result;
        }
	
	public static function checkAuthorization() {
		$server = Zend_Registry::get("server");
		if(empty($_SESSION["user"]) || $_SESSION["user"]['userType'] == 'Reader') {
			$_SESSION["loginRedirect"] = "http://".$server->apphost.$_SERVER['REQUEST_URI'];
			session::write(session_id(), $_SESSION);
			PS_Util::redirect("http://".$server->apphost."/auth");
			exit;
		}	
	}	
	
	// get user's 5 latest activity
	public static function getActivity($user_id)
	{
		$db = Zend_Registry::get("db");		
		$sql = "SELECT	* FROM	activity WHERE (sender = '".$user_id."' OR receiver = '".$user_id."') AND deleted_user != '".$user_id."' ORDER BY id DESC LIMIT 0,5 ";	
		//echo $sql;	
		return $db->fetchAll($sql);
	}
	
	/// get user's all activity
	public static function getAllActivity($user_id)
	{
		$db = Zend_Registry::get("db");		
		$sql = "SELECT	* FROM	activity WHERE (sender = '".$user_id."' OR receiver = '".$user_id."') AND deleted_user != '".$user_id."' ORDER BY id DESC LIMIT 5, 18446744073709551615";	
		//echo $sql;	
		return $db->fetchAll($sql);
	}
	
	public static function getActivityLatest($user_id, $lastid)
	{
		$db = Zend_Registry::get("db");		
		$sql = "SELECT	* FROM	activity WHERE (sender = '".$user_id."' OR receiver = '".$user_id."') AND id > ".$lastid." ORDER BY id DESC LIMIT 0,10 ";	
		//echo $sql;	
		return $db->fetchAll($sql);
	}
	
	/// Get time diffence
	public static function getTime($timestamp)
	{
		$postedc =  time() - $timestamp;        
		///
		if($postedc < 3600) {
         if(round($postedc/60) > 1)
          $postedcdisplay = round($postedc/60)." ".PS_Translate::_("minutes ago");
         else
          $postedcdisplay = "1 ".PS_Translate::_("minute ago");
        }
		else if($postedc < 86000) {
         if(round($postedc/3600) > 1)
          $postedcdisplay = round($postedc/3600)." ".PS_Translate::_("hours")." ago ";
         else
          $postedcdisplay = "1 ".PS_Translate::_("hour")." ago ";
        }
		 else if($postedc < 430000) {
         if(round($postedc/86000) > 1)
          $postedcdisplay = round($postedc/86000)." ".PS_Translate::_("days")." ago ";
         else 
          $postedcdisplay = "1 ".PS_Translate::_("day")." ago ";
        }
        else {
        // $postedcdisplay = " on ".date("m/d/Y", strtotime($result->wishdate));
		 $postedcdisplay = " on ".date("m/d/Y", $timestamp);
        }
		
		return $postedcdisplay;
	}
	
	public static function chkActivityRemoved($actid)
	{
		$db = Zend_Registry::get("db");		
		$sql = "SELECT	deleted_user FROM	activity WHERE id = '".$actid."'";	
		//echo $sql;	
		return $db->fetchOne($sql);
	}
	
	// get newly joined FB friend notification
	public static function getnewFBjoinNotify($user_id)
	{
		$db = Zend_Registry::get("db");
		
		$sql = "SELECT u.user_id, u.first_name, u.last_name, u.profile_pic FROM users u JOIN newfbuser_notify_tmp n ON n.user_id = u.user_id WHERE n.friend_id = '".$user_id."'";
	//echo $sql;
		//exit;
		return $db->fetchAll($sql);
	}
	
	/*   list all users from DB       */
	
	public static function listAllUsers()
	{
		$db = Zend_Registry::get("db");		
		$sql = "SELECT user_id, first_name, last_name, gender, email_id, profile_pic, device_type, gender, created_date, DATE_FORMAT(FROM_DAYS(DATEDIFF(NOW(),birth_date)), '%Y')+0 AS age,
				(SELECT COUNT(id) FROM userwishlist AS uw WHERE uw.user_id = u.user_id) AS total_item,
				(SELECT COUNT(id) FROM groups AS g WHERE g.created_by = u.user_id) AS groups,
				(SELECT COUNT(id) FROM usersfriend AS f WHERE f.user_id = u.user_id) AS following,
				(SELECT COUNT(id) FROM usersfriend AS f1 WHERE f1.friend_id = u.user_id) AS follower
				
				FROM users AS u ORDER BY first_name";	
		//echo $sql;
		return $db->fetchAll($sql);
	}

        public static function GetFeaturedProductCuratorsTop()
        {
                $db = Zend_Registry::get("db");

                $sql = "SELECT *  FROM users WHERE user_id > 1 ORDER BY influence_score DESC LIMIT 0, 100";

                return $db->fetchAll($sql);
        }

        public static function GetUserRanking($user_id) {
             $db = Zend_Registry::get("db");
             $sql = "SELECT  influence_score from users where user_id='".$user_id."'"; 
             $influence_score = $db->fetchRow($sql);
             $logger = Zend_Registry::get("logger");
             //$logger->info("influence_score = ". $influence_score->influence_score);     
             $sql = "SELECT count(*) AS count from users where influence_score >'".$influence_score->influence_score."'";
             //$logger->info(" ". $sql);
             $count_score_greator = $db->fetchRow($sql);
             //$logger->info(" count_score_greator =".$count_score_greator->count);
             $sql = "SELECT count(*) AS count from users where influence_score <'".$influence_score->influence_score."'"; 
             $count_score_lesser = $db->fetchRow($sql);
             //$logger->info(" count_score_lesser =".$count_score_lesser->count);
             $rank_score = (100 * $count_score_greator->count) / ($count_score_lesser->count + $count_score_greator->count);
             if ($rank_score < 10 ) { 
                 $rank_pos = 10; 
             } else if ($rank_score < 25) {
                 $rank_pos = 25;
             } else if ($rank_score < 50) {
                 $rank_pos = 50; 
             } else if ($rank_score < 100) {
                 $rank_pos = 100;
             } else { $rank_pos = 0; }
             //$logger->info("rank pos ". $rank_pos);
             //$logger->info(" ". $rank_score);
             return $rank_pos;
       }

         public static function GetFeaturedProductCurators()
        {
        	 $db = Zend_Registry::get("db");

                $sql = "SELECT user_id, first_name, last_name, gender, email_id, profile_pic, about_me, curated_description FROM users WHERE curator = '1' ORDER BY modified_date DESC LIMIT 0, 3";

                return$db->fetchAll($sql);
        }

	public static function listAllUsersFilter($filterSql)
	{
		$db = Zend_Registry::get("db");		
		$sql = "SELECT user_id, first_name, last_name, gender, email_id, profile_pic, device_type, gender, created_date, DATE_FORMAT(FROM_DAYS(DATEDIFF(NOW(),birth_date)), '%Y')+0 AS age,
				(SELECT COUNT(id) FROM userwishlist AS uw WHERE uw.user_id = u.user_id) AS total_item,
				(SELECT COUNT(id) FROM groups AS g WHERE g.created_by = u.user_id) AS groups,
				(SELECT COUNT(id) FROM usersfriend AS f WHERE f.user_id = u.user_id) AS following,
				(SELECT COUNT(id) FROM usersfriend AS f1 WHERE f1.friend_id = u.user_id) AS follower
				
				FROM users AS u WHERE ".$filterSql." ORDER BY first_name";	
		//echo $sql;
		return $db->fetchAll($sql);
	}
	/*public static function searchbypeople($people){
		$db = Zend_Registry::get("db");	
		$sql = "SELECT * FROM users WHERE status ='0' AND  first_name LIKE '%".$people."%' or last_name LIKE '%".$people."%'  ORDER BY user_id";
		$result = $db->fetchAll($sql);
		return $result;
	}*/
		
	public static function searchbypeople($people){
		$db = Zend_Registry::get("db");	
		$sql = "SELECT * FROM users WHERE status ='0' AND  CONCAT( first_name,' ',last_name ) LIKE '%".$people."%' 	ORDER BY user_id";
		
		$result = $db->fetchAll($sql);
		return $result;
	}
	
	public static function countFBUsers()
	{
		$db = Zend_Registry::get("db");		
		$sql = "SELECT count(user_id) AS total FROM users WHERE facebook_id != ''";	
		return $db->fetchOne($sql);
	}
	public static function countTwitterUsers()
	{
		$db = Zend_Registry::get("db");		
		$sql = "SELECT count(user_id) AS total FROM users WHERE twitter_name != ''";	
		return $db->fetchOne($sql);
	}
    public static function getLastLogin($userId)
    {
        $db = Zend_Registry::get("db");
        $sql = "SELECT current_login FROM users WHERE user_id = '".$userId."'";
        return $db->fetchOne($sql);
    }

    public static function isPicUpdated($userId){
        $db = Zend_Registry::get("db");
        $is_pic_updated = $db->fetchOne("
			SELECT	is_pic_updated
			FROM	users
			WHERE	user_id = ?",
            $userId
        );
        return $is_pic_updated;
    }

   public static function updateUserWishlinkid($userID, $wishlinkid)
   {
             $db = Zend_Registry::get("db");
             $logger = Zend_Registry::get("logger");

             $db->beginTransaction();
             $data=array('wishlinkid'=> $wishlinkid);
             $where = array("user_id = ?" => $userID);
             $db->update('users',$data,$where);
             $db->commit();
   }

 public static function updateUserWishlistcount($userID)
 {
             $db = Zend_Registry::get("db");
             $logger = Zend_Registry::get("logger");

             $sql = "SELECT  DISTINCT(w.id), count(*) AS ct FROM wishlistitmes w LEFT JOIN userwishlist uw ON w.id = uw.item_id WHERE uw.user_id = '".$userID."'";
	     $count_items = $db->fetchRow($sql);

             $db->beginTransaction();
             $data=array('total_wishlist_items'=> $count_items->ct);
             $where = array("user_id = ?" => $userID);
             $db->update('users',$data,$where);
             $db->commit();
  }

	public static function getUserIdByHashCode($code)
	{
		$db = Zend_Registry::get("db");
		$result = $db->fetchOne("
			SELECT 		user_id
			FROM		users
			WHERE		md5(user_id) 	= ?",
			$code
		);
		return $result;
	}

	public static function getUserIdByEmail($email)
	{
		$db = Zend_Registry::get("db");
		$result = $db->fetchOne("
			SELECT 		user_id
			FROM		users
			WHERE		email_id 	= ?",
			$email
		);
		return $result;
	}

	public static function getUserIdByFullName($fullName)
	{
		try{
			$db = Zend_Registry::get("db");
			$fullName = explode(' ', $fullName);
			$sql = "
			SELECT 		user_id
			FROM		users
			WHERE		first_name 	= '" . $fullName[0]. "' AND last_name = '" . $fullName[1] . "'";
			$result = $db->fetchOne($sql);
			return $result;
		} catch (Exception $e) {
			return $e->getMessage();
		}
		
	}
	public static function getUserFriendDetails($user_id, $friend_id)
	{
		$db = Zend_Registry::get("db");
		$friend = $db->fetchRow("
			SELECT 		*
			FROM		users
			WHERE		user_id	= ?",
			$friend_id
		);

		$matching_users_friends_map = usersfriend::searchFriendsByNameMap($user_id, '');
		return self::prepareFriendDetails($friend, $matching_users_friends_map);
		
	}
	
	public static function prepareFriendDetails($friend, $matching_users_friends_map) {
		$server = Zend_Registry::get("server");
		$result= array();
		$result['id'] = $friend->user_id;
		$result['name'] = $friend->first_name . ' ' . $friend->last_name;

		if($friend->profile_pic != '')
		{

			$result['profile_pic'] = $server->securehttp.$server->apphost."/images/userpics/".$friend->profile_pic;
		}else{
			$result['profile_pic'] = $server->securehttp.$server->apphost."/images/userpics/default-profile-pic.png";
		}

		$result['created_date'] = $friend->created_date;
		$result['link'] = $server->securehttp.$server->apphost . '/wishlist/wishlist/friend_id/' . md5($friend->user_id);

		if ($matching_users_friends_map[$friend->user_id] != '') {
			$user_friend = $matching_users_friends_map[$friend->user_id];
			$list['type'] = $user_friend['type'];
			$list['request_status'] = $user_friend['request_status'];
			$list['status'] = $user_friend['status'];
			$list['is_yourfollower'] = $user_friend['is_your_follower'];
		} else {
			$list['type'] = 'follower';
			$list['status'] = 0; // 0-active, 1-inactive
			$list['request_status'] = '1'; // 0-active, 1-inactive
			$list['is_yourfollower'] = 'N';
		}
		return $result;
	}

        public static function curatorcuratedinfo($userId, $curatedDescription) {
		$db = Zend_Registry::get("db");
		$data['curated_description'] = $curatedDescription;
		if(isset($userId) && $userId != '') {
			// update curated_description;
			try {
				$db->update("users", $data, "user_id=" . $userId);
				return true;
			} catch (Exception $e) {
				throw $e;
			}
		}
		return false;
	}
	
	public static function updateAccount($params) {
		$db = Zend_Registry::get("db");
		try {
			$db->beginTransaction();
	
			$datetime = date("Y-m-d H:i:s");
			//add user
			$user_id = $_SESSION['user_id'];
			$userDetail = self::getUserDetails($user_id);
			$name = $userDetail->first_name . ' ' . $userDetail->last_name;
			if (!empty($params["full_name"])) {
				$name = trim($params["full_name"]);
				$fullName = explode(' ', $name);
				$data['first_name'] = $fullName[0];
				$data['last_name'] = $fullName[1];
			}

			if (!empty($params["email"])) {
				$data['email_id'] = trim($params["email"]);
			}
			if (!empty($params["gender"])) {
				$data['gender'] = trim($params["gender"]);
			}
			if (!empty($params["dob"])) {
				$birthDate = DateTime::createFromFormat('m/j/y',$params['dob']);
				$data['birth_date'] = $birthDate->format('Y-m-d');
				$data['device_type'] = 'web';
				$data['modified_date'] = $datetime;
	
				//update event 
				$event_datetmp = date('Y-m-d', strtotime($params['dob']));
				$edtArr = explode('-', $event_datetmp);
				$event_date = date('Y') . '-' . $edtArr[1] . '-' . $edtArr[2];
	
				$event_name = $name . ' - Birthday';
	
				$eventData = array(
					"event_date" => $event_date,
					"title" => $event_name,
					"updated_date" => $datetime
				);
				$db->update('events',$eventData,'created_user = '.$user_id.' AND event_type = "Birthday"');
				$data['birth_date'] = $birthDate->format('Y-m-d');
			}
			if (!empty($params['password'])) {
				$data['password'] = md5($params['password']);
			}

			if (!empty($params['profile_pic'])) {
				$data['profile_pic'] = 	$params['profile_pic'];
			}

			if (!empty($data)) {
				$data['device_type'] = 'web';
				$data['modified_date'] = $datetime;
				$db->update('users', $data, 'user_id = ' . $user_id);
				$db->commit();
			}
			
		} catch (Exception $e) {
			//Rollback transaction
			$db->rollBack();
		}
	}

}///end class


