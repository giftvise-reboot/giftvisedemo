<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * GiftviseAuth is a front controller plugin to check
 * authentication before accessing any controller and action
 *
 * @author kishoreu
 */
class GiftviseAuth extends Zend_Controller_Plugin_Abstract {
    /*
    public function routeStartup(Zend_Controller_Request_Abstract $request) {
        
    }
    
    public function routeShutdown(Zend_Controller_Request_Abstract $request) {
        
    }
    
    public function dispatchLoopStartup(Zend_Controller_Request_Abstract $request) {
        
    }
    */
    
    public function preDispatch(Zend_Controller_Request_Abstract $request) {
        /*
        // check user login details and verify before dispatch
        $logger = Zend_Registry::get("logger");
        
        $logger->debug("Controller: ". $request->getControllerName() .
                        " Action: ". $request->getActionName());
        
        if ($request->getControllerName() != 'index' &&
            ($request->getActionName() != 'index' || $request->getActionName() != ' setaccesstoken')) {
            if ($_SESSION['userId'] == '') {
                // redirect to login page when user not logged in
                $logger->debug("Not logged-in: Redirecting to login page");
                $request->setModuleName('default');
                $request->setControllerName('index');
                $request->setActionName('index');
            }
        }
        */
    }
    
    /*
    public function postDispatch(Zend_Controller_Request_Abstract $request) {
        
    }
    
    public function dispatchLoopShutdown(Zend_Controller_Request_Abstract $request) {
        
    }
    */
}
