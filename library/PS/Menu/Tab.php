<?php


class PS_Menu_Tab
{
	private $text;
	private $href;
	private $ID;
	private $children 			= array();
	public 	$display;
	private	$parentNode;
	private $classes 			= array();
	private $disabled 			= false;
	private	$hideIfChildless	= true;
	
	function __construct($text, $href = NULL, $display = true)
	{
		$this->text 	= $text;
		$this->href 	= $href;
		$this->display 	= $display;
	}
	
	public function hideIfChildless($hide)
	{
		$this->hideIfChildless = $hide;
	}
	
	public function addClasses()
	{
		foreach(func_get_args() as $class){
			$this->addClass($class);
		}
		
		return $this;
	}
	
	public function addClass($class)
	{
		$this->classes[] = $class;
		
		return $this;
	}
	
	public function displayable()
	{
		if(!$this->hideIfChildless){
			return true;
		}
		
		foreach($this->children as $child)
		{
			if($child->displayable()){
				return true;
			}
		}
		
		return false;
	}
	
	public function setID($ID)
	{
		$this->ID = $ID;
		
		return $this;
	}
	
	public function getID()
	{
		return $this->getParentID()."_".$this->ID;
	}
	
	public function getParentID()
	{
		if($this->parentNode){
			return $this->parentNode->getID();
		}
	}
	
	public function setParent($parentNode)
	{
		$this->parentNode = $parentNode;
	}
	
	public function addOption(PS_Menu_Option $option)
	{
		if($option->display)
		{
			//Add to children
			$this->children[] = $option;
			
			//Set pointer back to parent
			$option->setParent($this);
			
			//Check if link is selected
			$option->checkSelected();
		}
		
		return $this;
	}
	
	public function addOptions()
	{
		foreach(func_get_args() as $option){
			$this->addOption($option);
		}
		
		return $this;
	}
	
	public function disable($disabled)
	{
		$this->disabled = $disabled;
		
		foreach($this->children as $child){
			$child->disable($disabled);
		}
		
		return $this;
	}
	
	private function getClasses()
	{
		return implode(" ", $this->classes);
	}
	
	public function display()
	{
		//Get full ID
		$ID = $this->getID();
		
		$returnStr = '<div class="link '.$this->getClasses().' '.($this->disabled ? "disabled" : "").'">
			<a class="parent" href="'.($this->href ? $this->href : "javascript:void(0)").'">'.$this->text.'</a>';
		
		//Add children html
		if(!empty($this->children))
		{
			$returnStr .= '<div class="menu-options">';
			
			$count = count($this->children);
			
			foreach($this->children as $child){
				$returnStr .= '<div class="menu-option '.(--$count == 0 ? "last-option" : "").'">'.$child->display().'</div>';
			}
			
			$returnStr .= '</div>';
		}
		
		
		$returnStr .= '</div>';
		
		return $returnStr;
	}
}