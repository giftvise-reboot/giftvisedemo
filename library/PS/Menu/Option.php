<?php


class PS_Menu_Option
{
	private	$text;
	private $href;
	private	$selectRegExp;
	private	$parentNode;
	private $selected 	= false;
	public 	$display;
	private $classes 	= array();
	private $disabled 	= false;
	private $title;
	private $attributes;
	
	function __construct($text, $href = NULL, $display = true)
	{
		$this->text		= $text;
		$this->href 	= $href;
		$this->display	= $display;
	}
	
	public function addClasses()
	{
		foreach(func_get_args() as $class){
			$this->addClass($class);
		}
		
		return $this;
	}
	
	public function addClass($class)
	{
		$this->classes[] = $class;
		
		return $this;
	}
	
	public function addAttribute($key, $value)
	{
		$this->attributes[$key] = $value;
		
		return $this;
	}
	
	public function displayable()
	{
		return true;
	}
	
	public function setParent($parentNode)
	{
		$this->parentNode = $parentNode;
	}
	
	public function display()
	{
		if($this->disabled){
			return '<div class="disabled">'.$this->text.'</div>';
		}
		
		return '<a href="'.$this->href.'" '.$this->getAttributes().' '.$this->getTitle().' class="'.($this->selected ? "selected" : "").' '.$this->getClasses().'">'.$this->text.'</a>';
	}
	
	private function getAttributes()
	{
		if(!empty($this->attributes))
		{
			foreach($this->attributes as $key => $value){
				$attributes[] = $key.'="'.$value.'"';
			}
			
			return implode(" ", $attributes);
		}
	}
	
	private function getTitle()
	{
		return $this->title ? 'title="'.$this->title.'"' : "";
	}
	
	private function getClasses()
	{
		return implode(" ", $this->classes);
	}
	
	public function checkSelected()
	{
		if(eregi($this->selectRegExp, $_SERVER["REQUEST_URI"])){
			$this->selected = true;
		}
	}
	
	public function disable($disabled)
	{
		$this->disabled = $disabled;
		
		return $this;
	}
	
	public function getIDs(){}
	
	public function setTitle($title)
	{
		$this->title = str_replace('"', '\"', $title);
		
		return $this;
	}
}