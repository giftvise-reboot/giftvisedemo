<?php


class PS_Filter_Date implements Zend_Filter_Interface
{
	public function filter($value)
	{
		if($value){
			return date("Y-m-d", strtotime($value));
		}
	}
}