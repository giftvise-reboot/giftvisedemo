<?php



class PS_Wizard
{
	protected $separator;
	protected $steps = array();
	protected $prepend;
	protected $append;
	protected $numberSteps = false;
	protected $numberSeparator;
	
	public function __construct($separator = ">")
	{
		$this->separator = $separator;
	}
	
	public function numberSteps($numberSeparator = ".")
	{
		$this->numberSteps 		= true;
		$this->numberSeparator 	= $numberSeparator;
			
		return $this;
	}
	
	public function setTextBefore(PS_Wizard_Text $text)
	{
		$this->prepend = $text->getText("before-text");
		
		return $this;
	}
	
	public function setTextAfter(PS_Wizard_Text $text)
	{
		$this->append = $text->getText("after-text");
		
		return $this;
	}
	
	public function addStep(PS_Wizard_Step $step)
	{
		$this->steps[] = $step;
		
		return $this;
	}
	
	public function setClickable($indexes, $clickable = true)
	{
		if(!is_array($indexes)){
			$indexes = array($indexes);
		}
		
		foreach($indexes as $index){
			$this->steps[$index]->setClickable($clickable);
		}
		
		return $this;
	}
	
	public function setClickableAll($clickable = true)
	{
		foreach($this->steps as $step){
			$step->setClickable($clickable);
		}
		
		return $this;
	}
	
	public function execute()
	{
		$selectedIndex = -1;
		
		foreach($this->steps as $index => $step)
		{
			if($step->selected()){
				$selectedIndex = $index;
			}
		}
		
		$stepNum = 1;
		
		foreach($this->steps as $index => $step)
		{
			$link = '<span class="step">';
			
			//Step #
			if($this->numberSteps){
				$link .= '<span class="stepNum">'.$stepNum++.$this->numberSeparator.' </span>';
			}
			
			//Current step
			if($index == $selectedIndex){
				$link .= $step->getPlainText("selected");
			}
			//Completed step
			else if($index < $selectedIndex)
			{
				if($step->isClickable()){
					$link .= $step->getLink("completed");
				}
				else{
					$link .= $step->getPlainText("completed");
				}
			}
			//Uncompleted step
			else{
				$link .= $step->getPlainText("uncompleted");
			}
			
			$link .= '</span>';
			
			$links[] = $link;
		}
		
		return '<div class="wizard">'.$this->prepend.implode('<span class="separator">'.htmlentities($this->separator).'</span>', $links).$this->append.'</div>';
	}
}