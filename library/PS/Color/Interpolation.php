<?php

/*

Example:

$colors = new PS_Color_Interpolation(0x000000, 0xFFFFFF, 15);

foreach($colors as $color){
	echo $color."<br />";
}

*/

class PS_Color_Interpolation implements SeekableIterator, Countable
{
	protected $startRGB;
	protected $endRGB;	
	protected $position = 0;
	protected $numSteps;
	
	public function __construct($startHex, $endHex, $numSteps)
	{
		$this->startRGB = $this->getRGB($startHex);
		$this->endRGB	= $this->getRGB($endHex);
		$this->numSteps	= $numSteps;
	}
	
	protected function getComponent($start, $end)
	{
		if($start < $end){
			return (($end - $start) * ($this->position / $this->numSteps)) + $start;
		}
		else{
			return (($start - $end) * (1 - ($this->position / $this->numSteps))) + $end;
		}
	}
	
	public function getRGB($hex)
	{
		return array(
			($hex & 0xff0000) >> 16,
			($hex & 0x00ff00) >> 8,
			($hex & 0x0000ff) >> 0
		);
	}
	
	public function rewind()
	{
		$this->position = 0;
	}
	
	public function current()
	{
		$red 	= $this->getComponent($this->startRGB[0], $this->endRGB[0]);
		$green 	= $this->getComponent($this->startRGB[1], $this->endRGB[1]);
		$blue 	= $this->getComponent($this->startRGB[2], $this->endRGB[2]);
  		
		return sprintf("#%06X", ((($red << 8) | $green) << 8) | $blue);
	}
	
	public function key()
    {
        return $this->position;
    }
    
    public function next()
    {
        ++$this->position;
    }
    
    public function valid()
    {
        return $this->position >= 0 and $this->position <= $this->numSteps;
    }
    
    public function count()
    {
        return $this->numSteps;
    }
    
    public function seek($position)
    {
        $this->position = (int) $position;
        
        if(!$this->valid()){
            throw new OutOfBoundsException("Invalid seek position ($position)");
        }
    }
}