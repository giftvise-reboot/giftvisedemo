<?php

class PS_Weight
{
	const DESK_JOB 					= "deskJob";
	const THREE_TIMES_WEEK 			= "3timesWeek";
	const FIVE_TIMES_WEEK 			= "5timesWeek";
	const FIVE_TIMES_WEEK_INTENSE 	= "5timesWeekIntense";
	const EVERY_DAY 				= "everyDay";
	const EVERY_DAY_INTENSE 		= "everyDayIntense";
	const EVERY_DAY_PHYSICAL_JOB	= "everyDayPhysicalJob";
	
	const UNDERWEIGHT				= "underweight";
	const NORMAL					= "normal";
	const OVERWEIGHT				= "overweight";
	const OBESE						= "obese";
	
	public static function BMI($weight, $height)
	{
		return round(($weight * 703) / ($height * $height), 1);
	}
	
	public static function maleBMR($weight, $height, $age)
	{
		return 66 + (6.23 * $weight) + (12.7 * $height) - (6.8 * $age);
	}
	
	public static function femaleBMR($weight, $height, $age)
	{
		return 655 + (4.35 * $weight) + (4.7 * $height) - (4.7 * $age);
	}
	
	public static function dailyCaloricNeeds($BMR, $dailyActivityLevel)
	{
		switch($dailyActivityLevel)
		{
			case self::DESK_JOB:
				$multiplier = 1.2;
				break;
			case self::THREE_TIMES_WEEK:
				$multiplier = 1.37;
				break;
			case self::FIVE_TIMES_WEEK:
				$multiplier = 1.45;
				break;
			case self::FIVE_TIMES_WEEK_INTENSE:
				$multiplier = 1.55;
				break;
			case self::EVERY_DAY:
				$multiplier = 1.64;
				break;
			case self::EVERY_DAY_INTENSE:
				$multiplier = 1.73;
				break;
			case self::EVERY_DAY_PHYSICAL_JOB:
				$multiplier = 1.9;
				break;
		}
		
		return $BMR * $multiplier;
	}
	
	public static function weightClass($BMI)
	{
		if($BMI < 18.5){
			$weightClass = self::UNDERWEIGHT;
		}
		else if($BMI >= 18.5 and $BMI < 25){
			$weightClass = self::NORMAL;
		}
		else if($BMI >= 25 and $BMI < 30){
			$weightClass = self::OVERWEIGHT;
		}
		else if($BMI >= 30){
			$weightClass = self::OBESE;
		}
		
		return $weightClass;
	}
	
	public static function recommendedWeight($weight, $height)
	{
		$BMI = self::BMI($weight, $height);
		
		if($BMI < 18.5){
			$targetBMI = 18.5;
		}
		else if($BMI >= 25){
			$targetBMI = 24.9;
		}
		else{
			$targetBMI = $BMI;
		}
		
		return ($targetBMI * ($height * $height)) / 703;
	}
	
	public static function dailyCaloricDeficit($currentWeight, $goalWeight, $currentDate, $goalDate)
	{
		$numDays 		= (int)((strtotime($goalDate) - strtotime($currentDate)) / 86400);
		$numCalories 	= ($goalWeight - $currentWeight) * 3500;
		$dailyDeficit 	= $numCalories / $numDays;
		
		return $dailyDeficit;
	}
}