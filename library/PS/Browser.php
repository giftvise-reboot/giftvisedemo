<?php


class PS_Browser
{
	public static function getCSS()
	{
		$classes = array();
		
		if(eregi("windows", $_SERVER["HTTP_USER_AGENT"])){
			$os = "os-win";
		}
		else if(eregi("macintosh", $_SERVER["HTTP_USER_AGENT"])){
			$os = "os-mac";
		}
		
		array_push($classes, $os);
		
		if(eregi("msie ([0-9]+)", $_SERVER["HTTP_USER_AGENT"], $regs)){
			$browser = array("browser-ie", "browser-ie-".$regs[1]);
		}
		else if(eregi("AppleWebKit", $_SERVER["HTTP_USER_AGENT"])){
			$browser = array("browser-webkit", "browser-not-ie");
		}
		else if(eregi("opera", $_SERVER["HTTP_USER_AGENT"])){
			$browser = array("browser-opera", "browser-not-ie");
		}
		else if(eregi("gecko", $_SERVER["HTTP_USER_AGENT"]))
		{
			$browser = array("browser-gecko", "browser-not-ie");
			
			if(eregi("firefox/([0-9.]+)", $_SERVER["HTTP_USER_AGENT"], $regs)){
				$browser[] = "browser-firefox";
				$browser[] = "browser-firefox-".intval($regs[1]);
			}
		}
		
		foreach($browser as $value){
			array_push($classes, $value, $os."-".$value);
		}
		
		return implode(" ", $classes);
	}
	
	public static function get()
	{
		if(eregi("msie ([0-9]+)", $_SERVER["HTTP_USER_AGENT"], $regs)){
			$browser = array("ie", "ie".$regs[1]);
		}
		else if(eregi("AppleWebKit", $_SERVER["HTTP_USER_AGENT"], $regs))
		{
			$browser = array("webkit");
			
			if(eregi("version/([0-9.]+) safari", $_SERVER["HTTP_USER_AGENT"], $regs)){
				array_push($browser, "safari", "safari".intval($regs[1]));
			}
			else if(eregi("chrome/([0-9.]+) safari", $_SERVER["HTTP_USER_AGENT"], $regs)){
				array_push($browser, "chrome", "chrome".intval($regs[1]));
			}
		}
		else if(eregi("opera/([0-9.]+)", $_SERVER["HTTP_USER_AGENT"])){
			$browser = array("opera", "opera".$regs[1]);
		}
		else if(eregi("gecko", $_SERVER["HTTP_USER_AGENT"]))
		{
			$browser = array("gecko");
			
			if(eregi("firefox/([0-9.]+)", $_SERVER["HTTP_USER_AGENT"], $regs)){
				array_push($browser, "firefox", "firefox".intval($regs[1]));
			}
		}
		
		return $browser;
	}
	
	public static function is($browsers)
	{
		if(!is_array($browsers)){
			$browsers = array($browsers);
		}
		
		$useragent = self::get();
		
		foreach($browsers as $browser)
		{
			if(in_array(strtolower($browser), $useragent)){
				return true;
			}
		}
		
		return false;
	}
}