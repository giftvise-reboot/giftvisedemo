<?php

/**
 * Leaf node (link)
 *
 */
class PS_Nav_Link
{
	private $href;
	private	$text;
	private	$selectRegExp;
	private	$parentNode;
	private $selected 	= false;
	public 	$display;
	private $classes 	= array();
	public 	$height 	= 23;
	private $disabled 	= false;
	private $title;
	private $attributes;
	
	/**
	 * @param string $href
	 * @param string $text
	 * @param string $selectRegExp
	 * @param boolean $display
	 */
	function __construct($href, $text, $selectRegExp = NULL, $display = true)
	{
		$this->href 		= $href;
		$this->text			= $text;
		$this->selectRegExp	= $selectRegExp;
		$this->display		= $display;
	}
	
	/**
	 * Add CSS classes
	 * 
	 * @param string $class,...
	 */
	public function addClasses()
	{
		foreach(func_get_args() as $class)
		{
			$this->addClass($class);
		}
		
		return $this;
	}
	
	/**
	 * Add CSS class
	 *
	 * @param string $class
	 */
	public function addClass($class)
	{
		$this->classes[] = $class;
		
		return $this;
	}
	
	/**
	 * Add link attribute
	 *
	 * @param string $key
	 * @param string $value
	 */
	public function addAttribute($key, $value)
	{
		$this->attributes[$key] = $value;
		
		return $this;
	}
	
	/**
	 * 
	 *
	 * @return boolean
	 */
	public function validLinks()
	{
		return true;
	}
	
	/**
	 * Set pointer to parent
	 *
	 * @param navGroup|nav $parentNode
	 */
	public function setParent($parentNode)
	{
		$this->parentNode = $parentNode;
	}
	
	/**
	 * Display link html
	 *
	 * @return string
	 */
	public function display()
	{
		if($this->disabled)
		{
			return '
			<div class="disabled">
				<div class="nav-node nav-link '.implode(" ", $this->classes).'">'.$this->text.'</div>
			</div>';
		}
		
		return '<a href="'.$this->href.'" '.$this->getAttributes().' '.$this->getTitle().' class="nav-node nav-link '.($this->selected ? "selected" : "").' '.$this->getClasses().'">'.$this->text.'</a>';
	}
	
	/**
	 * Get attribute string
	 *
	 * @return string
	 */
	private function getAttributes()
	{
		if(!empty($this->attributes))
		{
			foreach($this->attributes as $key => $value)
			{
				$attributes[] = $key.'="'.$value.'"';
			}
			
			return implode(" ", $attributes);
		}
	}
	
	/**
	 * Get title string
	 *
	 * @return string
	 */
	private function getTitle()
	{
		return $this->title ? 'title="'.$this->title.'"' : "";
	}
	
	/**
	 * Get css classes
	 *
	 * @return string
	 */
	private function getClasses()
	{
		return implode(" ", $this->classes);
	}
	
	/**
	 * Check if link is selected. Open parent if true.
	 *
	 */
	public function checkSelected()
	{
		if(eregi($this->selectRegExp, $_SERVER["REQUEST_URI"]))
		{
			$this->selected = true;
			
			$this->open();
		}
	}
	
	/**
	 * Open node (recursive down)
	 *
	 */
	public function openAll()
	{
		$this->open = true;
	}
	
	/**
	 * Open node and parent
	 *
	 */
	public function open()
	{
		if($this->parentNode)
		{
			$this->parentNode->open();
		}
	}
	
	/**
	 * Close node
	 *
	 */
	public function close()
	{
		$this->open = false;
	}
	
	/**
	 * Disable node
	 * 
	 */
	public function disable($disabled)
	{
		$this->disabled = $disabled;
		
		return $this;
	}
	
	/**
	 * 
	 *
	 */
	public function getIDs()
	{
		
	}
	
	/**
	 * Set link title
	 *
	 * @param string $title
	 */
	public function setTitle($title)
	{
		$this->title = str_replace('"', '\"', $title);
		
		return $this;
	}
}