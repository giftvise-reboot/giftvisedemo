<?php

/**
 * Spacer node with specific height
 *
 */


class PS_Nav_Whitespace
{
	/**
	 * Parent node
	 *
	 * @var navGroup
	 */
	
	private $parentNode;
	
	/**
	 * Height of node
	 *
	 * @var int
	 */
	
	public $height;
	
	/**
	 * Toggle display (on/off)
	 *
	 * @var boolean
	 */
	
	public $display;
	
	/**
	 *
	 * @param int $height
	 * @param boolean $display
	 */
	
	function __construct($height = 10, $display = true)
	{
		$this->height 	= intval($height);
		$this->display	= $display;
	}
	
	/**
	 * Get node HTML
	 *
	 * @return string
	 */
	
	public function display()
	{
		return '<div class="nav-whitespace" style="height: '.$this->height.'px"></div>';
	}
	
	/**
	 * Set point to parent node
	 *
	 * @param navGroup $parentNode
	 */
	
	public function setParent($parentNode)
	{
		$this->parentNode = $parentNode;
	}
	
	/**
	 * 
	 *
	 */
	
	public function openAll(){}
	
	/**
	 * 
	 *
	 */
	
	public function disable(){}
	
	public function validLinks()
	{
		return false;
	}
}