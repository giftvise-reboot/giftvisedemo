<?php

/**
 * Non leaf node in navigation tree
 *
 */
class PS_Nav_Section
{
	private $ID;
	private $children 		= array();
	private $childID 		= 1;
	private $text;
	public 	$display;
	private	$parentNode;
	private $classes 		= array();
	private $disabled 		= false;
	private $attributes;
	
	/** 
	 * @param string $text
	 * @param boolean $display
	 */
	function __construct($text, $display = true)
	{
		$this->text 	= $text;
		$this->display 	= $display;
	}
	
	/**
	 * Add CSS classes
	 * 
	 * @param string $class,...
	 */
	public function addClasses()
	{
		foreach(func_get_args() as $class)
		{
			$this->addClass($class);
		}
		
		return $this;
	}
	
	/**
	 * Add CSS class
	 *
	 * @param string $class
	 */
	public function addClass($class)
	{
		$this->classes[] = $class;
		
		return $this;
	}
	
	/**
	 * Add CSS class for group
	 *
	 * @param string $class
	 */
	public function addGroupClass($class)
	{
		$this->groupClasses[] = $class;
		
		return $this;
	}
	
	/**
	 * Add link attribute
	 *
	 * @param string $key
	 * @param string $value
	 */
	public function addAttribute($key, $value)
	{
		$this->attributes[$key] = $value;
		
		return $this;
	}
	
	/**
	 * Check if group has links
	 *
	 * @return boolean
	 */
	public function validLinks()
	{
		foreach($this->children as $child)
		{
			if($child->validLinks())
			{
				return true;
			}
		}
		
		return false;
	}
	
	/**
	 * Open all nodes recursively
	 *
	 */
	public function openAll()
	{
		foreach($this->children as $child)
		{
			$child->openAll();
		}
		
		return $this;
	}
	
	/**
	 * Open this node and parent recursively
	 *
	 */
	public function open()
	{
		if($this->parentNode)
		{
			$this->parentNode->open();
		}
		
		return $this;
	}
	
	/**
	 * Close nodes recursively
	 *
	 */
	public function close()
	{
		foreach($this->children as $child)
		{
			$child->close();
		}
		
		return $this;
	}
	
	/**
	 * Set ID
	 *
	 * @param int $ID
	 */
	public function setID($ID)
	{
		$this->ID = $ID;
		
		return $this;
	}
	
	/**
	 * Get full ID
	 *
	 * @return string
	 */
	public function getID()
	{
		return $this->getParentID()."_".$this->ID;
	}
	
	/**
	 * Get parent ID
	 *
	 * @return string
	 */
	public function getParentID()
	{
		if($this->parentNode)
		{
			return $this->parentNode->getID();
		}
	}
	
	/**
	 * Get IDs of nav tree recursively
	 *
	 * @return array
	 */
	public function getIDs()
	{
		$returnArray[] = $this->getID();
		
		foreach($this->children as $child)
		{
			if(get_class($child) == "navGroup")
			{
				$returnArray = array_merge($returnArray, $child->getIDs());
			}
		}
		
		return $returnArray;
	}
	
	/**
	 * Set pointer to parent
	 *
	 * @param navGroup|nav $parentNode
	 */
	public function setParent($parentNode)
	{
		$this->parentNode = $parentNode;
	}
	
	/**
	 * Add group node to node
	 *
	 * @param navGroup $group
	 */
	public function addGroup(PS_Nav_Group $group)
	{
		if($group->validLinks() and $group->display)
		{
			//Add to children
			$this->children[] = $group;
			
			//Set child ID
			$group->setID($this->childID++);
			
			//Set pointer back to parent
			$group->setParent($this);
		}
		
		return $this;
	}
	
	/**
	 * Add one or more groups
	 *
	 * @param navGroup $group,...
	 */
	public function addGroups()
	{
		foreach(func_get_args() as $group)
		{
			$this->addGroup($group);
		}
		
		return $this;
	}
	
	/**
	 * Add link to node
	 *
	 * @param navLink $link
	 */
	public function addLink(PS_Nav_Link $link)
	{
		if($link->display)
		{
			//Add to children
			$this->children[] = $link;
			
			//Set pointer back to parent
			$link->setParent($this);
			
			//Check if link is selected
			$link->checkSelected();
		}
		
		return $this;
	}
	
	/**
	 * Add one or more links
	 * 
	 * @param navLink $link,...
	 */
	public function addLinks()
	{
		foreach(func_get_args() as $link)
		{
			$this->addLink($link);
		}
		
		return $this;
	}
	
	public function addSection(PS_Nav_Section $node)
	{
		if($node->validLinks() and $node->display)
		{
			//Add to children
			$this->children[] = $node;
			
			//Set child ID
			$node->setID($this->childID++);
			
			//Set pointer back to parent
			$node->setParent($this);
		}
		
		return $this;
	}
	
	/**
	 * Add one or more groups or links
	 * 
	 * @param PS_Nav_Link|PS_Nav_Group|PS_Nav_Whitespace $node,...
	 */
	public function add()
	{
		foreach(func_get_args() as $node)
		{
			switch(get_class($node))
			{
				case "PS_Nav_Link":
					$this->addLink($node);
					break;
				case "PS_Nav_Group":
					$this->addGroup($node);
					break;
				case "PS_Nav_Section":
					$this->addSection($node);
					break;
				case "PS_Nav_Whitespace":
					$this->addSpacer($node);
					break;
			}
		}
		
		return $this;
	}
	
	/**
	 * Add spacer to nav
	 *
	 * @param PS_Nav_Whitespace $spacer
	 */
	public function addSpacer($spacer)
	{
		if($spacer->display)
		{
			$this->children[] = $spacer;
			
			//Set pointer back to parent
			$spacer->setParent($this);
		}
		
		return $this;
	}
	
	/**
	 * Disable node and children
	 *
	 */
	public function disable($disabled)
	{
		$this->disabled = $disabled;
		
		foreach($this->children as $child)
		{
			$child->disable($disabled);
		}
		
		return $this;
	}
	
	/**
	 * Make node and children unclickable
	 *
	 */
	public function noClick()
	{
		foreach($this->children as $child)
		{
			if(get_class($child) == "navGroup")
			{
				$child->noClick();
			}
		}
		
		return $this;
	}
	
	/**
	 * Display node and children nodes
	 *
	 * @return string html for node
	 */
	public function display()
	{
		//Get full ID
		$ID = $this->getID();
		
		foreach($this->children as $child)
		{
			//Child html
			$childrenStr .= $child->display();
		}
		
		$returnStr = 
		'<div class="'.($this->disabled ? "disabled" : "").'">'.
			'<div class="nav-node section-name '.implode(" ", $this->classes).'" '.$this->getAttributes().' id="'.$ID.'">'.$this->text.'</div>'.
			($childrenStr ? $childrenStr : "").
		'</div>';
		
		return $returnStr;
	}
	
	/**
	 * Get attribute string
	 *
	 * @return string
	 */
	private function getAttributes()
	{
		if(!empty($this->attributes))
		{
			foreach($this->attributes as $key => $value)
			{
				if(strtolower($key) == "id"){
					continue;
				}
				
				$attributes[] = $key.'="'.$value.'"';
			}
			
			return implode(" ", $attributes);
		}
	}
}