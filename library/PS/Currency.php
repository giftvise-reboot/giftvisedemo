<?php


class PS_Currency
{
	public static function get($value)
	{
		if($value === NULL or $value === ""){
			$value = 0;
		}
		
		$currency = new Zend_Currency();
		
		return $currency->toCurrency($value);
	}
	
	public static function getShortName()
	{
		$currency = new Zend_Currency();
		
		return $currency->getShortName();
	}
}