<?php


class PS_Util_Math
{
	public static function roundBy($num, $divisor)
	{
		return (int)(($num + (int)($divisor / 2)) / $divisor) * $divisor;
	}
	
	public static function toPercentage($num, $decimals = 0)
	{
		if($num == 0){
			$decimals = 0;
		}
		
		return number_format(($num * 100), $decimals)."%";
	}
	
	public static function ordinalize($num)
	{
		if(!is_numeric($num)){
			return $num;
		}
		
		if($num % 100 >= 11 and $num % 100 <= 13){
			return $num."th";
		}
		else if($num % 10 == 1){
			return $num."st";
		}
		else if($num % 10 == 2){
			return $num."nd";
		}
		else if($num % 10 == 3){
			return $num."rd";
		}
		else{ // $num % 10 == 0, 4-9
			return $num."th";
		}
	}
	
	public static function formatFloat($num)
	{
		return ((intval($num) / $num) == 1) ? intval($num) : $num;
	}
}