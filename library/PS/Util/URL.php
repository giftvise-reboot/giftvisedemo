<?php


class PS_Util_URL
{
	public static function buildQuery($vars)
	{
		foreach($vars as $name => $value)
		{
			if(is_array($value))
			{
				foreach($value as $val){
					$return[] = $name."[]=".$val;
				}
			}
			else{
				$return[] = $name."=".$value;
			}
		}
		
		return implode("&", $return);
	}
}