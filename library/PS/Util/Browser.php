<?php


class PS_Util_Browser
{
	public static function get()
	{
		if(preg_match("/windows/i", $_SERVER["HTTP_USER_AGENT"])){
			$os = "os-win";
		}
		else if(preg_match("/macintosh/i", $_SERVER["HTTP_USER_AGENT"])){
			$os = "os-mac";
		}
		
		if(preg_match("/msie ([0-9]+)/i", $_SERVER["HTTP_USER_AGENT"], $regs))
		{
			$browser = "IE";
			$version = $regs[1];
		}
		else if(preg_match("safari", $_SERVER["HTTP_USER_AGENT"]))
		{
			$browser = "Safari";
			
			preg_match("|Version/([0-9.]+)|", $_SERVER["HTTP_USER_AGENT"], $regs);
			
			$version = $regs[1];
		}
		else if(preg_match("/opera/i", $_SERVER["HTTP_USER_AGENT"])){
			$browser = "Opera";
		}
		else if(preg_match("/AppleWebKit/i", $_SERVER["HTTP_USER_AGENT"]))
		{
			$browser = "Webkit";
			
			if(preg_match("|Chrome/([0-9.]+)|", $_SERVER["HTTP_USER_AGENT"], $regs))
			{
				$browser	.= " (Chrome)";
				$version 	= $regs[1];
			}
		}
		else if(preg_match("/gecko/i", $_SERVER["HTTP_USER_AGENT"]))
		{
			$browser = "Gecko";
			
			if(preg_match("|Firefox/([0-9.]+)|", $_SERVER["HTTP_USER_AGENT"], $regs))
			{
				$browser	.= " (Firefox)";
				$version 	= $regs[1];
			}
		}
		
		return array(
			"os" 				=> $os,
			"browser" 			=> $browser,
			"browserVersion" 	=> $version
		);
	}
	
	public static function getCSS()
	{
		if(preg_match("/windows/i", $_SERVER["HTTP_USER_AGENT"])){
			$os = "os-win";
		}
		else if(preg_match("/macintosh/i", $_SERVER["HTTP_USER_AGENT"])){
			$os = "os-mac";
		}
		
		if(preg_match("/msie ([0-9]+)/i", $_SERVER["HTTP_USER_AGENT"], $regs)){
			$browser = array("browser-ie", "browser-ie-".$regs[1]);
		}
		else if(preg_match("/safari/i", $_SERVER["HTTP_USER_AGENT"])){
			$browser = array("browser-safari");
		}
		else if(preg_match("/opera/i", $_SERVER["HTTP_USER_AGENT"])){
			$browser = array("browser-opera");
		}
		else if(preg_match("/gecko/i", $_SERVER["HTTP_USER_AGENT"])){
			$browser = array("browser-gecko");
		}
		
		foreach($browser as $value){
			$browser[] = $os."-".$value;
		}
		
		return implode(" ", $browser);
	}
}