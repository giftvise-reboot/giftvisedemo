<?php


class PS_Util_DB
{
	public static function index($array, $index = NULL, $column = NULL)
	{
		if(empty($array)){
			return array();
		}
		
		if($index !== NULL)
		{
			if(is_array($index))
			{
				$arrayStr = "\$returnArray";
				
				foreach($index as $indexName){
					$arrayStr .= "[\$row->$indexName]";
				}
			}
			else{
				$arrayStr = "\$returnArray[\$row->$index]";
			}
		}
		else{
			$arrayStr = "\$returnArray[]";
		}
		
		if($column !== NULL)
		{
			if(is_array($column))
			{
				$arrayStr .= " = array(";
				
				foreach($column as $col){
					$arrayStr .= " '$col' => \"\$row->$col\",";
				}
				
				$arrayStr .= ");";
			}
			else{
				$arrayStr .= " = \$row->$column;";
			}
		}
		else{
			$arrayStr .= " = \$row;";
		}
		
		foreach($array as $row){
			eval($arrayStr);
		}
		
		return $returnArray;
	}
	
	public static function group($array, $index, $column = NULL)
	{
		if(empty($array)){
			return array();
		}
		
		foreach($array as $row)
		{
			if(is_array($index))
			{
				$arrayStr = "\$returnArray";
				
				foreach($index as $indexName){
					$arrayStr .= "[\$row->$indexName]";
				}
				
				if($column === NULL){
					$arrayStr .= "[] = \$row;";
				}
				else{
					$arrayStr .= "[] = \$row->$column;";
				}
				
				eval($arrayStr);
			}
			else
			{
				if($column === NULL){
					$returnArray[$row->{$index}][] = $row;
				}
				else{
					$returnArray[$row->{$index}][] = $row->$column;
				}
			}
		}
		
		return $returnArray;
	}
	
	public static function parseDate($date)
	{
		if(!$date){
			return;
		}
		
		return date("m/d/Y", strtotime($date));
	}
	
	public static function formatDate($date)
	{
		if(!$date){
			return;
		}
		
		return date("Y-m-d", strtotime($date));
	}
	
	public static function getErrorCode($message)
	{
		if(preg_match("/^SQLSTATE\[.*\]:[A-Za-z\s]+:\s+([0-9]+)/", $message, $regs)){
			return $regs[1];
		}
		
		if(preg_match("/^SQLSTATE\[.*\]\s*\[([0-9]+)\]/", $message, $regs)){
			return $regs[1];
		}
	}
	
	public static function parseField($fieldName)
	{
		$n 	= eregi_replace("^[A-Z0-9]+_", "", $fieldName);
		$n 	= ereg_replace("ID$", "", $n);
		$n 	= ereg_replace("([A-Z]{1})", " \\1", $n);
		$n	= ucwords($n);
		
		return $n;
	}
	
	public static function batch($database, $filename)
	{
		//Convert to object (if array)
		$database = (object) $database;
		
		exec("mysql --user=".$database->username." --password=".$database->password." --host=".$database->host." ".$database->dbname." < ".$filename);
	}
}