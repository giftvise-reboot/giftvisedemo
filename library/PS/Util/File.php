<?php


class PS_Util_File
{
	public static function createDir($dirs)
	{
		if(!is_array($dirs)){
			$dirs = array($dirs);
		}
		
		foreach($dirs as $dir)
		{
			if(!is_dir($dir)){
				mkdir($dir);
			}
		}
	}
	
	public static function getExtension($fileName)
	{
		return substr(strrchr($fileName, "."), 1);
	}
	
	public static function mergePaths()
	{
		$paths = func_get_args();
		
		if(!empty($paths))
		{
			if(count($paths) == 1){
				return $paths[0];
			}
			
			foreach($paths as $path)
			{
				$result	= eregi_replace("/+$", "", $result);
				$path 	= eregi_replace("^/+", "", $path);
				$path 	= eregi_replace("/{2,}$", "/", $path);
				
				if(eregi("^http(s)?://", $path)){
					$result .= $path;
				}
				else{
					$result .= "/".$path;
				}
			}
		}
		
		return $result;
	}
	
	function readdir($dir, $options = array())
	{
		if(!array_key_exists("recursive", $options)){
			$options["recursive"] = false;
		}
		
		if(!array_key_exists("type", $options)){
			$options["type"] = "ALL";
		}
		
		$contents 	= array();
		$dir 		= realpath($dir);
		
		foreach(scandir($dir) as $item)
		{
			//Skip files that begin with a period
			if(ereg("^\.", $item)){
				continue;
			}
			
			$path = self::mergePaths(array($dir, $item));
			
			if(is_dir($path))
			{
				if($options["recursive"]){
					$contents[$item] = self::readdir($path, $options);
				}
				else if($options["type"] != "FILES"){
					$contents[] = $item;
				}
			}
			else if($options["type"] != "FOLDERS"){
				$contents[] = $item;
			}
		}
		
		return $contents;
	}
	
	function normalize($name)
	{
		return strtolower(eregi_replace("-{2,}", "-", eregi_replace("[^0-9A-Z_.-]+", "", eregi_replace("[[:space:]]+", "-", trim($name)))));
	}
	
	function truncate($str, $maxlen)
	{
		if(!is_int($maxlen)){
			return $str;
		}
		
		$strlen = strlen($str);
		
		if($strlen > $maxlen)
		{
			if($maxlen <= 4){
				$str = substr(substr($str, 0, 1)."...", 0, $maxlen);
			}
			else
			{
				$leftlen 	= ceil(($maxlen - 3) / 2);
				$rightlen	= $maxlen - 3 - $leftlen;
				
				$left	= substr($str, 0, $leftlen);
				$right	= substr($str, $strlen - $rightlen, $rightlen);
				
				$str = $left."...".$right;
			}
		}
		
		return $str;
	}
	
	function export($filename, $contents = NULL)
	{
		//Turn off compression
		if(ini_get("zlib.output_compression")){
			ini_set("zlib.output_compression", "Off");
		}
		
		//Get file info
		$info 		= pathinfo($filename);
		$extension 	= $info["extension"];
		$basename	= $info["basename"];
		
		if($contents === NULL){
			$filesize 	= filesize($contents);
			$contents	= file_get_contents($filename);
		}
		else{
			$filesize	= mb_strlen($contents);
		}
		
		//Get content type
		switch($extension)
		{
			case "pdf": $ctype = "application/pdf"; break;
			case "exe": $ctype = "application/octet-stream"; break;
			case "zip": $ctype = "application/zip"; break;
			case "doc": $ctype = "application/msword"; break;
			case "xls": $ctype = "application/vnd.ms-excel"; break;
			case "ppt": $ctype = "application/vnd.ms-powerpoint"; break;
			case "gif": $ctype = "image/gif"; break;
			case "png": $ctype = "image/png"; break;
			case "jpeg":
			case "jpg": $ctype = "image/jpg"; break;
			default: $ctype = "application/force-download";
		}
		
		//Output headers and content
		header("Pragma: public");
		header("Expires: 0");
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		header("Cache-Control: private", false); 
		header("Content-Type: ".$ctype);
		header("Content-Disposition: attachment; filename=".$basename.";");
		header("Content-Transfer-Encoding: binary");
		header("Content-Length: ".$filesize);
		echo $contents;
		exit();
	}
}