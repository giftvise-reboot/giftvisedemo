<?php


class PS_Util_Array
{
	public static function push($array)
	{
		$args 		= func_get_args();
		$elements 	= array_slice($args, 1);
		
		foreach($elements as $element){
			$array[] = $element;
		}
		
		return $array;
	}
	
	public static function removeEmpty($array)
	{
		return array_diff($array, array(""));
	}
	
	public static function removeKey($array, $remove)
	{
		if(!is_array($remove)){
			$remove = array($remove);
		}
		
		foreach($array as $key => $value)
		{
			if(!in_array($key, $remove)){
				$return[$key] = $value;
			}
		}
		
		return $return;
	}
	
	public static function remove($array, $remove)
	{
		if(!is_array($remove)){
			$remove = array($remove);
		}
		
		foreach($remove as $value)
		{
			if($key = array_search($value, $array)){
				unset($array[$key]);
			}
		}
		
		return $array;
	}
	
	public static function prepend($array, $string)
	{
		foreach($array as $key => $value){
			$return[$key] = $string.$value;
		}
		
		return $return;
	}
	
	public static function append($array, $string)
	{
		foreach($array as $key => $value){
			$return[$key] = $value.$string;
		}
		
		return $return;
	}
	
	public static function keyMatch($array, $regExp)
	{
		foreach($array as $key => $value)
		{
			if(preg_match($regExp, $key)){
				$return[$key] = $value;
			}
		}
		
		return $return;
	}
	
	public static function range($low, $high, $step = 1)
	{
		$range = range($low, $high, $step);
		
		return array_combine($range, $range);
	}
	
	public static function getFirstKey($array)
	{
		foreach($array as $key => $value){
			return $key;
		}
	}
	
	public static function getFirstValue($array)
	{
		foreach($array as $key => $value){
			return $value;
		}
	}
	
	public static function intersect($array1, $array2)
	{
		$return = array();
		
		foreach($array2 as $key => $value)
		{
			if(in_array($key, $array1)){
				$return[$key] = $value;
			}
		}
		
		return $return;
	}
	
	public static function merge($arr, $arr2)
	{
		# Loop through all Elements in $arr2:
		if (is_array($arr) && is_array($arr2)) foreach ($arr2 as $k => $v){
		# Key exists in $arr and both Elemente are Arrays: Merge recursively.
		if (isset($arr[$k]) && is_array($v) && is_array($arr[$k])) $arr[$k] = self::merge($arr[$k],$v);
		# Place more Conditions here (see below)
		# ...
		# Otherwise replace Element in $arr with Element in $arr2:
		else $arr[$k] = $v;
		}
		# Return merged Arrays:
		return($arr);
	}
	
	public static function omerge()
	{
	    $args 	= func_get_args();
	   	$return = $args[0];
	   	
		for($i = 1; $i < count($args); $i++)
		{
			foreach($args[$i] as $key => $value)
			{
				if(is_array($value)){
					$return[$key] = self::omerge($return[$key], $value);
				}
				else{
					$return[$key] = (object) ((array) $return[$key] + (array) $value);
				}
			}
		}
	   
	    return $return;
	}
}