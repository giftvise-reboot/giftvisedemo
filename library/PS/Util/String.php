<?php


class PS_Util_String
{
	function truncate($str, $maxlen)
	{
		if(!is_int($maxlen)){
			return $str;
		}
		
		if(strlen($str) > abs($maxlen))
		{
			if($maxlen > 0){
				$str = substr($str, 0, $maxlen - 3)."...";
			}
			else{
				$str = "...".substr($str, $maxlen + 3);
			}
		}
		
		return $str;
	}
	
	public static function truncateHTML($str, $maxlen)
	{
		return self::truncate(html_entity_decode(strip_tags($str), ENT_COMPAT, "UTF-8"), $maxlen);
	}
	
	public static function cleanupChars($str)
	{
		return iconv("UTF-8", "ISO-8859-1//TRANSLIT", $str);
	}
	
	public static function prependURL($text, $url)
	{
		preg_match('/href="[^"]+"/i', $text, $matches, PREG_OFFSET_CAPTURE);
		
		foreach($matches as $match)
		{
			eregi('"([^"]+)"', $match[0], $regs);
			
			$matchedURL = $regs[1];
			$replace 	= 'href="'.$url.'?redirect='.urlencode($matchedURL).'"';
			$text 		= substr_replace($text, $replace, $match[1], strlen($match[0]));
		}
		
		return $text;
	}
}