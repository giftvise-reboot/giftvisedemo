<?php


class PS_Util_XML
{
	public static function entities($str)
	{
		return str_replace(array('&','"',"'",'<','>'), array('&amp;','&quot;','&apos;','&lt;','&gt;'), $str);
	}
	
	public static function CDATA($str)
	{
		if(preg_match("/[\'\"<>&]+/", $str)){
			return "<![CDATA[".$str."]]>";
		}
		
		return $str;
	}
}