<?php

class PS_Translate
{
	public static function _($messageID, $vars = NULL)
	{
		$translate 	= Zend_Registry::get("translate");
		$str		= $translate->_($messageID);
		
		if($vars !== NULL){
			$str = self::replaceVars($str, $vars);
		}
		
		return $str;
	}
	
	public static function replaceVars($str, $vars)
	{
		if(is_array($vars))
		{
			foreach($vars as $key => $value){
				$str = str_replace("%".$key."%", strval($value), $str);
			}
		}
		else{
			$str = eregi_replace("%[^%]+%", strval($vars), $str);
		}
		
		return $str;
	}
	
	public static function isTranslated($messageID)
	{
		$translate 	= Zend_Registry::get("translate");
		
		return $translate->isTranslated($messageID);
	}
}