<?php


class PS_Referrer
{
	public static function getPath()
	{
		$path = parse_url($_SERVER["REQUEST_URI"], PHP_URL_PATH);
		eregi("^(/([^/]*)?(/[^/]*)?)", $path, $regs);
		
		return rtrim($regs[1], "/");
	}
	
	public static function autoTrack()
	{
		$httpReferrer = (isset($_GET["httpReferrer"])) ? $_GET["httpReferrer"] : null;
		
		if(preg_match("/^action:(.*)/i", $httpReferrer, $regs))
		{
			$action = $regs[1];
			
			switch($action)
			{
				case "clear":
					self::clearCurrent();
					unset($httpReferrer);
					break;
			}
		}
		
		if($httpReferrer){
			self::set(self::getPath(), $httpReferrer);
		}
	}
	
	public static function get($path = NULL)
	{
		if($path === NULL)
		{
			$path = self::getPath();
			
			if(array_key_exists($path, $_SESSION["httpReferrer"])){
				return $_SESSION["httpReferrer"][$path];
			}
		}
		else
		{
			foreach($_SESSION["httpReferrer"] as $uri => $referrer)
			{
				if(eregi($path, $uri)){
					return $referrer;
				}
			}
		}
	}
	
	public static function getEncodedURI()
	{
		return rawurlencode($_SERVER["REQUEST_URI"]);
	}
	
	public static function getURI()
	{
		return $_SERVER["REQUEST_URI"];
	}
	
	public static function set($path, $url)
	{
		$_SESSION["httpReferrer"][$path] = $url;
	}
	
	public static function flush()
	{
		unset($_SESSION["httpReferrer"]);
	}
	
	public static function clear($path)
	{
		unset($_SESSION["httpReferrer"][$path]);
	}
	
	public static function clearCurrent()
	{
		unset($_SESSION["httpReferrer"][self::getPath()]);
	}
	
	public static function exists($path = NULL)
	{
		return self::get($path) ? true : false;
	}
}