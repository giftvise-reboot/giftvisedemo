<?php


class PS_Listing extends PS_Listing_Abstract implements SeekableIterator, Countable
{
	protected $countQuery;
	protected $dataQuery;
	
	protected $validParams		= array("sort", "increment", "start");
	protected $validConfig		= array("sort", "increment", "maxPages");
	
	protected $numRows;
	protected $data;
	protected $sort;
	protected $increment		= 10;
	protected $start			= 1;
	protected $end;
	protected $maxPages		 	= 11;
	
	protected $position			= 0;
	protected $count;
	
	public function __construct(
		Zend_Db_Select $countQuery,
		Zend_Db_Select $dataQuery,
		$params = array(),
		$config = array()
	)
	{
		$this->countQuery 	= $countQuery;
		$this->dataQuery 	= $dataQuery;
		
		$this->setConfig($config);
		$this->setParams($params);
		$this->queryCount();
		$this->determineEnd();
		$this->determineCount();
		$this->queryData();
	}
	
	public function setParams($params)
	{
		if(!empty($params))
		{
			foreach($params as $key => $value)
			{
				if(in_array($key, $this->validParams))
				{
					$this->{$key} = $value;
				}
			}
		}
		
		return $this;
	}
	
	public function setConfig($config)
	{
		if(!empty($config))
		{
			foreach($config as $key => $value)
			{
				if(in_array($key, $this->validConfig))
				{
					if($key == "sort" and is_string($value)){
						$value = array($value => "asc");
					}
					
					$this->{$key} = $value;
				}
			}
		}
		
		return $this;
	}
	
	protected function determineEnd()
	{
		$this->end = ($this->start + $this->increment > $this->numRows) ?
			$this->numRows :
			$this->start + $this->increment - 1;
	}
	
	protected function determineCount()
	{
		$numRows = $this->end - $this->start + 1;
		
		$this->count = ($numRows < $this->increment) ? $numRows : $this->increment;
	}
	
	protected function queryCount()
	{
		$db = Zend_Registry::get("db");
		
		$this->numRows = $db->fetchOne($this->countQuery);
	}
	
	protected function queryData()
	{
		$db = Zend_Registry::get("db");
		
		if(!empty($this->sort)){
			foreach($this->sort as $column => $direction){
				$order[] = $column." ".$direction;
			}
		}
		
		$result = $db->fetchAll(
			$this->dataQuery->limit($this->increment, $this->start - 1)
			->order($order)
		);
		
		$this->data = $result;
	}
	
	public function getNavLinks()
	{
		$returnStr = ($this->numRows > 0) ?
			Zend_Locale_Format::toNumber($this->start)." - ".Zend_Locale_Format::toNumber($this->end)." of ".Zend_Locale_Format::toNumber($this->numRows) :
			"0 - 0 of 0";
		
		$numPages	= ceil($this->numRows / $this->increment);
		$pageNum	= floor($this->start / $this->increment) + 1;
		$pagePad 	= ($this->maxPages - 1) / 2;
		$endPage 	= $pageNum + $pagePad;
		
		if($this->maxPages > $numPages)
		{
			$startPage 	= 1;
			$endPage	= $numPages;
		}
		else if($endPage > $this->maxPages)
		{
			$startPage 	= $pageNum - $pagePad;
			
			if($numPages < $endPage)
			{
				$endPage = $numPages;
			}
		}
		else
		{
			$startPage 	= 1;
			$endPage	= $this->maxPages;
		}
		
		$start = (($startPage - 1) * $this->increment) + 1;
		
		if($numPages > 1)
		{
			$requiredVars = array(
				"increment"	=> $this->increment,
				"sort"		=> $this->sort
			);
			
			$returnStr .= '<span class="separator">|</span>';
			
			if($this->start > 1)
			{
				$queryVars 	= $requiredVars + array("start" => $this->getPrevStart());
				$returnStr .= $this->createNavLink(
					"?".$this->createQuery($queryVars),
					"&lt;Prev ".$this->increment,
					"Previous ".$this->increment
				);
				
				$returnStr .= '<span class="navSpacer">&nbsp;</span>';
			}
			
			for($i = $startPage; $i <= $endPage; $i++)
			{
				$queryVars = $requiredVars + array("start" => $start);
				
				if($i == $pageNum){
					$returnStr .= $i;
				}
				else{
					$returnStr .= $this->createNavLink(
						"?".$this->createQuery($queryVars),
						$i,
						'Results '.$start.' - '.($start + $this->increment - 1)
					);
				}
				
				if($i != $endPage){
					$returnStr .= '<span class="navSpacer">&nbsp;</span>';
				}
				
				$start += $this->increment;
			}
			
			if($this->end < $this->numRows)
			{
				$returnStr .= '<span class="navSpacer">&nbsp;</span>';
				
				$queryVars 	= $requiredVars + array("start" => $this->getNextStart());
				$returnStr .= $this->createNavLink(
					"?".$this->createQuery($queryVars),
					"Next ".$this->increment."&gt;",
					"Next ".$this->increment
				);
			}
		}
		
		return $returnStr;
	}
	
	protected function createNavLink($url, $text, $title)
	{
		return '<a href="javascript:PS.listing.goTo(\''.$url.'\')" title="'.$title.'">'.$text.'</a>';
	}
	
	protected function getPrevStart()
	{
		return ($this->start - $this->increment < 1) ? 1 : ($this->start - $this->increment);
	}
	
	protected function getNextStart()
	{
		return $this->start + $this->increment;
	}
	
	protected function createQuery($vars)
	{
		return http_build_query($vars);
	}
	
	public function getStart()
	{
		return $this->start;
	}
	
	public function printCol($column, $displayName = NULL, $textOnly = false, $attr = array())
	{
		$classes = array();
		
		if(!$textOnly){
			$classes[] = "link";
		}
		
		foreach($attr as $key => $value)
		{
			if($key == "class")
			{
				if(is_array($value)){
					$classes += $value;
				}
				else{
					$classes[] = $value;
				}
			}
			else{
				$attrStr .= $key.'="'.$value.'"';
			}
		}
		
		$normalizedColumn = eregi_replace("^.*\.", "", $column);
		
		if($displayName === NULL)
		{
			if(self::$translate and PS_Translate::isTranslated($normalizedColumn)){
				$displayName = PS_Translate::_($normalizedColumn);
			}
			else{
				$displayName = PS_Util_DB::parseField($normalizedColumn);
			}
		}
		
		return '<td class="'.implode(" ", $classes).' '.
			($this->isCurrentSort($column) ? "selected" : "").'" '.$attrStr.' '.
			'onClick="PS.listing.goTo(\''.$this->getColSort($column).'\')">'.
			'<div class="column-name">'.$displayName.$this->getSortImage($column).'</div>'.
			'</td>';
	}
	
	public function printViewAll()
	{
		$query = $this->createQuery(
			array(
				"start"		=> 1,
				"increment"	=> $this->numRows,
				"sort"		=> $this->sort
			)
		);
		
		if($this->numRows <= 1000){
			return '<a href="javascript:PS.listing.goTo(\'?'.$query.'\')">View All</a>';
		}
	}
	
	protected function getColSort($column)
	{
		$sort[$column] = (!empty($this->sort) and $this->isCurrentSort($column) and $this->sort[$column] == "asc") ?
			"desc" : "asc";
		
		return "?".$this->createQuery(
			array(
				"start"		=> $this->start,
				"increment"	=> $this->increment,
				"sort"		=> $sort
			)
		);
	}
	
	protected function getSortImage($column)
	{
		if($this->isCurrentSort($column))
		{
			$image = ($this->sort[$column] == "desc") ? self::$sortDescImg : self::$sortAscImg;
			
			return '&nbsp;<img src="'.$image.'" border="0" />';
		}
	}
	
	protected function isCurrentSort($column)
	{
		return isset($this->sort[$column]);
	}
	
	public function rewind()
	{
		$this->position = 0;
	}
	
	public function current()
	{
		return $this->data[$this->position];
	}
	
	public function key()
    {
        return $this->position;
    }
    
    public function next()
    {
        ++$this->position;
    }
    
    public function valid()
    {
        return $this->position >= 0 and $this->position < $this->count;
    }
    
    public function count()
    {
        return $this->count;
    }
    
    public function seek($position)
    {
        $this->position = (int) $position;
        
        if(!$this->valid()){
            throw new OutOfBoundsException("Invalid seek position ($position)");
        }       
    }
    
    public function printAlt()
    {
    	return $this->position % 2 == 0 ? "alternative" : "";
    }
    
    public function printLast()
    {
    	return $this->position + 1 == $this->count ? "last" : "";
    }
    
    public function isEmpty()
    {
    	return $this->count == 0;
    }
    
    public function getTotal()
    {
    	return $this->numRows;
    }
}