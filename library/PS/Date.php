<?php


class PS_Date
{
	const LOCAL_DATE 		= "LOCAL_DATE";
	const LOCAL_TIME 		= "LOCAL_TIME";
	const LOCAL_DATETIME 	= "LOCAL_DATETIME";
	
	protected static function _getFormat($format)
	{
		switch($format)
		{
			case self::LOCAL_DATE:
				return Zend_Locale_Format::getDateFormat();
			case self::LOCAL_TIME:
				return Zend_Locale_Format::getTimeFormat();
			case self::LOCAL_DATETIME:
				return Zend_Locale_Format::getDateFormat()." ".Zend_Locale_Format::getTimeFormat();
		}
	}
	
	public static function convert($date, $format = self::LOCAL_DATE)
	{
		if(!$date){
			return;
		}
		
		$server = Zend_Registry::get("server");
		
		if(defined("PS_Date::".$format)){
			$format = self::_getFormat($format);
		}
		
		$date = new Zend_Date(strtotime($date." ".$server->timezone));
		
		return $date->toString($format);
	}
	
	public static function format($date, $format = self::LOCAL_DATE)
	{
		if(!$date){
			return;
		}
		
		if(defined("PS_Date::".$format)){
			$format = self::_getFormat($format);
		}
		
		$date = new Zend_Date(strtotime($date));
		
		return $date->toString($format);
	}
	
	public static function today($format = self::LOCAL_DATE)
	{
		if(defined("PS_Date::".$format)){
			$format = self::_getFormat($format);
		}
		
		$date = new Zend_Date();
		
		return $date->toString($format);
	}
	
	public static function get($date, $part)
	{
		$server = Zend_Registry::get("server");
		$date 	= new Zend_Date(strtotime($date." ".$server->timezone));
		
		return $date->get($part);
	}
	
	public static function getMonthPairs($start = 1, $end = 12, $part = Zend_Date::MONTH_NAME)
	{
		for($i = $start ; $i <= $end; $i++)
		{
			$date = new Zend_Date();
			$date->setMonth($i);
			
			$return[$i] = $date->get($part);
		}
		
		return $return;
	}
	
	public static function getMonthYearPairs($startDate = "Y-m-01", $timeframe = "+1 year")
	{
		$startTime 	= strtotime(date($startDate));
		$endTime 	= strtotime($timeframe);
		$i 			= $startTime;
		
		while($i <= $endTime)
		{
			$return[date("Ym", $i)] = date("F, Y", $i);
			
			$i = strtotime("+1 month", $i);
		}
		
		return $return;
	}
	
	public static function getCCMonthPairs($start = 1, $end = 12)
	{
		for($i = $start ; $i <= $end; $i++)
		{
			$date = new Zend_Date();
			$date->setMonth($i);
			
			$return[$i] = $date->get(Zend_Date::MONTH_NAME)." (".$date->toValue(Zend_Date::MONTH_SHORT).")";
		}
		
		return $return;
	}
	
	public static function getDayPairs($start = 1, $end = 31, $part = Zend_Date::DAY_SHORT)
	{
		for($i = $start ; $i <= $end; $i++)
		{
			$date = new Zend_Date();
			$date->setDay($i);
			
			$return[$i] = $date->toValue($part);
		}
		
		return $return;
	}
	
	public static function getYearPairs($start = NULL, $end = NULL, $part = Zend_Date::YEAR)
	{
		if($start === NULL){
			$date 	= new Zend_Date();
			$start 	= $date->toValue($part);
		}
		
		if($end === NULL){
			$end = $start + 5;
		}
		
		for($i = $start ; $i <= $end; $i++)
		{
			$date = new Zend_Date();
			$date->setYear($i);
			
			$return[$i] = $date->toValue($part);
		}
		
		return $return;
	}
	
	public static function getHourPairs($startHour = 0, $endHour = 23)
	{
		for($i = $startHour; $i <= $endHour; $i++)
		{	
			if($i == 0){
				$hour = "12 AM";
			}
			else if($i < 12){
				$hour = $i." AM";
			}
			else if($i == 12){
				$hour = "12 PM";
			}
			else if($i < 24){
				$hour = ($i - 12)." PM";
			}
			else{
				$hour = "12 AM";
			}
			
			$hours[$i] = $hour;
		}
		
		return $hours;
	}
	
	public static function getHourRangePairs($start = 0, $end = 23, $step = 1)
	{
		for($i = $start; $i <= $end; $i++)
		{	
			if($i == 0){
				$startHour = "12 AM";
			}
			else if($i < 12){
				$startHour = $i." AM";
			}
			else if($i == 12){
				$startHour = "12 PM";
			}
			else if($i < 24){
				$startHour = ($i - 12)." PM";
			}
			else{
				$startHour = "12 AM";
			}
			
			if($i + $step < 12){
				$endHour = ($i + $step)." AM";
			}
			else if($i + $step == 12){
				$endHour = "12 PM";
			}
			else if($i + $step < 24){
				$endHour = ($i + $step - 12)." PM";
			}
			else{
				$endHour = "12 AM";
			}
			
			$hours[$i] = $startHour." - ".$endHour;
		}
		
		return $hours;
	}
}