<?php

class PS_Rating
{
	public static $stars 		= 5;
	public static $split 		= 2;
	protected static $autogen 	= 1;
	
	public static function printFixed($selected)
	{
		$increment = 1 / self::$split;
		
		for($i = $increment; $i <= self::$stars; $i += $increment)
		{
			$return .= '<input class="star {split:'.self::$split.'}" type="radio" name="autogen'.self::$autogen.'" value="'.$i.'" disabled="disabled" '.($i == $selected ? 'checked="checked"' : "").' />';
		}
		
		self::$autogen++;
		
		return $return;
	}
}