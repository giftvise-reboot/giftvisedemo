<?php


class PS_View_Helper_PrintErrors extends Zend_View_Helper_Abstract
{
	public function printErrors($display = true)
	{
		if($display == false){
			return;
		}
		
		if(!empty($this->view->errors))
		{
			$str = '<div class="errors"><ul>';
			
			foreach($this->view->errors as $error)
			{
				$str .= '<li>'.$error.'</li>';
			}
			
			$str .= '</ul></div>';
		}
		
		return $str;
	}
}