<?php


class PS_View_Helper_Radio extends Zend_View_Helper_Abstract
{
	private $options = array(
		"IDPrefix"	=> "form",
		"separator" => "\n"
	);
	
	public function radio($values, $name, $selected = NULL, $default = NULL, $attrs = array(), $options = array())
	{
		if(empty($values)){
			return;
		}
		
		foreach($options as $key => $value){
			$this->options[$key] = $value;
		}
		
		foreach($attrs as $key => $value){
			$attrStr .= ' '.$key.'="'.$value.'"';
		}
		
		$counter = 1;
		
		foreach($values as $value => $label)
		{
			$checked = "";
			
			if(($value == $selected) or ($selected === NULL and $value == $default)){
				$checked = ' checked="checked"';
			}
			
			//Element ID
			$id = $this->options["IDPrefix"]."-".str_replace(array("[", "]"), "-", $name).$counter;
			
			$element =  
				'<span class="'.$this->options["IDPrefix"].'-radio">'.
				'<input id="'.$id.'" type="radio" name="'.$name.'" value="'.$this->view->escape($value).'"'.$checked.$attrStr.' />';
			
			if(isset($label)){
				$element .= '<label for="'.$id.'" class="radio">'.$label.'</label>';
			}
			
			$element .=	'</span>';
			
			$elements[] = $element;
			
			//Increment counter
			$counter++;
		}
		
		return implode($this->options["separator"] ,$elements);
	}
}