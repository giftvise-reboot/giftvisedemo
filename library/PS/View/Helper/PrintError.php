<?php


class PS_View_Helper_PrintError extends Zend_View_Helper_Abstract
{
	public function printError($fieldNames)
	{
		if(!is_array($fieldNames)){
			$fieldNames = array($fieldNames);
		}
		
		foreach($fieldNames as $fieldName)
		{
			if($this->view->fields->exists($fieldName))
			{
				$field = $this->view->fields->{$fieldName};
				
				if(!$field->isValid())
				{
					$errorsExist = true;
					
					foreach($field->getErrors() as $message){
						$str .= '<li>'.$message.'</li>';
					}
				}
			}
		}
		
		if($errorsExist){
			return '<div class="fieldError"><ul>'.$str.'</ul></div>';
		}
	}
}