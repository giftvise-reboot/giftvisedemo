<?php


class PS_View_Helper_PrintAlt extends Zend_View_Helper_Abstract
{
	public function printAlt($index)
	{
		return $index % 2 == 0 ? "alternative" : "";
	}
}