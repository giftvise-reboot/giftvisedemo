<?php


class PS_View_Helper_Select extends Zend_View_Helper_Abstract
{
	public function select($values, $selected = NULL, $css = NULL)
	{
		if(empty($values)){
			return;
		}
		
		if(is_array($selected))
		{
			foreach($selected as $key => $value){
				$temp[$key] = strval($value);
			}
			
			$selected = $temp;
		}
		else{
			$selected = strval($selected);
		}
		
		foreach($values as $key => $value)
		{
			if(is_array($value))
			{
				$options .= '<optgroup label="'.$key.'">';
			
				foreach($value as $subKey => $subValue)
				{
					$subKey = strval($subKey);
					
					if((is_array($selected) and in_array($subKey, $selected, true)) or ($subKey === $selected)){
						$selectStr = ' selected="selected"';
					}
					else{
						$selectStr = "";
					}
					
					$class = $css[$subKey] ? $css[$subKey] : "";
					
					$options .= '<option value="'.$subKey.'"'.$selectStr.' class="'.$class.'">'.$this->view->escape($subValue).'</option>'."\n";
				}
				
				$options .= '</optgroup>';
			}
			else
			{
				$key = strval($key);
				
				if((is_array($selected) and in_array($key, $selected, true)) or ($key === $selected)){
					$selectStr = ' selected="selected"';
				}
				else{
					$selectStr = "";
				}
				
				$class = $css[$key] ? $css[$key] : "";
				
				$options .= '<option value="'.$key.'"'.$selectStr.' class="'.$class.'">'.$this->view->escape($value).'</option>'."\n";
			}
		}
		
		return $options;
	}
}