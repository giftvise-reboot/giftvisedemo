<?php


class PS_View_Helper_Checked extends Zend_View_Helper_Abstract
{
	public function checked($elementVal, $currentVal)
	{
		if($currentVal == $elementVal or in_array($elementVal, $currentVal)){
			return 'checked="checked"';
		}
	}
}