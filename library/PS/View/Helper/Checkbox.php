<?php


class PS_View_Helper_Checkbox extends Zend_View_Helper_Abstract
{
	private $options = array(
		"IDPrefix"		=> "form",
		"separator" 	=> "\n",
		"arrayBrackets"	=> true
	);
	
	public function checkbox($values, $name, $selected = NULL, $default = NULL, $attrs = array(), $options = array())
	{
		if(empty($values)){
			return;
		}
		
		foreach($options as $key => $value){
			$this->options[$key] = $value;
		}
		
		foreach($attrs as $key => $value){
			$attrStr = ' '.$key.'="'.$value.'"';
		}
		
		if(!is_array($values)){
			$values 	= array($values => NULL);
			$singular 	= true;
		}
		else if(count($values) == 1){
			$singular = true;
		}
		
		$counter = 1;
		
		foreach($values as $value => $label)
		{
			$checked = "";
			
			if(	($singular and $value == $selected) or
				in_array($value, $selected) or (
					($selected === NULL) and (
						($singular and $value == $default) or
						in_array($value, $default)
					)
				)
			){
				$checked = ' checked="checked"';
			}
			
			//Element ID
			$id = $this->options["IDPrefix"]."-".$name;
			
			if(!$singular)
			{
				$id .= $counter;
				
				if($this->options["arrayBrackets"]){
					$brackets = "[]";
				}
			}
			
			$element =  
				'<span class="'.$this->options["IDPrefix"].'-checkbox">'.
				'<input id="'.$id.'" type="checkbox" name="'.$name.$brackets.'" value="'.$value.'"'.$checked.$attrStr.' />';
			
			if(isset($label)){
				$element .= '<label for="'.$id.'" class="checkbox">'.$label.'</label>';
			}
			
			$element .=	'</span>';
			
			$elements[] = $element;
				
			//Increment counter
			$counter++;
		}
		
		return implode($this->options["separator"], $elements);
	}
}