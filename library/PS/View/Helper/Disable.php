<?php


class PS_View_Helper_Disable extends Zend_View_Helper_Abstract
{
	public function disable($value)
	{
		if($value){
			return 'disabled="disabled"';
		}
	}
}