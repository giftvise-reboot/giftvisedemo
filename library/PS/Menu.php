<?php


class PS_Menu
{
	private $ID;
	private $children 	= array();
	private $childID 	= 1;
	private $display;
	
	function __construct($ID, $display = true)
	{
		$this->ID 		= $ID;
		$this->display	= $display;
	}
	
	public function open()
	{
		return;
	}
	
	public function getID()
	{
		return $this->ID;
	}
	
	public function addTab(PS_Menu_Tab $tab)
	{
		if($tab->displayable() and $tab->display)
		{
			//Add to children
			$this->children[] = $tab;
			
			//Set child ID
			$tab->setID($this->childID++);
			
			//Set pointer back to parent
			$tab->setParent($this);
		}
		
		return $this;
	}
	
	public function addTabs()
	{
		foreach(func_get_args() as $tab){
			$this->addTab($tab);
		}
		
		return $this;
	}
	
	public function displayable()
	{
		foreach($this->children as $child)
		{
			if($child->displayable()){
				return true;
			}
		}
		
		return false;
	}
	
	public function display()
	{
		if(!$this->children or !$this->display){
			return;
		}
		
		foreach($this->children as $child){
			$returnStr .= $child->display();
		}
		
		return $returnStr;
	}
}