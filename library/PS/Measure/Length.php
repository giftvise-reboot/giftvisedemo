<?php


class PS_Measure_Length
{
	public static function convertTo($value, $orgUnits, $newUnits)
	{
		$measure 	= new Zend_Measure_Length($value, $orgUnits);
		$converted	= $measure->convertTo($newUnits);
		
		return $converted;
	}
}