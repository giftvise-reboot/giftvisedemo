<?php


class PS_Measure_Binary
{
	public static function getBestUnit($value)
	{
		if($value < 1024){
			return Zend_Measure_Binary::BYTE;
		}
		else if($value / 1024 < 1024){
			return Zend_Measure_Binary::KILOBYTE;
		}
		else if($value / 1024 / 1024 < 1024){
			return Zend_Measure_Binary::MEGABYTE;
		}
		else if($value / 1024 / 1024 / 1024 < 1024){
			return Zend_Measure_Binary::GIGABYTE;
		}
		else{
			return Zend_Measure_Binary::TERABYTE;
		}
	}
	
	public static function getUnitAbbr($unit)
	{
		$measure 	= new Zend_Measure_Binary();
		$conv		= $measure->getConversionList();
		
		return $conv[$unit][1];
	}
	
	public static function toBest($value, $showUnit = true)
	{
		$measure 	= new Zend_Measure_Binary($value);
		$converted	= $measure->convertTo(
			self::getBestUnit($value)
		);
		
		if(!$showUnit){
			$converted = floatval($converted);
		}
		
		return $converted;
	}
	
	public static function convertTo($values, $unit, $showUnit = true)
	{
		if(is_array($values))
		{
			foreach($values as $value){
				$measure 	= new Zend_Measure_Binary($value);
				$converted	= $measure->convertTo($unit);
				
				if(!$showUnit){
					$converted = floatval($converted);
				}
				
				$return[] = $converted;
			}
			
			return $return;
		}
		
		$measure 	= new Zend_Measure_Binary($values);
		$converted	= $measure->convertTo($unit);
				
		if(!$showUnit){
			$converted = floatval($converted);
		}
		
		return $converted;
	}
}