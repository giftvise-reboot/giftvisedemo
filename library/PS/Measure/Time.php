<?php


class PS_Measure_Time
{
	public static function breakdown($seconds)
	{
		$return = array(
			"years" 	=> 0,
			"days" 		=> 0,
			"hours" 	=> 0,
			"minutes" 	=> 0,
			"seconds" 	=> 0
		);
		
		if($seconds >= 31556926){
			$return["years"] = floor($seconds / 31556926);
			$seconds = ($seconds % 31556926);
		}
		
		if($seconds >= 86400){
			$return["days"] = floor($seconds / 86400);
			$seconds = ($seconds % 86400);
		}
		
		if($seconds >= 3600){
			$return["hours"] = floor($seconds / 3600);
			$seconds = ($seconds % 3600);
		}
		
		if($seconds >= 60){
			$return["minutes"] = floor($seconds / 60);
			$seconds = ($seconds % 60);
		}
		
		$return["seconds"] = floor($seconds);
		
		return $return;
	}
	
	public static function parseSeconds($seconds, $granularity = array("years", "days", "hours", "minutes", "seconds"))
	{
		$breakdown = self::breakdown($seconds);
		
		if($breakdown["years"] and in_array("years", $granularity))
		{
			$unit = new Zend_Measure_Time($breakdown["years"], Zend_Measure_Time::YEAR);
			
			$return[] = $unit->toString();
		}
		
		if($breakdown["days"] and in_array("days", $granularity))
		{
			$unit = new Zend_Measure_Time($breakdown["days"], Zend_Measure_Time::DAY);
			
			$return[] = $unit->toString();
		}
		
		if($breakdown["hours"] and in_array("hours", $granularity))
		{
			$unit = new Zend_Measure_Time($breakdown["hours"], Zend_Measure_Time::HOUR);
			
			$return[] = $unit->toString();
		}
		
		if($breakdown["minutes"] and in_array("minutes", $granularity))
		{
			$unit = new Zend_Measure_Time($breakdown["minutes"], Zend_Measure_Time::MINUTE);
			
			$return[] = $unit->toString();
		}
		
		if($breakdown["seconds"] and in_array("seconds", $granularity))
		{
			$unit = new Zend_Measure_Time($breakdown["seconds"], Zend_Measure_Time::SECOND);
			
			$return[] = $unit->toString();
		}
		
		return implode(", ", $return);
	}
}