<?php


class PS_Validate_Decimal extends Zend_Validate_Abstract
{
	const INVALID 		= "decimalInvalid";
	const POSITIVE 		= "decimalPositive";
	const NON_NEGATIVE 	= "decimalNonNegative";
	
	protected $_messageTemplates = array(
		self::INVALID 		=> "Value is not a number",
		self::POSITIVE 		=> "Number must be positive",
		self::NON_NEGATIVE 	=> "Number must be non-negative"
	);
	
	protected $restrict;
	
	public function __construct($restrict = NULL)
	{
		$this->setRestrict($restrict);
	}
	
	public function setRestrict($restrict)
	{
		$this->restrict = $restrict;
        return $this;
	}
	
	public function isValid($value)
    {
        $valueString = (string) $value;

        $this->_setValue($valueString);

        $locale = localeconv();
		
		//Filter value
        $valueFiltered = str_replace($locale["thousands_sep"], "", $valueString);
		
		//Validate number
        if(strval((float)($valueFiltered)) != $valueFiltered){
            $this->_error(self::INVALID);
            return false;
        }
		
		//Validate positive/negative
		if($this->restrict)
		{
			switch($this->restrict)
			{
				case self::POSITIVE:
					if($valueFiltered <= 0){
						$this->_error(self::POSITIVE);
			            return false;
					}
					break;
				case self::NON_NEGATIVE:
					if($valueFiltered < 0){
						$this->_error(self::NON_NEGATIVE);
			            return false;
					}
					break;
			}
		}
		
        return true;
    }
}