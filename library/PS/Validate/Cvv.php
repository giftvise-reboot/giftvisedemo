<?php


class PS_Validate_Cvv extends Zend_Validate_Abstract
{
    const INVALID = "cvvInvalid";

    protected $_messageTemplates = array(
        self::INVALID => "Value must be a 3 - 4 digit number"
    );
    
    public function isValid($value)
    {
		$this->_setValue($value);
		
		if(!eregi("^[0-9]{3,4}$", $value)){
			$this->_error();
			return false;
		}
		
		return true;
    }
}