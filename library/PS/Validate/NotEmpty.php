<?php


class PS_Validate_NotEmpty extends Zend_Validate_NotEmpty
{
	public function __construct()
	{
		$this->setMessage("Value is empty", self::IS_EMPTY);		
	}
}