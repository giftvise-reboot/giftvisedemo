<?php


class PS_Validate_Currency extends Zend_Validate_Abstract
{
    const INVALID = "currencyInvalid";

    protected $_messageTemplates = array(
        //self::INVALID => "Value is not valid for currency"
		self::INVALID => "Please do not include any special characters"
    );
    
    public function isValid($value)
    {
        $this->_setValue($value);
		
		//Filter
		$value = str_replace("$", "", $value);
		
		if(!eregi("^[0-9]*(\.[0-9]{0,2})?$", $value) or $value < 0){
			$this->_error();
			return false;
		}
		
		return true;
    }
}