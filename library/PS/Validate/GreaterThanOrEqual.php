<?php


class PS_Validate_GreaterThanOrEqual extends Zend_Validate_Abstract
{
    const INVALID = "notGreaterThanOrEqual";

    protected $_messageTemplates = array(
        self::INVALID => "Value must be >= %min%"
    );
    
    protected $_messageVariables = array(
        "min" => "min"
    );
    
    protected $min;
    
    public function __construct($min)
    {
        $this->setMin($min);
    }
    
    public function setMin($min)
    {
        $this->min = $min;
        return $this;
    }
    
    public function isValid($value)
    {
        $this->_setValue($value);
		
		if($value < $this->min){
			$this->_error();
			return false;
		}
		
		return true;
    }
}