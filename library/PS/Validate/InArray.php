<?php


class PS_Validate_InArray extends Zend_Validate_InArray
{
	public function __construct()
	{
		//Get method arguments
		$args = func_get_args();
		
		//Call parent constructor
		call_user_func_array(array("parent", "__construct"), $args);
		
		//Override messages
		$this->setMessage( "Value is not a valid option");
	}
}