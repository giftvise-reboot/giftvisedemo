<?php


class PS_Validate_Username extends Zend_Validate_Abstract
{
    const LENGTH 	= "usernameLength";
    const INVALID 	= "usernameInvalid";

	protected $_messageTemplates = array(
		self::LENGTH 	=> "Length must be %min% to %max% characters",
		self::INVALID 	=> "Spaces not allowed"
	);
    
	protected $_messageVariables = array(
		"min" => "min",
		"max" => "max"
	);
    
    protected $min 	= 1;
    protected $max	= 255;
	
	public function __construct($options = array())
	{
		if(array_key_exists("min", $options)){
			$this->min = $options["min"];
		}
		
		if(array_key_exists("max", $options)){
			$this->min = $options["max"];
		}
	}
        
    public function isValid($value)
    {
        $this->_setValue($value);
		
        $valid = true;
        
        //Check length
        if(strlen($value) < $this->min or strlen($value) > $this->max){
        	$this->_error(self::LENGTH);
        	$valid = false;
        }
        
		//Check if spaces exist
        if(eregi("[[:space:]]+", $value)){
			$this->_error(self::INVALID);
			$valid = false;
        }

        return $valid;
    }
}