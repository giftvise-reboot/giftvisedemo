<?php


class PS_Validate_PregMatch extends Zend_Validate_Abstract
{
    const INVALID = "stringInvalid";

    protected $_messageTemplates = array(
        self::INVALID => "Value contains invalid characters"
    );
    
    protected $regExp;
    
    public function __construct($regExp)
	{
		$this->regExp = $regExp;
	}
    
    public function isValid($value)
    {
        $this->_setValue($value);
		
		if(!preg_match($this->regExp, $value)){
			return false;
		}
		
		return true;
    }
}