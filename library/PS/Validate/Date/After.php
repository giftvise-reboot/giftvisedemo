<?php


class PS_Validate_Date_After extends Zend_Validate_Abstract
{
    const NOT_AFTER 			= "dateNotAfter";
    const NOT_AFTER_OR_EQUAL 	= "dateNotAfterOrEqual";

    protected $_messageTemplates = array(
        self::NOT_AFTER 			=> "Date must be after %date%",
        self::NOT_AFTER_OR_EQUAL 	=> "Date must be after or equal to %date%"
    );
    protected $_messageVariables = array(
		"date" => "compareDate"
	);
    protected $compareDate;
    protected $canBeEqual;
    
	public function __construct($date, $canBeEqual = false)
	{
		$this->compareDate 	= $date;
		$this->canBeEqual 	= $canBeEqual;
	}
    
    public function isValid($value)
    {
        $this->_setValue($value);
		
		if($this->canBeEqual)
		{
			if($this->compareDate and strtotime($value) < strtotime($this->compareDate)){
				$this->_error(self::NOT_AFTER_OR_EQUAL);
				return false;
			}
		}
		else
		{
			if($this->compareDate and strtotime($value) <= strtotime($this->compareDate)){
				$this->_error(self::NOT_AFTER);
				return false;
			}
		}
		
		return true;
    }
}