<?php


class PS_Validate_Date_Future extends Zend_Validate_Abstract
{
    const NOT_FUTURE_STRICT = "dateNotInFuture";
    const NOT_FUTURE		= "dateNotInFutureOrToday";

    protected $_messageTemplates = array(
        self::NOT_FUTURE_STRICT => "Date must be in the future",
        self::NOT_FUTURE 		=> "Date must be set to today or beyond"
    );
	protected $strict;
  	
  	public function __construct($strict = true)
	{
		$this->strict = $strict;
	}
  	
    public function isValid($value)
    {
        $this->_setValue($value);
		
		$date 			= strtotime($value);
		$currentDate 	= strtotime(date("Y-m-d"));
		
		if($this->strict)
		{
			if($date <= $currentDate){
				$this->_error(self::NOT_FUTURE_STRICT);
				return false;
			}
		}
		else{
			if($date < $currentDate){
				$this->_error(self::NOT_FUTURE);
				return false;
			}
		}
		
		return true;
    }
}