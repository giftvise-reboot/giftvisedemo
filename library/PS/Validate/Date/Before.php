<?php


class PS_Validate_Date_Before extends Zend_Validate_Abstract
{
    const NOT_BEFORE = "dateNotBefore";

    protected $_messageTemplates = array(
        self::NOT_BEFORE => "Date must be before %date%"
    );
    protected $_messageVariables = array(
		"date" => "compareDate"
	);
    protected $compareDate;
    
	public function __construct($date)
	{
		$this->compareDate = $date;
	}
    
    public function isValid($value)
    {
        $this->_setValue($value);
		
		if($this->compareDate and strtotime($value) >= strtotime($this->compareDate)){
			$this->_error(self::NOT_BEFORE);
			return false;
		}
		
		return true;
    }
}