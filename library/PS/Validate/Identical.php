<?php


class PS_Validate_Identical extends Zend_Validate_Identical
{
	public function __construct()
	{
		//Get method arguments
		$args = func_get_args();
		
		//Call parent constructor
		call_user_func_array(array("parent", "__construct"), $args);
		
		//Override messages
		$this->setMessages(
			array(
				 self::MISSING_TOKEN => "No value was provided to match against"
			)
		);
	}
}