<?php


class PS_Validate_Birthdate extends Zend_Validate_Abstract
{
    const INVALID = "birthdateInvalid";

    protected $_messageTemplates = array(
        self::INVALID => "Birthdate is not valid"
    );
    
    public function isValid($value)
    {
		$this->_setValue($value);
		
			$birthTimestamp = strtotime($value);
			
			if($birthTimestamp < strtotime("-120 years") or $birthTimestamp > time()){
				$this->_error();
				return false;
			}
			return true;
		
    }
}