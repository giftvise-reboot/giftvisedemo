<?php


class PS_Validate_Ccnum extends Zend_Validate_Ccnum
{
	public function __construct()
	{
		//Override messages
		$this->setMessages(
			array(
				self::LENGTH => "Must contain between 13 and 19 digits",
				self::CHECKSUM => "Number is not valid"
			)
		);
	}
}