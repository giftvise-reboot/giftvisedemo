<?php


class PS_Validate_Phone extends Zend_Validate_Abstract
{
    const INVALID = "phoneInvalid";

    protected $_messageTemplates = array(
        self::INVALID => "Number must be at least 10 digits"
    );
    
    public function isValid($value)
    {
        $this->_setValue($value);
		
		if(!eregi("^[0-9]{10,}$", eregi_replace("[^0-9]+", "", $value))){
			$this->_error();
			return false;
		}
		
		return true;
    }
}