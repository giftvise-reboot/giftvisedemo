<?php


class PS_Validate_Password extends Zend_Validate_Abstract
{
    const LENGTH 	= "passwordLength";
    const INVALID 	= "passwordInvalid";
    const DIGIT 	= "passwordDigitRequired";
    const SPECIAL 	= "passwordSpecialRequired";

	protected $_messageTemplates = array(
		self::LENGTH 	=> "Length must be %min% characters or more",
		self::DIGIT 	=> "Value must contain at least one digit",
		self::SPECIAL 	=> "Value must contain at least one special character (@,$,%,& etc.)",
		self::INVALID 	=> "Spaces not allowed"
	);
    
	protected $_messageVariables = array(
		"min" => "min"
	);
    
    protected $min;
    protected $digitRequired 	= false;
    protected $specialRequired 	= false;
	
	public function __construct($options = array())
	{
		if(array_key_exists("min", $options)){
			$this->setMin($options["min"]);
		}
		
		if(array_key_exists("digitRequired", $options)){
			$this->setDigitRequired($options["digitRequired"]);
		}
		
		if(array_key_exists("specialRequired", $options)){
			$this->setSpecialRequired($options["specialRequired"]);
		}
	}
    
    /**
     * Returns the min option
     *
     * @return int
     */
    public function getMin()
    {
        return $this->min;
    }

    /**
     * Sets the min option
     *
     * @param  int $min
     * @return PS_Validate_Password
     */
    public function setMin($min)
    {
        $this->min = $min;
        return $this;
    }
    
    public function getDigitRequired()
    {
    	return $this->digitRequired;
    }
    
    public function setDigitRequired($required)
    {
        $this->digitRequired = $required;
        return $this;
    }
    
    public function getSpecialRequired()
    {
    	return $this->specialRequired;
    }
    
    public function setSpecialRequired($required)
    {
        $this->specialRequired = $required;
        return $this;
    }
    
    public function isValid($value)
    {
        $this->_setValue($value);
		
        $valid = true;
        
        //Check length
        if(isset($this->min) and strlen($value) < $this->min){
        	$this->_error(self::LENGTH);
        	$valid = false;
        }
        
        //Check for digit (optional)
		if($this->digitRequired and !eregi("[0-9]+", $value)){
			$this->_error(self::DIGIT);
			$valid = false;
		}
		
		//Check for special character (optional)
		if($this->specialRequired and !eregi("[+=!@#$%^&*()_|<>?,./~{}-]+", $value)){
			$this->_error(self::SPECIAL);
			$valid = false;
		}
        
		//Check if spaces exist
        if(eregi("[[:space:]]+", $value)){
			$this->_error(self::INVALID);
			$valid = false;
        }

        return $valid;
    }
}