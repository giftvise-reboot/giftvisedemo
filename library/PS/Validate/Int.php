<?php


class PS_Validate_Int extends Zend_Validate_Abstract
{
	const INVALID 		= "intInvalid";
	const POSITIVE 		= "intPositive";
	const NON_NEGATIVE 	= "intNonNegative";
	const NON_ZERO	 	= "intNonZero";
	
	protected $_messageTemplates = array(
		self::INVALID 		=> "Value is not an integer",
		self::POSITIVE 		=> "Number must be positive",
		self::NON_NEGATIVE 	=> "Number must be non-negative",
		self::NON_ZERO	 	=> "Number cannot be zero"
	);
	
	protected $restrict;
	
	public function __construct($restrict = NULL)
	{
		$this->setRestrict($restrict);
	}
	
	public function setRestrict($restrict)
	{
		$this->restrict = $restrict;
        return $this;
	}
	
	public function isValid($value)
    {
        $valueString = (string) $value;

        $this->_setValue($valueString);

        $locale = localeconv();

        $valueFiltered = str_replace($locale['decimal_point'], '.', $valueString);
        $valueFiltered = str_replace($locale['thousands_sep'], '', $valueFiltered);
		
        if(strval(intval($valueFiltered)) != $valueFiltered){
            $this->_error(self::INVALID);
            return false;
        }
		
		if($this->restrict)
		{
			switch($this->restrict)
			{
				case self::POSITIVE:
					if($valueFiltered < 1){
						$this->_error(self::POSITIVE);
			            return false;
					}
					break;
				case self::NON_NEGATIVE:
					if($valueFiltered < 0){
						$this->_error(self::NON_NEGATIVE);
			            return false;
					}
					break;
				case self::NON_ZERO:
					if($valueFiltered == 0){
						$this->_error(self::NON_ZERO);
			            return false;
					}
					break;
			}
		}
		
        return true;
    }
}