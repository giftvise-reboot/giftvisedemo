<?php


class PS_Validate_Password_MD5 extends Zend_Validate_Abstract
{
    const INVALID = "passwordIncorrect";

	protected $_messageTemplates = array(
		self::INVALID => "Password is incorrect"
	);
    
    protected $hash;
    protected $salt;
	
	public function __construct($hash, $salt = NULL)
	{
		$this->setHash($hash);
		$this->setSalt($salt);
	}
    
    public function setHash($hash)
    {
        $this->hash = $hash;
        return $this;
    }
    
    public function setSalt($salt)
    {
        $this->salt = $salt;
        return $this;
    }
    
    public function isValid($value)
    {
        $this->_setValue($value);
		
		//Check if password matches hash
        if(isset($this->salt))
        {
        	if(md5($this->salt.$value) != $this->hash){
				$this->_error(self::INVALID);
				return false;
			}
        }
        else{
        	if(md5($value) != $this->hash){
        		$this->_error(self::INVALID);
        		return false;
        	}
        }

        return true;
    }
}