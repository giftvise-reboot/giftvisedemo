<?php


class PS_Validate_DateTime extends Zend_Validate_Abstract
{
    const INVALID = "dateTimeInvalid";

    protected $_messageTemplates = array(
        self::INVALID => "Date format must be MM/DD/YYYY HH:MM:SS"
    );
    
    public function isValid($value)
    {
        $this->_setValue($value);
		
		if(!eregi("^[0-9]{1,2}/[0-9]{1,2}/[0-9]{2,4} [0-9]{1,2}:[0-9]{1,2}:[0-9]{1,2} (am|pm)$", $value)){
			$this->_error();
			return false;
		}
		
		return true;
    }
}