<?php


class PS_Validate_Array extends Zend_Validate_Abstract
{
    const MIN_LENGTH 	= "arrayMinLength";
    const MAX_LENGTH 	= "arrayMaxLength";
    const NOT_ARRAY 	= "notAnArray";
    const NOT_IN_ARRAY 	= "notInArray";

	protected $_messageTemplates = array(
		self::MIN_LENGTH 	=> "List must contain at least %min% items",
		self::MAX_LENGTH 	=> "List cannot contain more than %min% items",
		self::NOT_ARRAY 	=> "Value is not a list",
		self::NOT_IN_ARRAY 	=> "Value is not a valid option"
	);
    
	protected $_messageVariables = array(
		"list"	=> "list",
		"min" 	=> "min",
		"max" 	=> "max"
	);
    
    protected $min;
    protected $max;
	
	public function __construct($options = array())
	{
		if(array_key_exists("list", $options)){
			$this->setList($options["list"]);
		}
		
		if(array_key_exists("min", $options)){
			$this->setMin($options["min"]);
		}
		
		if(array_key_exists("max", $options)){
			$this->setMin($options["max"]);
		}
	}
	
	public function getList()
    {
        return $this->list;
    }
	
	public function setList($list)
    {
        $this->list = $list;
        
        return $this;
    }
    
    public function getMin()
    {
        return $this->min;
    }

    public function setMin($min)
    {
        $this->min = $min;
        
        return $this;
    }
    
    public function getMax()
    {
        return $this->max;
    }

    public function setMax($max)
    {
        $this->max = $max;
        
        return $this;
    }
    
    public function isValid($value)
    {
        $this->_setValue($value);
		
        $valid = true;
        
        if(!is_array($value)){
        	$this->_error(self::NOT_ARRAY);
        	$valid = false;
        }
        
        //Check if values are in the list
        if(isset($this->list))
        {
        	foreach($value as $item)
        	{
        		if(!in_array($item, $this->list)){
        			$this->_error(self::NOT_IN_ARRAY);
		        	$valid = false;
        		}
        	}
        }
        
        //Check length
        if(isset($this->min) and count($value) < $this->min){
        	$this->_error(self::MIN_LENGTH);
        	$valid = false;
        }
        
        if(isset($this->max) and count($value) > $this->max){
        	$this->_error(self::MAX_LENGTH);
        	$valid = false;
        }
        
        return $valid;
    }
}