<?php


class PS_Validate_Date extends Zend_Validate_Abstract
{
    const INVALID = "dateInvalid";

    protected $_messageTemplates = array(
        self::INVALID => "Date format must be MM/DD/YYYY"
    );
    
    public function isValid($value)
    {
        $this->_setValue($value);
		
		if(!eregi("^[0-9]{1,2}/[0-9]{1,2}/[0-9]{2,4}$", $value)){
			$this->_error();
			return false;
		}
		
		return true;
    }
}