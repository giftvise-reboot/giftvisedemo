<?php


class PS_Validate_EmailAddress extends Zend_Validate_EmailAddress
{
	public function __construct()
	{
		//Get method arguments
		$args = func_get_args();
		
		//Call parent constructor
		call_user_func_array(array("parent", "__construct"), $args);
		
		//Override messages
		$this->setMessages(
			array(
				self::INVALID => "Email is invalid"
			)
		);
	}
}