<?php


class PS_Validate_PostalCode extends Zend_Validate_Abstract
{
    const INVALID = "postalCodeInvalid";

    protected $_messageTemplates = array(
        self::INVALID => "Postal code is invalid"
    );
    
    protected $strict;
    
	public function __construct($strict = false)
	{
		$this->strict = $strict;
	}
    
    public function isValid($value)
    {
        $this->_setValue($value);
		
		if(!eregi("^[0-9]{5}(-[0-9]{4})".($this->strict ? "" : "?")."$", $value)){
			$this->_error(self::INVALID);
			return false;
		}
		
		return true;
    }
}