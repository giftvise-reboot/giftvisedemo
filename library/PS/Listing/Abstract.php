<?php

abstract class PS_Listing_Abstract
{
	protected static $translate 	= false;
	protected static $sortAscImg 	= "/images/sortup.gif";
	protected static $sortDescImg 	= "/images/sortdown.gif";
	
	public static function lookForTranslation()
	{
		self::$translate = true;
	}
	
	public static function setImages($sortDown, $sortUp)
	{
		self::$sortAscImg 	= $sortDown;
		self::$sortDescImg 	= $sortUp;
	}
}