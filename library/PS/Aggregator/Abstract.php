<?php


abstract class PS_Aggregator_Abstract
{
	static protected $options = array();
	
	public static function setOptions($options)
	{
		foreach($options as $key => $value)
		{
			if(array_key_exists($key, self::$options)){
				self::$options[$key] = $value;
			}
		}
	}
	
	protected static function getNewestTime($files)
	{
		$max = 0;
		
		foreach($files as $file)
		{
			$time = filemtime($file);
			
			if($time > $max){
				$max = $time;
			}
		}
		
		return $max;
	}
	
	protected static function cache($files, $extension)
	{
		$dir	= Zend_Registry::get("dir");
		$aggDir = PS_Util_File::mergePaths($dir->webroot, "aggregated");
		
		$groups	= array_keys($files);
		
		foreach($groups as $group)
		{
			$files 		= $files[$group];
			$groupHash 	= md5(implode("|", $files));
			$aggFile	= $groupHash.$extension;
			$aggPath	= PS_Util_File::mergePaths($aggDir, $aggFile);
			$aggFiles[]	= PS_Util_File::mergePaths("/aggregated", $aggFile);
			
			if(!file_exists($aggPath) or (filemtime($aggPath) < self::getNewestTime($files)) or true)
			{
				foreach($files as $file){
					$contents .= file_get_contents(PS_Util_File::mergePaths($dir->webroot, $file))."\n\n";
				}
				
				//Write cache
				file_put_contents($aggPath, $contents);
			}
		}
		
		return $aggFiles;
	}
}