<?php


class PS_Aggregator_JS extends PS_Aggregator_Abstract
{
	static protected $files 	= array();
	static protected $extension = ".js";
	
	public static function addFile($file, $group = "_default")
	{
		self::$files[$group][] = $file;
	}
	
	public static function addFiles($files, $group = "_default")
	{
		foreach($files as $file){
			self::$files[$group][] = $file;
		}
	}
	
	public static function get($host)
	{
		$files = self::cache(self::$files, self::$extension);
		
		foreach($files as $file){
			$return .= '<script type="text/javascript" src="'.PS_Util_File::mergePaths($host, $file).'"></script>';
		}
		
		return $return;
	}
}