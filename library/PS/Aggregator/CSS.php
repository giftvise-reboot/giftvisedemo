<?php


class PS_Aggregator_CSS extends PS_Aggregator_Abstract
{
	static protected $files 	= array();
	static protected $extension = ".css";
	
	public static function addFile($file, $group = "_default")
	{
		self::$files[$group][] = $file;
	}
	
	public static function addFiles($files, $group = "_default")
	{
		foreach($files as $file){
			self::$files[$group][] = $file;
		}
	}
	
	public static function get($host)
	{
		$files = self::cache(self::$files, self::$extension);
		
		foreach($files as $file){
			$return .= '<link href="'.PS_Util_File::mergePaths($host, $file).'" media="all" rel="stylesheet" type="text/css" />';
		}
		
		return $return;
	}
}