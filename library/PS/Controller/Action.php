<?php


class PS_Controller_Action extends Zend_Controller_Action
{
	public function init()
	{
		$config = Zend_Registry::get("config");
		$server = Zend_Registry::get("server");
		$dir 	= Zend_Registry::get("dir");
		
		//Helpers
		$this->view->setHelperPath("../library/PS/View/Helper/", "PS_View_Helper");
		
		//Add to view
		$this->view->errors 	= array();
		$this->view->request 	= (object) $this->_getAllParams();
		$this->view->config		= $config;
		$this->view->server		= $server;
		$this->view->dir		= $dir;

		// Code to run jquery function just after every hour of user login.
		if(isset($_SESSION["user"]))
		{
			$logintime = strtotime($_SESSION["user"]["lastvisitDate"]) + 3600;
			// Difference in milisecond
			$difftime = ($logintime - time()) * 1000;
			$this->view->timeleftinhour = $difftime;
		}		
		// End Code
		
		//Add action helpers
		Zend_Controller_Action_HelperBroker::addHelper(
			new PS_Controller_Action_Helper_LayoutResources($this, "/layouts")
		);
		Zend_Controller_Action_HelperBroker::addHelper(
			new PS_Controller_Action_Helper_Resources($this, "/resources", array("minified" => true))
		);
		
		Zend_Controller_Action_HelperBroker::addHelper(
			new PS_Controller_Action_Helper_TranslateController()
		);
		Zend_Controller_Action_HelperBroker::addHelper(
			new PS_Controller_Action_Helper_TranslateLayout()
		);
		
		//Add serialized context
		$context = $this->_helper->contextSwitch();
		
		if(!$context->hasContext("serialized"))
		{
			$context->addContext("serialized",
				array(
					"suffix"    => "serial",
					"headers"   => array("Content-Type" => "text/plain"),
					"callbacks" => array(
						"init" => array($this, "initSerializedContext"),
						"post" => array($this, "postSerializedContext")
					)
				)
			);
		}
		
		//Set meta title
		if(!$this->view->headTitle()->count()){
			$this->view->headTitle($config->sitename)->setSeparator(" : ");
		}
	}
	
	//Serialized context
	
	public function initSerializedContext()
	{
		$this->_helper->viewRenderer->setNoRender();
	}
	
	public function postSerializedContext()
    {
		$vars = serialize($this->view->getVars());
		$this->getResponse()->setBody($vars);
	}
	
	//Javascript
	
	protected function prependJS($file)
	{
		$this->view->headScript()->prependFile($file);
	}
	
	protected function appendJS($file)
	{
		$this->view->headScript()->appendFile($file);
	}
	
	protected function offsetJS($offset, $file)
	{
		$this->view->headScript()->offsetSetFile($offset, $file);
	}
	
	//CSS
	
	protected function prependCSS($file)
	{
		$this->view->headLink()->prependStylesheet($file, "all");
	}
	
	protected function appendCSS($file)
	{
		$this->view->headLink()->appendStylesheet($file, "all");
	}
	
	protected function offsetCSS($offset, $file)
	{
		$this->view->headLink()->offsetSetStylesheet($offset, $file, "all");
	}
	
	//Style
	
	protected function appendStyle($content)
	{
		$this->view->headStyle()->appendStyle($content);
	}
	
	//Load directory
	
	public function loadDir($webPath)
	{
		$dir 	= Zend_Registry::get("dir");
		$server = Zend_Registry::get("server");
		
		//Javascript
		if(file_exists(PS_Util_File::mergePaths($dir->webroot, $webPath.".js"))){
			$this->prependJS("http://".$server->statichost.$webPath.".js");
		}
		
		//CSS
		if(file_exists(PS_Util_File::mergePaths($dir->webroot, $webPath.".css"))){
			$this->prependCSS("http://".$server->statichost.$webPath.".css");
		}
	}
	
	//Error no script
	
	protected function errorOnNoScript()
	{
		$this->view->placeholder("head")
		->set('<noscript><meta http-equiv="refresh" content="0; URL=/error/javascript"/></noscript>');
	}
}