<?php


class PS_Controller_Action_Helper_TranslateLayout extends Zend_Controller_Action_Helper_Abstract
{
	public function init()
	{
		
	}
	
	public function postDispatch()
	{
		$translate 	= Zend_Registry::get("translate");
		$layout 	= Zend_Layout::getMvcInstance()->getLayout();
		
		try{
			$translate->addTranslation("../application/locale/layout.".$layout.".tmx");
		}
		catch(Exception $e){}
	}
}