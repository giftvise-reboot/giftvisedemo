<?php


class PS_Controller_Action_Helper_TranslateController extends Zend_Controller_Action_Helper_Abstract
{
	public function init()
	{
		
	}
	
	public function preDispatch()
	{
		try{
			$request = $this->getRequest();
			 
			$translate = Zend_Registry::get("translate");
			$translate->addTranslation("../application/modules/".$request->getParam("module")."/locale/".$request->getParam("controller").".tmx");
		}
		catch(Exception $e){}
	}
}