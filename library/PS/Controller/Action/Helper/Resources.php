<?php


class PS_Controller_Action_Helper_Resources extends Zend_Controller_Action_Helper_Abstract
{
	protected $resourceDir;
	
	protected $action;
	protected $controller;
	protected $module;
	protected $loaded 	= false;
	protected $options 	= array(
		"minified" => true
	);
	
	public function __construct(PS_Controller_Action $actionController, $resourceDir, $options = array())
	{
		$this->setActionController($actionController);
		$this->resourceDir = $resourceDir;
		
		foreach($options as $key => $value){
			$this->options[$key] = $value;
		}
	}
	
	public function init()
	{
		
	}
	
	public function set($action, $controller = NULL, $module = NULL)
	{
		$this->action 		= $action;
		$this->controller 	= $controller;
		$this->module	 	= $module;
		
		return $this;
	}
	
	public function direct($action, $controller = NULL, $module = NULL)
	{
		$this->set($action, $controller, $module);
		
		return $this;
	}
	
	public function postDispatch()
	{
		$this->load();
	}
	
	protected function load()
	{
		if($this->loaded){
			throw new PS_Controller_Action_Helper_Exception("Resources already loaded");
		}
		
		$request = $this->getRequest();
		
		if($this->action === NULL){
			$this->action = $request->getParam("action");
		}
		
		if($this->controller === NULL){
			$this->controller = $request->getParam("controller");
		}
		
		if($this->module === NULL){
			$this->module = $request->getParam("module");
		}
		
		//Load css and js resources
		$this->getActionController()->loadDir(
			PS_Util_File::mergePaths(
				$this->resourceDir, $this->module, $this->controller, $this->action.($this->options["minified"] ? "-min" : "")
			)
		);
		
		$this->loaded = true;
	}
}