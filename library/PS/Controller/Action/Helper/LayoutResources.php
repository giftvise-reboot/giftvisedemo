<?php


class PS_Controller_Action_Helper_LayoutResources extends Zend_Controller_Action_Helper_Abstract
{
	protected $layoutDir;
	protected $layout;
	protected $loaded 	= false;
	protected $options 	= array(
		"minified" => true
	);
	
	public function __construct(PS_Controller_Action $actionController, $layoutDir, $options = array())
	{
		$this->setActionController($actionController);
		$this->layoutDir = $layoutDir;
		
		foreach($options as $key => $value){
			$this->options[$key] = $value;
		}
	}
	
	public function init()
	{
		
	}
	
	public function set($layout)
	{
		$this->layout = $layout;
		
		return $this;
	}
	
	public function direct($layout)
	{
		$this->set($layout);
		
		return $this;
	}
	
	public function postDispatch()
	{
		$this->load();
	}
	
	protected function load()
	{
		if($this->loaded){
			throw new PS_Controller_Action_Helper_Exception("Layout resources already loaded");
		}
		
		if($this->layout === NULL){
			$this->layout = Zend_Layout::getMvcInstance()->getLayout();
		}
		
		//Load css and js resources
		$this->getActionController()->loadDir(
			PS_Util_File::mergePaths(
				$this->layoutDir, $this->layout.($this->options["minified"] ? "-min" : "")
			)
		);
		
		$this->loaded = true;
	}
}