<?php


class PS_Cycle implements SeekableIterator, Countable
{
	protected $data			= array();
	protected $pointer 		= 0;
	protected $count		= 0;
	protected $numRows		= 0;
	
	protected $validParams	= array("increment", "start");
	protected $validConfig	= array("increment", "maxPages");
	
	protected $increment	= 10000;
	protected $start		= 1;
	protected $end;
	protected $maxPages		= 11;
	
	public function __construct($data, $params = array(), $config = array())
	{
		$this->setConfig($config);
		$this->setParams($params);
		
		$this->numRows = count($data);
		
		$this->determineEnd();
		$this->determineCount();
		
		$this->data = array_slice($data, $this->start - 1, $this->increment);
	}
	
	public function setParams($params)
	{
		if(!empty($params))
		{
			foreach($params as $key => $value)
			{
				if(in_array($key, $this->validParams))
				{
					$this->{$key} = $value;
				}
			}
		}
		
		return $this;
	}
	
	public function setConfig($config)
	{
		if(!empty($config))
		{
			foreach($config as $key => $value)
			{
				if(in_array($key, $this->validConfig))
				{
					if($key == "sort" and is_string($value)){
						$value = array($value => "asc");
					}
					
					$this->{$key} = $value;
				}
			}
		}
		
		return $this;
	}
	
	protected function determineEnd()
	{
		$this->end = ($this->start + $this->increment > $this->numRows) ?
			$this->numRows :
			$this->start + $this->increment - 1;
	}
	
	protected function determineCount()
	{
		$numRows = $this->end - $this->start + 1;
		
		$this->count = ($numRows < $this->increment) ? $numRows : $this->increment;
	}
	
	public function rewind()
	{
		$this->pointer = 0;
		
		return $this;
	}
	
	public function current()
	{
		return $this->data[$this->pointer];
	}
	
	public function key()
    {
        return $this->pointer;
    }
    
    public function next()
    {
        ++$this->pointer;
    }
    
    public function valid()
    {
        return $this->pointer < $this->count;
    }
    
    public function count()
    {
        return $this->count;
    }
    
    public function seek($position)
    {
        $position = (int) $position;
        
        if($position < 0 or $position > $this->count){
            
        }
        
        $this->pointer = $position;
        
        return $this;        
    }
    
    public function printAlt($remainder = 0)
    {
    	return $this->pointer % 2 == $remainder ? "alternative" : "";
    }
    
    public function printLast()
    {
    	return $this->pointer + 1 == $this->count ? "last" : "";
    }
    
    public function isEmpty()
    {
    	return $this->count == 0;
    }
    
    public function getNavLinks()
	{
		$returnStr = ($this->numRows > 0) ?
			Zend_Locale_Format::toNumber($this->start)." - ".Zend_Locale_Format::toNumber($this->end)." of ".Zend_Locale_Format::toNumber($this->numRows) :
			"0 - 0 of 0";
		
		$numPages	= ceil($this->numRows / $this->increment);
		$pageNum	= floor($this->start / $this->increment) + 1;
		$pagePad 	= ($this->maxPages - 1) / 2;
		$endPage 	= $pageNum + $pagePad;
		
		if($this->maxPages > $numPages)
		{
			$startPage 	= 1;
			$endPage	= $numPages;
		}
		else if($endPage > $this->maxPages)
		{
			$startPage 	= $pageNum - $pagePad;
			
			if($numPages < $endPage)
			{
				$endPage = $numPages;
			}
		}
		else
		{
			$startPage 	= 1;
			$endPage	= $this->maxPages;
		}
		
		$start = (($startPage - 1) * $this->increment) + 1;
		
		if($numPages > 1)
		{
			$requiredVars = array(
				"increment"	=> $this->increment,
				"sort"		=> $this->sort
			);
			
			$returnStr .= '<span class="separator">|</span>';
			
			if($this->start > 1)
			{
				$queryVars 	= $requiredVars + array("start" => $this->getPrevStart());
				$returnStr .= $this->createNavLink(
					"?".$this->createQuery($queryVars),
					"&lt;Prev ".$this->increment,
					"Previous ".$this->increment
				);
				
				$returnStr .= '<span class="navSpacer">&nbsp;</span>';
			}
			
			for($i = $startPage; $i <= $endPage; $i++)
			{
				$queryVars = $requiredVars + array("start" => $start);
				
				if($i == $pageNum){
					$returnStr .= $i;
				}
				else{
					$returnStr .= $this->createNavLink(
						"?".$this->createQuery($queryVars),
						$i,
						'Results '.$start.' - '.($start + $this->increment - 1)
					);
				}
				
				if($i != $endPage){
					$returnStr .= '<span class="navSpacer">&nbsp;</span>';
				}
				
				$start += $this->increment;
			}
			
			if($this->end < $this->numRows)
			{
				$returnStr .= '<span class="navSpacer">&nbsp;</span>';
				
				$queryVars 	= $requiredVars + array("start" => $this->getNextStart());
				$returnStr .= $this->createNavLink(
					"?".$this->createQuery($queryVars),
					"Next ".$this->increment."&gt;",
					"Next ".$this->increment
				);
			}
		}
		
		return $returnStr;
	}
	
	public function printViewAll($addParams = NULL)
	{
		$params = array(
			"start"		=> 1,
			"increment"	=> $this->numRows
		);
		
		if($addParams !== NULL){
			$params = array_merge($params, $addParams);
		}
		
		$query = $this->createQuery($params);
		
		return '<a href="?'.$query.'">View All</a>';
	}
	
	protected function createNavLink($url, $text, $title)
	{
		return '<a href="'.$url.'" title="'.$title.'">'.$text.'</a>';
	}
	
	protected function getPrevStart()
	{
		return ($this->start - $this->increment < 1) ? 1 : ($this->start - $this->increment);
	}
	
	protected function getNextStart()
	{
		return $this->start + $this->increment;
	}
	
	protected function createQuery($vars)
	{
		return http_build_query($vars);
	}
	
	public function getTotal()
    {
    	return $this->numRows;
    }
}