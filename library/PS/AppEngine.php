<?php

class PS_AppEngine
{
	private $domains		= array();
	private $modules		= array();
	private $layouts		= array();
	private $currentModule	= "default";
	private $currentLayout	= "layout";
	
	public function addDomainToModule($module, $domains)
	{
		if(!is_array($domains)){
			$domains = array($domains);
		}

		
		foreach($domains as $rule){
			$this->domains[$rule] = $module;
		}
		
		return $this;
	}
	
	public function addLayoutToModule($module, $layout)
	{
		$this->layouts[$module] = $layout;
		
		return $this;
	}
	
	public function execute()
	{
		if(defined("PS_CURRENT_MODULE")){
			$this->currentModule = PS_CURRENT_MODULE;
		}
		else{
			$httpHost = $_SERVER["HTTP_HOST"];
			
			foreach($this->domains as $rule => $module)
			{
				if(preg_match($rule, $httpHost))
				{
					$this->currentModule = $module;
					break;
				}
			}
		}
		
		if(defined("PS_CURRENT_LAYOUT")){
			$this->currentLayout = PS_CURRENT_LAYOUT;
		}
		else if(isset($this->layouts[$this->currentModule]))
		{
			foreach($this->layouts[$this->currentModule] as $layout => $rules)
			{
				if(!is_array($rules)){
					$rules = array($rules);
				}
				
				foreach($rules as $rule)
				{
					if (substr($rule, 0, 1) === '^'){
						$rule = '/' . $rule . 'i';
					}
					if(preg_match($rule, parse_url($_SERVER["REQUEST_URI"], PHP_URL_PATH)))
					{
						$this->currentLayout = $layout;
						break 2;
					}
				}
			}
		}
		
		return $this;
	}
	
	public function getModule()
	{
		return $this->currentModule;
	}
	
	public function getLayout()
	{
		return $this->currentLayout;
	}
}