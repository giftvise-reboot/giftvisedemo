<?php


abstract class PS_Form_Fields_Abstract
{
	protected static $translate = false;
	
	public static function lookForTranslation()
	{
		self::$translate = true;
	}
}