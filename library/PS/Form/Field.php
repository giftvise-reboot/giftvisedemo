<?php


class PS_Form_Field
{
	private $fieldName;
	public	$displayName;
	private $value;
	private $validators	= array();
	private $required 	= false;
	private $filters	= array();
	private $isValid 	= true;
	private $messages 	= array();
	
	public function __construct($fieldName)
	{
		$this->setName($fieldName);
		
		$this->addFilter(new Zend_Filter_StringTrim());
		$this->addFilter(new PS_Filter_StripSlashes());
	}
	
	public function setName($fieldName)
	{
		$this->fieldName 	= $fieldName;
		$this->displayName 	= PS_Util_DB::parseField($fieldName);
		
		return $this;
	}
	
	public function getName()
	{
		return $this->fieldName;
	}
	
	public function setDisplayName($displayName)
	{
		$this->displayName = trim($displayName);
		
		return $this;
	}
	
	public function setValue($value)
	{
		$this->value = $this->filter($value);
		
		return $this;
	}
	
	public function setValueClean($value)
	{
		$this->value = $value;
		
		return $this;
	}
	
	protected function filter($value)
	{
		if(is_array($value)){
			return array_map(array($this, "filter"), $value);
		}
		else
		{
			$filterChain = new Zend_Filter();
			
			foreach($this->filters as $filter){
				$filterChain->addFilter($filter);
			}
			
			return $filterChain->filter($value);
		}
	}
	
	public function getValue($select = NULL)
	{
		if($select !== NULL)
		{
			foreach($select as $key){
				$str .= "[".$key."]";
			}
			
			eval("\$value = \$this->value".$str.";");
			
			return $value;
		}
		
		return $this->value;
	}
	
	public function __toString()
	{
		return (string) $this->value;
	}
	
	public function clear()
	{
		$this->value = NULL;
		
		return $this;
	}
	
	public function isEmpty()
	{
		if($this->value === NULL or $this->value === ""){
			return true;
		}
		
		return false;
	}
	
	public function notEmpty()
	{
		return $this->valueSet();
	}
	
	public function valueSet()
	{
		if($this->value !== NULL and $this->value !== ""){
			return true;
		}
		
		return false;
	}
	
	public function clearFilters()
	{
		$this->filters = array();
	}
	
	public function addFilter($filters)
	{
		if(is_array($filters)){
			$this->filters = $filters + $this->filters;
		}
		else{
			array_unshift($this->filters, $filters);
		}
		
		return $this;
	}
	
	public function setRequired($required)
	{
		$this->required = (bool) $required;
		
		return $this;
	}
	
	public function required()
	{
		$this->required = true;
		
		return $this;
	}
	
	public function printRequired()
	{
		return $this->required ? "required" : "";
	}
	
	public function setValidator($validators)
	{
		if(!is_array($validators)){
			$validators = array($validators);
		}
		
		$this->validators = $validators;
		
		return $this;
	}
	
	public function addValidator($validators)
	{
		if(!is_array($validators)){
			$validators = array($validators);
		}
		
		$this->validators = array_merge($this->validators, $validators);
		
		return $this;
	}
	
	public function getValidator($index = 0)
	{
		return $this->validators[$index];
	}
	
	public function removeValidators()
	{
		unset($this->validators);
		
		return $this;
	}
	
	public function addError($message)
	{
		if(is_array($message)){
			$this->messages = array_merge($this->messages, $message);
		}
		else{
			$this->messages[] = $message;
		}
		
		$this->isValid = false;
		
		return $this;
	}
	
	public function validate()
	{
		if(!empty($this->validators))
		{
			foreach($this->validators as $validator)
			{
				if(!$this->required and $this->value == ""){
					continue;
				}
				
				if(!$validator->isValid($this->value)){
					$this->isValid 	= false;
					$this->messages += $validator->getMessages();
				}
			}
		}
			
		return $this;
	}
	
	public function isValid()
	{
		return $this->isValid;
	}
	
	public function getErrors()
	{
		return $this->messages;
	}
}