<?php


class PS_Form_Fields extends PS_Form_Fields_Abstract implements Iterator, Countable
{
	protected $fields = array();
	
	public function __construct($fields = NULL)
	{
		if($fields !== NULL){
			$this->addField($fields);
		}
	}
	
	public function addField($fields)
	{
		foreach($fields as $fieldName => $settings)
		{
			$field = new PS_Form_Field($fieldName);
			
			//Validators
			if($settings[0]){
				$field->setValidator($settings[0]);
			}
			
			//Filter
			if(array_key_exists("filter", $settings)){
				$field->addFilter($settings["filter"]);
			}
			
			//Required
			if(array_key_exists("required", $settings)){
				$field->setRequired($settings["required"]);
			}
			
			//Display name
			if(self::$translate and array_key_exists("translationKey", $settings)){
				$field->setDisplayName(PS_Translate::_($settings["translationKey"]));
			}
			else if(self::$translate and PS_Translate::isTranslated($fieldName)){
				$field->setDisplayName(PS_Translate::_($fieldName));
			}
			else if(array_key_exists("displayName", $settings)){
				$field->setDisplayName($settings["displayName"]);
			}
			
			//Default value
			if(array_key_exists("default", $settings)){
				$field->setValue($settings["default"]);
			}
			
			$this->fields[$fieldName] = $field;
		}
	}
	
	public function add($fields)
	{
		$this->addField($fields);
	}
	
	public function removeField($fields)
	{
		if(!is_array($fields)){
			$fields = array($fields);
		}
		
		foreach($fields as $fieldName){
			unset($this->fields[$fieldName]);
		}
	}
	
	public function remove($fields)
	{
		$this->removeField($fields);
	}
	
	public function getValue($fieldName)
	{
		if(isset($this->fields[$fieldName])){
			return $this->fields[$fieldName]->getValue();
		}
	}
	
	public function setData($data)
	{
		if(get_class($data) == "Zend_Db_Table_Rowset"){
			$data = $data->getRow(0);
		}
		
		if(get_class($data) == "Zend_Db_Table_Row"){
			$data = $data->toArray();
		}
		
		if(!empty($data))
		{
			foreach($data as $key => $value)
			{
				if($field = $this->fields[$key]){
					$field->setValue($value);
				}
			}
		}
	}
	
	public function setDataClean($data)
	{
		if(get_class($data) == "Zend_Db_Table_Rowset"){
			$data = $data->getRow(0);
		}
		
		if(get_class($data) == "Zend_Db_Table_Row"){
			$data = $data->toArray();
		}
		
		if(!empty($data))
		{
			foreach($data as $key => $value)
			{
				if($field = $this->fields[$key]){
					$field->setValueClean($value);
				}
			}
		}
	}
	
	public function __get($var)
	{
		return $this->fields[$var];
	}
	
	public function exists($fieldName)
	{
		if(array_key_exists($fieldName, $this->fields)){
			return true;
		}
		
		return false;
	}
	
	protected function filter($filter)
	{
		if($filter !== NULL)
		{
			if(!is_array($filter)){
				$filter = array($filter);
			}
			
			foreach($this->fields as $fieldName => $field)
			{
				if(in_array($fieldName, $filter)){
					$fields[$fieldName] = $field;
				}
			}
		}
		else{
			$fields = $this->fields;
		}
		
		return $fields;
	}
	
	public function toArray($filter = NULL)
	{
		$fields = $this->filter($filter);
		
		$return = array();
		
		foreach($fields as $fieldName => $field)
		{
			//Remove prefix
			$n = eregi_replace("^[A-Z0-9]+_", "", $fieldName);
			
			//Create array
			$return[$n] = $field->getValue();
		}
		
		return $return;
	}
	
	public function validate($filter = NULL)
	{
		$fields = $this->filter($filter);
		
		$throwException = false;
		
		foreach($fields as $field)
		{
			$field->validate();
			
			if(!$field->isValid()){
				$throwException = true;
			}
		}
		
		if($throwException){
			throw new PS_Form_Fields_Exception("One or more fields are invalid");
		}
	}
	
	public function getErrors($filter = NULL)
	{
		$fields = $this->filter($filter);
		
		foreach($fields as $field){
			$messages[] = $field->getMessages();
		}
		
		return $messages;
	}
	
	public function get()
	{
		return $this->fields;
	}
	
	public function notEmpty($fields)
	{
		foreach($fields as $field)
		{
			if(!$this->fields[$field]->isEmpty()){
				return true;
			}
		}
		
		return false;
	}
	
	//Iterator
	
	public function current()
	{
		return current($this->fields);
	}
	
	public function key()
    {
        return key($this->fields);
    }
    
    public function next()
    {
        next($this->fields);
    }
	
	public function rewind()
	{
		reset($this->fields);
	}
	
    public function valid()
    {
        return key($this->fields) !== NULL;
    }
    
    //Countable
    
    public function count()
    {
        return count($this->fields);
    }
}