<?php


class PS_Whois
{
	private $domain;
	private $data;
	private $summary;
	private $loaded = false;
	
	public function __construct($domain)
	{
		$this->domain = $domain;
	}
	
	public function get($searchKey = NULL)
	{
		//Lazy load
		if(!$this->loaded){
			$this->load();
		}
		
		if($searchKey){
			return $this->summary[$searchKey];
		}
		
		return $this->summary;
	}
	
	protected function load()
	{
		exec("whois ".$this->domain, $output);
		
		$i = 0;
		
		foreach($output as $line)
		{
			if($line !== ""){
				$sections[$i][] = trim($line);
			}
			else{
				$i++;
			}
		}
		
		$summary = array();
		
		//Parse summary
		foreach($sections[3] as $line)
		{
			eregi("^([^:]+):(.*)", $line, $regs);
			$key 	= eregi_replace("[[:space:]]*", "", $regs[1]);
			$value 	= trim($regs[2]);
			
			if(array_key_exists($key, $summary))
			{	
				if(!is_array($summary[$key])){
					$summary[$key] = array($summary[$key]);
				}
				
				$summary[$key][] = $value;
			}
			else{
				$summary[$key] = $value;
			}
		}

		$this->data 	= $sections;
		$this->summary	= $summary;
		$this->loaded 	= true;
	}
}