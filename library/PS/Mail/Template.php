<?php


class PS_Mail_Template
{
	protected $message;
	protected $vars;
	protected $template = "default";
	
	public function __construct($message, $vars = array())
	{
		$this->message 	= $message;
		$this->vars		= $vars;
	}
	
	public function setTemplate($template)
	{
		$this->template = $template;
		
		return $this;
	}
	
	protected function getBody($type)
	{
		$dir 	= Zend_Registry::get("dir");
		$config = Zend_Registry::get("config");
		$server = Zend_Registry::get("server");
		$locale = new Zend_Locale("en_US");
		
		if(!$this->message){
			throw new Exception("Email message not set");
		}
		
		$vars = array_merge(
			$this->vars,
			array(
				"config"		=> $config,
				"server"		=> $server,
				"messagePath" 	=> PS_Util_File::mergePaths($dir->email, "messages", $this->message, $locale, $type.".phtml")
			)
		);
		
		return PS_Util::getContents(
			PS_Util_File::mergePaths($dir->email, "templates", $this->template, $locale, $type.".phtml"),
			$vars
		);
	}
	
	public function getHTML()
	{
		return $this->getBody("html");
	}
	
	
	public function getText()
	{
		return $this->getBody("text");
	}
}