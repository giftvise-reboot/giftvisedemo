<?php


class PS_SFTP
{
	protected $host;
	protected $username;
	protected $password;
	protected $connection;
	protected $sftp;
	
	public function __construct($host, $username, $password)
	{
		$this->host 	= $host;
		$this->username = $username;
		$this->password = $password;
		
		//Connect to host
		if(!($connection = ssh2_connect($host))){
			throw new Exception("Could not connect to ".$host);
		}
		
		//Authenticate
		if(!ssh2_auth_password($connection, $username, $password)){ 
			throw new Exception("Authentication failed for ".$username);
		}
		
		//Initialize SFTP subsystem
		$sftp = ssh2_sftp($connection);
		
		$this->connection 	= $connection;
		$this->sftp			= $sftp;
	}
	
	public function getSFTP()
	{
		return $this->sftp;
	}
	
	public function getConnection()
	{
		return $this->connection;
	}
	
	public function rename($from, $to)
	{
		ssh2_sftp_rename($this->sftp, $from, $to);
	}
	
	public function getFileSize($file)
	{
		return filesize("ssh2.sftp://".$this->sftp.$file);
	}
	
	public function stat($file)
	{
		return ssh2_sftp_stat($this->sftp, $file);
	}
	
	public function download($remoteFile, $localFile)
	{
		//Remote file
		if(!$stream = fopen("ssh2.sftp://".$this->sftp.$remoteFile, "r")){
			throw new Exception("Could not open remote file ".$remoteFile);
		}
		
		//Local file
		if(!$handle = fopen($localFile, "w")){
			throw new Exception("Could not open local file ".$localFile);
		}
		
		$read 		= 0;
		$size 		= $this->getFileSize($remoteFile);
		$bufferSize = 8192;
		
		//Write to local file
	    while($read < $size and ($buffer = fread($stream, $bufferSize)))
	    {
	    	$read += strlen($buffer);
	    	
			fwrite($handle, $buffer);
		}
	    
	    fclose($stream);
	    fclose($handle);
	}
	
	public function upload($localFile, $remoteFile)
	{
		//Local file
		if(!$handle = fopen($localFile, "r")){
			throw new Exception("Could not open local file ".$localFile);
		}
		
		//Remote file
		if(!$stream = fopen("ssh2.sftp://".$this->sftp.$remoteFile, "w")){
			throw new Exception("Could not open remote file ".$remoteFile);
		}
		
		//Write to remote file
	    while(!feof($handle)){
			fwrite($stream, fread($handle, 8192));
		}
	    
	    fclose($stream);
	    fclose($handle);
	}
}