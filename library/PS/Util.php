<?php


class PS_Util
{
	public static function p()
	{
		$args = func_get_args();
		
		echo "<pre style='text-align:left; margin-left:150px;'>";
		
		foreach($args as $arg){
			print_r($arg);
		}
		
		echo "</pre>";
	}
	
	public static function redirect($url)
	{
		header("Location: ".$url);
		exit();
	}
	
	public static function getContents($file, $vars = array())
	{
		if(!empty($vars)){
			extract($vars);
		}
		
		ob_start();
		include($file);
		$contents = ob_get_contents();
		ob_end_clean();
		
		return $contents;
	}
	
	public static function uuid($prefix = NULL)
	{
		return uniqid($prefix, true);
	}
	
	public static function randKey($len = 8)
	{
		$lchar 	= 0;
		$char 	= 0;
		
		for($i = 0; $i < $len; $i++)
		{
			while($char == $lchar)
			{
				$char = rand(48, 109);
				
				if($char > 57) $char += 7;
				if($char > 90) $char += 6;
			}
			
			$key .= chr($char);
			$lchar = $char;
		}
		
		return $key;
	}
	
	public static function sha1Encode($value)
	{
		return base64_encode(sha1($value, true));
	}
	
	public static function max($array)
	{
		foreach($array as $value)
		{
			$value = floatval($value);
			
			if($value > $max){
				$max = $value;
			}
		}
		
		return $max;
	}
	
	public static function parseCamelCasing($str)
	{
		return ucwords(ereg_replace("([A-Z]{1})", " \\1", $str));
	}
	
	public static function convertToCamelCasing($str)
	{
		$str 	= str_replace(" ", "", ucwords($str));
		$str{0} = strtolower($str{0});
		
		return $str;
	}
	
	public static function calculateAge($dob)
	{
		$formattedDOB = date("Y-m-d", strtotime($dob));
		
	    list($year, $month, $day) = explode("-", $formattedDOB);
	    
	    $yearDiff  	= date("Y") - $year;
	    $monthDiff 	= date("m") - $month;
	    $dayDiff	= date("d") - $day;
	    
	    if($monthDiff < 0){
	    	$yearDiff--;
	    }
	    else if($monthDiff == 0 and $dayDiff < 0){
	    	$yearDiff--;
	    }
	    
	    return $yearDiff;
	}
	
	public static function secondsTillNextHour()
	{
		$date = new DateTime(NULL, new DateTimeZone("GMT"));
		$date->add(new DateInterval("PT1H"));
		
		return strtotime($date->format("Y-m-d H:00:00 e")) - time();
	}
	
	public static function secondsTillNextDay($timezone = "America/Los_Angeles")
	{
		return strtotime(date("Y-m-d 00:00:00", strtotime("+1 day"))." ".$timezone) - time();
	}
}