<?php


class PS_Messages
{
	const TYPE_SUCCESS 		= "success";
	const TYPE_ERROR 		= "error";
	const TYPE_WARN 		= "warning";
	const TYPE_NOTE 		= "note";
	
	public static function add($message, $type = NULL, $sticky = false)
	{		
		$_SESSION["messages"][$type][] = array(
			"text" 		=> $message,
			"sticky"	=> $sticky
		);
	}
	
	public static function get()
	{
		$messages = $_SESSION["messages"];
		
		//Clear messages
		self::clear();
		
		return $messages;
	}
	
	public static function getJS()
	{
		return json_encode(self::get());
	}
	
	public static function clear()
	{
		unset($_SESSION["messages"]);
	}
	
	public static function exist()
	{
		return count($_SESSION["messages"]);
	}
}