<?php



class PS_Auth
{
	protected static $_instance = null;
	protected $viewablePaths;
	
	/**
     * Returns an instance of PS_Auth
     *
     * Singleton pattern implementation
     *
     * @return SBW_Auth Provides a fluent interface
     */
	public static function getInstance()
    {
        if (null === self::$_instance) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }
    
    /**
     * Singleton pattern implementation makes "new" unavailable
     *
     * @return void
     */
    private function __construct(){}

    /**
     * Singleton pattern implementation makes "clone" unavailable
     *
     * @return void
     */
    private function __clone(){}
	
	public function setViewable($paths)
	{
		if(!is_array($paths)){
			$paths = array($paths);
		}
		
		$this->viewablePaths = $paths;
		
		return $this;
	}
	
	public function check($loggedIn)
	{
		$server = Zend_Registry::get("server");
		$currentPath = rtrim(
			parse_url($_SERVER["REQUEST_URI"], PHP_URL_PATH),
			"/"
		);
		
		if(!$loggedIn)
		{
			//Special case for ajax requests
			if(Zend_Controller_Request_Http::getHeader("X_REQUESTED_WITH") == "XMLHttpRequest"){
				PS_Util::redirect("/error/xhr");
			}
			
			foreach($this->viewablePaths as $path)
			{
				if(preg_match($path, $currentPath)){
					$viewable = true;
					break;
				}
			}
			
			if(!$viewable){
				PS_Util::redirect("http://".$server->apphost."/auth");
			}
		}
		
		return $this;
	}
}