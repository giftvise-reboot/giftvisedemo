<?php


class PS_DB
{
	protected $masterServers;
	protected $slaveServers;
	protected $numMasterServers 		= 0;
	protected $numSlaveServers 			= 0;
    protected $masterConnection;
    protected $slaveConnection;
    protected $currentConnection;
    protected $masterServerNum			= 1;
    protected $slaveServerNum;
    protected $fetchMode;
    protected $forceConnection 			= false;
    protected $connectedTo 				= "slave";
    protected $enabledReadFromMaster	= true;
    protected $slaveServerFail 			= array();
    protected $allSlaveServersFail		= false;
    
    public function __construct($config)
    {
    	$this->parseConfig($config);
    	
    	$this->masterConnection 	= $this->initMasterConnection();
    	$this->slaveConnection 		= $this->currentConnection = $this->initSlaveConnection();
    }
    
    protected function parseConfig($config)
    {
    	$adapter	= $config->adapter;
    	$params		= $config->params->toArray();
    	
    	foreach($config->servers->toArray() as $server => $host)
    	{
    		preg_match("/(master|slave)([0-9]+)/", $server, $regs);
    		
    		$serverType 	= $regs[1];
    		$serverNum 		= $regs[2];
    		$params["host"] = $host;
    		
    		//Number of servers
    		$this->{"num".ucfirst($serverType)."Servers"}++;
    		
    		//Server config
    		$this->{$serverType."Servers"}[$serverNum] = (object) array(
    			"adapter"	=> $adapter,
    			"params"	=> $params
    		);
    	}
    }
    
    public function setFetchMode($mode)
    {
    	$this->fetchMode = $mode;
    	
    	$this->masterConnection->setFetchMode($mode);
    	$this->slaveConnection->setFetchMode($mode);
    }
    
    protected function initMasterConnection()
    {
    	$selectedServer = $this->masterServers[1];
	    
    	return $this->initConnection($selectedServer);
    }
    
    protected function initSlaveConnection($serverNum = NULL)
    {
    	//Randomly select a slave server
    	if($serverNum === NULL){
	    	$serverNum = mt_rand(1, $this->numSlaveServers);
	    }
    	
    	$selectedServer 		= $this->slaveServers[$serverNum];
    	$this->slaveServerNum 	= $serverNum;
    	
    	return $this->initConnection($selectedServer);
    }
    
    protected function initConnection($server)
    {
    	return Zend_Db::factory($server->adapter, $server->params);
    }
    
    public function getMasterConnection()
    {
    	return $this->masterConnection;
    }
    
    public function getSlaveConnection()
    {
    	return $this->slaveConnection;
    }
    
    public function getConnection()
    {
    	return $this->currentConnection;
    }
    
    protected function switchToMasterConnection()
    {
    	$this->connectedTo			= "master";
    	$this->currentConnection 	= $this->masterConnection;
    }
    
    protected function changeSlaveConnection()
    {
    	$numServerFail = count($this->slaveServerFail);
    	
    	//All slave servers have failed
    	if($numServerFail == $this->numSlaveServers)
    	{
    		$this->allSlaveServersFail = true;
    		
    		//Read from master if all slave servers have failed
    		if($this->enabledReadFromMaster)
    		{
    			$this->connectedTo		= "master";
    			$this->slaveConnection 	= $this->masterConnection;
    			return;
	    	}
	    	
	    	throw new Exception("Failed to connect to all slave servers");
    	}
    	
    	//Randomly select a slave server that has not failed
    	do{
    		$serverNum = mt_rand(1, $this->numSlaveServers);
    	}
    	while(in_array($serverNum, $this->slaveServerFail));
    	
    	$this->slaveConnection = $this->initSlaveConnection($serverNum);
    	$this->slaveConnection->setFetchMode($this->fetchMode);
    }
    
    public function getMasterConfig($serverNum = NULL)
    {
    	if($serverNum === NULL){
    		$serverNum = $this->masterServerNum;
    	}
    	
    	return $this->masterServers[$serverNum];
    }
    
    public function getSlaveConfig($serverNum = NULL)
    {
    	if($serverNum === NULL){
    		$serverNum = $this->slaveServerNum;
    	}
    	
    	return $this->slaveServers[$serverNum];
    }
    
    public function forceMasterConnection()
    {
    	$this->forceConnection = true;
    	$this->switchToMasterConnection();
    }
    
    public function forceSlaveConnection()
    {
    	$this->forceConnection 		= true;
    	$this->connectedTo 			= "slave";
    	$this->currentConnection 	= $this->slaveConnection;
    }
    
    protected function retryRead($methodName, $args)
    {
    	//Flag server as failed
		$this->slaveServerFail[] = $this->slaveServerNum;
		
		//Change to another slave server
		$this->changeSlaveConnection();
		
		//Set new slave connection as current connection
		$this->currentConnection = $this->slaveConnection;
		
		//Call function again
		return call_user_func_array(array($this, $methodName), $args);
    }
    
    public function query($sql, $bind = array())
    {
    	if(!$this->forceConnection)
    	{
	    	if(!($sql instanceof Zend_Db_Select) and !preg_match("/^\s*SELECT/i", $sql)){
	    		$this->switchToMasterConnection();
	    	}
	    }
	    
	    try{
	    	return $this->currentConnection->query($sql, $bind);
	    }
	    catch(Exception $e)
	    {
	    	$errorCode = PS_Util_DB::getErrorCode($e->getMessage());
	    	
	    	//Failed to connect
	    	if($errorCode == 2002)
	    	{
	    		//Change slave connection and retry
	    		if($this->connectedTo == "slave"){
	    			return $this->retryRead("query", func_get_args());
		    	}
	    	}
	    	else{
	    		throw $e;
	    	}
	    }
    }
    
    public function exec($sql)
    {
    	if(!$this->forceConnection)
    	{
	    	if(!($sql instanceof Zend_Db_Select) and !preg_match("/^\s*SELECT/i", $sql)){
	    		$this->switchToMasterConnection();
	    	}
	    }
    	
	    return $this->currentConnection->exec($sql);
    }
    
    public function lastInsertId($tableName = null, $primaryKey = null)
    {
    	$this->switchToMasterConnection();
    	return $this->currentConnection->lastInsertId($tableName, $primaryKey);
    }
    
    public function fetchAll($sql, $bind = array(), $fetchMode = null)
    {
    	try{
	        return $this->currentConnection->fetchAll($sql, $bind, $fetchMode);
		}
		catch(Exception $e)
	    {
	    	$errorCode = PS_Util_DB::getErrorCode($e->getMessage());
	    	
	    	//Failed to connect
	    	if($errorCode == 2002)
	    	{
	    		//Change slave connection and retry
	    		if($this->connectedTo == "slave"){
	    			return $this->retryRead("fetchAll", func_get_args());
		    	}
	    	}
	    	else{
	    		throw $e;
	    	}
	    }
    }
    
    public function fetchRow($sql, $bind = array(), $fetchMode = null)
    {
    	try{
	    	return $this->currentConnection->fetchRow($sql, $bind, $fetchMode);
	    }
		catch(Exception $e)
	    {
	    	$errorCode = PS_Util_DB::getErrorCode($e->getMessage());
	    	
	    	//Failed to connect
	    	if($errorCode == 2002)
	    	{
	    		//Change slave connection and retry
	    		if($this->connectedTo == "slave"){
	    			return $this->retryRead("fetchRow", func_get_args());
		    	}
	    	}
	    	else{
	    		throw $e;
	    	}
	    }
    }
    
    public function fetchCol($sql, $bind = array())
    {
    	try{
	    	return $this->currentConnection->fetchCol($sql, $bind);
	    }
		catch(Exception $e)
	    {
	    	$errorCode = PS_Util_DB::getErrorCode($e->getMessage());
	    	
	    	//Failed to connect
	    	if($errorCode == 2002)
	    	{
	    		//Change slave connection and retry
	    		if($this->connectedTo == "slave"){
	    			return $this->retryRead("fetchCol", func_get_args());
		    	}
	    	}
	    	else{
	    		throw $e;
	    	}
	    }
    }
    
    public function fetchPairs($sql, $bind = array())
    {
    	try{
	    	return $this->currentConnection->fetchPairs($sql, $bind);
	    }
		catch(Exception $e)
	    {
	    	$errorCode = PS_Util_DB::getErrorCode($e->getMessage());
	    	
	    	//Failed to connect
	    	if($errorCode == 2002)
	    	{
	    		//Change slave connection and retry
	    		if($this->connectedTo == "slave"){
	    			return $this->retryRead("fetchPairs", func_get_args());
		    	}
	    	}
	    	else{
	    		throw $e;
	    	}
	    }
    }
    
    public function fetchOne($sql, $bind = array())
    {
    	try{
	    	return $this->currentConnection->fetchOne($sql, $bind);
	    }
		catch(Exception $e)
	    {
	    	$errorCode = PS_Util_DB::getErrorCode($e->getMessage());
	    	
	    	//Failed to connect
	    	if($errorCode == 2002)
	    	{
	    		//Change slave connection and retry
	    		if($this->connectedTo == "slave"){
	    			return $this->retryRead("fetchOne", func_get_args());
		    	}
	    	}
	    	else{
	    		throw $e;
	    	}
	    }
    }
    
    public function delete($table, $where = "")
    {
    	$this->switchToMasterConnection();
    	return $this->currentConnection->delete($table, $where);
    }
    
    public function insert($table, array $bind)
    {
    	$this->switchToMasterConnection();
    	return $this->currentConnection->insert($table, $bind);
    }
    
    public function update($table, array $bind, $where = '')
    {
    	$this->switchToMasterConnection();
    	return $this->currentConnection->update($table, $bind, $where);
    }
    
    public function beginTransaction()
    {
		$this->switchToMasterConnection();
		return $this->currentConnection->beginTransaction();
    }
    
    public function commit()
    {
		$this->switchToMasterConnection();
		return $this->currentConnection->commit();
    }
    
    public function rollback()
    {
		$this->switchToMasterConnection();
		return $this->currentConnection->rollback();
    }
   	
   	public function select()
   	{
   		try{
	   		return $this->currentConnection->select();
	   	}
		catch(Exception $e)
	    {
	    	$errorCode = PS_Util_DB::getErrorCode($e->getMessage());
	    	
	    	//Failed to connect
	    	if($errorCode == 2002)
	    	{
	    		//Change slave connection and retry
	    		if($this->connectedTo == "slave"){
	    			return $this->retryRead("select", func_get_args());
		    	}
	    	}
	    	else{
	    		throw $e;
	    	}
	    }
   	}
	
    public function quote($value, $type = null)
    {
    	try{
	        return $this->currentConnection->quote($value, $type);
		}
		catch(Exception $e)
	    {
	    	$errorCode = PS_Util_DB::getErrorCode($e->getMessage());
	    	
	    	//Failed to connect
	    	if($errorCode == 2002)
	    	{
	    		//Change slave connection and retry
	    		if($this->connectedTo == "slave"){
	    			return $this->retryRead("quote", func_get_args());
		    	}
	    	}
	    	else{
	    		throw $e;
	    	}
	    }
    }
	
    public function quoteInto($text, $value, $type = null, $count = null)
    {
        return $this->currentConnection->quoteInto($text, $value, $type, $count);
    }
    
    public function quoteIdentifier($ident, $auto = false)
    {
    	return $this->currentConnection->quoteIdentifier($ident, $auto);
    }
} 