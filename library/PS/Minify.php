<?php


class PS_Minify
{
	public static function run($paths)
	{
		if(!is_array($paths)){
			$paths = array($paths);
		}
		
		foreach($paths as $path)
		{
			//Path is a directory
			if(is_dir($path))
			{
				$files = PS_Util_File::readdir($path);
				
				foreach($files as $file)
				{
					if(!eregi("-min$", pathinfo($file, PATHINFO_FILENAME)))
					{
						self::run(
							PS_Util_File::mergePaths($path, $file)
						);
					}
				}
				
				continue;
			}
			
			//Get file info
			$info = pathinfo($path);
			
			//Can't minify file
			if(!in_array($info["extension"], array("css", "js"))){
				continue;
			}
			
			//Min filename
			$minfile = $info["dirname"]."/".$info["filename"]."-min.".$info["extension"];
			
			//Check if minified file is up to date
			if(filemtime($path) <= filemtime($minfile)){
				continue;
			}
			
			//Get file contents
			$contents = file_get_contents($path);
			
			//Minify contents
			switch($info["extension"])
			{
				case "css":
					$minified = self::minifyCSS($contents);
					break;
				case "js":
					$minified = self::minifyJS($contents);
					break;
			}
			
			//Write minified contents to file
			file_put_contents($minfile, $minified);
		}
	}
	
	public static function minifyJS($contents)
	{
		return JSMin::minify($contents);
	}
	
	public static function minifyCSS($contents)
	{
		//Compress whitespace
		$contents = preg_replace('/\s+/', ' ', $contents);
	
		//Remove comments
		$contents = preg_replace('/\/\*.*?\*\//', '', $contents);
	
		return trim($contents);
	}
}