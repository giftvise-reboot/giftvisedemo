<?php



class PS_Wizard_Text
{
	protected $text;
	
	public function __construct($text)
	{
		$this->text = $text;
	}
	
	public function getText($class = NULL)
	{
		return '<span class="plaintext '.$class.'">'.$this->text.'</span>';
	}
}