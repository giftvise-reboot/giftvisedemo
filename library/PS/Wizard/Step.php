<?php


class PS_Wizard_Step
{
	protected	$name;
	protected 	$href;
	protected	$regEx;
	protected 	$clickable;
	
	public function __construct($name, $regEx, $href = NULL)
	{
		$this->name 		= htmlentities($name);
		$this->regEx 		= $regEx;
		$this->href 		= $href;
		$this->clickable	= $href !== NULL ? true : false;
	}
	
	public function selected()
	{
		if(eregi($this->regEx, parse_url($_SERVER["REQUEST_URI"], PHP_URL_PATH))){
			return true;
		}
		
		return false;
	}
	
	public function setClickable($clickable)
	{
		$this->clickable = $clickable;
		
		return $this;
	}
	
	public function isClickable()
	{
		return $this->clickable;
	}
	
	public function getLink($class = NULL)
	{
		return '<a href="'.$this->href.'" class="'.$class.'">'.$this->name.'</a>';
	}
	
	public function getPlainText($class = NULL)
	{
		return '<span class="'.$class.'">'.$this->name.'</span>';
	}
}