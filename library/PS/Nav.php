<?php



/**
 * Navigation root
 *
 */
class PS_Nav
{
	private $ID;
	private $children 	= array();
	private $childID 	= 1;
	private $display;
	
	/**
	 * Contstructor
	 *
	 * @param string $ID
	 * @param boolean $display
	 */
	function __construct($ID, $display = true)
	{
		$this->ID 		= $ID;
		$this->display	= $display;
	}
	
	/**
	 * Collapse all nodes
	 *
	 */
	public function collapseAll()
	{
		foreach($this->children as $child)
		{
			$child->close();
		}
	}
	
	/**
	 * Open allnodes
	 *
	 */
	public function openAll()
	{
		foreach($this->children as $child)
		{
			$child->openAll();
		}
	}
	
	/**
	 * Disable all nodes
	 *
	 */
	public function disableAll()
	{
		foreach($this->children as $child)
		{
			$child->disable(true);
		}
	}
	
	/**
	 * Make all nodes unclickable
	 *
	 */
	public function noClickAll()
	{
		foreach($this->children as $child)
		{
			if(get_class($child) == "PS_Nav_Group"){
				$child->noClick();
			}
		}
	}
	
	/**
	 * Recursive stopping point
	 *
	 */
	public function open()
	{
		return;
	}
	
	/**
	 * Get ID
	 *
	 * @return string
	 */
	public function getID()
	{
		return $this->ID;
	}
	
	/**
	 * Add group node to node
	 *
	 * @param navGroup $group
	 */
	public function addGroup(PS_Nav_Group $group)
	{
		if($group->validLinks() and $group->display)
		{
			//Add to children
			$this->children[] = $group;
			
			//Set child ID
			$group->setID($this->childID++);
			
			//Set pointer back to parent
			$group->setParent($this);
		}
		
		return $this;
	}
	
	/**
	 * Add one or more groups
	 *
	 * @param navGroup $group,...
	 */
	public function addGroups()
	{
		foreach(func_get_args() as $group)
		{
			$this->addGroup($group);
		}
		
		return $this;
	}
	
	/**
	 * Add link to node
	 *
	 * @param navLink $link
	 */
	public function addLink(PS_Nav_Link $link)
	{
		if($link->display)
		{
			//Add to children
			$this->children[] = $link;
			
			//Set pointer back to parent
			$link->setParent($this);
			
			//Check if link is selected
			$link->checkSelected();
		}
		
		return $this;
	}
	
	/**
	 * Add one or more links
	 * 
	 * @param navLink $link,...
	 */
	public function addLinks()
	{
		foreach(func_get_args() as $link)
		{
			$this->addLink($link);
		}
		
		return $this;
	}
	
	public function addSection(PS_Nav_Section $node)
	{
		if($node->validLinks() and $node->display)
		{
			//Add to children
			$this->children[] = $node;
			
			//Set child ID
			$node->setID($this->childID++);
			
			//Set pointer back to parent
			$node->setParent($this);
		}
		
		return $this;
	}
	
	/**
	 * Add one or more groups or links
	 * 
	 * @param PS_Nav_Link|PS_Nav_Group|PS_Nav_Whitespace $node,...
	 */
	public function add()
	{
		foreach(func_get_args() as $node)
		{
			switch(get_class($node))
			{
				case "PS_Nav_Link":
					$this->addLink($node);
					break;
				case "PS_Nav_Group":
					$this->addGroup($node);
					break;
				case "PS_Nav_Section":
					$this->addSection($node);
					break;
				case "PS_Nav_Whitespace":
					$this->addSpacer($node);
					break;
			}
		}
		
		return $this;
	}
	
	/**
	 * Valid links
	 *
	 * @return boolean
	 */
	public function validLinks()
	{
		foreach($this->children as $child)
		{
			if($child->validLinks())
			{
				return true;
			}
		}
		
		return false;
	}
	
	/**
	 * Display nav
	 *
	 * @return string
	 */
	public function display()
	{
		if(!$this->validLinks() or !$this->display)
		{
			return;
		}
		
		foreach($this->children as $child)
		{
			$returnStr .= $child->display();
		}
		
		return $returnStr;
	}
	
	/**
	 * Sort IDS
	 *
	 * @param string $a
	 * @param string $b
	 * @return int
	 */
	function sortIDs($a, $b)
	{
		preg_match_all("/([0-9]+)/", $a, $regsA);
		preg_match_all("/([0-9]+)/", $b, $regsB);
		
		$aCount = count($regsA[0]);
		$bCount	= count($regsB[0]);
		
		if($aCount == $bCount)
		{
			return 0;
		}
		else if($aCount > $bCount)
		{
			return -1;
		}
		else
		{
			return 1;
		}
	}
	
	/**
	 * Get IDs
	 *
	 * @return string json encoded
	 */
	public function getIDs()
	{
		$returnArray = array();
		
		foreach($this->children as $child)
		{
			$returnArray = array_merge($returnArray, $child->getIDs());
		}
		
		usort($returnArray, array($this, "sortIDs"));
		
		return json_encode($returnArray);
	}
}